/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"


#ifndef _Included_org_gnu_gtk_Action
#define _Included_org_gnu_gtk_Action
#include "org_gnu_gtk_Action.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_action_get_type();
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Action_gtk_1action_1new
  (JNIEnv *env, jclass cls, jstring name, jstring label, jstring tooltip, jstring stockId)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	const gchar* t = (*env)->GetStringUTFChars(env, tooltip, NULL);
	const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
	jobject value = getGObjectHandle(env, (GObject *) gtk_action_new(n, l, t, s));
	(*env)->ReleaseStringUTFChars(env, name, n);
	(*env)->ReleaseStringUTFChars(env, label, l);
	(*env)->ReleaseStringUTFChars(env, tooltip, t);
	(*env)->ReleaseStringUTFChars(env, stockId, s);
	return value;
}
                                                                                
/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1name
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	const gchar* name = gtk_action_get_name(action_g);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_activate
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1activate
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	gtk_action_activate(action_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_create_icon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Action_gtk_1action_1create_1icon
  (JNIEnv *env, jclass cls, jobject action, jint iconSize)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return getGObjectHandle(env, 
			(GObject *) gtk_action_create_icon(action_g, (GtkIconSize)iconSize));
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_create_menu_item
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Action_gtk_1action_1create_1menu_1item
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return getGObjectHandle(env, 
			(GObject *) gtk_action_create_menu_item(action_g));
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_create_tool_item
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Action_gtk_1action_1create_1tool_1item
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return getGObjectHandle(env, 
			(GObject *) gtk_action_create_tool_item(action_g));
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_connect_proxy
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1connect_1proxy
  (JNIEnv *env, jclass cls, jobject action, jobject proxy)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	GtkWidget* proxy_g = (GtkWidget*)getPointerFromHandle(env, proxy);
	gtk_action_connect_proxy(action_g, proxy_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_disconnect_proxy
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1disconnect_1proxy
  (JNIEnv *env, jclass cls, jobject action, jobject proxy)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	GtkWidget* proxy_g = (GtkWidget*)getPointerFromHandle(env, proxy);
	gtk_action_disconnect_proxy(action_g, proxy_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_proxies
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1proxies
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return getGObjectHandlesFromGSList(env,gtk_action_get_proxies(action_g));
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_connect_accelerator
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1connect_1accelerator
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	gtk_action_connect_accelerator(action_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_disconnect_accelerator
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1disconnect_1accelerator
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	gtk_action_disconnect_accelerator(action_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_accel_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1accel_1path
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return (*env)->NewStringUTF(env, gtk_action_get_accel_path(action_g));
}

/*
 * Class: 	  org_gnu_gtk_Action
 * Method:	  gtk_action_set_accel_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1set_1accel_1path
  (JNIEnv *env, jclass cls, jobject action, jstring path)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	const gchar* cpath = (*env)->GetStringUTFChars(env, path, NULL);
	gtk_action_set_accel_path(action_g, cpath);
}


/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_is_sensitive
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Action_gtk_1action_1is_1sensitive
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return (jboolean)gtk_action_is_sensitive(action_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_sensitive
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1sensitive
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return (jboolean)gtk_action_get_sensitive(action_g);
}

/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_set_sensitive
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1set_1sensitive
  (JNIEnv *env, jclass cls, jobject action, jboolean sensitive)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	gtk_action_set_sensitive(action_g, (gboolean)sensitive);
}	
                                                                                
/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_is_visible
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Action_gtk_1action_1is_1visible
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return (jboolean)gtk_action_is_visible(action_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_get_visible
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Action_gtk_1action_1get_1visible
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	return (jboolean)gtk_action_get_visible(action_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Action
 * Method:    gtk_action_set_visible
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1set_1visible
  (JNIEnv *env, jclass cls, jobject action, jboolean visible)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	gtk_action_set_visible(action_g, (gboolean)visible);
}

/*
 * Class: 	  org_gnu_gtk_Action
 * Method:	  gtk_action_set_accel_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Action_gtk_1action_1set_1accel_1group
  (JNIEnv *env, jclass cls, jobject action, jobject group)
{
	GtkAction* action_g = (GtkAction*)getPointerFromHandle(env, action);
	GtkAccelGroup* group_g = (GtkAccelGroup*)getPointerFromHandle(env, group);
	gtk_action_set_accel_group(action_g, group_g);
}

#ifdef __cplusplus
}
#endif
#endif
