/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Item.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Item
 * Method:    gtk_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Item_gtk_1item_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_item_get_type ();
}

/*
 * Class:     org.gnu.gtk.Item
 * Method:    gtk_item_select
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Item_gtk_1item_1select (JNIEnv *env, jclass cls, jobject 
    item) 
{
    GtkItem *item_g = (GtkItem *)getPointerFromHandle(env, item);
    gtk_item_select (item_g);
}

/*
 * Class:     org.gnu.gtk.Item
 * Method:    gtk_item_deselect
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Item_gtk_1item_1deselect (JNIEnv *env, jclass cls, jobject 
    item) 
{
    GtkItem *item_g = (GtkItem *)getPointerFromHandle(env, item);
    gtk_item_deselect (item_g);
}

/*
 * Class:     org.gnu.gtk.Item
 * Method:    gtk_item_toggle
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Item_gtk_1item_1toggle (JNIEnv *env, jclass cls, jobject 
    item) 
{
    GtkItem *item_g = (GtkItem *)getPointerFromHandle(env, item);
    gtk_item_toggle (item_g);
}

#ifdef __cplusplus
}

#endif
