/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventSetting.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventSetting
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventSetting_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventSetting *obj_g = (GdkEventSetting *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventSetting
 * Method:    getSendEvent
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventSetting_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventSetting *obj_g = (GdkEventSetting *)getPointerFromHandle(env, obj);
    return (jint) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventSetting
 * Method:    getAction
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventSetting_getAction (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventSetting *obj_g = (GdkEventSetting *)getPointerFromHandle(env, obj);
    return (jint) obj_g->action;
}

/*
 * Class:     org.gnu.gdk.EventSetting
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_EventSetting_getName (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventSetting *obj_g = (GdkEventSetting *)getPointerFromHandle(env, obj);
    const gchar *result_g = obj_g->name;
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
