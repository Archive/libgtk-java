/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Alignment.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Alignment
 * Method:    gtk_alignment_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Alignment_gtk_1alignment_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_alignment_get_type ();
}

/*
 * Class:     org.gnu.gtk.Alignment
 * Method:    gtk_alignment_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Alignment_gtk_1alignment_1new (JNIEnv *env, jclass cls, 
    jdouble xalign, jdouble yalign, jdouble xscale, jdouble yscale) 
{
    gfloat xalign_g = (gfloat) xalign;
    gfloat yalign_g = (gfloat) yalign;
    gfloat xscale_g = (gfloat) xscale;
    gfloat yscale_g = (gfloat) yscale;
    GObject *widget = (GObject *) gtk_alignment_new (xalign_g, yalign_g, xscale_g, yscale_g);
    return getGObjectHandle(env, widget);
}

/*
 * Class:     org.gnu.gtk.Alignment
 * Method:    gtk_alignment_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Alignment_gtk_1alignment_1set (JNIEnv *env, jclass cls, 
    jobject alignment, jdouble xalign, jdouble yalign, jdouble xscale, jdouble yscale) 
{
    GtkAlignment *alignment_g = (GtkAlignment *)getPointerFromHandle(env, alignment);
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gdouble xscale_g = (gdouble) xscale;
    gdouble yscale_g = (gdouble) yscale;
    gtk_alignment_set (alignment_g, xalign_g, yalign_g, xscale_g, yscale_g);
}

/*
 * Class:     org.gnu.gtk.Alignment
 * Method:    gtk_alignment_set_padding
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Alignment_gtk_1alignment_1set_1padding (JNIEnv *env, jclass cls, 
    jobject alignment, jint top, jint bottom, jint left, jint right) 
{
    GtkAlignment *alignment_g = (GtkAlignment *)getPointerFromHandle(env, alignment);
    gtk_alignment_set_padding (alignment_g, 
				(guint) top, (guint) bottom,
				(guint) left, (guint) right);
}

/*
 * Class:     org_gnu_gtk_Alignment
 * Method:    gtk_alignment_get_padding
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Alignment_gtk_1alignment_1get_1padding
  (JNIEnv *env, jclass cls, jobject alignment, jintArray top, jintArray bottom, jintArray left, jintArray right)
{
	//FIXME Check if I can remove the warning
//	int* t;
//	int* b;
//	int* l;
//	int* r;
//	
//	t = (int*)(*env)->GetIntArrayElements(env, top, NULL);
//	b = (int*)(*env)->GetIntArrayElements(env, bottom, NULL);
//	l = (int*)(*env)->GetIntArrayElements(env, left, NULL);
//	r = (int*)(*env)->GetIntArrayElements(env, right, NULL);
//    GtkAlignment *alignment_g = (GtkAlignment *)getPointerFromHandle(env, alignment);
//	gtk_alignment_get_padding(alignment_g, t, b, l, r);
//	(*env)->ReleaseIntArrayElements(env, top, (jint*)t, 0);
//	(*env)->ReleaseIntArrayElements(env, bottom, (jint*)b, 0);
//	(*env)->ReleaseIntArrayElements(env, left, (jint*)l, 0);
//	(*env)->ReleaseIntArrayElements(env, right, (jint*)r, 0);
}
                                                                                
#
#ifdef __cplusplus
}

#endif
