/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrIterator.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.AttrIterator
 * Method:    pango_attr_iterator_range
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrIterator_pango_1attr_1iterator_1range 
  (JNIEnv *env, jclass cls, jobject iterator, jintArray start, jintArray end) 
{
    PangoAttrIterator *iterator_g = 
        (PangoAttrIterator *)getPointerFromHandle(env, iterator);
    gint *start_g = (gint *) (*env)->GetIntArrayElements (env, start, NULL);
    gint *end_g = (gint *) (*env)->GetIntArrayElements (env, end, NULL);
    pango_attr_iterator_range (iterator_g, start_g, end_g);
    (*env)->ReleaseIntArrayElements (env, start, (jint *) start_g, 0);
    (*env)->ReleaseIntArrayElements (env, end, (jint *) end_g, 0);
}

/*
 * Class:     org.gnu.pango.AttrIterator
 * Method:    pango_attr_iterator_next
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_AttrIterator_pango_1attr_1iterator_1next 
  (JNIEnv *env, jclass cls, jobject iterator) 
{
    PangoAttrIterator *iterator_g = 
        (PangoAttrIterator *)getPointerFromHandle(env, iterator);
    return (jboolean) (pango_attr_iterator_next (iterator_g));
}

/*
 * Class:     org.gnu.pango.AttrIterator
 * Method:    pango_attr_iterator_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrIterator_pango_1attr_1iterator_1copy 
  (JNIEnv *env, jclass cls, jobject iterator) 
{
    PangoAttrIterator *iterator_g = 
        (PangoAttrIterator *)getPointerFromHandle(env, iterator);
    return getStructHandle(env, pango_attr_iterator_copy (iterator_g),
                           (JGCopyFunc)NULL, 
                           (JGFreeFunc)pango_attr_iterator_destroy);
}

/*
 * Class:     org.gnu.pango.AttrIterator
 * Method:    pango_attr_iterator_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrIterator_pango_1attr_1iterator_1get 
  (JNIEnv *env, jclass cls, jobject iterator, jint type) 
{
    PangoAttrIterator *iterator_g = 
        (PangoAttrIterator *)getPointerFromHandle(env, iterator);
    PangoAttrType type_g = (PangoAttrType) type;
    return getStructHandle(env, 
                           pango_attr_iterator_get(iterator_g, 
                                                   type_g),
                           (JGCopyFunc)pango_attribute_copy, 
                           (JGFreeFunc)pango_attribute_destroy);
}

#ifdef __cplusplus
}

#endif
