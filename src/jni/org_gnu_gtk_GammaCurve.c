/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_GammaCurve.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkWidget * GtkGammaCurve_get_table (GtkGammaCurve * cptr) 
{
    return cptr->table;
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    getTable
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_GammaCurve_getTable (JNIEnv *env, jclass cls, jobject cptr)
{
    GtkGammaCurve *cptr_g = (GtkGammaCurve *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkGammaCurve_get_table (cptr_g));
}

static GtkWidget * GtkGammaCurve_get_curve (GtkGammaCurve * cptr) 
{
    return cptr->curve;
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    getCurve
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_GammaCurve_getCurve (JNIEnv *env, jclass cls, jobject cptr)
{
    GtkGammaCurve *cptr_g = (GtkGammaCurve *)getPointerFromHandle(env, cptr);
    return getGObjectHandle (env, (GObject *) GtkGammaCurve_get_curve (cptr_g));
}

static gdouble GtkGammaCurve_get_gamma (GtkGammaCurve * cptr) 
{
    return cptr->gamma;
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    getGamma
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_GammaCurve_getGamma (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkGammaCurve *cptr_g = (GtkGammaCurve *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkGammaCurve_get_gamma (cptr_g));
}

static GtkWidget * GtkGammaCurve_get_gamma_dialog (GtkGammaCurve * cptr) 
{
    return cptr->gamma_dialog;
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    getGammaDialog
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_GammaCurve_getGammaDialog (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkGammaCurve *cptr_g = (GtkGammaCurve *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkGammaCurve_get_gamma_dialog (cptr_g));
}

static GtkWidget * GtkGammaCurve_get_gamma_text (GtkGammaCurve * cptr) 
{
    return cptr->gamma_text;
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    getGammaText
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_GammaCurve_getGammaText (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkGammaCurve *cptr_g = (GtkGammaCurve *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkGammaCurve_get_gamma_text (cptr_g));
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    gtk_gamma_curve_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_GammaCurve_gtk_1gamma_1curve_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_gamma_curve_get_type ();
}

/*
 * Class:     org.gnu.gtk.GammaCurve
 * Method:    gtk_gamma_curve_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_GammaCurve_gtk_1gamma_1curve_1new (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_gamma_curve_new ());
}

#ifdef __cplusplus
}

#endif
