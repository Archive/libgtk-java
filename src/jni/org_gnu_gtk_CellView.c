/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_CellView
#define _Included_org_gnu_gtk_CellView
#include "org_gnu_gtk_CellView.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_cell_view_get_type();
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_cell_view_new());
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_new_with_text
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1new_1with_1text
  (JNIEnv *env, jclass cls, jstring text)
{
	const gchar *t = (*env)->GetStringUTFChars(env, text, NULL);
	jobject handle = getGObjectHandle(env, (GObject *) gtk_cell_view_new_with_text(t));
	(*env)->ReleaseStringUTFChars(env, text, t);
	return handle;
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_new_with_markup
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1new_1with_1markup
  (JNIEnv *env, jclass cls, jstring text)
{
	const gchar *t = (*env)->GetStringUTFChars(env, text, NULL);
	jobject handle = getGObjectHandle(env, (GObject *) gtk_cell_view_new_with_markup(t));
	(*env)->ReleaseStringUTFChars(env, text, t);
	return handle;
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_new_with_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1new_1with_1pixbuf
  (JNIEnv *env, jclass cls, jobject pixbuf)
{
	GdkPixbuf* pixbuf_g = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
	return getGObjectHandle(env, (GObject *) gtk_cell_view_new_with_pixbuf(pixbuf_g));
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_set_model
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1set_1model
  (JNIEnv *env, jclass cls, jobject view, jobject model)
{
	GtkCellView* view_g = (GtkCellView*)getPointerFromHandle(env, view);
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	gtk_cell_view_set_model(view_g, model_g);
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_set_displayed_row
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1set_1displayed_1row
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkCellView* view_g = (GtkCellView*)getPointerFromHandle(env, view);
	GtkTreePath* path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	gtk_cell_view_set_displayed_row(view_g, path_g);
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_get_displayed_row
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1get_1displayed_1row
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkCellView* view_g = (GtkCellView*)getPointerFromHandle(env, view);
	return getGBoxedHandle(env, gtk_cell_view_get_displayed_row(view_g),
			GTK_TYPE_TREE_PATH, NULL, (GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_get_size_of_row
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1get_1size_1of_1row
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkCellView* view_g;
	GtkTreePath* path_g;
	GtkRequisition* req;
	path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	view_g = (GtkCellView*)getPointerFromHandle(env, view);
	req = g_new(GtkRequisition, 1);
	gtk_cell_view_get_size_of_row(view_g, path_g, req);
	return getGBoxedHandle(env, req, GTK_TYPE_REQUISITION, NULL,
			(GBoxedFreeFunc) gtk_requisition_free);
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_set_background_color
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1set_1background_1color
  (JNIEnv *env, jclass cls, jobject view, jobject color)
{
	GtkCellView* view_g = (GtkCellView*)getPointerFromHandle(env, view);
	GdkColor* color_g = (GdkColor*)getPointerFromHandle(env, color);
	gtk_cell_view_set_background_color(view_g, color_g);
}

/*
 * Class:     org_gnu_gtk_CellView
 * Method:    gtk_cell_view_get_cell_renderers
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_CellView_gtk_1cell_1view_1get_1cell_1renderers
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkCellView* view_g = (GtkCellView*)getPointerFromHandle(env, view);
	return getGObjectHandlesFromGList(env, gtk_cell_view_get_cell_renderers(view_g));
}

#ifdef __cplusplus
}
#endif
#endif
