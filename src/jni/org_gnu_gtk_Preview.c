/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"


#ifndef _Included_org_gnu_gtk_Preview
#define _Included_org_gnu_gtk_Preview
#include "org_gnu_gtk_Preview.h"
#ifdef __cplusplus
extern "C" {
#endif


/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_preview_get_type();	
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_uninit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1uninit
  (JNIEnv *env, jclass cls)
{
	gtk_preview_uninit();
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_new
 * Signature: (I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1new
  (JNIEnv *env, jclass cls, jint previewType)
{
	GtkPreviewType pt = (GtkPreviewType)previewType;
	GtkPreview *preview;
	
	preview = (GtkPreview*)gtk_preview_new(pt);
	return getGObjectHandle(env, (GObject *) preview);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_size
 * Signature: (Lorg/gnu/glib/Handle;II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1size
  (JNIEnv *env, jclass cls, jobject preview, jint width, jint height)
{
	GtkPreview *preview_g;
	
	preview_g = (GtkPreview*)getPointerFromHandle(env, preview);
	gtk_preview_size(preview_g, (gint)width, (gint)height);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_put
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;IIIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1put
  (JNIEnv *env, jclass cls, jobject preview, jobject window, jobject gc, 
  	jint srcx, jint srcy, jint destx, jint desty, jint width, jint height)
 {
 	GtkPreview *preview_g;
 	GdkWindow *window_g;
 	GdkGC *gc_g;
 	
 	preview_g = (GtkPreview*)getPointerFromHandle(env, preview);
 	window_g = (GdkWindow*)getPointerFromHandle(env, window);
 	gc_g = (GdkGC*)getPointerFromHandle(env, gc);
 	gtk_preview_put(preview_g, window_g, gc_g, (gint)srcx, (gint)srcy,
 			(gint)destx, (gint)desty, (gint)width, (gint)height);
 }

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_draw_row
 * Signature: (Lorg/gnu/glib/Handle;[BIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1draw_1row
  (JNIEnv *env, jclass cls, jobject preview, jbyteArray data, jint x, jint y, jint w)
 {
 	GtkPreview *preview_g;
	guchar* data_g;
	 	
 	preview_g = (GtkPreview*)getPointerFromHandle(env, preview);
 	data_g = (guchar*)(*env)->GetByteArrayElements(env, data, 0);
 	gtk_preview_draw_row(preview_g, data_g, x, y, w);
 	(*env)->ReleaseByteArrayElements(env, data, (jbyte*)data_g, 0);
 }

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_expand
 * Signature: (Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1expand
  (JNIEnv *env, jclass cls, jobject preview, jboolean expand)
{
 	GtkPreview *preview_g;
	 	
 	preview_g = (GtkPreview*)getPointerFromHandle(env, preview);
 	gtk_preview_set_expand(preview_g, (gboolean)expand);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_gama
 * Signature: (D)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1gama
  (JNIEnv *env, jclass cls, jdouble gamma)
{
	gtk_preview_set_gamma((gdouble)gamma);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_color_cube
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1color_1cube
  (JNIEnv *env, jclass cls, jint nred, jint ngreen, jint nblue, jint ngrey)
{
	gtk_preview_set_color_cube(nred, ngreen, nblue, ngrey);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_install_cmap
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1install_1cmap
  (JNIEnv *env, jclass cls, jint cmap)
{
	gtk_preview_set_install_cmap(cmap);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_reserved
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1reserved
  (JNIEnv *env, jclass cls, jint nreserved)
{
	gtk_preview_set_reserved(nreserved);
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_set_dither
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1set_1dither
  (JNIEnv *env, jclass cls, jobject preview, jint dither)
{
 	GtkPreview *preview_g;
	 	
 	preview_g = (GtkPreview*)getPointerFromHandle(env, preview);
	gtk_preview_set_dither(preview_g, (GdkRgbDither)dither);	
}

/*
 * Class:     org_gnu_gtk_Preview
 * Method:    gtk_preview_reset
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Preview_gtk_1preview_1reset
  (JNIEnv *env, jclass cls)
{
	gtk_preview_reset();
}

#ifdef __cplusplus
}
#endif
#endif
