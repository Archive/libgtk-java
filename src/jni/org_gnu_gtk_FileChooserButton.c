/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FileChooserButton
#define _Included_org_gnu_gtk_FileChooserButton
#include "org_gnu_gtk_FileChooserButton.h"
#ifdef __cplusplus
extern "C" {
#endif


/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_file_chooser_button_get_type();
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1new
  (JNIEnv *env, jclass cls, jstring title, jint action)
{
	const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
	GObject *obj = (GObject *) gtk_file_chooser_button_new(title_g,
			(GtkFileChooserAction)action);
	jobject ret_val = getGObjectHandle(env, obj);
	(*env)->ReleaseStringUTFChars(env, title, title_g);
	return ret_val;
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_new_with_backend
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1new_1with_1backend
  (JNIEnv *env, jclass cls, jstring title, jint action, jstring backend)
{
	const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
	const gchar* backend_g = (*env)->GetStringUTFChars(env, backend, NULL);
	GObject *obj = (GObject *) gtk_file_chooser_button_new_with_backend(title_g,
			(GtkFileChooserAction)action, backend_g);
	jobject ret_val = getGObjectHandle(env, obj);
	(*env)->ReleaseStringUTFChars(env, title, title_g);
	(*env)->ReleaseStringUTFChars(env, backend, backend_g);
	return ret_val;
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_new_with_dialog
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1new_1with_1dialog
  (JNIEnv *env, jclass cls, jobject dialog)
{
	GtkWidget* dialog_g = (GtkWidget*)getPointerFromHandle(env, dialog);
	return getGObjectHandle(env, (GObject *) gtk_file_chooser_button_new_with_dialog(dialog_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1get_1title
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkFileChooserButton* button_g = (GtkFileChooserButton*)getPointerFromHandle(env, button);
	return (*env)->NewStringUTF(env, gtk_file_chooser_button_get_title(button_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1set_1title
  (JNIEnv *env, jclass cls, jobject button, jstring title)
{
	GtkFileChooserButton* button_g = (GtkFileChooserButton*)getPointerFromHandle(env, button);
	const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
	gtk_file_chooser_button_set_title(button_g, title_g);
	(*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_get_width_chars
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1get_1width_1chars
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkFileChooserButton* button_g = (GtkFileChooserButton*)getPointerFromHandle(env, button);
	return (jint)gtk_file_chooser_button_get_width_chars(button_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserButton
 * Method:    gtk_file_chooser_button_set_width_chars
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserButton_gtk_1file_1chooser_1button_1set_1width_1chars
  (JNIEnv *env, jclass cls, jobject button, jint width)
{
	GtkFileChooserButton* button_g = (GtkFileChooserButton*)getPointerFromHandle(env, button);
	gtk_file_chooser_button_set_width_chars(button_g, width);
}

#ifdef __cplusplus
}
#endif
#endif
