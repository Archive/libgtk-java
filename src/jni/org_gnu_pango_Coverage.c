/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Coverage.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1new 
  (JNIEnv *env, jclass cls) 
{
    return getStructHandle(env, pango_coverage_new(), 
                           (JGCopyFunc)pango_coverage_ref,
                           (JGFreeFunc)pango_coverage_unref);
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1copy 
  (JNIEnv *env, jclass cls, jobject coverage) 
{
    PangoCoverage *coverage_g = 
        (PangoCoverage *)getPointerFromHandle(env, coverage);
    return getStructHandle(env, coverage_g, 
                           (JGCopyFunc)pango_coverage_ref, 
                           (JGFreeFunc)pango_coverage_unref);
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_get
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1get 
  (JNIEnv *env, jclass cls, jobject coverage, jint index) 
{
    PangoCoverage *coverage_g = 
        (PangoCoverage *)getPointerFromHandle(env, coverage);
    gint32 index_g = (gint32) index;
    return (jint) (pango_coverage_get (coverage_g, index_g));
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_set
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1set 
  (JNIEnv *env, jclass cls, jobject coverage, jint index, jint level) 
{
    PangoCoverage *coverage_g = 
        (PangoCoverage *)getPointerFromHandle(env, coverage);
    gint32 index_g = (gint32) index;
    PangoCoverageLevel level_g = (PangoCoverageLevel) level;
    pango_coverage_set (coverage_g, index_g, level_g);
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_max
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1max 
  (JNIEnv *env, jclass cls, jobject coverage, jobject other) 
{
    PangoCoverage *coverage_g = 
        (PangoCoverage *)getPointerFromHandle(env, coverage);
    PangoCoverage *other_g = (PangoCoverage *)getPointerFromHandle(env, other);
    pango_coverage_max (coverage_g, other_g);
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_to_bytes
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1to_1bytes 
  (JNIEnv *env, jclass cls, jobject coverage) 
{
    PangoCoverage *coverage_g = 
        (PangoCoverage *)getPointerFromHandle(env, coverage);
    jbyte *bytes_j;
    guchar** bytes = NULL;
    gint numBytes;
    pango_coverage_to_bytes (coverage_g, bytes, &numBytes);
	bytes_j = (jbyte *) *bytes;
    jbyteArray ret = (*env)->NewByteArray(env, numBytes);
    (*env)->SetByteArrayRegion(env, ret, 0, numBytes, bytes_j);
    g_free( bytes );
    return ret;
    /*
    jint bytes_len = (*env)->GetArrayLength(env, bytes);
    guchar** bytes_g = (guchar**)g_malloc(bytes_len + 1);
    gint *numBytes_g = 
        (gint *) (*env)->GetIntArrayElements (env, numBytes, NULL);
    (*env)->GetByteArrayRegion(env, bytes, 0, bytes_len, (jbyte*)bytes_g);
    bytes_g[bytes_len] = 0;
    pango_coverage_to_bytes (coverage_g, bytes_g, numBytes_g);
    (*env)->ReleaseIntArrayElements (env, numBytes, (jint *) numBytes_g, 0);
    */
}

/*
 * Class:     org.gnu.pango.Coverage
 * Method:    pango_coverage_from_bytes
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Coverage_pango_1coverage_1from_1bytes 
  (JNIEnv *env, jclass cls, jbyteArray bytes, jint numBytes) 
{
    jint bytes_len = (*env)->GetArrayLength(env, bytes);
    guchar* bytes_g = (guchar*)g_malloc(bytes_len + 1);
    gint32 numBytes_g = (gint32) numBytes;
    (*env)->GetByteArrayRegion(env, bytes, 0, bytes_len, (jbyte*)bytes_g);
    bytes_g[bytes_len] = 0;
    return getStructHandle(env, 
                           pango_coverage_from_bytes(bytes_g, 
                                                     numBytes_g),
                           (JGCopyFunc)pango_coverage_ref, 
                           (JGFreeFunc)pango_coverage_unref);
}


#ifdef __cplusplus
}

#endif
