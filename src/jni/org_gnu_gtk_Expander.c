/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_Expander
#define _Included_org_gnu_gtk_Expander
#include "org_gnu_gtk_Expander.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_expander_get_type();
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1new
  (JNIEnv *env, jclass cls, jstring label)
{
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	jobject ret = getGObjectHandle(env, (GObject *) gtk_expander_new(l));
	(*env)->ReleaseStringUTFChars(env, label, l);
	return ret;
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1new_1with_1mnemonic
  (JNIEnv *env, jclass cls, jstring label)
{
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	jobject ret = getGObjectHandle(env, (GObject *) gtk_expander_new_with_mnemonic(l));
	(*env)->ReleaseStringUTFChars(env, label, l);
	return ret;
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_expanded
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1expanded
  (JNIEnv *env, jclass cls, jobject expander, jboolean expanded)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	gtk_expander_set_expanded(expander_g, (gboolean)expanded);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_expanded
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1expanded
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	return (jboolean)gtk_expander_get_expanded(expander_g);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1spacing
  (JNIEnv *env, jclass cls, jobject expander, jint spacing)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	gtk_expander_set_spacing(expander_g, (gint)spacing);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1spacing
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	return (jint)gtk_expander_get_spacing(expander_g);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1label
  (JNIEnv *env, jclass cls, jobject expander, jstring label)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	gtk_expander_set_label(expander_g, l);
	(*env)->ReleaseStringUTFChars(env, label, l);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_label
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1label
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	const gchar *label = gtk_expander_get_label(expander_g);
	return (*env)->NewStringUTF(env, label);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_use_underline
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1use_1underline
  (JNIEnv *env, jclass cls, jobject expander, jboolean underline)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	gtk_expander_set_use_underline(expander_g, (gboolean)underline);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_use_underline
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1use_1underline
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	return (gboolean)gtk_expander_get_use_underline(expander_g);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_use_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1use_1markup
  (JNIEnv *env, jclass cls, jobject expander, jboolean markup)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	gtk_expander_set_use_markup(expander_g, (gboolean)markup);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_use_markup
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1use_1markup
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	return (gboolean)gtk_expander_get_use_markup(expander_g);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_set_label_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1set_1label_1widget
  (JNIEnv *env, jclass cls, jobject expander, jobject label)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	GtkWidget* label_g = (GtkWidget*)getPointerFromHandle(env, label);
	gtk_expander_set_label_widget(expander_g, label_g);
}

/*
 * Class:     org_gnu_gtk_Expander
 * Method:    gtk_expander_get_label_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Expander_gtk_1expander_1get_1label_1widget
  (JNIEnv *env, jclass cls, jobject expander)
{
	GtkExpander* expander_g = (GtkExpander*)getPointerFromHandle(env, expander);
	return getGObjectHandle(env, (GObject *) gtk_expander_get_label_widget(expander_g));
}

#ifdef __cplusplus
}
#endif
#endif
