/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Cursor.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Cursor
 * Method:    getType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Cursor_getType 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkCursor *obj_g = (GdkCursor *)getPointerFromHandle(env, obj);
    return (jint) obj_g->type;
}

/*
 * Class:     org.gnu.gdk.Cursor
 * Method:    gdk_cursor_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Cursor_gdk_1cursor_1new 
	(JNIEnv *env, jclass cls, jint cursorType) 
{
    GdkCursor *cursor;

    cursor = gdk_cursor_new((GdkCursorType)cursorType);
	
    return getGBoxedHandle(env, cursor, gdk_cursor_get_type(),
                           NULL, (JGFreeFunc)gdk_cursor_unref);
}

/*
 * Class:     org.gnu.gdk.Cursor
 * Method:    gdk_cursor_new_from_pixmap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Cursor_gdk_1cursor_1new_1from_1pixmap 
	(JNIEnv *env, jclass cls, jobject source, jobject mask, 
	jobject fg, jobject bg, jint x, jint y) 
{
    GdkPixmap *source_g;
    GdkPixmap *mask_g;
    GdkColor *fg_g;
    GdkColor *bg_g;
    GdkCursor *cursor;
	
    source_g = (GdkPixmap *)getPointerFromHandle(env, source);
    mask_g = (GdkPixmap *)getPointerFromHandle(env, mask);
    fg_g = (GdkColor *)getPointerFromHandle(env, fg);
    bg_g = (GdkColor *)getPointerFromHandle(env, bg);

    cursor = gdk_cursor_new_from_pixmap(source_g, mask_g, 
                                        fg_g, bg_g, x, y);
			
    return getGBoxedHandle(env, cursor, gdk_cursor_get_type(),
                           NULL, (JGFreeFunc)gdk_cursor_unref);
}


#ifdef __cplusplus
}

#endif
