/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_ComboBoxEntry
#define _Included_org_gnu_gtk_ComboBoxEntry
#include "org_gnu_gtk_ComboBoxEntry.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_combo_box_entry_get_type();
}

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_combo_box_entry_new());
}

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_new_with_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1new_1with_1model
  (JNIEnv *env, jclass cls, jobject model, jint textColumn)
{
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	return getGObjectHandle(env, 
			(GObject *) gtk_combo_box_entry_new_with_model(model_g, (gint)textColumn));
}

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_set_text_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1set_1text_1column
  (JNIEnv *env, jclass cls, jobject entry, jint column)
{
	GtkComboBoxEntry* entry_g = (GtkComboBoxEntry*)getPointerFromHandle(env, entry);
	gtk_combo_box_entry_set_text_column(entry_g, (gint)column);
}

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_get_text_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1get_1text_1column
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkComboBoxEntry* entry_g = (GtkComboBoxEntry*)getPointerFromHandle(env, entry);
	return (jint)gtk_combo_box_entry_get_text_column(entry_g);
}

/*
 * Class:     org_gnu_gtk_ComboBoxEntry
 * Method:    gtk_combo_box_entry_new_text
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBoxEntry_gtk_1combo_1box_1entry_1new_1text
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_combo_box_entry_new_text());
}


#ifdef __cplusplus
}
#endif
#endif
