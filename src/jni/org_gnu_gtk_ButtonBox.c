/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ButtonBox.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ButtonBox
 * Method:    gtk_button_box_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ButtonBox_gtk_1button_1box_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_button_box_get_type ();
}

/*
 * Class:     org.gnu.gtk.ButtonBox
 * Method:    gtk_button_box_get_layout
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ButtonBox_gtk_1button_1box_1get_1layout (JNIEnv *env, 
    jclass cls, jobject widget) 
{
    GtkButtonBox *widget_g = (GtkButtonBox *)getPointerFromHandle(env, widget);
    return (jint) (gtk_button_box_get_layout (widget_g));
}

/*
 * Class:     org.gnu.gtk.ButtonBox
 * Method:    gtk_button_box_set_layout
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ButtonBox_gtk_1button_1box_1set_1layout (JNIEnv *env, 
    jclass cls, jobject widget, jint layoutStyle) 
{
    GtkButtonBox *widget_g = (GtkButtonBox *)getPointerFromHandle(env, widget);
    GtkButtonBoxStyle layoutStyle_g = (GtkButtonBoxStyle) layoutStyle;
    gtk_button_box_set_layout (widget_g, layoutStyle_g);
}

/*
 * Class:     org.gnu.gtk.ButtonBox
 * Method:    gtk_button_box_set_child_secondary
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ButtonBox_gtk_1button_1box_1set_1child_1secondary (
    JNIEnv *env, jclass cls, jobject widget, jobject child, jboolean isSecondary) 
{
    GtkButtonBox *widget_g = (GtkButtonBox *)getPointerFromHandle(env, widget);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean isSecondary_g = (gboolean) isSecondary;
    gtk_button_box_set_child_secondary (widget_g, child_g, isSecondary_g);
}

/*
 * Class:     org_gnu_gtk_ButtonBox
 * Method:    gtk_button_box_get_child_secondary
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ButtonBox_gtk_1button_1box_1get_1child_1secondary
  (JNIEnv *env, jclass cls, jobject widget, jobject child)
{
    GtkButtonBox *widget_g = (GtkButtonBox *)getPointerFromHandle(env, widget);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
	return (jboolean)gtk_button_box_get_child_secondary(widget_g, child_g);
}

#ifdef __cplusplus
}

#endif
