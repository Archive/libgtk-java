/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AspectFrame.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.AspectFrame
 * Method:    gtk_aspect_frame_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AspectFrame_gtk_1aspect_1frame_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_aspect_frame_get_type ();
}

/*
 * Class:     org.gnu.gtk.AspectFrame
 * Method:    gtk_aspect_frame_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AspectFrame_gtk_1aspect_1frame_1new (JNIEnv *env, 
    jclass cls, jstring label, jdouble xalign, jdouble yalign, jdouble ratio, jboolean 
    obeyChild) 
{
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gdouble ratio_g = (gdouble) ratio;
    gboolean obeyChild_g = (gboolean) obeyChild;
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
    jobject retval =  getGObjectHandle(env,
    		(GObject *) gtk_aspect_frame_new (label_g, xalign_g, yalign_g, ratio_g, obeyChild_g));
	(*env)->ReleaseStringUTFChars( env, label, label_g);
	return retval;    
}

/*
 * Class:     org.gnu.gtk.AspectFrame
 * Method:    gtk_aspect_frame_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AspectFrame_gtk_1aspect_1frame_1set (JNIEnv *env, 
    jclass cls, jobject aspect_frame, jdouble xalign, jdouble yalign, jdouble ratio, jboolean 
    obeyChild) 
{
    GtkAspectFrame *aspect_frame_g = (GtkAspectFrame *)getPointerFromHandle(env, aspect_frame);
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gdouble ratio_g = (gdouble) ratio;
    gboolean obeyChild_g = (gboolean) obeyChild;
    gtk_aspect_frame_set (aspect_frame_g, xalign_g, yalign_g, ratio_g, obeyChild_g);
}


#ifdef __cplusplus
}

#endif
