/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_BindingSet
#define _Included_org_gnu_gtk_BindingSet
#include "org_gnu_gtk_BindingSet.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_binding_set_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1set_1new
  (JNIEnv *env , jclass cls, jstring name)
{
	const gchar* setName = (*env)->GetStringUTFChars(env, name, NULL);
	jobject retval = getStructHandle(env, gtk_binding_set_new(setName), NULL, g_free);
	(*env)->ReleaseStringUTFChars(env, name, setName);
	return retval;
}

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_binding_set_find
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1set_1find
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar* setName = (*env)->GetStringUTFChars(env, name, NULL);
	jobject retval = getStructHandle(env, gtk_binding_set_find(setName), NULL, g_free);
	(*env)->ReleaseStringUTFChars(env, name, setName);
	return retval;
}

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_bindings_activate
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_BindingSet_gtk_1bindings_1activate
  (JNIEnv *env, jclass cls, jobject object, jint keyval, jint modifier)
{
	GtkObject* obj = getPointerFromHandle(env, object);
	return (jboolean)gtk_bindings_activate(obj, (guint)keyval, (GdkModifierType)modifier);
}

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_binding_set_activate
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1set_1activate
  (JNIEnv *env, jclass cls, jobject binding, jint keyval, jint modifier, jobject object)
{
	GtkBindingSet* bs = getPointerFromHandle(env, binding);
	GtkObject* obj = getPointerFromHandle(env, object);
	return (jboolean)gtk_binding_set_activate(bs, (guint)keyval, (GdkModifierType)modifier, obj);
}

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_binding_entry_clear
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1entry_1clear
  (JNIEnv *env, jclass cls, jobject binding, jint keyval, jint modifier)
{
	GtkBindingSet* bs = getPointerFromHandle(env, binding);
	gtk_binding_entry_clear(bs, (guint)keyval, (GdkModifierType)modifier);
}

/*
 * Class:     org_gnu_gtk_BindingSet
 * Method:    gtk_binding_set_add_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1set_1add_1path
  (JNIEnv *env, jclass cls, jobject binding, jint pathType, jstring pathPattern, jint priority)
{
	GtkBindingSet* bs = getPointerFromHandle(env, binding);
	const gchar* pattern = (*env)->GetStringUTFChars(env, pathPattern, NULL);
	gtk_binding_set_add_path(bs, (GtkPathType)pathType, pattern, (GtkPathPriorityType)priority);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_BindingSet_gtk_1bindings_1activate_1event
(JNIEnv *env, jclass cls, jobject object, jobject event)
{
    GtkObject * object_g = (GtkObject *)getPointerFromHandle(env, object);
    GdkEventKey * event_g = (GdkEventKey *)getPointerFromHandle(env, event);
    return (jboolean)gtk_bindings_activate_event(object_g, event_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_BindingSet_gtk_1binding_1entry_1remove
(JNIEnv *env, jclass cls, jobject binding_set, jint keyval, jint modifiers)
{
    GtkBindingSet * binding_set_g = (GtkBindingSet *)getPointerFromHandle(env, binding_set);
    guint keyval_g = (guint)keyval;
    GdkModifierType modifiers_g = (GdkModifierType)modifiers;
    gtk_binding_entry_remove(binding_set_g, keyval_g, modifiers_g);
}

#ifdef __cplusplus
}
#endif
#endif
