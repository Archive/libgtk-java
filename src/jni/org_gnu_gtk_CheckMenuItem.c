/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CheckMenuItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_check_menu_item_get_type ();
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_check_menu_item_new());
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1new_1with_1label (
    JNIEnv *env, jclass cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
    GObject *obj = (GObject *) gtk_check_menu_item_new_with_label (label_g);
	jobject retval = getGObjectHandle(env, obj);
	(*env)->ReleaseStringUTFChars( env, label, label_g );
	return retval;
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1new_1with_1mnemonic (JNIEnv *env, jclass 
    cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
    GObject *obj = (GObject *) gtk_check_menu_item_new_with_mnemonic(label_g);
	jobject retval =  getGObjectHandle(env, obj);
	(*env)->ReleaseStringUTFChars(env, label, label_g);
	return retval;
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_set_active
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1set_1active (
    JNIEnv *env, jclass cls, jobject check_menu_item, jboolean isActive) 
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
    gboolean isActive_g = (gboolean) isActive;
    gtk_check_menu_item_set_active (check_menu_item_g, isActive_g);
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_get_active
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1get_1active (
    JNIEnv *env, jclass cls, jobject check_menu_item) 
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
    return (jboolean) (gtk_check_menu_item_get_active (check_menu_item_g));
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_toggled
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1toggled (JNIEnv 
    *env, jclass cls, jobject check_menu_item) 
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
    gtk_check_menu_item_toggled (check_menu_item_g);
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_set_inconsistent
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1set_1inconsistent (
    JNIEnv *env, jclass cls, jobject check_menu_item, jboolean setting) 
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
    gboolean setting_g = (gboolean) setting;
    gtk_check_menu_item_set_inconsistent (check_menu_item_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.CheckMenuItem
 * Method:    gtk_check_menu_item_get_inconsistent
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1get_1inconsistent (JNIEnv *env, jclass 
    cls, jobject check_menu_item) 
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
    return (jboolean) (gtk_check_menu_item_get_inconsistent (check_menu_item_g));
}


/*
 * Class:     org_gnu_gtk_CheckMenuItem
 * Method:    gtk_check_menu_item_set_draw_as_radio
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1set_1draw_1as_1radio
  (JNIEnv *env, jclass cls, jobject check_menu_item, jboolean radio)
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
	gtk_check_menu_item_set_draw_as_radio(check_menu_item_g, (gboolean)radio);
}
                                                                                
/*
 * Class:     org_gnu_gtk_CheckMenuItem
 * Method:    gtk_check_menu_item_get_draw_as_radio
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_CheckMenuItem_gtk_1check_1menu_1item_1get_1draw_1as_1radio
  (JNIEnv *env, jclass cls, jobject check_menu_item)
{
    GtkCheckMenuItem *check_menu_item_g = 
    		(GtkCheckMenuItem *) getPointerFromHandle(env, check_menu_item);
	return (jboolean)gtk_check_menu_item_get_draw_as_radio(check_menu_item_g);
}
                                                                                

#ifdef __cplusplus
}

#endif
