/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TearoffMenuItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.TearoffMenuItem
 * Method:    gtk_tearoff_menu_item_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TearoffMenuItem_gtk_1tearoff_1menu_1item_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_tearoff_menu_item_get_type ();
}

/*
 * Class:     org.gnu.gtk.TearoffMenuItem
 * Method:    gtk_tearoff_menu_item_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TearoffMenuItem_gtk_1tearoff_1menu_1item_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_tearoff_menu_item_new ());
}

/*
 * Class:     org_gnu_gtk_TearoffMenuItem
 * Method:    getTornOff
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TearoffMenuItem_getTornOff(JNIEnv *env, jclass cls, jobject menu)
{
    GtkTearoffMenuItem *menu_g = 
        (GtkTearoffMenuItem*)getPointerFromHandle(env, menu);
    return (jboolean)menu_g->torn_off;
}

#ifdef __cplusplus
}

#endif
