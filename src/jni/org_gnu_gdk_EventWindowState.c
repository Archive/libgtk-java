/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventWindowState.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventWindowState
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventWindowState_getWindow (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventWindowState *obj_g = (GdkEventWindowState *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventWindowState
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventWindowState_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventWindowState *obj_g = (GdkEventWindowState *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventWindowState
 * Method:    getChangedMask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventWindowState_getChangedMask (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventWindowState *obj_g = (GdkEventWindowState *)getPointerFromHandle(env, obj);
    return (jint) obj_g->changed_mask;
}

/*
 * Class:     org.gnu.gdk.EventWindowState
 * Method:    getNewWindowState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventWindowState_getNewWindowState (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventWindowState *obj_g = (GdkEventWindowState *)getPointerFromHandle(env, obj);
    return (jint) obj_g->new_window_state;
}


#ifdef __cplusplus
}

#endif
