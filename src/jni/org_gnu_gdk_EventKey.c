/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventKey.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventKey_getWindow (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventKey_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getTime (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getState (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->state;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getKeyval
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getKeyval (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->keyval;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getLength
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getLength (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->length;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getString
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_EventKey_getString (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    gchar *result_g = obj_g->string;
    if (result_g) 
    	return (*env)->NewStringUTF(env, result_g);
    else
    	return (*env)->NewStringUTF(env, "");
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getHardwareKeycode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getHardwareKeycode (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->hardware_keycode;
}

/*
 * Class:     org.gnu.gdk.EventKey
 * Method:    getGroup
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventKey_getGroup (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventKey *obj_g = (GdkEventKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->group;
}


#ifdef __cplusplus
}

#endif
