/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include <stdlib.h>
#include <string.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToolTipsData.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ToolTipsData
 * Method:    getToolTip
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolTipsData_getToolTip (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkTooltipsData *cptr_g = 
        (GtkTooltipsData *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) cptr_g->tooltips);
}

/*
 * Class:     org.gnu.gtk.ToolTipsData
 * Method:    getWidget
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolTipsData_getWidget (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkTooltipsData *cptr_g = 
        (GtkTooltipsData *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) cptr_g->widget);
}

/*
 * Class:     org.gnu.gtk.ToolTipsData
 * Method:    getTipText
 * Signature: (I)java.lang.String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ToolTipsData_getTipText (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkTooltipsData *cptr_g = 
        (GtkTooltipsData *)getPointerFromHandle(env, cptr);
    gchar *result_g = cptr_g->tip_text;
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.ToolTipsData
 * Method:    gtk_tooltips_data_get
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolTipsData_gtk_1tooltips_1data_1get
		(JNIEnv *env, jclass cls, jobject widget) 
{
	GtkTooltipsData *data;
	GtkTooltipsData *data_copy;
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    data_copy = g_malloc(sizeof(GtkTooltipsData));
    data = gtk_tooltips_data_get(widget_g);
    memcpy(data_copy, data, sizeof(GtkTooltipsData));
    return getStructHandle(env, data_copy, NULL, (JGFreeFunc) g_free);
}


#ifdef __cplusplus
}

#endif
