/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_HScale.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.HScale
 * Method:    gtk_hscale_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HScale_gtk_1hscale_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_hscale_get_type ();
}

/*
 * Class:     org.gnu.gtk.HScale
 * Method:    gtk_hscale_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_HScale_gtk_1hscale_1new (JNIEnv *env, jclass cls, jobject 
    adjustment) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    return getGObjectHandle(env, (GObject *) gtk_hscale_new (adjustment_g));
}

/*
 * Class:     org.gnu.gtk.HScale
 * Method:    gtk_hscale_new_with_range
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_HScale_gtk_1hscale_1new_1with_1range (JNIEnv *env, 
    jclass cls, jdouble min, jdouble max, jdouble step) 
{
    gdouble min_g = (gdouble) min;
    gdouble max_g = (gdouble) max;
    gdouble step_g = (gdouble) step;
    return getGObjectHandle(env, (GObject *) gtk_hscale_new_with_range (min_g, max_g, step_g));
}

#ifdef __cplusplus
}

#endif
