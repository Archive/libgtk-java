/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_ColorButton
#define _Included_org_gnu_gtk_ColorButton
#include "org_gnu_gtk_ColorButton.h"
#ifdef __cplusplus
extern "C" {
#endif



/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_color_button_get_type();
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_color_button_new());
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_new_with_color
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1new_1with_1color
  (JNIEnv *env, jclass cls, jobject color)
{
	GdkColor* color_g = (GdkColor*)getPointerFromHandle(env, color);
	return getGObjectHandle(env, (GObject *) gtk_color_button_new_with_color(color_g));
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_set_color
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1set_1color
  (JNIEnv *env, jclass cls, jobject button, jobject color)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	GdkColor* color_g = (GdkColor*)getPointerFromHandle(env, color);
	gtk_color_button_set_color(button_g, color_g);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_set_alpha
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1set_1alpha
  (JNIEnv *env, jclass cls, jobject button, jint alpha)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	gtk_color_button_set_alpha(button_g, (guint16)alpha);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_get_color
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1get_1color
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	GdkColor *color = g_malloc(sizeof(GdkColor));
	gtk_color_button_get_color(button_g, color);
	return getStructHandle(env, color, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_get_alpha
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1get_1alpha
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	return (jint)gtk_color_button_get_alpha(button_g);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_set_use_alpha
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1set_1use_1alpha
  (JNIEnv *env, jclass cls, jobject button, jboolean useAlpha)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	gtk_color_button_set_use_alpha(button_g, (gboolean)useAlpha);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_get_use_alpha
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1get_1use_1alpha
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	return (jboolean)gtk_color_button_get_use_alpha(button_g);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1set_1title
  (JNIEnv *env, jclass cls, jobject button, jstring title)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	const gchar* t = (gchar*)(*env)->GetStringUTFChars(env, title, NULL);
	gtk_color_button_set_title(button_g, t);
	(*env)->ReleaseStringUTFChars(env, title, t);
}

/*
 * Class:     org_gnu_gtk_ColorButton
 * Method:    gtk_color_button_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ColorButton_gtk_1color_1button_1get_1title
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkColorButton* button_g = (GtkColorButton*)getPointerFromHandle(env, button);
	const gchar *title = gtk_color_button_get_title(button_g);
	return (*env)->NewStringUTF(env, title);
}

#ifdef __cplusplus
}
#endif
#endif
