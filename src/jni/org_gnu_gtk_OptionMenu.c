/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_OptionMenu.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_option_menu_get_type ();
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1new (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_option_menu_new ());
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_get_menu
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1get_1menu (JNIEnv *env, 
    jclass cls, jobject option_menu) 
{
    GtkOptionMenu *option_menu_g = (GtkOptionMenu *)getPointerFromHandle(env, option_menu);
    return getGObjectHandle(env, (GObject *) gtk_option_menu_get_menu (option_menu_g));
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_set_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1set_1menu (JNIEnv *env, 
    jclass cls, jobject option_menu, jobject menu) 
{
    GtkOptionMenu *option_menu_g = (GtkOptionMenu *)getPointerFromHandle(env, option_menu);
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gtk_option_menu_set_menu (option_menu_g, (GtkWidget*)menu_g);
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_remove_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1remove_1menu (JNIEnv 
    *env, jclass cls, jobject option_menu) 
{
    GtkOptionMenu *option_menu_g = (GtkOptionMenu *)getPointerFromHandle(env, option_menu);
    gtk_option_menu_remove_menu (option_menu_g);
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_get_history
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1get_1history (JNIEnv 
    *env, jclass cls, jobject option_menu) 
{
    GtkOptionMenu *option_menu_g = (GtkOptionMenu *)getPointerFromHandle(env, option_menu);
    return (jint) (gtk_option_menu_get_history (option_menu_g));
}

/*
 * Class:     org.gnu.gtk.OptionMenu
 * Method:    gtk_option_menu_set_history
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_OptionMenu_gtk_1option_1menu_1set_1history (JNIEnv 
    *env, jclass cls, jobject option_menu, jint index) 
{
    GtkOptionMenu *option_menu_g = (GtkOptionMenu *)getPointerFromHandle(env, option_menu);
    gint32 index_g = (gint32) index;
    gtk_option_menu_set_history (option_menu_g, index_g);
}


#ifdef __cplusplus
}

#endif
