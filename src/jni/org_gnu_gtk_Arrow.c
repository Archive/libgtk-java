/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Arrow.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Arrow
 * Method:    gtk_arrow_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Arrow_gtk_1arrow_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_arrow_get_type ();
}

/*
 * Class:     org.gnu.gtk.Arrow
 * Method:    gtk_arrow_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Arrow_gtk_1arrow_1new (JNIEnv *env, jclass cls, jint 
    arrowType, jint shadowType) 
{
    GtkArrowType arrowType_g = (GtkArrowType) arrowType;
    GtkShadowType shadowType_g = (GtkShadowType) shadowType;
    return getGObjectHandle(env, (GObject *) gtk_arrow_new (arrowType_g, shadowType_g));
}

/*
 * Class:     org.gnu.gtk.Arrow
 * Method:    gtk_arrow_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Arrow_gtk_1arrow_1set (JNIEnv *env, jclass cls, jobject 
    arrow, jint arrowType, jint shadowType) 
{
    GtkArrow *arrow_g = (GtkArrow *)getPointerFromHandle(env, arrow);
    GtkArrowType arrowType_g = (GtkArrowType) arrowType;
    GtkShadowType shadowType_g = (GtkShadowType) shadowType;
    gtk_arrow_set (arrow_g, arrowType_g, shadowType_g);
}


#ifdef __cplusplus
}

#endif
