/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_PixbufLoader.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gdk_pixbuf_loader_get_type ();
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandleAndRef(env, G_OBJECT(gdk_pixbuf_loader_new ()));
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_new_with_type
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1new_1with_1type (
    JNIEnv *env, jclass cls, jstring imageType, jint error) 
{
    const gchar* imageType_g = (*env)->GetStringUTFChars(env, imageType, 0);
    jobject result = 
        getGObjectHandleAndRef(env, 
                         G_OBJECT(gdk_pixbuf_loader_new_with_type (imageType_g, 
								   (GError**)NULL)));
    (*env)->ReleaseStringUTFChars(env, imageType, imageType_g);
    return result;
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_write
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1write (JNIEnv 
    *env, jclass cls, jobject loader, jbyteArray buf, jint count, jint error) 
{
    GdkPixbufLoader *loader_g = (GdkPixbufLoader *)getPointerFromHandle(env, loader);
    jint buf_len = (*env)->GetArrayLength(env, buf);
    guchar* buf_g = (guchar*)g_malloc(buf_len + 1);
    gint32 count_g = (gint32) count;
    (*env)->GetByteArrayRegion(env, buf, 0, buf_len, (jbyte*)buf_g);
    buf_g[buf_len] = 0;
    return (jboolean) (gdk_pixbuf_loader_write (loader_g, buf_g, count_g, NULL));
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_get_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1get_1pixbuf (JNIEnv 
    *env, jclass cls, jobject loader) 
{
    GdkPixbufLoader *loader_g = (GdkPixbufLoader *)getPointerFromHandle(env, loader);
    return getGObjectHandle(env, G_OBJECT(gdk_pixbuf_loader_get_pixbuf (loader_g)));
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_get_animation
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1get_1animation (
    JNIEnv *env, jclass cls, jobject loader) 
{
    GdkPixbufLoader *loader_g = (GdkPixbufLoader *)getPointerFromHandle(env, loader);
    return getGObjectHandle(env, G_OBJECT(gdk_pixbuf_loader_get_animation (loader_g)));
}

/*
 * Class:     org.gnu.gdk.PixbufLoader
 * Method:    gdk_pixbuf_loader_close
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufLoader_gdk_1pixbuf_1loader_1close (JNIEnv 
    *env, jclass cls, jobject loader, jint error) 
{
    GdkPixbufLoader *loader_g = (GdkPixbufLoader *)getPointerFromHandle(env, loader);
    return (jboolean) (gdk_pixbuf_loader_close (loader_g, NULL));
}


#ifdef __cplusplus
}

#endif
