/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"

#include "org_gnu_pango_LayoutLine.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1ref (JNIEnv *env, 
    jclass cls, jobject line) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    pango_layout_line_ref (line_g);
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1unref (JNIEnv *env, 
    jclass cls, jobject line) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    pango_layout_line_unref (line_g);
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_x_to_index
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1x_1to_1index (
    JNIEnv *env, jclass cls, jobject line, jint xPos, jintArray index, jintArray trailing) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    gint32 xPos_g = (gint32) xPos;
    gint *index_g = (gint *) (*env)->GetIntArrayElements (env, index, NULL);
    gint *trailing_g = (gint *) (*env)->GetIntArrayElements (env, trailing, NULL);
    jboolean result_j = (jboolean) (pango_layout_line_x_to_index (line_g, xPos_g, index_g, 
                trailing_g));
    (*env)->ReleaseIntArrayElements (env, index, (jint *) index_g, 0);
    (*env)->ReleaseIntArrayElements (env, trailing, (jint *) trailing_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_index_to_x
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1index_1to_1x (JNIEnv 
    *env, jclass cls, jobject line, jint index, jboolean trailing, jintArray xPos) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    gint32 index_g = (gint32) index;
    gboolean trailing_g = (gboolean) trailing;
    gint *xPos_g = (gint *) (*env)->GetIntArrayElements (env, xPos, NULL);
    pango_layout_line_index_to_x (line_g, index_g, trailing_g, xPos_g);
    (*env)->ReleaseIntArrayElements (env, xPos, (jint *) xPos_g, 0);
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_get_x_ranges
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1get_1x_1ranges (
    JNIEnv *env, jclass cls, jobject line, jint startIndex, jint endIndex, jintArray ranges, 
    jintArray numRanges) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    gint32 startIndex_g = (gint32) startIndex;
    gint32 endIndex_g = (gint32) endIndex;
    gint **ranges_g = (gint **) (*env)->GetIntArrayElements (env, ranges, NULL);
    gint *numRanges_g = (gint *) (*env)->GetIntArrayElements (env, numRanges, NULL);
    pango_layout_line_get_x_ranges (line_g, startIndex_g, 
				    endIndex_g, ranges_g, numRanges_g);
    (*env)->ReleaseIntArrayElements (env, ranges, (jint *) ranges_g, 0);
    (*env)->ReleaseIntArrayElements (env, numRanges, (jint *) numRanges_g, 0);
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_get_extents
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1get_1extents (JNIEnv 
    *env, jclass cls, jobject line, jobject inkRect, jobject logicalRect) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
	pango_layout_line_get_extents (line_g, inkRect_g, logicalRect_g);
}

/*
 * Class:     org.gnu.pango.LayoutLine
 * Method:    pango_layout_line_get_pixel_extents
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_LayoutLine_pango_1layout_1line_1get_1pixel_1extents (
    JNIEnv *env, jclass cls, jobject line, jobject inkRect, jobject logicalRect) 
{
    PangoLayoutLine *line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
    pango_layout_line_get_pixel_extents (line_g, inkRect_g, logicalRect_g);
}


#ifdef __cplusplus
}

#endif
