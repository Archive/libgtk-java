/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventOwnerChange.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gdk_EventOwnerChange
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventOwnerChange_getWindow
  (JNIEnv *env, jclass cls, jobject event)
{
    GdkEventOwnerChange* event_g = (GdkEventOwnerChange*)getPointerFromHandle(env, event);
    return getGObjectHandleAndRef(env, (GObject *)event_g->window);
}

/*
 * Class:     org_gnu_gdk_EventOwnerChange
 * Method:    getSendEvent
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventOwnerChange_getSendEvent
  (JNIEnv *env, jclass cls, jobject event)
{
    GdkEventOwnerChange* event_g = (GdkEventOwnerChange*)getPointerFromHandle(env, event);
    return (jint)event_g->send_event;
}

#ifdef __cplusplus
}

#endif
