/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Border.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GtkBorder_get_left (GtkBorder * cptr) 
{
    return cptr->left;
}

/*
 * Class:     org.gnu.gtk.Border
 * Method:    getLeft
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Border_getLeft (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkBorder *cptr_g = (GtkBorder *)getPointerFromHandle(env, cptr);
    return (jint) (GtkBorder_get_left (cptr_g));
}

static gint32 GtkBorder_get_right (GtkBorder * cptr) 
{
    return cptr->right;
}

/*
 * Class:     org.gnu.gtk.Border
 * Method:    getRight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Border_getRight (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkBorder *cptr_g = (GtkBorder *)getPointerFromHandle(env, cptr);
    return (jint) (GtkBorder_get_right (cptr_g));
}

static gint32 GtkBorder_get_top (GtkBorder * cptr) 
{
    return cptr->top;
}

/*
 * Class:     org.gnu.gtk.Border
 * Method:    getTop
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Border_getTop (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkBorder *cptr_g = (GtkBorder *)getPointerFromHandle(env, cptr);
    return (jint) (GtkBorder_get_top (cptr_g));
}

static gint32 GtkBorder_get_bottom (GtkBorder * cptr) 
{
    return cptr->bottom;
}

/*
 * Class:     org.gnu.gtk.Border
 * Method:    getBottom
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Border_getBottom (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkBorder *cptr_g = (GtkBorder *)getPointerFromHandle(env, cptr);
    return (jint) (GtkBorder_get_bottom (cptr_g));
}


#ifdef __cplusplus
}

#endif
