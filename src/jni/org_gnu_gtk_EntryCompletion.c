/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_EntryCompletion
#define _Included_org_gnu_gtk_EntryCompletion
#include "org_gnu_gtk_EntryCompletion.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_entry_completion_get_type();
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_entry_completion_new());
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_entry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1entry
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return getGObjectHandle(env, (GObject *) gtk_entry_completion_get_entry(entry_g));
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_model
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1model
  (JNIEnv *env, jclass cls, jobject entry, jobject model)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	gtk_entry_completion_set_model(entry_g, model_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1model
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return getGObjectHandle(env, (GObject *) gtk_entry_completion_get_model(entry_g));
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_minimum_key_length
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1minimum_1key_1length
  (JNIEnv *env, jclass cls, jobject entry, jint length)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_set_minimum_key_length(entry_g, (gint)length);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_minimum_key_length
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1minimum_1key_1length
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return (jint)gtk_entry_completion_get_minimum_key_length(entry_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_complete
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1complete
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_complete(entry_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion  
 * Method:    gtk_entry_completion_insert_prefix
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1insert_1prefix
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_insert_prefix(entry_g);
} 

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_insert_action_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1insert_1action_1text
  (JNIEnv *env, jclass cls, jobject entry, jint index, jstring text)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	const gchar* t = (gchar*)(*env)->GetStringUTFChars(env, text, NULL);
	gtk_entry_completion_insert_action_text(entry_g, (gint)index, t);
	(*env)->ReleaseStringUTFChars(env, text, t);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_insert_action_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1insert_1action_1markup
  (JNIEnv *env, jclass cls, jobject entry, jint index, jstring markup)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	const gchar* m = (gchar*)(*env)->GetStringUTFChars(env, markup, NULL);
	gtk_entry_completion_insert_action_markup(entry_g, (gint)index, m);
	(*env)->ReleaseStringUTFChars(env, markup, m);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_delete_action
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1delete_1action
  (JNIEnv *env, jclass cls, jobject entry, jint index)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_delete_action(entry_g, (gint)index);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_inline_completion
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1inline_1completion
  (JNIEnv *env, jclass cls, jobject entry, jboolean value)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_set_inline_completion(entry_g, (gboolean)value);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_inline_completion
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1inline_1completion
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return (jboolean)gtk_entry_completion_get_inline_completion(entry_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_popup_completion
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1popup_1completion
  (JNIEnv *env, jclass cls, jobject entry, jboolean value)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_set_popup_completion(entry_g, (gboolean)value);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_popup_completion
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1popup_1completion
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return (jboolean)gtk_entry_completion_get_popup_completion(entry_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_text_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1text_1column
  (JNIEnv *env, jclass cls, jobject entry, jint column)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_set_text_column(entry_g, (gint)column);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_text_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1text_1column
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return (jint)gtk_entry_completion_get_text_column(entry_g);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_set_popup_set_width
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1set_1popup_1set_1width
  (JNIEnv *env, jclass cls, jobject entry, jboolean value)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	gtk_entry_completion_set_popup_set_width(entry_g, value);
}

/*
 * Class:     org_gnu_gtk_EntryCompletion
 * Method:    gtk_entry_completion_get_popup_set_width
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EntryCompletion_gtk_1entry_1completion_1get_1popup_1set_1width
  (JNIEnv *env, jclass cls, jobject entry)
{
	GtkEntryCompletion* entry_g = (GtkEntryCompletion*)getPointerFromHandle(env, entry);
	return (jboolean)gtk_entry_completion_get_popup_set_width(entry_g);
}


#ifdef __cplusplus
}
#endif
#endif
