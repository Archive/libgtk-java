/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventMotion.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventMotion_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventMotion_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventMotion_getTime (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getX
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventMotion_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->x;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getY
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventMotion_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->y;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventMotion_getState (JNIEnv *env, jclass cls, jobject obj)
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jint) obj_g->state;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getIsHint
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventMotion_getIsHint (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jint) obj_g->is_hint;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getDevice
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventMotion_getDevice (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->device);
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getXRoot
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventMotion_getXRoot (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->x_root;
}

/*
 * Class:     org.gnu.gdk.EventMotion
 * Method:    getYRoot
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventMotion_getYRoot (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventMotion *obj_g = (GdkEventMotion *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->y_root;
}


#ifdef __cplusplus
}

#endif
