/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventNoExpose.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventNoExpose
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventNoExpose_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventNoExpose *obj_g = (GdkEventNoExpose *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventNoExpose
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventNoExpose_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventNoExpose *obj_g = (GdkEventNoExpose *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}


#ifdef __cplusplus
}

#endif
