/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Misc.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Misc
 * Method:    gtk_misc_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Misc_gtk_1misc_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_misc_get_type ();
}

/*
 * Class:     org.gnu.gtk.Misc
 * Method:    gtk_misc_set_alignment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Misc_gtk_1misc_1set_1alignment (JNIEnv *env, jclass 
    cls, jobject misc, jdouble xalign, jdouble yalign) 
{
    GtkMisc *misc_g = (GtkMisc *)getPointerFromHandle(env, misc);
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gtk_misc_set_alignment (misc_g, xalign_g, yalign_g);
}

/*
 * Class:     org.gnu.gtk.Misc
 * Method:    gtk_misc_get_alignment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Misc_gtk_1misc_1get_1alignment (JNIEnv *env, jclass 
    cls, jobject misc, jdoubleArray xalign, jdoubleArray yalign) 
{
    GtkMisc *misc_g = (GtkMisc *)getPointerFromHandle(env, misc);
    gdouble *xalign_g = (gdouble *) (*env)->GetDoubleArrayElements (env, xalign, NULL);
    gdouble *yalign_g = (gdouble *) (*env)->GetDoubleArrayElements (env, yalign, NULL);
    gtk_misc_get_alignment (misc_g, (gfloat*)xalign_g, (gfloat*)yalign_g);
    (*env)->ReleaseDoubleArrayElements (env, xalign, (jdouble *) xalign_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, yalign, (jdouble *) yalign_g, 0);
}

/*
 * Class:     org.gnu.gtk.Misc
 * Method:    gtk_misc_set_padding
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Misc_gtk_1misc_1set_1padding (JNIEnv *env, jclass cls, 
    jobject misc, jint xpad, jint ypad) 
{
    GtkMisc *misc_g = (GtkMisc *)getPointerFromHandle(env, misc);
    gint32 xpad_g = (gint32) xpad;
    gint32 ypad_g = (gint32) ypad;
    gtk_misc_set_padding (misc_g, xpad_g, ypad_g);
}

/*
 * Class:     org.gnu.gtk.Misc
 * Method:    gtk_misc_get_padding
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Misc_gtk_1misc_1get_1padding (JNIEnv *env, jclass cls, 
    jobject misc, jintArray xpad, jintArray ypad) 
{
    GtkMisc *misc_g = (GtkMisc *)getPointerFromHandle(env, misc);
    gint *xpad_g = (gint *) (*env)->GetIntArrayElements (env, xpad, NULL);
    gint *ypad_g = (gint *) (*env)->GetIntArrayElements (env, ypad, NULL);
    gtk_misc_get_padding (misc_g, xpad_g, ypad_g);
    (*env)->ReleaseIntArrayElements (env, xpad, (jint *) xpad_g, 0);
    (*env)->ReleaseIntArrayElements (env, ypad, (jint *) ypad_g, 0);
}


#ifdef __cplusplus
}

#endif
