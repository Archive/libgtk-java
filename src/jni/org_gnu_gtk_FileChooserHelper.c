/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FileChooserHelper
#define _Included_org_gnu_gtk_FileChooserHelper
#include "org_gnu_gtk_FileChooserHelper.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_action
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1action
  (JNIEnv *env, jclass cls, jobject chooser, jint action)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_set_action(chooser_g, (GtkFileChooserAction)action);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_action
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1action
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (jint)gtk_file_chooser_get_action(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_local_only
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1local_1only
  (JNIEnv *env, jclass cls, jobject chooser, jboolean localOnly)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_set_local_only(chooser_g, (gboolean)localOnly);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_local_only
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1local_1only
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (jboolean)gtk_file_chooser_get_local_only(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_select_multiple
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1select_1multiple
  (JNIEnv *env, jclass cls, jobject chooser, jboolean selectMultiple)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_set_select_multiple(chooser_g, (gboolean)selectMultiple);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_select_multiple
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1select_1multiple
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (jboolean)gtk_file_chooser_get_select_multiple(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_current_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1current_1name
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_file_chooser_set_current_name(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1filename
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	char* result = (char*) gtk_file_chooser_get_filename(chooser_g);
	
	if(result == NULL)
		return NULL;
	
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_filename(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_filename
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1filename
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_set_filename(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_select_filename
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1select_1filename
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_select_filename(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_unselect_filename
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1unselect_1filename
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_file_chooser_unselect_filename(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_select_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1select_1all
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_select_all(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_unselect_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1unselect_1all
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_unselect_all(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_filenames
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1filenames
  (JNIEnv *env, jclass cls, jobject chooser)
{
	jobjectArray array;
	jclass strClass;
	jstring str;
	GSList* list;
	GSList* item;
	gchar* value;
	guint len;
	guint index;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);

	list = gtk_file_chooser_get_filenames(chooser_g);
	if (NULL == list)
		return NULL;
	len = g_slist_length(list);
	strClass = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, len, strClass, NULL);
	item = list;
	for (index = 0; item != NULL; item = item->next, ++index) {
		value = (gchar*)item->data;
		str = (*env)->NewStringUTF(env, value);
		(*env)->SetObjectArrayElement(env, array, index, str);
	}
	return array;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_current_folder
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1current_1folder
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_set_current_folder(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_current_folder
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1current_1folder
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_current_folder(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_uri
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1uri
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_uri(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_uri
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_set_uri(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_select_uri
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1select_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_select_uri(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_unselect_uri
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1unselect_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_file_chooser_unselect_uri(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_uris
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1uris
  (JNIEnv *env, jclass cls, jobject chooser)
{
	jobjectArray array;
	jclass strClass;
	jstring str;
	GSList* list;
	GSList* item;
	gchar* value;
	guint len;
	guint index;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);

	list = gtk_file_chooser_get_uris(chooser_g);
	if (NULL == list)
		return NULL;
	len = g_slist_length(list);
	strClass = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, len, strClass, NULL);
	item = list;
	for (index = 0; item != NULL; item = item->next, ++index) {
		value = (gchar*)item->data;
		str = (*env)->NewStringUTF(env, value);
		(*env)->SetObjectArrayElement(env, array, index, str);
	}
	return array;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_current_folder_uri
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1current_1folder_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring name)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = gtk_file_chooser_set_current_folder_uri(chooser_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_current_folder_uri
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1current_1folder_1uri
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_current_folder_uri(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_preview_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1preview_1widget
  (JNIEnv *env, jclass cls, jobject chooser, jobject widget)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	GtkWidget* widget_g = (GtkWidget*)getPointerFromHandle(env, widget);
	gtk_file_chooser_set_preview_widget(chooser_g, widget_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_preview_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1preview_1widget
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return getGObjectHandle(env, (GObject *) gtk_file_chooser_get_preview_widget(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_preview_widget_active
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1preview_1widget_1active
  (JNIEnv *env, jclass cls, jobject chooser, jboolean active)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_set_preview_widget_active(chooser_g, (gboolean)active);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_preview_widget_active
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1preview_1widget_1active
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (jboolean)gtk_file_chooser_get_preview_widget_active(chooser_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_preview_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1preview_1filename
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_preview_filename(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_preview_uri
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1preview_1uri
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return (*env)->NewStringUTF(env, (char*)gtk_file_chooser_get_preview_uri(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_extra_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1extra_1widget
  (JNIEnv *env, jclass cls, jobject chooser, jobject widget)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	gtk_file_chooser_set_extra_widget(chooser_g, (GtkWidget*)widget);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_extra_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1extra_1widget
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return getGObjectHandle(env, (GObject *) gtk_file_chooser_get_extra_widget(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_add_filter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1add_1filter
  (JNIEnv *env, jclass cls, jobject chooser, jobject filter)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	gtk_file_chooser_add_filter(chooser_g, filter_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_remove_filter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1remove_1filter
  (JNIEnv *env, jclass cls, jobject chooser, jobject filter)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	gtk_file_chooser_remove_filter(chooser_g, filter_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_list_filters
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1list_1filters
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return getGObjectHandlesFromGSListAndRef(env, gtk_file_chooser_list_filters(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_set_filter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1filter
  (JNIEnv *env, jclass cls, jobject chooser, jobject filter)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	gtk_file_chooser_set_filter(chooser_g, filter_g);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_get_filter
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1filter
  (JNIEnv *env, jclass cls, jobject chooser)
{
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	return getGObjectHandle(env, (GObject *) gtk_file_chooser_get_filter(chooser_g));
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_add_shortcut_folder
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1add_1shortcut_1folder
  (JNIEnv *env, jclass cls, jobject chooser, jstring folder)
{
	const gchar* str;
	GError* err;
	gboolean gbool;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	
	str = (gchar*)(*env)->GetStringUTFChars(env, folder, NULL);
	err = NULL;
	gbool = gtk_file_chooser_add_shortcut_folder(chooser_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, folder, str);
	if (gbool == TRUE)	{
		return NULL;
	}
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_remove_shortcut_folder
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1remove_1shortcut_1folder
  (JNIEnv *env, jclass cls, jobject chooser, jstring folder)
{
	const gchar* str;
	GError* err;
	gboolean gbool;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	
	str = (gchar*)(*env)->GetStringUTFChars(env, folder, NULL);
	gbool = gtk_file_chooser_remove_shortcut_folder(chooser_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, folder, str);
	if (gbool == TRUE)	{
		return NULL;
	}
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_list_shortcut_folders
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1list_1shortcut_1folders
  (JNIEnv *env, jclass cls, jobject chooser)
{
	jobjectArray array;
	jclass strClass;
	jstring str;
	GSList* list;
	GSList* item;
	gchar* value;
	guint len;
	guint index;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);

	list = gtk_file_chooser_list_shortcut_folders(chooser_g);
	if (NULL == list)
		return NULL;
	len = g_slist_length(list);
	strClass = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, len, strClass, NULL);
	item = list;
	for (index = 0; item != NULL; item = item->next, ++index) {
		value = (gchar*)item->data;
		str = (*env)->NewStringUTF(env, value);
		(*env)->SetObjectArrayElement(env, array, index, str);
	}
	return array;
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_add_shortcut_folder_uri
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1add_1shortcut_1folder_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring folder)
{
	const gchar* str;
	GError* err;
	gboolean gbool;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	
	str = (gchar*)(*env)->GetStringUTFChars(env, folder, NULL);
	gbool = gtk_file_chooser_add_shortcut_folder_uri(chooser_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, folder, str);
	if (gbool == TRUE)	{
		return NULL;
	}
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_remove_shortcut_folder_uri
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1remove_1shortcut_1folder_1uri
  (JNIEnv *env, jclass cls, jobject chooser, jstring folder)
{
	const gchar* str;
	GError* err;
	gboolean gbool;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);
	
	str = (gchar*)(*env)->GetStringUTFChars(env, folder, NULL);
	gbool = gtk_file_chooser_remove_shortcut_folder_uri(chooser_g, str, &err);
	(*env)->ReleaseStringUTFChars(env, folder, str);
	if (gbool == TRUE)	{
		return NULL;
	}
	return getStructHandle(env, err, NULL, (JGFreeFunc) g_error_free);
}

/*
 * Class:     org_gnu_gtk_FileChooserHelper
 * Method:    gtk_file_chooser_list_shortcut_folder_uris
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1list_1shortcut_1folder_1uris
  (JNIEnv *env, jclass cls, jobject chooser)
{
	jobjectArray array;
	jclass strClass;
	jstring str;
	GSList* list;
	GSList* item;
	gchar* value;
	guint len;
	guint index;
	GtkFileChooser* chooser_g = (GtkFileChooser*)getPointerFromHandle(env, chooser);

	list = gtk_file_chooser_list_shortcut_folder_uris(chooser_g);
	if (NULL == list)
		return NULL;
	len = g_slist_length(list);
	strClass = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, len, strClass, NULL);
	item = list;
	for (index = 0; item != NULL; item = item->next, ++index) {
		value = (gchar*)item->data;
		str = (*env)->NewStringUTF(env, value);
		(*env)->SetObjectArrayElement(env, array, index, str);
	}
	return array;
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1set_1show_1hidden
(JNIEnv *env, jclass cls, jobject chooser, jboolean show_hidden)
{
    GtkFileChooser * chooser_g = (GtkFileChooser *)getPointerFromHandle(env, chooser);
    gboolean show_hidden_g = (gboolean)show_hidden;
    gtk_file_chooser_set_show_hidden(chooser_g, show_hidden_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1show_1hidden
(JNIEnv *env, jclass cls, jobject chooser)
{
    GtkFileChooser * chooser_g = (GtkFileChooser *)getPointerFromHandle(env, chooser);
    return (jboolean)gtk_file_chooser_get_show_hidden(chooser_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserHelper_gtk_1file_1chooser_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_file_chooser_get_type();
}

#ifdef __cplusplus
}
#endif
#endif
