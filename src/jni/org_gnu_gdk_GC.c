/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_GC.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GdkGC_get_clip_x_origin (GdkGC * cptr) 
{
    return cptr->clip_x_origin;
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    getClipXOrigin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_GC_getClipXOrigin (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkGC *obj_g = (GdkGC *)getPointerFromHandle(env, obj);
    return (jint) (GdkGC_get_clip_x_origin (obj_g));
}

static gint32 GdkGC_get_clip_y_origin (GdkGC * cptr) 
{
    return cptr->clip_y_origin;
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    getClipYOrigin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_GC_getClipYOrigin (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkGC *obj_g = (GdkGC *)getPointerFromHandle(env, obj);
    return (jint) (GdkGC_get_clip_y_origin (obj_g));
}

static gint32 GdkGC_get_ts_x_origin (GdkGC * cptr) 
{
    return cptr->ts_x_origin;
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    getTsXOrigin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_GC_getTsXOrigin (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkGC *obj_g = (GdkGC *)getPointerFromHandle(env, obj);
    return (jint) (GdkGC_get_ts_x_origin (obj_g));
}

static gint32 GdkGC_get_ts_y_origin (GdkGC * cptr) 
{
    return cptr->ts_y_origin;
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    getTsYOrigin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_GC_getTsYOrigin (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkGC *obj_g = (GdkGC *)getPointerFromHandle(env, obj);
    return (jint) (GdkGC_get_ts_y_origin (obj_g));
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_gc_get_type ();
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1new (JNIEnv *env, jclass cls, jobject drawable)
{
    GdkDrawable *drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    return getGObjectHandle(env, G_OBJECT(gdk_gc_new (drawable_g)));
}

/*
 * Class:     org_gnu_gdk_GC
 * Method:    gdk_gc_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1get_1screen
  (JNIEnv *env, jclass cls, jobject gc)
{
   GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
   return getGObjectHandle(env, G_OBJECT(gdk_gc_get_screen(gc_g)));
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_foreground
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1foreground (JNIEnv *env, jclass cls, 
    jobject gc, jobject color) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gdk_gc_set_foreground (gc_g, color_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_background
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1background (JNIEnv *env, jclass cls, 
    jobject gc, jobject color) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gdk_gc_set_background (gc_g, color_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_function
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1function (JNIEnv *env, jclass cls, 
    jobject gc, jint function) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkFunction function_g = (GdkFunction) function;
    gdk_gc_set_function (gc_g, function_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_fill
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1fill (JNIEnv *env, jclass cls, jobject 
    gc, jint fill) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkFill fill_g = (GdkFill) fill;
    gdk_gc_set_fill (gc_g, fill_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_tile
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1tile (JNIEnv *env, jclass cls, jobject 
    gc, jobject tile) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkPixmap *tile_g = (GdkPixmap *)getPointerFromHandle(env, tile);
    gdk_gc_set_tile (gc_g, tile_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_stipple
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1stipple (JNIEnv *env, jclass cls, jobject 
    gc, jobject stipple) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkPixmap *stipple_g = (GdkPixmap *)getPointerFromHandle(env, stipple);
    gdk_gc_set_stipple (gc_g, stipple_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_ts_origin
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1ts_1origin (JNIEnv *env, jclass cls, 
    jobject gc, jint x, jint y) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gdk_gc_set_ts_origin (gc_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_clip_origin
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1clip_1origin (JNIEnv *env, jclass cls, 
    jobject gc, jint x, jint y) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gdk_gc_set_clip_origin (gc_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_clip_mask
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1clip_1mask (JNIEnv *env, jclass cls, 
    jobject gc, jobject mask) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    gdk_gc_set_clip_mask (gc_g, mask_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_clip_rectangle
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1clip_1rectangle (JNIEnv *env, jclass 
    cls, jobject gc, jobject rectangle) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkRectangle *rectangle_g = (GdkRectangle *)getPointerFromHandle(env, rectangle);
    gdk_gc_set_clip_rectangle (gc_g, rectangle_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_clip_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1clip_1region (JNIEnv *env, jclass cls, 
    jobject gc, jobject region) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gdk_gc_set_clip_region (gc_g, region_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_subwindow
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1subwindow (JNIEnv *env, jclass cls, 
    jobject gc, jint mode) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkSubwindowMode mode_g = (GdkSubwindowMode) mode;
    gdk_gc_set_subwindow (gc_g, mode_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_exposures
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1exposures (JNIEnv *env, jclass cls, 
    jobject gc, jboolean exposures) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gboolean exposures_g = (gboolean) exposures;
    gdk_gc_set_exposures (gc_g, exposures_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_line_attributes
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1line_1attributes (JNIEnv *env, jclass 
    cls, jobject gc, jint lineWidth, jint lineStyle, jint capStyle, jint joinStyle) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gint32 lineWidth_g = (gint32) lineWidth;
    GdkLineStyle lineStyle_g = (GdkLineStyle) lineStyle;
    GdkCapStyle capStyle_g = (GdkCapStyle) capStyle;
    GdkJoinStyle joinStyle_g = (GdkJoinStyle) joinStyle;
    gdk_gc_set_line_attributes (gc_g, lineWidth_g, lineStyle_g, capStyle_g, joinStyle_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_dashes
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1dashes (JNIEnv *env, jclass cls, jobject 
    gc, jint dashOffset, jintArray dashList, jint n) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gint32 dashOffset_g = (gint32) dashOffset;
    gint8 *dashList_g = (gint8 *) (*env)->GetIntArrayElements (env, dashList, NULL);
    gint32 n_g = (gint32) n;
    gdk_gc_set_dashes (gc_g, dashOffset_g, dashList_g, n_g);
    (*env)->ReleaseIntArrayElements (env, dashList, (jint *) dashList_g, 0);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_offset
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1offset (JNIEnv *env, jclass cls, jobject gc, 
    jint xOffset, jint yOffset) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    gint32 xOffset_g = (gint32) xOffset;
    gint32 yOffset_g = (gint32) yOffset;
    gdk_gc_offset (gc_g, xOffset_g, yOffset_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_copy
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1copy (JNIEnv *env, jclass cls, jobject dstGC, 
    jobject srcGC) 
{
    GdkGC *dstGC_g = (GdkGC *)getPointerFromHandle(env, dstGC);
    GdkGC *srcGC_g = (GdkGC *)getPointerFromHandle(env, srcGC);
    gdk_gc_copy (dstGC_g, srcGC_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_colormap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1colormap (JNIEnv *env, jclass cls, 
    jobject gc, jobject colormap) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkColormap *colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    gdk_gc_set_colormap (gc_g, colormap_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_get_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1get_1colormap (JNIEnv *env, jclass cls, 
    jobject gc) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    return getGObjectHandle(env, G_OBJECT(gdk_gc_get_colormap (gc_g)));
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_rgb_fg_color
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1rgb_1fg_1color (JNIEnv *env, jclass 
    cls, jobject gc, jobject color) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gdk_gc_set_rgb_fg_color (gc_g, color_g);
}

/*
 * Class:     org.gnu.gdk.GC
 * Method:    gdk_gc_set_rgb_bg_color
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GC_gdk_1gc_1set_1rgb_1bg_1color (JNIEnv *env, jclass 
    cls, jobject gc, jobject color) 
{
    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gdk_gc_set_rgb_bg_color (gc_g, color_g);
}


#ifdef __cplusplus
}

#endif
