/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellRendererToggle.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererToggle_setActivatable (JNIEnv *env, jclass 
    cls, jobject toggle, jboolean setting) 
{
	GtkCellRendererToggle *toggle_g = 
			(GtkCellRendererToggle *)getPointerFromHandle(env, toggle);
	toggle_g->activatable = setting;
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_cell_renderer_toggle_get_type ();
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1new (
    JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_cell_renderer_toggle_new ());
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_get_radio
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1get_1radio (JNIEnv *env, 
    jclass cls, jobject toggle) 
{
	GtkCellRendererToggle *toggle_g = 
			(GtkCellRendererToggle *)getPointerFromHandle(env, toggle);
    return (jboolean) (gtk_cell_renderer_toggle_get_radio (toggle_g));
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_set_radio
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1set_1radio (JNIEnv *env, 
    jclass cls, jobject toggle, jboolean radio) 
{
	GtkCellRendererToggle *toggle_g = 
			(GtkCellRendererToggle *)getPointerFromHandle(env, toggle);
    gboolean radio_g = (gboolean) radio;
    gtk_cell_renderer_toggle_set_radio (toggle_g, radio_g);
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_get_active
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1get_1active (JNIEnv *env, 
    jclass cls, jobject toggle) 
{
	GtkCellRendererToggle *toggle_g = 
			(GtkCellRendererToggle *)getPointerFromHandle(env, toggle);
    return (jboolean) (gtk_cell_renderer_toggle_get_active (toggle_g));
}

/*
 * Class:     org.gnu.gtk.CellRendererToggle
 * Method:    gtk_cell_renderer_toggle_set_active
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererToggle_gtk_1cell_1renderer_1toggle_1set_1active (JNIEnv *env, 
    jclass cls, jobject toggle, jboolean setting) 
{
	GtkCellRendererToggle *toggle_g = 
			(GtkCellRendererToggle *)getPointerFromHandle(env, toggle);
    gboolean setting_g = (gboolean) setting;
    gtk_cell_renderer_toggle_set_active (toggle_g, setting_g);
}


#ifdef __cplusplus
}

#endif
