/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_PangoAttrEmbossed.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.PangoAttrEmbossed
 * Method:    getEmbossed
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PangoAttrEmbossed_getEmbossed (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkPangoAttrEmbossed *obj_g = (GdkPangoAttrEmbossed *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->embossed;
}

/*
 * Class:     org.gnu.gdk.PangoAttrEmbossed
 * Method:    gdk_pango_attr_embossed_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PangoAttrEmbossed_gdk_1pango_1attr_1embossed_1new (
    JNIEnv *env, jclass cls, jboolean embossed) 
{
    gboolean embossed_g = (gboolean) embossed;
    return getStructHandle(env, gdk_pango_attr_embossed_new (embossed_g),
                           NULL, (JGFreeFunc) pango_attribute_destroy);
}

#ifdef __cplusplus
}

#endif
