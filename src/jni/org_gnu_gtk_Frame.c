/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Frame.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_frame_get_type ();
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1new (JNIEnv *env, jclass cls, 
    jstring label) 
{
	const char *label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval = getGObjectHandle(env, (GObject *) gtk_frame_new (label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
	return retval;
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_set_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1set_1label (JNIEnv *env, jclass cls, 
    jobject frame, jstring label) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
	const char *label_g = (*env)->GetStringUTFChars(env, label, NULL);
    gtk_frame_set_label (frame_g, label_g);
	(*env)->ReleaseStringUTFChars(env, label, label_g);
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_get_label
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1get_1label (JNIEnv *env, jclass 
    cls, jobject frame) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
	return (*env)->NewStringUTF(env, (gchar*)gtk_frame_get_label (frame_g) );
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_set_label_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1set_1label_1widget (JNIEnv *env, 
    jclass cls, jobject frame, jobject labelWidget) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    GtkWidget *labelWidget_g = (GtkWidget *)getPointerFromHandle(env, labelWidget);
    gtk_frame_set_label_widget (frame_g, labelWidget_g);
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_get_label_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1get_1label_1widget (JNIEnv *env, 
    jclass cls, jobject frame) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    return getGObjectHandle(env, (GObject *) gtk_frame_get_label_widget (frame_g));
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_set_label_align
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1set_1label_1align (JNIEnv *env, 
    jclass cls, jobject frame, jdouble xalign, jdouble yalign) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gtk_frame_set_label_align (frame_g, xalign_g, yalign_g);
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_get_label_align
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1get_1label_1align (JNIEnv *env, 
        jclass cls, jobject frame, jdoubleArray xalign, jdoubleArray yalign) 
{
	GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    gfloat *xalign_g = (gfloat *) (*env)->GetDoubleArrayElements (env, xalign, NULL);
    gfloat *yalign_g = (gfloat *) (*env)->GetDoubleArrayElements (env, yalign, NULL);
    gtk_frame_get_label_align (frame_g, xalign_g, yalign_g);
	(*env)->ReleaseDoubleArrayElements (env, xalign, (jdouble *) xalign_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, yalign, (jdouble *) yalign_g, 0);
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_set_shadow_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1set_1shadow_1type (JNIEnv *env, 
    jclass cls, jobject frame, jint type) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    GtkShadowType type_g = (GtkShadowType) type;
    gtk_frame_set_shadow_type (frame_g, type_g);
}

/*
 * Class:     org.gnu.gtk.Frame
 * Method:    gtk_frame_get_shadow_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Frame_gtk_1frame_1get_1shadow_1type (JNIEnv *env, 
    jclass cls, jobject frame) 
{
    GtkFrame *frame_g = (GtkFrame *)getPointerFromHandle(env, frame);
    return (jint) (gtk_frame_get_shadow_type (frame_g));
}

#ifdef __cplusplus
}

#endif
