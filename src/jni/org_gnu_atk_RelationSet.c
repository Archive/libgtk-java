/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#include "org_gnu_atk_RelationSet.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)atk_relation_set_get_type ();
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, G_OBJECT(atk_relation_set_new ()));
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_contains
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1contains (JNIEnv 
    *env, jclass cls, jobject set, jint relationship) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    AtkRelationType relationship_g = (AtkRelationType) relationship;
    jboolean result_j = (jboolean) (atk_relation_set_contains (set_g, relationship_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1remove (JNIEnv *env, 
    jclass cls, jobject set, jobject relation) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    AtkRelation *relation_g = (AtkRelation *)getPointerFromHandle(env, relation);
    atk_relation_set_remove (set_g, relation_g);
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_add
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1add (JNIEnv *env, 
    jclass cls, jobject set, jobject relation) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    AtkRelation *relation_g = (AtkRelation *)getPointerFromHandle(env, relation);
    atk_relation_set_add (set_g, relation_g);
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_get_n_relations
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1get_1n_1relations (
    JNIEnv *env, jclass cls, jobject set) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    return (jint) (atk_relation_set_get_n_relations (set_g));
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_get_relation
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1get_1relation (JNIEnv 
    *env, jclass cls, jobject set, jint index) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    gint32 index_g = (gint32) index;
    return getGObjectHandle(env, G_OBJECT(atk_relation_set_get_relation (set_g, index_g)));
}

/*
 * Class:     org.gnu.atk.RelationSet
 * Method:    atk_relation_set_get_relation_by_type
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1get_1relation_1by_1type (
    JNIEnv *env, jclass cls, jobject set, jint relationship) 
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    AtkRelationType relationship_g = (AtkRelationType) relationship;
    return getGObjectHandle(env, G_OBJECT(atk_relation_set_get_relation_by_type (set_g, relationship_g)));
}


/*
 * Class:     org_gnu_atk_RelationSet
 * Method:    atk_relation_set_add_relation_by_type
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_RelationSet_atk_1relation_1set_1add_1relation_1by_1type
  (JNIEnv *env, jclass cls, jobject set, jint relationship, jobject target)
{
    AtkRelationSet *set_g = (AtkRelationSet *)getPointerFromHandle(env, set);
    AtkRelationType relationship_g = (AtkRelationType) relationship;
    AtkObject *target_g = (AtkObject *)getPointerFromHandle(env, target);
    atk_relation_set_add_relation_by_type(set_g, relationship_g, target_g);
}

#ifdef __cplusplus
}

#endif
