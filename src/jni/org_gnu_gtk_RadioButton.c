/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_RadioButton.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.RadioButton
 * Method:    gtk_radio_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1get_1type (JNIEnv *env, 
    jclass cls) 
{
	return (jint)gtk_radio_button_get_type ();
}

                                                                                                  
/*
 * Class:     org_gnu_gtk_RadioButton
 * Method:    gtk_radio_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new
  (JNIEnv *env, jclass cls, jobjectArray group)
{
	GSList* list = getGSListFromHandles(env, group);
	return getGObjectHandle(env, (GObject *) gtk_radio_button_new(list));
}
                                                                                                  
/*
 * Class:     org.gnu.gtk.RadioButton
 * Method:    gtk_radio_button_new_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new_1from_1widget
    (JNIEnv *env, jclass cls, jobject group) 
{
	GtkRadioButton* group_g = (GtkRadioButton*)getPointerFromHandle(env, group);
    return getGObjectHandle(env, (GObject *) gtk_radio_button_new_from_widget(group_g));
}

/*
 * Class:     org_gnu_gtk_RadioButton
 * Method:    gtk_radio_button_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new_1with_1label
    (JNIEnv *env, jclass cls, jobjectArray group, jstring label) 
{
	GSList* list = getGSListFromHandles(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject value = getGObjectHandle(env, (GObject *)
			gtk_radio_button_new_with_label(list, label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
    return value;
}
                                                                                                  
/*
 * Class:     org.gnu.gtk.RadioButton
 * Method:    gtk_radio_button_new_with_label_from_widget
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new_1with_1label_1from_1widget
	(JNIEnv *env, jclass cls, jobject group, jstring label) 
{
	GtkRadioButton* group_g = (GtkRadioButton*)getPointerFromHandle(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval =  getGObjectHandle(env, (GObject *)
			gtk_radio_button_new_with_label_from_widget(group_g, label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g );
	return retval;
}

/*
 * Class:     org_gnu_gtk_RadioButton
 * Method:    gtk_radio_button_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new_1with_1mnemonic
    (JNIEnv *env, jclass cls, jobjectArray group, jstring label) 
{
	GSList* list = getGSListFromHandles(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject value = getGObjectHandle(env, (GObject *) 
			gtk_radio_button_new_with_mnemonic(list, label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
    return value;
}
                                                                                                  
/*
 * Class:     org.gnu.gtk.RadioButton
 * Method:    gtk_radio_button_new_with_mnemonic_from_widget
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1new_1with_1mnemonic_1from_1widget (JNIEnv 
    *env, jclass cls, jobject group, jstring label) 
{
	GtkRadioButton* group_g = (GtkRadioButton*)getPointerFromHandle(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval =  getGObjectHandle(env, (GObject *)
			gtk_radio_button_new_with_mnemonic_from_widget(group_g, label_g));
	(*env)->ReleaseStringUTFChars( env, label, label_g );
	return retval;
}

/*
 * Class:     org_gnu_gtk_RadioButton
 * Method:    gtk_radio_button_get_group
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1get_1group
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkRadioButton* button_g = (GtkRadioButton*)getPointerFromHandle(env, button);
    return getGObjectHandlesFromGSList(env, gtk_radio_button_get_group(button_g));
}
                                                                                
/*
 * Class:     org_gnu_gtk_RadioButton
 * Method:    gtk_radio_button_set_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioButton_gtk_1radio_1button_1set_1group
  (JNIEnv *env, jclass cls, jobject button, jobjectArray group)
{
	GtkRadioButton* button_g = (GtkRadioButton*)getPointerFromHandle(env, button);
	GSList* list = getGSListFromHandles(env, group);
	gtk_radio_button_set_group(button_g, list);
}
                                                                                

#ifdef __cplusplus
}

#endif
