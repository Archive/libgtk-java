/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#ifndef _GTK_JAVA_H_
#define _GTK_JAVA_H_
#include <jni.h>
#include <glib.h>
#include <glib-object.h>
#include <jg_jnu.h>

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * Struct to be used as the data parameter for callback functions. 
 * See TreeView, TreeModelFilter and Gtk for examples.
 * If the callback is a static method, use the "cls" parameter, otherwise
 * use the "obj" parameter.
 */
typedef struct
{
    JNIEnv *env;
    jobject obj;
    jclass cls;
    jmethodID methodID;
} JGFuncCallbackRef;

typedef struct
{
	JNIEnv *env;
	jobject object;
} JGRef;

/*
 * Typedefs and structs used in memory management.
 * @since 2.8
 */

typedef void (*JGFreeFunc) (gpointer mem);
typedef void (*JGCopyFunc) (gpointer mem);
typedef struct{
    gpointer thestruct;
    GType type;
    JGFreeFunc free;
    jobject handle;
} JGStruct;

extern char * javaobject_from_gtktype(GType argtype);

extern char * javatype_from_gtktype(GType argtype);

extern void* getPointerFromJavaGObject(JNIEnv* env, jobject gobject);

extern jobjectArray getGObjectHandlesFromGList(JNIEnv* env, GList* list);

extern jobjectArray getGObjectHandlesFromGSList(JNIEnv* env, GSList* list);

extern jobjectArray getGObjectHandlesFromPointers(JNIEnv* env, void** pointer, int numPtrs);

extern jobjectArray getGObjectHandlesFromGListAndRef(JNIEnv* env, GList* list);

extern jobjectArray getGObjectHandlesFromGSListAndRef(JNIEnv* env, GSList* list);

extern jobjectArray getGObjectHandlesFromPointersAndRef(JNIEnv* env,
		void** pointer, int numPtrs, GetHandleFunc hndlFunc);

extern jobjectArray getGBoxedHandlesFromPointers(JNIEnv* env, void** pointer,
		int numPtrs, GetHandleFunc hndlFunc);

extern jobjectArray getGBoxedHandlesFromGList(JNIEnv* env, GList* list,
		GetHandleFunc hndlFunc);
		
extern jobjectArray getGBoxedHandlesFromGSList(JNIEnv* env, GSList* list,
		GetHandleFunc hndlFunc);

extern jobjectArray getStructHandlesFromGList(JNIEnv* env, GList* list,
		GetHandleFunc hndlFunc);
		
extern jobjectArray getStructHandlesFromGSList(JNIEnv* env, GSList* list,
		GetHandleFunc hndlFunc);
		
extern jobjectArray getStructHandlesFromPointers(JNIEnv* env, void** pointer,
		int numPtrs, GetHandleFunc hndlFunc);
		
/*
 * Memory managment functions.
 * @since v2.8
 */

extern void initMemoryManagement();
extern void toggleNotify(gpointer data, GObject *object, gboolean is_last_ref);

extern jobject getGObjectHandle(JNIEnv* env, GObject* object);
extern jobject getGObjectHandleAndRef(JNIEnv* env, GObject* object);
extern jobject getPersistentGObjectHandle(JNIEnv* env, GObject* object);
extern jobject getGBoxedHandle(JNIEnv* env, gpointer box, GType type, 
                               GBoxedCopyFunc copy, GBoxedFreeFunc free);
extern jobject getStructHandle(JNIEnv* env, gpointer thestruct, 
                               JGCopyFunc copy, JGFreeFunc free);
                               
extern void updateStructHandle(JNIEnv* env, jobject handle, gpointer thestruct,
							   JGFreeFunc free);

extern void nativeFinalizeGObject( JNIEnv* env, jobject handle );
extern void nativeFinalizeGBoxed( JNIEnv* env, jobject handle );
extern void nativeFinalizeStruct( JNIEnv* env, jobject handle );

extern guint processPendingGObject();
extern guint processPendingGBoxed();
extern guint processPendingStruct();

extern GType* getGTypesFromJArray(JNIEnv* env, jint size, jintArray typesArray,
		jint** returnPointerArray);

/* Obsoleted code.  Will be removed shortly. */
/*
typedef struct
{
	gpointer boxed;
	GBoxedFreeFunc free;
} JGBoxed;

typedef void (*JGFreeFunc) (gpointer mem);

typedef struct
{
    gpointer thestruct;
    JGFreeFunc free;
} JGStruct;

typedef enum
{
	JG_GOBJECT,
	JG_GBOXED,
	JG_STRUCT
} ObjectType;
*/
//extern void nativeFinalize( JNIEnv* env, ObjectType type, jobject handle );

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* !_GTK_JAVA_H_ */
