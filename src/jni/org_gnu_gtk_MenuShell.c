/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_MenuShell.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_menu_shell_get_type ();
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_append
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1append (JNIEnv *env, jclass 
    cls, jobject menu_shell, jobject child) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    GtkMenuItem *child_g = (GtkMenuItem *)getPointerFromHandle(env, child);
    gtk_menu_shell_append (menu_shell_g, (GtkWidget*)child_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_prepend
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1prepend (JNIEnv *env, 
    jclass cls, jobject menu_shell, jobject child) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    GtkMenuItem *child_g = (GtkMenuItem *)getPointerFromHandle(env, child);
    gtk_menu_shell_prepend (menu_shell_g, (GtkWidget*)child_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_insert
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1insert (JNIEnv *env, jclass 
    cls, jobject menu_shell, jobject child, jint position) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    GtkMenuItem *child_g = (GtkMenuItem *)getPointerFromHandle(env, child);
    gint32 position_g = (gint32) position;
    gtk_menu_shell_insert (menu_shell_g, (GtkWidget*)child_g, position_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_deactivate
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1deactivate (JNIEnv *env, 
    jclass cls, jobject menu_shell) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    gtk_menu_shell_deactivate (menu_shell_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_select_item
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1select_1item (JNIEnv *env, 
    jclass cls, jobject menu_shell, jobject menuItem) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    GtkMenuItem *menuItem_g = (GtkMenuItem *)getPointerFromHandle(env, menuItem);
    gtk_menu_shell_select_item (menu_shell_g, (GtkWidget*)menuItem_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_deselect
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1deselect (JNIEnv *env, 
    jclass cls, jobject menu_shell) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    gtk_menu_shell_deselect (menu_shell_g);
}

/*
 * Class:     org.gnu.gtk.MenuShell
 * Method:    gtk_menu_shell_activate_item
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1activate_1item (JNIEnv 
    *env, jclass cls, jobject menu_shell, jobject menuItem, jboolean forceDeactivate) 
{
    GtkMenuShell *menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    GtkMenuItem *menuItem_g = (GtkMenuItem *)getPointerFromHandle(env, menuItem);
    gboolean forceDeactivate_g = (gboolean) forceDeactivate;
    gtk_menu_shell_activate_item (menu_shell_g, 
				  (GtkWidget*)menuItem_g, 
				  forceDeactivate_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1select_1first
(JNIEnv *env, jclass cls, jobject menu_shell, jboolean search_sensitive)
{
    GtkMenuShell * menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    gboolean search_sensitive_g = (gboolean)search_sensitive;
    gtk_menu_shell_select_first(menu_shell_g, search_sensitive_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1cancel
(JNIEnv *env, jclass cls, jobject menu_shell)
{
    GtkMenuShell * menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    gtk_menu_shell_cancel(menu_shell_g);
}

/*
 * Class:     org_gnu_gtk_MenuShell
 * Method:    gtk_menu_shell_get_take_focus
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1get_1take_1focus
  (JNIEnv *env, jclass cls, jobject menu_shell)
{
    GtkMenuShell * menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    return (jboolean)gtk_menu_shell_get_take_focus(menu_shell_g);
}

/*
 * Class:     org_gnu_gtk_MenuShell
 * Method:    gtk_menu_shell_set_take_focus
 * Signature: (Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuShell_gtk_1menu_1shell_1set_1take_1focus
  (JNIEnv *env, jclass cls, jobject menu_shell, jboolean take_focus)
{
    GtkMenuShell * menu_shell_g = (GtkMenuShell *)getPointerFromHandle(env, menu_shell);
    gtk_menu_shell_set_take_focus(menu_shell_g, (gboolean)take_focus);
}




#ifdef __cplusplus
}

#endif
