/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Curve.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_curve_get_type ();
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_curve_new());
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_reset
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1reset (JNIEnv *env, jclass cls, jobject 
    curve) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    gtk_curve_reset (curve_g);
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_set_gamma
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1set_1gamma (JNIEnv *env, jclass cls, 
    jobject curve, jdouble gamma) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    gdouble gamma_g = (gdouble) gamma;
    gtk_curve_set_gamma (curve_g, gamma_g);
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_set_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1set_1range (JNIEnv *env, jclass cls, 
    jobject curve, jdouble minX, jdouble maxX, jdouble minY, jdouble maxY) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    gdouble minX_g = (gdouble) minX;
    gdouble maxX_g = (gdouble) maxX;
    gdouble minY_g = (gdouble) minY;
    gdouble maxY_g = (gdouble) maxY;
    gtk_curve_set_range (curve_g, minX_g, maxX_g, minY_g, maxY_g);
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_get_vector
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1get_1vector (JNIEnv *env, jclass cls, 
    jobject curve, jint veclen, jdoubleArray vector) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    gint32 veclen_g = (gint32) veclen;
    gdouble *vector_g = (gdouble *) (*env)->GetDoubleArrayElements (env, vector, NULL);
    gtk_curve_get_vector (curve_g, veclen_g, (gfloat*)vector_g);
    (*env)->ReleaseDoubleArrayElements (env, vector, (jdouble *) vector_g, 0);
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_set_vector
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1set_1vector (JNIEnv *env, jclass cls, 
    jobject curve, jint veclen, jdoubleArray vector) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    gint32 veclen_g = (gint32) veclen;
    gdouble *vector_g = (gdouble *) (*env)->GetDoubleArrayElements (env, vector, NULL);
    gtk_curve_set_vector (curve_g, veclen_g, (gfloat*)vector_g);
    (*env)->ReleaseDoubleArrayElements (env, vector, (jdouble *) vector_g, 0);
}

/*
 * Class:     org.gnu.gtk.Curve
 * Method:    gtk_curve_set_curve_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Curve_gtk_1curve_1set_1curve_1type (JNIEnv *env, jclass 
    cls, jobject curve, jint type) 
{
    GtkCurve *curve_g = (GtkCurve *)getPointerFromHandle(env, curve);
    GtkCurveType type_g = (GtkCurveType) type;
    gtk_curve_set_curve_type (curve_g, type_g);
}


#ifdef __cplusplus
}

#endif
