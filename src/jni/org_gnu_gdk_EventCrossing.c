/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventCrossing.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventCrossing_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventCrossing_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getSubwindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventCrossing_getSubwindow (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventCrossing_getTime (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getX
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventCrossing_getX (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->x;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getY
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventCrossing_getY (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->y;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getXRoot
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventCrossing_getXRoot (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->x_root;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getYRoot
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_EventCrossing_getYRoot (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jdouble) obj_g->y_root;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getMode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventCrossing_getMode (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jint) obj_g->mode;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getDetail
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventCrossing_getDetail (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jint) obj_g->detail;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getFocus
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventCrossing_getFocus (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->focus;
}

/*
 * Class:     org.gnu.gdk.EventCrossing
 * Method:    getState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventCrossing_getState (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventCrossing *obj_g = (GdkEventCrossing *)getPointerFromHandle(env, obj);
    return (jint) obj_g->state;
}


#ifdef __cplusplus
}

#endif
