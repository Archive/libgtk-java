/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ImageMenuItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_image_menu_item_get_type ();
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_image_menu_item_new ());
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1new_1with_1label (
    JNIEnv *env, jclass cls, jstring label) 
{
    gchar* label_g = (gchar*)(*env)->GetStringUTFChars(env, label, 0);
    jobject result = getGObjectHandle(env, (GObject *) gtk_image_menu_item_new_with_label (label_g));
    (*env)->ReleaseStringUTFChars(env, label, label_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1new_1with_1mnemonic (JNIEnv *env, jclass 
    cls, jstring label) 
{
    gchar* label_g = (gchar*)(*env)->GetStringUTFChars(env, label, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_image_menu_item_new_with_mnemonic (label_g));
    (*env)->ReleaseStringUTFChars(env, label, label_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_new_from_stock
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1new_1from_1stock (
    JNIEnv *env, jclass cls, jstring stockId, jobject accelGroup) 
{
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_image_menu_item_new_from_stock (stockId_g, accelGroup_g));
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_set_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1set_1image (
    JNIEnv *env, jclass cls, jobject image_menu_item, jobject image) 
{
    GtkImageMenuItem *image_menu_item_g = (GtkImageMenuItem *)getPointerFromHandle(env, image_menu_item);
    GtkWidget *image_g = (GtkWidget *)getPointerFromHandle(env, image);
    gtk_image_menu_item_set_image (image_menu_item_g, image_g);
}

/*
 * Class:     org.gnu.gtk.ImageMenuItem
 * Method:    gtk_image_menu_item_get_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ImageMenuItem_gtk_1image_1menu_1item_1get_1image (
    JNIEnv *env, jclass cls, jobject image_menu_item) 
{
    GtkImageMenuItem *image_menu_item_g = (GtkImageMenuItem *)
    		getPointerFromHandle(env, image_menu_item);
    return getGObjectHandle(env, (GObject *) gtk_image_menu_item_get_image (image_menu_item_g));
}


#ifdef __cplusplus
}

#endif
