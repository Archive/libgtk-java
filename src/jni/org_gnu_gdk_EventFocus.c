/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventFocus.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventFocus
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventFocus_getWindow (JNIEnv *env, jclass cls, jobject obj)
{
    GdkEventFocus *obj_g = (GdkEventFocus *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g);
}

/*
 * Class:     org.gnu.gdk.EventFocus
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventFocus_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventFocus *obj_g = (GdkEventFocus *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventFocus
 * Method:    getIn
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventFocus_getIn (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventFocus *obj_g = (GdkEventFocus *)getPointerFromHandle(env, obj);
    return (jint) obj_g->in;
}

#ifdef __cplusplus
}

#endif
