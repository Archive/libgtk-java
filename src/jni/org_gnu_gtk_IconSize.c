/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IconSize.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1lookup
(JNIEnv *env, jclass cls, jint size, jintArray width, jintArray height)
{
    GtkIconSize size_g = (GtkIconSize)size;
    gint * width_g = (gint *)(*env)->GetIntArrayElements(env, width, NULL);
    gint * height_g = (gint *)(*env)->GetIntArrayElements(env, height, NULL);
    jboolean ret_g = (jboolean)gtk_icon_size_lookup(size_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements(env, width, (jint *)width_g, 0);
    (*env)->ReleaseIntArrayElements(env, height, (jint *)height_g, 0);
    return ret_g;
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1lookup_1for_1settings
(JNIEnv *env, jclass cls, jobject settings, jint size, jintArray width, jintArray height)
{
    GtkSettings * settings_g = (GtkSettings *)getPointerFromHandle(env, settings);
    GtkIconSize size_g = (GtkIconSize)size;
    gint * width_g = (gint *)(*env)->GetIntArrayElements(env, width, NULL);
    gint * height_g = (gint *)(*env)->GetIntArrayElements(env, height, NULL);
    jboolean ret_g = (jboolean)gtk_icon_size_lookup_for_settings(settings_g, size_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements(env, width, (jint *)width_g, 0);
    (*env)->ReleaseIntArrayElements(env, height, (jint *)height_g, 0);
    return ret_g;
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1register
(JNIEnv *env, jclass cls, jstring name, jint width, jint height)
{
    const gchar * name_g = (const gchar *)(*env)->GetStringUTFChars(env, name, 0);
    gint width_g = (gint)width;
    gint height_g = (gint)height;
    jint ret_g = (jint)gtk_icon_size_register(name_g, width_g, height_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
    return ret_g;
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1register_1alias
(JNIEnv *env, jclass cls, jstring alias, jint target)
{
    const gchar * alias_g = (const gchar *)(*env)->GetStringUTFChars(env, alias, 0);
    GtkIconSize target_g = (GtkIconSize)target;
    gtk_icon_size_register_alias(alias_g, target_g);
    (*env)->ReleaseStringUTFChars(env, alias, alias_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1from_1name
(JNIEnv *env, jclass cls, jstring name)
{
    const gchar * name_g = (const gchar *)(*env)->GetStringUTFChars(env, name, 0);
    jint ret_g = (jint)gtk_icon_size_from_name(name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
    return ret_g;
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconSize_gtk_1icon_1size_1get_1name
(JNIEnv *env, jclass cls, jint size)
{
    GtkIconSize size_g = (GtkIconSize)size;
    return (*env)->NewStringUTF(env, gtk_icon_size_get_name(size_g));
}

#ifdef __cplusplus
}
#endif
