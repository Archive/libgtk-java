/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Event.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Event
 * Method:    getType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Event_getType (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEvent *obj_g = (GdkEvent *)getPointerFromHandle(env, obj);
    return (jint) obj_g->type;
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get (JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, gdk_event_get (), GDK_TYPE_EVENT, 
                           NULL, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_peek
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Event_gdk_1event_1peek (JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, gdk_event_peek (), GDK_TYPE_EVENT,
                           NULL, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get_graphics_expose
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get_1graphics_1expose (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getGBoxedHandle(env, gdk_event_get_graphics_expose (window_g),
                           GDK_TYPE_EVENT, (GBoxedCopyFunc) gdk_event_copy, 
                           (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_put
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Event_gdk_1event_1put (JNIEnv *env, jclass cls, jobject 
    event) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gdk_event_put (event_g);
}

/*
 * Class:     org_gnu_gdk_Event
 * Method:    gdk_event_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Event_gdk_1event_1new
  (JNIEnv *env, jclass cls, jint type)
{
	return getGBoxedHandle(env, gdk_event_new((GdkEventType)type),
                               GDK_TYPE_EVENT,
                               NULL, (GBoxedFreeFunc) gdk_event_free);
}


/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Event_gdk_1event_1copy (JNIEnv *env, jclass cls, jobject 
    event) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    return getGBoxedHandle(env, gdk_event_copy (event_g),
                           GDK_TYPE_EVENT, NULL, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get_time
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get_1time (JNIEnv *env, jclass cls, 
    jobject event) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    return (jint) (gdk_event_get_time (event_g));
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get_coords
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get_1coords (JNIEnv *env, jclass 
    cls, jobject event, jdoubleArray xWin, jdoubleArray yWin) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gdouble *xWin_g = (gdouble *) (*env)->GetDoubleArrayElements (env, xWin, NULL);
    gdouble *yWin_g = (gdouble *) (*env)->GetDoubleArrayElements (env, yWin, NULL);
    jboolean result_j = (jboolean) (gdk_event_get_coords (event_g, xWin_g, yWin_g));
    (*env)->ReleaseDoubleArrayElements (env, xWin, (jdouble *) xWin_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, yWin, (jdouble *) yWin_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get_root_coords
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get_1root_1coords (JNIEnv *env, 
    jclass cls, jobject event, jdoubleArray xRoot, jdoubleArray yRoot) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gdouble *xRoot_g = (gdouble *) (*env)->GetDoubleArrayElements (env, xRoot, NULL);
    gdouble *yRoot_g = (gdouble *) (*env)->GetDoubleArrayElements (env, yRoot, NULL);
    jboolean result_j = (jboolean) (gdk_event_get_root_coords (event_g, xRoot_g, yRoot_g));
    (*env)->ReleaseDoubleArrayElements (env, xRoot, (jdouble *) xRoot_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, yRoot, (jdouble *) yRoot_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Event
 * Method:    gdk_event_get_axis
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Event_gdk_1event_1get_1axis (JNIEnv *env, jclass 
    cls, jobject event, jint axisUse, jdoubleArray value) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GdkAxisUse axisUse_g = (GdkAxisUse) axisUse;
    gdouble *value_g = (gdouble *) (*env)->GetDoubleArrayElements (env, value, NULL);
    jboolean result_j = (jboolean) (gdk_event_get_axis (event_g, axisUse_g, value_g));
    (*env)->ReleaseDoubleArrayElements (env, value, (jdouble *) value_g, 0);
    return result_j;
}

/*
 * Class:     org_gnu_gdk_Event
 * Method:    gdk_events_pending
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Event_gdk_1events_1pending
  (JNIEnv *env, jclass cls)
{
	return (jboolean)gdk_events_pending();
}


#ifdef __cplusplus
}

#endif
