/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Rc.h"
#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_add_default_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1add_1default_1file (JNIEnv *env, jclass 
    cls, jstring filename) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gtk_rc_add_default_file (filename_g);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_set_default_files
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1set_1default_1files (JNIEnv *env, jclass 
    cls, jobjectArray filenames) 
{
    gchar **filenames_g = getStringArray(env, filenames);
    gtk_rc_set_default_files (filenames_g);
    freeStringArray(env, filenames, filenames_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_default_files
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1default_1files (JNIEnv *env, jclass 
    cls) 
{
    gchar** result = gtk_rc_get_default_files ();
    int size = sizeof(result) / sizeof(char*);
    jclass strClass = (*env)->FindClass(env, "java/lang/String");
    jobjectArray array = (*env)->NewObjectArray(env, size, strClass, NULL);
    int index;
    if (NULL == array)
    	return NULL;
    for (index = 0; index < size; index++) {
    	jstring str = (*env)->NewStringUTF(env, result[index]);
    	(*env)->SetObjectArrayElement(env, array, index, str);
    }
    return array;
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_style
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1style (JNIEnv *env, jclass cls, jobject 
    widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandle(env, (GObject *) gtk_rc_get_style (widget_g));
} 

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_style_by_paths
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1style_1by_1paths (JNIEnv *env, jclass 
    cls, jobject settings, jstring widget_path, jstring class_path, jint type) 
{
	GtkSettings* settings_g = (GtkSettings*)getPointerFromHandle(env, settings);
    gchar* widget_path_g = (gchar*)(*env)->GetStringUTFChars(env, widget_path, 0);
    gchar* class_path_g = (gchar*)(*env)->GetStringUTFChars(env, class_path, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_rc_get_style_by_paths (settings_g, widget_path_g, class_path_g, (GType)type));
    (*env)->ReleaseStringUTFChars(env, widget_path, widget_path_g);
    (*env)->ReleaseStringUTFChars(env, class_path, class_path_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_reparse_all_for_settings
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1reparse_1all_1for_1settings (JNIEnv 
    *env, jclass cls, jobject settings, jboolean forceLoad) 
{
    GtkSettings *settings_g = (GtkSettings *)getPointerFromHandle(env, settings);
    gboolean forceLoad_g = (gboolean) forceLoad;
    return (jboolean) (gtk_rc_reparse_all_for_settings (settings_g, forceLoad_g));
}

/*
 * Class:     org_gnu_gtk_Rc
 * Method:    gtk_rc_reset_style
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1reset_1styles
  (JNIEnv *env, jclass cls, jobject setting)
{
    GtkSettings *setting_g = (GtkSettings *)getPointerFromHandle(env, setting);
	gtk_rc_reset_styles(setting_g);
}
                                                                                
/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_parse
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1parse (JNIEnv *env, jclass cls, jstring 
    filename) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gtk_rc_parse (filename_g);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_parse_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1parse_1string (JNIEnv *env, jclass cls, 
    jstring rcString) 
{
    gchar* rcString_g = (gchar*)(*env)->GetStringUTFChars(env, rcString, 0);
    gtk_rc_parse_string (rcString_g);
    (*env)->ReleaseStringUTFChars(env, rcString, rcString_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_reparse_all
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1reparse_1all (JNIEnv *env, jclass cls) 
{
    return (jboolean) (gtk_rc_reparse_all ());
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_find_module_in_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1find_1module_1in_1path (JNIEnv *env, 
    jclass cls, jstring moduleFile) 
{
    gchar* moduleFile_g = (gchar*)(*env)->GetStringUTFChars(env, moduleFile, 0);
    gchar *result_g = (gchar*)gtk_rc_find_module_in_path (moduleFile_g);
    (*env)->ReleaseStringUTFChars(env, moduleFile, moduleFile_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_theme_dir
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1theme_1dir (JNIEnv *env, jclass 
    cls) 
{
    gchar *result_g = gtk_rc_get_theme_dir ();
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_module_dir
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1module_1dir (JNIEnv *env, jclass 
    cls) 
{
    gchar *result_g = gtk_rc_get_module_dir ();
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_im_module_path
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1im_1module_1path (JNIEnv *env, 
    jclass cls) 
{
    gchar *result_g = gtk_rc_get_im_module_path ();
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Rc
 * Method:    gtk_rc_get_im_module_file
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Rc_gtk_1rc_1get_1im_1module_1file (JNIEnv *env, 
    jclass cls) 
{
    gchar *result_g = gtk_rc_get_im_module_file ();
    return (*env)->NewStringUTF(env, result_g);
}

#ifdef __cplusplus
}

#endif
