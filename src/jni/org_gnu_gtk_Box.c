/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Box.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Box_gtk_1box_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_box_get_type ();
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_pack_start
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1pack_1start (JNIEnv *env, jclass cls, 
    jobject box, jobject child, jboolean expand, jboolean fill, jint padding) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean expand_g = (gboolean) expand;
    gboolean fill_g = (gboolean) fill;
    gint32 padding_g = (gint32) padding;
    gtk_box_pack_start (box_g, child_g, expand_g, fill_g, padding_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_pack_end
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1pack_1end (JNIEnv *env, jclass cls, jobject 
    box, jobject child, jboolean expand, jboolean fill, jint padding) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean expand_g = (gboolean) expand;
    gboolean fill_g = (gboolean) fill;
    gint32 padding_g = (gint32) padding;
    gtk_box_pack_end (box_g, child_g, expand_g, fill_g, padding_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_pack_start_defaults
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1pack_1start_1defaults (JNIEnv *env, 
    jclass cls, jobject box, jobject child) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_box_pack_start_defaults (box_g, child_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_pack_end_defaults
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1pack_1end_1defaults (JNIEnv *env, jclass 
    cls, jobject box, jobject child) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_box_pack_end_defaults (box_g, child_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_set_homogeneous
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1set_1homogeneous (JNIEnv *env, jclass 
    cls, jobject box, jboolean homogenous) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    gboolean homogenous_g = (gboolean) homogenous;
    gtk_box_set_homogeneous (box_g, homogenous_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_get_homogeneous
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Box_gtk_1box_1get_1homogeneous (JNIEnv *env, jclass 
    cls, jobject box) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    return (jboolean) (gtk_box_get_homogeneous (box_g));
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_set_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1set_1spacing (JNIEnv *env, jclass cls, 
    jobject box, jint spacing) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    gint32 spacing_g = (gint32) spacing;
    gtk_box_set_spacing (box_g, spacing_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_get_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Box_gtk_1box_1get_1spacing (JNIEnv *env, jclass cls, 
    jobject box) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    return (jint) (gtk_box_get_spacing (box_g));
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_reorder_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1reorder_1child (JNIEnv *env, jclass cls, 
    jobject box, jobject child, jint position) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gint32 position_g = (gint32) position;
    gtk_box_reorder_child (box_g, child_g, position_g);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_query_child_packing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1query_1child_1packing (JNIEnv *env, 
    jclass cls, jobject box, jobject child, jbooleanArray expand, jbooleanArray fill, jintArray 
    padding, jintArray packType) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean *expand_g = (gboolean *) (*env)->GetBooleanArrayElements(env, expand, NULL);
    gboolean *fill_g = (gboolean *) (*env)->GetBooleanArrayElements(env, fill, NULL);
    guint *padding_g = (guint *) (*env)->GetIntArrayElements (env, padding, NULL);
    gint *packType_g_g = (gint *) (*env)->GetIntArrayElements (env, packType, NULL);
    GtkPackType *packType_g = (GtkPackType *)packType_g_g;
    gtk_box_query_child_packing (box_g, child_g, expand_g, fill_g, padding_g, packType_g);
    (*env)->ReleaseBooleanArrayElements(env, expand, (jboolean *)expand_g, 0);
    (*env)->ReleaseBooleanArrayElements(env, fill, (jboolean *)fill_g, 0);
    (*env)->ReleaseIntArrayElements (env, padding, (jint *) padding_g, 0);
    (*env)->ReleaseIntArrayElements (env, packType, (jint *)packType_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.Box
 * Method:    gtk_box_set_child_packing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Box_gtk_1box_1set_1child_1packing (JNIEnv *env, jclass 
    cls, jobject box, jobject child, jboolean expand, jboolean fill, jint padding, jint packType) 
{
    GtkBox *box_g = (GtkBox *)getPointerFromHandle(env, box);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean expand_g = (gboolean) expand;
    gboolean fill_g = (gboolean) fill;
    gint32 padding_g = (gint32) padding;
    GtkPackType packType_g = (GtkPackType) packType;
    gtk_box_set_child_packing (box_g, child_g, expand_g, fill_g, padding_g, packType_g);
}


#ifdef __cplusplus
}

#endif
