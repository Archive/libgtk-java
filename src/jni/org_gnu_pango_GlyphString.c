/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_GlyphString.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGBoxedHandle(env, pango_glyph_string_new (),
                           PANGO_TYPE_GLYPH_STRING, NULL,
                           (JGFreeFunc)pango_glyph_string_free);
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_set_size
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1set_1size (JNIEnv 
    *env, jclass cls, jobject string, jint newLen) 
{
    PangoGlyphString *string_g = 
        (PangoGlyphString *)getPointerFromHandle(env, string);
    gint32 newLen_g = (gint32) newLen;
    pango_glyph_string_set_size (string_g, newLen_g);
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)pango_glyph_string_get_type ();
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1copy (JNIEnv *env, 
    jclass cls, jobject string) 
{
    PangoGlyphString *string_g = (PangoGlyphString *)getPointerFromHandle(env, string);
    return getGBoxedHandle(env, pango_glyph_string_copy (string_g),
                           PANGO_TYPE_GLYPH_STRING, NULL,
                           (JGFreeFunc)pango_glyph_string_free);
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_extents
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1extents (JNIEnv 
    *env, jclass cls, jobject glyphs, jobject font, jobject inkRect, jobject logicalRect) 
{
    PangoGlyphString *glyphs_g = (PangoGlyphString *)getPointerFromHandle(env, glyphs);
    PangoFont *font_g = (PangoFont *)getPointerFromHandle(env, font);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
    pango_glyph_string_extents (glyphs_g, font_g, inkRect_g, logicalRect_g);
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_extents_range
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1extents_1range (
    JNIEnv *env, jclass cls, jobject glyphs, jint start, jint end, jobject font, jobject inkRect, jobject 
    logicalRect) 
{
    gint32 start_g = (gint32) start;
    gint32 end_g = (gint32) end;
    PangoGlyphString *glyphs_g = (PangoGlyphString *)getPointerFromHandle(env, glyphs);
    PangoFont *font_g = (PangoFont *)getPointerFromHandle(env, font);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
    pango_glyph_string_extents_range (glyphs_g, start_g, end_g, font_g, inkRect_g, logicalRect_g);
}

/*
 * Class:     org.gnu.pango.GlyphString
 * Method:    pango_glyph_string_get_logical_widths
 */
JNIEXPORT void JNICALL 
Java_org_gnu_pango_GlyphString_pango_1glyph_1string_1get_1logical_1widths (JNIEnv *env, jclass 
    cls, jobject glyphs, jstring text, jint length, jint embeddingLevel, jintArray logicalWidths) 
{
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, 0);
    gint *logicalWidths_g = (gint *) (*env)->GetIntArrayElements (env, logicalWidths, NULL);
    pango_glyph_string_get_logical_widths ((PangoGlyphString *)getPointerFromHandle(env, glyphs), 
    		text_g, (gint32)length, (gint32)embeddingLevel, logicalWidths_g);
    (*env)->ReleaseIntArrayElements (env, logicalWidths, (jint *) logicalWidths_g, 0);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
}


#ifdef __cplusplus
}

#endif
