/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_CellRendererCombo
#define _Included_org_gnu_gtk_CellRendererCombo
#include "org_gnu_gtk_CellRendererCombo.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    getModel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererCombo_getModel
  (JNIEnv *env, jclass cls, jobject renderer)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	return getGObjectHandle(env, G_OBJECT(renderer_g->model));
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    setModel
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRendererCombo_setModel
  (JNIEnv *env, jclass cls, jobject renderer, jobject model)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	renderer_g->model = model_g;
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    getTextColumn
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellRendererCombo_getTextColumn
  (JNIEnv *env, jclass cls, jobject renderer)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	return (jint)renderer_g->text_column;
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    setTextColumn
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRendererCombo_setTextColumn
  (JNIEnv *env, jclass cls, jobject renderer, jint textColumn)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	renderer_g->text_column = (gint)textColumn;
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    getHasEntry
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_CellRendererCombo_getHasEntry
  (JNIEnv *env, jclass cls, jobject renderer)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	return (jboolean)renderer_g->has_entry;
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    setHasEntry
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRendererCombo_setHasEntry
  (JNIEnv *env, jclass cls, jobject renderer, jboolean hasEntry)
{
	GtkCellRendererCombo* renderer_g = (GtkCellRendererCombo*)getPointerFromHandle(env, renderer);
	renderer_g->has_entry = (gboolean)hasEntry;
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    gtk_cell_renderer_combo_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellRendererCombo_gtk_1cell_1renderer_1combo_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_cell_renderer_combo_get_type();
}

/*
 * Class:     org_gnu_gtk_CellRendererCombo
 * Method:    gtk_cell_renderer_combo_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererCombo_gtk_1cell_1renderer_1combo_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_cell_renderer_combo_new());
}
#ifdef __cplusplus
}
#endif
#endif
