/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventProximity.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventProximity
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventProximity_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProximity *obj_g = (GdkEventProximity *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventProximity
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventProximity_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventProximity *obj_g = (GdkEventProximity *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventProximity
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventProximity_getTime (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProximity *obj_g = (GdkEventProximity *)getPointerFromHandle(env, obj);
    return obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventProximity
 * Method:    getDevice
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventProximity_getDevice (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProximity *obj_g = (GdkEventProximity *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, G_OBJECT(obj_g->device));
}


#ifdef __cplusplus
}

#endif
