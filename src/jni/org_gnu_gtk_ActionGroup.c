/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <string.h>
#include <jg_jnu.h>
#include "gtk_java.h"
#include "handleCallbackAction.h"
#ifndef _Included_org_gnu_gtk_ActionGroup
#define _Included_org_gnu_gtk_ActionGroup
#include "org_gnu_gtk_ActionGroup.h"
#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    JNIEnv *env;
    jobject obj;  /* object to recieve the signal */
    jmethodID methodID;  /* methodID of callback method */
} CallbackInfo;

static void internalHandleActionCallback(gpointer action, CallbackInfo *cbi)
{
	jvalue *jargs = g_new(jvalue, 1);

	if (cbi == NULL) {
		g_critical("Java-GNOME - unable to determine the callback method\n");
		return;
	}

	jargs->l = getGObjectHandle(cbi->env, action);
	(* cbi->env)->CallVoidMethodA(cbi->env, cbi->obj, cbi->methodID, jargs);
}

/*
 * Callback method to handle all signals.
 */
static void handleCallbackRadioAction(GtkRadioAction* action, GtkRadioAction* current, gpointer userData)
{
	CallbackInfo *cbi = userData;
	internalHandleActionCallback((gpointer) action, cbi);
}


void handleCallbackAction(GtkAction* action, gpointer userData) {
	CallbackInfo *cbi = userData;
	internalHandleActionCallback((gpointer) action, cbi);
}
                                                                                                                                               
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_action_group_get_type();
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1new
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jobject ret = getGObjectHandle(env, (GObject *) gtk_action_group_new(n));
	(*env)->ReleaseStringUTFChars(env, name, n);
	return ret;
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1get_1name
  (JNIEnv *env, jclass cls, jobject group)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	const gchar* name = gtk_action_group_get_name(g);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_get_sensitive
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1get_1sensitive
  (JNIEnv *env, jclass cls, jobject group)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	return (jboolean)gtk_action_group_get_sensitive(g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_set_sensitive
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1set_1sensitive
  (JNIEnv *env, jclass cls, jobject group, jboolean value)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	gtk_action_group_set_sensitive(g, (gboolean)value);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_get_visible
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1get_1visible
  (JNIEnv *env, jclass cls, jobject group)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	return (jboolean)gtk_action_group_get_visible(g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_set_visible
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1set_1visible
  (JNIEnv *env, jclass cls, jobject group, jboolean value)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	gtk_action_group_set_sensitive(g, (gboolean)value);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_get_action
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1get_1action
  (JNIEnv *env, jclass cls, jobject group, jstring name)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	GtkAction* action = gtk_action_group_get_action(g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	if (NULL == action)
		return NULL;
	return getGObjectHandle(env, (GObject *) action);
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_list_actions
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1list_1actions
  (JNIEnv *env, jclass cls, jobject group)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	return getGObjectHandlesFromGList(env, gtk_action_group_list_actions(g));
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_add_action
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1add_1action
  (JNIEnv *env, jclass cls, jobject group, jobject action)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	GtkAction* a = getPointerFromHandle(env, action);
	gtk_action_group_add_action(g, a);
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_remove_action
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1remove_1action
  (JNIEnv *env, jclass cls, jobject group, jobject action)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	GtkAction* a = getPointerFromHandle(env, action);
	gtk_action_group_remove_action(g, a);
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_set_translation_domain
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1set_1translation_1domain
  (JNIEnv *env, jclass cls, jobject group, jstring domain)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	const gchar* d = (*env)->GetStringUTFChars(env, domain, NULL);
	gtk_action_group_set_translation_domain(g, d);
	(*env)->ReleaseStringUTFChars(env, domain, d);
}

/*  
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_translate_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1translate_1string
  (JNIEnv *env, jclass cls, jobject group, jstring str)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	const gchar* s = (*env)->GetStringUTFChars(env, str, NULL);
	const gchar *string = gtk_action_group_translate_string(g, s);
	(*env)->ReleaseStringUTFChars(env, str, s);
	return (*env)->NewStringUTF(env, string);
}


/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    gtk_action_group_add_action_with_accel
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_gtk_1action_1group_1add_1action_1with_1accel
  (JNIEnv *env, jclass cls, jobject group, jobject action, jstring accel)
{
	GtkActionGroup* g = getPointerFromHandle(env, group);
	GtkAction* act = getPointerFromHandle(env, action);
	const gchar* a = (*env)->GetStringUTFChars(env, accel, NULL);
	gtk_action_group_add_action_with_accel(g, act, a);
	(*env)->ReleaseStringUTFChars(env, accel, a);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    addActions
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_addActions
  (JNIEnv *env, jclass cls, jobject group, jobjectArray entries, jobjectArray objs)
{
	CallbackInfo *cbData;
  	jthrowable exc;
	jsize len;
	int index;

	GtkActionGroup* g = getPointerFromHandle(env, group);
	len = (*env)->GetArrayLength(env, entries);
	for (index = 0; index < len; index++) {
		jobject act = (*env)->GetObjectArrayElement(env, entries, index);
		GtkActionEntry* entry = (GtkActionEntry*)getPointerFromHandle(env, act);
		jobject obj = (*env)->GetObjectArrayElement(env, objs, (jsize)index);
		cbData = g_new(CallbackInfo, 1);
		cbData->env = env;
		cbData->obj = (*env)->NewGlobalRef(env, obj);
		cbData->methodID = (*env)->GetMethodID(env, (*env)->GetObjectClass(env, obj),
				"handleCallback", "(Lorg/gnu/glib/Handle;)V");
	 	exc = (*env)->ExceptionOccurred(env);
  		if (exc) {
	   		g_critical("Java-GNOME - exception is:\n");
	    	(*env)->ExceptionDescribe(env);
    		(*env)->ExceptionClear(env);
	   		g_warning("\n\nJava-GNOME - signal will not be mapped\n\n");
	   	 	return;
		}
		gtk_action_group_add_actions(g, entry, 1, cbData);
	}
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    addToggleActions
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_addToggleActions
  (JNIEnv *env, jclass cls, jobject group, jobjectArray entries, jobjectArray objs)
{
	CallbackInfo *cbData;
  	jthrowable exc;
	jsize len;
	int index;

	GtkActionGroup* g = getPointerFromHandle(env, group);
	len = (*env)->GetArrayLength(env, entries);
	for (index = 0; index < len; index++) {
		jobject act = (*env)->GetObjectArrayElement(env, entries, index);
		GtkToggleActionEntry* entry = (GtkToggleActionEntry*)getPointerFromHandle(env, act);
		jobject obj = (*env)->GetObjectArrayElement(env, objs, (jsize)index);
		cbData = g_new(CallbackInfo, 1);
		cbData->env = env;
		cbData->obj = (*env)->NewGlobalRef(env, obj);
		cbData->methodID = (*env)->GetMethodID(env, (*env)->GetObjectClass(env, obj),
				"handleCallback", "(Lorg/gnu/glib/Handle;)V");
  		exc = (*env)->ExceptionOccurred(env);
	  	if (exc) {
	    	g_critical("Java-GNOME - exception is:\n");
    		(*env)->ExceptionDescribe(env);
    		(*env)->ExceptionClear(env);
	    	g_warning("\n\nJava-GNOME - signal will not be mapped\n\n");
   		 	return;
	 	}
		gtk_action_group_add_toggle_actions(g, entry, 1, cbData);
	}
}

/*
 * Class:     org_gnu_gtk_ActionGroup
 * Method:    addRadioActions
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionGroup_addRadioActions
  (JNIEnv *env, jclass cks, jobject group, jobjectArray entries, int value, jobject listener)
{
	CallbackInfo *cbData;
  	jthrowable exc;
	jsize len;
	int index;
	GtkRadioActionEntry* ents;
	GtkRadioActionEntry* ptr;

	GtkActionGroup* g = getPointerFromHandle(env, group);
	len = (*env)->GetArrayLength(env, entries);
	ents = (GtkRadioActionEntry*)g_malloc(sizeof(GtkRadioActionEntry) * len);
	ptr = ents;
	for (index = 0; index < len; index++) {
		jobject act = (*env)->GetObjectArrayElement(env, entries, index);
		GtkRadioActionEntry* r = (GtkRadioActionEntry*)getPointerFromHandle(env, act);
		memcpy(ptr, r, sizeof(GtkRadioActionEntry));
		ptr++;
	}
	
	cbData = g_malloc(sizeof(CallbackInfo));
	cbData->env = env;
	cbData->obj = listener;
	cbData->methodID = (*env)->GetMethodID(env, (*env)->GetObjectClass(env, listener),
			"handleRadioAction", "(Lorg/gnu/glib/Handle;I)V");
  	exc = (*env)->ExceptionOccurred(env);
	if (exc) {
	   	g_critical("Java-GNOME - exception is:\n");
    	(*env)->ExceptionDescribe(env);
    	(*env)->ExceptionClear(env);
	   	g_warning("\n\nJava-GNOME - signal will not be mapped\n\n");
   	 	return;
	}
	gtk_action_group_add_radio_actions(g, ents, len, (guint)value, G_CALLBACK(handleCallbackRadioAction), (gpointer)cbData);
}


#ifdef __cplusplus
}
#endif
#endif
