/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TextTag.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    getName
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextTag_getName 
  (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
    gchar *result_g = cptr_g->name;
    return result_g ? (*env)->NewStringUTF(env, result_g) : NULL;
}

///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getBgColorSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getBgColorSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->bg_color_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getBgStippleSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getBgStippleSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->bg_stipple_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getFgColorSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getFgColorSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->fg_color_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getScaleSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getScaleSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->scale_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getFgStippleSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getFgStippleSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->fg_stipple_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getJustificationSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getJustificationSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->justification_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getLeftMarginSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getLeftMarginSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->left_margin_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getIndentSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getIndentSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return (jboolean) cptr_g->indent_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getRiseSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getRiseSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->rise_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getStrikethroughSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getStrikethroughSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->strikethrough_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getRightMarginSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getRightMarginSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->right_margin_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPixelsAboveLinesSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPixelsAboveLinesSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pixels_above_lines_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPixelsBelowLinesSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPixelsBelowLinesSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pixels_below_lines_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPixelsInsideWrapSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPixelsInsideWrapSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pixels_inside_wrap_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getTabsSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getTabsSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->tabs_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getUnderlineSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getUnderlineSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->underline_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getWrapModeSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getWrapModeSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->wrap_mode_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getBgFullHeightSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getBgFullHeightSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->bg_full_height_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getInvisibleSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getInvisibleSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->invisible_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getEditableSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getEditableSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->editable_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getLanguageSet
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getLanguageSet (JNIEnv *env, jclass cls, jobject cptr) 
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->language_set;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPad1
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPad1 (JNIEnv *env, jclass cls, jobject cptr)
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pad1;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPad2
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPad2 (JNIEnv *env, jclass cls, jobject cptr)
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pad2;
//}
//
///*
// * Class:     org.gnu.gtk.TextTag
// * Method:    getPad3
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_getPad3 (JNIEnv *env, jclass cls, jobject cptr)
//{
//    GtkTextTag *cptr_g = (GtkTextTag *)getPointerFromHandle(env, cptr);
//    return cptr_g->pad3;
//}

/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    gtk_text_tag_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextTag_gtk_1text_1tag_1get_1type 
   (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_text_tag_get_type ();
}

/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    gtk_text_tag_new
 * Signature: ([B)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextTag_gtk_1text_1tag_1new 
   (JNIEnv *env, jclass cls, jstring name) 
{
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    jobject handle = getGObjectHandle(env, (GObject *) gtk_text_tag_new (utf));
    (*env)->ReleaseStringUTFChars(env, name, utf);
    return handle;
}

/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    gtk_text_tag_get_priority
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextTag_gtk_1text_1tag_1get_1priority 
   (JNIEnv *env, jclass cls, jobject tag) 
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jint) (gtk_text_tag_get_priority (tag_g));
}

/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    gtk_text_tag_set_priority
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_gtk_1text_1tag_1set_1priority 
   (JNIEnv *env, jclass cls, jobject tag, jint priority) 
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    gint32 priority_g = (gint32) priority;
    gtk_text_tag_set_priority (tag_g, priority_g);
}

  /* As it is in the java code, so shall it be here. Commented */
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.TextTag
 * Method:    gtk_text_tag_event
 * Signature: (IIII)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextTag_gtk_1text_1tag_1event 
   (JNIEnv *env, jclass jclass, jobject tag, jobject eventObject, jobject event, jobject iter) 
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    GObject *eventObject_g = (GObject *)getPointerFromHandle(env, eventObject);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_tag_event (tag_g, eventObject_g, event_g, 
                                           iter_g));
}
#endif

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setBooleanProperty 
   (JNIEnv *env, jclass cls, jobject renderer, jstring property, jboolean setting) 
{
    GtkTextTag *renderer_g = (GtkTextTag *)getPointerFromHandle(env, renderer);
    const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
    g_object_set( renderer_g,  property_utf, setting, NULL);
    (*env)->ReleaseStringUTFChars( env, property, property_utf);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setStringProperty 
   (JNIEnv *env, jclass cls, jobject renderer, jstring property, jstring setting) 
{
    GtkTextTag *renderer_g = (GtkTextTag *)getPointerFromHandle(env, renderer);
    const char* setting_utf = (*env)->GetStringUTFChars( env, setting, NULL );
    const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
    g_object_set( renderer_g,  property_utf, setting_utf, NULL);
    (*env)->ReleaseStringUTFChars( env, property, property_utf);
}
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setIntProperty
  (JNIEnv *env, jclass cls, jobject renderer, jstring property, jint setting) 
{
    GtkTextTag *renderer_g = (GtkTextTag *)getPointerFromHandle(env, renderer);
    const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
    g_object_set( renderer_g,  property_utf, setting, NULL);
    (*env)->ReleaseStringUTFChars( env, property, property_utf);
}
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setDoubleProperty 
  (JNIEnv *env, jclass cls, jobject renderer, jstring property, jdouble setting) 
{
    GtkTextTag *renderer_g = (GtkTextTag *)getPointerFromHandle(env, renderer);
    const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
    g_object_set( renderer_g,  property_utf, setting, NULL);
    (*env)->ReleaseStringUTFChars( env, property, property_utf);
}

/*
 * Class:     org_gnu_gtk_TextTag
 * Method:    setPixmapProperty
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setPixmapProperty
  (JNIEnv *env, jclass cls, jobject tag, jstring property, jobject pixmap)
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
    GdkPixmap *pixmap_g = (GdkPixmap*)getPointerFromHandle(env, pixmap);
    g_object_set( tag_g, property_utf, pixmap_g, NULL);
    (*env)->ReleaseStringUTFChars( env, property, property_utf);
}

/*
 * Class:     org_gnu_gtk_TextTag
 * Method:    setTabs
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setTabs
  (JNIEnv *env, jclass cls, jobject tag, jobject tab)
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    PangoTabArray *tabArray = (PangoTabArray*)getPointerFromHandle(env, tab);
    g_object_set( tag_g, "tabs", tabArray, NULL);
}

/*
 * Class:     org_gnu_gtk_TextTag
 * Method:    setFontDesc
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextTag_setFontDesc
  (JNIEnv *env, jclass cls, jobject tag, jobject fontDesc)
{
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    PangoFontDescription *fontDesc_g = (PangoFontDescription*)getPointerFromHandle(env, fontDesc);
    g_object_set( tag_g, "font_desc", fontDesc_g, NULL);
}



#ifdef __cplusplus
}

#endif
