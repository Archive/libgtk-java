/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Requisition.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    gtk_requisition_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Requisition_gtk_1requisition_1new (JNIEnv *env, jclass cls)
{
    return getGBoxedHandle(env, g_malloc(sizeof(GtkRequisition)),
    		GTK_TYPE_REQUISITION, NULL, (GBoxedFreeFunc) gtk_requisition_free);
}

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Requisition_getWidth(JNIEnv *env, jclass cls, jobject cptr) 
{
	GtkRequisition* cptr_g = (GtkRequisition*)getPointerFromHandle(env, cptr);
    return (jint)cptr_g->width;
}

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    setWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Requisition_setWidth (JNIEnv *env, jclass cls, jobject cptr, jint 
    width) 
{
	GtkRequisition* cptr_g = (GtkRequisition*)getPointerFromHandle(env, cptr);
    cptr_g->width = width;
}

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Requisition_getHeight (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
	GtkRequisition* cptr_g = (GtkRequisition*)getPointerFromHandle(env, cptr);
    return cptr_g->height;
}

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    setHeight
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Requisition_setHeight (JNIEnv *env, jclass cls, jobject cptr, jint 
    height) 
{
	GtkRequisition* cptr_g = (GtkRequisition*)getPointerFromHandle(env, cptr);
    cptr_g->height = height;
}

/*
 * Class:     org.gnu.gtk.Requisition
 * Method:    gtk_requisition_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Requisition_gtk_1requisition_1free (JNIEnv *env, jclass 
    cls, jobject requisition) 
{
	GtkRequisition* requisition_g = (GtkRequisition*)getPointerFromHandle(env, requisition);
    gtk_requisition_free(requisition_g);
}

#ifdef __cplusplus
}

#endif
