/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_InputDialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.InputDialog
 * Method:    gtk_input_dialog_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_InputDialog_gtk_1input_1dialog_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_input_dialog_get_type ();
}

/*
 * Class:     org.gnu.gtk.InputDialog
 * Method:    gtk_input_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_InputDialog_gtk_1input_1dialog_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_input_dialog_new ());
}

#ifdef __cplusplus
}

#endif
