/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_PixbufAnimationIter.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.PixbufAnimationIter
 * Method:    gdk_pixbuf_animation_iter_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gdk_PixbufAnimationIter_gdk_1pixbuf_1animation_1iter_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gdk_pixbuf_animation_iter_get_type ();
}

/*
 * Class:     org_gnu_gdk_PixbufAnimationIter
 * Method:    gdk_pixbuf_animation_iter_advance
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufAnimationIter_gdk_1pixbuf_1animation_1iter_1advance
  (JNIEnv *env, jclass cls, jobject iter, jlong sec, jlong usec)
{
    GdkPixbufAnimationIter *iter_g = (GdkPixbufAnimationIter *)getPointerFromHandle(env, iter);
	GTimeVal* timeVal = g_new(GTimeVal, 1);
	timeVal->tv_sec = sec;
	timeVal->tv_usec = usec;
	return (jboolean)gdk_pixbuf_animation_iter_advance(iter_g, timeVal);
}

/*
 * Class:     org.gnu.gdk.PixbufAnimationIter
 * Method:    gdk_pixbuf_animation_iter_get_delay_time
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gdk_PixbufAnimationIter_gdk_1pixbuf_1animation_1iter_1get_1delay_1time (JNIEnv 
    *env, jclass cls, jobject iter) 
{
    GdkPixbufAnimationIter *iter_g = (GdkPixbufAnimationIter *)getPointerFromHandle(env, iter);
    return (jint) gdk_pixbuf_animation_iter_get_delay_time (iter_g);
}

/*
 * Class:     org.gnu.gdk.PixbufAnimationIter
 * Method:    gdk_pixbuf_animation_iter_get_pixbuf
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gdk_PixbufAnimationIter_gdk_1pixbuf_1animation_1iter_1get_1pixbuf (JNIEnv *env, 
    jclass cls, jobject iter) 
{
    GdkPixbufAnimationIter *iter_g = (GdkPixbufAnimationIter *)getPointerFromHandle(env, iter);
    return getGObjectHandle(env, G_OBJECT(gdk_pixbuf_animation_iter_get_pixbuf (iter_g)));
}

/*
 * Class:     org.gnu.gdk.PixbufAnimationIter
 * Method:    gdk_pixbuf_animation_iter_on_currently_loading_frame
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gdk_PixbufAnimationIter_gdk_1pixbuf_1animation_1iter_1on_1currently_1loading_1frame
(JNIEnv *env, jclass cls, jobject iter) 
{
    GdkPixbufAnimationIter *iter_g = (GdkPixbufAnimationIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gdk_pixbuf_animation_iter_on_currently_loading_frame (iter_g));
}


#ifdef __cplusplus
}

#endif
