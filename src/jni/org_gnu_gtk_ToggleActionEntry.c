/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"
#include "handleCallbackAction.h"
#include "org_gnu_gtk_ToggleActionEntry.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    allocate
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToggleActionEntry_allocate(JNIEnv *env, jclass cls)
{
    GtkToggleActionEntry* entry = 
        (GtkToggleActionEntry*)g_malloc(sizeof(GtkToggleActionEntry));
    entry->callback = G_CALLBACK(handleCallbackAction);
    return getStructHandle(env, entry, NULL, g_free);
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    free
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_free(JNIEnv *env, jclass cls, jobject entry)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    g_free(entry_g);
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setName
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setName(JNIEnv *env, jclass cls, jobject entry, jstring name)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    if (NULL == name) {
        entry_g->name = NULL;
    } else {
        gchar* n = (gchar*)(*env)->GetStringUTFChars(env, name, NULL);
        entry_g->name = n;
    }
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setStockId
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setStockId(JNIEnv *env, jclass cls, jobject entry, jstring value)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    if (NULL == value) {
        entry_g->stock_id = NULL;
    } else {
        gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
        entry_g->stock_id = v;
    }
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setLabel
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setLabel(JNIEnv *env, jclass cls, jobject entry, jstring value)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    if (NULL == value) {
        entry_g->label = NULL;
    } else {
        gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
        entry_g->label = v;
    }
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setAccelerator
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setAccelerator(JNIEnv *env, jclass cls, jobject entry, jstring value)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    if (NULL == value) {
        entry_g->accelerator = NULL;
    } else {
        gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
        entry_g->accelerator = v;
    }
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setToolTip
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setToolTip(JNIEnv *env, jclass cls, jobject entry, jstring value)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    if (NULL == value) {
        entry_g->tooltip = NULL;
    } else {
        gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
        entry_g->tooltip = v;
    }
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    setActive
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleActionEntry_setActive(JNIEnv *env, jclass cls, jobject entry, jboolean active)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    entry_g->is_active = (gboolean)active;
}

/*
 * Class:     org_gnu_gtk_ToggleActionEntry
 * Method:    getActive
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToggleActionEntry_getActive(JNIEnv *env, jclass cls, jobject entry)
{
    GtkToggleActionEntry* entry_g = 
        (GtkToggleActionEntry*)getPointerFromHandle(env, entry);
    return (jboolean)entry_g->is_active;
}

#ifdef __cplusplus
}
#endif
