/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 * This file was orriginally generated by the Java-GNOME Code Generator
 * Please do not modify the code that is identified as generated.  Also,
 * please insert your code above the generated code.
 *
 * Generation date: 2002-08-02 09:42:28 EDT
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_WidgetAuxInfo.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getX
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getX (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->x);
}

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getY
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getY (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->y);
}

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getWidth
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getWidth (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->width);
}

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getHeight
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getHeight (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->height);
}

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getXSet
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getXSet (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->x_set);
}

/*
 * Class:     org.gnu.gtk.WidgetAuxInfo
 * Method:    getYSet
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_WidgetAuxInfo_getYSet (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkWidgetAuxInfo *cptr_g = 
        (GtkWidgetAuxInfo *)getPointerFromHandle(env, cptr);
    return (jint)(cptr_g->y_set);
}

#ifdef __cplusplus
}

#endif
