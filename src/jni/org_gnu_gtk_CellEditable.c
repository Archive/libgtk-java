/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellEditable.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_CellEditable
 * Method:    newObject
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellEditable_newObject
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, g_object_new(GTK_TYPE_CELL_EDITABLE, NULL));
}
                                                                                

/*
 * Class:     org.gnu.gtk.CellEditable
 * Method:    gtk_cell_editable_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellEditable_gtk_1cell_1editable_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_cell_editable_get_type ();
}

/*
 * Class:     org.gnu.gtk.CellEditable
 * Method:    gtk_cell_editable_start_editing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellEditable_gtk_1cell_1editable_1start_1editing (
    JNIEnv *env, jclass cls, jobject cellEditable, jobject event) 
{
    GtkCellEditable *cellEditable_g = (GtkCellEditable *)getPointerFromHandle(env, cellEditable);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gtk_cell_editable_start_editing (cellEditable_g, event_g);
}

/*
 * Class:     org.gnu.gtk.CellEditable
 * Method:    gtk_cell_editable_editing_done
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellEditable_gtk_1cell_1editable_1editing_1done (JNIEnv 
    *env, jclass cls, jobject cellEditable) 
{
    GtkCellEditable *cellEditable_g = (GtkCellEditable *)getPointerFromHandle(env, cellEditable);
    gtk_cell_editable_editing_done (cellEditable_g);
}

/*
 * Class:     org.gnu.gtk.CellEditable
 * Method:    gtk_cell_editable_remove_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellEditable_gtk_1cell_1editable_1remove_1widget (
    JNIEnv *env, jclass cls, jobject cellEditable) 
{
    GtkCellEditable *cellEditable_g = (GtkCellEditable *)getPointerFromHandle(env, cellEditable);
    gtk_cell_editable_remove_widget (cellEditable_g);
}

#ifdef __cplusplus
}

#endif
