/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_FontDescription.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)pango_font_description_get_type ();
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1new (JNIEnv 
    *env, jclass cls) 
{
    return getGBoxedHandle(env, pango_font_description_new (),
                           PANGO_TYPE_FONT_DESCRIPTION, NULL,
                           (JGFreeFunc)pango_font_description_free);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1copy (
    JNIEnv *env, jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    return getGBoxedHandle(env, pango_font_description_copy (desc_g),
                           PANGO_TYPE_FONT_DESCRIPTION, NULL,
                           (JGFreeFunc)pango_font_description_free);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_hash
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1hash (
    JNIEnv *env, jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_hash (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_equal
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1equal (
    JNIEnv *env, jclass cls, jobject desc1, jobject desc2) 
{
    PangoFontDescription *desc1_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc1);
    PangoFontDescription *desc2_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc2);
    return (jboolean) (pango_font_description_equal (desc1_g, desc2_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_family
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1family (
    JNIEnv *env, jclass cls, jobject desc, jstring family) 
{
    gchar* family_g = (gchar*)(*env)->GetStringUTFChars(env, family, 0);
    pango_font_description_set_family ((PangoFontDescription*)getPointerFromHandle(env, desc), family_g);
    (*env)->ReleaseStringUTFChars(env, family, family_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_family
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1family (JNIEnv *env, jclass 
    cls, jobject desc) 
{
    const gchar *result_g = pango_font_description_get_family(
    		(PangoFontDescription *)getPointerFromHandle(env, desc));
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_style
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1style (
    JNIEnv *env, jclass cls, jobject desc, jint style) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoStyle style_g = (PangoStyle) style;
    pango_font_description_set_style (desc_g, style_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_style
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1style (
    JNIEnv *env, jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_style (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_variant
 */
JNIEXPORT void JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1variant (JNIEnv *env, jclass 
    cls, jobject desc, jint variant) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoVariant variant_g = (PangoVariant) variant;
    pango_font_description_set_variant (desc_g, variant_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_variant
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1variant (JNIEnv *env, jclass 
    cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_variant (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_weight
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1weight (
    JNIEnv *env, jclass cls, jobject desc, jint weight) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoWeight weight_g = (PangoWeight) weight;
    pango_font_description_set_weight (desc_g, weight_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_weight
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1weight (
    JNIEnv *env, jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_weight (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_stretch
 */
JNIEXPORT void JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1stretch (JNIEnv *env, jclass 
    cls, jobject desc, jint stretch) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoStretch stretch_g = (PangoStretch) stretch;
    pango_font_description_set_stretch (desc_g, stretch_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_stretch
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1stretch (JNIEnv *env, jclass 
    cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_stretch (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_set_size
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1set_1size (
    JNIEnv *env, jclass cls, jobject desc, jint size) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    gint32 size_g = (gint32) size;
    pango_font_description_set_size (desc_g, size_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1size (
    JNIEnv *env, jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_size (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_get_set_fields
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1get_1set_1fields (JNIEnv *env, 
    jclass cls, jobject desc) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jint) (pango_font_description_get_set_fields (desc_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_unset_fields
 */
JNIEXPORT void JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1unset_1fields (JNIEnv *env, jclass 
    cls, jobject desc, jint toUnset) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    pango_font_description_unset_fields (desc_g, (PangoFontMask)toUnset);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_merge
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontDescription_pango_1font_1description_1merge (
    JNIEnv *env, jclass cls, jobject desc, jobject descToMerge, jboolean replaceExisting) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoFontDescription *descToMerge_g = (PangoFontDescription *)getPointerFromHandle(env, descToMerge);
    gboolean replaceExisting_g = (gboolean) replaceExisting;
    pango_font_description_merge (desc_g, descToMerge_g, replaceExisting_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_better_match
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1better_1match (JNIEnv *env, jclass 
    cls, jobject desc, jobject oldMatch, jobject newMatch) 
{
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoFontDescription *oldMatch_g = (PangoFontDescription *)getPointerFromHandle(env, oldMatch);
    PangoFontDescription *newMatch_g = (PangoFontDescription *)getPointerFromHandle(env, newMatch);
    return (jboolean) (pango_font_description_better_match (desc_g, oldMatch_g, newMatch_g));
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_from_string
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1from_1string (JNIEnv *env, jclass 
    cls, jstring str) 
{
    gchar* str_g = (gchar*)(*env)->GetStringUTFChars(env, str, 0);
    jobject result = 
        getGBoxedHandle(env, pango_font_description_from_string (str_g),
                        PANGO_TYPE_FONT_DESCRIPTION, NULL,
                        (JGFreeFunc)pango_font_description_free);
    (*env)->ReleaseStringUTFChars(env, str, str_g);
    return result;
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_to_string
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1to_1string (JNIEnv *env, jclass 
    cls, jobject desc) 
{
    gchar *result_g = pango_font_description_to_string(
    		(PangoFontDescription *)getPointerFromHandle(env, desc));
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.pango.FontDescription
 * Method:    pango_font_description_to_filename
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_pango_FontDescription_pango_1font_1description_1to_1filename (JNIEnv *env, jclass 
    cls, jobject desc) 
{
    gchar *result_g = pango_font_description_to_filename(
    		(PangoFontDescription *)getPointerFromHandle(env, desc));
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
