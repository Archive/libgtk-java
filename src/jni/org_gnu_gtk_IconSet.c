/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IconSet.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1new (JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, gtk_icon_set_new (), GTK_TYPE_ICON_SET, NULL,
    		(GBoxedFreeFunc) gtk_icon_set_unref);
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_new_from_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1new_1from_1pixbuf (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return getGBoxedHandle(env, gtk_icon_set_new_from_pixbuf (pixbuf_g),
    		GTK_TYPE_ICON_SET, NULL, (GBoxedFreeFunc) gtk_icon_set_unref);
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1ref (JNIEnv *env, jclass cls, 
    jobject iconSet) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
   	gtk_icon_set_ref (iconSet_g);
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1unref (JNIEnv *env, jclass cls, 
    jobject iconSet) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    gtk_icon_set_unref (iconSet_g);
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_render_icon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1render_1icon (JNIEnv *env, 
    jclass cls, jobject iconSet, jobject style, jint direction, jint state, jint size, jobject widget, 
    jstring detail) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    GtkStyle *style_g = (GtkStyle *)getPointerFromHandle(env, style);
    GtkTextDirection direction_g = (GtkTextDirection) direction;
    GtkStateType state_g = (GtkStateType) state;
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gchar* detail_g = (gchar*)(*env)->GetStringUTFChars(env, detail, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_icon_set_render_icon (iconSet_g, style_g, direction_g, state_g, 
            		(GtkIconSize)size, widget_g, detail_g));
    (*env)->ReleaseStringUTFChars(env, detail, detail_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_add_source
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1add_1source (JNIEnv *env, 
    jclass cls, jobject iconSet, jobject source) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gtk_icon_set_add_source (iconSet_g, source_g);
}

/*
 * Class:     org.gnu.gtk.IconSet
 * Method:    gtk_icon_set_get_sizes
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1get_1sizes (JNIEnv *env, jclass 
    cls, jobject iconSet, jintArray sizes, jintArray numSizes) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    gint *sizes_g_g = (gint *) (*env)->GetIntArrayElements (env, sizes, NULL);
    GtkIconSize **sizes_g = (GtkIconSize **)sizes_g_g;
    gint *numSizes_g = (gint *) (*env)->GetIntArrayElements (env, numSizes, NULL);
    gtk_icon_set_get_sizes (iconSet_g, sizes_g, numSizes_g);
    (*env)->ReleaseIntArrayElements (env, sizes, (jint *)sizes_g_g, 0);
    (*env)->ReleaseIntArrayElements (env, numSizes, (jint *) numSizes_g, 0);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSet_gtk_1icon_1set_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_icon_set_get_type();
}

#ifdef __cplusplus
}

#endif
