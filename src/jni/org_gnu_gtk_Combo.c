/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Combo.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkEntry * GtkCombo_get_entry (GtkCombo * cptr)
{
    return (GtkEntry*)cptr->entry;
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    getEntry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Combo_getEntry (JNIEnv *env, jclass
cls, jobject cptr)
{
    GtkCombo *cptr_g = (GtkCombo *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkCombo_get_entry (cptr_g));
}

   /*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_combo_get_type ();
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_combo_new ());
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_value_in_list
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1value_1in_1list (JNIEnv *env, 
    jclass cls, jobject combo, jboolean val, jboolean okIfEmpty) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    gboolean val_g = (gboolean) val;
    gboolean okIfEmpty_g = (gboolean) okIfEmpty;
    gtk_combo_set_value_in_list (combo_g, val_g, okIfEmpty_g);
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_use_arrows
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1use_1arrows (JNIEnv *env, jclass 
    cls, jobject combo, jboolean val) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    gboolean val_g = (gboolean) val;
    gtk_combo_set_use_arrows (combo_g, val_g);
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_use_arrows_always
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1use_1arrows_1always (JNIEnv 
    *env, jclass cls, jobject combo, jboolean val) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    gboolean val_g = (gboolean) val;
    gtk_combo_set_use_arrows_always (combo_g, val_g);
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_case_sensitive
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1case_1sensitive (JNIEnv *env, 
    jclass cls, jobject combo, jboolean val) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    gboolean val_g = (gboolean) val;
    gtk_combo_set_case_sensitive (combo_g, val_g);
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_item_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1item_1string (JNIEnv *env, 
    jclass cls, jobject combo, jobject item, jstring itemValue) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    GtkItem *item_g = (GtkItem *)getPointerFromHandle(env, item);
    const gchar* itemValue_g = (*env)->GetStringUTFChars(env, itemValue, NULL);
	gtk_combo_set_item_string (combo_g, item_g, itemValue_g);
	(*env)->ReleaseStringUTFChars(env, itemValue, itemValue_g );
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_set_popdown_strings
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1set_1popdown_1strings
  (JNIEnv *env, jclass cls, jobject combo, jobjectArray strings) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    //GList* strings_g = (GList*)getPointerFromHandle(env, strings);
    GList* strings_g = getGListFromStringArray(env, strings);
    gtk_combo_set_popdown_strings (combo_g, strings_g);
    releaseStringArrayInGList(env, strings, strings_g);
}

/*
 * Class:     org.gnu.gtk.Combo
 * Method:    gtk_combo_disable_activate
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Combo_gtk_1combo_1disable_1activate (JNIEnv *env, 
    jclass cls, jobject combo) 
{
    GtkCombo *combo_g = (GtkCombo *)getPointerFromHandle(env, combo);
    gtk_combo_disable_activate (combo_g);
}


#ifdef __cplusplus
}

#endif
