/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_PixbufAnimation.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gdk_pixbuf_animation_get_type ();
}

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_new_from_file
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1new_1from_1file (
    JNIEnv *env, jclass cls, jstring filename, jobject error) 
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, 0);
    GError* error_g = NULL;
    jobject result = 
        getGObjectHandle(env, 
                         G_OBJECT(gdk_pixbuf_animation_new_from_file (filename_g, 
                                                             &error_g)));
    if (NULL != error_g)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result;
}

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_get_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1get_1width (
    JNIEnv *env, jclass cls, jobject animation) 
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    return (jint) (gdk_pixbuf_animation_get_width (animation_g));
}

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_get_height
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1get_1height (
    JNIEnv *env, jclass cls, jobject animation) 
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    return (jint) (gdk_pixbuf_animation_get_height (animation_g));
}

/*
 * Class:     org_gnu_gdk_PixbufAnimation
 * Method:    gdk_pixbuf_animation_get_iter
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1get_1iter
  (JNIEnv *env, jclass cls, jobject animation, jlong sec, jlong usec)
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    GTimeVal* timeVal = g_new(GTimeVal, 1);
    timeVal->tv_sec = sec;
    timeVal->tv_usec = usec;
    return getGObjectHandle(env, 
   					G_OBJECT(gdk_pixbuf_animation_get_iter(animation_g, timeVal)));
}

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_is_static_image
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1is_1static_1image (JNIEnv *env, jclass 
    cls, jobject animation) 
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    return (jboolean) (gdk_pixbuf_animation_is_static_image (animation_g));
}

/*
 * Class:     org.gnu.gdk.PixbufAnimation
 * Method:    gdk_pixbuf_animation_get_static_image
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gdk_PixbufAnimation_gdk_1pixbuf_1animation_1get_1static_1image (JNIEnv *env, 
    jclass cls, jobject animation) 
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    return getGObjectHandle(env, 
    			G_OBJECT(gdk_pixbuf_animation_get_static_image (animation_g)));
}


#ifdef __cplusplus
}

#endif
