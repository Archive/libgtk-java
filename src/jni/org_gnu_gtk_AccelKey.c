/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AccelKey.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static guint32 GtkAccelKey_get_accel_key (GtkAccelKey * cptr) 
{
    return cptr->accel_key;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    getAccelKey
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1get_1key (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    return (jint) (GtkAccelKey_get_accel_key (cptr_g));
}

static void GtkAccelKey_set_accel_key (GtkAccelKey * cptr, guint32 accel_key) 
{
    cptr->accel_key = accel_key;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    setAccelKey
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1set_1key (JNIEnv *env, jclass klass, jobject cptr, jint 
    accel_key) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    guint32 accel_key_g = (guint32) accel_key;
    GtkAccelKey_set_accel_key (cptr_g, accel_key_g);
}

static GdkModifierType GtkAccelKey_get_accel_mods (GtkAccelKey * cptr) 
{
    return cptr->accel_mods;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    getAccelMods
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1get_1mods (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    return (jint) (GtkAccelKey_get_accel_mods (cptr_g));
}

static void GtkAccelKey_set_accel_mods (GtkAccelKey * cptr, GdkModifierType accel_mods) 
{
    cptr->accel_mods = accel_mods;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    setAccelMods
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1set_1mods (JNIEnv *env, jclass klass, jobject cptr, jint 
    accel_mods) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    GdkModifierType accel_mods_g = (GdkModifierType) accel_mods;
    GtkAccelKey_set_accel_mods (cptr_g, accel_mods_g);
}

static guint32 GtkAccelKey_get_accel_flags (GtkAccelKey * cptr) 
{
    return cptr->accel_flags;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    getAccelFlags
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1get_1flags (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    return (jint) (GtkAccelKey_get_accel_flags (cptr_g));
}

static void GtkAccelKey_set_accel_flags (GtkAccelKey * cptr, guint32 accel_flags) 
{
    cptr->accel_flags = accel_flags;
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    setAccelFlags
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1set_1flags (JNIEnv *env, jclass klass, jobject cptr, jint 
    accel_flags) 
{
    GtkAccelKey *cptr_g = (GtkAccelKey *)getPointerFromHandle(env, cptr);
    guint32 accel_flags_g = (guint32) accel_flags;
    GtkAccelKey_set_accel_flags (cptr_g, accel_flags_g);
}

/*
 * Class:     org.gnu.gtk.AccelKey
 * Method:    accel_key_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AccelKey_accel_1key_1new (
	JNIEnv *env, jobject cptr) 
{
	GtkAccelKey *key = (GtkAccelKey *) g_malloc(sizeof(GtkAccelKey));
	
	return getStructHandle(env, key, NULL, (JGFreeFunc) g_free);
}


#ifdef __cplusplus
}

#endif
