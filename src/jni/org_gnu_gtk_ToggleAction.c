/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToggleAction.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_toggle_action_get_type();
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_new
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1new(JNIEnv *env, jclass cls, jstring name, jstring label, jstring tooltip, jstring stockId)
{
    const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
    const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
    const gchar* t = (*env)->GetStringUTFChars(env, tooltip, NULL);
    const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
    jobject value = 
        getGObjectHandle(env, (GObject *) gtk_toggle_action_new(n, l, t, s));
    (*env)->ReleaseStringUTFChars(env, name, n);
    (*env)->ReleaseStringUTFChars(env, label, l);
    (*env)->ReleaseStringUTFChars(env, tooltip, t);
    (*env)->ReleaseStringUTFChars(env, stockId, s);
    return value;
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_toggled
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1toggled(JNIEnv *env, jclass cls, jobject action)
{
    gtk_toggle_action_toggled((GtkToggleAction*)action);
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_set_active
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1set_1active(JNIEnv *env, jclass cls, jobject action, jboolean active)
{
    GtkToggleAction *action_g = 
        (GtkToggleAction*)getPointerFromHandle(env, action);
    gboolean active_g = (gboolean)active;
    gtk_toggle_action_set_active(action_g, active_g);
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_get_active
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1get_1active(JNIEnv *env, jclass cls, jobject action)
{
    GtkToggleAction *action_g = 
        (GtkToggleAction*)getPointerFromHandle(env, action);
    return (gboolean)gtk_toggle_action_get_active(action_g);
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_set_draw_as_radio
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1set_1draw_1as_1radio(JNIEnv *env, jclass cls, jobject action, jboolean radio)
{
    GtkToggleAction *action_g = 
        (GtkToggleAction*)getPointerFromHandle(env, action);
    gboolean radio_g = (gboolean)radio;
    gtk_toggle_action_set_draw_as_radio(action_g, radio_g);
}

/*
 * Class:     org_gnu_gtk_ToggleAction
 * Method:    gtk_toggle_action_get_draw_as_radio
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToggleAction_gtk_1toggle_1action_1get_1draw_1as_1radio(JNIEnv *env, jclass cls, jobject action)
{
    GtkToggleAction *action_g = 
        (GtkToggleAction*)getPointerFromHandle(env, action);
    return (gboolean)gtk_toggle_action_get_draw_as_radio(action_g);
}

#ifdef __cplusplus
}
#endif
