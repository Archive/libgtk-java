/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Dialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkBox * GtkDialog_get_vbox (GtkDialog * cptr) 
{
    return (GtkBox*)cptr->vbox;
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    getVbox
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Dialog_getVbox (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkDialog *cptr_g = (GtkDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkDialog_get_vbox (cptr_g));
}

static GtkBox * GtkDialog_get_action_area (GtkDialog * cptr) 
{
    return (GtkBox*)cptr->action_area;
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    getActionArea
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Dialog_getActionArea (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkDialog *cptr_g = (GtkDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkDialog_get_action_area (cptr_g));
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_dialog_get_type ();
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_dialog_new ());
}


/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_add_action_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1add_1action_1widget (JNIEnv *env, 
    jclass cls, jobject dialog, jobject child, jint responseId) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gint32 responseId_g = (gint32) responseId;
    gtk_dialog_add_action_widget (dialog_g, child_g, responseId_g);
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_add_button
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1add_1button (JNIEnv *env, jclass 
    cls, jobject dialog, jstring buttonText, jint responseId) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gint32 responseId_g = (gint32) responseId;
    const gchar* buttonText_g = (*env)->GetStringUTFChars(env, buttonText, NULL);
	jobject retval =  getGObjectHandle(env, (GObject *) 
			gtk_dialog_add_button (dialog_g, buttonText_g, responseId_g));
	(*env)->ReleaseStringUTFChars( env, buttonText, buttonText_g );
	return retval;
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_set_response_sensitive
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1set_1response_1sensitive (JNIEnv 
    *env, jclass cls, jobject dialog, jint responseId, jboolean setting) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gint32 responseId_g = (gint32) responseId;
    gboolean setting_g = (gboolean) setting;
    gtk_dialog_set_response_sensitive (dialog_g, responseId_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_set_default_response
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1set_1default_1response (JNIEnv 
    *env, jclass cls, jobject dialog, jint responseId) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gint32 responseId_g = (gint32) responseId;
    gtk_dialog_set_default_response (dialog_g, responseId_g);
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_set_has_separator
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1set_1has_1separator (JNIEnv *env, 
    jclass cls, jobject dialog, jboolean setting) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gboolean setting_g = (gboolean) setting;
    gtk_dialog_set_has_separator (dialog_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_get_has_separator
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1get_1has_1separator (JNIEnv 
    *env, jclass cls, jobject dialog) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    return (jboolean) (gtk_dialog_get_has_separator (dialog_g));
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_response
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1response (JNIEnv *env, jclass cls, 
    jobject dialog, jint responseId) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    gint32 responseId_g = (gint32) responseId;
    gtk_dialog_response (dialog_g, responseId_g);
}

/*
 * Class:     org.gnu.gtk.Dialog
 * Method:    gtk_dialog_run
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1run
    (JNIEnv *env, jclass cls, jobject dialog) 
{
    GtkDialog *dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    return (jint) (gtk_dialog_run (dialog_g));
}

/*
 * Class:     org_gnu_gtk_Dialog
 * Method:    gtk_dialog_get_response_for_widget  * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)I
 */ 
 JNIEXPORT jint JNICALL Java_org_gnu_gtk_Dialog_gtk_1dialog_1get_1response_1for_1widget
    (JNIEnv *env, jclass cls, jobject dialog, jobject widget)
{
	GtkDialog *dialog_g;
	GtkWidget *widget_g;
	gint response;
	
    dialog_g = (GtkDialog *)getPointerFromHandle(env, dialog);
    widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
	response = gtk_dialog_get_response_for_widget(dialog_g, widget_g);
	return response;
}
 

#ifdef __cplusplus
}
#endif
