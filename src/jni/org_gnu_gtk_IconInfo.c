/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_IconInfo
#define _Included_org_gnu_gtk_IconInfo
#include "org_gnu_gtk_IconInfo.h"
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_icon_info_get_type();
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1free
  (JNIEnv *env, jclass cls, jobject info)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	gtk_icon_info_free(info_g);
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_base_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1base_1size
  (JNIEnv *env, jclass cls, jobject info)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	return (jint)gtk_icon_info_get_base_size(info_g);
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1filename
  (JNIEnv *env, jclass cls, jobject info)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	return (*env)->NewStringUTF(env, gtk_icon_info_get_filename(info_g));
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_builtin_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1builtin_1pixbuf
  (JNIEnv *env, jclass cls, jobject info)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	return getGObjectHandle(env, (GObject *) gtk_icon_info_get_builtin_pixbuf(info_g));
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_load_icon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1load_1icon
  (JNIEnv *env,  jclass cls, jobject info, jintArray error)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	GError* e = (GError*)(*env)->GetIntArrayElements(env, error, NULL);
	jobject value = getGObjectHandle(env, (GObject *) gtk_icon_info_load_icon(info_g, &e));
	(*env)->ReleaseIntArrayElements(env, error, (jint*)e, 0);
	return value;
}
	
/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_set_raw_coordinates
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1set_1raw_1coordinates
  (JNIEnv *env, jclass cls, jobject info, jboolean raw)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	gtk_icon_info_set_raw_coordinates(info_g, (gboolean)raw);
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_embedded_rect
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1embedded_1rect
  (JNIEnv *env, jclass cls, jobject info, jobject rect)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	GdkRectangle* rect_g = (GdkRectangle*)getPointerFromHandle(env, rect);
	return (jboolean)gtk_icon_info_get_embedded_rect(info_g, rect_g);
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_attach_points
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1attach_1points
  (JNIEnv *env, jclass cls, jobject info, jobjectArray points, jint numPoints)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	GdkPoint** point = g_malloc(sizeof(GdkPoint*) * (gint)numPoints);
	int index;
	
	for (index = 0; index < (gint)numPoints; index++)
	{
	  point[index] = (GdkPoint*)getPointerFromHandle(env, (*env)->GetObjectArrayElement(env, points, index));
	}
	return gtk_icon_info_get_attach_points(info_g, point, (gint *)&numPoints);
}

/*
 * Class:     org_gnu_gtk_IconInfo
 * Method:    gtk_icon_info_get_display_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconInfo_gtk_1icon_1info_1get_1display_1name
  (JNIEnv *env, jclass cls, jobject info)
{
	GtkIconInfo* info_g = (GtkIconInfo*)getPointerFromHandle(env, info);
	return (*env)->NewStringUTF(env, gtk_icon_info_get_display_name(info_g));
}

#ifdef __cplusplus
}
#endif
#endif
