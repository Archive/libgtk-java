/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_MenuItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * 
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_menu_item_get_type ();
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_menu_item_new ());
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1new_1with_1label (JNIEnv 
    *env, jclass cls, jstring label) 
{
    gchar* label_g = (gchar*)(*env)->GetStringUTFChars(env, label, 0);
    jobject result = getGObjectHandle(env, (GObject *) gtk_menu_item_new_with_label (label_g));
    (*env)->ReleaseStringUTFChars(env, label, label_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1new_1with_1mnemonic (JNIEnv 
    *env, jclass cls, jstring label) 
{
    gchar* label_g = (gchar*)(*env)->GetStringUTFChars(env, label, 0);
    jobject result = getGObjectHandle(env, (GObject *) gtk_menu_item_new_with_mnemonic (label_g));
    (*env)->ReleaseStringUTFChars(env, label, label_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_set_submenu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1set_1submenu (JNIEnv *env, 
    jclass cls, jobject menu_item, jobject submenu) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    GtkWidget *submenu_g = (GtkWidget *)getPointerFromHandle(env, submenu);
    gtk_menu_item_set_submenu (menu_item_g, submenu_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_get_submenu
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1get_1submenu (JNIEnv *env, 
    jclass cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    return getGObjectHandle(env, (GObject *) gtk_menu_item_get_submenu (menu_item_g));
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_remove_submenu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1remove_1submenu (JNIEnv *env, 
    jclass cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gtk_menu_item_remove_submenu (menu_item_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_select
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1select (JNIEnv *env, jclass 
    cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gtk_menu_item_select (menu_item_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_deselect
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1deselect (JNIEnv *env, jclass 
    cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gtk_menu_item_deselect (menu_item_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_activate
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1activate (JNIEnv *env, jclass 
    cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gtk_menu_item_activate (menu_item_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_toggle_size_request
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1toggle_1size_1request (JNIEnv 
    *env, jclass cls, jobject menu_item, jintArray requisition) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gint *requisition_g = (gint *) (*env)->GetIntArrayElements (env, requisition, NULL);
    gtk_menu_item_toggle_size_request (menu_item_g, requisition_g);
    (*env)->ReleaseIntArrayElements (env, requisition, (jint *) requisition_g, 0);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_toggle_size_allocate
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1toggle_1size_1allocate (
    JNIEnv *env, jclass cls, jobject menu_item, jint allocation) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gint32 allocation_g = (gint32) allocation;
    gtk_menu_item_toggle_size_allocate (menu_item_g, allocation_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_set_right_justified
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1set_1right_1justified (JNIEnv 
    *env, jclass cls, jobject menu_item, jboolean rightJustified) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gboolean rightJustified_g = (gboolean) rightJustified;
    gtk_menu_item_set_right_justified (menu_item_g, rightJustified_g);
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_get_right_justified
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1get_1right_1justified (
    JNIEnv *env, jclass cls, jobject menu_item) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    return (jboolean) (gtk_menu_item_get_right_justified (menu_item_g));
}

/*
 * Class:     org.gnu.gtk.MenuItem
 * Method:    gtk_menu_item_set_accel_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuItem_gtk_1menu_1item_1set_1accel_1path (JNIEnv 
    *env, jclass cls, jobject menu_item, jstring accelPath) 
{
    GtkMenuItem *menu_item_g = (GtkMenuItem *)getPointerFromHandle(env, menu_item);
    gchar* accelPath_g = (gchar*)(*env)->GetStringUTFChars(env, accelPath, 0);
    gtk_menu_item_set_accel_path (menu_item_g, accelPath_g);
    if (accelPath) 
    	(*env)->ReleaseStringUTFChars(env, accelPath, accelPath_g);
}


#ifdef __cplusplus
}

#endif
