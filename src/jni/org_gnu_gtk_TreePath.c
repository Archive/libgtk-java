/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreePath.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static jobject getTreePath(JNIEnv* env, GtkTreePath* path)
{
	return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH,
			NULL, (GBoxedFreeFunc) gtk_tree_path_free);	
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1new (JNIEnv *env, jclass cls) 
{
    return getTreePath(env, gtk_tree_path_new ());
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_new_from_string
 * Signature: ([B)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1new_1from_1string (JNIEnv *env, jclass cls, jstring path) 
{
    const gchar* path_g = (*env)->GetStringUTFChars( env, path, NULL );
    jobject retval = 
        getTreePath(env, gtk_tree_path_new_from_string (path_g));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return retval;
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_to_string
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1to_1string 
(JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (*env)->NewStringUTF( env, gtk_tree_path_to_string (path_g) );
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_new_first
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1new_1first (JNIEnv *env, jclass cls) 
{
    return getTreePath(env, gtk_tree_path_new_first ());
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_append_index
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1append_1index (JNIEnv *env, jclass cls, jobject path, jint index) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gint32 index_g = (gint32) index;
    gtk_tree_path_append_index (path_g, index_g);
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_prepend_index
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1prepend_1index (JNIEnv *env, jclass cls, jobject path, jint index) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gint32 index_g = (gint32) index;
    gtk_tree_path_prepend_index (path_g, index_g);
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_get_depth
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1get_1depth (JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jint) (gtk_tree_path_get_depth (path_g));
}

/*
 * Class:     org_gnu_gtk_TreePath
 * Method:    gtk_tree_path_get_indices
 * Signature: (I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1get_1indices(JNIEnv *env, jclass cls, jobject path)
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gint* list = gtk_tree_path_get_indices(path_g);
    gint len;
    jintArray ar;
    if (NULL == list)
        return NULL;
    len = gtk_tree_path_get_depth(path_g);
    ar = (*env)->NewIntArray(env, len);
    (*env)->SetIntArrayRegion(env, ar, 0, len, (jint*)list);
    return ar;
}
                                                                               
/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_compare
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1compare (JNIEnv *env, jclass cls, jobject a, jobject b) 
{
    GtkTreePath *a_g = (GtkTreePath *)getPointerFromHandle(env, a);
    GtkTreePath *b_g = (GtkTreePath *)getPointerFromHandle(env, b);
    return (jint) (gtk_tree_path_compare (a_g, b_g));
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_next
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1next (JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gtk_tree_path_next (path_g);
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_prev
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1prev (JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jboolean) (gtk_tree_path_prev (path_g));
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_up
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1up (JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jboolean) (gtk_tree_path_up (path_g));
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_down
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1down (JNIEnv *env, jclass cls, jobject path) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gtk_tree_path_down (path_g);
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_is_ancestor
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1is_1ancestor (JNIEnv *env, jclass cls, jobject path, jobject descendant) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreePath *descendant_g = 
        (GtkTreePath *)getPointerFromHandle(env, descendant);
    return (jboolean) (gtk_tree_path_is_ancestor (path_g, descendant_g));
}

/*
 * Class:     org.gnu.gtk.TreePath
 * Method:    gtk_tree_path_is_descendant
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreePath_gtk_1tree_1path_1is_1descendant (JNIEnv *env, jclass cls, jobject path, jobject ancestor) 
{
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreePath *ancestor_g = 
        (GtkTreePath *)getPointerFromHandle(env, ancestor);
    return (jboolean) (gtk_tree_path_is_descendant (path_g, ancestor_g));
}


#ifdef __cplusplus
}

#endif
