/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Span.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Span
 * Method:    gdk_span_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Span_gdk_1span_1new (JNIEnv *env, jclass cls, jint x, jint y, jint width) 
{
    GdkSpan *obj = g_new(GdkSpan, 1);
    obj->x = (gint)x;
    obj->y = (gint)y;
    obj->width = (gint)width;
    return getStructHandle(env, obj, NULL, g_free);
}

static gint32 GdkSpan_get_x (GdkSpan * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Span_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    return (jint) (GdkSpan_get_x (obj_g));
}

static void GdkSpan_set_x (GdkSpan * cptr, gint32 x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    setX
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Span_setX (JNIEnv *env, jclass klass, jobject obj, jint x) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    gint32 x_g = (gint32) x;
    GdkSpan_set_x (obj_g, x_g);
}

static gint32 GdkSpan_get_y (GdkSpan * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Span_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    return (jint) (GdkSpan_get_y (obj_g));
}

static void GdkSpan_set_y (GdkSpan * cptr, gint32 y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Span_setY (JNIEnv *env, jclass klass, jobject obj, jint y) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    gint32 y_g = (gint32) y;
    GdkSpan_set_y (obj_g, y_g);
}

static gint32 GdkSpan_get_width (GdkSpan * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Span_getWidth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    return (jint) (GdkSpan_get_width (obj_g));
}

static void GdkSpan_set_width (GdkSpan * cptr, gint32 width) 
{
    cptr->width = width;
}

/*
 * Class:     org.gnu.gdk.Span
 * Method:    setWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Span_setWidth (JNIEnv *env, jclass klass, jobject obj, jint width) 
{
    GdkSpan *obj_g = (GdkSpan *)getPointerFromHandle(env, obj);
    gint32 width_g = (gint32) width;
    GdkSpan_set_width (obj_g, width_g);
}


#ifdef __cplusplus
}

#endif
