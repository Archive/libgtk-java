/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Notebook.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_notebook_get_type ();
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_notebook_new ());
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_append_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1append_1page (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jobject tabLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    gtk_notebook_append_page (notebook_g, child_g, tabLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_append_page_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1append_1page_1menu (JNIEnv 
    *env, jclass cls, jobject notebook, jobject child, jobject tabLabel, jobject menuLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    GtkWidget *menuLabel_g = (GtkWidget *)getPointerFromHandle(env, menuLabel);
    gtk_notebook_append_page_menu (notebook_g, child_g, tabLabel_g, menuLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_prepend_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1prepend_1page (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jobject tabLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    gtk_notebook_prepend_page (notebook_g, child_g, tabLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_prepend_page_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1prepend_1page_1menu (JNIEnv 
    *env, jclass cls, jobject notebook, jobject child, jobject tabLabel, jobject menuLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    GtkWidget *menuLabel_g = (GtkWidget *)getPointerFromHandle(env, menuLabel);
    gtk_notebook_prepend_page_menu (notebook_g, child_g, tabLabel_g, menuLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_insert_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1insert_1page (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jobject tabLabel, jint position) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    gint32 position_g = (gint32) position;
    gtk_notebook_insert_page (notebook_g, child_g, tabLabel_g, position_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_insert_page_menu
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1insert_1page_1menu (JNIEnv 
    *env, jclass cls, jobject notebook, jobject child, jobject tabLabel, jobject menuLabel, jint position) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    GtkWidget *menuLabel_g = (GtkWidget *)getPointerFromHandle(env, menuLabel);
    gint32 position_g = (gint32) position;
    gtk_notebook_insert_page_menu (notebook_g, child_g, tabLabel_g, menuLabel_g, position_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_remove_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1remove_1page (JNIEnv *env, 
    jclass cls, jobject notebook, jint pageNum) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gint32 pageNum_g = (gint32) pageNum;
    gtk_notebook_remove_page (notebook_g, pageNum_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_current_page
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1current_1page (JNIEnv 
    *env, jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    return (jint) (gtk_notebook_get_current_page (notebook_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_nth_page
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1nth_1page (JNIEnv *env, 
    jclass cls, jobject notebook, jint pageNum) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gint32 pageNum_g = (gint32) pageNum;
    return getGObjectHandle(env, (GObject *)
    		gtk_notebook_get_nth_page (notebook_g, pageNum_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_page_num
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1page_1num (JNIEnv *env, jclass 
    cls, jobject notebook, jobject child) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    return (jint) (gtk_notebook_page_num (notebook_g, child_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_current_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1current_1page (JNIEnv 
    *env, jclass cls, jobject notebook, jint pageNum) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gint32 pageNum_g = (gint32) pageNum;
    gtk_notebook_set_current_page (notebook_g, pageNum_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_next_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1next_1page (JNIEnv *env, jclass 
    cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gtk_notebook_next_page (notebook_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_prev_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1prev_1page (JNIEnv *env, jclass 
    cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gtk_notebook_prev_page (notebook_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_show_border
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1show_1border (JNIEnv *env, 
    jclass cls, jobject notebook, jboolean showBorder) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gboolean showBorder_g = (gboolean) showBorder;
    gtk_notebook_set_show_border (notebook_g, showBorder_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_show_border
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1show_1border (JNIEnv 
    *env, jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    return (jboolean) (gtk_notebook_get_show_border (notebook_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_show_tabs
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1show_1tabs (JNIEnv *env, 
    jclass cls, jobject notebook, jboolean showTabs) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gboolean showTabs_g = (gboolean) showTabs;
    gtk_notebook_set_show_tabs (notebook_g, showTabs_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_show_tabs
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1show_1tabs (JNIEnv 
    *env, jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    return (jboolean) (gtk_notebook_get_show_tabs (notebook_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_tab_pos
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1tab_1pos (JNIEnv *env, 
    jclass cls, jobject notebook, jint pos) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkPositionType pos_g = (GtkPositionType) pos;
    gtk_notebook_set_tab_pos (notebook_g, pos_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_tab_pos
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1tab_1pos (JNIEnv *env, 
    jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    return (jint) (gtk_notebook_get_tab_pos (notebook_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_scrollable
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1scrollable (JNIEnv *env, 
    jclass cls, jobject notebook, jboolean scrollable) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gboolean scrollable_g = (gboolean) scrollable;
    gtk_notebook_set_scrollable (notebook_g, scrollable_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_scrollable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1scrollable (JNIEnv 
    *env, jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    return (jboolean) (gtk_notebook_get_scrollable (notebook_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_popup_enable
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1popup_1enable (JNIEnv *env, 
    jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gtk_notebook_popup_enable (notebook_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_popup_disable
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1popup_1disable (JNIEnv *env, 
    jclass cls, jobject notebook) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    gtk_notebook_popup_disable (notebook_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_tab_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1tab_1label (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    return getGObjectHandle(env, (GObject *) gtk_notebook_get_tab_label (notebook_g, child_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_tab_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1tab_1label (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jobject tabLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *tabLabel_g = (GtkWidget *)getPointerFromHandle(env, tabLabel);
    gtk_notebook_set_tab_label (notebook_g, child_g, tabLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_tab_label_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1tab_1label_1text (JNIEnv 
    *env, jclass cls, jobject notebook, jobject child, jstring tabText) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gchar* tabText_g = (gchar*)(*env)->GetStringUTFChars(env, tabText, 0);
    gtk_notebook_set_tab_label_text (notebook_g, child_g, tabText_g);
    (*env)->ReleaseStringUTFChars(env, tabText, tabText_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_tab_label_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1tab_1label_1text (
    JNIEnv *env, jclass cls, jobject notebook, jobject child) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gchar *result_g = (gchar*)gtk_notebook_get_tab_label_text (notebook_g, child_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_menu_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1menu_1label (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    return getGObjectHandle(env, (GObject *) gtk_notebook_get_menu_label (notebook_g, child_g));
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_menu_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1menu_1label (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jobject menuLabel) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkWidget *menuLabel_g = (GtkWidget *)getPointerFromHandle(env, menuLabel);
    gtk_notebook_set_menu_label (notebook_g, child_g, menuLabel_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_menu_label_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1menu_1label_1text (JNIEnv 
    *env, jclass cls, jobject notebook, jobject child, jstring menuText) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gchar* menuText_g = (gchar*)(*env)->GetStringUTFChars(env, menuText, 0);
    gtk_notebook_set_menu_label_text (notebook_g, child_g, menuText_g);
    (*env)->ReleaseStringUTFChars(env, menuText, menuText_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_get_menu_label_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1menu_1label_1text (
    JNIEnv *env, jclass cls, jobject notebook, jobject child) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gchar *result_g = (gchar*)gtk_notebook_get_menu_label_text(notebook_g, child_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_query_tab_label_packing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1query_1tab_1label_1packing (
    JNIEnv *env, jclass cls, jobject notebook, jobject child, jbooleanArray expand, jbooleanArray 
    fill, jintArray packType) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean *expand_g = (gboolean *) (*env)->GetBooleanArrayElements(env, expand, NULL);
    gboolean *fill_g = (gboolean *) (*env)->GetBooleanArrayElements(env, fill, NULL);
    gint *packType_g_g = (gint *) (*env)->GetIntArrayElements (env, packType, NULL);
    GtkPackType *packType_g = (GtkPackType *)packType_g_g;
    gtk_notebook_query_tab_label_packing (notebook_g, child_g, expand_g, fill_g, packType_g);
    (*env)->ReleaseBooleanArrayElements(env, expand, (jboolean *)expand_g, 0);
    (*env)->ReleaseBooleanArrayElements(env, fill, (jboolean *)fill_g, 0);
    (*env)->ReleaseIntArrayElements (env, packType, (jint*)packType_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_set_tab_label_packing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1set_1tab_1label_1packing (
    JNIEnv *env, jclass cls, jobject notebook, jobject child, jboolean expand, jboolean fill, jint 
    packType) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean expand_g = (gboolean) expand;
    gboolean fill_g = (gboolean) fill;
    GtkPackType packType_g = (GtkPackType) packType;
    gtk_notebook_set_tab_label_packing (notebook_g, child_g, expand_g, fill_g, packType_g);
}

/*
 * Class:     org.gnu.gtk.Notebook
 * Method:    gtk_notebook_reorder_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1reorder_1child (JNIEnv *env, 
    jclass cls, jobject notebook, jobject child, jint position) 
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gint32 position_g = (gint32) position;
    gtk_notebook_reorder_child (notebook_g, child_g, position_g);
}


 /*
 * Class:     org_gnu_gtk_Notebook
 * Method:    gtk_notebook_get_n_pages
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Notebook_gtk_1notebook_1get_1n_1pages
  (JNIEnv *env, jclass cls, jobject notebook)
{
    GtkNotebook *notebook_g = (GtkNotebook *)getPointerFromHandle(env, notebook);
	return (jint)gtk_notebook_get_n_pages(notebook_g);
}
                                                                                
     
 
#ifdef __cplusplus
}

#endif
