/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <stddef.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <jni.h>
#include <jg_jnu.h>
#include <glib_java.h>
#include "gtk_java.h"

/*
 * Memory management functions.
 * @since v2.8
 */

void initGObject(GObject *object, JGRef *ref)
{
    if (!GTK_IS_WINDOW(object))	{
    	g_object_add_toggle_ref (object, toggleNotify, ref);
    
    	if (GTK_IS_OBJECT (object))	{
        	gtk_object_sink ((GtkObject*) object);
    	}
    	else {
        	g_object_unref(object);
    	}
    }
}
