/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FileFilter
#define _Included_org_gnu_gtk_FileFilter
#include "org_gnu_gtk_FileFilter.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_file_filter_get_type();
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_file_filter_new());
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_set_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1set_1name
  (JNIEnv *env, jclass cls, jobject filter, jstring name)
{
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_file_filter_set_name(filter_g, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1get_1name
  (JNIEnv *env, jclass cls, jobject filter)
{
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	return (*env)->NewStringUTF(env, (char*)gtk_file_filter_get_name(filter_g));
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_add_mime_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1add_1mime_1type
  (JNIEnv *env, jclass cls, jobject filter, jstring mimeType)
{
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	const gchar* mt = (*env)->GetStringUTFChars(env, mimeType, NULL);
	gtk_file_filter_add_mime_type(filter_g, mt);
	(*env)->ReleaseStringUTFChars(env, mimeType, mt);
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_add_pattern
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1add_1pattern
  (JNIEnv *env, jclass cls, jobject filter, jstring pattern)
{
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	const gchar* p = (*env)->GetStringUTFChars(env, pattern, NULL);
	gtk_file_filter_add_pattern(filter_g, p);
	(*env)->ReleaseStringUTFChars(env, pattern, p);
}

/*
 * Class:     org_gnu_gtk_FileFilter
 * Method:    gtk_file_filter_get_needed
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1get_1needed
  (JNIEnv *env, jclass cls, jobject filter)
{
	GtkFileFilter* filter_g = (GtkFileFilter*)getPointerFromHandle(env, filter);
	return (jint)gtk_file_filter_get_needed(filter_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_FileFilter_gtk_1file_1filter_1add_1pixbuf_1formats
(JNIEnv *env, jclass cls, jobject filter)
{
    GtkFileFilter * filter_g = (GtkFileFilter *)getPointerFromHandle(env, filter);
    gtk_file_filter_add_pixbuf_formats(filter_g);
}


#ifdef __cplusplus
}
#endif
#endif
