/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Drawable.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1type 
	(JNIEnv *env, jclass cls) 
{
    return (jint)gdk_drawable_get_type ();
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1size 
	(JNIEnv *env, jclass cls, jobject drawable, jintArray width, jintArray height) 
{
    GdkDrawable *drawable_g;
    gint *width_g;
    gint *height_g;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    
    gdk_drawable_get_size (drawable_g, width_g, height_g);
    
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_set_colormap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1set_1colormap 
	(JNIEnv *env, jclass cls, jobject drawable, jobject colormap) 
{
    GdkDrawable *drawable_g;
    GdkColormap *colormap_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    
    gdk_drawable_set_colormap (drawable_g, colormap_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1colormap 
	(JNIEnv *env, jclass cls, jobject drawable) 
{
    GdkDrawable *drawable_g;
    GdkColormap *cm;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    cm = gdk_drawable_get_colormap (drawable_g);
    
    return getGObjectHandle(env, (GObject*)cm);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_visual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1visual 
	(JNIEnv *env, jclass cls, jobject drawable) 
{
    GdkDrawable *drawable_g;
    GdkVisual *vis;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    vis = gdk_drawable_get_visual (drawable_g);
    return getGObjectHandle(env, (GObject*)vis);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_depth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1depth 
	(JNIEnv *env, jclass cls, jobject drawable) 
{
    GdkDrawable *drawable_g;
    jint depth;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    depth = (jint)gdk_drawable_get_depth (drawable_g);
    
    return depth;
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jint x, jint y, jint width, jint height) 
{
    GdkDrawable *drawable_g;
    GdkImage *img;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    img = gdk_drawable_get_image (drawable_g, x, y, width, height);
    
    return getGObjectHandle(env, (GObject*)img);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_clip_region
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1clip_1region 
	(JNIEnv *env, jclass cls, jobject drawable) 
{
    GdkDrawable *drawable_g;
    GdkRegion *region;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    region = gdk_drawable_get_clip_region (drawable_g);
    
    return getStructHandle(env, region, NULL,
			   (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_drawable_get_visible_region
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1visible_1region 
	(JNIEnv *env, jclass cls, jobject drawable) 
{
    GdkDrawable *drawable_g;
    GdkRegion *region;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    region = gdk_drawable_get_visible_region (drawable_g);
    
    return getStructHandle(env, region, NULL,
			   (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_rgb_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1rgb_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jint width, jint height, jint dith, jbyteArray rgbBuf, jint rowstride) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbDither dith_g;
    jint rgbBuf_len;
    gchar* rgbBuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    rgbBuf_len = (*env)->GetArrayLength(env, rgbBuf);
    rgbBuf_g = (gchar*)g_malloc(rgbBuf_len + 1);
    (*env)->GetByteArrayRegion(env, rgbBuf, 0, rgbBuf_len, (jbyte*)rgbBuf_g);
    rgbBuf_g[rgbBuf_len] = 0;
    
    gdk_draw_rgb_image (drawable_g, gc_g, x, y, width, height, dith_g,
    		(guchar *)rgbBuf_g, rowstride);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_rgb_image_dithalign
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1rgb_1image_1dithalign 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jint width, jint height, jint dith, jbyteArray rgbBuf, jint rowstride, 
	jint xDith, jint yDith) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbDither dith_g;
    jint rgbBuf_len;
    gchar* rgbBuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    rgbBuf_len = (*env)->GetArrayLength(env, rgbBuf);
    rgbBuf_g = (gchar*)g_malloc(rgbBuf_len + 1);
    (*env)->GetByteArrayRegion(env, rgbBuf, 0, rgbBuf_len, (jbyte*)rgbBuf_g);
    rgbBuf_g[rgbBuf_len] = 0;

    gdk_draw_rgb_image_dithalign (drawable_g, gc_g, x, y, width, height, dith_g, 
            (guchar *) rgbBuf_g, rowstride, xDith, yDith);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_rgb_32_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1rgb_132_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jint width, jint height, jint dith, jbyteArray rgbBuf, jint rowstride) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbDither dith_g;
    jint rgbBuf_len;
    gchar* rgbBuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    rgbBuf_len = (*env)->GetArrayLength(env, rgbBuf);
    rgbBuf_g = (gchar*)g_malloc(rgbBuf_len + 1);
    (*env)->GetByteArrayRegion(env, rgbBuf, 0, rgbBuf_len, (jbyte*)rgbBuf_g);
    rgbBuf_g[rgbBuf_len] = 0;

    gdk_draw_rgb_32_image (drawable_g, gc_g, x, y, width, height, dith_g,
    		(guchar *) rgbBuf_g, rowstride);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_rgb_32_image_dithalign
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1rgb_132_1image_1dithalign 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, jint width, 
	jint height, jint dith, jbyteArray rgbBuf, jint rowstride, jint xDith, jint yDith) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbDither dith_g;
    jint rgbBuf_len;
    gchar* rgbBuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    rgbBuf_len = (*env)->GetArrayLength(env, rgbBuf);
    rgbBuf_g = (gchar*)g_malloc(rgbBuf_len + 1);
    (*env)->GetByteArrayRegion(env, rgbBuf, 0, rgbBuf_len, (jbyte*)rgbBuf_g);
    rgbBuf_g[rgbBuf_len] = 0;

    gdk_draw_rgb_32_image_dithalign (drawable_g, gc_g, x, y, width, height, dith_g, 
            (guchar *) rgbBuf_g, rowstride, xDith, yDith);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_gray_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1gray_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jint width, jint height, jint dith, jbyteArray rgbBuf, jint rowstride) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbDither dith_g;
    jint rgbBuf_len;
    gchar* rgbBuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    rgbBuf_len = (*env)->GetArrayLength(env, rgbBuf);
    rgbBuf_g = (gchar*)g_malloc(rgbBuf_len + 1);
    (*env)->GetByteArrayRegion(env, rgbBuf, 0, rgbBuf_len, (jbyte*)rgbBuf_g);
    rgbBuf_g[rgbBuf_len] = 0;

    gdk_draw_gray_image (drawable_g, gc_g, x, y, width, height, dith_g,
    		(guchar *) rgbBuf_g, rowstride);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_indexed_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1indexed_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jint width, jint height, jint dith, jbyteArray buf, jint rowstride, jobject cmap) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkRgbCmap *cmap_g;
    GdkRgbDither dith_g;
    jint buf_len;
    gchar* buf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    dith_g = (GdkRgbDither) dith;
    buf_len = (*env)->GetArrayLength(env, buf);
    buf_g = (gchar*)g_malloc(buf_len + 1);
    (*env)->GetByteArrayRegion(env, buf, 0, buf_len, (jbyte*)buf_g);
    buf_g[buf_len] = 0;
    cmap_g = (GdkRgbCmap *)getPointerFromHandle(env, cmap);

    gdk_draw_indexed_image (drawable_g, gc_g, x, y, width, height, dith_g,
    		(guchar *) buf_g, rowstride, cmap_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_drawable
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1drawable 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobject src, 
	jint xsrc, jint ysrc, jint xdest, jint ydest, jint width, jint height) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkDrawable *src_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    src_g = (GdkDrawable *)getPointerFromHandle(env, src);

    gdk_draw_drawable (drawable_g, gc_g, src_g, xsrc, ysrc, xdest, ydest, 
    	width, height);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1image 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobject image, 
	jint xsrc, jint ysrc, jint xdest, jint ydest, jint width, jint height) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkImage *image_g;
    
	drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
	gc_g = (GdkGC *)getPointerFromHandle(env, gc);
	image_g = (GdkImage *)getPointerFromHandle(env, image);
	
    gdk_draw_image (drawable_g, gc_g, image_g, xsrc, ysrc, xdest, ydest, 
    	width, height);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_points
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1points 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobjectArray points) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkPoint *points_g;
    gint32 numPoints;

	drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
	gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    points_g = (GdkPoint *)getPointerArrayFromHandles(env, points);
    numPoints = (gint32)(*env)->GetArrayLength(env, points);
    
    gdk_draw_points (drawable_g, gc_g, points_g, numPoints);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_segments
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1segments 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobjectArray segments) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkSegment *segments_g;
    gint32 numSegs;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    segments_g = (GdkSegment *)getPointerArrayFromHandles(env, segments);
    numSegs = (gint32)(*env)->GetArrayLength(env, segments);

    gdk_draw_segments (drawable_g, gc_g, segments_g, numSegs);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_glyphs
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1glyphs 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobject font, 
	jint x, jint y, jobject glyphs) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    PangoFont *font_g;
    PangoGlyphString *glyphs_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    font_g = (PangoFont *)getPointerFromHandle(env, font);
    glyphs_g = (PangoGlyphString *)getPointerFromHandle(env, glyphs);

    gdk_draw_glyphs (drawable_g, gc_g, font_g, x, y, glyphs_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_layout_line
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1layout_1line 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, 
	jint x, jint y, jobject line) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    PangoLayoutLine *line_g;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);

    gdk_draw_layout_line (drawable_g, gc_g, x, y, line_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_layout
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1layout 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, 
	jint x, jint y, jobject layout) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    PangoLayout *layout_g;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    layout_g = (PangoLayout *)getPointerFromHandle(env, layout);

    gdk_draw_layout (drawable_g, gc_g, x, y, layout_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_layout_line_with_colors
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1layout_1line_1with_1colors 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jobject line, jobject foreground, jobject background) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    PangoLayoutLine *line_g;
    GdkColor *foreground_g;
    GdkColor *background_g;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    line_g = (PangoLayoutLine *)getPointerFromHandle(env, line);
    foreground_g = (GdkColor *)getPointerFromHandle(env, foreground);
    background_g = (GdkColor *)getPointerFromHandle(env, background);

    gdk_draw_layout_line_with_colors (drawable_g, gc_g, x, y, line_g, foreground_g, 
            background_g);
}

/*
 * Class:     org.gnu.gdk.Drawable
 * Method:    gdk_draw_layout_with_colors
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1layout_1with_1colors 
	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y, 
	jobject layout, jobject foreground, jobject background) 
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    PangoLayout *layout_g;
    GdkColor *foreground_g;
    GdkColor *background_g;

    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    foreground_g = (GdkColor *)getPointerFromHandle(env, foreground);
    background_g = (GdkColor *)getPointerFromHandle(env, background);

    gdk_draw_layout_with_colors (drawable_g, gc_g, x, y, layout_g, foreground_g, 
            background_g);
}

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_drawable_get_display
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1display
  (JNIEnv *env, jclass cls, jobject drawable)
{
    GdkDrawable *drawable_g;
    GdkDisplay *display;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    display = gdk_drawable_get_display(drawable_g);
    
    return getGObjectHandle(env, (GObject*)display);
}

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_drawable_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Drawable_gdk_1drawable_1get_1screen
  (JNIEnv *env, jclass cls, jobject drawable)
{
    GdkDrawable *drawable_g;
    GdkScreen *screen;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    screen = gdk_drawable_get_screen(drawable_g);
    
    return getGObjectHandle(env, (GObject*)screen);
}

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_rectangle
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1rectangle
  (JNIEnv *env, jclass cls, jobject drawable, jobject gc, jboolean filled, 
  		jint x, jint y, jint width, jint height)
 {
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    
    gdk_draw_rectangle(drawable_g, gc_g, filled, x, y, width, height);
 }

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_arc
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1arc
  (JNIEnv *env, jclass cls, jobject drawable, jobject gc, jboolean filled, 
  		jint x, jint y, jint width, jint height, jint a1, jint a2)
 {
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    
    gdk_draw_arc(drawable_g, gc_g, filled, x, y, width, height, a1, a2);
 }

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_polygon
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1polygon
  	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jboolean filled, 
  	jobjectArray points)
 {
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    gint num;
    GdkPoint* points_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    num = (*env)->GetArrayLength(env, points);
    points_g = (GdkPoint*)getPointerArrayFromHandles(env, points);
    
    gdk_draw_polygon(drawable_g, gc_g, filled, points_g, num);
 }

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_line
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1line
  (JNIEnv *env, jclass cls, jobject drawable, jobject gc, 
  jint x1, jint y1, jint x2, jint y2)
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    
    gdk_draw_line(drawable_g, gc_g, x1, y1, x2, y2);
}
 
/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_lines
 */ 
 JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1lines
  (JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobjectArray points)
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    gint num;
    GdkPoint* points_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    num = (*env)->GetArrayLength(env, points);
    points_g = (GdkPoint*)getArrayFromHandles(env, points, 
    													sizeof(GdkPoint), 1, 1);
    gdk_draw_lines(drawable_g, gc_g, points_g, num);
}

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_pixbuf
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1pixbuf
  	(JNIEnv *env, jclass cls, jobject drawable, jobject gc, jobject pixbuf, jint srcx, jint srcy, 
  	jint destx, jint desty, jint width, jint height, jint dither, jint ditherx, jint dithery)
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    GdkPixbuf *pixbuf_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);

	gdk_draw_pixbuf(drawable_g, gc_g, pixbuf_g, srcx, srcy, destx, desty, width, height,
			(GdkRgbDither)dither, ditherx, dithery);
}

/*
 * Class:     org_gnu_gdk_Drawable
 * Method:    gdk_draw_point
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Drawable_gdk_1draw_1point
  (JNIEnv *env, jclass cls, jobject drawable, jobject gc, jint x, jint y)
{
    GdkDrawable *drawable_g;
    GdkGC *gc_g;
    
    drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
    gc_g = (GdkGC *)getPointerFromHandle(env, gc);
    
    gdk_draw_point(drawable_g, gc_g, x, y);
}


#ifdef __cplusplus
}

#endif
