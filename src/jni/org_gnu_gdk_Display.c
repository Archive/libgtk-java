/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <cairo.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Display.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gdk_display_get_type();
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_open
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1open
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar *n;
	GdkDisplay *dis;
	
	n = (*env)->GetStringUTFChars(env, name, NULL);
	dis = gdk_display_open(n);
	
	(*env)->ReleaseStringUTFChars(env, name, n);
	return getGObjectHandle(env, (GObject*)dis);
}


/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1name
  (JNIEnv *env, jclass cls, jobject display)
{
	const gchar* name;
	GdkDisplay *dis;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	name = gdk_display_get_name(dis);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_n_screens
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1n_1screens
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	gint numScreens;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	numScreens = gdk_display_get_n_screens(dis);
	return (jint)numScreens;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1screen
  (JNIEnv *env, jclass cls, jobject display, jint screenNum)
{
	GdkDisplay *dis;
	GdkScreen *screen;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	screen = gdk_display_get_screen(dis, screenNum);
	
	return getGObjectHandle(env, (GObject*)screen);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_default_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1default_1screen
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	GdkScreen *screen;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	screen = gdk_display_get_default_screen(dis);
	
	return getGObjectHandle(env, (GObject*)screen);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_pointer_ungrab
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1pointer_1ungrab
  (JNIEnv *env, jclass cls, jobject display, jint time)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_pointer_ungrab(dis, (guint32)time);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_keyboard_ungrab
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1keyboard_1ungrab
  (JNIEnv *env, jclass cls, jobject display, jint time)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_keyboard_ungrab(dis, (guint32)time);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_pointer_is_grabbed
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1pointer_1is_1grabbed
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result =  (jboolean)gdk_display_pointer_is_grabbed(dis);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_beep
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1beep
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_beep(dis);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_sync
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1sync
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_sync(dis);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_flush
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1flush
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_flush(dis);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_close
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1close
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_close(dis);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_list_devices
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Display_gdk_1display_1list_1devices
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	return getList(env,gdk_display_list_devices(dis));
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_event
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1event
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	GdkEvent *event;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	event = gdk_display_get_event(dis);
	return getGBoxedHandle(env, event, GDK_TYPE_EVENT, 
                               NULL, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_peek_event
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1peek_1event
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	GdkEvent *event;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	event = gdk_display_peek_event(dis);
	return getGBoxedHandle(env, event, GDK_TYPE_EVENT,
                               NULL, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_put_event
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1put_1event
  (JNIEnv *env, jclass cls, jobject display, jobject event)
{
	GdkDisplay *dis;
	GdkEvent *evt;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	evt = (GdkEvent*)getPointerFromHandle(env, event);
	gdk_display_put_event(dis, evt);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1default
  (JNIEnv *env,  jclass cls)
{
	GdkDisplay *dis;
	
	dis = gdk_display_get_default();
	return getGObjectHandle(env, (GObject*)dis);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_core_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1core_1pointer
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	GdkDevice *pointer;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	pointer = gdk_display_get_core_pointer(dis);
	
	return getGObjectHandle(env, (GObject*)pointer);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_pointer
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1pointer
  (JNIEnv *env, jclass cls, jobject display, jobject screen, 
  jintArray xVal, jintArray yVal, jintArray mask)
{
	GdkDisplay *dis;
	GdkScreen* scr;
	int* x;
	int* y;
	GdkModifierType* mod;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	scr = g_new(GdkScreen, 1);
	x = (int*)(*env)->GetIntArrayElements(env, xVal, NULL);
	y = (int*)(*env)->GetIntArrayElements(env, yVal, NULL);
	mod = (GdkModifierType*)(*env)->GetIntArrayElements(env, mask, NULL);
	gdk_display_get_pointer(dis, &scr, x, y, mod);
	(*env)->ReleaseIntArrayElements(env, xVal, (jint*)x, 0);
	(*env)->ReleaseIntArrayElements(env, yVal, (jint*)y, 0);
	(*env)->ReleaseIntArrayElements(env, mask, (jint*)mod, 0);
	screen = getGObjectHandle(env, (GObject*)scr);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_window_at_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1window_1at_1pointer
  (JNIEnv *env, jclass cls, jobject display, jint x, jint y)
{
	GdkDisplay *dis;
	GdkDevice *pointer;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	pointer = (GdkDevice *) gdk_display_get_window_at_pointer(dis, (gint *)&x, (gint *)&y);
	return getGObjectHandle(env, (GObject*)pointer);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_supports_cursor_alpha
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1supports_1cursor_1alpha
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result = (jboolean)gdk_display_supports_cursor_alpha(dis);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_supports_cursor_color
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1supports_1cursor_1color
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result = (jboolean)gdk_display_supports_cursor_color(dis);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_default_cursor_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1default_1cursor_1size
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jint result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result = (jint)gdk_display_get_default_cursor_size(dis);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_maximum_cursor_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1maximum_1cursor_1size
  (JNIEnv *env, jclass cls, jobject display, jintArray width, jintArray height)
{
	GdkDisplay *dis;
	guint *w, *h;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	w = (guint*)(*env)->GetIntArrayElements(env, width, NULL);
	h = (guint*)(*env)->GetIntArrayElements(env, height, NULL);
	
	gdk_display_get_maximal_cursor_size(dis, w, h);
	(*env)->ReleaseIntArrayElements(env, width, (jint*)w, 0);
	(*env)->ReleaseIntArrayElements(env, height, (jint*)h, 0);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_supports_selection_notification
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1supports_1selection_1notification
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result = (jboolean)gdk_display_supports_selection_notification(dis);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_request_selection_notification
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1request_1selection_1notification
  (JNIEnv *env, jclass cls, jobject display, jobject atom)
{
	GdkDisplay *dis;
	GdkAtom *atom_g;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	atom_g = (GdkAtom*)getPointerFromHandle(env, atom);
	result = (jboolean)gdk_display_request_selection_notification(dis, *atom_g);
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_supports_clipboard_persistence
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Display_gdk_1display_1supports_1clipboard_1persistence
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	jboolean result;

	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	result = (jboolean)gdk_display_supports_clipboard_persistence(dis);	
	return result;
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_store_clipboard
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;I[Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1store_1clipboard
  (JNIEnv *env, jclass cls, jobject display, jobject clipboardWindow, jint time, 
  jobjectArray targets)
{
	GdkDisplay *dis;
	GdkWindow* clipboardWindow_g;
	GdkAtom* targets_g;
	jint size;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	clipboardWindow_g = (GdkWindow*)getPointerFromHandle(env, clipboardWindow);
	targets_g = (GdkAtom*)getPointerArrayFromHandles(env, targets);
	size = (*env)->GetArrayLength(env, targets);
	gdk_display_store_clipboard(dis, clipboardWindow_g, time, targets_g, size);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_get_default_group
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Display_gdk_1display_1get_1default_1group
  (JNIEnv *env, jclass cls, jobject display)
{
	GdkDisplay *dis;
	GdkWindow* group;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	group = gdk_display_get_default_group(dis);
	return getGObjectHandle(env, (GObject*)group);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_set_double_click_time
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1set_1double_1click_1time
  (JNIEnv *env, jclass cls, jobject display, jint time)
{
	GdkDisplay *dis;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_set_double_click_time(dis, time);
}

/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_set_double_click_distance
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1set_1double_1click_1distance
  (JNIEnv *env, jclass cls, jobject display, jint distance)
{
	GdkDisplay *dis;
	
	dis = (GdkDisplay*)getPointerFromHandle(env, display);
	gdk_display_set_double_click_distance(dis, distance);
}


/*
 * Class:     org_gnu_gdk_Display
 * Method:    gdk_display_warp_pointer
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Display_gdk_1display_1warp_1pointer
  (JNIEnv *env, jclass cls, jobject display, jobject screen, jint x, jint y)
{
	GdkDisplay* display_g = (GdkDisplay*)getPointerFromHandle(env, display);
	GdkScreen *screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	gdk_display_warp_pointer(display_g, screen_g, x, y);	
}
  
#ifdef __cplusplus
}
#endif
