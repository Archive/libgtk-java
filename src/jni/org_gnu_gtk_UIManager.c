/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_UIManager.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_ui_manager_get_type();
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1new(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, (GObject *) gtk_ui_manager_new());
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_set_add_tearoffs
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1set_1add_1tearoffs(JNIEnv *env, jclass cls, jobject uim, jboolean value)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    gboolean value_g = (gboolean)value;
    gtk_ui_manager_set_add_tearoffs(uim_g, value_g);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_add_tearoffs
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1add_1tearoffs(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    return (jboolean)gtk_ui_manager_get_add_tearoffs(uim_g);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_insert_action_group
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1insert_1action_1group(JNIEnv *env, jclass cls, jobject uim, jobject group, jint pos)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    GtkActionGroup *group_g = 
        (GtkActionGroup *)getPointerFromHandle(env, group);
    gtk_ui_manager_insert_action_group(uim_g, group_g, (gint)pos);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_remove_action_group
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1remove_1action_1group(JNIEnv *env, jclass cls, jobject uim, jobject group)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    GtkActionGroup *group_g = 
        (GtkActionGroup *)getPointerFromHandle(env, group);
    gtk_ui_manager_remove_action_group(uim_g, group_g);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_action_groups
 * Signature: (I)[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1action_1groups(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    return getGObjectHandlesFromGList(env,gtk_ui_manager_get_action_groups(uim_g));
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_accel_group
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1accel_1group(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    return getGObjectHandle(env, (GObject *) gtk_ui_manager_get_accel_group(uim_g));
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_widget
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1widget(JNIEnv *env, jclass cls, jobject uim, jstring path)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
    jobject ret = getGObjectHandle(env, (GObject *) gtk_ui_manager_get_widget(uim_g, p));
    (*env)->ReleaseStringUTFChars(env, path, p);
    return ret;
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_toplevels
 * Signature: (II)[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1toplevels(JNIEnv *env, jclass cls, jobject uim, jint types)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    GtkUIManagerItemType types_g = (GtkUIManagerItemType)types;
    return getGObjectHandlesFromGSList(env,gtk_ui_manager_get_toplevels(uim_g, types_g));
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_action
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1action(JNIEnv *env, jclass cls, jobject uim, jstring path)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
    jobject ret = getGObjectHandle(env, (GObject *) gtk_ui_manager_get_action(uim_g, p));
    (*env)->ReleaseStringUTFChars(env, path, p);
    return ret;
}
/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_add_ui_from_string
 * Signature: (ILjava/lang/String;I[I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1add_1ui_1from_1string(JNIEnv *env, jclass cls, jobject uim, jstring buffer, jint len)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    const gchar* b = (*env)->GetStringUTFChars(env, buffer, NULL);
    GError* err = NULL;
    jint ret = (jint)gtk_ui_manager_add_ui_from_string(uim_g, b, (gint)len, &err);
    (*env)->ReleaseStringUTFChars(env, buffer, b);
    if (err != NULL)
	    g_free( err );
    return ret;
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_add_ui_from_file
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1add_1ui_1from_1file(JNIEnv *env, jclass cls, jobject uim, jstring filename)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    const gchar* f = (*env)->GetStringUTFChars(env, filename, NULL);
    GError* err = NULL;
    jint ret = (jint)gtk_ui_manager_add_ui_from_file(uim_g, f, &err);
    (*env)->ReleaseStringUTFChars(env, filename, f);
    if (NULL != err)
	    g_free( err );
    return ret;
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_add_ui
 * Signature: (IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1add_1ui(JNIEnv *env, jclass cls, jobject uim, jint id, jstring path, jstring name, jstring action, jint type, jboolean top)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    guint id_g = (guint)id;
    const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
    const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
    const gchar* a = (*env)->GetStringUTFChars(env, action, NULL);
    GtkUIManagerItemType type_g = (GtkUIManagerItemType)type;
    gboolean top_g = (gboolean)top;

    gtk_ui_manager_add_ui(uim_g, id_g, p, n, a, type_g, top_g);
    (*env)->ReleaseStringUTFChars(env, path, p);
    (*env)->ReleaseStringUTFChars(env, name, n);
    (*env)->ReleaseStringUTFChars(env, action, a);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_remove_ui
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1remove_1ui(JNIEnv *env, jclass cls, jobject uim, jint id)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    guint id_g = (guint)id;
    gtk_ui_manager_remove_ui(uim_g, id_g);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_get_ui
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1get_1ui(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    gchar* ui = gtk_ui_manager_get_ui(uim_g);
    return (*env)->NewStringUTF(env, ui);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_ensure_update
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1ensure_1update(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    gtk_ui_manager_ensure_update(uim_g);
}

/*
 * Class:     org_gnu_gtk_UIManager
 * Method:    gtk_ui_manager_new_merge_id
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_UIManager_gtk_1ui_1manager_1new_1merge_1id(JNIEnv *env, jclass cls, jobject uim)
{
    GtkUIManager *uim_g = (GtkUIManager *)getPointerFromHandle(env, uim);
    return (jint)gtk_ui_manager_new_merge_id(uim_g);
}

#ifdef __cplusplus
}
#endif
