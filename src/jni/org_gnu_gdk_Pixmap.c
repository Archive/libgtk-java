/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Pixmap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Pixmap
 * Method:    gdk_pixmap_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_pixmap_get_type ();
}

/*
 * Class:     org.gnu.gdk.Pixmap
 * Method:    gdk_pixmap_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1new (JNIEnv *env, jclass cls, jobject 
    window, jint width, jint height, jint depth) 
{
    GdkDrawable *window_g = (GdkDrawable *)getPointerFromHandle(env, window);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gint32 depth_g = (gint32) depth;
    return getGObjectHandle(env, 
                            G_OBJECT(gdk_pixmap_new (window_g, width_g, 
						     height_g, depth_g)));
}

/*
 * Class:     org.gnu.gdk.Pixmap
 * Method:    gdk_pixmap_create_from_data
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1create_1from_1data (JNIEnv *env, 
    jclass cls, jobject window, jbyteArray data, jint width, jint height, jint depth, jobject fg, 
    jobject bg) 
{
    GdkDrawable *window_g = (GdkDrawable *)getPointerFromHandle(env, window);
    jint data_len = (*env)->GetArrayLength(env, data);
    gchar* data_g = (gchar*)g_malloc(data_len + 1);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gint32 depth_g = (gint32) depth;
    GdkColor *fg_g = (GdkColor *)getPointerFromHandle(env, fg);
    GdkColor *bg_g = (GdkColor *)getPointerFromHandle(env, bg);
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
    return getGObjectHandle(env, 
                            G_OBJECT(gdk_pixmap_create_from_data (window_g, data_g, 
                                                         width_g, height_g, 
                                                         depth_g, fg_g, bg_g)));
}

/*
 * Class:     org.gnu.gdk.Pixmap
 * Method:    gdk_pixmap_create_from_xpm
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1create_1from_1xpm (JNIEnv *env, 
    jclass cls, jobject window, jobject mask, jobject transparentColor, jstring filename) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    GdkDrawable* window_g = (GdkDrawable*)getPointerFromHandle(env, window);
    GdkBitmap** mask_g = (GdkBitmap**)getPointerFromHandle(env, mask);
    GdkColor* transparentColor_g = 
        (GdkColor*)getPointerFromHandle(env, transparentColor);
    jobject result = 
        getGObjectHandle(env, 
                         G_OBJECT(gdk_pixmap_create_from_xpm (window_g, mask_g, 
                                                     transparentColor_g, 
                                                     filename_g)));
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result;
}

///*
// * Class:     org.gnu.gdk.Pixmap
// * Method:    gdk_pixmap_colormap_create_from_xpm
// * Signature: (IIIIjava.lang.String;)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1colormap_1create_1from_1xpm (JNIEnv 
//    *env, jclass cls, jint window, jint colormap, jint mask, jint transparentColor, jstring filename) 
//{
//    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
//    jint result = (jint)gdk_pixmap_colormap_create_from_xpm ((GdkDrawable*)window, 
//							 (GdkColormap*)colormap, 
//							 (GdkBitmap**)mask, 
//							 (GdkColor*)transparentColor, 
//							 filename_g);
//(*env)->ReleaseStringUTFChars(env, filename, filename_g);
//return result;
//}
//
///*
// * Class:     org.gnu.gdk.Pixmap
// * Method:    gdk_pixmap_create_from_xpm_d
// * Signature: (III[B)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1create_1from_1xpm_1d (JNIEnv *env, 
//    jclass cls, jint window, jint mask, jint transparentColor, jbyteArray data) 
//{
//    GdkDrawable *window_g = (GdkDrawable *)window;
//    GdkBitmap **mask_g = (GdkBitmap **)mask;
//    GdkColor *transparentColor_g = (GdkColor *)transparentColor;
//    jint data_len = (*env)->GetArrayLength(env, data);
//    gchar* data_g = (gchar*)g_malloc(data_len + 1);
//    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
//    data_g[data_len] = 0;
//return (jint)gdk_pixmap_create_from_xpm_d (window_g, mask_g, transparentColor_g, data_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixmap
// * Method:    gdk_pixmap_colormap_create_from_xpm_d
// * Signature: (IIII[B)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1colormap_1create_1from_1xpm_1d (
//    JNIEnv *env, jclass cls, jint window, jint colormap, jint mask, jint transparentColor, 
//    jbyteArray data) 
//{
//    GdkDrawable *window_g = (GdkDrawable *)window;
//    GdkColormap *colormap_g = (GdkColormap *)colormap;
//    GdkBitmap **mask_g = (GdkBitmap **)mask;
//    GdkColor *transparentColor_g = (GdkColor *)transparentColor;
//    jint data_len = (*env)->GetArrayLength(env, data);
//    gchar* data_g = (gchar*)g_malloc(data_len + 1);
//    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
//    data_g[data_len] = 0;
//return (jint)gdk_pixmap_colormap_create_from_xpm_d (window_g, colormap_g, mask_g, 
//						    transparentColor_g, data_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixmap
// * Method:    gdk_pixmap_foreign_new
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixmap_gdk_1pixmap_1foreign_1new (JNIEnv *env, jclass 
//    cls, jint anid) 
//{
//    GdkNativeWindow anid_g = (GdkNativeWindow)&anid;
//    {
//        return (jint)gdk_pixmap_foreign_new (anid_g);
//    }
//}


#ifdef __cplusplus
}

#endif
