/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Rectangle.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Rectangle
 * Method:    pango_rectangle_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Rectangle_pango_1rectangle_1new (JNIEnv *env, jclass cls, jint x, jint y, jint width, jint height) 
{
    PangoRectangle *obj = g_new(PangoRectangle, 1);
    obj->x = x;
    obj->y = y;
    obj->width = width;
    obj->height = height;
    return getStructHandle(env, obj, NULL, g_free);
}

/*
 * Class:     org.gnu.pango.Rectangle
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Rectangle_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoRectangle *obj_g = (PangoRectangle *)getPointerFromHandle(env, obj);
    return (jint)obj_g->x;
}

/*
 * Class:     org.gnu.pango.Rectangle
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Rectangle_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoRectangle *obj_g = (PangoRectangle *)getPointerFromHandle(env, obj);
    return (jint)obj_g->y;
}

/*
 * Class:     org.gnu.pango.Rectangle
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Rectangle_getWidth (JNIEnv *env, jclass cls, jobject obj)
{
    PangoRectangle *obj_g = (PangoRectangle *)getPointerFromHandle(env, obj);
    return (jint)obj_g->width;
}

/*
 * Class:     org.gnu.pango.Rectangle
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Rectangle_getHeight (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoRectangle *obj_g = (PangoRectangle *)getPointerFromHandle(env, obj);
    return (jint)obj_g->height;
}


#ifdef __cplusplus
}

#endif
