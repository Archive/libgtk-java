/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Ruler.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GtkRuler_get_xsrc (GtkRuler * cptr) 
{
    return cptr->xsrc;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getXsrc
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Ruler_getXsrc (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jint) (GtkRuler_get_xsrc (cptr_g));
}

static gint32 GtkRuler_get_ysrc (GtkRuler * cptr) 
{
    return cptr->ysrc;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getYsrc
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Ruler_getYsrc (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jint) (GtkRuler_get_ysrc (cptr_g));
}

static gint32 GtkRuler_get_slider_size (GtkRuler * cptr) 
{
    return cptr->slider_size;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getSliderSize
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Ruler_getSliderSize (JNIEnv *env, jclass cls, jobject cptr)
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jint) (GtkRuler_get_slider_size (cptr_g));
}

static gdouble GtkRuler_get_lower (GtkRuler * cptr) 
{
    return cptr->lower;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getLower
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Ruler_getLower (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkRuler_get_lower (cptr_g));
}

static gdouble GtkRuler_get_upper (GtkRuler * cptr) 
{
    return cptr->upper;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getUpper
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Ruler_getUpper (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkRuler_get_upper (cptr_g));
}

static gdouble GtkRuler_get_position (GtkRuler * cptr) 
{
    return cptr->position;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getPosition
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Ruler_getPosition (JNIEnv *env, jclass cls, jobject cptr) 
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkRuler_get_position (cptr_g));
}

static gdouble GtkRuler_get_max_size (GtkRuler * cptr) 
{
    return cptr->max_size;
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    getMaxSize
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Ruler_getMaxSize (JNIEnv *env, jclass cls, jobject cptr)
{
    GtkRuler *cptr_g = (GtkRuler *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkRuler_get_max_size (cptr_g));
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_ruler_get_type ();
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_set_metric
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1set_1metric (JNIEnv *env, jclass cls, 
    jobject ruler, jint metric) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    GtkMetricType metric_g = (GtkMetricType) metric;
    gtk_ruler_set_metric (ruler_g, metric_g);
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_set_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1set_1range (JNIEnv *env, jclass cls, 
    jobject ruler, jdouble lower, jdouble upper, jdouble position, jdouble maxSize) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    gdouble lower_g = (gdouble) lower;
    gdouble upper_g = (gdouble) upper;
    gdouble position_g = (gdouble) position;
    gdouble maxSize_g = (gdouble) maxSize;
    gtk_ruler_set_range (ruler_g, lower_g, upper_g, position_g, maxSize_g);
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_draw_ticks
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1draw_1ticks (JNIEnv *env, jclass cls, 
    jobject ruler) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    gtk_ruler_draw_ticks (ruler_g);
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_draw_pos
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1draw_1pos (JNIEnv *env, jclass cls, 
    jobject ruler) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    gtk_ruler_draw_pos (ruler_g);
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_get_metric
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1get_1metric (JNIEnv *env, jclass cls, 
    jobject ruler) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    return (jint) (gtk_ruler_get_metric (ruler_g));
}

/*
 * Class:     org.gnu.gtk.Ruler
 * Method:    gtk_ruler_get_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Ruler_gtk_1ruler_1get_1range (JNIEnv *env, jclass cls, 
    jobject ruler, jdoubleArray lower, jdoubleArray upper, jdoubleArray position, jdoubleArray 
    maxSize) 
{
    GtkRuler *ruler_g = (GtkRuler *)getPointerFromHandle(env, ruler);
    gdouble *lower_g = (gdouble *) (*env)->GetDoubleArrayElements (env, lower, NULL);
    gdouble *upper_g = (gdouble *) (*env)->GetDoubleArrayElements (env, upper, NULL);
    gdouble *position_g = (gdouble *) (*env)->GetDoubleArrayElements (env, position, NULL);
    gdouble *maxSize_g = (gdouble *) (*env)->GetDoubleArrayElements (env, maxSize, NULL);
    gtk_ruler_get_range (ruler_g, lower_g, upper_g, position_g, maxSize_g);
    (*env)->ReleaseDoubleArrayElements (env, lower, (jdouble *) lower_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, upper, (jdouble *) upper_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, position, (jdouble *) position_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, maxSize, (jdouble *) maxSize_g, 0);
}


#ifdef __cplusplus
}

#endif
