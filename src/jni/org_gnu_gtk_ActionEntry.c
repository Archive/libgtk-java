/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"
#include "handleCallbackAction.h"
#ifndef _Included_org_gnu_gtk_ActionEntry
#define _Included_org_gnu_gtk_ActionEntry
#include "org_gnu_gtk_ActionEntry.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    allocate
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ActionEntry_allocate
  (JNIEnv *env, jclass cls)
{
	GtkActionEntry* entry = (GtkActionEntry*)g_malloc(sizeof(GtkActionEntry));
	entry->callback = G_CALLBACK(handleCallbackAction);
	
	return getStructHandle(env, entry, NULL, g_free);
}

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    setName
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionEntry_setName
  (JNIEnv *env, jclass cls, jobject entry, jstring name)
{
	GtkActionEntry* e = (GtkActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == name) {
		e->name = NULL;
	} else {
		gchar* n = (gchar*)(*env)->GetStringUTFChars(env, name, NULL);
		e->name = n;
	}
}

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    setStockId
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionEntry_setStockId
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkActionEntry* e = (GtkActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		e->stock_id = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		e->stock_id = v;
	}
}

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    setLabel
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionEntry_setLabel
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkActionEntry* e = (GtkActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		e->label = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		e->label = v;
	}
}

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    setAccelerator
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionEntry_setAccelerator
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkActionEntry* e = (GtkActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		e->accelerator = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		e->accelerator = v;
	}
}

/*
 * Class:     org_gnu_gtk_ActionEntry
 * Method:    setToolTtip
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ActionEntry_setToolTip
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkActionEntry* e = (GtkActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		e->tooltip = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		e->tooltip = v;
	}
}


#ifdef __cplusplus
}
#endif
#endif
