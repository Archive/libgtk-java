/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FileChooserWidget
#define _Included_org_gnu_gtk_FileChooserWidget
#include "org_gnu_gtk_FileChooserWidget.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_FileChooserWidget
 * Method:    gtk_file_chooser_widget_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserWidget_gtk_1file_1chooser_1widget_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_file_chooser_widget_get_type();
}

/*
 * Class:     org_gnu_gtk_FileChooserWidget
 * Method:    gtk_file_chooser_widget_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserWidget_gtk_1file_1chooser_1widget_1new
  (JNIEnv *env, jclass cls, jint action)
{
	return getGObjectHandle(env, (GObject *) gtk_file_chooser_widget_new(
			(GtkFileChooserAction)action));
}

#ifdef __cplusplus
}
#endif
#endif
