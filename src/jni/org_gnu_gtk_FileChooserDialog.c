/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FileChooserDialog
#define _Included_org_gnu_gtk_FileChooserDialog
#include "org_gnu_gtk_FileChooserDialog.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_FileChooserDialog
 * Method:    gtk_file_chooser_dialog_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileChooserDialog_gtk_1file_1chooser_1dialog_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_file_chooser_dialog_get_type();
}

/*
 * Class:     org_gnu_gtk_FileChooserDialog
 * Method:    gtk_file_chooser_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileChooserDialog_gtk_1file_1chooser_1dialog_1new
  (JNIEnv *env, jclass cls, jstring title, jobject parent, jint action)
{
	const gchar* t = (*env)->GetStringUTFChars(env, title, NULL);
	GtkWindow* parent_g = (GtkWindow*)getPointerFromHandle(env, parent);
	jobject handle = getGObjectHandle(env, (GObject *)
			gtk_file_chooser_dialog_new(t, parent_g, (GtkFileChooserAction)action,
                     NULL /* first button text */, NULL));
	(*env)->ReleaseStringUTFChars(env, title, t);
	return handle;
}

#ifdef __cplusplus
}
#endif
#endif
