/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Point.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Point
 * Method:    gdk_point_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Point_gdk_1point_1new
	(JNIEnv *env, jclass cls) 
{
    GdkPoint *obj_g = (GdkPoint *)g_malloc(sizeof(GdkPoint));
    return getStructHandle(env, obj_g, NULL, g_free);
}

/*
 * Class:     org.gnu.gdk.Point
 * Method:    gdk_point_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Point_gdk_1point_1free
	(JNIEnv *env, jclass cls, jobject obj) 
{
	GdkPoint *obj_g = (GdkPoint *)getPointerFromHandle(env, obj);
	g_free(obj_g);
}


static gint32 GdkPoint_get_x (GdkPoint * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gdk.Point
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Point_getX (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkPoint *obj_g = (GdkPoint *)getPointerFromHandle(env, obj);
    return (jint) (GdkPoint_get_x (obj_g));
}

static void GdkPoint_set_x (GdkPoint * cptr, gint32 x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gdk.Point
 * Method:    setX
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Point_setX (JNIEnv *env, jclass cls, jobject obj, jint x) 
{
	GdkPoint *obj_g = (GdkPoint *)getPointerFromHandle(env, obj);
    gint32 x_g = (gint32) x;
    GdkPoint_set_x (obj_g, x_g);
}

static gint32 GdkPoint_get_y (GdkPoint * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gdk.Point
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Point_getY (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkPoint *obj_g = (GdkPoint *)getPointerFromHandle(env, obj);
    return (jint) (GdkPoint_get_y (obj_g));
}

static void GdkPoint_set_y (GdkPoint * cptr, gint32 y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gdk.Point
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Point_setY (JNIEnv *env, jclass cls, jobject obj, jint y) 
{
	GdkPoint *obj_g = (GdkPoint *)getPointerFromHandle(env, obj);
    gint32 y_g = (gint32) y;
    GdkPoint_set_y (obj_g, y_g);
}

#ifdef __cplusplus
}

#endif
