/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <cairo.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Color.h"
#ifdef __cplusplus
extern "C" 
{
#endif


JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Color_allocHandle 
	(JNIEnv *env, jclass cls) 
{
    GdkColor* handle = g_new(GdkColor, 1);
    return getGBoxedHandle(env, handle, gdk_color_get_type(), NULL, 
		    (GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    getPixel
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_getPixel 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->pixel;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    getRed
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_getRed 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->red;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    setRed
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Color_setRed 
	(JNIEnv *env, jclass cls, jobject obj, jint red) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    obj_g->red = (guint16)red;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    getGreen
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_getGreen 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->green;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    setGreen
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Color_setGreen 
	(JNIEnv *env, jclass cls, jobject obj, jint green) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    obj_g->green = (guint16)green;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    getBlue
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_getBlue 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->blue;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    setBlue
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Color_setBlue 
	(JNIEnv *env, jclass cls, jobject obj, jint blue) 
{
    GdkColor *obj_g = (GdkColor *)getPointerFromHandle(env, obj);
    obj_g->blue = (guint16)blue;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    gdk_color_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_gdk_1color_1get_1type 
	(JNIEnv *env, jclass cls) 
{
    return (jint)gdk_color_get_type ();
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    gdk_color_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Color_gdk_1color_1copy 
	(JNIEnv *env, jclass cls, jobject color) 
{
    GdkColor *color_g;
    color_g = (GdkColor *)getPointerFromHandle(env, color);
    
    return getGBoxedHandle(env, color_g, gdk_color_get_type(),
    		(GBoxedCopyFunc) gdk_color_copy, (GBoxedFreeFunc) gdk_color_free);
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    gdk_color_parse
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Color_gdk_1color_1parse 
(JNIEnv *env, jclass cls, jstring spec)
{
    const gchar* spec_g;
    GdkColor *color_g;
    jint result;
    
    spec_g = (*env)->GetStringUTFChars(env, spec, 0);
    color_g = g_new(GdkColor, 1);
    result = (jint) (gdk_color_parse (spec_g, color_g));
    
    (*env)->ReleaseStringUTFChars(env, spec, spec_g);
    if (result != 0) {
        return getGBoxedHandle(env, color_g, gdk_color_get_type(), NULL,
                               (GBoxedFreeFunc)g_free);
    } else {
        return NULL;
    }
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    gdk_color_hash
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Color_gdk_1color_1hash 
	(JNIEnv *env, jclass cls, jobject color) 
{
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    jint result_j = (jint) (gdk_color_hash (color_g));
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Color
 * Method:    gdk_color_equal
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Color_gdk_1color_1equal 
	(JNIEnv *env, jclass cls, jobject colora, jobject colorb) 
{
    GdkColor *colora_g = (GdkColor *)getPointerFromHandle(env, colora);
    GdkColor *colorb_g = (GdkColor *)getPointerFromHandle(env, colorb);
    return (jboolean) (gdk_color_equal (colora_g, colorb_g));
}


#ifdef __cplusplus
}

#endif
