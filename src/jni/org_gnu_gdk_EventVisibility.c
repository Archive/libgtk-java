/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventVisibility.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventVisibility
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventVisibility_getWindow (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventVisibility *obj_g = (GdkEventVisibility *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventVisibility
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventVisibility_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventVisibility *obj_g = (GdkEventVisibility *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventVisibility
 * Method:    getState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventVisibility_getState (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventVisibility *obj_g = (GdkEventVisibility *)getPointerFromHandle(env, obj);
    return (jint) obj_g->state;
}


#ifdef __cplusplus
}

#endif
