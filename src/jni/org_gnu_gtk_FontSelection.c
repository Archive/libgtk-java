/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_FontSelection.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_font_selection_get_type ();
}

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_font_selection_new ());
}

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_get_font_name
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1get_1font_1name (JNIEnv *env, jclass cls, 
    jobject fontsel) 
{
	GtkFontSelection* fontsel_g = (GtkFontSelection*)getPointerFromHandle(env, fontsel);
    gchar *result_g = gtk_font_selection_get_font_name (fontsel_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_set_font_name
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1set_1font_1name (
    JNIEnv *env, jclass cls, jobject fontsel, jstring fontname) 
{
	GtkFontSelection* fontsel_g = (GtkFontSelection*)getPointerFromHandle(env, fontsel);
    gchar* fontname_g = (gchar*)(*env)->GetStringUTFChars(env, fontname, 0);
    jboolean result_j = (jboolean)gtk_font_selection_set_font_name (fontsel_g, fontname_g);
    (*env)->ReleaseStringUTFChars(env, fontname, fontname_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_get_preview_text
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1get_1preview_1text (JNIEnv *env, jclass 
    cls, jobject fontsel) 
{
	GtkFontSelection* fontsel_g = (GtkFontSelection*)getPointerFromHandle(env, fontsel);
    gchar *result_g = (gchar*)gtk_font_selection_get_preview_text (fontsel_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.FontSelection
 * Method:    gtk_font_selection_set_preview_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontSelection_gtk_1font_1selection_1set_1preview_1text (
    JNIEnv *env, jclass cls, jobject fontsel, jstring text) 
{
	GtkFontSelection* fontsel_g = (GtkFontSelection*)getPointerFromHandle(env, fontsel);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gtk_font_selection_set_preview_text (fontsel_g, text_g);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
}

#ifdef __cplusplus
}

#endif
