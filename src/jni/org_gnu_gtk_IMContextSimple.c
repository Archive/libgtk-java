/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IMContextSimple.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.IMContextSimple
 * Method:    gtk_im_context_simple_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IMContextSimple_gtk_1im_1context_1simple_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gtk_im_context_simple_get_type ();
}

/*
 * Class:     org.gnu.gtk.IMContextSimple
 * Method:    gtk_im_context_simple_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IMContextSimple_gtk_1im_1context_1simple_1new (JNIEnv 
    *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_im_context_simple_new ());
}

  // Commented out since this makes no sense (jobject to gpointer == VERY BAD). Not even sure what it does
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.IMContextSimple
 * Method:    gtk_im_context_simple_add_table
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContextSimple_gtk_1im_1context_1simple_1add_1table (
    JNIEnv *env, jclass cls, jobject context_simple, jobject data, jint maxSeqLen, jint numSeqs) 
{
    GtkIMContextSimple *context_simple_g = getPointerFromHandle(env, context_simple);
    gpointer *data_g = (gpointer *)data;
    gint32 maxSeqLen_g = (gint32) maxSeqLen;
    gint32 numSeqs_g = (gint32) numSeqs;
    gtk_im_context_simple_add_table (context_simple_g, (guint16*)data_g, maxSeqLen_g, numSeqs_g);
}
#endif


#ifdef __cplusplus
}

#endif
