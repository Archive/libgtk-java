/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_RcStyle.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gchar * GtkRcStyle_get_name (GtkRcStyle * cptr) 
{
    return cptr->name;
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_RcStyle_getName (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkRcStyle *cptr_g = (GtkRcStyle *)getPointerFromHandle(env, cptr);
    gchar *result_g = GtkRcStyle_get_name (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    getFontDesc
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RcStyle_getFontDesc (JNIEnv *env,
		jclass cls, jobject cptr)
{
    GtkRcStyle *cptr_g = (GtkRcStyle *)getPointerFromHandle(env, cptr);
    return getGBoxedHandle(env, cptr_g, PANGO_TYPE_FONT_DESCRIPTION,
    		(GBoxedCopyFunc) pango_font_description_copy,
    		(GBoxedFreeFunc) pango_font_description_free);
}

static gint32 GtkRcStyle_get_xthickness (GtkRcStyle * cptr) 
{
    return cptr->xthickness;
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    getXthickness
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RcStyle_getXthickness (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkRcStyle *cptr_g = (GtkRcStyle *)getPointerFromHandle(env, cptr);
    return (jint) (GtkRcStyle_get_xthickness (cptr_g));
}

static gint32 GtkRcStyle_get_ythickness (GtkRcStyle * cptr) 
{
    return cptr->ythickness;
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    getYthickness
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RcStyle_getYthickness (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkRcStyle *cptr_g = (GtkRcStyle *)getPointerFromHandle(env, cptr);
    return (jint) (GtkRcStyle_get_ythickness (cptr_g));
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    gtk_rc_style_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RcStyle_gtk_1rc_1style_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_rc_style_get_type ();
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    gtk_rc_style_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RcStyle_gtk_1rc_1style_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_rc_style_new ());
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    gtk_rc_style_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RcStyle_gtk_1rc_1style_1copy (JNIEnv *env, jclass cls, 
    jobject orig) 
{
    GtkRcStyle *orig_g = (GtkRcStyle *)getPointerFromHandle(env, orig);
    return getGObjectHandle(env, (GObject *) gtk_rc_style_copy (orig_g));
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    gtk_rc_style_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RcStyle_gtk_1rc_1style_1ref (JNIEnv *env, jclass cls, 
    jobject rc_style) 
{
    GtkRcStyle *rc_style_g = (GtkRcStyle *)getPointerFromHandle(env, rc_style);
    gtk_rc_style_ref (rc_style_g);
}

/*
 * Class:     org.gnu.gtk.RcStyle
 * Method:    gtk_rc_style_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RcStyle_gtk_1rc_1style_1unref (JNIEnv *env, jclass cls, 
    jobject rc_style) 
{
    GtkRcStyle *rc_style_g = (GtkRcStyle *)getPointerFromHandle(env, rc_style);
    gtk_rc_style_unref (rc_style_g);
}


#ifdef __cplusplus
}

#endif
