/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Button.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_button_get_type ();
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Button_gtk_1button_1new (JNIEnv *env, jclass cls) 
{
	return getGObjectHandle(env, (GObject*) gtk_button_new());
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Button_gtk_1button_1new_1with_1label (JNIEnv *env, 
    jclass cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars( env, label, NULL);
    jobject retval =  getGObjectHandle(env, (GObject *) gtk_button_new_with_label (label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
	return retval;
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_new_from_stock
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Button_gtk_1button_1new_1from_1stock (JNIEnv *env, 
    jclass cls, jstring stock_id) 
{
    const gchar* stock_id_g = (*env)->GetStringUTFChars(env, stock_id, NULL);
     jobject retval = getGObjectHandle(env, (GObject *) gtk_button_new_from_stock (stock_id_g));
	 (*env)->ReleaseStringUTFChars(env, stock_id, stock_id_g);
	 return retval;
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Button_gtk_1button_1new_1with_1mnemonic (JNIEnv *env, 
    jclass cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval = getGObjectHandle(env, (GObject *) gtk_button_new_with_mnemonic (label_g));
	(*env)->ReleaseStringUTFChars( env, label, label_g );
	return retval;
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_pressed
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1pressed (JNIEnv *env, jclass cls, 
    jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_pressed (button_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_released
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1released (JNIEnv *env, jclass cls, 
    jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_released (button_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_clicked
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1clicked (JNIEnv *env, jclass cls, 
    jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_clicked (button_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_enter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1enter (JNIEnv *env, jclass cls, 
    jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_enter (button_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_leave
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1leave (JNIEnv *env, jclass cls, 
    jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_leave (button_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_set_relief
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1relief (JNIEnv *env, jclass 
    cls, jobject button, jint relief) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    GtkReliefStyle relief_g = (GtkReliefStyle) relief;
    gtk_button_set_relief (button_g, relief_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_get_relief
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1relief (JNIEnv *env, jclass 
    cls, jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    return (jint) (gtk_button_get_relief (button_g));
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_set_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1label (JNIEnv *env, jclass 
    cls, jobject button, jstring label) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    const gchar* label_g = (*env)->GetStringUTFChars( env, label, NULL);
	gtk_button_set_label (button_g, label_g);
	(*env)->ReleaseStringUTFChars( env, label, label_g );
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_get_label
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1label (JNIEnv *env, 
    jclass cls, jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
	return (*env)->NewStringUTF( env, gtk_button_get_label (button_g) );
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_set_use_underline
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1use_1underline (JNIEnv *env, 
    jclass cls, jobject button, jboolean useUnderline) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gboolean useUnderline_g = (gboolean) useUnderline;
    gtk_button_set_use_underline (button_g, useUnderline_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_get_use_underline
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1use_1underline (JNIEnv 
    *env, jclass cls, jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    return (jboolean) (gtk_button_get_use_underline (button_g));
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_set_use_stock
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1use_1stock (JNIEnv *env, 
    jclass cls, jobject button, jboolean useStock) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gboolean useStock_g = (gboolean) useStock;
    gtk_button_set_use_stock (button_g, useStock_g);
}

/*
 * Class:     org.gnu.gtk.Button
 * Method:    gtk_button_get_use_stock
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1use_1stock (JNIEnv *env, 
    jclass cls, jobject button) 
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    return (jboolean) (gtk_button_get_use_stock (button_g));
}

/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_set_focus_on_click
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1focus_1on_1click
  (JNIEnv *env, jclass cls, jobject button, jboolean focus)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
	gtk_button_set_focus_on_click(button_g, (gboolean)focus);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_get_focus_on_click
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1focus_1on_1click
  (JNIEnv *env, jclass cls, jobject button)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
	return (jboolean)gtk_button_get_focus_on_click(button_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_set_alignment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1alignment
  (JNIEnv *env, jclass cls, jobject button, jdouble xalign, jdouble yalign)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gtk_button_set_alignment(button_g, xalign, yalign);
}
/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_get_alignment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1alignment
  (JNIEnv *env, jclass cls, jobject button, jdoubleArray xalign, jdoubleArray yalign)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    gfloat *xalign_g = (gfloat*)(*env)->GetDoubleArrayElements(env, xalign, NULL);
    gfloat *yalign_g = (gfloat*)(*env)->GetDoubleArrayElements(env, yalign, NULL);
    gtk_button_get_alignment(button_g, xalign_g, yalign_g);
    (*env)->ReleaseDoubleArrayElements(env, xalign, (jdouble*)xalign_g, 0);
    (*env)->ReleaseDoubleArrayElements(env, yalign, (jdouble*)yalign_g, 0);
}

/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_set_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Button_gtk_1button_1set_1image
  (JNIEnv *env, jclass cls, jobject button, jobject image)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    GtkWidget *image_g = (GtkWidget *)getPointerFromHandle(env, image);
    gtk_button_set_image(button_g, image_g);
}

/*
 * Class:     org_gnu_gtk_Button
 * Method:    gtk_button_get_image
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Button_gtk_1button_1get_1image
  (JNIEnv *env, jclass cls, jobject button)
{
    GtkButton *button_g = (GtkButton *)getPointerFromHandle(env, button);
    return getGObjectHandle(env, (GObject *) gtk_button_get_image(button_g));
}


#ifdef __cplusplus
}

#endif
