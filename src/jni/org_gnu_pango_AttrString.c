/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"

#include "org_gnu_pango_AttrString.h"
#ifdef __cplusplus
extern "C" 
{
#endif


static gchar * PangoAttrString_get_value (PangoAttrString * cptr) 
{
    return cptr->value;
}

/*
 * Class:     org.gnu.pango.AttrString
 * Method:    getValue
 */
JNIEXPORT jstring JNICALL Java_org_gnu_pango_AttrString_getValue (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    gchar *result_g = PangoAttrString_get_value ((PangoAttrString*)getPointerFromHandle(env, obj));
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
