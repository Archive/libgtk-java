/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_RadioActionEntry
#define _Included_org_gnu_gtk_RadioActionEntry
#include "org_gnu_gtk_RadioActionEntry.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    allocate
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioActionEntry_allocate
  (JNIEnv *env, jclass cls)
{
	GtkRadioActionEntry* entry = (GtkRadioActionEntry*)g_malloc(sizeof(GtkRadioActionEntry));
	return getStructHandle(env, entry, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setName
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setName
  (JNIEnv *env, jclass cls, jobject entry, jstring name)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == name) {
		entry_g->name = NULL;
	} else {
		gchar* n = (gchar*)(*env)->GetStringUTFChars(env, name, NULL);
		entry_g->name = n;
	}
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setStockId
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setStockId
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		entry_g->stock_id = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		entry_g->stock_id = v;
	}
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setLabel
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setLabel
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		entry_g->label = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		entry_g->label = v;
	}
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setAccelerator
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setAccelerator
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		entry_g->accelerator = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		entry_g->accelerator = v;
	}
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setToolTip
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setToolTip
  (JNIEnv *env, jclass cls, jobject entry, jstring value)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	if (NULL == value) {
		entry_g->tooltip = NULL;
	} else {
		gchar* v = (gchar*)(*env)->GetStringUTFChars(env, value, NULL);
		entry_g->tooltip = v;
	}
}

/*
 * Class:     org_gnu_gtk_RadioActionEntry
 * Method:    setValue
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioActionEntry_setValue
  (JNIEnv *env, jclass cls, jobject entry, jint value)
{
	GtkRadioActionEntry* entry_g = (GtkRadioActionEntry*)getPointerFromHandle(env, entry);
	entry_g->value = value;
}

#ifdef __cplusplus
}
#endif
#endif
