/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include <stdlib.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeSelection.h"
#ifdef __cplusplus
extern "C" 
{
#endif

typedef struct{
    JNIEnv *env;
    jobject obj;
    jmethodID mid;
} ForEachData;

static void forEachFunc(GtkTreeModel *model,
                 GtkTreePath *path,
                 GtkTreeIter *iter,
                 gpointer data)
{
    jvalue *jargs;
    ForEachData *fedata = (ForEachData *) data;
    JNIEnv* env = fedata->env;
    jargs = malloc(sizeof(jvalue) * 3);
    jargs[0].l = getGObjectHandleAndRef(env, (GObject *) model);
    jargs[1].l = getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, (GBoxedCopyFunc)
    		gtk_tree_path_copy, (GBoxedFreeFunc) gtk_tree_path_free);
    jargs[2].l = getGBoxedHandle(env, iter, GTK_TYPE_TREE_ITER, (GBoxedCopyFunc)
    		gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free);
    (*(fedata->env))->CallVoidMethodA(fedata->env, fedata->obj, fedata->mid, jargs);
    g_free(jargs);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_startForEach (JNIEnv *env,
		jclass cls, jobject selection, jobject connect_to) 
{
    ForEachData * fedata = g_new(ForEachData, 1);
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    fedata->env = env;				
    fedata->obj = (*env)->NewGlobalRef(env, connect_to);
    fedata->mid = (*env)->GetMethodID(env, 
    	(*env)->GetObjectClass(env, connect_to), "doEach", 
    		"(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V");
    gtk_tree_selection_selected_foreach(selection_g, 
    	(GtkTreeSelectionForeachFunc) forEachFunc, fedata);
    // free the references now that the foreach function has iterated through
    // all of the selections
    (*env)->DeleteGlobalRef(env, fedata->obj);
    g_free(fedata);
}


/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_tree_selection_get_type ();
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_set_mode
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1set_1mode (JNIEnv *env, jclass cls, jobject selection, jint type) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkSelectionMode type_g = (GtkSelectionMode) type;
    gtk_tree_selection_set_mode (selection_g, type_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_get_mode
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1mode
		(JNIEnv *env, jclass cls, jobject selection) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    return (jint) (gtk_tree_selection_get_mode (selection_g));
}

///*
// * Class:     org.gnu.gtk.TreeSelection
// * Method:    gtk_tree_selection_get_user_data
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1user_1data (
//    JNIEnv *env, jclass cls, jobject selection) 
//{
//    GtkTreeSelection *selection_g = (GtkTreeSelection *)getPointerFromHandle(env, selection);
//        return (jint)gtk_tree_selection_get_user_data (selection_g);
//}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_get_tree_view
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1tree_1view
		(JNIEnv *env, jclass cls, jobject selection) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    return getGObjectHandle(env, (GObject *)gtk_tree_selection_get_tree_view (selection_g));
}

///*
// * Class:     org.gnu.gtk.TreeSelection
// * Method:    gtk_tree_selection_get_selected
// * Signature: (I[Lint ;I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1selected (
//    JNIEnv *env, jclass cls, jobject selection, jintArray model, jint iter) 
//{
//    GtkTreeSelection *selection_g = (GtkTreeSelection *)getPointerFromHandle(env, selection);
//    gint *model_g_g = (gint *) (*env)->GetIntArrayElements (env, model, NULL);
//    GtkTreeModel **model_g = (GtkTreeModel **)model_g_g;
//    GtkTreeIter *iter_g = (GtkTreeIter *)iter;
//    {
//        jboolean result_j = (jboolean) (gtk_tree_selection_get_selected (selection_g, model_g, 
//                iter_g));
//        (*env)->ReleaseIntArrayElements (env, model, (jint*)model_g_g, 0);
//        return result_j;
//    }
//}

static jobject getTreePath(JNIEnv *env, gpointer path)	{
	return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, NULL,
			(GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_TreeSelection
 * Method:    gtk_tree_selection_get_selected_rows
 * Signature: (I)[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1get_1selected_1rows(JNIEnv *env, jclass cls, jobject selection)
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    return getGBoxedHandlesFromGList(env,
    		gtk_tree_selection_get_selected_rows(selection_g, NULL),
    		(GetHandleFunc) getTreePath);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeSelection
 * Method:    gtk_tree_selection_count_selected_rows
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1count_1selected_1rows(JNIEnv *env, jclass cls, jobject selection)
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    return (jint)gtk_tree_selection_count_selected_rows(selection_g);
}
                                                                                         

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_select_path
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1select_1path (JNIEnv *env, jclass cls, jobject selection, jobject path) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gtk_tree_selection_select_path (selection_g, path_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_unselect_path
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1unselect_1path (JNIEnv *env, jclass cls, jobject selection, jobject path) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gtk_tree_selection_unselect_path (selection_g, path_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_select_iter
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1select_1iter (JNIEnv *env, jclass cls, jobject selection, jobject iter) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    gtk_tree_selection_select_iter (selection_g, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_unselect_iter
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1unselect_1iter (JNIEnv *env, jclass cls, jobject selection, jobject iter) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    gtk_tree_selection_unselect_iter (selection_g, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_path_is_selected
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1path_1is_1selected (JNIEnv *env, jclass cls, jobject selection, jobject path) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jboolean) (gtk_tree_selection_path_is_selected (selection_g, 
                                                            path_g));
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_iter_is_selected
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1iter_1is_1selected (JNIEnv *env, jclass cls, jobject selection, jobject iter) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_tree_selection_iter_is_selected (selection_g, 
                                                            iter_g));
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_select_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1select_1all (JNIEnv *env, jclass cls, jobject selection) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    gtk_tree_selection_select_all (selection_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_unselect_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1unselect_1all (JNIEnv *env, jclass cls, jobject selection) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    gtk_tree_selection_unselect_all (selection_g);
}

/*
 * Class:     org.gnu.gtk.TreeSelection
 * Method:    gtk_tree_selection_select_range
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1select_1range (JNIEnv *env, jclass cls, jobject selection, jobject startPath, jobject endPath) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreePath *startPath_g = 
        (GtkTreePath *)getPointerFromHandle(env, startPath);
    GtkTreePath *endPath_g = 
        (GtkTreePath *)getPointerFromHandle(env, endPath);
    gtk_tree_selection_select_range (selection_g, startPath_g, endPath_g);
}

/*
 * Class:     org_gnu_gtk_TreeSelection
 * Method:    gtk_tree_selection_unselect_range
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSelection_gtk_1tree_1selection_1unselect_1range(JNIEnv *env, jclass cls, jobject selection, jobject startPath, jobject endPath) 
{
    GtkTreeSelection *selection_g = 
        (GtkTreeSelection *)getPointerFromHandle(env, selection);
    GtkTreePath *startPath_g = 
        (GtkTreePath *)getPointerFromHandle(env, startPath);
    GtkTreePath *endPath_g = 
        (GtkTreePath *)getPointerFromHandle(env, endPath);
    gtk_tree_selection_unselect_range (selection_g, startPath_g, endPath_g);
}

#ifdef __cplusplus
}

#endif
