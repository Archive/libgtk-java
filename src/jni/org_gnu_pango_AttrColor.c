/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrColor.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrColor_getColor 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrColor *col_g = (PangoAttrColor *)getPointerFromHandle(env, obj);
    return getStructHandle(env, &col_g->color, 
                           (JGCopyFunc)pango_color_copy, 
                           (JGFreeFunc)pango_color_free);
}

#ifdef __cplusplus
}

#endif
