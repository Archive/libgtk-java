/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_EventBox.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.EventBox
 * Method:    gtk_event_box_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_event_box_get_type ();
}

/*
 * Class:     org.gnu.gtk.EventBox
 * Method:    gtk_event_box_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_event_box_new ());
}

/*
 * Class:     org_gnu_gtk_EventBox
 * Method:    gtk_event_box_set_visible_window
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1set_1visible_1window
  (JNIEnv *env, jclass cls, jobject ebox, jboolean visible)
{
	GtkEventBox *ebox_g = (GtkEventBox*)getPointerFromHandle(env, ebox);
	gtk_event_box_set_visible_window(ebox_g, (gboolean)visible);
}
                                                                                
/*
 * Class:     org_gnu_gtk_EventBox
 * Method:    gtk_event_box_get_visible_window
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1get_1visible_1window
  (JNIEnv *env, jclass cls, jobject ebox)
{
	GtkEventBox *ebox_g = (GtkEventBox*)getPointerFromHandle(env, ebox);
	return (jboolean)gtk_event_box_get_visible_window(ebox_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_EventBox
 * Method:    gtk_event_box_set_above_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1set_1above_1child
  (JNIEnv *env, jclass cls, jobject ebox, jboolean above)
{
	GtkEventBox *ebox_g = (GtkEventBox*)getPointerFromHandle(env, ebox);
	gtk_event_box_set_above_child(ebox_g, (gboolean)above);
}
                                                                                
/*
 * Class:     org_gnu_gtk_EventBox
 * Method:    gtk_event_box_get_above_child
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EventBox_gtk_1event_1box_1get_1above_1child
  (JNIEnv *env, jclass cls, jobject ebox)
{
	GtkEventBox *ebox_g = (GtkEventBox*)getPointerFromHandle(env, ebox);
	return (jboolean)gtk_event_box_get_above_child(ebox_g);
}
                                                                                
     

#ifdef __cplusplus
}

#endif
