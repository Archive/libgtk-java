/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Region.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Region_gdk_1region_1new (JNIEnv *env, jclass cls) 
{
    return getStructHandle(env, gdk_region_new (), 
                           NULL, (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_polygon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Region_gdk_1region_1polygon (JNIEnv *env, jclass cls, 
    jobjectArray points, jint fillRule) 
{
    GdkPoint *points_g = (GdkPoint *)getPointerArrayFromHandles(env, points);
    gint numPoints = (*env)->GetArrayLength(env, points);
    GdkFillRule fillRule_g = (GdkFillRule) fillRule;
    return getStructHandle(env, gdk_region_polygon (points_g, numPoints, 
                                                    fillRule_g), 
                           NULL, (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_rectangle
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Region_gdk_1region_1rectangle (JNIEnv *env, jclass cls, 
    jobject rectangle) 
{
    GdkRectangle *rectangle_g = (GdkRectangle *)getPointerFromHandle(env, rectangle);
    return getStructHandle(env, gdk_region_rectangle (rectangle_g), 
                           NULL, (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_get_clipbox
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1get_1clipbox (JNIEnv *env, jclass 
    cls, jobject region, jobject rectangle) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    GdkRectangle *rectangle_g = (GdkRectangle *)getPointerFromHandle(env, rectangle);
    gdk_region_get_clipbox (region_g, rectangle_g);
}

static jobject getRectangleHandle( JNIEnv *env, gpointer rect ) {
    return getGBoxedHandle(env, rect, GDK_TYPE_RECTANGLE, 
                           (GBoxedCopyFunc) NULL, (GBoxedFreeFunc)g_free);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_get_rectangles
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Region_gdk_1region_1get_1rectangles (JNIEnv *env, 
    jclass cls, jobject region) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gint len;
    GdkRectangle *rectangles_g = NULL;
    gdk_region_get_rectangles (region_g, &rectangles_g, &len);
    if (NULL == rectangles_g)
        return NULL;
    jobjectArray ret = getGBoxedHandlesFromPointers(env, (void*)&rectangles_g, 
                                                    len, getRectangleHandle);
    g_free( rectangles_g );
    return ret;
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_empty
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Region_gdk_1region_1empty (JNIEnv *env, jclass cls, 
    jobject region) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    return (jboolean) (gdk_region_empty (region_g));
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_equal
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Region_gdk_1region_1equal (JNIEnv *env, jclass cls, 
    jobject region1, jobject region2) 
{
    GdkRegion *region1_g = (GdkRegion *)getPointerFromHandle(env, region1);
    GdkRegion *region2_g = (GdkRegion *)getPointerFromHandle(env, region2);
    return (jboolean) (gdk_region_equal (region1_g, region2_g));
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_point_in
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Region_gdk_1region_1point_1in (JNIEnv *env, jclass 
    cls, jobject region, jint x, jint y) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    return (jboolean) (gdk_region_point_in (region_g, x_g, y_g));
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_rect_in
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Region_gdk_1region_1rect_1in (JNIEnv *env, jclass cls, 
    jobject region, jobject rect) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    GdkRectangle *rect_g = (GdkRectangle *)getPointerFromHandle(env, rect);
    return (jint) (gdk_region_rect_in (region_g, rect_g));
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_offset
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1offset (JNIEnv *env, jclass cls, 
    jobject region, jint dx, jint dy) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gint32 dx_g = (gint32) dx;
    gint32 dy_g = (gint32) dy;
    gdk_region_offset (region_g, dx_g, dy_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_shrink
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1shrink (JNIEnv *env, jclass cls, 
    jobject region, jint dx, jint dy) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gint32 dx_g = (gint32) dx;
    gint32 dy_g = (gint32) dy;
    gdk_region_shrink (region_g, dx_g, dy_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_union_with_rect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1union_1with_1rect (JNIEnv *env, 
    jclass cls, jobject region, jobject rect) 
{
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    GdkRectangle *rect_g = (GdkRectangle *)getPointerFromHandle(env, rect);
    gdk_region_union_with_rect (region_g, rect_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_intersect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1intersect (JNIEnv *env, jclass cls, 
    jobject source1, jobject source2) 
{
    GdkRegion *source1_g = (GdkRegion *)getPointerFromHandle(env, source1);
    GdkRegion *source2_g = (GdkRegion *)getPointerFromHandle(env, source2);
    gdk_region_intersect (source1_g, source2_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_union
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1union (JNIEnv *env, jclass cls, 
    jobject source1, jobject source2) 
{
    GdkRegion *source1_g = (GdkRegion *)getPointerFromHandle(env, source1);
    GdkRegion *source2_g = (GdkRegion *)getPointerFromHandle(env, source2);
    gdk_region_union (source1_g, source2_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_subtract
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1subtract (JNIEnv *env, jclass cls, 
    jobject source1, jobject source2) 
{
    GdkRegion *source1_g = (GdkRegion *)getPointerFromHandle(env, source1);
    GdkRegion *source2_g = (GdkRegion *)getPointerFromHandle(env, source2);
    gdk_region_subtract (source1_g, source2_g);
}

/*
 * Class:     org.gnu.gdk.Region
 * Method:    gdk_region_xor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Region_gdk_1region_1xor (JNIEnv *env, jclass cls,
    jobject source1, jobject source2) 
{
    GdkRegion *source1_g = (GdkRegion *)getPointerFromHandle(env, source1);
    GdkRegion *source2_g = (GdkRegion *)getPointerFromHandle(env, source2);
    gdk_region_xor (source1_g, source2_g);
}


#ifdef __cplusplus
}
#endif

