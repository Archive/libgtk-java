/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Geometry.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GdkGeometry_get_min_width (GdkGeometry * cptr) 
{
    return cptr->min_width;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMinWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getMinWidth (JNIEnv *env, jclass cls, jobject obj)
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_min_width (obj_g));
}

static gint32 GdkGeometry_get_min_height (GdkGeometry * cptr) 
{
    return cptr->min_height;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMinHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getMinHeight (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_min_height (obj_g));
}

static gint32 GdkGeometry_get_max_width (GdkGeometry * cptr) 
{
    return cptr->max_width;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMaxWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getMaxWidth (JNIEnv *env, jclass cls, jobject obj)
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_max_width (obj_g));
}

static gint32 GdkGeometry_get_max_height (GdkGeometry * cptr) 
{
    return cptr->max_height;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMaxHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getMaxHeight (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_max_height (obj_g));
}

static gint32 GdkGeometry_get_base_width (GdkGeometry * cptr) 
{
    return cptr->base_width;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getBaseWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getBaseWidth (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_base_width (obj_g));
}

static gint32 GdkGeometry_get_base_height (GdkGeometry * cptr) 
{
    return cptr->base_height;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getBaseHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getBaseHeight (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_base_height (obj_g));
}

static gint32 GdkGeometry_get_width_inc (GdkGeometry * cptr) 
{
    return cptr->width_inc;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getWidthInc
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getWidthInc (JNIEnv *env, jclass cls, jobject obj)
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_width_inc (obj_g));
}

static gint32 GdkGeometry_get_height_inc (GdkGeometry * cptr) 
{
    return cptr->height_inc;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getHeightInc
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getHeightInc (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_height_inc (obj_g));
}

static gdouble GdkGeometry_get_min_aspect (GdkGeometry * cptr) 
{
    return cptr->min_aspect;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMinAspect
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_Geometry_getMinAspect (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jdouble) (GdkGeometry_get_min_aspect (obj_g));
}

static gdouble GdkGeometry_get_max_aspect (GdkGeometry * cptr) 
{
    return cptr->max_aspect;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getMaxAspect
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gdk_Geometry_getMaxAspect (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jdouble) (GdkGeometry_get_max_aspect (obj_g));
}

static GdkGravity GdkGeometry_get_win_gravity (GdkGeometry * cptr) 
{
    return cptr->win_gravity;
}

/*
 * Class:     org.gnu.gdk.Geometry
 * Method:    getWinGravity
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Geometry_getWinGravity (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkGeometry *obj_g = (GdkGeometry *)getPointerFromHandle(env, obj);
    return (jint) (GdkGeometry_get_win_gravity (obj_g));
}


#ifdef __cplusplus
}

#endif
