/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#include "org_gnu_atk_Relation.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.atk.Relation
 * Method:    atk_relation_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Relation_atk_1relation_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)atk_relation_get_type ();
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_type_register
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Relation_atk_1relation_1type_1register
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar *n = (*env)->GetStringUTFChars(env, name, NULL);
	jint type = (jint)atk_relation_type_register(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return type;
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_type_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_Relation_atk_1relation_1type_1get_1name
  (JNIEnv *env, jclass cls, jint type)
{
	return (*env)->NewStringUTF(env, atk_relation_type_get_name((AtkRelationType)type));
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_type_for_name
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Relation_atk_1relation_1type_1for_1name
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar *n = (*env)->GetStringUTFChars(env, name, NULL);
	jint type = (jint)atk_relation_type_for_name(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return type;
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_Relation_atk_1relation_1new
  (JNIEnv *env, jclass cls, jobjectArray targets, jint relationship)
{
	AtkObject** targets_g = (AtkObject**)getPointerArrayFromHandles(env, targets);
	gint len = (gint)(*env)->GetArrayLength(env, targets);
	return getGObjectHandle(env, G_OBJECT(atk_relation_new(targets_g, len, (AtkRelationType)relationship)));
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_get_relation_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Relation_atk_1relation_1get_1relation_1type
  (JNIEnv *env, jclass cls, jobject relation)
{
	AtkRelation *relation_g = (AtkRelation*)getPointerFromHandle(env, relation);
	return (jint)atk_relation_get_relation_type(relation_g);
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_get_target
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_atk_Relation_atk_1relation_1get_1target
  (JNIEnv *env, jclass cls, jobject relation)
{
	AtkRelation *relation_g = (AtkRelation*)getPointerFromHandle(env, relation);
	GPtrArray* targets = atk_relation_get_target(relation_g);
	return getGObjectHandlesFromPointers(env, targets->pdata, targets->len);
}

/*
 * Class:     org_gnu_atk_Relation
 * Method:    atk_relation_add_target
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_Relation_atk_1relation_1add_1target
  (JNIEnv *env, jclass cls, jobject relation, jobject target)
{
	AtkRelation *relation_g = (AtkRelation*)getPointerFromHandle(env, relation);
	AtkObject *target_g = (AtkObject*)getPointerFromHandle(env, target);
	atk_relation_add_target(relation_g, target_g);
}


#ifdef __cplusplus
}

#endif
