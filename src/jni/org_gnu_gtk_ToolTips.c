/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include <stdlib.h>
#include <string.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToolTips.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_tooltips_get_type ();
}

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_tooltips_new ());
}

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_enable
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1enable (JNIEnv *env, jclass cls, jobject tooltips) 
{
    GtkTooltips *tooltips_g = 
        (GtkTooltips *)getPointerFromHandle(env, tooltips);
    gtk_tooltips_enable (tooltips_g);
}

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_disable
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1disable (JNIEnv *env, jclass cls, jobject tooltips) 
{
    GtkTooltips *tooltips_g = 
        (GtkTooltips *)getPointerFromHandle(env, tooltips);
    gtk_tooltips_disable (tooltips_g);
}

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_set_tip
 * Signature: (IIjava.lang.String;java.lang.String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1set_1tip (JNIEnv *env, jclass cls, jobject tooltips, jobject widget, jstring tipText, jstring tipPrivate) 
{
    GtkTooltips *tooltips_g = 
        (GtkTooltips *)getPointerFromHandle(env, tooltips);
    GtkWidget *widget_g = 
        (GtkWidget *)getPointerFromHandle(env, widget);
    gchar* tipText_g = 
        (gchar*)(*env)->GetStringUTFChars(env, tipText, 0);
    gchar* tipPrivate_g = 
        (gchar*)(*env)->GetStringUTFChars(env, tipPrivate, 0);

    gtk_tooltips_set_tip (tooltips_g, widget_g, tipText_g, tipPrivate_g);

    (*env)->ReleaseStringUTFChars(env, tipText, tipText_g);
    (*env)->ReleaseStringUTFChars(env, tipPrivate, tipPrivate_g);
}

/*
 * Class:     org.gnu.gtk.ToolTips
 * Method:    gtk_tooltips_force_window
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1force_1window (JNIEnv *env, jclass cls, jobject tooltips) 
{
    GtkTooltips *tooltips_g = 
        (GtkTooltips *)getPointerFromHandle(env, tooltips);
    gtk_tooltips_force_window (tooltips_g);
}

/*
 * Class:     org_gnu_gtk_ToolTips
 * Method:    gtk_tooltips_data_get
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolTips_gtk_1tooltips_1data_1get
		(JNIEnv *env, jclass cls, jobject widget)
{
	GtkTooltipsData *data;
	GtkTooltipsData *data_copy;
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    data_copy = g_malloc(sizeof(GtkTooltipsData));
    data = gtk_tooltips_data_get(widget_g);
    memcpy(data_copy, data, sizeof(GtkTooltipsData));
    return getStructHandle(env, data_copy, NULL, (JGFreeFunc) g_free);
}

#ifdef __cplusplus
}

#endif
