#ifndef _HANDLECALLBACKACTION_H
#define _HANDLECALLBACKACTION_H

#include <gtk/gtk.h>

void handleCallbackAction(GtkAction* action, gpointer userData);

#endif
