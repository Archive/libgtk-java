/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Menu.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_popup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1popup (JNIEnv *env, jclass cls, 
    jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gtk_menu_popup(menu_g, NULL, NULL, NULL, NULL, 0, gtk_get_current_event_time());
}


/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_menu_get_type ();
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_menu_new ());
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_reposition
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1reposition (JNIEnv *env, jclass cls, 
    jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gtk_menu_reposition (menu_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_popdown
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1popdown (JNIEnv *env, jclass cls, jobject 
    menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gtk_menu_popdown (menu_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_active
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1active (JNIEnv *env, jclass cls, 
    jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    return (GtkWidget *) getGObjectHandle(env, G_OBJECT(gtk_menu_get_active (menu_g)));
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_set_active
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1active (JNIEnv *env, jclass cls, 
    jobject menu, jint index) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gint32 index_g = (gint32) index;
    gtk_menu_set_active (menu_g, index_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_set_accel_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1accel_1group (JNIEnv *env, jclass 
    cls, jobject menu, jobject accelGroup) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_menu_set_accel_group (menu_g, accelGroup_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_accel_group
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1accel_1group (JNIEnv *env, jclass 
    cls, jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    return getGObjectHandle(env, (GObject *) gtk_menu_get_accel_group (menu_g));
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_set_accel_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1accel_1path (JNIEnv *env, jclass 
    cls, jobject menu, jstring accelPath) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gchar* accelPath_g = (gchar*)(*env)->GetStringUTFChars(env, accelPath, 0);
    gtk_menu_set_accel_path (menu_g, accelPath_g);
    (*env)->ReleaseStringUTFChars(env, accelPath, accelPath_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_detach
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1detach (JNIEnv *env, jclass cls, jobject 
    menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gtk_menu_detach (menu_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_attach_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1attach_1widget (JNIEnv *env, 
    jclass cls, jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    return getGObjectHandle(env, (GObject *) gtk_menu_get_attach_widget (menu_g));
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_set_tearoff_state
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1tearoff_1state (JNIEnv *env, 
    jclass cls, jobject menu, jboolean tornOff) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gboolean tornOff_g = (gboolean) tornOff;
    gtk_menu_set_tearoff_state (menu_g, tornOff_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_tearoff_state
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1tearoff_1state (JNIEnv *env, 
    jclass cls, jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    return (jboolean) (gtk_menu_get_tearoff_state (menu_g));
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1title (JNIEnv *env, jclass cls, 
    jobject menu, jstring title) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    gtk_menu_set_title (menu_g, title_g);
    (*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1title (JNIEnv *env, jclass 
    cls, jobject menu) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    gchar *result_g = (gchar*)gtk_menu_get_title (menu_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Menu
 * Method:    gtk_menu_reorder_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1reorder_1child (JNIEnv *env, jclass 
    cls, jobject menu, jobject child, jint position) 
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GtkMenuItem *child_g = (GtkMenuItem *)getPointerFromHandle(env, child);
    gint32 position_g = (gint32) position;
    gtk_menu_reorder_child (menu_g, (GtkWidget*)child_g, position_g);
}

/*
 * Class:     org_gnu_gtk_Menu
 * Method:    gtk_menu_set_screen
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1screen
  (JNIEnv *env, jclass cls, jobject menu, jobject screen)
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GdkScreen *screen_g = (GdkScreen *)getPointerFromHandle(env, screen);
	gtk_menu_set_screen(menu_g, screen_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Menu
 * Method:    gtk_menu_attach
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1attach
  (JNIEnv *env, jclass cls, jobject menu, jobject child, jint left, jint right, jint top, jint bottom)
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
    GtkMenuItem *child_g = (GtkMenuItem *)getPointerFromHandle(env, child);
	gtk_menu_attach(menu_g, (GtkWidget*)child_g, (gint)left, (gint)right, (gint)top, (gint)bottom);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Menu
 * Method:    gtk_menu_set_monitor
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1set_1monitor
  (JNIEnv *env, jclass cls, jobject menu, jint monNum)
{
    GtkMenu *menu_g = (GtkMenu *)getPointerFromHandle(env, menu);
	gtk_menu_set_monitor(menu_g, (gint)monNum);
}

JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Menu_gtk_1menu_1get_1for_1attach_1widget
(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget * widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandlesFromGList(env, gtk_menu_get_for_attach_widget(widget_g));
}

#ifdef __cplusplus
}

#endif
