/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Fixed.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_fixed_get_type ();
}

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_fixed_new ());
}

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_put
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1put (JNIEnv *env, jclass cls, jobject 
    fixed, jobject widget, jint x, jint y) 
{
    GtkFixed *fixed_g = (GtkFixed *)getPointerFromHandle(env, fixed);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gtk_fixed_put (fixed_g, widget_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_move
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1move (JNIEnv *env, jclass cls, jobject 
    fixed, jobject widget, jint x, jint y) 
{
    GtkFixed *fixed_g = (GtkFixed *)getPointerFromHandle(env, fixed);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gtk_fixed_move (fixed_g, widget_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_set_has_window
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1set_1has_1window (JNIEnv *env, jclass 
    cls, jobject fixed, jboolean hasWindow) 
{
    GtkFixed *fixed_g = (GtkFixed *)getPointerFromHandle(env, fixed);
    gboolean hasWindow_g = (gboolean) hasWindow;
    gtk_fixed_set_has_window (fixed_g, hasWindow_g);
}

/*
 * Class:     org.gnu.gtk.Fixed
 * Method:    gtk_fixed_get_has_window
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Fixed_gtk_1fixed_1get_1has_1window (JNIEnv *env, 
    jclass cls, jobject fixed) 
{
    GtkFixed *fixed_g = (GtkFixed *)getPointerFromHandle(env, fixed);
    return (jboolean) (gtk_fixed_get_has_window (fixed_g));
}


#ifdef __cplusplus
}

#endif
