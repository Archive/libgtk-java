/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_FontButton
#define _Included_org_gnu_gtk_FontButton
#include "org_gnu_gtk_FontButton.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_font_button_get_type();
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_font_button_new());
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_new_with_font
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1new_1with_1font
  (JNIEnv *env, jclass cls, jstring font)
{
	const gchar* f = (*env)->GetStringUTFChars(env, font, NULL);
	jobject ret = getGObjectHandle(env, (GObject *) gtk_font_button_new_with_font(f));
	(*env)->ReleaseStringUTFChars(env, font, f);
	return ret;
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_title
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1title
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	const gchar* ret = gtk_font_button_get_title(fb_g);
	return (*env)->NewStringUTF(env, ret);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1title
  (JNIEnv *env, jclass cls, jobject fb, jstring title)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	const gchar* t = (*env)->GetStringUTFChars(env, title, NULL);
	gtk_font_button_set_title(fb_g, t);
	(*env)->ReleaseStringUTFChars(env, title, t);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_use_font
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1use_1font
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	return (jboolean)gtk_font_button_get_use_font(fb_g);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_use_font
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1use_1font
  (JNIEnv *env, jclass cls, jobject fb, jboolean val)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	gtk_font_button_set_use_font(fb_g, (gboolean)val);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_use_size
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1use_1size
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	return (jboolean)gtk_font_button_get_use_size(fb_g);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_use_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1use_1size
  (JNIEnv *env, jclass cls, jobject fb, jboolean val)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	gtk_font_button_set_use_size(fb_g, (gboolean)val);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_font_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1font_1name
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	return (*env)->NewStringUTF(env, gtk_font_button_get_font_name(fb_g));
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_font_name
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1font_1name
  (JNIEnv *env, jclass cls, jobject fb, jstring font)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	const gchar* f = (*env)->GetStringUTFChars(env, font, NULL);
	jboolean ret = (jboolean)gtk_font_button_set_font_name(fb_g, f);
	(*env)->ReleaseStringUTFChars(env, font, f);
	return ret;
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_show_style
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1show_1style
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	return (jboolean)gtk_font_button_get_show_style(fb_g);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_show_style
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1show_1style
  (JNIEnv *env, jclass cls, jobject fb, jboolean val)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	gtk_font_button_set_show_style(fb_g, (gboolean)val);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_get_show_size
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1get_1show_1size
  (JNIEnv *env, jclass cls, jobject fb)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	return (jboolean)gtk_font_button_get_show_size(fb_g);
}

/*
 * Class:     org_gnu_gtk_FontButton
 * Method:    gtk_font_button_set_show_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FontButton_gtk_1font_1button_1set_1show_1size
  (JNIEnv *env, jclass cls, jobject fb, jboolean val)
{
	GtkFontButton* fb_g = (GtkFontButton*)getPointerFromHandle(env, fb);
	gtk_font_button_set_show_size(fb_g, (gboolean)val);
}

#ifdef __cplusplus
}
#endif
#endif
