/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Container.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_container_get_type ();
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_border_width
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1border_1width (JNIEnv 
    *env, jclass cls, jobject container, jint borderWidth) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    gint32 borderWidth_g = (gint32) borderWidth;
    gtk_container_set_border_width (container_g, borderWidth_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_border_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1border_1width (JNIEnv 
    *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    return (jint) (gtk_container_get_border_width (container_g));
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_add
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1add (JNIEnv *env, jclass cls, 
    jobject container, jobject widget) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_container_add (container_g, widget_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1remove (JNIEnv *env, jclass 
    cls, jobject container, jobject widget) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_container_remove (container_g, widget_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_resize_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1resize_1mode (JNIEnv 
    *env, jclass cls, jobject container, jint mode) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkResizeMode mode_g = (GtkResizeMode) mode;
    gtk_container_set_resize_mode (container_g, mode_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_resize_mode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1resize_1mode (JNIEnv 
    *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    return (jint) (gtk_container_get_resize_mode (container_g));
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_check_resize
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1check_1resize (JNIEnv *env, 
    jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    gtk_container_check_resize (container_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_children
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1children (JNIEnv *env, 
    jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GList *list = gtk_container_get_children(container_g);
    return getGObjectHandlesFromGList(env, list);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_propagate_expose
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1propagate_1expose (JNIEnv 
    *env, jclass cls, jobject container, jobject child, jobject event) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GdkEventExpose *event_g = (GdkEventExpose *)getPointerFromHandle(env, event);
    gtk_container_propagate_expose (container_g, child_g, event_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_focus_chain
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1focus_1chain (JNIEnv 
    *env, jclass cls, jobject container, jobject focusableWidgets) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GList *focusableWidgets_g = (GList *)getPointerFromHandle(env, focusableWidgets);
    gtk_container_set_focus_chain (container_g, focusableWidgets_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_focus_chain
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1focus_1chain (JNIEnv 
    *env, jclass cls, jobject container, jobject focusableWidgets) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GList **focusableWidgets_g = (GList **)getPointerFromHandle(env, focusableWidgets);
    return (jboolean) (gtk_container_get_focus_chain (container_g, 
                focusableWidgets_g));
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_unset_focus_chain
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1unset_1focus_1chain (JNIEnv 
    *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    gtk_container_unset_focus_chain (container_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_reallocate_redraws
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1reallocate_1redraws (
    JNIEnv *env, jclass cls, jobject container, jboolean needsRedraws) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    gboolean needsRedraws_g = (gboolean) needsRedraws;
    gtk_container_set_reallocate_redraws (container_g, needsRedraws_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_focus_child
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1focus_1child (JNIEnv 
    *env, jclass cls, jobject container, jobject child) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_container_set_focus_child (container_g, child_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_focus_vadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1focus_1vadjustment (
    JNIEnv *env, jclass cls, jobject container, jobject adjustment) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_container_set_focus_vadjustment (container_g, adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_focus_vadjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1focus_1vadjustment (
    JNIEnv *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    return getGObjectHandle(env, (GObject *) gtk_container_get_focus_vadjustment (container_g));
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_set_focus_hadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1set_1focus_1hadjustment (
    JNIEnv *env, jclass cls, jobject container, jobject adjustment) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_container_set_focus_hadjustment (container_g, adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_get_focus_hadjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Container_gtk_1container_1get_1focus_1hadjustment (
    JNIEnv *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    return getGObjectHandle(env, (GObject *) gtk_container_get_focus_hadjustment (container_g));
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_resize_children
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1resize_1children (JNIEnv 
    *env, jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    gtk_container_resize_children (container_g);
}

/*
 * Class:     org.gnu.gtk.Container
 * Method:    gtk_container_child_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Container_gtk_1container_1child_1type (JNIEnv *env, 
    jclass cls, jobject container) 
{
    GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
    return (jint)gtk_container_child_type (container_g);
}

/*
 * Class:	org.gnu.gtk.Container
 * Method:	gtk_container_child_set_property
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Container_gtk_1container_1child_1set_1property (JNIEnv *env,
	jclass cls, jobject container, jobject child, jstring property_name, jobject value)
{
	GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
	GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
	const gchar *property_name_g = (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);
	const GValue *value_g = (const GValue *)getPointerFromHandle(env, value);
	
	gdk_threads_enter();
	gtk_container_child_set_property(container_g, child_g, property_name_g, value_g);
	gdk_threads_leave();
	
	(*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
}

/*
 * Class:	org.gnu.gtk.Container
 * Method:	gtk_container_child_get_property
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Container_gtk_1container_1child_1get_1property (JNIEnv *env, 
	jclass cls, jobject container, jobject child, jstring property_name)
{
	GValue *value_g;
	
	GtkContainer *container_g = (GtkContainer *)getPointerFromHandle(env, container);
	GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
	const gchar *property_name_g = (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);

	gdk_threads_enter();
	GParamSpec *pspec =  gtk_container_class_find_child_property(G_OBJECT_GET_CLASS(container_g), 
                                         property_name_g);
	if ( !pspec ) {
           gdk_threads_leave();
	    (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
    	    return NULL;
	}

	value_g = g_malloc( sizeof( GValue ) );
	value_g->g_type = 0;
	value_g = g_value_init(value_g, pspec->value_type);

	// Get the property.
	gtk_container_child_get_property(container_g, child_g, property_name_g, value_g);
	gdk_threads_leave();
    
	(*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
	return getStructHandle(env, value_g, NULL, g_free);
}

#ifdef __cplusplus
}

#endif
