/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_GtkObject.h"
#ifdef __cplusplus
extern "C"
{
#endif


static gint32 GtkObject_get_flags (GtkObject * cptr)
{
    return cptr->flags;
}

/*
 * Class:     org.gnu.gtk.GtkObject
 * Method:    getFlags
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_GtkObject_getFlags (JNIEnv *env, jclass cls, jobject obj)
{
    GtkObject *obj_g = (GtkObject *)getPointerFromHandle(env, obj);
    return (jint) (GtkObject_get_flags (obj_g));
}

static void GtkObject_set_flags (GtkObject * cptr, gint32 flags)
{
    cptr->flags = flags;
}

/*
 * Class:     org.gnu.gtk.GtkObject
 * Method:    setFlags
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_GtkObject_setFlags (JNIEnv *env, jclass cls, jobject obj, jint flags)
{
    GtkObject *obj_g = (GtkObject *)getPointerFromHandle(env, obj);
    gint32 flags_g = (gint32) flags;
    GtkObject_set_flags (obj_g, flags_g);
}

/*
 * Class:     org.gnu.gtk.GtkObject
 * Method:    gtk_object_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_GtkObject_gtk_1object_1get_1type (JNIEnv *env, jclass
    cls)
{
    return (jint)gtk_object_get_type ();
}

/*
 * Class:     org.gnu.gtk.GtkObject
 * Method:    gtk_object_sink
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_GtkObject_gtk_1object_1sink (JNIEnv *env, jclass cls,
    jobject obj)
{
    GtkObject *obj_g = (GtkObject *)getPointerFromHandle(env, obj);
    gtk_object_sink (obj_g);
}

/*
 * Class:     org.gnu.gtk.GtkObject
 * Method:    gtk_object_destroy
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_GtkObject_gtk_1object_1destroy (JNIEnv *env, jclass
    cls, jobject obj)
{
    GtkObject *obj_g = (GtkObject *)getPointerFromHandle(env, obj);
    gtk_object_destroy (obj_g);
}

#ifdef __cplusplus
}

#endif

