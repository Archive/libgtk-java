/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Context.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jboolean JNICALL Java_org_gnu_pango_Context_pango_1context_1load_1fontNULL 
  (JNIEnv *env, jclass cls, jobject context, jobject desc) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    return (jboolean)( NULL == pango_context_load_font (context_g, desc_g));
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Context_pango_1context_1get_1type 
  (JNIEnv *env, jclass cls) 
{
    return (jint)pango_context_get_type ();
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_list_families
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_pango_Context_pango_1context_1list_1families 
  (JNIEnv *env, jclass cls, jobject context) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoFontFamily **families_g = NULL;
    int numFam;
    
    pango_context_list_families (context_g, &families_g, &numFam);
    return getGObjectHandlesFromPointers(env, (void**)families_g, numFam);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_load_font
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Context_pango_1context_1load_1font
  (JNIEnv *env, jclass cls, jobject context, jobject desc) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoFont *font = pango_context_load_font (context_g, desc_g);
    if ( font ) {
        return getGObjectHandle(env, G_OBJECT(font));
    } else {
        return NULL;
    }
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_get_metrics
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Context_pango_1context_1get_1metrics 
  (JNIEnv *env, jclass cls, jobject context, jobject desc, jobject language) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    PangoLanguage *language_g = 
        (PangoLanguage *)getPointerFromHandle(env, language);
    return getGBoxedHandle(env, 
                           pango_context_get_metrics (context_g, 
                                                      desc_g, 
                                                      language_g),
                           PANGO_TYPE_FONT_METRICS, 
                           NULL, (JGFreeFunc)pango_font_metrics_unref);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_set_font_description
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Context_pango_1context_1set_1font_1description 
  (JNIEnv *env, jclass cls, jobject context, jobject desc) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoFontDescription *desc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, desc);
    pango_context_set_font_description (context_g, desc_g);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_get_font_description
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Context_pango_1context_1get_1font_1description 
  (JNIEnv *env, jclass cls, jobject context) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    return getGBoxedHandle(env, 
                           pango_context_get_font_description(context_g),
                           PANGO_TYPE_FONT_DESCRIPTION, 
                           (GBoxedCopyFunc)pango_font_description_copy, 
                           (GBoxedFreeFunc)pango_font_description_free);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_get_language
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Context_pango_1context_1get_1language 
  (JNIEnv *env, jclass cls, jobject context) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    return getGBoxedHandle(env, 
                           pango_context_get_language(context_g),
                           PANGO_TYPE_LANGUAGE, NULL, NULL);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_set_language
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Context_pango_1context_1set_1language
  (JNIEnv *env, jclass cls, jobject context, jobject language) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoLanguage *language_g = 
        (PangoLanguage *)getPointerFromHandle(env, language);
    pango_context_set_language (context_g, language_g);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_set_base_dir
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Context_pango_1context_1set_1base_1dir
  (JNIEnv *env, jclass cls, jobject context, jint direction) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    PangoDirection direction_g = (PangoDirection) direction;
    pango_context_set_base_dir (context_g, direction_g);
}

/*
 * Class:     org.gnu.pango.Context
 * Method:    pango_context_get_base_dir
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Context_pango_1context_1get_1base_1dir 
  (JNIEnv *env, jclass cls, jobject context) 
{
    PangoContext *context_g = 
        (PangoContext *)getPointerFromHandle(env, context);
    return (jint) (pango_context_get_base_dir (context_g));
}


#ifdef __cplusplus
}

#endif
