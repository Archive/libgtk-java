/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TargetEntry.h"
#ifdef __cplusplus
extern "C" 
{
#endif
	
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TargetEntry_newTargetEntry(JNIEnv *env,
		jclass cls, jstring name, jint flags, jint id) 
{
	GtkTargetEntry *targetEntry = g_new(GtkTargetEntry, 1);
	targetEntry->target = 
            (gchar *) (*env)->GetStringUTFChars( env, name, NULL );
	targetEntry->flags = (guint) flags;
	targetEntry->info = (guint) id;

	return getStructHandle(env, targetEntry, NULL, (JGFreeFunc) g_free);
}
	
#ifdef __cplusplus
}

#endif
