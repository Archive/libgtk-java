/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AccelGroup.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_group_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1group_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_accel_group_get_type ();
}

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_group_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1group_1new (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_accel_group_new ());
}

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_group_lock
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1group_1lock (JNIEnv *env, jclass 
    cls, jobject accelGroup) 
{
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_accel_group_lock (accelGroup_g);
}

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_group_unlock
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1group_1unlock (JNIEnv *env, 
    jclass cls, jobject accelGroup) 
{
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_accel_group_unlock (accelGroup_g);
}

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_group_disconnect_key
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1group_1disconnect_1key (
    JNIEnv *env, jclass cls, jobject accelGroup, jint accelKey, jint accelMods) 
{
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    guint32 accelKey_g = (guint32) accelKey;
    GdkModifierType accelMods_g = (GdkModifierType) accelMods;
    return (jboolean) (gtk_accel_group_disconnect_key (accelGroup_g, 
                accelKey_g, accelMods_g));
}

/*
 * Class:     org.gnu.gtk.AccelGroup
 * Method:    gtk_accel_groups_activate
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AccelGroup_gtk_1accel_1groups_1activate (JNIEnv 
    *env, jclass cls, jobject object, jint accelKey, jint accelMods) 
{
    GObject *object_g = (GObject *)getPointerFromHandle(env, object);
    guint32 accelKey_g = (guint32) accelKey;
    GdkModifierType accelMods_g = (GdkModifierType) accelMods;
    return (jboolean) (gtk_accel_groups_activate (object_g, accelKey_g, 
                accelMods_g));
}

#ifdef __cplusplus
}

#endif
