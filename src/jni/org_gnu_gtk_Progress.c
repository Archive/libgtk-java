/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Progress.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gdouble GtkProgress_get_x_align (GtkProgress * cptr) 
{
    return cptr->x_align;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getXAlign
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Progress_getXAlign (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkProgress_get_x_align (cptr_g));
}

static gdouble GtkProgress_get_y_align (GtkProgress * cptr) 
{
    return cptr->y_align;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getYAlign
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Progress_getYAlign (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkProgress_get_y_align (cptr_g));
}

static gboolean GtkProgress_get_show_text (GtkProgress * cptr) 
{
    return cptr->show_text;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getShowText
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Progress_getShowText (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return (jboolean) (GtkProgress_get_show_text (cptr_g));
}

static gboolean GtkProgress_get_activity_mode (GtkProgress * cptr) 
{
    return cptr->activity_mode;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getActivityMode
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Progress_getActivityMode (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return (jboolean) (GtkProgress_get_activity_mode (cptr_g));
}

static gboolean GtkProgress_get_use_text_format (GtkProgress * cptr) 
{
    return cptr->use_text_format;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getUseTextFormat
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Progress_getUseTextFormat (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return (jboolean) (GtkProgress_get_use_text_format (cptr_g));
}

static gchar * GtkProgress_get_format (GtkProgress * cptr) 
{
    return cptr->format;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getFormat
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Progress_getFormat (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
	return (*env)->NewStringUTF(env,  GtkProgress_get_format (cptr_g) );
}

static GtkAdjustment * GtkProgress_get_adjustment (GtkProgress * cptr) 
{
    return cptr->adjustment;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getAdjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Progress_getAdjustment (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkProgress_get_adjustment (cptr_g));
}

static GdkPixmap * GtkProgress_get_offscreen_pixmap (GtkProgress * cptr) 
{
    return cptr->offscreen_pixmap;
}

/*
 * Class:     org.gnu.gtk.Progress
 * Method:    getOffscreenPixmap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Progress_getOffscreenPixmap (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkProgress *cptr_g = (GtkProgress *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkProgress_get_offscreen_pixmap (cptr_g));
}


#ifdef __cplusplus
}

#endif
