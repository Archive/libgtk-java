/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Layout.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_layout_get_type ();
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1new (JNIEnv *env, jclass cls, jobject 
    hadjustment, jobject vadjustment) 
{
    GtkAdjustment *hadjustment_g = (GtkAdjustment *)getPointerFromHandle(env, hadjustment);
    GtkAdjustment *vadjustment_g = (GtkAdjustment *)getPointerFromHandle(env, vadjustment);
    return getGObjectHandle(env, (GObject *) gtk_layout_new (hadjustment_g, vadjustment_g));
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_put
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1put (JNIEnv *env, jclass cls, jobject 
    layout, jobject childWidget, jint x, jint y) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    GtkWidget *childWidget_g = (GtkWidget *)getPointerFromHandle(env, childWidget);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gtk_layout_put (layout_g, childWidget_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_move
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1move (JNIEnv *env, jclass cls, jobject 
    layout, jobject childWidget, jint x, jint y) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    GtkWidget *childWidget_g = (GtkWidget *)getPointerFromHandle(env, childWidget);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gtk_layout_move (layout_g, childWidget_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_set_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1set_1size (JNIEnv *env, jclass cls, 
    jobject layout, jint width, jint height) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_layout_set_size (layout_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_get_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1get_1size (JNIEnv *env, jclass cls, 
    jobject layout, jintArray width, jintArray height) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    guint *width_g = (guint *) (*env)->GetIntArrayElements (env, width, NULL);
    guint *height_g = (guint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_layout_get_size (layout_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_get_hadjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1get_1hadjustment (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    return getGObjectHandle(env, (GObject *) gtk_layout_get_hadjustment (layout_g));
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_get_vadjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1get_1vadjustment (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    return getGObjectHandle(env, (GObject *) gtk_layout_get_vadjustment (layout_g));
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_set_hadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1set_1hadjustment (JNIEnv *env, 
    jclass cls, jobject layout, jobject adjustment) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_layout_set_hadjustment (layout_g, adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_set_vadjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1set_1vadjustment (JNIEnv *env, 
    jclass cls, jobject layout, jobject adjustment) 
{
    GtkLayout *layout_g = (GtkLayout *)getPointerFromHandle(env, layout);
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_layout_set_vadjustment (layout_g, adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Layout
 * Method:    gtk_layout_get_bin_window
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Layout_gtk_1layout_1get_1bin_1window (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    GtkLayout *layout_g = GTK_LAYOUT(getPointerFromHandle(env, layout));
    return getGObjectHandleAndRef(env, (GObject *) (layout_g->bin_window));
}


#ifdef __cplusplus
}

#endif
