/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AccelMap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_AccelMap
 * Method:    gtk_accel_map_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1get
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_accel_map_get());
}
                                                                                
/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_add_entry
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1add_1entry (JNIEnv *env, 
    jclass cls, jstring accelPath, jint accelKey, jint accelMods) 
{
    gchar* accelPath_g = (gchar*)(*env)->GetStringUTFChars(env, accelPath, 0);
    gtk_accel_map_add_entry (accelPath_g, (guint32)accelKey, (guint32)accelMods);
    (*env)->ReleaseStringUTFChars(env, accelPath, accelPath_g);
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_lookup_entry
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1lookup_1entry (JNIEnv 
    *env, jclass cls, jstring accelPath, jobject key) 
{
    gchar* accelPath_g = (gchar*)(*env)->GetStringUTFChars(env, accelPath, 0);
    GtkAccelKey* g_key = (GtkAccelKey *) g_malloc(sizeof(GtkAccelKey));
    jboolean result_j = (jboolean)gtk_accel_map_lookup_entry (accelPath_g, g_key);
    (*env)->ReleaseStringUTFChars(env, accelPath, accelPath_g);
    if(g_key != NULL)
      updateStructHandle(env, key, g_key, (JGFreeFunc) g_free);
	else
		key = NULL;
		
    return result_j;
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_change_entry
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1change_1entry (JNIEnv 
    *env, jclass cls, jstring accelPath, jint accekKey, jint accelMods, jboolean replace) 
{
    gchar* accelPath_g = (gchar*)(*env)->GetStringUTFChars(env, accelPath, 0);
    GdkModifierType accelMods_g = (GdkModifierType) accelMods;
    jboolean result_j = (jboolean) (gtk_accel_map_change_entry (accelPath_g, (guint32)accekKey, 
                accelMods_g, (gboolean)replace));
   	(*env)->ReleaseStringUTFChars(env, accelPath, accelPath_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_load
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1load (JNIEnv *env, jclass 
    cls, jstring fileName) 
{
    gchar* fileName_g = (gchar*)(*env)->GetStringUTFChars(env, fileName, 0);
    gtk_accel_map_load (fileName_g);
    (*env)->ReleaseStringUTFChars(env, fileName, fileName_g);
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_save
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1save (JNIEnv *env, jclass 
    cls, jstring fileName) 
{
    gchar* fileName_g = (gchar*)(*env)->GetStringUTFChars(env, fileName, 0);
    gtk_accel_map_save (fileName_g);
    (*env)->ReleaseStringUTFChars(env, fileName, fileName_g);
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_load_fd
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1load_1fd (JNIEnv *env, jclass 
    cls, jint fd) 
{
    gtk_accel_map_load_fd ((gint)fd);
}

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_save_fd
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1save_1fd (JNIEnv *env, jclass 
    cls, jint fd) 
{
    gtk_accel_map_save_fd ((gint)fd);
}

/*
 * Class:     org_gnu_gtk_AccelMap
 * Method:    gtk_accel_map_lock_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1lock_1path
  (JNIEnv *env, jclass cls, jstring path)
{
	const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
	gtk_accel_map_lock_path(p);
	(*env)->ReleaseStringUTFChars(env, path, p);
}
                                                                                
/*
 * Class:     org_gnu_gtk_AccelMap
 * Method:    gtk_accel_map_unlock_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1unlock_1path
  (JNIEnv *env, jclass cls, jstring path)
{
	const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
	gtk_accel_map_unlock_path(p);
	(*env)->ReleaseStringUTFChars(env, path, p);
}
                                                                                

/*
 * Class:     org.gnu.gtk.AccelMap
 * Method:    gtk_accel_map_add_filter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelMap_gtk_1accel_1map_1add_1filter (JNIEnv *env, 
    jclass cls, jstring filterPattern) 
{
    gchar* filterPattern_g = (gchar*)(*env)->GetStringUTFChars(env, filterPattern, 0);
    gtk_accel_map_add_filter (filterPattern_g);
    (*env)->ReleaseStringUTFChars(env, filterPattern, filterPattern_g);
}

#ifdef __cplusplus
}

#endif
