/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <string.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Widget.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_widget_get_type ();
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_destroy
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1destroy (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_destroy (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_destroyed
 * Signature: (I[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1destroyed (JNIEnv *env, jclass cls, jobject widget, jobjectArray widgetPointer) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    jsize len = (*env)->GetArrayLength(env, widgetPointer);
    GtkWidget **widgetPointer_g_g = g_malloc(sizeof(GtkWidget*) * len);
    int index;
    for (index = 0; index < len; index++ )
        widgetPointer_g_g[index] = 
            (GtkWidget*)
            getPointerFromHandle(env, 
                                 (*env)->GetObjectArrayElement(env,
                                                               widgetPointer,
                                                               index));
    gtk_widget_destroyed(widget_g, widgetPointer_g_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_unparent
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1unparent (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_unparent (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_show
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1show (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_show (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_show_now
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1show_1now (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_show_now (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_hide
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1hide (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_hide (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_show_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1show_1all (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_show_all (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_hide_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1hide_1all (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_hide_all (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_map
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1map (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_map (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_unmap
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1unmap (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_unmap (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_realize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1realize (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_realize (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_unrealize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1unrealize (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_unrealize (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_queue_draw
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1queue_1draw (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_queue_draw (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_queue_draw_area
 * Signature: (IIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1queue_1draw_1area (JNIEnv *env, jclass cls, jobject widget, jint x, jint y, jint width, jint height) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_widget_queue_draw_area (widget_g, x_g, y_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_queue_resize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1queue_1resize (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_queue_resize (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_size_request
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1size_1request (JNIEnv *env, jclass cls, jobject widget, jobject requisition) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkRequisition *requisition_g = 
        (GtkRequisition *)getPointerFromHandle(env, requisition);
    gtk_widget_size_request (widget_g, requisition_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_child_requisition
 * Signature: (I[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1child_1requisition (JNIEnv *env, jclass cls, jobject widget, jobjectArray requisition) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    jsize len = (*env)->GetArrayLength(env, requisition);
    GtkRequisition **requisition_g_g = g_malloc(sizeof(GtkRequisition*) * len);
    int index;
    for (index = 0; index < len; index++ )
        requisition_g_g[index] = 
            (GtkRequisition*)
            getPointerFromHandle(env, 
                                 (*env)->GetObjectArrayElement(env,
                                                               requisition,
                                                               index));
    gtk_widget_get_child_requisition (widget_g, *requisition_g_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_size_allocate
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1size_1allocate (JNIEnv *env, jclass cls, jobject widget, jobject allocation) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkAllocation *allocation_g = 
        (GtkAllocation *)getPointerFromHandle(env, allocation);
    gtk_widget_size_allocate (widget_g, allocation_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_add_accelerator
 * Signature: (I[BIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1add_1accelerator (JNIEnv *env, jclass cls, jobject widget, jobject accelGroup, jint accelKey, jint accelMods, jint accelFlags) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    guint32 accelKey_g = (guint32) accelKey;
    guint32 accelMods_g = (guint32) accelMods;
    GtkAccelFlags accelFlags_g = (GtkAccelFlags) accelFlags;
    gtk_widget_add_accelerator (widget_g, "activate", accelGroup_g, 
                                accelKey_g, accelMods_g, accelFlags_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_remove_accelerator
 * Signature: (IIII)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1remove_1accelerator (JNIEnv *env, jclass cls, jobject widget, jobject accelGroup, jint accelKey, jint accelMods) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    guint32 accelKey_g = (guint32) accelKey;
    guint32 accelMods_g = (guint32) accelMods;
    return (jboolean)(gtk_widget_remove_accelerator (widget_g, accelGroup_g, 
                                                     accelKey_g, accelMods_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_accel_path
 * Signature: (I[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1accel_1path (JNIEnv *env, jclass cls, jobject widget, jstring accelPath, jobject accelGroup) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    const gchar* accelPath_g = (*env)->GetStringUTFChars(env, accelPath, NULL);
    gtk_widget_set_accel_path (widget_g, accelPath_g, accelGroup_g);
    (*env)->ReleaseStringUTFChars( env, accelPath, accelPath_g );
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_list_accel_closures
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1list_1accel_1closures (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getStructHandle(env, gtk_widget_list_accel_closures (widget_g), NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_can_activate_accel
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1can_1activate_1accel(JNIEnv *env, jclass cls, jobject widget, jint signalId)
{
    GtkWidget *widget_g = (GtkWidget*)getPointerFromHandle(env, widget);
    return (jboolean)gtk_widget_can_activate_accel(widget_g, (gint)signalId);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_mnemonic_activate
 * Signature: (IZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1mnemonic_1activate (JNIEnv *env, jclass cls, jobject widget, jboolean groupCycling) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean groupCycling_g = (gboolean) groupCycling;
    return (jboolean)(gtk_widget_mnemonic_activate(widget_g, groupCycling_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_event
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1event (JNIEnv *env, jclass cls, jobject widget, jobject event) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    return (jboolean)(gtk_widget_event (widget_g, event_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_send_expose
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1send_1expose (JNIEnv *env, jclass cls, jobject widget, jobject event) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    return (jint) (gtk_widget_send_expose (widget_g, event_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_activate
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1activate (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean) (gtk_widget_activate (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_scroll_adjustments
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1scroll_1adjustments (JNIEnv *env, jclass cls, jobject widget, jobject hadj, jobject vadj) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkAdjustment *hadj_g = (GtkAdjustment *)getPointerFromHandle(env, hadj);
    GtkAdjustment *vadj_g = (GtkAdjustment *)getPointerFromHandle(env, vadj);
    gtk_widget_set_scroll_adjustments (widget_g, hadj_g, vadj_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_reparent
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1reparent (JNIEnv *env, jclass cls, jobject widget, jobject newParent) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkWidget *newParent_g = (GtkWidget *)getPointerFromHandle(env, newParent);
    gtk_widget_reparent (widget_g, newParent_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_intersect
 * Signature: (II[Lint ;)Z
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1intersect
		(JNIEnv *env, jclass cls, jobject widget, jobject area) 
{
    GtkWidget *widget_g = (GtkWidget *) getPointerFromHandle(env, widget);
    GdkRectangle *area_g = (GdkRectangle *) getPointerFromHandle(env, area);
    GdkRectangle *intersection = g_new(GdkRectangle, 1);
    gboolean result = gtk_widget_intersect (widget_g, area_g, intersection);
    if (result == TRUE)	{
    	return getGBoxedHandle(env, intersection, GDK_TYPE_RECTANGLE, NULL,
    		(GBoxedFreeFunc) g_free);
    }
    g_free(intersection);
    return NULL;
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_region_intersect
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1region_1intersect (JNIEnv *env, 
                                                                               jclass cls, jobject widget, jobject region) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    return getGObjectHandle(env, G_OBJECT(gtk_widget_region_intersect (widget_g, region_g)));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_freeze_child_notify
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1freeze_1child_1notify (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_freeze_child_notify (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_child_notify
 * Signature: (Ijava.lang.String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1child_1notify (JNIEnv *env, jclass cls, jobject widget, jstring childProperty) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gchar* childProperty_g = 
        (gchar*)(*env)->GetStringUTFChars(env, childProperty, 0);
    gtk_widget_child_notify (widget_g, childProperty_g);
    if (childProperty) 
        (*env)->ReleaseStringUTFChars(env, childProperty, childProperty_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_thaw_child_notify
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1thaw_1child_1notify (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_thaw_child_notify (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_is_focus
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1is_1focus (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean) (gtk_widget_is_focus (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_grab_focus
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1grab_1focus (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_grab_focus (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_grab_default
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1grab_1default (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GTK_WIDGET_SET_FLAGS (widget_g, GTK_CAN_DEFAULT);
    gtk_widget_grab_default (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_name
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1name (JNIEnv *env, jclass cls, jobject widget, jstring name) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    const gchar* name_g = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_widget_set_name (widget_g, name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_name
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1name (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (*env)->NewStringUTF( env, (gchar*)gtk_widget_get_name (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_state
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1state (JNIEnv *env, jclass cls, jobject widget, jint state) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStateType state_g = (GtkStateType) state;
    gtk_widget_set_state (widget_g, state_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_sensitive
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1sensitive (JNIEnv *env, jclass cls, jobject widget, jboolean sensitive) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean sensitive_g = (gboolean) sensitive;
    gtk_widget_set_sensitive (widget_g, sensitive_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_app_paintable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1app_1paintable (JNIEnv *env, jclass cls, jobject widget, jboolean appPaintable) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean appPaintable_g = (gboolean) appPaintable;
    gtk_widget_set_app_paintable (widget_g, appPaintable_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_double_buffered
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1double_1buffered (JNIEnv *env, jclass cls, jobject widget, jboolean doubleBuffered) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean doubleBuffered_g = (gboolean) doubleBuffered;
    gtk_widget_set_double_buffered (widget_g, doubleBuffered_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_redraw_on_allocate
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1redraw_1on_1allocate (JNIEnv *env, jclass cls, jobject widget, jboolean redrawOnAllocate) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean redrawOnAllocate_g = (gboolean) redrawOnAllocate;
    gtk_widget_set_redraw_on_allocate (widget_g, redrawOnAllocate_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_parent
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1parent (JNIEnv *env, jclass cls, jobject widget, jobject parent) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkWidget *parent_g = (GtkWidget *)getPointerFromHandle(env, parent);
    gtk_widget_set_parent (widget_g, parent_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_parent_window
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1parent_1window (JNIEnv *env, jclass cls, jobject widget, jobject parent) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkWindow *parent_g = (GdkWindow *)getPointerFromHandle(env, parent);
    gtk_widget_set_parent_window (widget_g, parent_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_child_visible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1child_1visible (JNIEnv *env, jclass cls, jobject widget, jboolean isVisible) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gboolean isVisible_g = (gboolean) isVisible;
    gtk_widget_set_child_visible (widget_g, isVisible_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_child_visible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1child_1visible (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean) (gtk_widget_get_child_visible (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_parent
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1parent (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandle(env, (GObject *) gtk_widget_get_parent (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_parent_window
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1parent_1window (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_parent_window (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_window
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1window (JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GObject *obj = (GObject *) (GTK_WIDGET_NO_WINDOW(widget_g) ? NULL : widget_g->window);
    return getGObjectHandleAndRef(env, obj);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_child_focus
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1child_1focus (JNIEnv *env, jclass cls, jobject widget, jint direction) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkDirectionType direction_g = (GtkDirectionType) direction;
    return (jboolean) (gtk_widget_child_focus (widget_g, direction_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_size_request
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1size_1request (JNIEnv *env, jclass cls, jobject widget, jint width, jint height) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_widget_set_size_request (widget_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_size_request
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1size_1request (JNIEnv *env, jclass cls, jobject widget, jintArray width, jintArray height) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_widget_get_size_request (widget_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_events
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1events (JNIEnv *env, jclass cls, jobject widget, jint events) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 events_g = (gint32) events;
    gtk_widget_set_events (widget_g, events_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_add_events
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1add_1events (JNIEnv *env, jclass cls, jobject widget, jint events) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint32 events_g = (gint32) events;
    gtk_widget_add_events (widget_g, events_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_extension_events
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1extension_1events (JNIEnv *env, jclass cls, jobject widget, jint mode) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkExtensionMode mode_g = (GdkExtensionMode) mode;
    gtk_widget_set_extension_events (widget_g, mode_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_extension_events
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1extension_1events (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jint) (gtk_widget_get_extension_events (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_toplevel
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1toplevel (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandle(env, (GObject *) gtk_widget_get_toplevel (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_ancestor
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1ancestor(JNIEnv *env, jclass cls, jobject widget, jint widgetType) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GType widgetType_g = (GType)widgetType;
    return getGObjectHandle(env, (GObject *) gtk_widget_get_ancestor(widget_g, widgetType_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_colormap
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1colormap (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_colormap (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_visual
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1visual (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_visual (widget_g));
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_get_screen
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1screen
		(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_screen(widget_g));
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_has_screen
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1has_1screen
		(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean)gtk_widget_has_screen(widget_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_get_display
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1display
		(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_display(widget_g));
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_get_root_window
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1root_1window(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_root_window(widget_g));
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_get_sensitive
 * Signature: (I)I
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1sensitive
		( JNIEnv *env, jclass cls, jobject widget )
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean) (GTK_WIDGET_SENSITIVE(widget_g));
}
	
/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_settings
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1settings
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_settings (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_accessible
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1accessible
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_accessible (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_colormap
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1colormap (JNIEnv *env, jclass cls, jobject widget, jobject cmap) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkColormap *cmap_g = (GdkColormap *)getPointerFromHandle(env, cmap);
    gtk_widget_set_colormap (widget_g, cmap_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_events
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1events (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jint) (gtk_widget_get_events (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_pointer
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1pointer (JNIEnv *env, jclass cls, jobject widget, jintArray x, jintArray y) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gtk_widget_get_pointer (widget_g, x_g, y_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_is_ancestor
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1is_1ancestor (JNIEnv *env, jclass cls, jobject widget, jobject ancestor) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkWidget *ancestor_g = (GtkWidget *)getPointerFromHandle(env, ancestor);
    return (jboolean) (gtk_widget_is_ancestor (widget_g, ancestor_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_translate_coordinates
 * Signature: (IIII[Lint ;[Lint ;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1translate_1coordinates (JNIEnv *env, jclass cls, jobject srcWidget, jobject destWidget, jint srcX, jint srcY, jintArray destX, jintArray destY) 
{
    GtkWidget *srcWidget_g = 
        (GtkWidget *)getPointerFromHandle(env, srcWidget);
    GtkWidget *destWidget_g = 
        (GtkWidget *)getPointerFromHandle(env, destWidget);
    gint32 srcX_g = (gint32) srcX;
    gint32 srcY_g = (gint32) srcY;
    gint *destX_g = (gint *) (*env)->GetIntArrayElements (env, destX, NULL);
    gint *destY_g = (gint *) (*env)->GetIntArrayElements (env, destY, NULL);
    jboolean result_j = 
        (jboolean) (gtk_widget_translate_coordinates (srcWidget_g, 
                                                      destWidget_g, 
                                                      srcX_g, srcY_g, 
                                                      destX_g, destY_g));
    (*env)->ReleaseIntArrayElements (env, destX, (jint *) destX_g, 0);
    (*env)->ReleaseIntArrayElements (env, destY, (jint *) destY_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_hide_on_delete
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1hide_1on_1delete (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jboolean) (gtk_widget_hide_on_delete (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_style
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1style
		(JNIEnv *env, jclass cls, jobject widget, jobject style) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStyle *style_g = (GtkStyle *)getPointerFromHandle(env, style);
    gtk_widget_set_style (widget_g, style_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_ensure_style
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1ensure_1style (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_ensure_style (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_style
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1style
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_style (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_style
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1style
		(JNIEnv *env, jclass cls, jobject widget, jobject style) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkRcStyle *style_g = (GtkRcStyle *)getPointerFromHandle(env, style);
    gtk_widget_modify_style (widget_g, style_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_modifier_style
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1modifier_1style
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_modifier_style (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_fg
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1fg (JNIEnv *env, jclass cls, jobject widget, jint state, jobject color) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStateType state_g = (GtkStateType) state;
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_widget_modify_fg (widget_g, state_g, color_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_bg
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1bg (JNIEnv *env, jclass cls, jobject widget, jint state, jobject color) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStateType state_g = (GtkStateType) state;
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_widget_modify_bg (widget_g, state_g, color_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_text
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1text (JNIEnv *env, jclass cls, jobject widget, jint state, jobject color) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStateType state_g = (GtkStateType) state;
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_widget_modify_text (widget_g, state_g, color_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_base
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1base (JNIEnv *env, jclass cls, jobject widget, jint state, jobject color) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkStateType state_g = (GtkStateType) state;
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_widget_modify_base (widget_g, state_g, color_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_modify_font
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1modify_1font (JNIEnv *env, jclass cls, jobject widget, jobject fontDesc) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    PangoFontDescription *fontDesc_g = 
        (PangoFontDescription *)getPointerFromHandle(env, fontDesc);
    gtk_widget_modify_font (widget_g, fontDesc_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_create_pango_context
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1create_1pango_1context
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandle(env, (GObject *) gtk_widget_create_pango_context (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_pango_context
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1pango_1context
		(JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_pango_context (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_create_pango_layout
 * Signature: (I[B)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1create_1pango_1layout
		(JNIEnv *env, jclass cls, jobject widget, jstring text) 
{
	PangoLayout *layout;
	jobject retval;
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
    layout = gtk_widget_create_pango_layout (widget_g, text_g);
    retval = getGObjectHandle(env, (GObject *) layout);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
    return retval;
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_render_icon
 * Signature: (I[BI[B)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1render_1icon (JNIEnv *env, jclass cls, jobject widget, jstring stockID, jint size, jstring detail) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkIconSize size_g = (GtkIconSize) size;
    const gchar* stockID_g = (*env)->GetStringUTFChars(env, stockID, NULL);
    const gchar* detail_g = (*env)->GetStringUTFChars(env, detail, NULL);
    jobject retval = 
        getGObjectHandleAndRef(env, (GObject *) gtk_widget_render_icon (widget_g,
        		stockID_g, size_g, detail_g));
    (*env)->ReleaseStringUTFChars( env, stockID, stockID_g );
    (*env)->ReleaseStringUTFChars( env, detail, detail_g );
    return retval;
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_composite_name
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1composite_1name (JNIEnv *env, jclass cls, jobject widget, jstring name) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    const gchar* name_g = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_widget_set_composite_name (widget_g, name_g);
    (*env)->ReleaseStringUTFChars( env, name, name_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_composite_name
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1composite_1name (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (*env)->NewStringUTF( env, gtk_widget_get_composite_name (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_reset_rc_styles
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1reset_1rc_1styles (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_widget_reset_rc_styles (widget_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_push_colormap
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1push_1colormap (JNIEnv *env, jclass cls, jobject cmap) 
{
    GdkColormap *cmap_g = (GdkColormap *)getPointerFromHandle(env, cmap);
    gtk_widget_push_colormap (cmap_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_pop_colormap
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1pop_1colormap (JNIEnv *env, jclass cls) 
{
    gtk_widget_pop_colormap ();
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_push_composite_child
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1push_1composite_1child (JNIEnv *env, jclass cls) 
{
    gtk_widget_push_composite_child ();
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_pop_composite_child
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1pop_1composite_1child (JNIEnv *env, jclass cls) 
{
    gtk_widget_pop_composite_child ();
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_style_get_property
 * Signature: (I[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1style_1get_1property (JNIEnv *env, jclass cls, jobject widget, jstring propertyName, jobject value) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GValue *value_g = (GValue *)getPointerFromHandle(env, value);
    const gchar* propertyName_g = (*env)->GetStringUTFChars(env, propertyName, NULL);
    gtk_widget_style_get_property (widget_g, propertyName_g, value_g);
    (*env)->ReleaseStringUTFChars(env, propertyName, propertyName_g );
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_default_colormap
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1default_1colormap (JNIEnv *env, jclass cls, jobject cmap) 
{
    GdkColormap *cmap_g = (GdkColormap *)getPointerFromHandle(env, cmap);
    gtk_widget_set_default_colormap (cmap_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_default_colormap
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1default_1colormap
		(JNIEnv *env, jclass cls) 
{
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_default_colormap ());
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_default_visual
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1default_1visual
		(JNIEnv *env, jclass cls) 
{
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_default_visual ());
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_default_style
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1default_1style
		(JNIEnv *env, jclass cls) 
{
    return getGObjectHandleAndRef(env, (GObject *) gtk_widget_get_default_style ());
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_direction
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1direction
		(JNIEnv *env, jclass cls, jobject widget, jint dir) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkTextDirection dir_g = (GtkTextDirection) dir;
    gtk_widget_set_direction (widget_g, dir_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_direction
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1direction (JNIEnv *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return (jint) (gtk_widget_get_direction (widget_g));
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_set_default_direction
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1set_1default_1direction (JNIEnv *env, jclass cls, jint dir) 
{
    GtkTextDirection dir_g = (GtkTextDirection) dir;
    gtk_widget_set_default_direction (dir_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_get_default_direction
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1default_1direction (JNIEnv *env, jclass cls) 
{
    return (jint) (gtk_widget_get_default_direction ());
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_shape_combine_mask
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1shape_1combine_1mask (JNIEnv *env, jclass cls, jobject widget, jobject shapeMask, jint offsetX, jint offsetY) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkBitmap *shapeMask_g = (GdkBitmap *)getPointerFromHandle(env, shapeMask);
    gint32 offsetX_g = (gint32) offsetX;
    gint32 offsetY_g = (gint32) offsetY;
    gtk_widget_shape_combine_mask (widget_g, shapeMask_g, offsetX_g, offsetY_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_path
 * Signature: (I[Lint ;[java/lang/String;[Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1path (JNIEnv *env, jclass cls, jobject widget, jintArray pathLength, jobjectArray path, jobjectArray pathReversed) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    guint *pathLength_g = (guint *) (*env)->GetIntArrayElements (env, pathLength, NULL);
    gchar **path_g = getStringArray(env, path);
    gchar **pathReversed_g = getStringArray(env, pathReversed);
    gtk_widget_path (widget_g, pathLength_g, path_g, pathReversed_g);
    (*env)->ReleaseIntArrayElements (env, pathLength, (jint *) pathLength_g, 0);
    freeStringArray(env, path, path_g);
    freeStringArray(env, pathReversed, pathReversed_g);
}

/*
 * Class:     org.gnu.gtk.Widget
 * Method:    gtk_widget_class_path
 * Signature: (I[Lint ;[Ljava/lang/String;[Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1class_1path (JNIEnv *env, jclass cls, jobject widget, jintArray pathLength, jobjectArray path, jobjectArray pathReversed) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    guint *pathLength_g = (guint *) (*env)->GetIntArrayElements (env, pathLength, NULL);
    gchar **path_g = getStringArray(env, path);
    gchar **pathReversed_g = getStringArray(env, pathReversed);
    gtk_widget_class_path (widget_g, pathLength_g, path_g, pathReversed_g);
    (*env)->ReleaseIntArrayElements (env, pathLength, (jint *) pathLength_g, 0);
    freeStringArray(env, path, path_g);
    freeStringArray(env, pathReversed, pathReversed_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_list_mnemonic_labels
 * Signature: (I)[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1list_1mnemonic_1labels(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandlesFromGList(env, gtk_widget_list_mnemonic_labels(widget_g));
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_add_mnemonic_label
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1add_1mnemonic_1label(JNIEnv *env, jclass cls, jobject widget, jobject label)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkWidget *label_g = (GtkWidget *)getPointerFromHandle(env, label);
    gtk_widget_add_mnemonic_label(widget_g, label_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_remove_mnemonic_label
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1remove_1mnemonic_1label(JNIEnv *env, jclass cls, jobject widget, jobject label)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkWidget *label_g = (GtkWidget *)getPointerFromHandle(env, label);
    gtk_widget_remove_mnemonic_label(widget_g, label_g);
}


/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_widget_get_allocation
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Widget_gtk_1widget_1get_1allocation (JNIEnv *env, jclass cls, 
 jobject widget)
{
    jclass allocationClass;
    jmethodID constructor;

    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);

    allocationClass = (*env)->FindClass(env, "org/gnu/gtk/Allocation");
	 
     if ( NULL == allocationClass )
        return (jobject)NULL;
	 
    constructor = (*env)->GetMethodID(env, allocationClass, "<init>", "(IIII)V");
	 
    if ( NULL == constructor)
       return (jobject)NULL;

    return (*env)->NewObject(env, allocationClass, constructor, 
							 widget_g->allocation.x, widget_g->allocation.y,
							 widget_g->allocation.width, widget_g->allocation.height);
}

/****************************************
 * Drag and Drop Support
 ****************************************/
	
/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_source_set
 * Signature: (II[II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1source_1set(JNIEnv *env, jclass cls, jobject widget, jint buttonMask, jobjectArray targets, jint actions)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkModifierType buttonMask_g = (GdkModifierType)buttonMask;
    GdkDragAction actions_g = (GdkDragAction)actions;
    const GtkTargetEntry* entries = (GtkTargetEntry *)getArrayFromHandles(env,
    	targets, sizeof(GtkTargetEntry), JNI_FALSE, JNI_FALSE);
    jsize len = (*env)->GetArrayLength(env, targets);
    gtk_drag_source_set(widget_g, buttonMask_g, entries, (gint) len, actions_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_source_unset
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1source_1unset(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_drag_source_unset(widget_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_source_set_icon
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1source_1set_1icon(JNIEnv *env, jclass cls, jobject widget, jobject colormap, jobject pixmap, jobject mask)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkColormap *colormap_g = 
        (GdkColormap *)getPointerFromHandle(env, colormap);
    GdkPixmap *pixmap_g = (GdkPixmap *)getPointerFromHandle(env, pixmap);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    gtk_drag_source_set_icon(widget_g, colormap_g, pixmap_g, mask_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_source_set_icon_pixbuf
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1source_1set_1icon_1pixbuf(JNIEnv *env, jclass cls, jobject widget, jobject pixbuf)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gtk_drag_source_set_icon_pixbuf(widget_g, pixbuf_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_source_set_icon_stock
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1source_1set_1icon_1stock(JNIEnv *env, jclass cls, jobject widget, jstring stockId)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
    gtk_drag_source_set_icon_stock(widget_g, s);
    (*env)->ReleaseStringUTFChars(env, stockId, s);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_get_data
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1get_1data(JNIEnv *env, jclass cls, jobject widget, jobject context, jobject target, jint time)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkDragContext *context_g = 
        (GdkDragContext *)getPointerFromHandle(env, context);
    
    GdkAtom target_g = getPointerFromHandle(env, target);
    //(GdkAtom)target;
    gtk_drag_get_data(widget_g, context_g, target_g, time);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_finish
 * Signature: (IZZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1finish(JNIEnv *env, jclass cls, jobject context, jboolean success, jboolean del, jint time)
{
    GdkDragContext *context_g = 
        (GdkDragContext *)getPointerFromHandle(env, context);
    gboolean success_g = (gboolean)success;
    gboolean del_g = (gboolean)del;
    gtk_drag_finish(context_g, success_g, del_g, time);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_highlight
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1highlight(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_drag_highlight(widget_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_unhighlight
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1unhighlight(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_drag_unhighlight(widget_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_dest_set
 * Signature: (II[II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1dest_1set(JNIEnv *env, jclass cls, jobject widget, jint flags, jobjectArray targets, jint actions)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GtkDestDefaults flags_g = (GtkDestDefaults)flags;
    const GdkDragAction actions_g = (GdkDragAction)actions;
    const GtkTargetEntry* entries = (GtkTargetEntry *)getArrayFromHandles(env, targets, sizeof(GtkTargetEntry), JNI_FALSE, JNI_FALSE);
    jsize len = (*env)->GetArrayLength(env, targets);
    gtk_drag_dest_set(widget_g, flags_g, entries, (gint)len, actions_g);
}

/*
 * Class:     org_gnu_gtk_Widget
 * Method:    gtk_drag_dest_unset
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Widget_gtk_1drag_1dest_1unset(JNIEnv *env, jclass cls, jobject widget)
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_drag_dest_unset(widget_g);
}

#ifdef __cplusplus
}

#endif
