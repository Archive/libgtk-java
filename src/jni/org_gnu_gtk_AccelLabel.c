/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AccelLabel.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_accel_label_get_type ();
}

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1new (JNIEnv *env, jclass 
    cls, jstring str) 
{
    const gchar* str_g = (*env)->GetStringUTFChars(env, str, NULL);
	jobject retval = getGObjectHandle(env, (GObject *) gtk_accel_label_new (str_g));
	(*env)->ReleaseStringUTFChars(env, str, str_g);
	return retval;
}

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_get_accel_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1get_1accel_1widget (
    JNIEnv *env, jclass cls, jobject accel_label) 
{
    GtkAccelLabel *accel_label_g = (GtkAccelLabel *)getPointerFromHandle(env, accel_label);
    return getGObjectHandle(env, (GObject *) gtk_accel_label_get_accel_widget (accel_label_g));
}

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_set_accel_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1set_1accel_1widget (
    JNIEnv *env, jclass cls, jobject accel_label, jobject accelWidget) 
{
    GtkAccelLabel *accel_label_g = (GtkAccelLabel *)getPointerFromHandle(env, accel_label);
    GtkWidget *accelWidget_g = (GtkWidget *)getPointerFromHandle(env, accelWidget);
    gtk_accel_label_set_accel_widget (accel_label_g, accelWidget_g);
}

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_get_accel_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1get_1accel_1width (JNIEnv 
    *env, jclass cls, jobject accel_label) 
{
    GtkAccelLabel *accel_label_g = (GtkAccelLabel *)getPointerFromHandle(env, accel_label);
    return (jint) (gtk_accel_label_get_accel_width (accel_label_g));
}

/*
 * Class:     org.gnu.gtk.AccelLabel
 * Method:    gtk_accel_label_refetch
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AccelLabel_gtk_1accel_1label_1refetch (JNIEnv *env, 
    jclass cls, jobject accel_label) 
{
    GtkAccelLabel *accel_label_g = (GtkAccelLabel *)getPointerFromHandle(env, accel_label);
    return (jboolean) (gtk_accel_label_refetch (accel_label_g));
}

#ifdef __cplusplus
}

#endif
