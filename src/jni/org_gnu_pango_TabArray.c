/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_TabArray.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jint JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1tabAlignment (JNIEnv *env, 
    jclass cls, jobject tabArray, jint tabIndex) 
{
    PangoTabArray *tabArray_g = 
        (PangoTabArray *)getPointerFromHandle(env, tabArray);
    gint32 tabIndex_g = (gint32) tabIndex;
    //    PangoAlignment *alignment_g = (PangoAlignment *) g_malloc(sizeof(PangoAlignment));
    PangoTabAlign alignment_g;
    pango_tab_array_get_tab (tabArray_g, tabIndex_g, 
			     &alignment_g, NULL);
    return (jint) alignment_g;
}

JNIEXPORT jint JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1tabLocation (JNIEnv *env, 
    jclass cls, jobject tabArray, jint tabIndex) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    gint32 tabIndex_g = (gint32) tabIndex;
    //    gint *location_g = NULL; 
    gint location_g; 
    pango_tab_array_get_tab (tabArray_g, tabIndex_g, 
			     NULL, &location_g);
    return (jint) location_g;
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1new (JNIEnv *env, jclass 
    cls, jint initialSize, jboolean positionInPixels) 
{
    gint32 initialSize_g = (gint32) initialSize;
    gboolean positionInPixels_g = (gboolean) positionInPixels;
    return getGBoxedHandle(env, pango_tab_array_new (initialSize_g, 
                                                     positionInPixels_g),
                           PANGO_TYPE_TAB_ARRAY,
                           NULL, (JGFreeFunc)pango_tab_array_free);
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)pango_tab_array_get_type ();
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1copy (JNIEnv *env, jclass 
    cls, jobject src) 
{
    PangoTabArray *src_g = (PangoTabArray *)getPointerFromHandle(env, src);
    return getGBoxedHandle(env, pango_tab_array_copy (src_g),
                           PANGO_TYPE_TAB_ARRAY,
                           NULL, (JGFreeFunc)pango_tab_array_free);
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_get_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1size (JNIEnv *env, 
    jclass cls, jobject tabArray) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    return (jint) (pango_tab_array_get_size (tabArray_g));
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_resize
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1resize (JNIEnv *env, 
    jclass cls, jobject tabArray, jint newSize) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    gint32 newSize_g = (gint32) newSize;
    pango_tab_array_resize (tabArray_g, newSize_g);
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_set_tab
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1set_1tab (JNIEnv *env, 
    jclass cls, jobject tabArray, jint tabIndex, jint alignment, jint location) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    gint32 tabIndex_g = (gint32) tabIndex;
    PangoTabAlign alignment_g = (PangoTabAlign)alignment;
    gint32 location_g = (gint32) location;
    pango_tab_array_set_tab (tabArray_g, tabIndex_g, alignment_g, location_g);
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_get_tab
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1tab (JNIEnv *env, 
    jclass cls, jobject tabArray, jint tabIndex, jint alignment, jintArray location) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    gint32 tabIndex_g = (gint32) tabIndex;
    PangoTabAlign alignment_g = (PangoTabAlign)alignment;
    gint *location_g = (gint *) (*env)->GetIntArrayElements (env, location, NULL);
    pango_tab_array_get_tab (tabArray_g, tabIndex_g, &alignment_g, location_g);
    (*env)->ReleaseIntArrayElements (env, location, (jint *) location_g, 0);
}

/*
 * Class:     org.gnu.pango.TabArray
 * Method:    pango_tab_array_get_positions_in_pixels
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_pango_TabArray_pango_1tab_1array_1get_1positions_1in_1pixels (JNIEnv *env, jclass 
    cls, jobject tabArray) 
{
    PangoTabArray *tabArray_g = (PangoTabArray *)getPointerFromHandle(env, tabArray);
    return (jboolean) (pango_tab_array_get_positions_in_pixels (tabArray_g));
}

#ifdef __cplusplus
}

#endif
