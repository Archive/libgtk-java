/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventExpose.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventExpose
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventExpose_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventExpose *obj_g = (GdkEventExpose *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventExpose
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventExpose_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventExpose *obj_g = (GdkEventExpose *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventExpose
 * Method:    getRegion
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventExpose_getRegion (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventExpose *obj_g = (GdkEventExpose *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->region, 
                           (JGCopyFunc)gdk_region_copy, 
                           (JGFreeFunc)gdk_region_destroy);
}

/*
 * Class:     org.gnu.gdk.EventExpose
 * Method:    getArea
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventExpose_getArea (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventExpose *obj_g = (GdkEventExpose *)getPointerFromHandle(env, obj);
    GdkRectangle *rect = g_new( GdkRectangle, 1 );
    rect->x = obj_g->area.x;
    rect->y = obj_g->area.y;
    rect->width = obj_g->area.width;
    rect->height = obj_g->area.height;
    return getGBoxedHandle(env, rect, GDK_TYPE_RECTANGLE,
                           NULL,
                           (GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventExpose
 * Method:    getCount
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventExpose_getCount (JNIEnv *env, jclass cls, jobject obj)
{
    GdkEventExpose *obj_g = (GdkEventExpose *)getPointerFromHandle(env, obj);
    return (jint) obj_g->count;
}


#ifdef __cplusplus
}

#endif
