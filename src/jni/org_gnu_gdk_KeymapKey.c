/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_KeymapKey.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.KeymapKey
 * Method:    getKeycode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_KeymapKey_getKeycode (JNIEnv *env, jclass cls, jobject obj)
{
    GdkKeymapKey *obj_g = (GdkKeymapKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->keycode;
}

/*
 * Class:     org.gnu.gdk.KeymapKey
 * Method:    getGroup
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_KeymapKey_getGroup (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkKeymapKey *obj_g = (GdkKeymapKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->group;
}

/*
 * Class:     org.gnu.gdk.KeymapKey
 * Method:    getLevel
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_KeymapKey_getLevel (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkKeymapKey *obj_g = (GdkKeymapKey *)getPointerFromHandle(env, obj);
    return (jint) obj_g->level;
}


#ifdef __cplusplus
}

#endif
