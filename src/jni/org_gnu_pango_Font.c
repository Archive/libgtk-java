/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#include "org_gnu_pango_Font.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Font
 * Method:    pango_font_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Font_pango_1font_1get_1type 
  (JNIEnv *env, jclass cls) 
{
    return (jint)pango_font_get_type ();
}

/*
 * Class:     org.gnu.pango.Font
 * Method:    pango_font_describe
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Font_pango_1font_1describe 
  (JNIEnv *env, jclass cls, jobject font) 
{
    PangoFont *font_g = (PangoFont *)getPointerFromHandle(env, font);
    return (jobject)getGBoxedHandle(env, pango_font_describe (font_g), 
                                    PANGO_TYPE_FONT_DESCRIPTION, 
                                    NULL, (JGFreeFunc) pango_font_description_free);
}

/*
 * Class:     org.gnu.pango.Font
 * Method:    pango_font_get_coverage
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Font_pango_1font_1get_1coverage 
  (JNIEnv *env, jclass cls, jobject font, jobject language) 
{
    PangoFont *font_g = (PangoFont *)getPointerFromHandle(env, font);
    PangoLanguage *language_g = 
        (PangoLanguage *)getPointerFromHandle(env, language);
    return (jobject)getStructHandle(env, 
                                    pango_font_get_coverage(font_g, 
                                                            language_g),
                                    (JGCopyFunc) pango_coverage_ref, (JGFreeFunc) pango_coverage_unref);
}

/*
 * Class:     org.gnu.pango.Font
 * Method:    pango_font_get_metrics
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Font_pango_1font_1get_1metrics 
  (JNIEnv *env, jclass cls, jobject font, jobject language) 
{
    PangoFont *font_g = (PangoFont *)getPointerFromHandle(env, font);
    PangoLanguage *language_g = 
        (PangoLanguage *)getPointerFromHandle(env, language);
    return (jobject)getGBoxedHandle(env, 
                                    pango_font_get_metrics (font_g, 
                                                            language_g),
                                    PANGO_TYPE_FONT_METRICS, 
                                    NULL, (JGFreeFunc) pango_font_metrics_unref);
}

#ifdef __cplusplus
}

#endif
