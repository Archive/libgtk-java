/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_ToggleToolButton
#define _Included_org_gnu_gtk_ToggleToolButton
#include "org_gnu_gtk_ToggleToolButton.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_ToggleToolButton
 * Method:    gtk_toggle_tool_button_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToggleToolButton_gtk_1toggle_1tool_1button_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_toggle_tool_button_get_type();
}

/*
 * Class:     org_gnu_gtk_ToggleToolButton
 * Method:    gtk_toggle_tool_button_new
 * Signature: ()Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToggleToolButton_gtk_1toggle_1tool_1button_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_toggle_tool_button_new());
}

/*
 * Class:     org_gnu_gtk_ToggleToolButton
 * Method:    gtk_toggle_tool_button_new_from_stock
 * Signature: (Ljava/lang/String;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToggleToolButton_gtk_1toggle_1tool_1button_1new_1from_1stock
  (JNIEnv *env, jclass cls, jstring stock)
{
	const gchar *stock_g;
	GtkToolItem *item;
	
	stock_g = (const gchar*)(*env)->GetStringUTFChars(env, stock, NULL);
	item = gtk_toggle_tool_button_new_from_stock(stock_g);
	(*env)->ReleaseStringUTFChars(env, stock, stock_g);
	return getGObjectHandle(env, (GObject *) item);
}

/*
 * Class:     org_gnu_gtk_ToggleToolButton
 * Method:    gtk_toggle_tool_button_set_active
 * Signature: (Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToggleToolButton_gtk_1toggle_1tool_1button_1set_1active
  (JNIEnv *env, jclass cls, jobject button, jboolean active)
{
	GtkToggleToolButton *button_g;
	
	button_g = getPointerFromHandle(env, button);
	gtk_toggle_tool_button_set_active(button_g, (gboolean)active);
}

/*
 * Class:     org_gnu_gtk_ToggleToolButton
 * Method:    gtk_toggle_tool_button_get_active
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToggleToolButton_gtk_1toggle_1tool_1button_1get_1active
  (JNIEnv * env, jclass cls, jobject button)
{
	GtkToggleToolButton *button_g;
	
	button_g = getPointerFromHandle(env, button);
	return (jboolean)gtk_toggle_tool_button_get_active(button_g);
}

#ifdef __cplusplus
}
#endif
#endif
