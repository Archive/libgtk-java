/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gdk_Screen
#define _Included_org_gnu_gdk_Screen
#include "org_gnu_gdk_Screen.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gdk_screen_get_type();
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_default_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1default_1colormap
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_default_colormap(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_set_default_colormap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1set_1default_1colormap
  (JNIEnv *env, jclass cls, jobject screen, jobject colormap)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	GdkColormap* colormap_g = (GdkColormap*)getPointerFromHandle(env, colormap);
	gdk_screen_set_default_colormap(screen_g, colormap_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_system_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1system_1colormap
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_system_colormap(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_system_visual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1system_1visual
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_system_visual(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_rgb_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1rgb_1colormap
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_rgb_colormap(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_rgba_colormap
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1rgba_1colormap
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen *screen_g;
	GdkColormap *cmap;
	
	screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	cmap = gdk_screen_get_rgba_colormap(screen_g);
	
	return getGObjectHandle(env, (GObject *)cmap);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_rgb_visual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1rgb_1visual
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_rgb_visual(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_rgba_visual
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1rgba_1visual
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen *screen_g;
	GdkVisual *visual;
	
	screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	visual = gdk_screen_get_rgba_visual(screen_g);
	
	return getGObjectHandle(env, (GObject *)visual);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_root_window
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1root_1window
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_root_window(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_display
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1display
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *)gdk_screen_get_display(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_number
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1number
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_number(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_height
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1height
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_height(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1width
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_width(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_height_mm
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1height_1mm
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_height_mm(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_width_mm
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1width_1mm
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_width_mm(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_list_visuals
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1list_1visuals
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandlesFromGList(env, gdk_screen_list_visuals(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_toplevel_windows
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1toplevel_1windows
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandlesFromGList(env,gdk_screen_get_toplevel_windows(screen_g));
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_make_display_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1make_1display_1name
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	gchar* name = gdk_screen_make_display_name(screen_g);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_n_monitors
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1n_1monitors
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_n_monitors(screen_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_monitor_geometry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1monitor_1geometry
  (JNIEnv *env, jclass cls, jobject screen, jint monitorNum)
{
	GdkScreen *screen_g;
    GdkRectangle *rect;
    screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
    rect = (GdkRectangle *) g_new(GdkRectangle, 1);
	gdk_screen_get_monitor_geometry(screen_g, (gint)monitorNum, rect);
	return getGBoxedHandle(env, rect, GDK_TYPE_RECTANGLE, NULL,
			(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_monitor_at_point
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1monitor_1at_1point
  (JNIEnv *env, jclass cls, jobject screen, jint x, jint y)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return (jint)gdk_screen_get_monitor_at_point(screen_g, (gint)x, (gint)y);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_monitor_at_window
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1monitor_1at_1window
  (JNIEnv *env, jclass cls, jobject screen, jobject window)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	GdkWindow* window_g = (GdkWindow*)getPointerFromHandle(env, window);
	return (jint)gdk_screen_get_monitor_at_window(screen_g, window_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_broadcast_client_message
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1broadcast_1client_1message
  (JNIEnv *env, jclass cls, jobject screen, jobject event)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	GdkEvent* event_g = (GdkEvent*)getPointerFromHandle(env, event);
	gdk_screen_broadcast_client_message(screen_g, event_g);
}

/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1default
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *)gdk_screen_get_default());
}

#ifdef ZERO
/*
 * Class:     org_gnu_gdk_Screen
 * Method:    gdk_screen_get_setting
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Screen_gdk_1screen_1get_1setting
  (JNIEnv *env, jclass cls, jobject screen, jstring name, jobject value)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	GValue* value_g = (GValue*)getPointerFromHandle(env, value);
	jboolean bool = (jboolean)gdk_screen_get_setting(screen_g, n, value_g);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return bool;
}
#endif

#ifdef __cplusplus
}
#endif
#endif
