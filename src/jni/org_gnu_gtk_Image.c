/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Image.h"
#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_image_get_type ();
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_image_new ());
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_pixmap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1pixmap (JNIEnv *env, 
    jclass cls, jobject pixmap, jobject mask) 
{
    GdkPixmap *pixmap_g = (GdkPixmap *)getPointerFromHandle(env, pixmap);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    return getGObjectHandle(env, (GObject *) gtk_image_new_from_pixmap (pixmap_g, mask_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1image (JNIEnv *env, jclass 
    cls, jobject image, jobject mask) 
{
    GdkImage *image_g = (GdkImage *)getPointerFromHandle(env, image);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    return getGObjectHandle(env, (GObject *) gtk_image_new_from_image (image_g, mask_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_file
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1file (JNIEnv *env, jclass 
    cls, jstring filename) 
{
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
	jobject result = getGObjectHandle(env, (GObject *) gtk_image_new_from_file (filename_g));
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1pixbuf (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return getGObjectHandle(env, (GObject *) gtk_image_new_from_pixbuf (pixbuf_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_stock
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1stock (JNIEnv *env, jclass 
    cls, jstring stockId, jint size) 
{
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_image_new_from_stock (stockId_g, (GtkIconSize)size));
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_icon_set
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1icon_1set (JNIEnv *env, 
    jclass cls, jobject iconSet, jint size) 
{
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    GtkIconSize size_g = (GtkIconSize) size;
    return getGObjectHandle(env, (GObject *) gtk_image_new_from_icon_set (iconSet_g, size_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_new_from_animation
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1animation (JNIEnv *env, 
    jclass cls, jobject animation) 
{
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    return getGObjectHandle(env, (GObject *) gtk_image_new_from_animation (animation_g));
}

/*
 * Class:     org_gnu_gtk_Image
 * Method:    gtk_image_new_from_icon_name
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1new_1from_1icon_1name
  (JNIEnv *env, jclass cls, jstring name, jint size)
{
	const gchar *name_g = (*env)->GetStringUTFChars(env, name, NULL);
	jobject obj = getGObjectHandle(env, (GObject *)
			gtk_image_new_from_icon_name(name_g, (GtkIconSize)size));
	(*env)->ReleaseStringUTFChars(env, name, name_g);
	return obj;
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_pixmap
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1pixmap (JNIEnv *env, 
    jclass cls, jobject image, jobject pixmap, jobject mask) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GdkPixmap *pixmap_g = (GdkPixmap *)getPointerFromHandle(env, pixmap);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    gtk_image_set_from_pixmap (image_g, pixmap_g, mask_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1image (JNIEnv *env, jclass 
    cls, jobject image, jobject gdkImage, jobject mask) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GdkImage *gdkImage_g = (GdkImage *)getPointerFromHandle(env, gdkImage);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    gtk_image_set_from_image (image_g, gdkImage_g, mask_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_file
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1file (JNIEnv *env, jclass 
    cls, jobject image, jstring filename) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gtk_image_set_from_file (image_g, filename_g);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_pixbuf
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1pixbuf (JNIEnv *env, 
    jclass cls, jobject image, jobject pixbuf) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gtk_image_set_from_pixbuf (image_g, pixbuf_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_stock
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1stock (JNIEnv *env, jclass 
    cls, jobject image, jstring stockId, jint size) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    gtk_image_set_from_stock (image_g, stockId_g, (GtkIconSize )size);
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_icon_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1icon_1set (JNIEnv *env, 
    jclass cls, jobject image, jobject iconSet, jint size) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GtkIconSet *iconSet_g = (GtkIconSet *)getPointerFromHandle(env, iconSet);
    GtkIconSize size_g = (GtkIconSize) size;
    gtk_image_set_from_icon_set (image_g, iconSet_g, size_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_set_from_animation
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1animation (JNIEnv *env, 
    jclass cls, jobject image, jobject animation) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GdkPixbufAnimation *animation_g = (GdkPixbufAnimation *)getPointerFromHandle(env, animation);
    gtk_image_set_from_animation (image_g, animation_g);
}

/*
 * Class:     org_gnu_gtk_Image
 * Method:    gtk_image_set_from_icon_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1from_1icon_1name
  (JNIEnv *env, jclass cls, jobject image, jstring name, jint size)
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
	const gchar *name_g = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_image_set_from_icon_name(image_g, name_g, (GtkIconSize)size);
	(*env)->ReleaseStringUTFChars(env, name, name_g);
}

/*
 * Class:     org_gnu_gtk_Image
 * Method:    gtk_image_set_pixel_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1set_1pixel_1size
  (JNIEnv *env, jclass cls, jobject image, jint size)
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    gtk_image_set_pixel_size(image_g, (gint)size);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_storage_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1storage_1type (JNIEnv *env, 
    jclass cls, jobject image) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    return (jint) (gtk_image_get_storage_type (image_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_pixmap
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1pixmap (JNIEnv *env, jclass cls, 
    jobject image, jobject pixmap, jobject mask) 
{	
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GdkPixmap **pixmap_g = (GdkPixmap **)getPointerFromHandle(env, pixmap);
    GdkBitmap **mask_g = (GdkBitmap **)getPointerFromHandle(env, mask);
    gtk_image_get_pixmap (image_g, pixmap_g, mask_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1image (JNIEnv *env, jclass cls, 
    jobject image) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    
    GtkImageType type = gtk_image_get_storage_type(image_g);
    if(type != GTK_IMAGE_EMPTY || type != GTK_IMAGE_IMAGE)
    	return NULL;
    
    GdkImage **gdkImage_g;
    GdkBitmap **mask_g;
    gtk_image_get_image (image_g, gdkImage_g, mask_g);
    
 	return getGObjectHandleAndRef(env, (GObject*) *gdkImage_g);
}

/*
 * Class:		org.gnu.gtk.Image
 * Method:		gtk_image_get_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1image_1mask (JNIEnv *env, jclass cls, 
    jobject image) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    
    GtkImageType type = gtk_image_get_storage_type(image_g);
    if(type != GTK_IMAGE_EMPTY || type != GTK_IMAGE_IMAGE)
    	return NULL;
    
    GdkImage **gdkImage_g;
    GdkBitmap **mask_g;
    gtk_image_get_image (image_g, gdkImage_g, mask_g);
    
    return getGObjectHandleAndRef(env, (GObject *) *mask_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1pixbuf (JNIEnv *env, jclass cls, 
    jobject image) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GtkImageType type = gtk_image_get_storage_type(image_g);
    if(type != GTK_IMAGE_EMPTY || type != GTK_IMAGE_PIXBUF)
    	return NULL;
  
    return getGObjectHandleAndRef(env, (GObject *) gtk_image_get_pixbuf (image_g));
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_stock
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1stock (JNIEnv *env, jclass cls, 
    jobject image, jobjectArray stockId, jintArray size) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    gchar **stockId_g = getStringArray(env, stockId);
    gint *size_g_g = (gint *) (*env)->GetIntArrayElements (env, size, NULL);
    GtkIconSize *size_g = (GtkIconSize *)size_g_g;
    gtk_image_get_stock (image_g, stockId_g, size_g);
    (*env)->ReleaseIntArrayElements (env, size, (jint*)size_g_g, 0);
    freeStringArray(env, stockId, stockId_g);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_icon_set
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1icon_1set (JNIEnv *env, jclass 
    cls, jobject image, jobject iconSet, jintArray size) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    GtkIconSet **iconSet_g = (GtkIconSet **)getPointerFromHandle(env, iconSet);
    gint *size_g_g = (gint *) (*env)->GetIntArrayElements (env, size, NULL);
    GtkIconSize *size_g = (GtkIconSize *)size_g_g;
    gtk_image_get_icon_set (image_g, iconSet_g, size_g);
    (*env)->ReleaseIntArrayElements (env, size, (jint*)size_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.Image
 * Method:    gtk_image_get_animation
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1animation (JNIEnv *env, jclass 
    cls, jobject image) 
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    return getGObjectHandle(env, (GObject *) gtk_image_get_animation (image_g));
}

/*
 * Class:     org_gnu_gtk_Image
 * Method:    gtk_image_get_icon_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1icon_1name
  (JNIEnv *env, jclass cls, jobject image, jstring name, jintArray size)
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
	const gchar *name_g = (*env)->GetStringUTFChars(env, name, NULL);
	gint* size_g = (gint*)(*env)->GetIntArrayElements(env, size, NULL);
	gtk_image_get_icon_name(image_g, &name_g, (GtkIconSize*)size_g);
	(*env)->ReleaseStringUTFChars(env, name, name_g);
	(*env)->ReleaseIntArrayElements(env, size, (jint*)size_g, 0);
}

/*
 * Class:     org_gnu_gtk_Image
 * Method:    gtk_image_get_pixel_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Image_gtk_1image_1get_1pixel_1size
  (JNIEnv *env, jclass cls, jobject image)
{
    GtkImage *image_g = (GtkImage *)getPointerFromHandle(env, image);
    return (jint)gtk_image_get_pixel_size(image_g);
}


#ifdef __cplusplus
}

#endif
