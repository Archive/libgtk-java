/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventClient.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventClient
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventClient_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventClient *obj_g = (GdkEventClient *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventClient
 * Method:    getSendEvent
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventClient_getSendEvent (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventClient *obj_g = (GdkEventClient *)getPointerFromHandle(env, obj);
    return (jint) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventClient
 * Method:    getMessageType
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventClient_getMessageType (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventClient *obj_g = (GdkEventClient *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->message_type, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventClient
 * Method:    getDataFormat
 */
JNIEXPORT jshort JNICALL Java_org_gnu_gdk_EventClient_getDataFormat (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventClient *obj_g = (GdkEventClient *)getPointerFromHandle(env, obj);
    return (jshort) obj_g->data_format;
}

#ifdef __cplusplus
}

#endif
