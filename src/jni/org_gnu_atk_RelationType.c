/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <string.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#include "org_gnu_atk_RelationType.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.atk.RelationType
 * Method:    atk_relation_type_register
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_RelationType_atk_1relation_1type_1register (JNIEnv 
    *env, jclass cls, jbyteArray name) 
{
    jint name_len = (*env)->GetArrayLength(env, name);
    gchar* name_g = (gchar*)g_malloc(name_len + 1);
    (*env)->GetByteArrayRegion(env, name, 0, name_len, (jbyte*)name_g);
    name_g[name_len] = 0;
    return (jint) (atk_relation_type_register (name_g));
}

/*
 * Class:     org.gnu.atk.RelationType
 * Method:    atk_relation_type_get_name
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_atk_RelationType_atk_1relation_1type_1get_1name (
    JNIEnv *env, jclass cls, jint type) 
{
    AtkRelationType type_g = (AtkRelationType) type;
    const gchar *result_g = (gchar *)atk_relation_type_get_name (type_g);
    jsize result_len = result_g ? strlen (result_g) : 0;
    jbyteArray result_j = result_g ? (*env)->NewByteArray (env, result_len) : NULL;
    if (result_g) 
    	(*env)->SetByteArrayRegion (env, result_j, 0, result_len, (jbyte *)result_g);
    return result_j;
}

/*
 * Class:     org.gnu.atk.RelationType
 * Method:    atk_relation_type_for_name
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_RelationType_atk_1relation_1type_1for_1name (JNIEnv 
    *env, jclass cls, jbyteArray name) 
{
    jint name_len = (*env)->GetArrayLength(env, name);
    gchar* name_g = (gchar*)g_malloc(name_len + 1);
    (*env)->GetByteArrayRegion(env, name, 0, name_len, (jbyte*)name_g);
    name_g[name_len] = 0;
    return (jint) (atk_relation_type_for_name (name_g));
}


#ifdef __cplusplus
}

#endif
