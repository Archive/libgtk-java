/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Bitmap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Bitmap
 * Method:    gdk_bitmap_create_from_data
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Bitmap_gdk_1bitmap_1create_1from_1data (JNIEnv *env, 
    jclass cls, jobject window, jbyteArray data, jint width, jint height) 
{
    GdkWindow *window_g;
    GdkBitmap *bitmap;
    jint data_len;
    gchar *data_g;
	
    window_g = (GdkWindow *)getPointerFromHandle(env, window);
    data_len = (*env)->GetArrayLength(env, data);
    data_g = (gchar*)g_malloc(data_len + 1);
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
    bitmap = gdk_bitmap_create_from_data(window_g, data_g, width, height);
	
    return getGObjectHandle(env, (GObject*)bitmap);
}

#ifdef __cplusplus
}

#endif
