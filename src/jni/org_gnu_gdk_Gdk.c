/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"
#include "jg_jnu.h"

#include "org_gnu_gdk_Gdk.h"
#ifdef __cplusplus
extern "C" 
{
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_input_set_extension_events
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1input_1set_1extension_1events (JNIEnv *env, 
    jclass cls, jobject window, jint mask, jint mode) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 mask_g = (gint32) mask;
    GdkExtensionMode mode_g = (GdkExtensionMode) mode;
    gdk_input_set_extension_events (window_g, mask_g, mode_g);
}
#endif

/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_flush
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1flush (JNIEnv *env, jclass cls) 
{
    gdk_flush ();
}

/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_beep
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1beep (JNIEnv *env, jclass cls) 
{
    {
        gdk_beep ();
    }
}

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_set_show_events
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1set_1show_1events (JNIEnv *env, jclass cls, 
    jboolean showEvents) 
{
    gboolean showEvents_g = (gboolean) showEvents;
    {
        gdk_set_show_events (showEvents_g);
    }
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_get_show_events
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1get_1show_1events (JNIEnv *env, jclass cls)
{
    {
        jboolean result_j = (jboolean) (gdk_get_show_events ());
        return result_j;
    }
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_name
 * Signature: (I)java.lang.String
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1name (JNIEnv *env, jclass cls, 
    jint keyval) 
{
    gchar *result_g = gdk_keyval_name ((gint32)keyval);
    return (*env)->NewStringUTF(env, result_g);
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_from_name
 * Signature: (java.lang.String)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1from_1name (JNIEnv *env, jclass cls, 
    jstring keyvalName) 
{
    const gchar* keyvalName_g = (*env)->GetStringUTFChars(env, keyvalName, 0);
    jint result_j = (jint) (gdk_keyval_from_name (keyvalName_g));
    (*env)->ReleaseStringUTFChars(env, keyvalName, keyvalName_g);
    return result_j;
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_to_upper
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1to_1upper (JNIEnv *env, jclass cls, 
    jint keyval) 
{
    gint32 keyval_g = (gint32) keyval;
    {
        jint result_j = (jint) (gdk_keyval_to_upper (keyval_g));
        return result_j;
    }
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_to_lower
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1to_1lower (JNIEnv *env, jclass cls, 
    jint keyval) 
{
    gint32 keyval_g = (gint32) keyval;
    {
        jint result_j = (jint) (gdk_keyval_to_lower (keyval_g));
        return result_j;
    }
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_is_upper
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1is_1upper (JNIEnv *env, jclass 
    cls, jint keyval) 
{
    gint32 keyval_g = (gint32) keyval;
    {
        jboolean result_j = (jboolean) (gdk_keyval_is_upper (keyval_g));
        return result_j;
    }
}
#endif

#ifdef ZERO
/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_keyval_is_lower
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1keyval_1is_1lower (JNIEnv *env, jclass 
    cls, jint keyval) 
{
    gint32 keyval_g = (gint32) keyval;
    {
        jboolean result_j = (jboolean) (gdk_keyval_is_lower (keyval_g));
        return result_j;
    }
}
#endif


#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1init
(JNIEnv *env, jclass cls, jintArray argc, jobjectArray argv)
{
    gint *argc_g = (gint *) (*env)->GetIntArrayElements (env, argc, NULL);
    gchar **argv_g_arr = getStringArray(env, argv);
    gchar ***argv_g = &argv_g_arr;
    gdk_init(argc_g, argv_g);
    (*env)->ReleaseIntArrayElements (env, argc, (jint *) argc_g, 0);
    freeStringArray(env, argv, argv_g_arr);
}
#endif

#ifdef ZERO
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1init_1check
(JNIEnv *env, jclass cls, jintArray argc, jobjectArray argv)
{
    gint *argc_g = (gint *) (*env)->GetIntArrayElements (env, argc, NULL);
    gchar **argv_g_arr = getStringArray(env, argv);
    gchar ***argv_g = &argv_g_arr;
    jboolean ret = gdk_init_check(argc_g, argv_g);
    (*env)->ReleaseIntArrayElements (env, argc, (jint *) argc_g, 0);
    freeStringArray(env, argv, argv_g_arr);
    return ret;
}
#endif

#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1parse_1args
(JNIEnv *env, jclass cls, jintArray argc, jobjectArray argv)
{
    gint *argc_g = (gint *) (*env)->GetIntArrayElements (env, argc, NULL);
    gchar **argv_g_arr = getStringArray(env, argv);
    gchar ***argv_g = &argv_g_arr;
    gdk_parse_args(argc_g, argv_g);
    (*env)->ReleaseIntArrayElements (env, argc, (jint *) argc_g, 0);
    freeStringArray(env, argv, argv_g_arr);
}
#endif

#ifdef ZERO
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Gdk_gdk_1get_1program_1class
(JNIEnv *env, jclass cls)
{
    return (*env)->NewStringUTF(env, gdk_get_program_class());
}
#endif

#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1set_1program_1class
(JNIEnv *env, jclass cls, jstring program_class)
{
    const char * program_class_g = (const char *)(*env)->GetStringUTFChars(env, program_class, 0);
    gdk_set_program_class(program_class_g);
    (*env)->ReleaseStringUTFChars(env, program_class, program_class_g);
}
#endif

#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1error_1trap_1push
(JNIEnv *env, jclass cls)
{
    gdk_error_trap_push();
}
#endif

#ifdef ZERO
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Gdk_gdk_1error_1trap_1pop
(JNIEnv *env, jclass cls)
{
    return (jint)gdk_error_trap_pop();
}
#endif

#ifdef ZERO
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Gdk_gdk_1get_1display_1arg_1name
(JNIEnv *env, jclass cls)
{
    return (*env)->NewStringUTF(env, gdk_get_display_arg_name());
}
#endif

#ifdef ZERO
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1event_1send_1client_1message
(JNIEnv *env, jclass cls, jobject event, jint winid)
{
    GdkEvent * event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GdkNativeWindow winid_g = (GdkNativeWindow)winid;
    return (jboolean)gdk_event_send_client_message(event_g, winid_g);
}
#endif

#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1event_1send_1clientmessage_1toall
(JNIEnv *env, jclass cls, jobject event)
{
    GdkEvent * event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gdk_event_send_clientmessage_toall(event_g);
}
#endif

#ifdef ZERO
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Gdk_gdk_1event_1send_1client_1message_1for_1display
(JNIEnv *env, jclass cls, jobject display, jobject event, jint winid)
{
    GdkDisplay * display_g = (GdkDisplay *)getPointerFromHandle(env, display);
    GdkEvent * event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GdkNativeWindow winid_g = (GdkNativeWindow)winid;
    return (jboolean)gdk_event_send_client_message_for_display(display_g, event_g, winid_g);
}
#endif

#ifdef ZERO
JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1notify_1startup_1complete
(JNIEnv *env, jclass cls)
{
    gdk_notify_startup_complete();
}
#endif

JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1threads_1enter
(JNIEnv *env, jclass cls)
{
    gdk_threads_enter();
}

JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1threads_1leave
(JNIEnv *env, jclass cls)
{
    gdk_threads_leave();
}

JNIEXPORT void JNICALL Java_org_gnu_gdk_Gdk_gdk_1threads_1init
(JNIEnv *env, jclass cls)
{
    gdk_threads_init();
}

/*
 * Class:     org.gnu.gdk.Gdk
 * Method:    gdk_pango_context_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Gdk_gdk_1pango_1context_1get 
(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, G_OBJECT(gdk_pango_context_get()));
}

#ifdef __cplusplus
}

#endif
