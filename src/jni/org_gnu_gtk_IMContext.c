/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IMContext.h"
#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_im_context_get_type ();
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_set_client_window
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1set_1client_1window (JNIEnv 
    *env, jclass cls, jobject context, jobject window) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gtk_im_context_set_client_window (context_g, window_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_get_preedit_string
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1get_1preedit_1string (
    JNIEnv *env, jclass cls, jobject context, jobjectArray str, jintArray attrs, jintArray cursorPos) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gchar **str_g = getStringArray(env, str);
    gint *attrs_g_g = (gint *) (*env)->GetIntArrayElements (env, attrs, NULL);
    PangoAttrList **attrs_g = (PangoAttrList **)attrs_g_g;
    gint *cursorPos_g = (gint *) (*env)->GetIntArrayElements (env, cursorPos, NULL);
    gtk_im_context_get_preedit_string (context_g, str_g, attrs_g, cursorPos_g);
    (*env)->ReleaseIntArrayElements (env, attrs, (jint*)attrs_g_g, 0);
    (*env)->ReleaseIntArrayElements (env, cursorPos, (jint *) cursorPos_g, 0);
    freeStringArray(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_filter_keypress
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1filter_1keypress (
    JNIEnv *env, jclass cls, jobject context, jobject event) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    GdkEventKey *event_g = (GdkEventKey *)getPointerFromHandle(env, event);
    return (jboolean) (gtk_im_context_filter_keypress (context_g, event_g));
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_focus_in
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1focus_1in (JNIEnv *env, 
    jclass cls, jobject context) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gtk_im_context_focus_in (context_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_focus_out
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1focus_1out (JNIEnv *env, 
    jclass cls, jobject context) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gtk_im_context_focus_out (context_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_reset
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1reset (JNIEnv *env, jclass 
    cls, jobject context) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gtk_im_context_reset (context_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_set_cursor_location
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1set_1cursor_1location (
    JNIEnv *env, jclass cls, jobject context, jobject area) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    GdkRectangle *area_g = (GdkRectangle *)getPointerFromHandle(env, area);
    gtk_im_context_set_cursor_location (context_g, area_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_set_use_preedit
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1set_1use_1preedit (JNIEnv 
    *env, jclass cls, jobject context, jboolean usePreedit) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gboolean usePreedit_g = (gboolean) usePreedit;
    gtk_im_context_set_use_preedit (context_g, usePreedit_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_set_surrounding
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1set_1surrounding (JNIEnv 
    *env, jclass cls, jobject context, jstring text, jint len, jint cursorIndex) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gint32 len_g = (gint32) len;
    gint32 cursorIndex_g = (gint32) cursorIndex;
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
	gtk_im_context_set_surrounding (context_g, text_g, len_g, cursorIndex_g);
	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_get_surrounding
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1get_1surrounding (
    JNIEnv *env, jclass cls, jobject context, jobjectArray text, jintArray cursorIndex) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gchar **text_g = getStringArray(env, text);
    gint *cursorIndex_g = (gint *) (*env)->GetIntArrayElements (env, cursorIndex, NULL);
    jboolean result_j = (jboolean) (gtk_im_context_get_surrounding (context_g, text_g, 
                cursorIndex_g));
    (*env)->ReleaseIntArrayElements (env, cursorIndex, (jint *) cursorIndex_g, 0);
    freeStringArray(env, text, text_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.IMContext
 * Method:    gtk_im_context_delete_surrounding
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IMContext_gtk_1im_1context_1delete_1surrounding (
    JNIEnv *env, jclass cls, jobject context, jint offset, jint numChars) 
{
    GtkIMContext *context_g = (GtkIMContext *)getPointerFromHandle(env, context);
    gint32 offset_g = (gint32) offset;
    gint32 numChars_g = (gint32) numChars;
    return (jboolean) (gtk_im_context_delete_surrounding (context_g, offset_g, 
                numChars_g));
}

#ifdef __cplusplus
}

#endif
