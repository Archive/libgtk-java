/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"

#include "org_gnu_pango_AttrFloat.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gdouble PangoAttrFloat_get_value (PangoAttrFloat * cptr) 
{
    return cptr->value;
}

/*
 * Class:     org.gnu.pango.AttrFloat
 * Method:    getValue
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_pango_AttrFloat_getValue (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    PangoAttrFloat *obj_g = (PangoAttrFloat *)getPointerFromHandle(env, obj);
    return (jdouble) (PangoAttrFloat_get_value (obj_g));
}



#ifdef __cplusplus
}

#endif
