/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_MenuBar.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.MenuBar
 * Method:    gtk_menu_bar_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_menu_bar_get_type ();
}

/*
 * Class:     org.gnu.gtk.MenuBar
 * Method:    gtk_menu_bar_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_menu_bar_new ());
}

/*
 * Class:     org_gnu_gtk_MenuBar
 * Method:    gtk_menu_bar_get_pack_direction
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1get_1pack_1direction
  (JNIEnv *env, jclass cls, jobject mb)
{
	GtkMenuBar *mb_g = (GtkMenuBar*)getPointerFromHandle(env, mb);
	return (jint)gtk_menu_bar_get_pack_direction(mb_g);
}

/*
 * Class:     org_gnu_gtk_MenuBar
 * Method:    gtk_menu_bar_set_pack_direction
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1set_1pack_1direction
  (JNIEnv *env, jclass cls, jobject mb, jint dir)
{
	GtkMenuBar *mb_g = (GtkMenuBar*)getPointerFromHandle(env, mb);
	gtk_menu_bar_set_pack_direction(mb_g, (GtkPackDirection)dir);
}

/*
 * Class:     org_gnu_gtk_MenuBar
 * Method:    gtk_menu_bar_get_child_pack_direction
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1get_1child_1pack_1direction
  (JNIEnv *env, jclass cls, jobject mb)
{
	GtkMenuBar *mb_g = (GtkMenuBar*)getPointerFromHandle(env, mb);
	return gtk_menu_bar_get_child_pack_direction(mb_g);
}

/*
 * Class:     org_gnu_gtk_MenuBar
 * Method:    gtk_menu_bar_set_child_pack_direction
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuBar_gtk_1menu_1bar_1set_1child_1pack_1direction
  (JNIEnv *env, jclass cls, jobject mb, jint dir)
{
	GtkMenuBar *mb_g = (GtkMenuBar*)getPointerFromHandle(env, mb);
	gtk_menu_bar_set_child_pack_direction(mb_g, (GtkPackDirection)dir);
}

#ifdef __cplusplus
}

#endif
