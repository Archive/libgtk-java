/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventConfigure.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventConfigure_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventConfigure_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventConfigure_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return (jint) obj_g->x;
}

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventConfigure_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return (jint) obj_g->y;
}

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventConfigure_getWidth (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return (jint) obj_g->width;
}

/*
 * Class:     org.gnu.gdk.EventConfigure
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventConfigure_getHeight (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventConfigure *obj_g = (GdkEventConfigure *)getPointerFromHandle(env, obj);
    return (jint) obj_g->height;
}


#ifdef __cplusplus
}

#endif
