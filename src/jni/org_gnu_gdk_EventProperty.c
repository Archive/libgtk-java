/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventProperty.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventProperty
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventProperty_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProperty *obj_g = (GdkEventProperty *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventProperty
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventProperty_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventProperty *obj_g = (GdkEventProperty *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventProperty
 * Method:    getAtom
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventProperty_getAtom (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProperty *obj_g = (GdkEventProperty *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->atom, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventProperty
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventProperty_getTime (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProperty *obj_g = (GdkEventProperty *)getPointerFromHandle(env, obj);
    return obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventProperty
 * Method:    getState
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventProperty_getState (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventProperty *obj_g = (GdkEventProperty *)getPointerFromHandle(env, obj);
    return (jint) obj_g->state;
}


#ifdef __cplusplus
}

#endif
