/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_GdkCairo.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gdk_GdkCairo
 * Method:    gdk_cairo_create
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_GdkCairo_gdk_1cairo_1create
  (JNIEnv *env, jclass cls, jobject drawable)
{
	cairo_t *cr;
	GdkDrawable *drawable_g;
	
	drawable_g = (GdkDrawable*)getPointerFromHandle(env, drawable);
	
	cr = gdk_cairo_create(drawable_g);

	return getHandleFromPointer(env, cr);
}

/*
 * Class:     org_gnu_gdk_GdkCairo
 * Method:    gdk_cairo_set_source_color
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GdkCairo_gdk_1cairo_1set_1source_1color
  (JNIEnv *env, jclass cls, jobject cr, jobject color)
{
	cairo_t *cr_g;
	GdkColor *color_g;
	
	cr_g = (cairo_t*)getPointerFromHandle(env, cr);
	color_g = (GdkColor*)getPointerFromHandle(env, color);
	
	gdk_cairo_set_source_color(cr_g, color_g);
}

/*
 * Class:     org_gnu_gdk_GdkCairo
 * Method:    gdk_cairo_set_source_pixbuf
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;DD)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GdkCairo_gdk_1cairo_1set_1source_1pixbuf
  (JNIEnv *env, jclass cls, jobject cr, jobject pixbuf, jdouble x, jdouble y)
{
	cairo_t *cr_g;
	GdkPixbuf *pixbuf_g;
	
	cr_g = (cairo_t*)getPointerFromHandle(env, cr);
	pixbuf_g = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
	
	gdk_cairo_set_source_pixbuf(cr_g, pixbuf_g, x, y);
}

/*
 * Class:     org_gnu_gdk_GdkCairo
 * Method:    gdk_cairo_rectangle
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GdkCairo_gdk_1cairo_1rectangle
  (JNIEnv *env, jclass cls, jobject cr, jobject rect)
{
	cairo_t *cr_g;
	GdkRectangle *rect_g;
	
	cr_g = (cairo_t*)getPointerFromHandle(env, cr);
	rect_g = (GdkRectangle*)getPointerFromHandle(env, rect);
	
	gdk_cairo_rectangle(cr_g, rect_g);
}

/*
 * Class:     org_gnu_gdk_GdkCairo
 * Method:    gdk_cairo_region
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_GdkCairo_gdk_1cairo_1region
  (JNIEnv *env, jclass cls, jobject cr, jobject reg)
{
	cairo_t *cr_g;
	GdkRegion *reg_g;
	
	cr_g = (cairo_t*)getPointerFromHandle(env, cr);
	reg_g = (GdkRegion*)getPointerFromHandle(env, reg);
	
	gdk_cairo_region(cr_g, reg_g);
}

#ifdef __cplusplus
}
#endif
