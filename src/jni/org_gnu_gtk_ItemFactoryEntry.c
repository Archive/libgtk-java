/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ItemFactoryEntry.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gchar * GtkItemFactoryEntry_get_path (GtkItemFactoryEntry * cptr) 
{
    return cptr->path;
}

/*
 * Class:     org.gnu.gtk.ItemFactoryEntry
 * Method:    getPath
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ItemFactoryEntry_getPath (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkItemFactoryEntry *cptr_g = (GtkItemFactoryEntry *)getPointerFromHandle(env, cptr);
    gchar *result_g = GtkItemFactoryEntry_get_path (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static gchar * GtkItemFactoryEntry_get_accelerator (GtkItemFactoryEntry * cptr) 
{
    return cptr->accelerator;
}

/*
 * Class:     org.gnu.gtk.ItemFactoryEntry
 * Method:    getAccelerator
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ItemFactoryEntry_getAccelerator (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GtkItemFactoryEntry *cptr_g = (GtkItemFactoryEntry *)getPointerFromHandle(env, cptr);
    gchar *result_g = GtkItemFactoryEntry_get_accelerator (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}

static gchar * GtkItemFactoryEntry_get_item_type (GtkItemFactoryEntry * cptr) 
{
    return cptr->item_type;
}

/*
 * Class:     org.gnu.gtk.ItemFactoryEntry
 * Method:    getItemType
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ItemFactoryEntry_getItemType (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkItemFactoryEntry *cptr_g = (GtkItemFactoryEntry *)getPointerFromHandle(env, cptr);
    gchar *result_g = GtkItemFactoryEntry_get_item_type (cptr_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
