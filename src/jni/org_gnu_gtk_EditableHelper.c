/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_EditableHelper.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_editable_get_type ();
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_select_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1select_1region (JNIEnv *env, 
    jclass cls, jobject editable, jint start, jint end) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gint32 start_g = (gint32) start;
    gint32 end_g = (gint32) end;
    gtk_editable_select_region (editable_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_get_selection_bounds
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1get_1selection_1bounds (
    JNIEnv *env, jclass cls, jobject editable, jintArray start, jintArray end) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gint *start_g = (gint *) (*env)->GetIntArrayElements (env, start, NULL);
    gint *end_g = (gint *) (*env)->GetIntArrayElements (env, end, NULL);
    jboolean result_j = (jboolean) (gtk_editable_get_selection_bounds (editable_g, start_g, 
                end_g));
    (*env)->ReleaseIntArrayElements (env, start, (jint *) start_g, 0);
    (*env)->ReleaseIntArrayElements (env, end, (jint *) end_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_insert_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1insert_1text (JNIEnv *env, 
    jclass cls, jobject editable, jstring newText, jint newTextLength, jintArray position) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    const gchar* newText_g = (*env)->GetStringUTFChars(env, newText, NULL); 
    gint32 newTextLength_g = (gint32) newTextLength;
    gint *position_g = (gint *) (*env)->GetIntArrayElements (env, position, NULL);
	gtk_editable_insert_text (editable_g, newText_g, newTextLength_g, position_g);
	(*env)->ReleaseIntArrayElements (env, position, (jint *) position_g, 0);
	(*env)->ReleaseStringUTFChars( env, newText, newText_g );
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_delete_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1delete_1text (JNIEnv *env, 
    jclass cls, jobject editable, jint startPos, jint endPos) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gint32 startPos_g = (gint32) startPos;
    gint32 endPos_g = (gint32) endPos;
    gtk_editable_delete_text (editable_g, startPos_g, endPos_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_get_chars
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1get_1chars (JNIEnv *env, 
    jclass cls, jobject editable, jint start, jint end) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gint32 start_g = (gint32) start;
    gint32 end_g = (gint32) end;
	return (*env)->NewStringUTF(env,  gtk_editable_get_chars (editable_g, start_g, end_g));
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_cut_clipboard
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1cut_1clipboard (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gtk_editable_cut_clipboard (editable_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_copy_clipboard
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1copy_1clipboard (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gtk_editable_copy_clipboard (editable_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_paste_clipboard
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1paste_1clipboard (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gtk_editable_paste_clipboard (editable_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_delete_selection
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1delete_1selection (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gtk_editable_delete_selection (editable_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_set_position
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1set_1position (JNIEnv *env, 
    jclass cls, jobject editable, jint position) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gint32 position_g = (gint32) position;
    gtk_editable_set_position (editable_g, position_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_get_position
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1get_1position (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    return (jint) (gtk_editable_get_position (editable_g));
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_set_editable
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1set_1editable (JNIEnv *env, 
    jclass cls, jobject editable, jboolean isEditable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    gboolean isEditable_g = (gboolean) isEditable;
    gtk_editable_set_editable (editable_g, isEditable_g);
}

/*
 * Class:     org.gnu.gtk.EditableHelper
 * Method:    gtk_editable_get_editable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_EditableHelper_gtk_1editable_1get_1editable (JNIEnv *env, 
    jclass cls, jobject editable) 
{
    GtkEditable *editable_g = (GtkEditable *)getPointerFromHandle(env, editable);
    return (jboolean) (gtk_editable_get_editable (editable_g));
}


#ifdef __cplusplus
}

#endif
