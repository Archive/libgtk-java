/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#ifndef _Included_org_gnu_atk_State
#define _Included_org_gnu_atk_State
#include "org_gnu_atk_State.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_atk_State
 * Method:    atk_state_type_register
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_State_atk_1state_1type_1register
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jint value = (jint)atk_state_type_register(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

/*
 * Class:     org_gnu_atk_State
 * Method:    atk_state_type_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_State_atk_1state_1type_1get_1name
  (JNIEnv *env, jclass cls, jint type)
{
	return (*env)->NewStringUTF(env, atk_state_type_get_name((AtkStateType)type));
}

/*
 * Class:     org_gnu_atk_State
 * Method:    atk_state_type_for_name
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_State_atk_1state_1type_1for_1name
  (JNIEnv *env, jclass cls, jstring name)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jint value = (jint)atk_state_type_for_name(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return value;
}

#ifdef __cplusplus
}
#endif
#endif
