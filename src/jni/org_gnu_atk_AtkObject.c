/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_atk_AtkObject.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)atk_object_get_type ();
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1name (JNIEnv *env, 
    jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gchar *result_g = (gchar *)atk_object_get_name (accessible_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_description
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1description (JNIEnv 
    *env, jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gchar *result_g = (gchar *)atk_object_get_description (accessible_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_parent
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1parent (JNIEnv *env, jclass 
    cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    return getGObjectHandle(env, G_OBJECT(atk_object_get_parent (accessible_g)));
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_n_accessible_children
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1n_1accessible_1children (
    JNIEnv *env, jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    jint result_j = (jint) (atk_object_get_n_accessible_children (accessible_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_ref_accessible_child
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1ref_1accessible_1child (JNIEnv 
    *env, jclass cls, jobject accessible, jint i) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gint32 i_g = (gint32) i;
    return getGObjectHandle(env, G_OBJECT(atk_object_ref_accessible_child (accessible_g, i_g)));
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_ref_relation_set
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1ref_1relation_1set (JNIEnv *env, 
    jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    return getGObjectHandle(env, G_OBJECT(atk_object_ref_relation_set (accessible_g)));
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_role
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1role (JNIEnv *env, jclass 
    cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    jint result_j = (jint) (atk_object_get_role (accessible_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_layer
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1layer (JNIEnv *env, jclass 
    cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    jint result_j = (jint) (atk_object_get_layer (accessible_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_mdi_zorder
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1mdi_1zorder (JNIEnv *env, 
    jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    jint result_j = (jint) (atk_object_get_mdi_zorder (accessible_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_get_index_in_parent
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1get_1index_1in_1parent (JNIEnv 
    *env, jclass cls, jobject accessible) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    jint result_j = (jint) (atk_object_get_index_in_parent (accessible_g));
    return result_j;
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_set_name
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1set_1name (JNIEnv *env, jclass 
    cls, jobject accessible, jstring name) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gchar* name_g = (gchar*)(*env)->GetStringUTFChars(env, name, 0);
    atk_object_set_name(accessible_g, name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_set_description
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1set_1description (JNIEnv *env, 
    jclass cls, jobject accessible, jstring description) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gchar* description_g = (gchar*)(*env)->GetStringUTFChars(env, description, 0);
    atk_object_set_description (accessible_g, description_g);
    (*env)->ReleaseStringUTFChars(env, description, description_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_set_parent
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1set_1parent (JNIEnv *env, jclass 
    cls, jobject accessible, jobject parent) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    AtkObject *parent_g = (AtkObject *)getPointerFromHandle(env, parent);
    atk_object_set_parent (accessible_g, parent_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_set_role
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1set_1role (JNIEnv *env, jclass 
    cls, jobject accessible, jint role) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    AtkRole role_g = (AtkRole) role;
    atk_object_set_role (accessible_g, role_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_remove_property_change_handler
 */
JNIEXPORT void JNICALL 
Java_org_gnu_atk_AtkObject_atk_1object_1remove_1property_1change_1handler (JNIEnv *env, jclass 
    cls, jobject accessible, jint handlerId) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gint32 handlerId_g = (gint32) handlerId;
    atk_object_remove_property_change_handler (accessible_g, handlerId_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_notify_state_change
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1notify_1state_1change (JNIEnv 
    *env, jclass cls, jobject accessible, jint state, jboolean value) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gboolean value_g = (gboolean) value;
	atk_object_notify_state_change (accessible_g, (AtkState)state, value_g);
}

/*
 * Class:     org.gnu.atk.AtkObject
 * Method:    atk_object_initialize
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1initialize (JNIEnv *env, jclass 
    cls, jobject accessible, jlong data) 
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    gpointer *data_g = (gpointer *)data;
    atk_object_initialize (accessible_g, data_g);
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_role_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_AtkObject_atk_1role_1get_1name
  (JNIEnv *env, jclass cla, jint role)
{
	return (*env)->NewStringUTF(env, atk_role_get_name((AtkRole)role));
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_role_for_name
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_AtkObject_atk_1role_1for_1name
  (JNIEnv *env, jclass cla, jstring name)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	jint role = (jint)atk_role_for_name(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return role;
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_object_add_relationship
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1add_1relationship
  (JNIEnv *env, jclass cla, jobject accessible, jint type, jobject target)
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    AtkObject *target_g = (AtkObject *)getPointerFromHandle(env, target);
    return atk_object_add_relationship(accessible_g, (AtkRelationType)type, target_g);
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_object_remove_relationship
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1remove_1relationship
  (JNIEnv *env, jclass cla, jobject accessible, jint type, jobject target)
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    AtkObject *target_g = (AtkObject *)getPointerFromHandle(env, target);
    return atk_object_remove_relationship(accessible_g, (AtkRelationType)type, target_g);
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_role_get_localized_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_AtkObject_atk_1role_1get_1localized_1name
  (JNIEnv *env, jclass cla, jint role)
{
	return (*env)->NewStringUTF(env, atk_role_get_localized_name((AtkRole)role));
}

/*
 * Class:     org_gnu_atk_AtkObject
 * Method:    atk_object_ref_state_set
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_AtkObject_atk_1object_1ref_1state_1set
  (JNIEnv *env, jclass cla, jobject accessible)
{
    AtkObject *accessible_g = (AtkObject *)getPointerFromHandle(env, accessible);
    return getGObjectHandle(env, G_OBJECT(atk_object_ref_state_set(accessible_g)));
}


#ifdef __cplusplus
}

#endif
