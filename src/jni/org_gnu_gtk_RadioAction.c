/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_RadioAction
#define _Included_org_gnu_gtk_RadioAction
#include "org_gnu_gtk_RadioAction.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_RadioAction
 * Method:    gtk_radio_action_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RadioAction_gtk_1radio_1action_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_radio_action_get_type();
}

/*
 * Class:     org_gnu_gtk_RadioAction
 * Method:    gtk_radio_action_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioAction_gtk_1radio_1action_1new
  (JNIEnv *env, jclass cls, jstring name, jstring label, jstring tooltip, jstring stockId, jint val)
{
	const gchar* n = (*env)->GetStringUTFChars(env, name, NULL);
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	const gchar* t = (*env)->GetStringUTFChars(env, tooltip, NULL);
	const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
	jobject value = getGObjectHandle(env, (GObject *) gtk_radio_action_new(n, l, t, s, val));
	(*env)->ReleaseStringUTFChars(env, name, n);
	(*env)->ReleaseStringUTFChars(env, label, l);
	(*env)->ReleaseStringUTFChars(env, tooltip, t);
	(*env)->ReleaseStringUTFChars(env, stockId, s);
	return value;
}
                                                                                   
/*
 * Class:     org_gnu_gtk_RadioAction
 * Method:    gtk_radio_action_get_group
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_RadioAction_gtk_1radio_1action_1get_1group
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkRadioAction* action_g = (GtkRadioAction*)getPointerFromHandle(env, action);
	return getGObjectHandlesFromGSList(env, gtk_radio_action_get_group(action_g));
}

/*
 * Class:     org_gnu_gtk_RadioAction
 * Method:    gtk_radio_action_set_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioAction_gtk_1radio_1action_1set_1group
  (JNIEnv *env, jclass cls, jobject action, jobjectArray group)
{
	GtkRadioAction* action_g = (GtkRadioAction*)getPointerFromHandle(env, action);
	GSList* list = getGSListFromHandles(env, group);
	gtk_radio_action_set_group(action_g, list);
}

/*
 * Class:     org_gnu_gtk_RadioAction
 * Method:    gtk_radio_action_get_current_value
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RadioAction_gtk_1radio_1action_1get_1current_1value
  (JNIEnv *env, jclass cls, jobject action)
{
	GtkRadioAction* action_g = (GtkRadioAction*)getPointerFromHandle(env, action);
	return (jint)gtk_radio_action_get_current_value(action_g);
}


#ifdef __cplusplus
}
#endif
#endif
