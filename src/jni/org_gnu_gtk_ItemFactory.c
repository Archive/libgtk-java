/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ItemFactory.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_item_factory_get_type ();
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1new (JNIEnv *env, 
    jclass cls, jint containerType, jstring path, jobject accelGroup) 
{
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    jobject result = getGObjectHandle(env, (GObject *) 
    		gtk_item_factory_new ((GType)containerType, path_g, accelGroup_g));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_construct
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1construct (JNIEnv *env, 
    jclass cls, jobject ifactory, jint containerType, jstring path, jobject accelGroup) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    gtk_item_factory_construct (ifactory_g, (GType)containerType, path_g, accelGroup_g);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_add_foreign
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1add_1foreign (JNIEnv 
    *env, jclass cls, jobject accelWidget, jstring fullPath, jobject accelGroup, jint keyval, jint 
    modifiers) 
{
    GtkWidget *accelWidget_g = (GtkWidget *)getPointerFromHandle(env, accelWidget);
    gchar* fullPath_g = (gchar*)(*env)->GetStringUTFChars(env, fullPath, 0);
    GtkAccelGroup *accelGroup_g = (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_item_factory_add_foreign (accelWidget_g, fullPath_g, accelGroup_g, (gint32)keyval, 
            (GdkModifierType)modifiers);
  	(*env)->ReleaseStringUTFChars(env, fullPath, fullPath_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1from_1widget (JNIEnv 
    *env, jclass cls, jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    return getGObjectHandle(env, (GObject *) gtk_item_factory_from_widget (widget_g));
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_path_from_widget
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1path_1from_1widget (JNIEnv *env, jclass cls, 
    jobject widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gchar *result_g = (gchar*)gtk_item_factory_path_from_widget (widget_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_get_item
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1get_1item (JNIEnv *env, 
    jclass cls, jobject ifactory, jstring path) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_item_factory_get_item(ifactory_g, path_g));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_get_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1get_1widget (JNIEnv 
    *env, jclass cls, jobject ifactory, jstring path) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    jobject result = getGObjectHandle(env, (GObject *)
    		gtk_item_factory_get_widget(ifactory_g, path_g));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return result;
}

  // Not going to bother fixing, class is deprecated anyways.
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_get_widget_by_action
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1get_1widget_1by_1action (
    JNIEnv *env, jclass cls, jobject ifactory, jint action) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gint32 action_g = (gint32) action;
    return getGObjectHandle(env, (GObject *)
    		gtk_item_factory_get_widget_by_action (ifactory_g, action_g));
}
#endif

  /* Class is deprecated, why fix something that won't be here? */
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_get_item_by_action
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1get_1item_1by_1action (
    JNIEnv *env, jclass cls, jobject ifactory, jint action) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gint32 action_g = (gint32) action;
    return getGObjectHandle(env, (GObject *)
    		gtk_item_factory_get_item_by_action (ifactory_g, action_g));
}
#endif

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_create_item
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1create_1item (JNIEnv 
    *env, jclass cls, jobject ifactory, jobject entry, jobject callbackData, jint callbackType) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    GtkItemFactoryEntry *entry_g = (GtkItemFactoryEntry *)getPointerFromHandle(env, entry);
    gpointer *callbackData_g = (gpointer *)callbackData;
    gint32 callbackType_g = (gint32) callbackType;
    gtk_item_factory_create_item (ifactory_g, entry_g, callbackData_g, callbackType_g);
}


  /* Broken, won't fix. Deprecated */
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_create_items
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1create_1items (JNIEnv 
    *env, jclass cls, jobject ifactory, jint numEntries, jobjectArray entries, jobject callbackData) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gint32 numEntries_g = (gint32) numEntries;
    GtkItemFactoryEntry *entries_g = (GtkItemFactoryEntry *)getPointerArrayFromHandles(env, entries);
    gpointer *callbackData_g = (gpointer *)callbackData;
    gtk_item_factory_create_items (ifactory_g, numEntries_g, entries_g, callbackData_g);
}
#endif

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_delete_item
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1delete_1item (JNIEnv 
    *env, jclass cls, jobject ifactory, jstring path) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gchar* path_g = (gchar*)(*env)->GetStringUTFChars(env, path, 0);
    gtk_item_factory_delete_item (ifactory_g, path_g);
    (*env)->ReleaseStringUTFChars(env, path, path_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_delete_entry
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1delete_1entry (JNIEnv 
    *env, jclass cls, jobject ifactory, jobject entry) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    GtkItemFactoryEntry *entry_g = (GtkItemFactoryEntry *)getPointerArrayFromHandles(env, entry);
    gtk_item_factory_delete_entry (ifactory_g, entry_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_delete_entries
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1delete_1entries (JNIEnv 
    *env, jclass cls, jobject ifactory, jint numEnties, jobjectArray entries) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gint32 numEnties_g = (gint32) numEnties;
    GtkItemFactoryEntry *entries_g = (GtkItemFactoryEntry *)getPointerArrayFromHandles(env, entries);
    gtk_item_factory_delete_entries (ifactory_g, numEnties_g, entries_g);
}

/*
 * Class:     org.gnu.gtk.ItemFactory
 * Method:    gtk_item_factory_popup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ItemFactory_gtk_1item_1factory_1popup (JNIEnv *env, 
    jclass cls, jobject ifactory, jint x, jint y, jint mouseButton, jint time) 
{
    GtkItemFactory *ifactory_g = (GtkItemFactory *)getPointerFromHandle(env, ifactory);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint32 mouseButton_g = (gint32) mouseButton;
    gint32 time_g = (gint32) time;
    gtk_item_factory_popup (ifactory_g, x_g, y_g, mouseButton_g, time_g);
}


#ifdef __cplusplus
}

#endif
