/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#ifndef _Included_org_gnu_atk_Registry
#define _Included_org_gnu_atk_Registry
#include "org_gnu_atk_Registry.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_atk_Registry
 * Method:    atk_registry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Registry_atk_1registry_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)atk_registry_get_type();
}

/*
 * Class:     org_gnu_atk_Registry
 * Method:    atk_registry_set_factory_type
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_Registry_atk_1registry_1set_1factory_1type
  (JNIEnv *env, jclass cls, jobject reg, jint type, jint facType)
{
	AtkRegistry* reg_g = (AtkRegistry*)getPointerFromHandle(env, reg);
	atk_registry_set_factory_type(reg_g, (GType)type, (GType)facType);
}

/*
 * Class:     org_gnu_atk_Registry
 * Method:    atk_registry_get_factory_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Registry_atk_1registry_1get_1factory_1type
  (JNIEnv *env, jclass cls, jobject reg, jint type)
{
	AtkRegistry* reg_g = (AtkRegistry*)getPointerFromHandle(env, reg);
	return (jint)atk_registry_get_factory_type(reg_g, (GType)type);
}

/*
 * Class:     org_gnu_atk_Registry
 * Method:    atk_registry_get_factory
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_Registry_atk_1registry_1get_1factory
  (JNIEnv *env, jclass cls, jobject reg, jint type)
{
	AtkRegistry* reg_g = (AtkRegistry*)getPointerFromHandle(env, reg);
	return getGObjectHandle(env, G_OBJECT(atk_registry_get_factory(reg_g, (GType)type)));
}

/*
 * Class:     org_gnu_atk_Registry
 * Method:    atk_get_default_registry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_Registry_atk_1get_1default_1registry
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, G_OBJECT(atk_get_default_registry()));
}

#ifdef __cplusplus
}
#endif
#endif
