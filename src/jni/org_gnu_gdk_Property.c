/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "jg_jnu.h"

#include "org_gnu_gdk_Property.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Property
 * Method:    gdk_property_get
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Property_gdk_1property_1get (JNIEnv *env, jclass 
    cls, jobject window, jobject property, jobject type, jlong offset, jlong length, jint pdelete, 
    jintArray actualPropertyType, jintArray actualFormat, jintArray actualLength, jbyteArray 
    data) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkAtom *property_g = (GdkAtom *)getPointerFromHandle(env, property);
    GdkAtom *type_g = (GdkAtom *)getPointerFromHandle(env, type);
    glong offset_g = (glong) offset;
    glong length_g = (glong) length;
    gint32 pdelete_g = (gint32) pdelete;
    gint *actualPropertyType_g_g = (gint *) (*env)->GetIntArrayElements (env, 
        actualPropertyType, NULL);
    GdkAtom *actualPropertyType_g = (GdkAtom *)actualPropertyType_g_g;
    gint *actualFormat_g = (gint *) (*env)->GetIntArrayElements (env, actualFormat, NULL);
    gint *actualLength_g = (gint *) (*env)->GetIntArrayElements (env, actualLength, NULL);
    jint data_len = (*env)->GetArrayLength(env, data);
    guchar** data_g = (guchar**)g_malloc(data_len + 1);
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
	jboolean result_j = (jboolean) (gdk_property_get (window_g, *property_g, *type_g, 
						    offset_g, length_g, pdelete_g, 
						    actualPropertyType_g, actualFormat_g, 
						    actualLength_g, data_g));
  	(*env)->ReleaseIntArrayElements (env, actualPropertyType, (jint*)
				   actualPropertyType_g_g, 0);
  	(*env)->ReleaseIntArrayElements (env, actualFormat, (jint *) actualFormat_g, 0);
  	(*env)->ReleaseIntArrayElements (env, actualLength, (jint *) actualLength_g, 0);
  	return result_j;
}

/*
 * Class:     org.gnu.gdk.Property
 * Method:    gdk_property_change
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Property_gdk_1property_1change (JNIEnv *env, jclass 
    cls, jobject window, jobject property, jobject type, jint format, jint mode, jbyteArray data, jint 
    numElements) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkAtom *property_g = (GdkAtom *)getPointerFromHandle(env, property);
    GdkAtom *type_g = (GdkAtom *)getPointerFromHandle(env, type);
    gint32 format_g = (gint32) format;
    GdkPropMode mode_g = (GdkPropMode) mode;
    jint data_len = (*env)->GetArrayLength(env, data);
    guchar* data_g = (guchar*)g_malloc(data_len + 1);
    gint32 numElements_g = (gint32) numElements;
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
  	gdk_property_change (window_g, *property_g, *type_g, format_g, mode_g, data_g, 
		       numElements_g);
}

/*
 * Class:     org.gnu.gdk.Property
 * Method:    gdk_property_delete
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Property_gdk_1property_1delete (JNIEnv *env, jclass 
    cls, jobject window, jobject property) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkAtom *property_g = (GdkAtom *)getPointerFromHandle(env, property);
    gdk_property_delete (window_g, *property_g);
}


#ifdef __cplusplus
}

#endif
