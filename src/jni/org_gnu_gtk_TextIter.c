/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 * This file was orriginally generated by the Java-GNOME Code Generator
 * Please do not modify the code that is identified as generated.  Also,
 * please insert your code above the generated code.
 *
 * Generation date: 2002-08-02 09:42:28 EDT
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TextIter.h"
#ifdef __cplusplus
extern "C" 
{
#endif
    
/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_buffer
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1buffer (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return getGObjectHandle(env, (GObject *) gtk_text_iter_get_buffer (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_free
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1free
		(JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gtk_text_iter_free (iter_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_offset
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1offset (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_offset (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_line
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_line_offset
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1line_1offset (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_line_offset (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_line_index
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1line_1index (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_line_index (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_visible_line_offset
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1visible_1line_1offset (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_visible_line_offset (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_visible_line_index
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1visible_1line_1index (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_visible_line_index (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_char
 * Signature: (I)C
 */
JNIEXPORT jchar JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1char (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jchar) (gtk_text_iter_get_char (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_slice
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1slice (JNIEnv *env, jclass cls, jobject start, jobject end) 
{
    GtkTextIter *start_g = (GtkTextIter *)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter *)getPointerFromHandle(env, end);
    gchar *utf_g = gtk_text_iter_get_slice (start_g, end_g);
    if (utf_g == NULL)
        return NULL;
    return (*env)->NewStringUTF(env, (const char *)utf_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_text
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1text (JNIEnv *env, jclass cls, jobject start, jobject end) 
{
    GtkTextIter *start_g = (GtkTextIter *)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter *)getPointerFromHandle(env, end);
    gchar *utf_g = gtk_text_iter_get_text (start_g, end_g);
    if (utf_g == NULL)
        return NULL;
    return (*env)->NewStringUTF(env, (const char *)utf_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_visible_slice
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1visible_1slice (JNIEnv *env, jclass cls, jobject start, jobject end) 
{
    GtkTextIter *start_g = (GtkTextIter *)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter *)getPointerFromHandle(env, end);
    gchar *utf_g = gtk_text_iter_get_visible_slice (start_g, end_g);
    if (utf_g == NULL)
        return NULL;
    return (*env)->NewStringUTF(env, (const char *)utf_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_visible_text
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1visible_1text (JNIEnv *env, jclass cls, jobject start, jobject end) 
{
    GtkTextIter *start_g = (GtkTextIter *)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter *)getPointerFromHandle(env, end);
    gchar *utf_g = gtk_text_iter_get_visible_text (start_g, end_g);
    if (utf_g == NULL)
        return NULL;
    return (*env)->NewStringUTF(env, (const char *)utf_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_pixbuf
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1pixbuf (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return getGObjectHandle(env, (GObject *) gtk_text_iter_get_pixbuf (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_child_anchor
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1child_1anchor (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return getGObjectHandle(env, (GObject *) gtk_text_iter_get_child_anchor (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_begins_tag
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1begins_1tag (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jboolean) (gtk_text_iter_begins_tag (iter_g, tag_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_ends_tag
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1ends_1tag (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)tag;
    {
        jboolean result_j = (jboolean) (gtk_text_iter_ends_tag (iter_g, tag_g));
        return result_j;
    }
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_toggles_tag
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1toggles_1tag (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jboolean) (gtk_text_iter_toggles_tag (iter_g, tag_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_has_tag
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1has_1tag (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jboolean) (gtk_text_iter_has_tag (iter_g, tag_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_editable
 * Signature: (IZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1editable (JNIEnv *env, jclass cls, jobject iter, jboolean defaultSetting) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gboolean defaultSetting_g = (gboolean) defaultSetting;
    return (jboolean) (gtk_text_iter_editable (iter_g, defaultSetting_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_can_insert
 * Signature: (IZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1can_1insert (JNIEnv *env, jclass cls, jobject iter, jboolean defaultEditablity) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gboolean defaultEditablity_g = (gboolean) defaultEditablity;
    return (jboolean) (gtk_text_iter_can_insert (iter_g, defaultEditablity_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_starts_word
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1starts_1word (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_starts_word (iter_g));

}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_ends_word
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1ends_1word (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_ends_word (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_inside_word
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1inside_1word (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_inside_word (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_starts_sentence
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1starts_1sentence (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_starts_sentence (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_ends_sentence
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1ends_1sentence (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_ends_sentence (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_inside_sentence
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1inside_1sentence (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_inside_sentence (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_starts_line
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1starts_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_starts_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_ends_line
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1ends_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_ends_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_is_cursor_position
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1is_1cursor_1position (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_is_cursor_position (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_chars_in_line
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1chars_1in_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_chars_in_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_bytes_in_line
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1bytes_1in_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_text_iter_get_bytes_in_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_attributes
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1attributes (JNIEnv *env, jclass cls, jobject iter, jobject values) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextAttributes *values_g = (GtkTextAttributes *)getPointerFromHandle(env, values);
    return (jboolean) (gtk_text_iter_get_attributes (iter_g, values_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_get_language
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1get_1language (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return getStructHandle(env, gtk_text_iter_get_language (iter_g), NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_is_end
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1is_1end (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_is_end (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_is_start
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1is_1start (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_is_start (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_char
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1char (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_char (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_char
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1char (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_backward_char (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_chars
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1chars (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_forward_chars (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_chars
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1chars (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_backward_chars (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_line
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_line
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1line (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_backward_line (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_lines
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1lines (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_forward_lines (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_lines
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1lines (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_backward_lines (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_word_end
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1word_1end (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_word_end (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_word_start
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1word_1start (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_backward_word_start (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_word_ends
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1word_1ends (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_forward_word_ends (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_word_starts
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1word_1starts (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_backward_word_starts (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_sentence_end
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1sentence_1end (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_sentence_end (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_sentence_start
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1sentence_1start (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_backward_sentence_start (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_sentence_ends
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1sentence_1ends (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_forward_sentence_ends (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_sentence_starts
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1sentence_1starts (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_backward_sentence_starts (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_cursor_position
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1cursor_1position (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_cursor_position (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_cursor_position
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1cursor_1position (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_backward_cursor_position (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_cursor_positions
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1cursor_1positions (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_forward_cursor_positions (iter_g, count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_cursor_positions
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1cursor_1positions (JNIEnv *env, jclass cls, jobject iter, jint count) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_iter_backward_cursor_positions (iter_g, 
                                                                count_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_offset
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1offset (JNIEnv *env, jclass cls, jobject iter, jint charOffset) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 charOffset_g = (gint32) charOffset;
    gtk_text_iter_set_offset (iter_g, charOffset_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_line
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1line (JNIEnv *env, jclass cls, jobject iter, jint lineNumber) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 lineNumber_g = (gint32) lineNumber;
    gtk_text_iter_set_line (iter_g, lineNumber_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_line_offset
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1line_1offset (JNIEnv *env, jclass cls, jobject iter, jint charOnLine) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 charOnLine_g = (gint32) charOnLine;
    gtk_text_iter_set_line_offset (iter_g, charOnLine_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_line_index
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1line_1index (JNIEnv *env, jclass cls, jobject iter, jint byteOnLine) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 byteOnLine_g = (gint32) byteOnLine;
    gtk_text_iter_set_line_index (iter_g, byteOnLine_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_to_end
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1to_1end (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gtk_text_iter_forward_to_end (iter_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_to_line_end
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1to_1line_1end (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_iter_forward_to_line_end (iter_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_visible_line_offset
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1visible_1line_1offset (JNIEnv *env, jclass cls, jobject iter, jint charOnLine) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 charOnLine_g = (gint32) charOnLine;
    gtk_text_iter_set_visible_line_offset (iter_g, charOnLine_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_set_visible_line_index
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1set_1visible_1line_1index (JNIEnv *env, jclass cls, jobject iter, jint byteOnLine) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 byteOnLine_g = (gint32) byteOnLine;
    gtk_text_iter_set_visible_line_index (iter_g, byteOnLine_g);
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_to_tag_toggle
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1to_1tag_1toggle (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jboolean) (gtk_text_iter_forward_to_tag_toggle (iter_g, tag_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_to_tag_toggle
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1to_1tag_1toggle (JNIEnv *env, jclass cls, jobject iter, jobject tag) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextTag *tag_g = (GtkTextTag *)getPointerFromHandle(env, tag);
    return (jboolean) (gtk_text_iter_backward_to_tag_toggle (iter_g, tag_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_forward_search
 * Signature: (ILjava/lang/String;IIII)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1forward_1search (JNIEnv *env, jclass cls, jobject iter, jstring str, jint flags, jobject matchStart, jobject matchEnd, 
                                                                                       jobject limit) 
{
  GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
  GtkTextIter *matchStart_g = (GtkTextIter*)getPointerFromHandle(env, matchStart);
  GtkTextIter *matchEnd_g = (GtkTextIter*)getPointerFromHandle(env, matchEnd);
  GtkTextIter *limit_g = (GtkTextIter*)getPointerFromHandle(env, limit);

    const char *str_utf = (*env)->GetStringUTFChars(env, str, NULL);
    jboolean result_j = 
        (jboolean) gtk_text_iter_forward_search (iter_g, 
                                                 (gchar*)str_utf, 
                                                 (GtkTextSearchFlags)flags, 
                                                 matchStart_g, 
                                                 matchEnd_g, 
                                                 limit_g);
    updateHandle(env, matchStart, matchStart_g);
    updateHandle(env, matchEnd, matchEnd_g);
    (*env)->ReleaseStringUTFChars(env, str, str_utf);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_backward_search
 * Signature: (ILjava/lang/String;IIII)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1backward_1search (JNIEnv *env, jclass cls, jobject iter, jstring str, jint flags, jobject matchStart, jobject matchEnd, 
                                                                                        jobject limit) 
{
  GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
  GtkTextIter *matchStart_g = (GtkTextIter*)getPointerFromHandle(env, matchStart);
  GtkTextIter *matchEnd_g = (GtkTextIter*)getPointerFromHandle(env, matchEnd);
  GtkTextIter *limit_g = (GtkTextIter*)getPointerFromHandle(env, limit);

    const char *str_utf = (*env)->GetStringUTFChars(env, str, NULL);
    jboolean result_j = 
        (jboolean) gtk_text_iter_backward_search (iter_g, 
                                                  (gchar*)str_utf, 
                                                  (GtkTextSearchFlags)flags, 
                                                  matchStart_g, 
                                                  matchEnd_g, 
                                                  limit_g);
    (*env)->ReleaseStringUTFChars(env, str, str_utf);
    updateHandle(env, matchStart, matchStart_g);
    updateHandle(env, matchEnd, matchEnd_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_equal
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1equal (JNIEnv *env, jclass cls, jobject lhs, jobject rhs) 
{
    GtkTextIter *lhs_g = (GtkTextIter *)getPointerFromHandle(env, lhs);
    GtkTextIter *rhs_g = (GtkTextIter *)getPointerFromHandle(env, rhs);
    return (jboolean) (gtk_text_iter_equal (lhs_g, rhs_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_compare
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1compare (JNIEnv *env, jclass cls, jobject lhs, jobject rhs) 
{
    GtkTextIter *lhs_g = (GtkTextIter *)getPointerFromHandle(env, lhs);
    GtkTextIter *rhs_g = (GtkTextIter *)getPointerFromHandle(env, rhs);
    return (jint) (gtk_text_iter_compare (lhs_g, rhs_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_in_range
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1in_1range (JNIEnv *env, jclass cls, jobject iter, jobject start, jobject end) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    GtkTextIter *start_g = (GtkTextIter *)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter *)getPointerFromHandle(env, end);
    return (jboolean) (gtk_text_iter_in_range (iter_g, start_g, end_g));
}

/*
 * Class:     org.gnu.gtk.TextIter
 * Method:    gtk_text_iter_order
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1order (JNIEnv *env, jclass cls, jobject first, jobject second) 
{
    GtkTextIter *first_g = (GtkTextIter *)getPointerFromHandle(env, first);
    GtkTextIter *second_g = (GtkTextIter *)getPointerFromHandle(env, second);
    gtk_text_iter_order (first_g, second_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1isPixbuf (JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (NULL != gtk_text_iter_get_pixbuf (iter_g));
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextIter_gtk_1text_1iter_1isChildAnchor(JNIEnv *env, jclass cls, jobject iter) 
{
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (NULL != gtk_text_iter_get_child_anchor (iter_g));
}

#ifdef __cplusplus
}

#endif
