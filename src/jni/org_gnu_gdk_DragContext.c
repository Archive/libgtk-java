/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <gtk/gtkdnd.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_DragContext.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getProtocol
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_DragContext_getProtocol 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return (jint) obj_g->protocol;
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getIsSource
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_DragContext_getIsSource 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return (jboolean)obj_g->is_source;
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getSourceWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_DragContext_getSourceWindow 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return getGObjectHandleAndRef(env, (GObject*)obj_g->source_window);
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getDestWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_DragContext_getDestWindow 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject*)obj_g->dest_window);
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getActions
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_DragContext_getActions 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return (jint) obj_g->actions;
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getSuggestedAction
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_DragContext_getSuggestedAction 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return (jint) obj_g->suggested_action;
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    getAction
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_DragContext_getAction 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDragContext *obj_g;
    
    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    return (jint) obj_g->action;
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    gdk_drag_context_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_DragContext_gdk_1drag_1context_1get_1type 
	(JNIEnv *env, jclass cls) 
{
    return (jint)gdk_drag_context_get_type ();
}

/*
 * Class:     org.gnu.gdk.DragContext
 * Method:    gdk_drag_context_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_DragContext_gdk_1drag_1context_1new 
	(JNIEnv *env, jclass cls) 
{
	GdkDragContext *dc;
	
	dc = gdk_drag_context_new();
	
    return getGObjectHandle(env, (GObject*)dc);
}

/*
 * Class:     org_gnu_gdk_DragContext
 * Method:    getTargets
 * Signature: (Lorg/gnu/glib/Handle;)[Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_DragContext_getTargets
  (JNIEnv *env, jclass cls, jobject obj)
{
    GdkDragContext *obj_g;
  	
    obj_g = (GdkDragContext *) getPointerFromHandle(env, obj);
    if (obj_g->targets != NULL)
        // MEM_MGT_NOTE: ok. Returns handles to Atoms which are not memory managed.
        return getHandleArrayFromGList(env, obj_g->targets);
    return NULL;
}
  
JNIEXPORT void JNICALL Java_org_gnu_gdk_DragContext_gtk_1drag_1finish
  (JNIEnv *env, jclass cls, jobject obj, jboolean success, jboolean del, jint time)
{
	GdkDragContext *obj_g; 

    obj_g = (GdkDragContext *)getPointerFromHandle(env, obj);
    gboolean success_g = (gboolean)success;
    gboolean del_g = (gboolean)del;
    gtk_drag_finish(obj_g, success_g, del_g, time);
}


#ifdef __cplusplus
}

#endif
