/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Rectangle.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GdkRectangle_get_x (GdkRectangle * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Rectangle_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    return (jint) (GdkRectangle_get_x (obj_g));
}

static void GdkRectangle_set_x (GdkRectangle * cptr, gint32 x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    setX
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Rectangle_setX (JNIEnv *env, jclass cls, jobject obj, jint x) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    gint32 x_g = (gint32) x;
    GdkRectangle_set_x (obj_g, x_g);
}

static gint32 GdkRectangle_get_y (GdkRectangle * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Rectangle_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    return (jint) (GdkRectangle_get_y (obj_g));
}

static void GdkRectangle_set_y (GdkRectangle * cptr, gint32 y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Rectangle_setY (JNIEnv *env, jclass cls, jobject obj, jint y) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    gint32 y_g = (gint32) y;
    GdkRectangle_set_y (obj_g, y_g);
}

static guint32 GdkRectangle_get_width (GdkRectangle * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Rectangle_getWidth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    return (jint) (GdkRectangle_get_width (obj_g));
}

static void GdkRectangle_set_width (GdkRectangle * cptr, guint32 width) 
{
    cptr->width = width;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    setWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Rectangle_setWidth (JNIEnv *env, jclass cls, jobject obj, jint width) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    guint32 width_g = (guint32) width;
    GdkRectangle_set_width (obj_g, width_g);
}

static guint32 GdkRectangle_get_height (GdkRectangle * cptr) 
{
    return cptr->height;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Rectangle_getHeight (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    return (jint) (GdkRectangle_get_height (obj_g));
}

static void GdkRectangle_set_height (GdkRectangle * cptr, guint32 height) 
{
    cptr->height = height;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    setHeight
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Rectangle_setHeight (JNIEnv *env, jclass cls, jobject obj, jint height)
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    guint32 height_g = (guint32) height;
    GdkRectangle_set_height (obj_g, height_g);
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    gdk_rectangle_intersect
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Rectangle_gdk_1rectangle_1intersect (JNIEnv *env, 
    jclass cls, jobject src1, jobject src2) 
{
    GdkRectangle *src1_g = (GdkRectangle *)getPointerFromHandle(env, src1);
    GdkRectangle *src2_g = (GdkRectangle *)getPointerFromHandle(env, src2);
    GdkRectangle *dest = g_new(GdkRectangle, 1);
    gboolean val = gdk_rectangle_intersect (src1_g, src2_g, dest);
    if (val == TRUE)	{
    	return getGBoxedHandle(env, dest, GDK_TYPE_RECTANGLE, NULL,
    		(GBoxedFreeFunc) g_free);
	}
	g_free(dest);
	return NULL;
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    gdk_rectangle_union
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Rectangle_gdk_1rectangle_1union
		(JNIEnv *env, jclass cls, jobject src1, jobject src2) 
{
    GdkRectangle *src1_g = (GdkRectangle *)getPointerFromHandle(env, src1);
    GdkRectangle *src2_g = (GdkRectangle *)getPointerFromHandle(env, src2);
    GdkRectangle *dest = g_new(GdkRectangle, 1);
    gdk_rectangle_union (src1_g, src2_g, dest);
    return getGBoxedHandle(env, dest, GDK_TYPE_RECTANGLE, NULL, (GBoxedFreeFunc)
    		g_free);
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    gdk_rectangle_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Rectangle_gdk_1rectangle_1new
  (JNIEnv *env, jclass cls) 
{
    GdkRectangle *obj_g = (GdkRectangle *)g_new(GdkRectangle, 1);
    return getGBoxedHandle(env, obj_g, gdk_rectangle_get_type(), NULL, 
    		(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    gdk_rectangle_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Rectangle_gdk_1rectangle_1free
  (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkRectangle *obj_g = (GdkRectangle *)getPointerFromHandle(env, obj);
    g_free(obj_g);
}

/*
 * Class:     org.gnu.gdk.Rectangle
 * Method:    gdk_rectangle_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Rectangle_gdk_1rectangle_1get_1type
  (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_rectangle_get_type();
}


#ifdef __cplusplus
}

#endif
