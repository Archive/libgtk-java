/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"

#include "org_gnu_pango_AttrInt.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.AttrInt
 * Method:    getValue
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_AttrInt_getValue (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrInt *obj_g = (PangoAttrInt *)getPointerFromHandle(env, obj);
    return (jint)obj_g->value;
}

#ifdef __cplusplus
}

#endif
