/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToolItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_tool_item_get_type();
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1new(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, (GObject *) gtk_tool_item_new());
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_homogeneous
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1homogeneous(JNIEnv *env, jclass cls, jobject item, jboolean homo)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean homo_g = (gboolean)homo;
    gtk_tool_item_set_homogeneous(item_g, homo_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_homogeneous
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1homogeneous(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_tool_item_get_homogeneous(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_expand
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1expand(JNIEnv *env, jclass cls, jobject item, jboolean expand)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean expand_g = (gboolean)expand;
    gtk_tool_item_set_expand(item_g, expand_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_expand
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1expand(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (gboolean)gtk_tool_item_get_expand(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_tooltip
 * Signature: (IILjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1tooltip(JNIEnv *env, jclass cls, jobject item, jobject tip, jstring text, jstring private)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    GtkTooltips *tip_g = (GtkTooltips*)getPointerFromHandle(env, tip);
    const gchar* t = (*env)->GetStringUTFChars(env, text, NULL);
    const gchar* p = (*env)->GetStringUTFChars(env, private, NULL);
    gtk_tool_item_set_tooltip(item_g, tip_g, t, p);
    (*env)->ReleaseStringUTFChars(env, text, t);
    (*env)->ReleaseStringUTFChars(env, private, p);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_use_drag_window
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1use_1drag_1window(JNIEnv *env, jclass cls, jobject item, jboolean drag)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean drag_g = (gboolean)drag;
    gtk_tool_item_set_use_drag_window(item_g, drag_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_use_drag_window
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1use_1drag_1window(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_tool_item_get_use_drag_window(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_visible_horizontal
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1visible_1horizontal(JNIEnv *env, jclass cls, jobject item, jboolean visible)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean visible_g = (gboolean)visible;
    gtk_tool_item_set_visible_horizontal(item_g, visible_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_visible_horizontal
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1visible_1horizontal(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_tool_item_get_visible_horizontal(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_visible_vertical
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1visible_1vertical(JNIEnv *env, jclass cls, jobject item, jboolean visible)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean visible_g = (gboolean)visible;
    gtk_tool_item_set_visible_vertical(item_g, visible_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_visible_vertical
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1visible_1vertical(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_tool_item_get_visible_vertical(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_is_important
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1is_1important(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_tool_item_get_is_important(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_is_important
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1is_1important(JNIEnv *env, jclass cls, jobject item, jboolean important)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    gboolean important_g = (gboolean)important;
    gtk_tool_item_set_is_important(item_g, important_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_icon_size
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1icon_1size(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jint)gtk_tool_item_get_icon_size(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_orientation
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1orientation(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jint)gtk_tool_item_get_orientation(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_toolbar_style
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1toolbar_1style(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jint)gtk_tool_item_get_toolbar_style(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_relief_style
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1relief_1style(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return (jint)gtk_tool_item_get_relief_style(item_g);
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_retrieve_proxy_menu_item
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1retrieve_1proxy_1menu_1item
		(JNIEnv *env, jclass cls, jobject item)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    return getGObjectHandle(env, (GObject *) gtk_tool_item_retrieve_proxy_menu_item(item_g));
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_get_proxy_menu_item
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1get_1proxy_1menu_1item(JNIEnv *env, jclass cls, jobject item, jstring menuId)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    const gchar* m = (*env)->GetStringUTFChars(env, menuId, NULL);
    jobject ret = getGObjectHandle(env, (GObject *) gtk_tool_item_get_proxy_menu_item(item_g, m));
    (*env)->ReleaseStringUTFChars(env, menuId, m);
    return ret;
}

/*
 * Class:     org_gnu_gtk_ToolItem
 * Method:    gtk_tool_item_set_proxy_menu_item
 * Signature: (ILjava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1set_1proxy_1menu_1item(JNIEnv *env, jclass cls, jobject item, jstring menuId, jobject menuItem)
{
    GtkToolItem *item_g = (GtkToolItem*)getPointerFromHandle(env, item);
    const gchar* m = (*env)->GetStringUTFChars(env, menuId, NULL);
    GtkWidget *menuItem_g = (GtkWidget*)getPointerFromHandle(env, menuItem);
    gtk_tool_item_set_proxy_menu_item(item_g, m, menuItem_g);
    (*env)->ReleaseStringUTFChars(env, menuId, m);
}

/* GTK 2.6 additions. */

JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolItem_gtk_1tool_1item_1rebuild_1menu
(JNIEnv *env, jclass cls, jobject tool_item)
{
    GtkToolItem * tool_item_g = 
        (GtkToolItem *)getPointerFromHandle(env, tool_item);
    gtk_tool_item_rebuild_menu(tool_item_g);
}

#ifdef __cplusplus
}
#endif
