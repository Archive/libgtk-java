/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventSelection.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventSelection_getWindow (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, G_OBJECT(obj_g->window));
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventSelection_getSendEvent (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getSelection
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventSelection_getSelection (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->selection, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getTarget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventSelection_getTarget (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->target, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getProperty
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventSelection_getProperty (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return getStructHandle(env, obj_g->property, NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getRequestor
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventSelection_getRequestor (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    /* In the struct this is an guint32, which means that putting it into a 64 bit pointer is a
       nono (?). This appears to be unused so leave it like this for now, will change later */
    return (jint) obj_g->requestor;
}

/*
 * Class:     org.gnu.gdk.EventSelection
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventSelection_getTime (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkEventSelection *obj_g = (GdkEventSelection *)getPointerFromHandle(env, obj);
    return obj_g->time;
}


#ifdef __cplusplus
}

#endif
