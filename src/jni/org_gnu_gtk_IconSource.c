/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IconSource.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1new (JNIEnv *env, jclass 
    cls) 
{
    return getGBoxedHandle(env, gtk_icon_source_new(), GTK_TYPE_ICON_SOURCE,
    		NULL, (GBoxedFreeFunc) gtk_icon_source_free);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_free
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1free (JNIEnv *env, jclass 
    cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gtk_icon_source_free (source_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_filename
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1filename (JNIEnv 
    *env, jclass cls, jobject source, jstring filename) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gtk_icon_source_set_filename (source_g, filename_g);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_pixbuf
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1pixbuf (JNIEnv *env, 
    jclass cls, jobject source, jobject pixbuf) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gtk_icon_source_set_pixbuf (source_g, pixbuf_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1filename (
    JNIEnv *env, jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gchar *result_g = (gchar*)gtk_icon_source_get_filename (source_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1pixbuf (JNIEnv *env, 
    jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return getGObjectHandle(env, (GObject *) gtk_icon_source_get_pixbuf (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_direction_wildcarded
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1direction_1wildcarded (JNIEnv *env, jclass 
    cls, jobject source, jboolean setting) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gboolean setting_g = (gboolean) setting;
    gtk_icon_source_set_direction_wildcarded (source_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_state_wildcarded
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1state_1wildcarded (
    JNIEnv *env, jclass cls, jobject source, jboolean setting) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gboolean setting_g = (gboolean) setting;
    gtk_icon_source_set_state_wildcarded (source_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_size_wildcarded
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1size_1wildcarded (
    JNIEnv *env, jclass cls, jobject source, jboolean setting) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    gboolean setting_g = (gboolean) setting;
    gtk_icon_source_set_size_wildcarded (source_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_size_wildcarded
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1size_1wildcarded (
    JNIEnv *env, jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jboolean) (gtk_icon_source_get_size_wildcarded (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_state_wildcarded
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1state_1wildcarded (JNIEnv *env, jclass cls, 
    jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jboolean) (gtk_icon_source_get_state_wildcarded (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_direction_wildcarded
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1direction_1wildcarded (JNIEnv *env, jclass 
    cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jboolean) (gtk_icon_source_get_direction_wildcarded (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_direction
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1direction (JNIEnv 
    *env, jclass cls, jobject source, jint direction) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    GtkTextDirection direction_g = (GtkTextDirection) direction;
    gtk_icon_source_set_direction (source_g, direction_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_state
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1state (JNIEnv *env, 
    jclass cls, jobject source, jint state) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    GtkStateType state_g = (GtkStateType) state;
    gtk_icon_source_set_state (source_g, state_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_set_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1size (JNIEnv *env, 
    jclass cls, jobject source, jint size) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    GtkIconSize size_g = (GtkIconSize) size;
    gtk_icon_source_set_size (source_g, size_g);
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_direction
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1direction (JNIEnv 
    *env, jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jint) (gtk_icon_source_get_direction (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_state
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1state (JNIEnv *env, 
    jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jint) (gtk_icon_source_get_state (source_g));
}

/*
 * Class:     org.gnu.gtk.IconSource
 * Method:    gtk_icon_source_get_size
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1size (JNIEnv *env, 
    jclass cls, jobject source) 
{
    GtkIconSource *source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    return (jint) (gtk_icon_source_get_size (source_g));
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_icon_source_get_type();
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1set_1icon_1name
(JNIEnv *env, jclass cls, jobject source, jstring icon_name)
{
    GtkIconSource * source_g = (GtkIconSource *)getPointerFromHandle(env, source);
    const gchar * icon_name_g = (const gchar *)(*env)->GetStringUTFChars(env, icon_name, 0);
    gtk_icon_source_set_icon_name(source_g, icon_name_g);
    (*env)->ReleaseStringUTFChars(env, icon_name, icon_name_g);
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconSource_gtk_1icon_1source_1get_1icon_1name
(JNIEnv *env, jclass cls, jobject source)
{
    const GtkIconSource * source_g = (const GtkIconSource *)getPointerFromHandle(env, source);
    return (*env)->NewStringUTF(env, gtk_icon_source_get_icon_name(source_g));
}


#ifdef __cplusplus
}

#endif
