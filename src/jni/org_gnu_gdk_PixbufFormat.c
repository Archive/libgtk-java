/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gdk_PixbufFormat
#define _Included_org_gnu_gdk_PixbufFormat
#include "org_gnu_gdk_PixbufFormat.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1get_1name
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (*env)->NewStringUTF(env, gdk_pixbuf_format_get_name(format_g));
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_get_description
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1get_1description
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (*env)->NewStringUTF(env, gdk_pixbuf_format_get_description(format_g));
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_get_mime_types
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1get_1mime_1types
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return getJavaStringArray(env, (const gchar**)gdk_pixbuf_format_get_mime_types(format_g));
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_get_extensions
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1get_1extensions
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return getJavaStringArray(env, (const gchar**)gdk_pixbuf_format_get_extensions(format_g));
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_is_writable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1is_1writable
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (jboolean)gdk_pixbuf_format_is_writable(format_g);
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_is_scalable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1is_1scalable
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (jboolean)gdk_pixbuf_format_is_scalable(format_g);
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_is_disabled
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1is_1disabled
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (jboolean)gdk_pixbuf_format_is_disabled(format_g);
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_set_disabled
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1set_1disabled
  (JNIEnv* env, jclass cls, jobject format, jboolean disabled)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	gdk_pixbuf_format_set_disabled(format_g, (gboolean)disabled);
}

/*
 * Class:     org_gnu_gdk_PixbufFormat
 * Method:    gdk_pixbuf_format_get_license
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_PixbufFormat_gdk_1pixbuf_1format_1get_1license
  (JNIEnv* env, jclass cls, jobject format)
{
	GdkPixbufFormat* format_g = (GdkPixbufFormat*)getPointerFromHandle(env, format);
	return (*env)->NewStringUTF(env, gdk_pixbuf_format_get_license(format_g));
}

#ifdef __cplusplus
}
#endif
#endif
