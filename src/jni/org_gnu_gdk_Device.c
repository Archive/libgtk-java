/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Device.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Device
 * Method:    getName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Device_getName 
	(JNIEnv *env, jclass cls, jobject obj)
{
    GdkDevice *obj_g = (GdkDevice *)getPointerFromHandle(env, obj);
    gchar *result_g = obj_g->name;
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    getSource
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Device_getSource 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDevice *obj_g = (GdkDevice *)getPointerFromHandle(env, obj);
    return (jint) obj_g->source;
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    getMode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Device_getMode 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDevice *obj_g = (GdkDevice *)getPointerFromHandle(env, obj);
    return (jint) obj_g->mode;
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    getHasCursor
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Device_getHasCursor 
	(JNIEnv *env, jclass cls, jobject obj) 
{
    GdkDevice *obj_g = (GdkDevice *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->has_cursor;
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Device_gdk_1device_1get_1type 
	(JNIEnv *env, jclass cls) 
{
    return (jint)gdk_device_get_type ();
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_devices_list
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Device_gdk_1devices_1list 
	(JNIEnv *env, jclass cls) 
{
    GList *list = gdk_devices_list();
    jobjectArray ret = getGObjectHandlesFromGList( env, list );
    return ret;
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_set_source
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Device_gdk_1device_1set_1source 
	(JNIEnv *env, jclass cls, jobject device, jint source) 
{
    GdkDevice *device_g = (GdkDevice *)getPointerFromHandle(env, device);
    GdkInputSource source_g = (GdkInputSource) source;
    gdk_device_set_source (device_g, source_g);
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_set_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Device_gdk_1device_1set_1mode 
	(JNIEnv *env, jclass cls, jobject device, jint mode) 
{
    GdkDevice *device_g = (GdkDevice *)getPointerFromHandle(env, device);
    GdkInputMode mode_g = (GdkInputMode) mode;
    gdk_device_set_mode (device_g, mode_g);
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_set_key
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Device_gdk_1device_1set_1key 
	(JNIEnv *env, jclass cls, jobject device, jint index, jint keyval, 
	jint modifiers) 
{
    GdkDevice *device_g = (GdkDevice *)getPointerFromHandle(env, device);
    gint32 index_g = (gint32) index;
    gint32 keyval_g = (gint32) keyval;
    GdkModifierType modifiers_g = (GdkModifierType) modifiers;
    gdk_device_set_key (device_g, index_g, keyval_g, modifiers_g);
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_set_axis_use
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Device_gdk_1device_1set_1axis_1use 
	(JNIEnv *env, jclass cls, jobject device, jint index, jint use) 
{
    GdkDevice *device_g = (GdkDevice *)getPointerFromHandle(env, device);
    gint32 index_g = (gint32) index;
    GdkAxisUse use_g = (GdkAxisUse) use;
    gdk_device_set_axis_use (device_g, index_g, use_g);
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_get_axis
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Device_gdk_1device_1get_1axis 
	(JNIEnv *env, jclass  cls, jobject device, jdoubleArray axes, jint use, 
	jdoubleArray value) 
{
    GdkDevice *device_g = (GdkDevice *)getPointerFromHandle(env, device);
    gdouble *axes_g = (gdouble *) (*env)->GetDoubleArrayElements (env, axes, NULL);
    GdkAxisUse use_g = (GdkAxisUse) use;
    gdouble *value_g = (gdouble *) (*env)->GetDoubleArrayElements (env, value, NULL);
    jboolean result_j = (jboolean) (gdk_device_get_axis (device_g, axes_g, use_g, value_g));
    (*env)->ReleaseDoubleArrayElements (env, axes, (jdouble *) axes_g, 0);
    (*env)->ReleaseDoubleArrayElements (env, value, (jdouble *) value_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Device
 * Method:    gdk_device_get_core_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Device_gdk_1device_1get_1core_1pointer 
	(JNIEnv *env, jclass cls) 
{
	GdkDevice *device;
	
	device = gdk_device_get_core_pointer();
	
	return getGObjectHandle(env, G_OBJECT(device));
}


#ifdef __cplusplus
}

#endif
