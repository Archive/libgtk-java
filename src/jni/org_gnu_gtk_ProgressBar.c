/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ProgressBar.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_progress_bar_get_type ();
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_progress_bar_new ());
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_pulse
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1pulse (JNIEnv *env, 
    jclass cls, jobject pbar) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    gtk_progress_bar_pulse (pbar_g);
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1set_1text (JNIEnv *env, 
    jclass cls, jobject pbar, jstring text) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
	gtk_progress_bar_set_text (pbar_g, text_g);
	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_set_fraction
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1set_1fraction (JNIEnv 
    *env, jclass cls, jobject pbar, jdouble fraction) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    gdouble fraction_g = (gdouble) fraction;
    gtk_progress_bar_set_fraction (pbar_g, fraction_g);
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_set_pulse_step
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1set_1pulse_1step (
    JNIEnv *env, jclass cls, jobject pbar, jdouble fraction) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    gdouble fraction_g = (gdouble) fraction;
    gtk_progress_bar_set_pulse_step (pbar_g, fraction_g);
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_set_orientation
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1set_1orientation (
    JNIEnv *env, jclass cls, jobject pbar, jint orientation) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    GtkProgressBarOrientation orientation_g = (GtkProgressBarOrientation) orientation;
    gtk_progress_bar_set_orientation (pbar_g, orientation_g);
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1text (JNIEnv 
    *env, jclass cls, jobject pbar) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
	return (*env)->NewStringUTF(env,  (gchar*)gtk_progress_bar_get_text (pbar_g)) ;
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_get_fraction
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1fraction (
    JNIEnv *env, jclass cls, jobject pbar) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    return (jdouble) (gtk_progress_bar_get_fraction (pbar_g));
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_get_pulse_step
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1pulse_1step (
    JNIEnv *env, jclass cls, jobject pbar) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    return (jdouble) (gtk_progress_bar_get_pulse_step (pbar_g));
}

/*
 * Class:     org.gnu.gtk.ProgressBar
 * Method:    gtk_progress_bar_get_orientation
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1orientation (
    JNIEnv *env, jclass cls, jobject pbar) 
{
    GtkProgressBar *pbar_g = (GtkProgressBar *)getPointerFromHandle(env, pbar);
    return (jint) (gtk_progress_bar_get_orientation (pbar_g));
}

/* GTK 2.6 additions. */

JNIEXPORT void JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1set_1ellipsize
(JNIEnv *env, jclass cls, jobject pbar, jint mode)
{
    GtkProgressBar * pbar_g = 
        (GtkProgressBar *)getPointerFromHandle(env, pbar);
    PangoEllipsizeMode mode_g = (PangoEllipsizeMode)mode;
    gtk_progress_bar_set_ellipsize(pbar_g, mode_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_ProgressBar_gtk_1progress_1bar_1get_1ellipsize
(JNIEnv *env, jclass cls, jobject pbar)
{
    GtkProgressBar * pbar_g = 
        (GtkProgressBar *)getPointerFromHandle(env, pbar);
    return (jint)gtk_progress_bar_get_ellipsize(pbar_g);
}

#ifdef __cplusplus
}

#endif
