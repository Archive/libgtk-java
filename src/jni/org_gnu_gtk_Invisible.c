/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Invisible.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Invisible
 * Method:    gtk_invisible_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Invisible_gtk_1invisible_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_invisible_get_type ();
}

/*
 * Class:     org.gnu.gtk.Invisible
 * Method:    gtk_invisible_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Invisible_gtk_1invisible_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_invisible_new ());
}

/*
 * Class:     org_gnu_gtk_Invisible
 * Method:    gtk_invisible_new_for_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Invisible_gtk_1invisible_1new_1for_1screen
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *) gtk_invisible_new_for_screen(screen_g));
}
                                                                                                           
/*
 * Class:     org_gnu_gtk_Invisible
 * Method:    gtk_invisible_set_screen
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Invisible_gtk_1invisible_1set_1screen
  (JNIEnv *env, jclass cls, jobject inv, jobject screen)
{
	GtkInvisible* inv_g = (GtkInvisible*)getPointerFromHandle(env, inv);
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	gtk_invisible_set_screen(inv_g, screen_g);
}
                                                                                                           
/*
 * Class:     org_gnu_gtk_Invisible
 * Method:    gtk_invisible_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Invisible_gtk_1invisible_1get_1screen
  (JNIEnv *env, jclass cls, jobject inv)
{
	GtkInvisible* inv_g = (GtkInvisible*)getPointerFromHandle(env, inv);
	return getGObjectHandle(env, (GObject *) gtk_invisible_get_screen(inv_g));
}
       

#ifdef __cplusplus
}

#endif
