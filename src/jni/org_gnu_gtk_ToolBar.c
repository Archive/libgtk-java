/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToolBar.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_toolbar_get_type ();
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_toolbar_new ());
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_append_space
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1append_1space (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gtk_toolbar_append_space (toolbar_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_prepend_space
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1prepend_1space (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gtk_toolbar_prepend_space (toolbar_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_insert_space
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1insert_1space (JNIEnv *env, jclass cls, jobject toolbar, jint position) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gint32 position_g = (gint32) position;
    gtk_toolbar_insert_space (toolbar_g, position_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_remove_space
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1remove_1space (JNIEnv *env, jclass cls, jobject toolbar, jint position) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gint32 position_g = (gint32) position;
    gtk_toolbar_remove_space (toolbar_g, position_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_append_widget
 * Signature: (IIjava.lang.String;java.lang.String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1append_1widget (JNIEnv *env, jclass cls, jobject toolbar, jobject widget, jstring tooltipText, jstring tooltipPrivateText) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkWidget *widget_g = (GtkWidget*)getPointerFromHandle(env, widget);
    gchar* tooltipText_g = NULL;
    gchar* tooltipPrivateText_g = NULL;
        
    if (tooltipText != NULL)
        tooltipText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipText, 0);
    if (tooltipPrivateText != NULL)
        tooltipPrivateText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipPrivateText, 0);

    gtk_toolbar_append_widget (toolbar_g, widget_g, 
                               tooltipText_g, tooltipPrivateText_g);

    if (tooltipText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipText, tooltipText_g);
    if (tooltipPrivateText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipPrivateText, tooltipPrivateText_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_prepend_widget
 * Signature: (IIjava.lang.String;java.lang.String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1prepend_1widget (JNIEnv *env, jclass cls, jobject toolbar, jobject widget, jstring tooltipText, jstring tooltipPrivateText) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkWidget *widget_g = (GtkWidget*)getPointerFromHandle(env, widget);
    gchar* tooltipText_g = NULL;
    gchar* tooltipPrivateText_g = NULL;
        
    if (tooltipText != NULL)
        tooltipText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipText, 0);
    if (tooltipPrivateText != NULL)
        tooltipPrivateText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipPrivateText, 0);

    gtk_toolbar_prepend_widget (toolbar_g, widget_g, 
                                tooltipText_g, tooltipPrivateText_g);

    if (tooltipText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipText, tooltipText_g);
    if (tooltipPrivateText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipPrivateText, tooltipPrivateText_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_insert_widget
 * Signature: (IIjava.lang.String;java.lang.String;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1insert_1widget (JNIEnv *env, jclass cls, jobject toolbar, jobject widget, jstring tooltipText, jstring tooltipPrivateText, jint position) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkWidget *widget_g = (GtkWidget*)getPointerFromHandle(env, widget);
    gint32 position_g = (gint32)position;
    gchar* tooltipText_g = NULL;
    gchar* tooltipPrivateText_g = NULL;
        
    if (tooltipText != NULL)
        tooltipText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipText, 0);
    if (tooltipPrivateText != NULL)
        tooltipPrivateText_g = (gchar*)(*env)->GetStringUTFChars(env, tooltipPrivateText, 0);

    gtk_toolbar_insert_widget (toolbar_g, widget_g, 
                               tooltipText_g, tooltipPrivateText_g, 
                               position_g);

    if (tooltipText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipText, tooltipText_g);
    if (tooltipPrivateText != NULL)
        (*env)->ReleaseStringUTFChars(env, tooltipPrivateText, tooltipPrivateText_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_set_orientation
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1orientation (JNIEnv *env, jclass cls, jobject toolbar, jint orientation) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkOrientation orientation_g = (GtkOrientation) orientation;
    gtk_toolbar_set_orientation (toolbar_g, orientation_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_set_style
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1style (JNIEnv *env, jclass cls, jobject toolbar, jint style) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkToolbarStyle style_g = (GtkToolbarStyle) style;
    gtk_toolbar_set_style (toolbar_g, style_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_set_icon_size
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1icon_1size (JNIEnv *env, jclass cls, jobject toolbar, jint iconSize) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkIconSize iconSize_g = (GtkIconSize) iconSize;
    gtk_toolbar_set_icon_size (toolbar_g, iconSize_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_set_tooltips
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1tooltips (JNIEnv *env, jclass cls, jobject toolbar, jboolean enable) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gboolean enable_g = (gboolean) enable;
    gtk_toolbar_set_tooltips (toolbar_g, enable_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_unset_style
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1unset_1style (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gtk_toolbar_unset_style (toolbar_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_unset_icon_size
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1unset_1icon_1size (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gtk_toolbar_unset_icon_size (toolbar_g);
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_get_orientation
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1orientation (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jint) (gtk_toolbar_get_orientation (toolbar_g));
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_get_style
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1style (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jint) (gtk_toolbar_get_style (toolbar_g));
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_get_icon_size
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1icon_1size (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jint) (gtk_toolbar_get_icon_size (toolbar_g));
}

/*
 * Class:     org.gnu.gtk.ToolBar
 * Method:    gtk_toolbar_get_tooltips
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1tooltips (JNIEnv *env, jclass cls, jobject toolbar) 
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jboolean) (gtk_toolbar_get_tooltips (toolbar_g));
}


/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_insert
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1insert(JNIEnv *env, jclass cls, jobject toolbar, jobject toolitem, jint pos)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkToolItem *toolitem_g = 
        (GtkToolItem *)getPointerFromHandle(env, toolitem);
    gint pos_g = (gint)pos;
    gtk_toolbar_insert(toolbar_g, toolitem_g, pos_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_item_index
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1item_1index(JNIEnv *env, jclass cls, jobject toolbar, jobject item)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkToolItem *item_g = 
        (GtkToolItem *)getPointerFromHandle(env, item);
    return (jint)gtk_toolbar_get_item_index(toolbar_g, item_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_n_items
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1n_1items(JNIEnv *env, jclass cls, jobject toolbar)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jint)gtk_toolbar_get_n_items(toolbar_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_nth_item
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1nth_1item(JNIEnv *env, jclass cls, jobject toolbar, jint pos)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gint pos_g = (gint)pos;
    return getGObjectHandle(env, (GObject *) gtk_toolbar_get_nth_item(toolbar_g, pos_g));
}

/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_show_arrow
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1show_1arrow(JNIEnv *env, jclass cls, jobject toolbar)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jboolean)gtk_toolbar_get_show_arrow(toolbar_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_set_show_arrow
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1show_1arrow(JNIEnv *env, jclass cls, jobject toolbar, jboolean val)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gboolean val_g = (gboolean)val;
    gtk_toolbar_set_show_arrow(toolbar_g, val_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_relief_style
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1relief_1style(JNIEnv *env, jclass cls, jobject toolbar)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    return (jint)gtk_toolbar_get_relief_style(toolbar_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_get_drop_index
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1get_1drop_1index(JNIEnv *env, jclass cls, jobject toolbar, jint x, jint y)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    gint x_g = (gint)x;
    gint y_g = (gint)y;
    return (jint)gtk_toolbar_get_drop_index(toolbar_g, x_g, y_g);
}
                                                                                                     
/*
 * Class:     org_gnu_gtk_ToolBar
 * Method:    gtk_toolbar_set_drop_highlight_item
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolBar_gtk_1toolbar_1set_1drop_1highlight_1item(JNIEnv *env, jclass cls, jobject toolbar, jobject item, jint index)
{
    GtkToolbar *toolbar_g = (GtkToolbar *)getPointerFromHandle(env, toolbar);
    GtkToolItem *item_g = (GtkToolItem *)getPointerFromHandle(env, item);
    gint index_g = (gint)index;
    gtk_toolbar_set_drop_highlight_item(toolbar_g, item_g, index_g);
}

#ifdef __cplusplus
}

#endif
