/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Label.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_label_get_type ();
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Label_gtk_1label_1new (JNIEnv *env, jclass cls, 
    jstring str) 
{
	const  char *str_g = (*env)->GetStringUTFChars(env, str, 0);
    jobject result =  getGObjectHandle(env, (GObject *) gtk_label_new (str_g));
	(*env)->ReleaseStringUTFChars(env, str, str_g);
	return result;
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Label_gtk_1label_1new_1with_1mnemonic (JNIEnv *env, 
    jclass cls, jstring str) 
{
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);
    jobject result =  getGObjectHandle(env, (GObject *) gtk_label_new_with_mnemonic (str_g));
	(*env)->ReleaseStringUTFChars(env, str, str_g);
	return result;
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1text (JNIEnv *env, jclass cls, 
    jobject label, jstring str) 
{
	GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);
	gtk_label_set_text (label_g, str_g);
	(*env)->ReleaseStringUTFChars(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1text (JNIEnv *env, jclass 
    cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gchar *result_g = (gchar*)gtk_label_get_text (label_g);
	return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_attributes
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1attributes (JNIEnv *env, jclass 
    cls, jobject label, jobject attrs) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    PangoAttrList *attrs_g = (PangoAttrList *)getPointerFromHandle(env, attrs);
    gtk_label_set_attributes (label_g, attrs_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_attributes
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1attributes (JNIEnv *env, jclass 
    cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    PangoAttrList *list = gtk_label_get_attributes (label_g);
    return getGBoxedHandle(env, list,
                               PANGO_TYPE_ATTR_LIST, 
                               (GBoxedCopyFunc)pango_attr_list_ref,
                               (GBoxedFreeFunc)pango_attr_list_unref);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_label
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1label (JNIEnv *env, jclass cls, 
    jobject label, jstring str) 
{
	GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);
	gtk_label_set_label (label_g, str_g);
	(*env)->ReleaseStringUTFChars(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_label
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1label (JNIEnv *env, jclass 
    cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	gchar *result_g = (gchar*)gtk_label_get_label (label_g);
	return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1markup (JNIEnv *env, jclass cls, 
    jobject label, jstring str) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);
	gtk_label_set_markup (label_g, str_g);
	(*env)->ReleaseStringUTFChars(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_use_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1use_1markup (JNIEnv *env, jclass 
    cls, jobject label, jboolean setting) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gboolean setting_g = (gboolean) setting;
    gtk_label_set_use_markup (label_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_use_markup
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1use_1markup (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jboolean) (gtk_label_get_use_markup (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_use_underline
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1use_1underline (JNIEnv *env, 
    jclass cls, jobject label, jboolean setting) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gboolean setting_g = (gboolean) setting;
    gtk_label_set_use_underline (label_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_use_underline
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1use_1underline (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jboolean) (gtk_label_get_use_underline (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_markup_with_mnemonic
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1markup_1with_1mnemonic (JNIEnv 
    *env, jclass cls, jobject label, jstring str) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);	
	gtk_label_set_markup_with_mnemonic (label_g, str_g);
	(*env)->ReleaseStringUTFChars(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_mnemonic_keyval
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1mnemonic_1keyval (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jint) (gtk_label_get_mnemonic_keyval (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_mnemonic_widget
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1mnemonic_1widget (JNIEnv *env, 
    jclass cls, jobject label, jobject widget) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_label_set_mnemonic_widget (label_g, widget_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_mnemonic_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1mnemonic_1widget (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return getGObjectHandle(env, (GObject *) gtk_label_get_mnemonic_widget (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_text_with_mnemonic
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1text_1with_1mnemonic (JNIEnv 
    *env, jclass cls, jobject label, jstring str) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *str_g = (*env)->GetStringUTFChars(env, str, NULL);
	gtk_label_set_text_with_mnemonic (label_g, str_g);
	(*env)->ReleaseStringUTFChars(env, str, str_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_justify
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1justify (JNIEnv *env, jclass 
    cls, jobject label, jint jtype) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    GtkJustification jtype_g = (GtkJustification) jtype;
    gtk_label_set_justify (label_g, jtype_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_justify
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1justify (JNIEnv *env, jclass 
    cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jint) (gtk_label_get_justify (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_pattern
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1pattern (JNIEnv *env, jclass 
    cls, jobject label, jstring pattern) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
	const char *pattern_g = (*env)->GetStringUTFChars(env, pattern, NULL);
	gtk_label_set_pattern (label_g, pattern_g);
	(*env)->ReleaseStringUTFChars(env, pattern, pattern_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_line_wrap
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1line_1wrap (JNIEnv *env, jclass 
    cls, jobject label, jboolean wrap) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gboolean wrap_g = (gboolean) wrap;
    gtk_label_set_line_wrap (label_g, wrap_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_line_wrap
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1line_1wrap (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jboolean) (gtk_label_get_line_wrap (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_set_selectable
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1selectable (JNIEnv *env, jclass 
    cls, jobject label, jboolean setting) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gboolean setting_g = (gboolean) setting;
    gtk_label_set_selectable (label_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_selectable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1selectable (JNIEnv *env, 
    jclass cls, jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jboolean) (gtk_label_get_selectable (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_select_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1select_1region (JNIEnv *env, jclass 
    cls, jobject label, jint startOffset, jint endOffset) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gint32 startOffset_g = (gint32) startOffset;
    gint32 endOffset_g = (gint32) endOffset;
    gtk_label_select_region (label_g, startOffset_g, endOffset_g);
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_selection_bounds
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1selection_1bounds (JNIEnv 
    *env, jclass cls, jobject label, jintArray start, jintArray end) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gint *start_g = (gint *) (*env)->GetIntArrayElements (env, start, NULL);
    gint *end_g = (gint *) (*env)->GetIntArrayElements (env, end, NULL);
    jboolean result_j = (jboolean) (gtk_label_get_selection_bounds (label_g, start_g, end_g));
    (*env)->ReleaseIntArrayElements (env, start, (jint *) start_g, 0);
    (*env)->ReleaseIntArrayElements (env, end, (jint *) end_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_layout
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1layout (JNIEnv *env, jclass cls, 
    jobject label) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return getGObjectHandleAndRef(env, (GObject *) gtk_label_get_layout (label_g));
}

/*
 * Class:     org.gnu.gtk.Label
 * Method:    gtk_label_get_layout_offsets
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1layout_1offsets (JNIEnv *env, 
    jclass cls, jobject label, jintArray x, jintArray y) 
{
    GtkLabel *label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gtk_label_get_layout_offsets (label_g, x_g, y_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1width_1chars
(JNIEnv *env, jclass cls, jobject label, jint n_chars)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gint n_chars_g = (gint)n_chars;
    gtk_label_set_width_chars(label_g, n_chars_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1width_1chars
(JNIEnv *env, jclass cls, jobject label)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jint)gtk_label_get_width_chars(label_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1max_1width_1chars
(JNIEnv *env, jclass cls, jobject label, jint n_chars)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gint n_chars_g = (gint)n_chars;
    gtk_label_set_max_width_chars(label_g, n_chars_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1max_1width_1chars
(JNIEnv *env, jclass cls, jobject label)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jint)gtk_label_get_max_width_chars(label_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1angle
(JNIEnv *env, jclass cls, jobject label, jdouble angle)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gdouble angle_g = (gdouble)angle;
    gtk_label_set_angle(label_g, angle_g);
}

JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1angle
(JNIEnv *env, jclass cls, jobject label)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jdouble)gtk_label_get_angle(label_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1single_1line_1mode
(JNIEnv *env, jclass cls, jobject label, jboolean single_line_mode)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    gboolean single_line_mode_g = (gboolean)single_line_mode;
    gtk_label_set_single_line_mode(label_g, single_line_mode_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1single_1line_1mode
(JNIEnv *env, jclass cls, jobject label)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jboolean)gtk_label_get_single_line_mode(label_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Label_gtk_1label_1set_1ellipsize
(JNIEnv *env, jclass cls, jobject label, jint mode)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    PangoEllipsizeMode mode_g = (PangoEllipsizeMode)mode;
    gtk_label_set_ellipsize(label_g, mode_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Label_gtk_1label_1get_1ellipsize
(JNIEnv *env, jclass cls, jobject label)
{
    GtkLabel * label_g = (GtkLabel *)getPointerFromHandle(env, label);
    return (jint)gtk_label_get_ellipsize(label_g);
}

#ifdef __cplusplus
}

#endif
