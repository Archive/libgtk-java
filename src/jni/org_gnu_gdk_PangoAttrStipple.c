/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_PangoAttrStipple.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.PangoAttrStipple
 * Method:    getStipple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PangoAttrStipple_getStipple (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkPangoAttrStipple *obj_g = (GdkPangoAttrStipple *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, G_OBJECT(obj_g->stipple));
}

/*
 * Class:     org.gnu.gdk.PangoAttrStipple
 * Method:    gdk_pango_attr_stipple_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_PangoAttrStipple_gdk_1pango_1attr_1stipple_1new (JNIEnv 
    *env, jclass cls, jobject stipple) 
{
    GdkBitmap *stipple_g = (GdkBitmap *)getPointerFromHandle(env, stipple);
    return getStructHandle(env, gdk_pango_attr_stipple_new (stipple_g), 
                           NULL, (JGFreeFunc) pango_attribute_destroy);
}


#ifdef __cplusplus
}

#endif
