/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Accessible.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkWidget * GtkAccessible_get_widget (GtkAccessible * cptr) 
{
    return cptr->widget;
}

/*
 * Class:     org.gnu.gtk.Accessible
 * Method:    getWidget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Accessible_getWidget (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAccessible *cptr_g = (GtkAccessible *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkAccessible_get_widget (cptr_g));
}

/*
 * Class:     org.gnu.gtk.Accessible
 * Method:    gtk_accessible_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Accessible_gtk_1accessible_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_accessible_get_type ();
}


#ifdef __cplusplus
}

#endif
