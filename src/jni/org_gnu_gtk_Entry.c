/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Entry.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_entry_get_type ();
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_entry_new ());
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_visibility
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1visibility (JNIEnv *env, jclass 
    cls, jobject entry, jboolean visible) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gboolean visible_g = (gboolean) visible;
    gtk_entry_set_visibility (entry_g, visible_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_visibility
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1visibility (JNIEnv *env, 
    jclass cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jboolean) (gtk_entry_get_visibility (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_invisible_char
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1invisible_1char (JNIEnv *env, 
    jclass cls, jobject entry, jbyte ch) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gchar ch_g = (gchar) ch;
    gtk_entry_set_invisible_char (entry_g, ch_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_invisible_char
 */
JNIEXPORT jbyte JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1invisible_1char (JNIEnv *env, 
    jclass cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jbyte) (gtk_entry_get_invisible_char (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_has_frame
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1has_1frame (JNIEnv *env, jclass 
    cls, jobject entry, jboolean setting) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gboolean setting_g = (gboolean) setting;
    gtk_entry_set_has_frame (entry_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_has_frame
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1has_1frame (JNIEnv *env, 
    jclass cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jboolean) (gtk_entry_get_has_frame (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_max_length
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1max_1length (JNIEnv *env, jclass 
    cls, jobject entry, jint max) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gint32 max_g = (gint32) max;
    gtk_entry_set_max_length (entry_g, max_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_max_length
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1max_1length (JNIEnv *env, jclass 
    cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jint) (gtk_entry_get_max_length (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_activates_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1activates_1default (JNIEnv *env, 
    jclass cls, jobject entry, jboolean setting) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gboolean setting_g = (gboolean) setting;
    gtk_entry_set_activates_default (entry_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_activates_default
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1activates_1default (JNIEnv 
    *env, jclass cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jboolean) (gtk_entry_get_activates_default (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_width_chars
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1width_1chars (JNIEnv *env, 
    jclass cls, jobject entry, jint numChars) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gint32 numChars_g = (gint32) numChars;
    gtk_entry_set_width_chars (entry_g, numChars_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_width_chars
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1width_1chars (JNIEnv *env, 
    jclass cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jint) (gtk_entry_get_width_chars (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1text (JNIEnv *env, jclass cls, 
    jobject entry, jstring text) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
	gtk_entry_set_text (entry_g, text_g);
	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1text (JNIEnv *env, jclass 
    cls, jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
	return (*env)->NewStringUTF(env, (char *)gtk_entry_get_text(entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_layout
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1layout (JNIEnv *env, jclass cls, 
    jobject entry) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return getGObjectHandleAndRef(env, (GObject *) gtk_entry_get_layout (entry_g));
}

/*
 * Class:     org.gnu.gtk.Entry
 * Method:    gtk_entry_get_layout_offsets
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1layout_1offsets (JNIEnv *env, 
    jclass cls, jobject entry, jintArray x, jintArray y) 
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gtk_entry_get_layout_offsets (entry_g, x_g, y_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
}


/*
 * Class:     org_gnu_gtk_Entry
 * Method:    gtk_entry_set_completion
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1completion
  (JNIEnv *env, jclass cls, jobject entry, jobject completion)
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    GtkEntryCompletion *completion_g = (GtkEntryCompletion *)getPointerFromHandle(env, completion);
	gtk_entry_set_completion(entry_g, completion_g);
}
                                                                                 /*
 * Class:     org_gnu_gtk_Entry
 * Method:    gtk_entry_get_completion
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1completion
  (JNIEnv *env, jclass cls, jobject entry)
{
    GtkEntry *entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
	return getGObjectHandle(env, (GObject *) gtk_entry_get_completion(entry_g));
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1set_1alignment
(JNIEnv *env, jclass cls, jobject entry, jfloat xalign)
{
    GtkEntry * entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gfloat xalign_g = (gfloat)xalign;
    gtk_entry_set_alignment(entry_g, xalign_g);
}

JNIEXPORT jfloat JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1get_1alignment
(JNIEnv *env, jclass cls, jobject entry)
{
    GtkEntry * entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    return (jfloat)gtk_entry_get_alignment(entry_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1layout_1index_1to_1text_1index
(JNIEnv *env, jclass cls, jobject entry, jint layout_index)
{
    GtkEntry * entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gint layout_index_g = (gint)layout_index;
    return (jint)gtk_entry_layout_index_to_text_index(entry_g, layout_index_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Entry_gtk_1entry_1text_1index_1to_1layout_1index
(JNIEnv *env, jclass cls, jobject entry, jint text_index)
{
    GtkEntry * entry_g = (GtkEntry *)getPointerFromHandle(env, entry);
    gint text_index_g = (gint)text_index;
    return (jint)gtk_entry_text_index_to_layout_index(entry_g, text_index_g);
}


#ifdef __cplusplus
}

#endif
