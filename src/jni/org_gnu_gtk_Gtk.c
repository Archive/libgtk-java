/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib/gthread.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"
#include <string.h>
#include <stdlib.h>

#include "org_gnu_gtk_Gtk.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Common initialization functoin for
 * gtk_init and gtk_init_check
 */
static void init_common(void)
{
    /*
     * Important: glib's threads need to be enabled in order for the
     * CustomEvents to work, but enabling gdk's threads introduces deadlocks
     * all over the place. For so long as we can maintain the primary activity
     * of java-gnome as single threaded GUI code, we should be alright without
     * it. If anyone wants to enable GDK threads, then please see an archive of
     * the java-gnome-hackers mailing list discussions circa 31 Aug 05 for
     * discussion of the problems we encountered and some test cases to observe
     * deadlock behaviour.
     */
    /* 
     * glib thread susbsytem may be already initialized, check for that.
     */
	if (!g_thread_supported())	{
	    g_thread_init(NULL);
	}
 /* gdk_threads_init(); */

    /*
     * Make sure queued calls don't get sent to GTK/GDK while we're shuting
     * down. This is a potential trouble spot for the reasons discussed above -
     * care will have to be taken about nesting should GTK threads be enabled.
     * With GDK threads disabled, it's actually a no-op.
     */
    atexit(gdk_threads_enter);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_init
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1init (JNIEnv *env, 
						   jclass cls, 
						   jintArray argc, 
						   jobjectArray args) 
{
    gint *argc_g;
    gchar **args_g_arr;
    gchar ***args_g;

    argc_g = (gint *) (*env)->GetIntArrayElements (env, argc, NULL);
    args_g_arr = getStringArray(env, args);
    args_g = &args_g_arr;

	init_common();
    gtk_init (argc_g, args_g);

    (*env)->ReleaseIntArrayElements (env, argc, (jint *) argc_g, 0);
    freeStringArray(env, args, args_g_arr);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_init_check
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1init_1check (JNIEnv *env, jclass cls, 
    jintArray argc, jobjectArray args) 
{
	gint *argc_g;
    gchar **args_g_arr;
    gchar ***args_g;
    jboolean result;

    argc_g = (gint *) (*env)->GetIntArrayElements (env, argc, NULL);
    args_g_arr = getStringArray(env, args);
    args_g = &args_g_arr;
    
    init_common();
    result = (jboolean) gtk_init_check (argc_g, args_g);
    
    (*env)->ReleaseIntArrayElements (env, argc, (jint *) argc_g, 0);
    freeStringArray(env, args, args_g_arr);
    
    return result;
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_disable_setlocale
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1disable_1setlocale (JNIEnv *env, jclass cls) 
{
    gtk_disable_setlocale ();
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_set_locale
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Gtk_gtk_1set_1locale (JNIEnv *env, jclass cls) 
{
	return (*env)->NewStringUTF(env, gtk_set_locale ());
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_get_default_language
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Gtk_gtk_1get_1default_1language
		(JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, gtk_get_default_language(), PANGO_TYPE_LANGUAGE,
    		NULL, NULL);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_events_pending
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1events_1pending (JNIEnv
		*env, jclass cls) 
{
    return (jboolean)gtk_events_pending ();
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_main_do_event
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1main_1do_1event (JNIEnv *env,
		jclass cls, jobject event) 
{
    GdkEvent *event_g;
    
    event_g = (GdkEvent *)getPointerFromHandle(env, event);

    gtk_main_do_event (event_g);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_main
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1main (JNIEnv *env, jclass cls) 
{
    gtk_main ();
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_main_level
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Gtk_gtk_1main_1level (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_main_level();
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_main_quit
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1main_1quit (JNIEnv *env, jclass cls) 
{
    gtk_main_quit ();
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_main_iteration
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1main_1iteration (JNIEnv *env, jclass cls) 
{
    return (jboolean) (gtk_main_iteration ());
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_grab_add
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1grab_1add (JNIEnv *env, jclass cls, jobject 
    widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_grab_add (widget_g);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_grab_get_current
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Gtk_gtk_1grab_1get_1current (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_grab_get_current ());
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_grab_remove
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1grab_1remove (JNIEnv *env, jclass cls, jobject 
    widget) 
{
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_grab_remove (widget_g);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_get_current_event
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Gtk_gtk_1get_1current_1event (JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, gtk_get_current_event(), GDK_TYPE_EVENT,
    		(GBoxedCopyFunc) gdk_event_copy, (GBoxedFreeFunc) gdk_event_free);
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_get_current_event_time
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Gtk_gtk_1get_1current_1event_1time (JNIEnv *env, jclass 
    cls) 
{
    return (jint) (gtk_get_current_event_time ());
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_get_event_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Gtk_gtk_1get_1event_1widget (JNIEnv *env, jclass cls, 
    jobject event) 
{
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    return getGObjectHandle(env, (GObject *) gtk_get_event_widget (event_g));
}

/*
 * Class:     org.gnu.gtk.Gtk
 * Method:    gtk_type_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Gtk_gtk_1type_1name (JNIEnv *env, jclass cls, 
    jobject type) 
{
    GType *type_g = (GType *)getPointerFromHandle(env, type);
	return (*env)->NewStringUTF(env,  (gchar*)gtk_type_name (GTK_OBJECT_TYPE(type_g)));
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1true
(JNIEnv *env, jclass cls)
{
    return (jboolean)gtk_true();
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1false
(JNIEnv *env, jclass cls)
{
    return (jboolean)gtk_false();
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Gtk_gtk_1main_1iteration_1do
(JNIEnv *env, jclass cls, jboolean blocking)
{
    gboolean blocking_g = (gboolean)blocking;
    return (jboolean)gtk_main_iteration_do(blocking_g);
}

static gint gtkKeySnoopFunc(GtkWidget *grab_widget, GdkEventKey *event,
                            gpointer data) {
    jobject grab_widget_handle;
    void *event_copy;
    jobject event_handle;
    JGFuncCallbackRef *ref = (JGFuncCallbackRef*) data;
    grab_widget_handle = getGObjectHandle(ref->env, (GObject *) grab_widget);
    event_copy = g_malloc(sizeof(GdkEventKey));
    memcpy(event_copy, event, sizeof(GdkEventKey));
    event_handle = getStructHandle(ref->env, event_copy, NULL, (JGFreeFunc) g_free);
    return (gint) (* ref->env)->CallStaticBooleanMethod(ref->env, ref->cls,
    		ref->methodID, grab_widget_handle, event_handle);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtk_Gtk_gtk_1key_1snooper_1install
(JNIEnv *env, jclass cls, jobject snooper, jstring callback)
{
    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->obj = NULL;
    ref->cls = cls;

    const char *funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetStaticMethodID(env, cls,
                                  funcname, 
                                  "(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Z" );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return -1;
    }
    (*env)->ReleaseStringUTFChars(env, callback, funcname);

    return (jint)gtk_key_snooper_install(gtkKeySnoopFunc, ref);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1key_1snooper_1remove
(JNIEnv *env, jclass cls, jint snooper_handler_id)
{
    guint snooper_handler_id_g = (guint)snooper_handler_id;
    gtk_key_snooper_remove(snooper_handler_id_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Gtk_gtk_1propagate_1event
(JNIEnv *env, jclass cls, jobject widget, jobject event)
{
    GtkWidget * widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkEvent * event_g = (GdkEvent *)getPointerFromHandle(env, event);
    gtk_propagate_event(widget_g, event_g);
}

#ifdef __cplusplus
}

#endif
