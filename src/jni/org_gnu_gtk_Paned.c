/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Paned.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_paned_get_type ();
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_add1
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1add1 (JNIEnv *env, jclass cls, jobject 
    paned, jobject child) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_paned_add1 (paned_g, child_g);
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_add2
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1add2 (JNIEnv *env, jclass cls, jobject 
    paned, jobject child) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_paned_add2 (paned_g, child_g);
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_pack1
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1pack1 (JNIEnv *env, jclass cls, jobject 
    paned, jobject child, jboolean resize, jboolean shrink) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean resize_g = (gboolean) resize;
    gboolean shrink_g = (gboolean) shrink;
    gtk_paned_pack1 (paned_g, child_g, resize_g, shrink_g);
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_pack2
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1pack2 (JNIEnv *env, jclass cls, jobject 
    paned, jobject child, jboolean resize, jboolean shirnk) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gboolean resize_g = (gboolean) resize;
    gboolean shirnk_g = (gboolean) shirnk;
    gtk_paned_pack2 (paned_g, child_g, resize_g, shirnk_g);
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_get_position
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1get_1position (JNIEnv *env, jclass 
    cls, jobject paned) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    return (jint) (gtk_paned_get_position (paned_g));
}

/*
 * Class:     org.gnu.gtk.Paned
 * Method:    gtk_paned_set_position
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1set_1position (JNIEnv *env, jclass 
    cls, jobject paned, jint position) 
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
    gint32 position_g = (gint32) position;
    gtk_paned_set_position (paned_g, position_g);
}

/*
 * Class:     org_gnu_gtk_Paned
 * Method:    gtk_paned_get_child1
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1get_1child1
  (JNIEnv *env, jclass cls, jobject paned)
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
	return getGObjectHandle(env, (GObject *) gtk_paned_get_child1(paned_g));
}
                                                                                   
/*
 * Class:     org_gnu_gtk_Paned
 * Method:    gtk_paned_get_child2
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Paned_gtk_1paned_1get_1child2
  (JNIEnv *env, jclass cls, jobject paned)
{
    GtkPaned *paned_g = (GtkPaned *)getPointerFromHandle(env, paned);
	return getGObjectHandle(env, (GObject *) gtk_paned_get_child2(paned_g));
}

#ifdef __cplusplus
}

#endif
