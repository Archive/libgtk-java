/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CheckButton.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.CheckButton
 * Method:    gtk_check_button_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CheckButton_gtk_1check_1button_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_check_button_get_type ();
}

/*
 * Class:     org.gnu.gtk.CheckButton
 * Method:    gtk_check_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CheckButton_gtk_1check_1button_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_check_button_new ());
}

/*
 * Class:     org.gnu.gtk.CheckButton
 * Method:    gtk_check_button_new_with_label
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CheckButton_gtk_1check_1button_1new_1with_1label (
    JNIEnv *env, jclass cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval = getGObjectHandle(env, (GObject *) gtk_check_button_new_with_label (label_g));
	(*env)->ReleaseStringUTFChars( env, label, label_g );
	return retval;
}

/*
 * Class:     org.gnu.gtk.CheckButton
 * Method:    gtk_check_button_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CheckButton_gtk_1check_1button_1new_1with_1mnemonic (
    JNIEnv *env, jclass cls, jstring label) 
{
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject retval = getGObjectHandle(env, (GObject *) gtk_check_button_new_with_mnemonic (label_g));
	(*env)->ReleaseStringUTFChars( env, label, label_g );
	return retval;
}


#ifdef __cplusplus
}

#endif
