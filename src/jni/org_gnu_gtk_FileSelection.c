/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_FileSelection.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkWidget * GtkFileSelection_get_dir_list (GtkFileSelection * cptr) 
{
    return cptr->dir_list;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getDirList
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getDirList (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_dir_list (cptr_g));
}

static GtkWidget * GtkFileSelection_get_file_list (GtkFileSelection * cptr) 
{
    return cptr->file_list;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getFileList
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getFileList (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_file_list (cptr_g));
}

static GtkWidget * GtkFileSelection_get_selection_entry (GtkFileSelection * cptr) 
{
    return cptr->selection_entry;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getSelectionEntry
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getSelectionEntry (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_selection_entry (cptr_g));
}

static GtkWidget * GtkFileSelection_get_selection_text (GtkFileSelection * cptr) 
{
    return cptr->selection_text;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getSelectionText
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getSelectionText (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_selection_text (cptr_g));
}

static GtkWidget * GtkFileSelection_get_main_vbox (GtkFileSelection * cptr) 
{
    return cptr->main_vbox;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getMainVbox
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getMainVbox (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_main_vbox (cptr_g));
}

static GtkButton * GtkFileSelection_get_ok_button (GtkFileSelection * cptr) 
{
    return (GtkButton*)cptr->ok_button;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getOkButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getOkButton (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_ok_button (cptr_g));
}

static GtkButton * GtkFileSelection_get_cancel_button (GtkFileSelection * cptr) 
{
    return (GtkButton*)cptr->cancel_button;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getCancelButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getCancelButton (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_cancel_button (cptr_g));
}

static GtkButton * GtkFileSelection_get_help_button (GtkFileSelection * cptr) 
{
    return (GtkButton*)cptr->help_button;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getHelpButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getHelpButton (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_help_button (cptr_g));
}

static GtkWidget * GtkFileSelection_get_history_pulldown (GtkFileSelection * cptr) 
{
    return cptr->history_pulldown;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getHistoryPulldown
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getHistoryPulldown (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_history_pulldown (cptr_g));
}

static GtkWidget * GtkFileSelection_get_history_menu (GtkFileSelection * cptr) 
{
    return cptr->history_menu;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getHistoryMenu
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getHistoryMenu (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_history_menu (cptr_g));
}

static GtkWidget * GtkFileSelection_get_button_area (GtkFileSelection * cptr) 
{
    return cptr->button_area;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    getButtonArea
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_getButtonArea (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkFileSelection *cptr_g = (GtkFileSelection *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFileSelection_get_button_area (cptr_g));
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_file_selection_get_type ();
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1new (JNIEnv *env, 
    jclass cls, jstring title) 
{
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    jobject result = getGObjectHandle(env, (GObject *) gtk_file_selection_new (title_g));
    (*env)->ReleaseStringUTFChars(env, title, title_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_set_filename
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1set_1filename (
    JNIEnv *env, jclass cls, jobject filesel, jstring filename) 
{
	GtkFileSelection* filesel_g = (GtkFileSelection*)getPointerFromHandle(env, filesel);
    gchar* filename_g = (gchar*)(*env)->GetStringUTFChars(env, filename, 0);
    gtk_file_selection_set_filename (filesel_g, filename_g);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_get_filename
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1get_1filename (
    JNIEnv *env, jclass cls, jobject filesel) 
{
	GtkFileSelection* filesel_g = (GtkFileSelection*)getPointerFromHandle(env, filesel);
    gchar *result_g = (gchar*)gtk_file_selection_get_filename (filesel_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_complete
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1complete (JNIEnv 
    *env, jclass cls, jobject filesel, jstring pattern) 
{
	GtkFileSelection* filesel_g = (GtkFileSelection*)getPointerFromHandle(env, filesel);
    gchar* pattern_g = (gchar*)(*env)->GetStringUTFChars(env, pattern, 0);
    gtk_file_selection_complete (filesel_g, pattern_g);
    (*env)->ReleaseStringUTFChars(env, pattern, pattern_g);
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_show_fileop_buttons
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1show_1fileop_1buttons (JNIEnv *env, jclass 
    cls, jobject filesel) 
{
	GtkFileSelection* filesel_g = (GtkFileSelection*)getPointerFromHandle(env, filesel);
    gtk_file_selection_show_fileop_buttons (filesel_g);
}

/*
 * Class:     org.gnu.gtk.FileSelection
 * Method:    gtk_file_selection_hide_fileop_buttons
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1hide_1fileop_1buttons (JNIEnv *env, jclass 
    cls, jobject filesel) 
{
	GtkFileSelection* filesel_g = (GtkFileSelection*)getPointerFromHandle(env, filesel);
    gtk_file_selection_hide_fileop_buttons (filesel_g);
}

JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1get_1selections
(JNIEnv *env, jclass cls, jobject filesel)
{
    GtkFileSelection * filesel_g = (GtkFileSelection *)getPointerFromHandle(env, filesel);
    return getJavaStringArray(env, (const gchar* const *)gtk_file_selection_get_selections(filesel_g));
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1set_1select_1multiple
(JNIEnv *env, jclass cls, jobject filesel, jboolean select_multiple)
{
    GtkFileSelection * filesel_g = (GtkFileSelection *)getPointerFromHandle(env, filesel);
    gboolean select_multiple_g = (gboolean)select_multiple;
    gtk_file_selection_set_select_multiple(filesel_g, select_multiple_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FileSelection_gtk_1file_1selection_1get_1select_1multiple
(JNIEnv *env, jclass cls, jobject filesel)
{
    GtkFileSelection * filesel_g = (GtkFileSelection *)getPointerFromHandle(env, filesel);
    return (jboolean)gtk_file_selection_get_select_multiple(filesel_g);
}


#ifdef __cplusplus
}

#endif
