/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Calendar.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_calendar_get_type ();
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_calendar_new ());
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_select_month
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1select_1month (JNIEnv *env, 
    jclass cls, jobject calendar, jint month, jint year) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    guint32 month_g = (guint32) month;
    guint32 year_g = (guint32) year;
    return (jint) (gtk_calendar_select_month (calendar_g, month_g, year_g));
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_select_day
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1select_1day (JNIEnv *env, 
    jclass cls, jobject calendar, jint day) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    guint32 day_g = (guint32) day;
    gtk_calendar_select_day (calendar_g, day_g);
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_mark_day
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1mark_1day (JNIEnv *env, jclass 
    cls, jobject calendar, jint day) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    guint32 day_g = (guint32) day;
    return (jint) (gtk_calendar_mark_day (calendar_g, day_g));
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_unmark_day
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1unmark_1day (JNIEnv *env, 
    jclass cls, jobject calendar, jint day) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    guint32 day_g = (guint32) day;
    return (jint) (gtk_calendar_unmark_day (calendar_g, day_g));
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_clear_marks
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1clear_1marks (JNIEnv *env, 
    jclass cls, jobject calendar) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    gtk_calendar_clear_marks (calendar_g);
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_get_date
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1get_1date (JNIEnv *env, jclass 
    cls, jobject calendar, jintArray triple) 
{
    jint *triple_c;
    triple_c = (*env)->GetIntArrayElements(env, triple, NULL);
    if (triple_c == NULL) {
	    return;
    }

    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    guint year_g, month_g, day_g;

    gtk_calendar_get_date (calendar_g, &year_g, &month_g, &day_g);

    triple_c[0] = year_g;
    triple_c[1] = month_g;
    triple_c[2] = day_g;

    (*env)->ReleaseIntArrayElements (env, triple, triple_c, 0);
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_freeze
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1freeze (JNIEnv *env, jclass 
    cls, jobject calendar) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    gtk_calendar_freeze (calendar_g);
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_thaw
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1thaw (JNIEnv *env, jclass cls, 
    jobject calendar) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    gtk_calendar_thaw (calendar_g);
}

/*
 * Class:     org.gnu.gtk.Calendar
 * Method:    gtk_calendar_set_display_options
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1set_1display_1options (
    JNIEnv *env, jclass cls, jobject calendar, jint flags) 
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
    GtkCalendarDisplayOptions flags_g = (GtkCalendarDisplayOptions) flags;
    gtk_calendar_set_display_options (calendar_g, flags_g);
}

/*
 * Class:     org_gnu_gtk_Calendar
 * Method:    gtk_calendar_get_display_options
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Calendar_gtk_1calendar_1get_1display_1options
  (JNIEnv *env, jclass cls, jobject calendar)
{
    GtkCalendar *calendar_g = (GtkCalendar *)getPointerFromHandle(env, calendar);
	return (jint)gtk_calendar_get_display_options(calendar_g);
}
                                                                                

#ifdef __cplusplus
}

#endif
