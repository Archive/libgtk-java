/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Keymap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_keymap_get_type ();
}

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_get_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1get_1default (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, G_OBJECT(gdk_keymap_get_default ()));
}

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_lookup_key
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1lookup_1key (JNIEnv *env, jclass 
    cls, jobject keymap, jobject key) 
{
    GdkKeymap *keymap_g = (GdkKeymap *)getPointerFromHandle(env, keymap);
    GdkKeymapKey *key_g = (GdkKeymapKey *)getPointerFromHandle(env, key);
    return (jint) (gdk_keymap_lookup_key (keymap_g, key_g));
}

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_get_entries_for_keyval
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1get_1entries_1for_1keyval (
    JNIEnv *env, jclass cls, jobject keymap, jint keyval, jobjectArray keys) 
{
    GdkKeymap *keymap_g = (GdkKeymap *)getPointerFromHandle(env, keymap);
    gint32 keyval_g = (gint32) keyval;
    GdkKeymapKey **keys_g = (GdkKeymapKey **)getPointerArrayFromHandles(env, keys);
    gint *numKeys_g = NULL;
    return (jboolean) (gdk_keymap_get_entries_for_keyval (keymap_g, keyval_g, 
                keys_g, numKeys_g));
}

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_get_entries_for_keycode
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1get_1entries_1for_1keycode (
    JNIEnv *env, jclass cls, jobject keymap, jint hardwareKeycode, jobjectArray keys, jintArray 
    keyvals) 
{
    GdkKeymap *keymap_g = (GdkKeymap *)getPointerFromHandle(env, keymap);
    gint32 hardwareKeycode_g = (gint32) hardwareKeycode;
    GdkKeymapKey **keys_g = (GdkKeymapKey **)getPointerArrayFromHandles(env, keys);
    guint **keyvals_g = (guint **) (*env)->GetIntArrayElements (env, keyvals, NULL);
    gint *numEntries_g = NULL;
    jboolean result_j = (jboolean) (gdk_keymap_get_entries_for_keycode (keymap_g, 
                hardwareKeycode_g, keys_g, keyvals_g, numEntries_g));
    (*env)->ReleaseIntArrayElements (env, keyvals, (jint *) keyvals_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Keymap
 * Method:    gdk_keymap_get_direction
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Keymap_gdk_1keymap_1get_1direction (JNIEnv *env, jclass 
    cls, jobject keymap) 
{
    GdkKeymap *keymap_g = (GdkKeymap *)getPointerFromHandle(env, keymap);
    return (jint) (gdk_keymap_get_direction (keymap_g));
}


#ifdef __cplusplus
}

#endif
