/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ScrolledWindow.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_scrolled_window_get_type ();
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_new
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1new (JNIEnv *env, jclass cls, jobject hadjustment, jobject vadjustment) 
{
    GtkAdjustment *hadjustment_g = 
        (GtkAdjustment *)getPointerFromHandle(env, hadjustment);
    GtkAdjustment *vadjustment_g = 
        (GtkAdjustment *)getPointerFromHandle(env, vadjustment);
    return getGObjectHandle(env, (GObject *) gtk_scrolled_window_new (hadjustment_g, vadjustment_g));
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_set_hadjustment
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1set_1hadjustment (JNIEnv *env, jclass cls, jobject scrolled_window, jobject hadjustment) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkAdjustment *hadjustment_g = 
        (GtkAdjustment *)getPointerFromHandle(env, hadjustment);
    gtk_scrolled_window_set_hadjustment (scrolled_window_g, hadjustment_g);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_set_vadjustment
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1set_1vadjustment (JNIEnv *env, jclass cls, jobject scrolled_window, jobject vadjustment) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkAdjustment *vadjustment_g = 
        (GtkAdjustment *)getPointerFromHandle(env, vadjustment);
    gtk_scrolled_window_set_vadjustment (scrolled_window_g, vadjustment_g);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_hadjustment
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1hadjustment (JNIEnv *env, jclass cls, jobject scrolled_window) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    return getGObjectHandle(env, (GObject *) gtk_scrolled_window_get_hadjustment (scrolled_window_g));
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_vadjustment
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1vadjustment (JNIEnv *env, jclass cls, jobject scrolled_window) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    return getGObjectHandle(env, (GObject *) gtk_scrolled_window_get_vadjustment (scrolled_window_g));
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_set_policy
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1set_1policy (JNIEnv *env, jclass cls, jobject scrolled_window, jint hscrollbarPolicy, jint vscrollbarPolicy)
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkPolicyType hscrollbarPolicy_g = (GtkPolicyType) hscrollbarPolicy;
    GtkPolicyType vscrollbarPolicy_g = (GtkPolicyType) vscrollbarPolicy;
    gtk_scrolled_window_set_policy (scrolled_window_g, hscrollbarPolicy_g, 
                                    vscrollbarPolicy_g);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_policy
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1policy (JNIEnv *env, jclass cls, jobject scrolled_window, jintArray hscrollbarPolicy, jintArray vscrollbarPolicy) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    gint *hscrollbarPolicy_g_g = 
        (gint *) (*env)->GetIntArrayElements (env, hscrollbarPolicy, NULL);
    GtkPolicyType *hscrollbarPolicy_g = (GtkPolicyType *)hscrollbarPolicy_g_g;
    gint *vscrollbarPolicy_g_g = 
        (gint *) (*env)->GetIntArrayElements (env, vscrollbarPolicy, NULL);
    GtkPolicyType *vscrollbarPolicy_g = (GtkPolicyType *)vscrollbarPolicy_g_g;

    gtk_scrolled_window_get_policy (scrolled_window_g, hscrollbarPolicy_g, 
                                    vscrollbarPolicy_g);
    (*env)->ReleaseIntArrayElements (env, hscrollbarPolicy, 
                                     (jint*)hscrollbarPolicy_g_g, 0);
    (*env)->ReleaseIntArrayElements (env, vscrollbarPolicy, 
                                     (jint*)vscrollbarPolicy_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_set_placement
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1set_1placement (JNIEnv *env, jclass cls, jobject scrolled_window, jint windowPlacement) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkCornerType windowPlacement_g = (GtkCornerType) windowPlacement;
    gtk_scrolled_window_set_placement (scrolled_window_g, windowPlacement_g);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_placement
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1placement (JNIEnv *env, jclass cls, jobject scrolled_window) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    return (jint) (gtk_scrolled_window_get_placement (scrolled_window_g));
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_set_shadow_type
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1set_1shadow_1type (JNIEnv *env, jclass cls, jobject scrolled_window, jint type) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkShadowType type_g = (GtkShadowType) type;
    gtk_scrolled_window_set_shadow_type (scrolled_window_g, type_g);
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_get_shadow_type
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1shadow_1type (JNIEnv *env, jclass cls, jobject scrolled_window) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    return (jint) (gtk_scrolled_window_get_shadow_type (scrolled_window_g));
}

/*
 * Class:     org.gnu.gtk.ScrolledWindow
 * Method:    gtk_scrolled_window_add_with_viewport
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1add_1with_1viewport (JNIEnv *env, jclass cls, jobject scrolled_window, jobject child) 
{
    GtkScrolledWindow *scrolled_window_g = 
        (GtkScrolledWindow *)getPointerFromHandle(env, scrolled_window);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gtk_scrolled_window_add_with_viewport (scrolled_window_g, child_g);
}

/*
 * Class:     org_gnu_gtk_ScrolledWindow
 * Method:    gtk_scrolled_window_get_hscrollbar
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1hscrollbar
  (JNIEnv *env, jclass cls, jobject sw)
{
	GtkScrolledWindow *sw_g;
	GtkWidget *sb;
	
    sw_g = (GtkScrolledWindow *)getPointerFromHandle(env, sw);

	sb = gtk_scrolled_window_get_hscrollbar(sw_g);
	
	return getGObjectHandle(env, (GObject *) sb);	
}

/*
 * Class:     org_gnu_gtk_ScrolledWindow
 * Method:    gtk_scrolled_window_get_vscrollbar
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ScrolledWindow_gtk_1scrolled_1window_1get_1vscrollbar
  (JNIEnv *env, jclass cls, jobject sw)
{
	GtkScrolledWindow *sw_g;
	GtkWidget *sb;
	
    sw_g = (GtkScrolledWindow *)getPointerFromHandle(env, sw);

	sb = gtk_scrolled_window_get_vscrollbar(sw_g);
	
	return getGObjectHandle(env, (GObject *) sb);	
}



#ifdef __cplusplus
}
#endif
