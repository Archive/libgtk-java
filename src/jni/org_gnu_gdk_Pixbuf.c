/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <string.h>
#include <gdk/gdk.h>
#include <cairo.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Pixbuf.h"
#ifdef __cplusplus
extern "C" 
{
#endif

///*
// * Class:     org.gnu.gdk.Pixbuf
// * Method:    gdk_pixbuf_render_threshold_alpha
// */
//JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1render_1threshold_1alpha (JNIEnv 
//    *env, jclass cls, jobject pixbuf, jobject bitmap, jint srcX, jint srcY, jint destX, jint destY, 
//    jint width, jint height, jint alphaThreshold) 
//{
//    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
//    GdkBitmap *bitmap_g = (GdkBitmap *)getPointerFromHandle(env, bitmap);
//    gint32 srcX_g = (gint32) srcX;
//    gint32 srcY_g = (gint32) srcY;
//    gint32 destX_g = (gint32) destX;
//    gint32 destY_g = (gint32) destY;
//    gint32 width_g = (gint32) width;
//    gint32 height_g = (gint32) height;
//    gint32 alphaThreshold_g = (gint32) alphaThreshold;
//    gdk_pixbuf_render_threshold_alpha (pixbuf_g, bitmap_g, srcX_g, srcY_g, destX_g, 
//            destY_g, width_g, height_g, alphaThreshold_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixbuf
// * Method:    gdk_pixbuf_render_to_drawable
// */
//JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1render_1to_1drawable (JNIEnv *env, 
//    jclass cls, jobject pixbuf, jobject drawable, jobject gc, jint srcX, jint srcY, jint destX, jint 
//    destY, jint width, jint height, jint dither, jint xDither, jint yDither) 
//{
//    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
//    GdkDrawable *drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
//    GdkGC *gc_g = (GdkGC *)getPointerFromHandle(env, gc);
//    gint32 srcX_g = (gint32) srcX;
//    gint32 srcY_g = (gint32) srcY;
//    gint32 destX_g = (gint32) destX;
//    gint32 destY_g = (gint32) destY;
//    gint32 width_g = (gint32) width;
//    gint32 height_g = (gint32) height;
//    GdkRgbDither dither_g = (GdkRgbDither) dither;
//    gint32 xDither_g = (gint32) xDither;
//    gint32 yDither_g = (gint32) yDither;
//    gdk_pixbuf_render_to_drawable (pixbuf_g, drawable_g, gc_g, srcX_g, srcY_g, destX_g, 
//            destY_g, width_g, height_g, dither_g, xDither_g, yDither_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixbuf
// * Method:    gdk_pixbuf_render_to_drawable_alpha
// */
//JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1render_1to_1drawable_1alpha (JNIEnv 
//    *env, jclass cls, jobject pixbuf, jobject drawable, jint srcX, jint srcY, jint destX, jint destY, 
//    jint width, jint height, jint alphaMode, jint alphaThreshold, jint dither, jint xDither, 
//    jint yDither) 
//{
//    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
//    GdkDrawable *drawable_g = (GdkDrawable *)getPointerFromHandle(env, drawable);
//    gint32 srcX_g = (gint32) srcX;
//    gint32 srcY_g = (gint32) srcY;
//    gint32 destX_g = (gint32) destX;
//    gint32 destY_g = (gint32) destY;
//    gint32 width_g = (gint32) width;
//    gint32 height_g = (gint32) height;
//    GdkPixbufAlphaMode alphaMode_g = (GdkPixbufAlphaMode) alphaMode;
//    gint32 alphaThreshold_g = (gint32) alphaThreshold;
//    GdkRgbDither dither_g = (GdkRgbDither) dither;
//    gint32 xDither_g = (gint32) xDither;
//    gint32 yDither_g = (gint32) yDither;
//    gdk_pixbuf_render_to_drawable_alpha (pixbuf_g, drawable_g, srcX_g, srcY_g, destX_g, 
//            destY_g, width_g, height_g, alphaMode_g, alphaThreshold_g, dither_g, xDither_g, 
//            yDither_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixbuf
// * Method:    gdk_pixbuf_render_pixmap_and_mask_for_colormap
// */
//JNIEXPORT void JNICALL 
//Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1render_1pixmap_1and_1mask_1for_1colormap (JNIEnv *env, 
//    jclass cls, jobject pixbuf, jobject colormap, jobject pixmapReturn, jobject maskReturn, jint 
//    alphaThreshold) 
//{
//    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
//    GdkColormap *colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
//    GdkPixmap **pixmapReturn_g = (GdkPixmap **)getPointerFromHandle(env, pixmapReturn);
//    GdkBitmap **maskReturn_g = (GdkBitmap **)getPointerFromHandle(env, maskReturn);
//    gint32 alphaThreshold_g = (gint32) alphaThreshold;
//    gdk_pixbuf_render_pixmap_and_mask_for_colormap (pixbuf_g, colormap_g, pixmapReturn_g, 
//            maskReturn_g, alphaThreshold_g);
//}
//
///*
// * Class:     org.gnu.gdk.Pixbuf
// * Method:    gdk_pixbuf_render_pixmap_and_mask
// */
//JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1render_1pixmap_1and_1mask (JNIEnv 
//    *env, jclass cls, jobject pixbuf, jobject pixmapReturn, jobject maskReturn, jint alphaThreshold) 
//{
//    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
//    GdkPixmap **pixmapReturn_g = (GdkPixmap **)getPointerFromHandle(env, pixmapReturn);
//    GdkBitmap **maskReturn_g = (GdkBitmap **)getPointerFromHandle(env, maskReturn);
//    gint32 alphaThreshold_g = (gint32) alphaThreshold;
//    gdk_pixbuf_render_pixmap_and_mask (pixbuf_g, pixmapReturn_g, maskReturn_g, 
//            alphaThreshold_g);
//}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_from_drawable
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1from_1drawable (JNIEnv *env, 
    jclass cls, jobject src, jobject cmap, jint srcX, jint srcY, jint destX, jint destY, 
    jint width, jint height) 
{
    GdkDrawable *src_g = (GdkDrawable *)getPointerFromHandle(env, src);
    GdkColormap *cmap_g = (GdkColormap *)getPointerFromHandle(env, cmap);
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_get_from_drawable (NULL,
    		src_g, cmap_g, srcX, srcY, destX, destY, width, height));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_from_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1from_1image (JNIEnv *env, 
    jclass cls, jobject src, jobject cmap, jint srcX, jint srcY, jint destX, jint destY, 
    jint width, jint height) 
{
    GdkImage *src_g = (GdkImage *)getPointerFromHandle(env, src);
    GdkColormap *cmap_g = (GdkColormap *)getPointerFromHandle(env, cmap);
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_get_from_image (NULL,
    		src_g, cmap_g, srcX, srcY, destX, destY, width, height));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_colorspace
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1colorspace (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_colorspace (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_n_channels
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1n_1channels (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_n_channels (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_has_alpha
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1has_1alpha (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jboolean) (gdk_pixbuf_get_has_alpha (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_bits_per_sample
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1bits_1per_1sample (JNIEnv 
    *env, jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_bits_per_sample (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_pixels
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1pixels (JNIEnv *env, 
    jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    guchar *result_g = gdk_pixbuf_get_pixels (pixbuf_g);
    jsize result_len = result_g ? 
	    gdk_pixbuf_get_rowstride(pixbuf_g)*(gdk_pixbuf_get_height(pixbuf_g) - 1) +
	    gdk_pixbuf_get_width(pixbuf_g)*gdk_pixbuf_get_n_channels(pixbuf_g) 
	    : 0;
    
    jbyteArray result_j = result_g ? (*env)->NewByteArray (env, result_len) : NULL;
    if (result_g) 
    	(*env)->SetByteArrayRegion (env, result_j, 0, result_len, (jbyte*)result_g);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1width (JNIEnv *env, jclass 
    cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_width (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_height
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1height (JNIEnv *env, jclass 
    cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_height (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_rowstride
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1rowstride (JNIEnv *env, jclass 
    cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return (jint) (gdk_pixbuf_get_rowstride (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new (JNIEnv *env, jclass cls, jint 
    colorspace, jboolean hasAlpha, jint bitsPerSample, jint width, jint height) 
{
    GdkColorspace colorspace_g = (GdkColorspace) colorspace;
    gboolean hasAlpha_g = (gboolean) hasAlpha;
    gint32 bitsPerSample_g = (gint32) bitsPerSample;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_new (colorspace_g,
    		hasAlpha_g, bitsPerSample_g, width_g, height_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1copy (JNIEnv *env, jclass cls, jobject pixbuf) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_copy (pixbuf_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_new_subpixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1subpixbuf (JNIEnv *env, jclass 
    cls, jobject srcPixbuf, jint srcX, jint srcY, jint width, jint height) 
{
    GdkPixbuf *srcPixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, srcPixbuf);
    gint32 srcX_g = (gint32) srcX;
    gint32 srcY_g = (gint32) srcY;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    return getGObjectHandle(env, (GObject *)
    		gdk_pixbuf_new_subpixbuf (srcPixbuf_g, srcX_g, srcY_g, width_g, height_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_new_from_file
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1from_1file (JNIEnv *env, 
    jclass cls, jstring filename, jobject error) 
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, 0);
    GError* error_g = NULL;
    jobject result =  getGObjectHandle(env, (GObject *)
    		gdk_pixbuf_new_from_file (filename_g, &error_g));
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (NULL != error_g){
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    }
    return result;
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_new_from_file_at_size
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1from_1file_1at_1size
  (JNIEnv *env, jclass cls, jstring filename, jint width, jint height, jobject error)
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, 0);
    GError* error_g = NULL;
    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file_at_size (filename_g, width,
    		height, &error_g);
    jobject result =  getGObjectHandle(env, (GObject *) pixbuf);
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (NULL != error_g)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    return result;
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_new_from_file_at_scale
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1from_1file_1at_1scale
  (JNIEnv *env, jclass cls, jstring filename, jint width, jint height, jboolean aspect, jobject error)
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, 0);
    GError* error_g = NULL;
    jobject result =  getGObjectHandle(env, (GObject *)
    		gdk_pixbuf_new_from_file_at_scale (filename_g, width, height, aspect,
    		&error_g));
    (*env)->ReleaseStringUTFChars(env, filename, filename_g);
    if (NULL != error_g)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    return result;
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_get_file_info
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1file_1info
  (JNIEnv *env, jclass cls, jstring filename, jintArray width, jintArray height)
{
    const gchar* filename_g = (*env)->GetStringUTFChars(env, filename, 0);
    gint* w = (gint*)(*env)->GetIntArrayElements(env, width, NULL);
    gint* h = (gint*)(*env)->GetIntArrayElements(env, height, NULL);
    // GdkPixbufFormat is owned by the GdkPixbuf.
    jobject handle = getStructHandle(env, gdk_pixbuf_get_file_info(filename_g, w, h), NULL, NULL);
    (*env)->ReleaseIntArrayElements(env, width, (jint*)w, 0);
    (*env)->ReleaseIntArrayElements(env, height, (jint*)h, 0);
    return handle;
}



/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_new_from_xpm_data
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1from_1xpm_1data (JNIEnv *env, 
    jclass cls, jbyteArray data) 
{
    jint data_len = (*env)->GetArrayLength(env, data);
    const gchar** data_g = (const gchar**)g_malloc(data_len + 1);
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_new_from_xpm_data (data_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_new_from_inline
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1new_1from_1inline (JNIEnv *env, 
    jclass cls, jint dataLength, jbyteArray data, jboolean copyPixels, jobject error) 
{
    gint32 dataLength_g = (gint32) dataLength;
    jint data_len = (*env)->GetArrayLength(env, data);
    guchar* data_g = (guchar*)g_malloc(data_len + 1);
    gboolean copyPixels_g = (gboolean) copyPixels;
    GError* error_g = NULL;
    (*env)->GetByteArrayRegion(env, data, 0, data_len, (jbyte*)data_g);
    data_g[data_len] = 0;
    jobject handle = getGObjectHandle(env, (GObject *)
    	gdk_pixbuf_new_from_inline (dataLength_g, data_g, copyPixels_g, &error_g));
    if (NULL != error_g)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    return handle;
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_savev
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1savev
  (JNIEnv *env, jclass cls, jobject pixbuf, jstring filename, jstring type,
  		jobjectArray keys, jobjectArray values, jobject error)
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    const gchar* f = (*env)->GetStringUTFChars(env, filename, NULL);
    const gchar* t = (*env)->GetStringUTFChars(env, type, NULL);
    gchar** k = getStringArray(env, keys);
    gchar** v = getStringArray(env, values);
    GError* error_g = NULL;
	jboolean val = gdk_pixbuf_savev(pixbuf_g, f, t, k, v, &error_g);
    if (NULL != error_g)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
	(*env)->ReleaseStringUTFChars(env, filename, f);
	(*env)->ReleaseStringUTFChars(env, type, t);
	freeStringArray(env, keys, k);
	freeStringArray(env, values, v);
	return val;	
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_save_to_bufferv
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1save_1to_1bufferv
  (JNIEnv *env, jclass cls, jobject pixbuf, jstring type,
  		jobjectArray keys, jobjectArray values, jobject error)
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gchar* b = NULL;
    const gchar* t = (*env)->GetStringUTFChars(env, type, NULL);
    gchar** k = getStringArray(env, keys);
    gchar** v = getStringArray(env, values);
    GError* error_g = NULL;
    gsize size;
    jbyteArray ret_val;
    ret_val = NULL;
    gdk_pixbuf_save_to_bufferv(pixbuf_g, &b, &size, t, k, v, &error_g);
    if (error_g != NULL)
    	updateStructHandle(env, error, error_g, (JGFreeFunc) g_error_free);
    else	{
    	ret_val = (*env)->NewByteArray(env, (jsize) size);
    	(*env)->SetByteArrayRegion(env, ret_val, 0, (jsize) size, (jbyte *) b); 
    	
    }
	(*env)->ReleaseStringUTFChars(env, type, t);
	freeStringArray(env, keys, k);
	freeStringArray(env, values, v);
	
	return ret_val;
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_rotate_simple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1rotate_1simple
  (JNIEnv *env, jclass cls, jobject pixbuf, jint direction)
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_rotate_simple(pixbuf_g,
    		(GdkPixbufRotation) direction));
}

/*
 * Class:     org_gnu_gdk_Pixbuf
 * Method:    gdk_pixbuf_flip
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1flip
  (JNIEnv *env, jclass cls, jobject pixbuf, jboolean horizontal)
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    return getGObjectHandle(env, (GObject *) gdk_pixbuf_flip(pixbuf_g,
    		(gboolean)horizontal));
}  

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_fill
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1fill (JNIEnv *env, jclass cls, jobject 
    pixbuf, jint pixel) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gint32 pixel_g = (gint32) pixel;
    gdk_pixbuf_fill (pixbuf_g, pixel_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_add_alpha
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1add_1alpha (JNIEnv *env, jclass 
    cls, jobject pixbuf, jboolean substituteColor, jint r, jint g, jint b) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    gboolean substituteColor_g = (gboolean) substituteColor;
    gint32 r_g = (gint32) r;
    gint32 g_g = (gint32) g;
    gint32 b_g = (gint32) b;
    // returned pixbuf has ref count of 1.  should we use a different getGObjectHandle function here?
    return getGObjectHandle(env, (GObject *)
    		gdk_pixbuf_add_alpha (pixbuf_g, substituteColor_g, r_g, g_g, b_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_copy_area
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1copy_1area (JNIEnv *env, jclass 
    cls, jobject srcPixbuf, jint srcX, jint srcY, jint width, jint height, jobject destPixbuf, jint 
    destX, jint destY) 
{
    GdkPixbuf *srcPixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, srcPixbuf);
    gint32 srcX_g = (gint32) srcX;
    gint32 srcY_g = (gint32) srcY;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    GdkPixbuf *destPixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, destPixbuf);
    gint32 destX_g = (gint32) destX;
    gint32 destY_g = (gint32) destY;
    gdk_pixbuf_copy_area (srcPixbuf_g, srcX_g, srcY_g, width_g, height_g, destPixbuf_g, 
            destX_g, destY_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_saturate_and_pixelate
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1saturate_1and_1pixelate (JNIEnv 
    *env, jclass cls, jobject src, jobject dest, jdouble saturation, jboolean pixelate) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    GdkPixbuf *dest_g = (GdkPixbuf *)getPointerFromHandle(env, dest);
    gdouble saturation_g = (gdouble) saturation;
    gboolean pixelate_g = (gboolean) pixelate;
    gdk_pixbuf_saturate_and_pixelate (src_g, dest_g, saturation_g, pixelate_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_scale
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1scale (JNIEnv *env, jclass cls, 
    jobject src, jobject dest, jint destX, jint destY, jint destWidth, jint destHeight, jdouble 
    offsetX, jdouble offsetY, jdouble scaleX, jdouble scaleY, jint interpType) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    GdkPixbuf *dest_g = (GdkPixbuf *)getPointerFromHandle(env, dest);
    gint32 destX_g = (gint32) destX;
    gint32 destY_g = (gint32) destY;
    gint32 destWidth_g = (gint32) destWidth;
    gint32 destHeight_g = (gint32) destHeight;
    gdouble offsetX_g = (gdouble) offsetX;
    gdouble offsetY_g = (gdouble) offsetY;
    gdouble scaleX_g = (gdouble) scaleX;
    gdouble scaleY_g = (gdouble) scaleY;
    GdkInterpType interpType_g = (GdkInterpType) interpType;
    gdk_pixbuf_scale (src_g, dest_g, destX_g, destY_g, destWidth_g, destHeight_g, 
            offsetX_g, offsetY_g, scaleX_g, scaleY_g, interpType_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_composite
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1composite (JNIEnv *env, jclass cls, 
    jobject src, jobject dest, jint destX, jint destY, jint destWidth, jint destHeight, jdouble 
    offsetX, jdouble offsetY, jdouble scaleX, jdouble scaleY, jint interpType, jint 
    overallAlpha) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    GdkPixbuf *dest_g = (GdkPixbuf *)getPointerFromHandle(env, dest);
    gint32 destX_g = (gint32) destX;
    gint32 destY_g = (gint32) destY;
    gint32 destWidth_g = (gint32) destWidth;
    gint32 destHeight_g = (gint32) destHeight;
    gdouble offsetX_g = (gdouble) offsetX;
    gdouble offsetY_g = (gdouble) offsetY;
    gdouble scaleX_g = (gdouble) scaleX;
    gdouble scaleY_g = (gdouble) scaleY;
    GdkInterpType interpType_g = (GdkInterpType) interpType;
    gint32 overallAlpha_g = (gint32) overallAlpha;
    gdk_pixbuf_composite (src_g, dest_g, destX_g, destY_g, destWidth_g, destHeight_g, 
            offsetX_g, offsetY_g, scaleX_g, scaleY_g, interpType_g, overallAlpha_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_composite_color
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1composite_1color (JNIEnv *env, 
    jclass cls, jobject src, jobject dest, jint destX, jint destY, jint destWidth, jint destHeight, 
    jdouble offsetX, jdouble offsetY, jdouble scaleX, jdouble scaleY, jint interpType, jint 
    overallAlpha, jint checkX, jint checkY, jint checkSize, jint color1, jint color2) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    GdkPixbuf *dest_g = (GdkPixbuf *)getPointerFromHandle(env, dest);
    gint32 destX_g = (gint32) destX;
    gint32 destY_g = (gint32) destY;
    gint32 destWidth_g = (gint32) destWidth;
    gint32 destHeight_g = (gint32) destHeight;
    gdouble offsetX_g = (gdouble) offsetX;
    gdouble offsetY_g = (gdouble) offsetY;
    gdouble scaleX_g = (gdouble) scaleX;
    gdouble scaleY_g = (gdouble) scaleY;
    GdkInterpType interpType_g = (GdkInterpType) interpType;
    gint32 overallAlpha_g = (gint32) overallAlpha;
    gint32 checkX_g = (gint32) checkX;
    gint32 checkY_g = (gint32) checkY;
    gint32 checkSize_g = (gint32) checkSize;
    gint32 color1_g = (gint32) color1;
    gint32 color2_g = (gint32) color2;
    gdk_pixbuf_composite_color (src_g, dest_g, destX_g, destY_g, destWidth_g, destHeight_g, 
            offsetX_g, offsetY_g, scaleX_g, scaleY_g, interpType_g, overallAlpha_g, checkX_g, 
            checkY_g, checkSize_g, color1_g, color2_g);
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_scale_simple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1scale_1simple (JNIEnv *env, jclass 
    cls, jobject src, jint destWidth, jint destHeight, jint interpType) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    gint32 destWidth_g = (gint32) destWidth;
    gint32 destHeight_g = (gint32) destHeight;
    GdkInterpType interpType_g = (GdkInterpType) interpType;
    return getGObjectHandle(env, (GObject *)
    		gdk_pixbuf_scale_simple (src_g, destWidth_g, destHeight_g, interpType_g));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_composite_color_simple
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1composite_1color_1simple (JNIEnv 
    *env, jclass cls, jobject src, jint destWidth, jint destHeight, jint interpType, jint 
    overallAlpha, jint checkSize, jint color1, jint color2) 
{
    GdkPixbuf *src_g = (GdkPixbuf *)getPointerFromHandle(env, src);
    gint32 destWidth_g = (gint32) destWidth;
    gint32 destHeight_g = (gint32) destHeight;
    GdkInterpType interpType_g = (GdkInterpType) interpType;
    gint32 overallAlpha_g = (gint32) overallAlpha;
    gint32 checkSize_g = (gint32) checkSize;
    gint32 color1_g = (gint32) color1;
    gint32 color2_g = (gint32) color2;
    return getGObjectHandle(env, G_OBJECT(gdk_pixbuf_composite_color_simple
    		(src_g, destWidth_g, destHeight_g, interpType_g, overallAlpha_g,
    		checkSize_g, color1_g, color2_g)));
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_option
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1option (JNIEnv *env, 
    jclass cls, jobject pixbuf, jbyteArray key) 
{
    GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
    jint key_len = (*env)->GetArrayLength(env, key);
    gchar* key_g = (gchar*)g_malloc(key_len + 1);
    (*env)->GetByteArrayRegion(env, key, 0, key_len, (jbyte*)key_g);
    key_g[key_len] = 0;
    const gchar *result_g = (gchar*)gdk_pixbuf_get_option (pixbuf_g, key_g);
    jsize result_len = result_g ? strlen (result_g) : 0;
    jbyteArray result_j = result_g ? (*env)->NewByteArray (env, result_len) : NULL;
    if (result_g) 
    	(*env)->SetByteArrayRegion (env, result_j, 0, result_len, (jbyte*)result_g);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Pixbuf
 * Method:    gdk_pixbuf_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Pixbuf_gdk_1pixbuf_1get_1type (JNIEnv *env, 
    jclass cls)
{
	return (jint)gdk_pixbuf_get_type();
}

#ifdef __cplusplus
}

#endif
