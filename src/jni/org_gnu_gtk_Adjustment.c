/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Adjustment.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gdouble GtkAdjustment_get_lower (GtkAdjustment * cptr) 
{
    return cptr->lower;
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    getLower
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_getLower (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAdjustment *cptr_g = (GtkAdjustment *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkAdjustment_get_lower (cptr_g));
}

static gdouble GtkAdjustment_get_upper (GtkAdjustment * cptr) 
{
    return cptr->upper;
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    getUpper
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_getUpper (JNIEnv *env, jclass cls, jobject 
    cptr) 
{
    GtkAdjustment *cptr_g = (GtkAdjustment *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkAdjustment_get_upper (cptr_g));
}

static gdouble GtkAdjustment_get_step_increment (GtkAdjustment * cptr) 
{
    return cptr->step_increment;
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    getStepIncrement
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_getStepIncrement (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkAdjustment *cptr_g = (GtkAdjustment *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkAdjustment_get_step_increment (cptr_g));
}

static gdouble GtkAdjustment_get_page_increment (GtkAdjustment * cptr) 
{
    return cptr->page_increment;
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    getPageIncrement
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_getPageIncrement (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkAdjustment *cptr_g = (GtkAdjustment *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkAdjustment_get_page_increment (cptr_g));
}

static gdouble GtkAdjustment_get_page_size (GtkAdjustment * cptr) 
{
    return cptr->page_size;
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    getPageSize
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_getPageSize (JNIEnv *env, jclass cls, 
    jobject cptr) 
{
    GtkAdjustment *cptr_g = (GtkAdjustment *)getPointerFromHandle(env, cptr);
    return (jdouble) (GtkAdjustment_get_page_size (cptr_g));
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_adjustment_get_type ();
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1new (JNIEnv *env, jclass 
    cls, jdouble value, jdouble lower, jdouble upper, jdouble stepIncrement, jdouble 
    pageIncrement, jdouble pageSize) 
{
    gdouble value_g = (gdouble) value;
    gdouble lower_g = (gdouble) lower;
    gdouble upper_g = (gdouble) upper;
    gdouble stepIncrement_g = (gdouble) stepIncrement;
    gdouble pageIncrement_g = (gdouble) pageIncrement;
    gdouble pageSize_g = (gdouble) pageSize;
    return getGObjectHandle(env, (GObject *) gtk_adjustment_new (value_g,
    		lower_g, upper_g, stepIncrement_g, pageIncrement_g, pageSize_g));
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_changed
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1changed (JNIEnv *env, 
    jclass cls, jobject adjustment) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_adjustment_changed (adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_value_changed
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1value_1changed (JNIEnv 
    *env, jclass cls, jobject adjustment) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_adjustment_value_changed (adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_clamp_page
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1clamp_1page (JNIEnv *env, 
    jclass cls, jobject adjustment, jdouble lower, jdouble upper) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gdouble lower_g = (gdouble) lower;
    gdouble upper_g = (gdouble) upper;
    gtk_adjustment_clamp_page (adjustment_g, lower_g, upper_g);
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_get_value
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1get_1value (JNIEnv *env, 
    jclass cls, jobject adjustment) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    return (jdouble) (gtk_adjustment_get_value (adjustment_g));
}

/*
 * Class:     org.gnu.gtk.Adjustment
 * Method:    gtk_adjustment_set_value
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Adjustment_gtk_1adjustment_1set_1value (JNIEnv *env, 
    jclass cls, jobject adjustment, jdouble value) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gdouble value_g = (gdouble) value;
    gtk_adjustment_set_value (adjustment_g, value_g);
}


#ifdef __cplusplus
}

#endif
