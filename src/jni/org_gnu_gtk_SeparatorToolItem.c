/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_SeparatorToolItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_SeparatorToolItem
 * Method:    gtk_separator_tool_item_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_SeparatorToolItem_gtk_1separator_1tool_1item_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_separator_tool_item_get_type();
}

/*
 * Class:     org_gnu_gtk_SeparatorToolItem
 * Method:    gtk_separator_tool_item_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SeparatorToolItem_gtk_1separator_1tool_1item_1new(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, (GObject *) gtk_separator_tool_item_new());
}

/*
 * Class:     org_gnu_gtk_SeparatorToolItem
 * Method:    gtk_separator_tool_item_get_draw
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SeparatorToolItem_gtk_1separator_1tool_1item_1get_1draw(JNIEnv *env, jclass cls, jobject item)
{
    GtkSeparatorToolItem *item_g = 
        (GtkSeparatorToolItem*)getPointerFromHandle(env, item);
    return (jboolean)gtk_separator_tool_item_get_draw(item_g);
}

/*
 * Class:     org_gnu_gtk_SeparatorToolItem
 * Method:    gtk_separator_tool_item_set_draw
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_SeparatorToolItem_gtk_1separator_1tool_1item_1set_1draw(JNIEnv *env, jclass cls, jobject item, jboolean draw)
{
    GtkSeparatorToolItem *item_g = 
        (GtkSeparatorToolItem*)getPointerFromHandle(env, item);
    gboolean draw_g = (gboolean)draw;
    gtk_separator_tool_item_set_draw(item_g, draw_g);
}

#ifdef __cplusplus
}
#endif
