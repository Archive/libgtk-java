/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_RadioMenuItem.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.RadioMenuItem
 * Method:    gtk_radio_menu_item_new_with_label
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new_1with_1label(JNIEnv *env, 
    jclass cls, jobjectArray group, jstring label) 
{
	GSList* list = getGSListFromHandles(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject value = getGObjectHandle(env, (GObject *)
			gtk_radio_menu_item_new_with_label(list, label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
    return value;
}

/*
 * Class:     org.gnu.gtk.RadioMenuItem
 * Method:    gtk_radio_menu_item_new_with_mnemonic
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new_1with_1mnemonic(JNIEnv *env, 
    jclass cls, jobjectArray group, jstring label) 
{
	GSList* list = getGSListFromHandles(env, group);
    const gchar* label_g = (*env)->GetStringUTFChars(env, label, NULL);
	jobject value = getGObjectHandle(env, (GObject *)
			gtk_radio_menu_item_new_with_mnemonic(list, label_g));
	(*env)->ReleaseStringUTFChars(env, label, label_g);
    return value;
}

/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_new_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new_1from_1widget
  (JNIEnv *env, jclass cls, jobject group)
{
	GtkRadioMenuItem* group_g = (GtkRadioMenuItem*)getPointerFromHandle(env, group);
	return getGObjectHandle(env, (GObject *) gtk_radio_menu_item_new_from_widget(group_g));
}
                                                                                
/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_new_with_mnemonic_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new_1with_1mnemonic_1from_1widget
  (JNIEnv *env, jclass cls, jobject group, jstring label)
{
	GtkRadioMenuItem* group_g = (GtkRadioMenuItem*)getPointerFromHandle(env, group);
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	jobject hndl = getGObjectHandle(env, (GObject *)
			gtk_radio_menu_item_new_with_mnemonic_from_widget(group_g, l));
	(*env)->ReleaseStringUTFChars(env, label, l);
	return hndl;
}
                                                                                
/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_new_with_label_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new_1with_1label_1from_1widget
  (JNIEnv *env, jclass cls, jobject group, jstring label)
{
	GtkRadioMenuItem* group_g = (GtkRadioMenuItem*)getPointerFromHandle(env, group);
	const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
	jobject hndl = getGObjectHandle(env, (GObject *)
			gtk_radio_menu_item_new_with_label_from_widget(group_g, l));
	(*env)->ReleaseStringUTFChars(env, label, l);
	return hndl;
}
                                                                                
/*
 * Class:     org.gnu.gtk.RadioMenuItem
 * Method:    gtk_radio_menu_item_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_radio_menu_item_get_type ();
}

/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1new
  (JNIEnv *env, jclass cls, jobjectArray group)
{
	GSList* list = getGSListFromHandles(env, group);
	return getGObjectHandle(env, (GObject *) gtk_radio_menu_item_new(list));
}
                                                                                                           
/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_get_group
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1get_1group
  (JNIEnv *env, jclass cls, jobject menu)
{
	GtkRadioMenuItem* menu_g = (GtkRadioMenuItem*)getPointerFromHandle(env, menu);
	return getGObjectHandlesFromGSList(env, gtk_radio_menu_item_get_group(menu_g));
}

/*
 * Class:     org_gnu_gtk_RadioMenuItem
 * Method:    gtk_radio_menu_item_set_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioMenuItem_gtk_1radio_1menu_1item_1set_1group
  (JNIEnv *env, jclass cls, jobject menu, jobjectArray group)
{
	GtkRadioMenuItem* menu_g = (GtkRadioMenuItem*)getPointerFromHandle(env, menu);
	GSList* list = getGSListFromHandles(env, group);
	gtk_radio_menu_item_set_group(menu_g, list);
}
                                                                                                           

#ifdef __cplusplus
}

#endif
