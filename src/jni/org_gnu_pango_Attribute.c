/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>

#include "org_gnu_pango_Attribute.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Attribute
 * Method:    getStartIndex
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Attribute_getStartIndex 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttribute *obj_g = (PangoAttribute *)getPointerFromHandle(env, obj);
    return (jint)obj_g->start_index;
}

/*
 * Class:     org.gnu.pango.Attribute
 * Method:    getEndIndex
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Attribute_getEndIndex 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttribute *obj_g = (PangoAttribute *)getPointerFromHandle(env, obj);
    return (jint)obj_g->end_index;
}

#ifdef __cplusplus
}

#endif
