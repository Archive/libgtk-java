/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_WindowAttr.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    gdk_window_attr_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_WindowAttr_gdk_1window_1attr_1new (JNIEnv *env, jclass cls)
{
    GdkWindowAttr *obj_g = g_new( GdkWindowAttr, 1 );
    return getStructHandle(env, obj_g, NULL, g_free);
}

static gchar * GdkWindowAttr_get_title (GdkWindowAttr * cptr) 
{
    return cptr->title;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getTitle
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_WindowAttr_getTitle (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar *result_g = GdkWindowAttr_get_title (obj_g);
    return (*env)->NewStringUTF(env, result_g);
}

static void GdkWindowAttr_set_title (GdkWindowAttr * cptr, gchar * title) 
{
    cptr->title = title;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setTitle
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setTitle (JNIEnv *env, jclass klass, jobject obj, jstring 
    title) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    GdkWindowAttr_set_title (obj_g, title_g);
    (*env)->ReleaseStringUTFChars(env, title, title_g);
}

static gint32 GdkWindowAttr_get_event_mask (GdkWindowAttr * cptr) 
{
    return cptr->event_mask;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getEventMask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getEventMask (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_event_mask (obj_g));
}

static void GdkWindowAttr_set_event_mask (GdkWindowAttr * cptr, gint32 event_mask) 
{
    cptr->event_mask = event_mask;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setEventMask
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setEventMask (JNIEnv *env, jclass klass, jobject obj, jint 
    event_mask) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gint32 event_mask_g = (gint32) event_mask;
    GdkWindowAttr_set_event_mask (obj_g, event_mask_g);
}

static gint32 GdkWindowAttr_get_x (GdkWindowAttr * cptr) 
{
    return cptr->x;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getX
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getX (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_x (obj_g));
}

static void GdkWindowAttr_set_x (GdkWindowAttr * cptr, gint32 x) 
{
    cptr->x = x;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setX
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setX (JNIEnv *env, jclass klass, jobject obj, jint x) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gint32 x_g = (gint32) x;
    GdkWindowAttr_set_x (obj_g, x_g);
}

static gint32 GdkWindowAttr_get_y (GdkWindowAttr * cptr) 
{
    return cptr->y;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getY
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getY (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_y (obj_g));
}

static void GdkWindowAttr_set_y (GdkWindowAttr * cptr, gint32 y) 
{
    cptr->y = y;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setY
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setY (JNIEnv *env, jclass klass, jobject obj, jint y) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gint32 y_g = (gint32) y;
    GdkWindowAttr_set_y (obj_g, y_g);
}

static gint32 GdkWindowAttr_get_width (GdkWindowAttr * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getWidth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_width (obj_g));
}

static void GdkWindowAttr_set_width (GdkWindowAttr * cptr, gint32 width) 
{
    cptr->width = width;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setWidth
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setWidth (JNIEnv *env, jclass klass, jobject obj, jint width) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gint32 width_g = (gint32) width;
    GdkWindowAttr_set_width (obj_g, width_g);
}

static gint32 GdkWindowAttr_get_height (GdkWindowAttr * cptr) 
{
    return cptr->height;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getHeight (JNIEnv *env, jclass cls, jobject obj)
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_height (obj_g));
}

static void GdkWindowAttr_set_height (GdkWindowAttr * cptr, gint32 height) 
{
    cptr->height = height;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setHeight
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setHeight (JNIEnv *env, jclass klass, jobject obj, jint 
    height) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gint32 height_g = (gint32) height;
    GdkWindowAttr_set_height (obj_g, height_g);
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getWclass
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getWclass (JNIEnv *env, jclass cls, jobject obj)
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) obj_g->wclass;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setWclass
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setWclass (JNIEnv *env, jclass klass, jobject obj, jint 
    wclass) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    GdkWindowClass wclass_g = (GdkWindowClass) wclass;
    obj_g->wclass = wclass_g;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getVisual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_WindowAttr_getVisual (JNIEnv *env, jclass cls, jobject obj)
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->visual);
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setVisual
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setVisual (JNIEnv *env, jclass klass, jobject obj, jobject 
    visual) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    GdkVisual *visual_g = (GdkVisual *)getPointerFromHandle(env, visual);
    obj_g->visual = visual_g;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getColormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_WindowAttr_getColormap (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->colormap);
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setColormap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setColormap (JNIEnv *env, jclass klass, jobject obj, jobject 
    colormap) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    GdkColormap *colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    obj_g->colormap = colormap_g;
}

static GdkWindowType GdkWindowAttr_get_window_type (GdkWindowAttr * cptr) 
{
    return cptr->window_type;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getWindowType
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_WindowAttr_getWindowType (JNIEnv *env, jclass cls, jobject 
    obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jint) (GdkWindowAttr_get_window_type (obj_g));
}

static void GdkWindowAttr_set_window_type (GdkWindowAttr * cptr, GdkWindowType window_type) 
{
    cptr->window_type = window_type;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setWindowType
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setWindowType (JNIEnv *env, jclass klass, jobject obj, jint 
    window_type) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    GdkWindowType window_type_g = (GdkWindowType) window_type;
    GdkWindowAttr_set_window_type (obj_g, window_type_g);
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getCursor
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_WindowAttr_getCursor (JNIEnv *env, jclass cls, jobject obj)
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return getGBoxedHandle(env, obj_g->cursor, gdk_cursor_get_type(),
                           (GBoxedCopyFunc)gdk_cursor_ref, 
                           (GBoxedFreeFunc)gdk_cursor_unref);
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setCursor
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setCursor (JNIEnv *env, jclass klass, jobject obj, jobject 
    cursor) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    GdkCursor *cursor_g = (GdkCursor *)getPointerFromHandle(env, cursor);
    obj_g->cursor = cursor_g;
}

static gchar * GdkWindowAttr_get_wmclass_name (GdkWindowAttr * cptr) 
{
    return cptr->wmclass_name;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getWmclassName
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_WindowAttr_getWmclassName (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar *result_g = GdkWindowAttr_get_wmclass_name (obj_g);
    return (*env)->NewStringUTF(env, result_g);
}

static void GdkWindowAttr_set_wmclass_name (GdkWindowAttr * cptr, gchar * wmclass_name) 
{
    cptr->wmclass_name = wmclass_name;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setWmclassName
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setWmclassName (JNIEnv *env, jclass klass, jobject obj, 
    jstring wmclass_name) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar* wmclass_name_g = (gchar*)(*env)->GetStringUTFChars(env, wmclass_name, 0);
    GdkWindowAttr_set_wmclass_name (obj_g, wmclass_name_g);
    (*env)->ReleaseStringUTFChars(env, wmclass_name, wmclass_name_g);
}

static gchar * GdkWindowAttr_get_wmclass_class (GdkWindowAttr * cptr) 
{
    return cptr->wmclass_class;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getWmclassClass
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_WindowAttr_getWmclassClass (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar *result_g = GdkWindowAttr_get_wmclass_class (obj_g);
    return (*env)->NewStringUTF(env, result_g);
}

static void GdkWindowAttr_set_wmclass_class (GdkWindowAttr * cptr, gchar * wmclass_class) 
{
    cptr->wmclass_class = wmclass_class;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setWmclassClass
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setWmclassClass (JNIEnv *env, jclass klass, jobject obj, 
    jstring wmclass_class) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gchar* wmclass_class_g = (gchar*)(*env)->GetStringUTFChars(env, wmclass_class, 0);
    GdkWindowAttr_set_wmclass_class (obj_g, wmclass_class_g);
    (*env)->ReleaseStringUTFChars(env, wmclass_class, wmclass_class_g);
}

static gboolean GdkWindowAttr_get_override_redirect (GdkWindowAttr * cptr) 
{
    return cptr->override_redirect;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    getOverrideRedirect
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_WindowAttr_getOverrideRedirect (JNIEnv *env, jclass 
    cls, jobject obj) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    return (jboolean) (GdkWindowAttr_get_override_redirect (obj_g));
}

static void GdkWindowAttr_set_override_redirect (GdkWindowAttr * cptr, gboolean override_redirect) 
{
    cptr->override_redirect = override_redirect;
}

/*
 * Class:     org.gnu.gdk.WindowAttr
 * Method:    setOverrideRedirect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_WindowAttr_setOverrideRedirect (JNIEnv *env, jclass klass, jobject obj, 
    jboolean override_redirect) 
{
    GdkWindowAttr *obj_g = (GdkWindowAttr *)getPointerFromHandle(env, obj);
    gboolean override_redirect_g = (gboolean) override_redirect;
    GdkWindowAttr_set_override_redirect (obj_g, override_redirect_g);
}


#ifdef __cplusplus
}

#endif
