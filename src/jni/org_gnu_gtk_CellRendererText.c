/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellRendererText.h"
#ifdef __cplusplus
extern "C" 
{
#endif

// TODO: Fix the following properties.
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1setAttributes (
    JNIEnv *env, jclass cls, jobject renderer, jobject attr) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
    PangoAttrList* attr_g = (PangoAttrList*)getPointerFromHandle(env, attr);
	g_object_set(renderer_g, "attributes", attr_g, NULL);
}

JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1setFont (
    JNIEnv *env, jclass cls, jobject renderer, jobject font) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
    PangoFontDescription *font_g = (PangoFontDescription*)getPointerFromHandle(env, font);
	g_object_set( renderer_g, "font-desc", font_g, NULL);
}

JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1getFont (
    JNIEnv *env, jclass cls, jobject renderer) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
	PangoFontDescription *desc = pango_font_description_new();
	g_object_get( renderer_g, "font-desc", desc, NULL);
	return getGBoxedHandle(env, desc, PANGO_TYPE_FONT_DESCRIPTION, NULL,
			(JGFreeFunc) pango_font_description_free);
}


JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1setProperty (
    JNIEnv *env, jclass cls, jobject renderer, jstring property, jboolean setting) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
	const char* property_utf =  (*env)->GetStringUTFChars(env, property, NULL);
	g_object_set( renderer_g,  property_utf, setting, NULL);
}


JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1setUnderLine (
    JNIEnv *env, jclass cls, jobject renderer, jint underline) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
    g_object_set( renderer_g,  "underline", underline, NULL);
}


/*
 * Class:     org.gnu.gtk.CellRendererText
 * Method:    gtk_cell_renderer_text_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellRendererText_gtk_1cell_1renderer_1text_1get_1type (
    JNIEnv *env, jclass cls) 
{
    return (jint)gtk_cell_renderer_text_get_type ();
}

/*
 * Class:     org.gnu.gtk.CellRendererText
 * Method:    gtk_cell_renderer_text_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererText_gtk_1cell_1renderer_1text_1new (JNIEnv 
    *env, jclass cls) 
{
        return getGObjectHandle(env, (GObject *) gtk_cell_renderer_text_new ());
}

/*
 * Class:     org.gnu.gtk.CellRendererText
 * Method:    gtk_cell_renderer_text_set_fixed_height_from_font
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererText_gtk_1cell_1renderer_1text_1set_1fixed_1height_1from_1font (
    JNIEnv *env, jclass cls, jobject renderer, jint numberOfRows) 
{
    GtkCellRendererText *renderer_g = (GtkCellRendererText *)getPointerFromHandle(env, renderer);
    gint32 numberOfRows_g = (gint32) numberOfRows;
    gtk_cell_renderer_text_set_fixed_height_from_font (renderer_g, numberOfRows_g);
}


#ifdef __cplusplus
}

#endif
