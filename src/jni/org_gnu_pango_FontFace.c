/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_FontFace.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.FontFace
 * Method:    pango_font_face_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontFace_pango_1font_1face_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)pango_font_face_get_type ();
}

/*
 * Class:     org.gnu.pango.FontFace
 * Method:    pango_font_face_describe
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_FontFace_pango_1font_1face_1describe (JNIEnv *env, 
    jclass cls, jobject face) 
{
    PangoFontFace *face_g = (PangoFontFace *)getPointerFromHandle(env, face);
    return getGBoxedHandle(env, pango_font_face_describe (face_g),
                           PANGO_TYPE_FONT_DESCRIPTION, NULL,
                           (GBoxedFreeFunc) pango_font_description_free);
}

/*
 * Class:     org.gnu.pango.FontFace
 * Method:    pango_font_face_get_face_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_pango_FontFace_pango_1font_1face_1get_1face_1name (
    JNIEnv *env, jclass cls, jobject face) 
{
    gchar *result_g = (gchar *)pango_font_face_get_face_name (
    		(PangoFontFace *)getPointerFromHandle(env, face));
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
