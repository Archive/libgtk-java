/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_IconTheme
#define _Included_org_gnu_gtk_IconTheme
#include "org_gnu_gtk_IconTheme.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_icon_theme_get_type();
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_icon_theme_new());
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_get_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1default
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_icon_theme_get_default());
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_get_for_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1for_1screen
  (JNIEnv *env, jclass cls, jobject screen)
{
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	return getGObjectHandle(env, (GObject *) gtk_icon_theme_get_for_screen(screen_g));
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_set_screen
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1set_1screen
  (JNIEnv *env, jclass cls, jobject itheme, jobject screen)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	GdkScreen* screen_g = (GdkScreen*)getPointerFromHandle(env, screen);
	gtk_icon_theme_set_screen(itheme_g, screen_g);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_set_search_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1set_1search_1path
  (JNIEnv *env, jclass cls, jobject itheme, jobjectArray path, jint numElements)
{
	const gchar** elements = g_malloc(sizeof(gchar*) * (gint)numElements);
	int index;
	jobject obj;
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	
	for (index = 0; index < (gint)numElements; index++)
	{
		obj = (*env)->GetObjectArrayElement(env, path, index);
		elements[index] = (*env)->GetStringUTFChars(env, obj, NULL);
	}
	gtk_icon_theme_set_search_path(itheme_g, elements, (gint)numElements);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_get_search_path
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1search_1path
  (JNIEnv *env, jclass cls, jobject itheme)
{
	jobjectArray array;
	jclass strCls;
	gchar** path;
	int numElements;
	int index;
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	
	gtk_icon_theme_get_search_path(itheme_g, &path, &numElements);
	strCls = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, numElements, strCls, NULL);
	for (index = 0; index < numElements; index++) {
		(*env)->SetObjectArrayElement(env, array, index, (*env)->NewStringUTF(env, path[index]));
	}
	return array;
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_append_search_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1append_1search_1path
  (JNIEnv *env, jclass cls, jobject itheme, jstring path)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
	gtk_icon_theme_append_search_path(itheme_g, p);
	(*env)->ReleaseStringUTFChars(env, path, p);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_prepend_search_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1prepend_1search_1path
  (JNIEnv *env, jclass cls, jobject itheme, jstring path)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, path, NULL);
	gtk_icon_theme_prepend_search_path(itheme_g, p);
	(*env)->ReleaseStringUTFChars(env, path, p);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_set_custom_theme
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1set_1custom_1theme
  (JNIEnv *env, jclass cls, jobject itheme, jstring name)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_icon_theme_prepend_search_path(itheme_g, p);
	(*env)->ReleaseStringUTFChars(env, name, p);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_has_icon
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1has_1icon
  (JNIEnv *env, jclass cls, jobject itheme, jstring name)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, name, NULL);
	jboolean value = (jboolean)gtk_icon_theme_has_icon(itheme_g, p);
	(*env)->ReleaseStringUTFChars(env, name, p);
	return value;
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_lookup_icon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1lookup_1icon
  (JNIEnv *env, jclass cls, jobject itheme, jstring name, jint size, jint flags)
{
	GtkIconInfo *icon_info;
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, name, NULL);
	icon_info = gtk_icon_theme_lookup_icon(itheme_g, p, (gint) size,
			(GtkIconLookupFlags) flags);
	jobject ret = getGBoxedHandle(env, icon_info, GTK_TYPE_ICON_INFO, NULL,
			(GBoxedFreeFunc) gtk_icon_info_free);
	(*env)->ReleaseStringUTFChars(env, name, p);
	return ret;
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_load_icon
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1load_1icon
  (JNIEnv *env, jclass cls, jobject itheme, jstring name, jint size, jint flags)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, name, NULL);
	GError* e = NULL;
	jobject value =  getGObjectHandleAndRef(env,(GObject *)
			gtk_icon_theme_load_icon(itheme_g, p, (gint)size, (GtkIconLookupFlags)flags, &e));
	(*env)->ReleaseStringUTFChars(env, name, p);
	if (e != NULL)	{
    	g_error_free(e);
    	return NULL;
	}
	return value;
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_list_icons
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1list_1icons
  (JNIEnv *env, jclass cls, jobject itheme, jstring context)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	const gchar* p = (*env)->GetStringUTFChars(env, context, NULL);
	GList* list = gtk_icon_theme_list_icons(itheme_g, p);
	guint len;
	jobjectArray array;
	jclass strCls;
	guint index;

	(*env)->ReleaseStringUTFChars(env, context, p);
 	if (NULL == list)
		return NULL;
	 len = g_list_length(list);
	strCls = (*env)->FindClass(env, "java/lang/String");
	array = (*env)->NewObjectArray(env, len, strCls, NULL);
	for (index = 0; index < len; index++) {
		GList *item = g_list_nth(list, index);
		(*env)->SetObjectArrayElement(env, array, index, (*env)->NewStringUTF(env, item->data));
	}
	return array;
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_get_example_icon_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1example_1icon_1name
  (JNIEnv *env, jclass cls, jobject itheme)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	gchar* name = gtk_icon_theme_get_example_icon_name(itheme_g);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_rescan_if_needed
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1rescan_1if_1needed
  (JNIEnv *env, jclass cls, jobject itheme)
{
	GtkIconTheme* itheme_g = (GtkIconTheme*)getPointerFromHandle(env, itheme);
	return (jboolean)gtk_icon_theme_rescan_if_needed(itheme_g);
}

/*
 * Class:     org_gnu_gtk_IconTheme
 * Method:    gtk_icon_theme_add_builtin_icon
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1add_1builtin_1icon
  (JNIEnv *env, jclass cls, jstring name, jint size, jobject pixbuf)
{
	GdkPixbuf* pixbuf_g = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
	const gchar* p = (*env)->GetStringUTFChars(env, name, NULL);
	gtk_icon_theme_add_builtin_icon(p, (gint)size, pixbuf_g);
	(*env)->ReleaseStringUTFChars(env, name, p);
}

JNIEXPORT jintArray JNICALL Java_org_gnu_gtk_IconTheme_gtk_1icon_1theme_1get_1icon_1sizes
(JNIEnv *env, jclass cls, jobject icon_theme, jstring icon_name)
{
    GtkIconTheme * icon_theme_g = 
        (GtkIconTheme *)getPointerFromHandle(env, icon_theme);
    const gchar * icon_name_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, icon_name, 0);
    gint * sizes = gtk_icon_theme_get_icon_sizes(icon_theme_g, icon_name_g);
    int size = 0;
    while( sizes[ size ] != 0 ) {
        size++;
    }
    jintArray ret_g = (*env)->NewIntArray(env, size);
    (*env)->SetIntArrayRegion(env, ret_g, 0, size, (jint *)sizes);
    (*env)->ReleaseStringUTFChars(env, icon_name, icon_name_g);
    return ret_g;
}

#ifdef __cplusplus
}
#endif
#endif
