/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ColorSelection.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_color_selection_get_type ();
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_color_selection_new());
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_has_opacity_control
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1has_1opacity_1control (JNIEnv *env, 
    jclass cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    return (jboolean) (gtk_color_selection_get_has_opacity_control (colorsel_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_has_opacity_control
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1has_1opacity_1control (JNIEnv *env, 
    jclass cls, jobject colorsel, jboolean hasOpacity) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    gboolean hasOpacity_g = (gboolean) hasOpacity;
    gtk_color_selection_set_has_opacity_control (colorsel_g, hasOpacity_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_has_palette
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1has_1palette (JNIEnv *env, jclass 
    cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    return (jboolean) (gtk_color_selection_get_has_palette (colorsel_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_has_palette
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1has_1palette (
    JNIEnv *env, jclass cls, jobject colorsel, jboolean hasPalette) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    gboolean hasPalette_g = (gboolean) hasPalette;
    gtk_color_selection_set_has_palette (colorsel_g, hasPalette_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_current_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1current_1color (JNIEnv *env, jclass 
    cls, jobject colorsel, jobject color) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_color_selection_set_current_color (colorsel_g, color_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_current_alpha
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1current_1alpha (JNIEnv *env, jclass 
    cls, jobject colorsel, jint alpha) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    gint32 alpha_g = (gint32) alpha;
    gtk_color_selection_set_current_alpha (colorsel_g, alpha_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_current_color
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1current_1color (JNIEnv *env, jclass 
    cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g;
    GdkColor *color;
    colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    color = g_new(GdkColor, 1);
	gtk_color_selection_get_current_color (colorsel_g, color);
	return getGBoxedHandle(env, color, GDK_TYPE_COLOR,
		(GBoxedCopyFunc) gdk_color_copy, (GBoxedFreeFunc) gdk_color_free);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_current_alpha
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1current_1alpha (JNIEnv *env, jclass 
    cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    return (jint) (gtk_color_selection_get_current_alpha (colorsel_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_previous_color
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1previous_1color (JNIEnv *env, 
    jclass cls, jobject colorsel, jobject color) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gtk_color_selection_set_previous_color (colorsel_g, color_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_set_previous_alpha
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1set_1previous_1alpha (JNIEnv *env, 
    jclass cls, jobject colorsel, jint alpha) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    gint32 alpha_g = (gint32) alpha;
    gtk_color_selection_set_previous_alpha (colorsel_g, alpha_g);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_previous_color
 */
JNIEXPORT jobject JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1previous_1color (JNIEnv *env, 
    jclass cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    GdkColor *color_g = NULL;
	gtk_color_selection_get_previous_color (colorsel_g, color_g);
	return getGBoxedHandle(env, color_g, GDK_TYPE_COLOR,
			(GBoxedCopyFunc) gdk_color_copy, (GBoxedFreeFunc) gdk_color_free);
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_get_previous_alpha
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1get_1previous_1alpha (JNIEnv *env, 
    jclass cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    return (jint) (gtk_color_selection_get_previous_alpha (colorsel_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_is_adjusting
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1is_1adjusting (
    JNIEnv *env, jclass cls, jobject colorsel) 
{
    GtkColorSelection *colorsel_g = (GtkColorSelection *)getPointerFromHandle(env, colorsel);
    return (jboolean) (gtk_color_selection_is_adjusting (colorsel_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_palette_from_string
 *
 * TODO:  This method isn't implemented right, as it assumes that
 *        the passed in array and length are already allocated, when
 *        it appears that gtk_color_selection_palette_from_string
 *        allocates the array itself.  One clue is the pointer to
 *        the array length, as normally that means that the length
 *        will be set by the routine.
 *        
 *        Anyway, currently no Java code calls this method, so it
 *        can stay broken until needed.  (tball)
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1palette_1from_1string (JNIEnv *env, 
    jclass cls, jstring str, jobjectArray color) 
{
    gchar* str_g = (gchar*)(*env)->GetStringUTFChars(env, str, 0);
    GdkColor **color_g = (GdkColor**)getPointerArrayFromHandles(env, color);
    gint len = (*env)->GetArrayLength(env, color);
    jboolean result_j = (jboolean) gtk_color_selection_palette_from_string (str_g, 
                color_g, &len);
   	(*env)->ReleaseStringUTFChars(env, str, str_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.ColorSelection
 * Method:    gtk_color_selection_palette_to_string
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_ColorSelection_gtk_1color_1selection_1palette_1to_1string (JNIEnv *env, jclass 
    cls, jobject color, jint nColors) 
{
	GdkColor *color_g = (GdkColor*)getPointerFromHandle(env, color);
    gchar *result_g = gtk_color_selection_palette_to_string(color_g, (gint32)nColors);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
