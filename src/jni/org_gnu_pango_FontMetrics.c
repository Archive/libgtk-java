/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_FontMetrics.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)pango_font_metrics_get_type ();
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_ref
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1ref (JNIEnv *env, 
    jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = 
        (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    return getGBoxedHandle(env, pango_font_metrics_ref (metrics_g),
                           PANGO_TYPE_FONT_METRICS, NULL,
                           (JGFreeFunc)pango_font_metrics_unref);
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1unref (JNIEnv *env, 
    jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    pango_font_metrics_unref (metrics_g);
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_get_ascent
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1get_1ascent (JNIEnv 
    *env, jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    return (jint) (pango_font_metrics_get_ascent (metrics_g));
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_get_descent
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1get_1descent (
    JNIEnv *env, jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    return (jint) (pango_font_metrics_get_descent (metrics_g));
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_get_approximate_char_width
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1get_1approximate_1char_1width (JNIEnv 
    *env, jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    return (jint) (pango_font_metrics_get_approximate_char_width (metrics_g));
}

/*
 * Class:     org.gnu.pango.FontMetrics
 * Method:    pango_font_metrics_get_approximate_digit_width
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_pango_FontMetrics_pango_1font_1metrics_1get_1approximate_1digit_1width (JNIEnv 
    *env, jclass cls, jobject metrics) 
{
    PangoFontMetrics *metrics_g = (PangoFontMetrics *)getPointerFromHandle(env, metrics);
    return (jint) (pango_font_metrics_get_approximate_digit_width (metrics_g));
}


#ifdef __cplusplus
}

#endif
