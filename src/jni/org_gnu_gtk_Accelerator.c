/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Accelerator.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_valid
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1valid (JNIEnv *env, 
    jclass cls, jint key, jint mods) 
{
	guint keyval = (guint) key;
	GdkModifierType mods_g = (GdkModifierType) mods;
	return (jboolean) gtk_accelerator_valid(keyval, mods_g);
}

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_parse
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1parse (JNIEnv *env, jclass 
    cls, jstring accelerator, jintArray key, jintArray mods) 
{
	const gchar *accelerator_g = (*env)->GetStringUTFChars(env, accelerator, NULL);
	
    guint *keyval_g = (guint *) (*env)->GetIntArrayElements (env, key, NULL);
    GdkModifierType *mods_g = (GdkModifierType *) (*env)->GetIntArrayElements (env, mods, NULL);
	
	gtk_accelerator_parse(accelerator_g, keyval_g, mods_g);
	
    (*env)->ReleaseIntArrayElements (env, key, (jint *) keyval_g, 0);
    (*env)->ReleaseIntArrayElements (env, mods, (jint *) mods_g, 0);
	(*env)->ReleaseStringUTFChars(env, accelerator, accelerator_g);
}

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1name (JNIEnv *env, jclass 
    cls, jint key, jint mods) 
{
	guint keyval = (guint) key;
	GdkModifierType mods_g = (GdkModifierType) mods;
	
	return (*env)->NewStringUTF(env, gtk_accelerator_name(keyval, mods_g));
}

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_get_label
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1get_1label (JNIEnv *env, 
    jclass cls, jint key, jint mods) 
{
	guint keyval = (guint) key;
	GdkModifierType mods_g = (GdkModifierType) mods;
	
	return (*env)->NewStringUTF(env, gtk_accelerator_get_label(keyval, mods_g));
}

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_set_default_mod_mask
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1set_1default_1mod_1mask (
    JNIEnv *env, jclass cls, jint mods) 
{
	GdkModifierType mods_g = (GdkModifierType) mods;
	gtk_accelerator_set_default_mod_mask(mods_g);
}

/*
 * Class:     org.gnu.gtk.Accelerator
 * Method:    gtk_accelerator_get_default_mod_mask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Accelerator_gtk_1accelerator_1get_1default_1mod_1mask 
	(JNIEnv *env, jclass cls) 
{
	return (jint) gtk_accelerator_get_default_mod_mask();
}

#ifdef __cplusplus
}

#endif
