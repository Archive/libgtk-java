/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Window.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_window_get_type ();
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_new
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Window_gtk_1window_1new (JNIEnv *env, jclass cls, jint type) 
{
    GtkWindowType type_g = (GtkWindowType) type;
    return getGObjectHandle(env, (GObject *) gtk_window_new(type_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_title
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1title (JNIEnv *env, jclass cls, jobject window, jstring title) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
    gtk_window_set_title (window_g, title_g);
    (*env)->ReleaseStringUTFChars( env, title, title_g );
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_title
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1title (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (*env)->NewStringUTF(env, (gchar*)gtk_window_get_title (window_g) );
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_wmclass
 * Signature: (I[B[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1wmclass (JNIEnv *env, jclass cls, jobject window, jstring wmclassName, jstring wmclassClass) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* wmclassName_g = 
        (*env)->GetStringUTFChars(env, wmclassName, NULL);
    const gchar* wmclassClass_g = 
        (*env)->GetStringUTFChars(env, wmclassClass, NULL);
    gtk_window_set_wmclass (window_g, wmclassName_g, wmclassClass_g);
    (*env)->ReleaseStringUTFChars(env, wmclassName, wmclassName_g);
    (*env)->ReleaseStringUTFChars(env, wmclassClass, wmclassClass_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_role
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1role (JNIEnv *env, jclass cls, jobject window, jstring role) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* role_g = (*env)->GetStringUTFChars(env, role, NULL);
    gtk_window_set_role (window_g, role_g);
    (*env)->ReleaseStringUTFChars( env, role, role_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_role
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1role (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (*env)->NewStringUTF(env, (gchar*)gtk_window_get_role (window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_add_accel_group
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1add_1accel_1group (JNIEnv *env, jclass cls, jobject window, jobject accelGroup) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_window_add_accel_group (window_g, accelGroup_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_remove_accel_group
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1remove_1accel_1group (JNIEnv *env, jclass cls, jobject window, jobject accelGroup) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkAccelGroup *accelGroup_g = 
        (GtkAccelGroup *)getPointerFromHandle(env, accelGroup);
    gtk_window_remove_accel_group (window_g, accelGroup_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_position
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1position (JNIEnv *env, jclass cls, jobject window, jint position) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkWindowPosition position_g = (GtkWindowPosition) position;
    gtk_window_set_position (window_g, position_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_activate_focus
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Window_gtk_1window_1activate_1focus (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jint)(gtk_window_activate_focus(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_focus
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1focus (JNIEnv *env, jclass cls, jobject window, jobject focus) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkWidget *focus_g = (GtkWidget *)getPointerFromHandle(env, focus);
    gtk_window_set_focus (window_g, focus_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_focus
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1focus (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandle(env, (GObject *) gtk_window_get_focus (window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_default
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default (JNIEnv *env, jclass cls, jobject window, jobject defaultWidget) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkWidget *defaultWidget_g = 
        (GtkWidget *)getPointerFromHandle(env, defaultWidget);
    gtk_window_set_default (window_g, defaultWidget_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_activate_default
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1activate_1default (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_activate_default (window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_transient_for
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1transient_1for (JNIEnv *env, jclass cls, jobject window, jobject parent) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkWindow *parent_g = (GtkWindow *)getPointerFromHandle(env, parent);
    gtk_window_set_transient_for (window_g, parent_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_transient_for
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1transient_1for (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandle(env, (GObject *) gtk_window_get_transient_for (window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_type_hint
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1type_1hint (JNIEnv *env, jclass cls, jobject window, jint hint) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkWindowTypeHint hint_g = (GdkWindowTypeHint) hint;
    gtk_window_set_type_hint (window_g, hint_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_type_hint
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1type_1hint (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jint)(gtk_window_get_type_hint(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_destroy_with_parent
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1destroy_1with_1parent (JNIEnv *env, jclass cls, jobject window, jboolean setting) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean setting_g = (gboolean) setting;
    gtk_window_set_destroy_with_parent (window_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_destroy_with_parent
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1destroy_1with_1parent (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_get_destroy_with_parent(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_resizable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1resizable (JNIEnv *env, jclass cls, jobject window, jboolean resizable) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean resizable_g = (gboolean) resizable;
    gtk_window_set_resizable (window_g, resizable_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_resizable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1resizable (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_get_resizable(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_gravity
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1gravity (JNIEnv *env, jclass cls, jobject window, jint gravity) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkGravity gravity_g = (GdkGravity) gravity;
    gtk_window_set_gravity (window_g, gravity_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_gravity
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1gravity (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jint)(gtk_window_get_gravity(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_geometry_hints
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1geometry_1hints (JNIEnv *env, jclass cls, jobject window, jobject geometryWidget, jobject geometry, jint geomMask) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GtkWidget *geometryWidget_g = 
        (GtkWidget *)getPointerFromHandle(env, geometryWidget);
    GdkGeometry *geometry_g = 
        (GdkGeometry *)getPointerFromHandle(env, geometry);
    GdkWindowHints geomMask_g = (GdkWindowHints) geomMask;
    gtk_window_set_geometry_hints(window_g, geometryWidget_g, geometry_g, geomMask_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_has_frame
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1has_1frame (JNIEnv *env, jclass cls, jobject window, jboolean setting) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean setting_g = (gboolean) setting;
    gtk_window_set_has_frame (window_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_has_frame
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1has_1frame (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_get_has_frame(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_frame_dimensions
 * Signature: (IIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1frame_1dimensions (JNIEnv *env, jclass cls, jobject window, jint left, jint top, jint right, jint bottom) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 left_g = (gint32) left;
    gint32 top_g = (gint32) top;
    gint32 right_g = (gint32) right;
    gint32 bottom_g = (gint32) bottom;
    gtk_window_set_frame_dimensions (window_g, left_g, top_g, right_g, bottom_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_frame_dimensions
 * Signature: (I[Lint ;[Lint ;[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1frame_1dimensions (JNIEnv *env, jclass cls, jobject window, jintArray left, jintArray top, jintArray right, jintArray bottom) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint *left_g = (gint *) (*env)->GetIntArrayElements (env, left, NULL);
    gint *top_g = (gint *) (*env)->GetIntArrayElements (env, top, NULL);
    gint *right_g = (gint *) (*env)->GetIntArrayElements (env, right, NULL);
    gint *bottom_g = (gint *) (*env)->GetIntArrayElements (env, bottom, NULL);
    gtk_window_get_frame_dimensions (window_g, left_g, top_g, right_g, bottom_g);
    (*env)->ReleaseIntArrayElements (env, left, (jint *) left_g, 0);
    (*env)->ReleaseIntArrayElements (env, top, (jint *) top_g, 0);
    (*env)->ReleaseIntArrayElements (env, right, (jint *) right_g, 0);
    (*env)->ReleaseIntArrayElements (env, bottom, (jint *) bottom_g, 0);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_decorated
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1decorated (JNIEnv *env, jclass cls, jobject window, jboolean setting) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean setting_g = (gboolean) setting;
    gtk_window_set_decorated (window_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_decorated
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1decorated (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_get_decorated(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_icon_list
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1icon_1list 
	(JNIEnv *env, jclass cls, jobject window, jobjectArray list) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GList *list_g = (GList *)getGListFromHandles(env, list);
    gtk_window_set_icon_list (window_g, list_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_icon_list
 * Signature: (I)I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1icon_1list (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandlesFromGList(env, gtk_window_get_icon_list (window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_icon
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1icon (JNIEnv *env, jclass cls, jobject window, jobject icon) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkPixbuf *icon_g = (GdkPixbuf *)getPointerFromHandle(env, icon);
    gtk_window_set_icon (window_g, icon_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_icon
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1icon (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandle(env, (GObject *) gtk_window_get_icon (window_g));
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_default_icon_list
 * Signature: ([I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default_1icon_1list(JNIEnv *env, jclass cls, jobjectArray pixbufs)
{
    jsize len;
    int index;
    jobject p;
    GList *list;
    GdkPixbuf* pixbuf;

    list = g_list_alloc();
    len = (*env)->GetArrayLength(env, pixbufs);
    for (index = 0; index < len; index++) {
        p = (*env)->GetObjectArrayElement(env, pixbufs, index);
        pixbuf = (GdkPixbuf*)getPointerFromHandle(env, p);
        list = g_list_append(list, pixbuf);
    }
    gtk_window_set_default_icon_list(list);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Window  
 * * Method:    gtk_window_get_default_icon_list
 * Signature: ()[I  
 * */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1default_1icon_1list(JNIEnv *env, jclass cls)
{
    return getGObjectHandlesFromGList(env,gtk_window_get_default_icon_list());
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_modal
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1modal (JNIEnv *env, jclass cls, jobject window, jboolean modal) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean modal_g = (gboolean) modal;
    gtk_window_set_modal (window_g, modal_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_modal
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1modal (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)(gtk_window_get_modal(window_g));
}

/*  
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_list_toplevels  
 * Signature: ()[I
 */ 
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_Window_gtk_1window_1list_1toplevels (JNIEnv *env, jclass cls)
{
    return getGObjectHandlesFromGList(env,gtk_window_list_toplevels());
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_add_mnemonic
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1add_1mnemonic (JNIEnv *env, jclass cls, jobject window, jint keyval, jobject target) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 keyval_g = (gint32) keyval;
    GtkWidget *target_g = (GtkWidget *)getPointerFromHandle(env, target);
    gtk_window_add_mnemonic (window_g, keyval_g, target_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_remove_mnemonic
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1remove_1mnemonic (JNIEnv *env, jclass cls, jobject window, jint keyval, jobject target) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 keyval_g = (gint32) keyval;
    GtkWidget *target_g = (GtkWidget *)getPointerFromHandle(env, target);
    gtk_window_remove_mnemonic (window_g, keyval_g, target_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_mnemonic_activate
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1mnemonic_1activate (JNIEnv *env, jclass cls, jobject window, jint keyval, jint modifier) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 keyval_g = (gint32) keyval;
    GdkModifierType modifier_g = (GdkModifierType) modifier;
    return (jboolean)(gtk_window_mnemonic_activate (window_g, keyval_g, 
                                                    modifier_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_mnemonic_modifier
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1mnemonic_1modifier (JNIEnv *env, jclass cls, jobject window, jint modifier) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkModifierType modifier_g = (GdkModifierType) modifier;
    gtk_window_set_mnemonic_modifier (window_g, modifier_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_mnemonic_modifier
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1mnemonic_1modifier (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jint)(gtk_window_get_mnemonic_modifier(window_g));
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_present
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1present (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_present (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_iconify
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1iconify (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_iconify (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_deiconify
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1deiconify (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_deiconify (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_stick
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1stick (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_stick (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_unstick
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1unstick (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_unstick (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_maximize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1maximize (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_maximize (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_unmaximize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1unmaximize (JNIEnv *env, jclass cls, jobject window) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_unmaximize (window_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_begin_resize_drag
 * Signature: (IIIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1begin_1resize_1drag (JNIEnv *env, jclass cls, jobject window, jint edge, jint button, jint rootX, jint rootY, jint timestamp) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkWindowEdge edge_g = (GdkWindowEdge) edge;
    gint32 button_g = (gint32) button;
    gint32 rootX_g = (gint32) rootX;
    gint32 rootY_g = (gint32) rootY;
    time_t timestamp_g = (time_t) timestamp;
    gtk_window_begin_resize_drag (window_g, edge_g, button_g, 
                                  rootX_g, rootY_g, timestamp_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_begin_move_drag
 * Signature: (IIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1begin_1move_1drag (JNIEnv *env, jclass cls, jobject window, jint button, jint rootX, jint rootY, jint timestamp) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 button_g = (gint32) button;
    gint32 rootX_g = (gint32) rootX;
    gint32 rootY_g = (gint32) rootY;
    time_t timestamp_g = (time_t) timestamp;
    gtk_window_begin_move_drag (window_g, button_g, 
                                rootX_g, rootY_g, timestamp_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_set_default_size
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default_1size (JNIEnv *env, 
                                                                                jclass cls, jobject window, jint width, jint height) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_window_set_default_size (window_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_default_size
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1default_1size (JNIEnv *env, jclass cls, jobject window, jintArray width, jintArray height) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_window_get_default_size (window_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_resize
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1resize (JNIEnv *env, jclass cls, jobject window, jint width, jint height) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_window_resize (window_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_size
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1size (JNIEnv *env, jclass cls, jobject window, jintArray width, jintArray height) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_window_get_size (window_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_move
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1move (JNIEnv *env, jclass cls, jobject window, jint x, jint y) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gtk_window_move (window_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_get_position
 * Signature: (I[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1position (JNIEnv *env, jclass cls, jobject window, jintArray rootX, jintArray rootY) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gint *rootX_g = (gint *) (*env)->GetIntArrayElements (env, rootX, NULL);
    gint *rootY_g = (gint *) (*env)->GetIntArrayElements (env, rootY, NULL);
    gtk_window_get_position (window_g, rootX_g, rootY_g);
    (*env)->ReleaseIntArrayElements (env, rootX, (jint *) rootX_g, 0);
    (*env)->ReleaseIntArrayElements (env, rootY, (jint *) rootY_g, 0);
}

/*
 * Class:     org.gnu.gtk.Window
 * Method:    gtk_window_parse_geometry
 * Signature: (I[B)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1parse_1geometry (JNIEnv *env, jclass cls, jobject window, jstring geometry) 
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* geometry_g = (*env)->GetStringUTFChars(env, geometry, NULL);
    jboolean result_j = 
        (jboolean) (gtk_window_parse_geometry (window_g, geometry_g));
    (*env)->ReleaseStringUTFChars(env, geometry, geometry_g);
    return result_j;
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_skip_taskbar_hint
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1skip_1taskbar_1hint(JNIEnv *env, jclass cls, jobject window, jboolean hint)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_set_skip_taskbar_hint(window_g, (gboolean)hint);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_get_skip_taskbar_hint
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1skip_1taskbar_1hint(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (gboolean)gtk_window_get_skip_taskbar_hint(window_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_skip_pager_hint
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1skip_1pager_1hint(JNIEnv *env, jclass cls, jobject window, jboolean hint)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_set_skip_pager_hint(window_g, (gboolean)hint);
}
                                                                                
/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_get_skip_pager_hint
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1skip_1pager_1hint(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)gtk_window_get_skip_pager_hint(window_g);
}
                                                                               
/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_accept_focus
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1accept_1focus(JNIEnv *env, jclass cls, jobject window, jboolean value)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_set_accept_focus(window_g, (gboolean)value);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_get_accept_focus
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1accept_1focus(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)gtk_window_get_accept_focus(window_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_screen
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1screen(JNIEnv *env, jclass cls, jobject window, jobject screen)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    GdkScreen *screen_g = (GdkScreen *)getPointerFromHandle(env, screen);
    gtk_window_set_screen(window_g, screen_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_get_screen
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1screen(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandleAndRef(env, (GObject *) gtk_window_get_screen(window_g));
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_is_active
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1is_1active(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)gtk_window_is_active(window_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_has_toplevel_focus
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1has_1toplevel_1focus(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)gtk_window_has_toplevel_focus(window_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_icon_from_file
 * Signature: (ILjava/lang/String;[I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1icon_1from_1file(JNIEnv *env, jclass cls, jobject window, jstring filename)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar* name = (*env)->GetStringUTFChars(env, filename, NULL);
    GError* err;

    jboolean value = 
        (jboolean)gtk_window_set_icon_from_file(window_g, name, &err);

    (*env)->ReleaseStringUTFChars(env, filename, name);
    return value;
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_default_icon_from_file
 * Signature: (Ljava/lang/String;[I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default_1icon_1from_1file(JNIEnv *env, jclass cls, jstring filename)
{
    const gchar* name = (*env)->GetStringUTFChars(env, filename, NULL);
    GError* err;

    jboolean value = 
        (jboolean)gtk_window_set_default_icon_from_file(name, &err);

    (*env)->ReleaseStringUTFChars(env, filename, name);
    return value;
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_default_icon
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default_1icon(JNIEnv *env, jclass cls, jobject icon)
{
    GdkPixbuf * icon_g = (GdkPixbuf *)getPointerFromHandle(env, icon);
    gtk_window_set_default_icon(icon_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_auto_startup_notification
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1auto_1startup_1notification(JNIEnv *env, jclass cls, jboolean value)
{
    gtk_window_set_auto_startup_notification((gboolean)value);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_fullscreen
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1fullscreen(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_fullscreen(window_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_unfullscreen
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1unfullscreen(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_unfullscreen(window_g);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_keep_above
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1keep_1above(JNIEnv *env, jclass cls, jobject window, jboolean setting)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_set_keep_above(window_g, (gboolean)setting);
}

/*
 * Class:     org_gnu_gtk_Window
 * Method:    gtk_window_set_keep_below
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1keep_1below(JNIEnv *env, jclass cls, jobject window, jboolean setting)
{
    GtkWindow *window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gtk_window_set_keep_below(window_g, (gboolean)setting);
}

/* GTK 2.6 additions. */

JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1focus_1on_1map
(JNIEnv *env, jclass cls, jobject window, jboolean setting)
{
    GtkWindow * window_g = (GtkWindow *)getPointerFromHandle(env, window);
    gboolean setting_g = (gboolean)setting;
    gtk_window_set_focus_on_map(window_g, setting_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1focus_1on_1map
(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow * window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (jboolean)gtk_window_get_focus_on_map(window_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1icon_1name
(JNIEnv *env, jclass cls, jobject window, jstring name)
{
    GtkWindow * window_g = (GtkWindow *)getPointerFromHandle(env, window);
    const gchar * name_g = (const gchar *)(*env)->GetStringUTFChars(env, name, 0);
    gtk_window_set_icon_name(window_g, name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Window_gtk_1window_1get_1icon_1name
(JNIEnv *env, jclass cls, jobject window)
{
    GtkWindow * window_g = (GtkWindow *)getPointerFromHandle(env, window);
    return (*env)->NewStringUTF(env, gtk_window_get_icon_name(window_g));
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1set_1default_1icon_1name
(JNIEnv *env, jclass cls, jstring name)
{
    const gchar * name_g = (const gchar *)(*env)->GetStringUTFChars(env, name, 0);
    gtk_window_set_default_icon_name(name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_Window_gtk_1window_1destroy
(JNIEnv *env, jclass cls, jobject window)
{
	GtkWindow *window_g;
	window_g = getPointerFromHandle(env, window);
	(* env)->DeleteGlobalRef(env, window);
	gtk_object_destroy(GTK_OBJECT(window_g));
}

#ifdef __cplusplus
}

#endif
