/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#ifndef _Included_org_gnu_atk_StateSet
#define _Included_org_gnu_atk_StateSet
#include "org_gnu_atk_StateSet.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)atk_state_set_get_type();
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, G_OBJECT(atk_state_set_new()));
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_is_empty
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1is_1empty
  (JNIEnv *env, jclass cls, jobject set)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	return (jboolean)atk_state_set_is_empty(set_g);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_add_state
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1add_1state
  (JNIEnv *env, jclass cls, jobject set, jint type)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	return (jboolean)atk_state_set_add_state(set_g, (AtkStateType)type);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_add_states
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1add_1states
  (JNIEnv *env, jclass cls, jobject set, jintArray types)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	AtkStateType* t = (AtkStateType*)(*env)->GetIntArrayElements(env, types, NULL);
	gint size = (gint)(*env)->GetArrayLength(env, types);
	atk_state_set_add_states(set_g, t, size);
	(*env)->ReleaseIntArrayElements(env, types, (jint*)t, 0);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_clear_states
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1clear_1states
  (JNIEnv *env, jclass cls, jobject set)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	atk_state_set_clear_states(set_g);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_contains_state
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1contains_1state
  (JNIEnv *env, jclass cls, jobject set, jint type)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	return (jboolean)atk_state_set_contains_state(set_g, (AtkStateType)type);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_contains_states
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1contains_1states
  (JNIEnv *env, jclass cls, jobject set, jintArray types)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	AtkStateType* t = (AtkStateType*)(*env)->GetIntArrayElements(env, types, NULL);
	gint size = (gint)(*env)->GetArrayLength(env, types);
	jboolean value = (jboolean)atk_state_set_contains_states(set_g, t, size);
	(*env)->ReleaseIntArrayElements(env, types, (jint*)t, 0);
	return value;
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_remove_state
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1remove_1state
  (JNIEnv *env, jclass cls, jobject set, jint type)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	return (jboolean)atk_state_set_remove_state(set_g, (AtkStateType)type);
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_and_sets
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1and_1sets
  (JNIEnv *env, jclass cls, jobject set, jobject cset)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	AtkStateSet* cset_g = (AtkStateSet*)getPointerFromHandle(env, cset);
	return getGObjectHandle(env, G_OBJECT(atk_state_set_and_sets(set_g, cset_g)));
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_or_sets
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1or_1sets
  (JNIEnv *env, jclass cls, jobject set, jobject cset)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	AtkStateSet* cset_g = (AtkStateSet*)getPointerFromHandle(env, cset);
	return getGObjectHandle(env, G_OBJECT(atk_state_set_or_sets(set_g, cset_g)));
}

/*
 * Class:     org_gnu_atk_StateSet
 * Method:    atk_state_set_xor_sets
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_StateSet_atk_1state_1set_1xor_1sets
  (JNIEnv *env, jclass cls, jobject set, jobject cset)
{
	AtkStateSet* set_g = (AtkStateSet*)getPointerFromHandle(env, set);
	AtkStateSet* cset_g = (AtkStateSet*)getPointerFromHandle(env, cset);
	return getGObjectHandle(env, G_OBJECT(atk_state_set_xor_sets(set_g, cset_g)));
}

#ifdef __cplusplus
}
#endif
#endif
