/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_AboutDialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_about_dialog_get_type();
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_new
 * Signature: ()Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1new
(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, (GObject *) gtk_about_dialog_new());
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_name
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1name
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_name(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_name
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1name
(JNIEnv *env, jclass cls, jobject about, jstring name)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *name_g = (const gchar*)(*env)->GetStringUTFChars(env, name, NULL);
    gtk_about_dialog_set_name(about_g, name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_version
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1version
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_version(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_version
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1version
(JNIEnv *env, jclass cls, jobject about, jstring version)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *version_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, version, NULL);
    gtk_about_dialog_set_version(about_g, version_g);
    (*env)->ReleaseStringUTFChars(env, version, version_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_copyright
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1copyright
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_copyright(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_copyright
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1copyright
(JNIEnv *env, jclass cls, jobject about, jstring copyright)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *copyright_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, copyright, NULL);
    gtk_about_dialog_set_copyright(about_g, copyright_g);
    (*env)->ReleaseStringUTFChars(env, copyright, copyright_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_comments
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1comments
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_comments(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_comments
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1comments
(JNIEnv *env, jclass cls, jobject about, jstring comments)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *comments_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, comments, NULL);
    gtk_about_dialog_set_comments(about_g, comments_g);
    (*env)->ReleaseStringUTFChars(env, comments, comments_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_license
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1license
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_license(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_license
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1license
(JNIEnv *env, jclass cls, jobject about, jstring license)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *license_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, license, NULL);
    gtk_about_dialog_set_license(about_g, license_g);
    (*env)->ReleaseStringUTFChars(env, license, license_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_wrap_license
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1wrap_1license
  (JNIEnv *env, jclass cls, jobject about)
{
	GtkAboutDialog* aboutDialog;
	gboolean wrap;
	
	aboutDialog = (GtkAboutDialog*)getPointerFromHandle(env, about);
	wrap = gtk_about_dialog_get_wrap_license(aboutDialog);
	
	return wrap;
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_wrap_license
 * Signature: (Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1wrap_1license
  (JNIEnv *env, jclass cls, jobject about, jboolean wrap)
{
	GtkAboutDialog* aboutDialog;
	
	aboutDialog = (GtkAboutDialog*)getPointerFromHandle(env, about);
	gtk_about_dialog_set_wrap_license(aboutDialog, wrap);
	
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_website
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1website
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_website(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_website
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1website
(JNIEnv *env, jclass cls, jobject about, jstring website)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *website_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, website, NULL);
    gtk_about_dialog_set_website(about_g, website_g);
    (*env)->ReleaseStringUTFChars(env, website, website_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_website_label
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1website_1label
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_website_label(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_website_label
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1website_1label
(JNIEnv *env, jclass cls, jobject about, jstring website_label)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *website_label_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, website_label, NULL);
    gtk_about_dialog_set_website_label(about_g, website_label_g);
    (*env)->ReleaseStringUTFChars(env, website_label, website_label_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_authors
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1authors
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return getJavaStringArray(env, gtk_about_dialog_get_authors(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_authors
 * Signature: (Lorg/gnu/glib/Handle;[Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1authors
(JNIEnv *env, jclass cls, jobject about, jobjectArray authors)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    gchar **authors_g = getStringArray(env, authors);
    gtk_about_dialog_set_authors(about_g, (const gchar**)authors_g);
    freeStringArray(env, authors, authors_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_documenters
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1documenters
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return getJavaStringArray(env, gtk_about_dialog_get_documenters(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_documenters
 * Signature: (Lorg/gnu/glib/Handle;[Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1documenters
(JNIEnv *env, jclass cls, jobject about, jobjectArray documenters)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    gchar **documenters_g = getStringArray(env, documenters);
    gtk_about_dialog_set_documenters(about_g, (const gchar**)documenters_g);
    freeStringArray(env, documenters, documenters_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_artists
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1artists
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return getJavaStringArray(env, gtk_about_dialog_get_artists(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_artists
 * Signature: (Lorg/gnu/glib/Handle;[Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1artists
(JNIEnv *env, jclass cls, jobject about, jobjectArray artists)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    gchar **artists_g = getStringArray(env, artists);
    gtk_about_dialog_set_artists(about_g, (const gchar**)artists_g);
    freeStringArray(env, artists, artists_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_translator_credits
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1translator_1credits
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_translator_credits(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_translator_credits
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1translator_1credits
(JNIEnv *env, jclass cls, jobject about, jstring translator_credits)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *translator_credits_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, translator_credits, NULL);
    gtk_about_dialog_set_translator_credits(about_g, translator_credits_g);
    (*env)->ReleaseStringUTFChars(env, translator_credits, translator_credits_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_logo
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1logo
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return getGObjectHandle(env, (GObject *) gtk_about_dialog_get_logo(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_logo
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1logo
(JNIEnv *env, jclass cls, jobject about, jobject logo)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    GdkPixbuf *logo_g = (GdkPixbuf*)getPointerFromHandle(env, logo);
    gtk_about_dialog_set_logo(about_g, logo_g);
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_get_logo_icon_name
 * Signature: (Lorg/gnu/glib/Handle;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1get_1logo_1icon_1name
(JNIEnv *env, jclass cls, jobject about)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    return (*env)->NewStringUTF(env, gtk_about_dialog_get_logo_icon_name(about_g));
}

/*
 * Class:     org_gnu_gtk_AboutDialog
 * Method:    gtk_about_dialog_set_logo_icon_name
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1logo_1icon_1name
(JNIEnv *env, jclass cls, jobject about, jstring icon_name)
{
    GtkAboutDialog *about_g = 
        (GtkAboutDialog*)getPointerFromHandle(env, about);
    const gchar *icon_name_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, icon_name, NULL);
    gtk_about_dialog_set_logo_icon_name(about_g, icon_name_g);
    (*env)->ReleaseStringUTFChars(env, icon_name, icon_name_g);
}

static void aboutDialogActivateLinkFunc(GtkAboutDialog *about, 
                                        const gchar *link, gpointer data) {
    JGFuncCallbackRef *ref = (JGFuncCallbackRef*) data;
    (* ref->env)->CallStaticVoidMethod(ref->env, 
                                       ref->cls, 
                                       ref->methodID,
                                       getGObjectHandle(ref->env, (GObject*)about),
                                       (*ref->env)->NewStringUTF(ref->env, link));
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1email_1hook
(JNIEnv *env, jclass cls, jobject about, jstring callback)
{
    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->cls = cls;
    //    ref->obj = (* env)->NewGlobalRef(env, about);

    const char *funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    const char *sig = "(Lorg/gnu/glib/Handle;Ljava/lang/String;)V";
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetStaticMethodID(env, cls,
                                  funcname, sig );
    //(*env)->GetObjectClass(env, ref->obj), 
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, callback, funcname);

    gtk_about_dialog_set_email_hook(aboutDialogActivateLinkFunc, ref, NULL);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_AboutDialog_gtk_1about_1dialog_1set_1url_1hook
(JNIEnv *env, jclass cls, jobject about, jstring callback)
{
    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->cls = cls;
    //ref->obj = (* env)->NewGlobalRef(env, about);

    const char *funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    const char *sig = "(Lorg/gnu/glib/Handle;Ljava/lang/String;)V";
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetStaticMethodID(env, 
                                  cls,
                                  funcname, sig );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, callback, funcname);

    gtk_about_dialog_set_url_hook(aboutDialogActivateLinkFunc, ref, NULL);
}

#ifdef __cplusplus
}
#endif
