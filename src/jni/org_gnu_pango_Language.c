/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Language.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Language
 * Method:    pango_language_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Language_pango_1language_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)pango_language_get_type ();
}

/*
 * Class:     org.gnu.pango.Language
 * Method:    pango_language_from_string
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Language_pango_1language_1from_1string (JNIEnv *env, 
    jclass cls, jstring language) 
{
    const gchar* language_g = (*env)->GetStringUTFChars(env, language, 0);
    jobject result = 
        getGBoxedHandle(env, pango_language_from_string (language_g),
                        PANGO_TYPE_LANGUAGE, NULL, NULL);
    (*env)->ReleaseStringUTFChars(env, language, language_g);
    return result;
}

/*
 * Class:     org.gnu.pango.Language
 * Method:    pango_language_to_string
 */
JNIEXPORT jstring JNICALL Java_org_gnu_pango_Language_pango_1language_1to_1string (JNIEnv 
    *env, jclass cls, jobject language) 
{
    gchar *result_g = (gchar *)pango_language_to_string (
    		(PangoLanguage *)getPointerFromHandle(env, language));
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.pango.Language
 * Method:    pango_language_matches
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_Language_pango_1language_1matches (JNIEnv *env, 
    jclass cls, jobject language, jstring rangeList) 
{
    const gchar* rangeList_g = (*env)->GetStringUTFChars(env, rangeList, 0);
    jboolean result_j = (jboolean) (pango_language_matches (
    		(PangoLanguage *)getPointerFromHandle(env, language), rangeList_g));
    (*env)->ReleaseStringUTFChars(env, rangeList, rangeList_g);
    return result_j;
}


#ifdef __cplusplus
}

#endif
