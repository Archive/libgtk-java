/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Window.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1new (JNIEnv *env, jclass cls, jobject 
    parent, jobject attriutes, jint attributesMask) 
{
    GdkWindow *parent_g = (GdkWindow *)getPointerFromHandle(env, parent);
    GdkWindowAttr *attriutes_g = (GdkWindowAttr *)getPointerFromHandle(env, attriutes);
    gint32 attributesMask_g = (gint32) attributesMask;
    return getGObjectHandle(env, 
                            (GObject *)gdk_window_new (parent_g, 
                                                       attriutes_g, 
                                                       attributesMask_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_window_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1window_1type (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return (jint) (gdk_window_get_window_type (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1pointer (JNIEnv *env, 
    jclass cls, jobject window, jintArray x, jintArray y, jintArray mod_type) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
	GdkModifierType *modType_g = (GdkModifierType *) (*env)->GetIntArrayElements (env, mod_type, NULL);
	GdkWindow* result = gdk_window_get_pointer(window_g, x_g, y_g, modType_g);
	(*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
    (*env)->ReleaseIntArrayElements (env, mod_type, (jint *) modType_g, 0);
	
	if(result == NULL)
		return NULL;
		
	return getGObjectHandleAndRef(env, (GObject *)result);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_destroy
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1destroy (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_destroy (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_at_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1at_1pointer (JNIEnv *env, jclass cls) 
{
    gint x;
    gint y;
    return getGObjectHandleAndRef(env, (GObject *)gdk_window_at_pointer(&x, &y));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_show
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1show (JNIEnv *env, jclass cls, jobject 
    window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_show (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_hide
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1hide (JNIEnv *env, jclass cls, jobject 
    window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_hide (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_withdraw
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1withdraw (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_withdraw (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_show_unraised
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1show_1unraised (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_show_unraised (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_move
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1move (JNIEnv *env, jclass cls, jobject 
    window, jint x, jint y) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gdk_window_move (window_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_resize
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1resize (JNIEnv *env, jclass cls, 
    jobject window, jint width, jint height) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gdk_window_resize (window_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_move_resize
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1move_1resize (JNIEnv *env, jclass 
    cls, jobject window, jint x, jint y, jint width, jint height) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gdk_window_move_resize (window_g, x_g, y_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_reparent
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1reparent (JNIEnv *env, jclass cls, 
    jobject window, jobject newParent, jint x, jint y) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWindow *newParent_g = (GdkWindow *)getPointerFromHandle(env, newParent);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gdk_window_reparent (window_g, newParent_g, x_g, y_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_clear
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1clear (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_clear (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_clear_area
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1clear_1area (JNIEnv *env, jclass 
    cls, jobject window, jint x, jint y, jint width, jint height) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gdk_window_clear_area (window_g, x_g, y_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_clear_area_e
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1clear_1area_1e (JNIEnv *env, jclass 
    cls, jobject window, jint x, jint y, jint width, jint height) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gdk_window_clear_area_e (window_g, x_g, y_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_raise
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1raise (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_raise (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_lower
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1lower (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_lower (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_focus
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1focus (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_focus (window_g, GDK_CURRENT_TIME);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_user_data
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1user_1data (JNIEnv *env, 
    jclass cls, jobject window, jobject userData) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gpointer *userData_g = (gpointer *)userData;
    gdk_window_set_user_data (window_g, userData_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_override_redirect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1override_1redirect (JNIEnv 
    *env, jclass cls, jobject window, jboolean overrideRedirect) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gboolean overrideRedirect_g = (gboolean) overrideRedirect;
    gdk_window_set_override_redirect (window_g, overrideRedirect_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_scroll
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1scroll (JNIEnv *env, jclass cls, 
    jobject window, jint dx, jint dy) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 dx_g = (gint32) dx;
    gint32 dy_g = (gint32) dy;
    gdk_window_scroll (window_g, dx_g, dy_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_shape_combine_mask
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1shape_1combine_1mask (JNIEnv *env, 
    jclass cls, jobject window, jobject shape_mask, jint offsetX, jint offsetY) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkBitmap *shape_mask_g = (GdkBitmap *)getPointerFromHandle(env, shape_mask);
    gint32 offsetX_g = (gint32) offsetX;
    gint32 offsetY_g = (gint32) offsetY;
    gdk_window_shape_combine_mask (window_g, shape_mask_g, offsetX_g, offsetY_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_shape_combine_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1shape_1combine_1region (JNIEnv 
    *env, jclass cls, jobject window, jobject shape_region, jint offsetX, jint offsetY) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRegion *shape_region_g = (GdkRegion *)getPointerFromHandle(env, shape_region);
    gint32 offsetX_g = (gint32) offsetX;
    gint32 offsetY_g = (gint32) offsetY;
    gdk_window_shape_combine_region (window_g, shape_region_g, offsetX_g, offsetY_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_child_shapes
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1child_1shapes (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_set_child_shapes (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_merge_child_shapes
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1merge_1child_1shapes (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_merge_child_shapes (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_is_visible
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Window_gdk_1window_1is_1visible (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return (jboolean) (gdk_window_is_visible (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_is_viewable
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Window_gdk_1window_1is_1viewable (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return (jboolean) (gdk_window_is_viewable (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_state
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1state (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return (jint) (gdk_window_get_state (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_static_gravities
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1static_1gravities (JNIEnv 
    *env, jclass cls, jobject window, jboolean useStatic) 
{
    GdkWindow *window_g = (GdkWindow *)window;
    gboolean useStatic_g = (gboolean) useStatic;
    return (jboolean) (gdk_window_set_static_gravities (window_g, useStatic_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_foreign_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1foreign_1new (JNIEnv *env, jclass 
    cls, jint anid) 
{
    GdkNativeWindow anid_g = (GdkNativeWindow)anid;
    return getGObjectHandle(env, (GObject *)gdk_window_foreign_new (anid_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_type_hint
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1type_1hint (JNIEnv *env, 
    jclass cls, jobject window, jint hint) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWindowTypeHint hint_g = (GdkWindowTypeHint) hint;
    gdk_window_set_type_hint (window_g, hint_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_modal_hint
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1modal_1hint (JNIEnv *env, 
    jclass cls, jobject window, jboolean modal) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gboolean modal_g = (gboolean) modal;
    gdk_window_set_modal_hint (window_g, modal_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_geometry_hints
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1geometry_1hints (JNIEnv *env, 
    jclass cls, jobject window, jobject geometry, jint flags) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkGeometry *geometry_g = (GdkGeometry *)getPointerFromHandle(env, geometry);
    GdkWindowHints flags_g = (GdkWindowHints) flags;
    gdk_window_set_geometry_hints (window_g, geometry_g, flags_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_begin_paint_rect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1begin_1paint_1rect (JNIEnv *env, 
    jclass cls, jobject window, jobject rectangle) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRectangle *rectangle_g = (GdkRectangle *)getPointerFromHandle(env, rectangle);
    gdk_window_begin_paint_rect (window_g, rectangle_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_begin_paint_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1begin_1paint_1region (JNIEnv *env, 
    jclass cls, jobject window, jobject region) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gdk_window_begin_paint_region (window_g, region_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_end_paint
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1end_1paint (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_end_paint (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_title
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1title (JNIEnv *env, jclass 
    cls, jobject window, jstring title) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, 0);
    gdk_window_set_title (window_g, title_g);
    (*env)->ReleaseStringUTFChars(env, title, title_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_role
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1role (JNIEnv *env, jclass cls, 
    jobject window, jstring role) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    const gchar* role_g = (*env)->GetStringUTFChars(env, role, 0);
    gdk_window_set_role (window_g, role_g);
    (*env)->ReleaseStringUTFChars(env, role, role_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_transient_for
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1transient_1for (JNIEnv *env, 
    jclass cls, jobject Window, jobject leader) 
{
    GdkWindow *Window_g = (GdkWindow *)getPointerFromHandle(env, Window);
    GdkWindow *leader_g = (GdkWindow *)getPointerFromHandle(env, leader);
    gdk_window_set_transient_for (Window_g, leader_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_background
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1background (JNIEnv *env, 
    jclass cls, jobject window, jobject color) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkColor *color_g = (GdkColor *)getPointerFromHandle(env, color);
    gdk_window_set_background (window_g, color_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_back_pixmap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1back_1pixmap (JNIEnv *env, 
    jclass cls, jobject window, jobject pixmap, jboolean parentRelative) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkPixmap *pixmap_g = (GdkPixmap *)getPointerFromHandle(env, pixmap);
    gboolean parentRelative_g = (gboolean) parentRelative;
    gdk_window_set_back_pixmap (window_g, pixmap_g, parentRelative_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_cursor
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1cursor (JNIEnv *env, jclass 
    cls, jobject window, jobject cursor) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkCursor *cursor_g = (GdkCursor *)getPointerFromHandle(env, cursor);
    gdk_window_set_cursor (window_g, cursor_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_geometry
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1geometry (JNIEnv *env, jclass 
    cls, jobject window, jintArray x, jintArray y, jintArray width, jintArray height, jintArray 
    depth) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gint *depth_g = (gint *) (*env)->GetIntArrayElements (env, depth, NULL);
    gdk_window_get_geometry (window_g, x_g, y_g, width_g, height_g, depth_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
    (*env)->ReleaseIntArrayElements (env, depth, (jint *) depth_g, 0);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_position
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1position (JNIEnv *env, jclass 
    cls, jobject window, jintArray x, jintArray y) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gdk_window_get_position (window_g, x_g, y_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_origin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1origin (JNIEnv *env, jclass 
    cls, jobject window, jintArray x, jintArray y) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    jint result_j = (jint) (gdk_window_get_origin (window_g, x_g, y_g));
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_root_origin
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1root_1origin (JNIEnv *env, 
    jclass cls, jobject window, jintArray x, jintArray y) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint *x_g = (gint *) (*env)->GetIntArrayElements (env, x, NULL);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gdk_window_get_root_origin (window_g, x_g, y_g);
    (*env)->ReleaseIntArrayElements (env, x, (jint *) x_g, 0);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_frame_extents
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1frame_1extents
		(JNIEnv *env, jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRectangle *rect = g_new(GdkRectangle, 1);
    gdk_window_get_frame_extents (window_g, rect);
    return getGBoxedHandle(env, rect, GDK_TYPE_RECTANGLE, NULL,
    		(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_parent
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1parent (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandle(env, (GObject *)gdk_window_get_parent (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_toplevel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1toplevel (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandle(env, (GObject *)gdk_window_get_toplevel (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_children
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1children (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandlesFromGList(env, gdk_window_get_children (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_peek_children
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Window_gdk_1window_1peek_1children (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getGObjectHandlesFromGList(env, gdk_window_peek_children (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_events
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1events (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return (jint) (gdk_window_get_events (window_g));
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_events
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1events (JNIEnv *env, jclass 
    cls, jobject window, jint eventMask) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkEventMask eventMask_g = (GdkEventMask) eventMask;
    gdk_window_set_events (window_g, eventMask_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_icon_list
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1icon_1list (JNIEnv *env, 
    jclass cls, jobject window, jobjectArray pixbufs) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GList *pixbufs_g = (GList *)getGListFromHandles(env, pixbufs);
    gdk_window_set_icon_list (window_g, pixbufs_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_icon
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1icon (JNIEnv *env, jclass cls, 
    jobject window, jobject iconWindow, jobject pixmap, jobject mask) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWindow *iconWindow_g = (GdkWindow *)getPointerFromHandle(env, iconWindow);
    GdkPixmap *pixmap_g = (GdkPixmap *)getPointerFromHandle(env, pixmap);
    GdkBitmap *mask_g = (GdkBitmap *)getPointerFromHandle(env, mask);
    gdk_window_set_icon (window_g, iconWindow_g, pixmap_g, mask_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_icon_name
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1icon_1name (JNIEnv *env, 
    jclass cls, jobject window, jstring name) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    const gchar* name_g = (*env)->GetStringUTFChars(env, name, 0);
    gdk_window_set_icon_name (window_g, name_g);
    (*env)->ReleaseStringUTFChars(env, name, name_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1group (JNIEnv *env, jclass 
    cls, jobject window, jobject leader) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWindow *leader_g = (GdkWindow *)getPointerFromHandle(env, leader);
    gdk_window_set_group (window_g, leader_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_decorations
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1decorations (JNIEnv *env, 
    jclass cls, jobject window, jint decorations) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWMDecoration decorations_g = (GdkWMDecoration) decorations;
    gdk_window_set_decorations (window_g, decorations_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_functions
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1functions (JNIEnv *env, jclass 
    cls, jobject window, jint functions) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWMFunction functions_g = (GdkWMFunction) functions;
    gdk_window_set_functions (window_g, functions_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_toplevels
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1toplevels (JNIEnv *env, jclass 
    cls) 
{
    GList *list = gdk_window_get_toplevels();
    jobjectArray ret = getGObjectHandlesFromGList( env, list );
    g_list_free( list );
    return ret;
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_iconify
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1iconify (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_iconify (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_deiconify
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1deiconify (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_deiconify (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_stick
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1stick (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_stick (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_unstick
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1unstick (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_unstick (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_maximize
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1maximize (JNIEnv *env, jclass cls, 
    jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_maximize (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_unmaximize
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1unmaximize (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_unmaximize (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_register_dnd
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1register_1dnd (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_register_dnd (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_begin_resize_drag
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1begin_1resize_1drag (JNIEnv *env, 
    jclass cls, jobject window, jint edge, jint button, jint rootX, jint rootY, jint timestamp) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkWindowEdge edge_g = (GdkWindowEdge) edge;
    gint32 button_g = (gint32) button;
    gint32 rootX_g = (gint32) rootX;
    gint32 rootY_g = (gint32) rootY;
    gint32 timestamp_g = (gint32) timestamp;
    gdk_window_begin_resize_drag (window_g, edge_g, button_g, rootX_g, rootY_g, timestamp_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_begin_move_drag
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1begin_1move_1drag (JNIEnv *env, 
    jclass cls, jobject window, jint button, jint rootX, jint rootY, jint timestamp) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gint32 button_g = (gint32) button;
    gint32 rootX_g = (gint32) rootX;
    gint32 rootY_g = (gint32) rootY;
    gint32 timestamp_g = (gint32) timestamp;
    gdk_window_begin_move_drag (window_g, button_g, rootX_g, rootY_g, timestamp_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_invalidate_rect
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1invalidate_1rect (JNIEnv *env, 
    jclass cls, jobject window, jobject rect, jboolean invalidateChildren) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRectangle *rect_g = (GdkRectangle *)getPointerFromHandle(env, rect);
    gboolean invalidateChildren_g = (gboolean) invalidateChildren;
    gdk_window_invalidate_rect (window_g, rect_g, invalidateChildren_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_invalidate_region
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1invalidate_1region (JNIEnv *env, 
    jclass cls, jobject window, jobject region, jboolean invalidateChildren) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkRegion *region_g = (GdkRegion *)getPointerFromHandle(env, region);
    gboolean invalidateChildren_g = (gboolean) invalidateChildren;
    gdk_window_invalidate_region (window_g, region_g, invalidateChildren_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_update_area
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1update_1area (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    return getStructHandle(env, gdk_window_get_update_area (window_g), NULL, (JGFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_freeze_updates
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1freeze_1updates (JNIEnv *env, 
    jclass cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_freeze_updates (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_thaw_updates
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1thaw_1updates (JNIEnv *env, jclass 
    cls, jobject window) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gdk_window_thaw_updates (window_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_process_all_updates
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1process_1all_1updates (JNIEnv *env, 
    jclass cls) 
{
    gdk_window_process_all_updates ();
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_process_updates
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1process_1updates (JNIEnv *env, 
    jclass cls, jobject window, jboolean updateChildren) 
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gboolean updateChildren_g = (gboolean) updateChildren;
    gdk_window_process_updates (window_g, updateChildren_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_set_debug_updates
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1set_1debug_1updates (JNIEnv *env, 
    jclass cls, jboolean setting) 
{
    gboolean setting_g = (gboolean) setting;
    gdk_window_set_debug_updates (setting_g);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_constrain_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1constrain_1size (JNIEnv *env, 
    jclass cls, jobject geometry, jint flags, jint width, jint height, jintArray newWidth, 
    jintArray newHeight) 
{
    GdkGeometry *geometry_g = (GdkGeometry *)getPointerFromHandle(env, geometry);
    gint32 flags_g = (gint32) flags;
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gint *newWidth_g = (gint *) (*env)->GetIntArrayElements (env, newWidth, NULL);
    gint *newHeight_g = (gint *) (*env)->GetIntArrayElements (env, newHeight, NULL);
    gdk_window_constrain_size (geometry_g, flags_g, width_g, height_g, newWidth_g, 
            newHeight_g);
    (*env)->ReleaseIntArrayElements (env, newWidth, (jint *) newWidth_g, 0);
    (*env)->ReleaseIntArrayElements (env, newHeight, (jint *) newHeight_g, 0);
}

/*
 * Class:     org.gnu.gdk.Window
 * Method:    gdk_window_get_internal_paint_info
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1window_1get_1internal_1paint_1info (JNIEnv 
    *env, jclass cls, jobject window, jobject realDrawable, jintArray xOffset, jintArray yOffset)
{
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GdkDrawable **realDrawable_g = (GdkDrawable**)getPointerFromHandle(env, realDrawable);
    gint *xOffset_g = (gint *) (*env)->GetIntArrayElements (env, xOffset, NULL);
    gint *yOffset_g = (gint *) (*env)->GetIntArrayElements (env, yOffset, NULL);
    gdk_window_get_internal_paint_info (window_g, realDrawable_g, xOffset_g, yOffset_g);
    (*env)->ReleaseIntArrayElements (env, xOffset, (jint *) xOffset_g, 0);
    (*env)->ReleaseIntArrayElements (env, yOffset, (jint *) yOffset_g, 0);
}

JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1pointer_1grab
(JNIEnv *env, jclass cls, jobject window, jboolean owner_events, jint event_mask, jobject confine_to, jobject cursor, jint time_)
{
    GdkWindow * window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gboolean owner_events_g = (gboolean)owner_events;
    GdkEventMask event_mask_g = (GdkEventMask)event_mask;
    GdkWindow * confine_to_g = (GdkWindow *)getPointerFromHandle(env, confine_to);
    GdkCursor * cursor_g = (GdkCursor *)getPointerFromHandle(env, cursor);
    guint32 time__g = (guint32)time_;
    return (jint)gdk_pointer_grab(window_g, owner_events_g, event_mask_g, confine_to_g, cursor_g, time__g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gdk_Window_gdk_1keyboard_1grab
(JNIEnv *env, jclass cls, jobject window, jboolean owner_events, jint time_)
{
    GdkWindow * window_g = (GdkWindow *)getPointerFromHandle(env, window);
    gboolean owner_events_g = (gboolean)owner_events;
    guint32 time__g = (guint32)time_;
    return (jint)gdk_keyboard_grab(window_g, owner_events_g, time__g);
}

JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1pointer_1ungrab
(JNIEnv *env, jclass cls, jint time_)
{
    guint32 time__g = (guint32)time_;
    gdk_pointer_ungrab(time__g);
}

JNIEXPORT void JNICALL Java_org_gnu_gdk_Window_gdk_1keyboard_1ungrab
(JNIEnv *env, jclass cls, jint time_)
{
    guint32 time__g = (guint32)time_;
    gdk_keyboard_ungrab(time__g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Window_gdk_1pointer_1is_1grabbed
(JNIEnv *env, jclass cls)
{
    return (jboolean)gdk_pointer_is_grabbed();
}


#ifdef __cplusplus
}

#endif
