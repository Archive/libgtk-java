/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Color.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Color
 * Method:    getRed
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Color_getRed 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoColor *obj_g = (PangoColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->red;
}

/*
 * Class:     org.gnu.pango.Color
 * Method:    getGreen
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Color_getGreen 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoColor *obj_g = (PangoColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->green;
}


/*
 * Class:     org.gnu.pango.Color
 * Method:    getBlue
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Color_getBlue 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoColor *obj_g = (PangoColor *)getPointerFromHandle(env, obj);
    return (jint)obj_g->blue;
}

/*
 * Class:     org.gnu.pango.Color
 * Method:    pango_color_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Color_pango_1color_1get_1type 
  (JNIEnv *env, jclass cls) 
{
    return (jint)pango_color_get_type ();
}

/*
 * Class:     org.gnu.pango.Color
 * Method:    pango_color_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Color_pango_1color_1copy 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoColor *obj_g = (PangoColor *)getPointerFromHandle(env, obj);
    return getGBoxedHandle(env, pango_color_copy (obj_g), 
                           PANGO_TYPE_COLOR,
                           NULL, (JGFreeFunc)pango_color_free);
}

/*
 * Class:     org.gnu.pango.Color
 * Method:    pango_color_parse
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_Color_pango_1color_1parse 
  (JNIEnv *env, jclass cls, jobject color, jstring spec) 
{
    gchar* spec_g = (gchar*)(*env)->GetStringUTFChars(env, spec, 0);
    jboolean result_j = 
        (jboolean) pango_color_parse( (PangoColor*)
                                      getPointerFromHandle(env, color),
                                      spec_g );
    (*env)->ReleaseStringUTFChars(env, spec, spec_g);
    return result_j;
}

/*
 * Class:     org.gnu.pango.Color
 * Method:    pango_color_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Color_pango_1color_1new
  (JNIEnv *env, jclass cls) 
{
    PangoColor *c = g_new( PangoColor, 1 );
    return getGBoxedHandle(env, c, PANGO_TYPE_COLOR, NULL, (JGFreeFunc)g_free);
}

#ifdef __cplusplus
}

#endif
