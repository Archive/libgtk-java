/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrList.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1get_1type
  (JNIEnv *env, jclass cls) 
{
    return (jint)pango_attr_list_get_type ();
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1new
  (JNIEnv *env, jclass cls) 
{
    return getGBoxedHandle(env, pango_attr_list_new (), 
                           PANGO_TYPE_ATTR_LIST,
                           NULL, (JGFreeFunc)pango_attr_list_unref);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_ref
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1ref
  (JNIEnv *env, jclass cls, jobject list) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    pango_attr_list_ref (list_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_unref
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1unref 
  (JNIEnv *env, jclass cls, jobject list) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    pango_attr_list_unref (list_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1copy 
  (JNIEnv *env, jclass cls, jobject list) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    return getGBoxedHandle(env, pango_attr_list_copy (list_g), 
                           PANGO_TYPE_ATTR_LIST, 
                           NULL, (JGFreeFunc)pango_attr_list_unref);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_insert
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1insert 
  (JNIEnv *env, jclass cls, jobject list, jobject attr) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    PangoAttribute *attr_g = (PangoAttribute *)getPointerFromHandle(env, attr);
    pango_attr_list_insert (list_g, attr_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_insert_before
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1insert_1before 
  (JNIEnv *env, jclass cls, jobject list, jobject attr) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    PangoAttribute *attr_g = (PangoAttribute *)getPointerFromHandle(env, attr);
    pango_attr_list_insert_before (list_g, attr_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_change
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1change 
  (JNIEnv *env, jclass cls, jobject list, jobject attr) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    PangoAttribute *attr_g = (PangoAttribute *)getPointerFromHandle(env, attr);
    pango_attr_list_change (list_g, attr_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_splice
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1splice 
  (JNIEnv *env, jclass cls, jobject list, jobject other, jint pos, jint len) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    PangoAttrList *other_g = (PangoAttrList *)getPointerFromHandle(env, other);
    gint32 pos_g = (gint32) pos;
    gint32 len_g = (gint32) len;
    pango_attr_list_splice (list_g, other_g, pos_g, len_g);
}

/*
 * Class:     org.gnu.pango.AttrList
 * Method:    pango_attr_list_get_iterator
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrList_pango_1attr_1list_1get_1iterator 
  (JNIEnv *env, jclass cls, jobject list) 
{
    PangoAttrList *list_g = (PangoAttrList *)getPointerFromHandle(env, list);
    return getStructHandle(env, pango_attr_list_get_iterator (list_g),
                           NULL, (JGFreeFunc)pango_attr_iterator_destroy);
}

#ifdef __cplusplus
}

#endif
