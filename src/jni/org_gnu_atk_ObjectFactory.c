/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#ifndef _Included_org_gnu_atk_ObjectFactory
#define _Included_org_gnu_atk_ObjectFactory
#include "org_gnu_atk_ObjectFactory.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_atk_ObjectFactory
 * Method:    atk_object_factory_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_ObjectFactory_atk_1object_1factory_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)atk_object_factory_get_type();
}

/*
 * Class:     org_gnu_atk_ObjectFactory
 * Method:    atk_object_factory_create_accessible
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_ObjectFactory_atk_1object_1factory_1create_1accessible
  (JNIEnv *env, jclass cls, jobject factory, jobject object)
{
	AtkObjectFactory* factory_g = (AtkObjectFactory*)getPointerFromHandle(env, factory);
	GObject* object_g = (GObject*)getPointerFromHandle(env, object);
	return getGObjectHandle(env, G_OBJECT(atk_object_factory_create_accessible(factory_g, object_g)));
}

/*
 * Class:     org_gnu_atk_ObjectFactory
 * Method:    atk_object_factory_invalidate
 */
JNIEXPORT void JNICALL Java_org_gnu_atk_ObjectFactory_atk_1object_1factory_1invalidate
  (JNIEnv *env, jclass cls, jobject factory)
{
	AtkObjectFactory* factory_g = (AtkObjectFactory*)getPointerFromHandle(env, factory);
	atk_object_factory_invalidate(factory_g);
}

/*
 * Class:     org_gnu_atk_ObjectFactory
 * Method:    atk_object_factory_get_accessible_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_ObjectFactory_atk_1object_1factory_1get_1accessible_1type
  (JNIEnv *env, jclass cls, jobject factory)
{
	AtkObjectFactory* factory_g = (AtkObjectFactory*)getPointerFromHandle(env, factory);
	return (jint)atk_object_factory_get_accessible_type(factory_g);
}

#ifdef __cplusplus
}
#endif
#endif
