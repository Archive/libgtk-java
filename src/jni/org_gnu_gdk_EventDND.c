/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_EventDND.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getWindow
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventDND_getWindow (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->window);
}

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getSendEvent
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_EventDND_getSendEvent (JNIEnv *env, jclass cls, 
    jobject obj) 
{
    GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return (jboolean) obj_g->send_event;
}

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getContext
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_EventDND_getContext (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, (GObject *)obj_g->context);
}

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getTime
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_EventDND_getTime (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return (jint) obj_g->time;
}

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getXRoot
 */
JNIEXPORT jshort JNICALL Java_org_gnu_gdk_EventDND_getXRoot (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return (jshort) obj_g->x_root;
}

/*
 * Class:     org.gnu.gdk.EventDND
 * Method:    getYRoot
 */
JNIEXPORT jshort JNICALL Java_org_gnu_gdk_EventDND_getYRoot (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkEventDND *obj_g = (GdkEventDND *)getPointerFromHandle(env, obj);
    return (jshort) obj_g->y_root;
}


#ifdef __cplusplus
}

#endif
