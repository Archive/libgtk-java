/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 * Generation date: 2006-04-20 14:16:27 EDT
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeRowReference.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_TreeRowReference
 * Method:    gtk_tree_row_reference_new
 * Signature: (II)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeRowReference_gtk_1tree_1row_1reference_1new
		(JNIEnv *env, jclass cls, jobject model, jobject path)
{
 	GtkTreeModel* model_g = (GtkTreeModel*) getPointerFromHandle(env, model);
 	GtkTreePath* path_g = (GtkTreePath*) getPointerFromHandle(env, path);
 	
 	GtkTreeRowReference* ref_g = gtk_tree_row_reference_new(model_g, path_g);
 	
 	return getGBoxedHandle(env, ref_g, GTK_TYPE_TREE_ROW_REFERENCE, 
 		(GBoxedCopyFunc) NULL, 
 		(GBoxedFreeFunc) gtk_tree_row_reference_free);
}

/*
 * Class:     org_gnu_gtk_TreeRowReference
 * Method:    gtk_tree_row_reference_get_model
 * Signature: (II)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeRowReference_gtk_1tree_1row_1reference_1get_1model
		(JNIEnv *env, jclass cls, jobject reference)
{
	GtkTreeRowReference* ref_g = (GtkTreeRowReference*) getPointerFromHandle(env, reference);
	
	GtkTreeModel* model_g = gtk_tree_row_reference_get_model(ref_g);
	
	return getGObjectHandle(env, (GObject*) model_g);
}

/*
 * Class:     org_gnu_gtk_TreeRowReference
 * Method:    gtk_tree_row_reference_get_path
 * Signature: (II)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeRowReference_gtk_1tree_1row_1reference_1get_1path
		(JNIEnv *env, jclass cls, jobject reference)
{
 	GtkTreeRowReference* ref_g = (GtkTreeRowReference*) getPointerFromHandle(env, reference);
 	
 	GtkTreePath* path_g = gtk_tree_row_reference_get_path(ref_g);
 	
 	return getGBoxedHandle(env, path_g, GTK_TYPE_TREE_PATH, NULL,
 		(GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_TreeRowReference
 * Method:    gtk_tree_row_reference_valid
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeRowReference_gtk_1tree_1row_1reference_1valid
		(JNIEnv *env, jclass cls, jobject reference)
{
 	GtkTreeRowReference* ref_g = (GtkTreeRowReference*) getPointerFromHandle(env, reference);
 	return gtk_tree_row_reference_valid(ref_g);
}

#ifdef __cplusplus
}

#endif
