/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <atk/atk.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#ifndef _Included_org_gnu_atk_Hyperlink
#define _Included_org_gnu_atk_Hyperlink
#include "org_gnu_atk_Hyperlink.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)atk_hyperlink_get_type();
}


/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_uri
 */
JNIEXPORT jstring JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1uri
  (JNIEnv *env, jclass cls, jobject link, jint i)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (*env)->NewStringUTF(env, atk_hyperlink_get_uri(link_g, (gint)i));
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_object
 */
JNIEXPORT jobject JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1object
  (JNIEnv *env, jclass cls, jobject link, jint i)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return getGObjectHandle(env, G_OBJECT(atk_hyperlink_get_object(link_g, (gint)i)));
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_end_index
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1end_1index
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jint)atk_hyperlink_get_end_index(link_g);
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_start_index
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1start_1index
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jint)atk_hyperlink_get_start_index(link_g);
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_is_valid
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1is_1valid
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jboolean)atk_hyperlink_is_valid(link_g);
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_is_inline
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1is_1inline
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jboolean)atk_hyperlink_is_inline(link_g);
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_get_n_anchors
 */
JNIEXPORT jint JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1get_1n_1anchors
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jint)atk_hyperlink_get_n_anchors(link_g);
}

/*
 * Class:     org_gnu_atk_Hyperlink
 * Method:    atk_hyperlink_is_selected_link
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_atk_Hyperlink_atk_1hyperlink_1is_1selected_1link
  (JNIEnv *env, jclass cls, jobject link)
{
	AtkHyperlink* link_g = (AtkHyperlink*)getPointerFromHandle(env, link);
	return (jboolean)atk_hyperlink_is_selected_link(link_g);
}

#ifdef __cplusplus
}
#endif
#endif
