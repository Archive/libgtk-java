/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellRenderer.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_cell_renderer_get_type ();
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_get_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1get_1size (JNIEnv 
    *env, jclass cls, jobject cell, jobject widget, jobject cellArea, jintArray xOffset, jintArray 
    yOffset, jintArray width, jintArray height) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkRectangle *cellArea_g = (GdkRectangle *)getPointerFromHandle(env, cellArea);
    gint *xOffset_g = (gint *) (*env)->GetIntArrayElements (env, xOffset, NULL);
    gint *yOffset_g = (gint *) (*env)->GetIntArrayElements (env, yOffset, NULL);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_cell_renderer_get_size (cell_g, widget_g, cellArea_g, xOffset_g, yOffset_g, 
            width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, xOffset, (jint *) xOffset_g, 0);
    (*env)->ReleaseIntArrayElements (env, yOffset, (jint *) yOffset_g, 0);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_render
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1render (JNIEnv *env, 
    jclass cls, jobject cell, jobject window, jobject widget, jobject backgroundArea, jobject cellArea, jobject 
    exposedArea, jint flags) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    GdkWindow *window_g = (GdkWindow *)getPointerFromHandle(env, window);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkRectangle *backgroundArea_g = (GdkRectangle *)getPointerFromHandle(env, backgroundArea);
    GdkRectangle *cellArea_g = (GdkRectangle *)getPointerFromHandle(env, cellArea);
    GdkRectangle *exposedArea_g = (GdkRectangle *)getPointerFromHandle(env, exposedArea);
    GtkCellRendererState flags_g = (GtkCellRendererState) flags;
    gtk_cell_renderer_render (cell_g, window_g, widget_g, backgroundArea_g, cellArea_g, 
            exposedArea_g, flags_g);
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_activate
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1activate (JNIEnv 
    *env, jclass cls, jobject cell, jobject event, jobject widget, jstring path, jobject backgroundArea, 
    jobject cellArea, jint flags) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkRectangle *backgroundArea_g = (GdkRectangle *)getPointerFromHandle(env, backgroundArea);
    GdkRectangle *cellArea_g = (GdkRectangle *)getPointerFromHandle(env, cellArea);
    GtkCellRendererState flags_g = (GtkCellRendererState) flags;
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
	jboolean result_j = (jboolean) (gtk_cell_renderer_activate (cell_g, event_g, widget_g, 
			path_g, backgroundArea_g, cellArea_g, flags_g));
	(*env)->ReleaseStringUTFChars(env, path, path_g);
	return result_j;
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_start_editing
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1start_1editing (
    JNIEnv *env, jclass cls, jobject cell, jobject event, jobject widget, jstring path, jobject 
    backgroundArea, jobject cellArea, jint flags) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    GdkEvent *event_g = (GdkEvent *)getPointerFromHandle(env, event);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    GdkRectangle *backgroundArea_g = (GdkRectangle *)getPointerFromHandle(env, backgroundArea);
    GdkRectangle *cellArea_g = (GdkRectangle *)getPointerFromHandle(env, cellArea);
    GtkCellRendererState flags_g = (GtkCellRendererState) flags;
    const gchar* path_g = (*env)->GetStringUTFChars(env, path, NULL);
    jobject retval = getGObjectHandle(env, G_OBJECT(gtk_cell_renderer_start_editing (cell_g, event_g, widget_g, path_g, 
										     backgroundArea_g, cellArea_g, flags_g)));
    (*env)->ReleaseStringUTFChars(env, path, path_g);
    return retval;
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_set_fixed_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1set_1fixed_1size (
    JNIEnv *env, jclass cls, jobject cell, jint width, jint height) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    gtk_cell_renderer_set_fixed_size (cell_g, width_g, height_g);
}

/*
 * Class:     org.gnu.gtk.CellRenderer
 * Method:    gtk_cell_renderer_get_fixed_size
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1get_1fixed_1size (
    JNIEnv *env, jclass cls, jobject cell, jintArray width, jintArray height) 
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_cell_renderer_get_fixed_size (cell_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org_gnu_gtk_CellRenderer
 * Method:    gtk_cell_renderer_stop_editing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellRenderer_gtk_1cell_1renderer_1stop_1editing
  (JNIEnv *env, jclass cls, jobject cell, jboolean canceled)
{
    GtkCellRenderer *cell_g = (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gtk_cell_renderer_stop_editing(cell_g, (gboolean)canceled);
}


#ifdef __cplusplus
}

#endif
