/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ColorSelectionDialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkColorSelection * GtkColorSelectionDialog_get_colorsel (GtkColorSelectionDialog * cptr) 
{
    return (GtkColorSelection*)cptr->colorsel;
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    getColorsel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelectionDialog_getColorsel (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkColorSelectionDialog *cptr_g = (GtkColorSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, (GObject *) GtkColorSelectionDialog_get_colorsel (cptr_g));
}

static GtkButton * GtkColorSelectionDialog_get_ok_button (GtkColorSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->ok_button;
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    getOkButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelectionDialog_getOkButton (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkColorSelectionDialog *cptr_g = (GtkColorSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, (GObject *) GtkColorSelectionDialog_get_ok_button (cptr_g));
}

static GtkButton * GtkColorSelectionDialog_get_cancel_button (GtkColorSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->cancel_button;
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    getCancelButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelectionDialog_getCancelButton (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GtkColorSelectionDialog *cptr_g = (GtkColorSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, (GObject *) GtkColorSelectionDialog_get_cancel_button (cptr_g));
}

static GtkButton * GtkColorSelectionDialog_get_help_button (GtkColorSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->help_button;
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    getHelpButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelectionDialog_getHelpButton (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkColorSelectionDialog *cptr_g = (GtkColorSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandleAndRef(env, (GObject *) GtkColorSelectionDialog_get_help_button (cptr_g));
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    gtk_color_selection_dialog_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_ColorSelectionDialog_gtk_1color_1selection_1dialog_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_color_selection_dialog_get_type ();
}

/*
 * Class:     org.gnu.gtk.ColorSelectionDialog
 * Method:    gtk_color_selection_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ColorSelectionDialog_gtk_1color_1selection_1dialog_1new (
    JNIEnv *env, jclass cls, jstring title) 
{
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    jobject hndl = getGObjectHandle(env, (GObject *) gtk_color_selection_dialog_new (title_g));
    (*env)->ReleaseStringUTFChars(env, title, title_g);
    return hndl;
}


#ifdef __cplusplus
}

#endif
