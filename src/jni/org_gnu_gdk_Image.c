/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Image.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getVisual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Image_getVisual (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return getGObjectHandle(env, G_OBJECT(obj_g->visual));
}

static GdkByteOrder GdkImage_get_byte_order (GdkImage * cptr) 
{
    return cptr->byte_order;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getByteOrder
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getByteOrder (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_byte_order (obj_g));
}

static guint32 GdkImage_get_width (GdkImage * cptr) 
{
    return cptr->width;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getWidth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_width (obj_g));
}

static guint32 GdkImage_get_height (GdkImage * cptr) 
{
    return cptr->height;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getHeight
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getHeight (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_height (obj_g));
}

static guint32 GdkImage_get_depth (GdkImage * cptr) 
{
    return cptr->depth;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getDepth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getDepth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_depth (obj_g));
}

static guint32 GdkImage_get_bpp (GdkImage * cptr) 
{
    return cptr->bpp;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getBpp
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getBpp (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_bpp (obj_g));
}

static guint32 GdkImage_get_bpl (GdkImage * cptr) 
{
    return cptr->bpl;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getBpl
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getBpl (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_bpl (obj_g));
}

static guint32 GdkImage_get_bits_per_pixel (GdkImage * cptr) 
{
    return cptr->bits_per_pixel;
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    getBitsPerPixel
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_getBitsPerPixel (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkImage *obj_g = (GdkImage *)getPointerFromHandle(env, obj);
    return (jint) (GdkImage_get_bits_per_pixel (obj_g));
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_gdk_1image_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_image_get_type ();
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Image_gdk_1image_1new (JNIEnv *env, jclass cls, jint 
    type, jobject visual, jint width, jint height) 
{
    GdkImageType type_g = (GdkImageType) type;
    GdkVisual *visual_g = (GdkVisual *)getPointerFromHandle(env, visual);
    gint32 width_g = (gint32) width;
    gint32 height_g = (gint32) height;
    return getGObjectHandle(env, G_OBJECT(gdk_image_new (type_g, visual_g, width_g, height_g)));
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_put_pixel
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Image_gdk_1image_1put_1pixel (JNIEnv *env, jclass cls, 
    jobject image, jint x, jint y, jint pixel) 
{
    GdkImage *image_g = (GdkImage *)getPointerFromHandle(env, image);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    guint32 pixel_g = (guint32) pixel;
    gdk_image_put_pixel (image_g, x_g, y_g, pixel_g);
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_get_pixel
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Image_gdk_1image_1get_1pixel (JNIEnv *env, jclass cls, 
    jobject image, jint x, jint y) 
{
    GdkImage *image_g = (GdkImage *)getPointerFromHandle(env, image);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    return (jint) (gdk_image_get_pixel (image_g, x_g, y_g));
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_set_colormap
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Image_gdk_1image_1set_1colormap (JNIEnv *env, jclass 
    cls, jobject image, jobject colormap) 
{
    GdkImage *image_g = (GdkImage *)getPointerFromHandle(env, image);
    GdkColormap *colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    gdk_image_set_colormap (image_g, colormap_g);
}

/*
 * Class:     org.gnu.gdk.Image
 * Method:    gdk_image_get_colormap
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Image_gdk_1image_1get_1colormap (JNIEnv *env, jclass 
    cls, jobject image) 
{
    GdkImage *image_g = (GdkImage *)getPointerFromHandle(env, image);
    return getGObjectHandle(env, G_OBJECT(gdk_image_get_colormap (image_g)));
}


#ifdef __cplusplus
}

#endif
