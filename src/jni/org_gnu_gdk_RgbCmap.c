/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_RgbCmap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.RgbCmap
 * Method:    gdk_rgb_cmap_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_RgbCmap_gdk_1rgb_1cmap_1new (JNIEnv *env, jclass cls, 
    jintArray colors, jint numColors) 
{
    guint *colors_g = (guint *) (*env)->GetIntArrayElements (env, colors, NULL);
    gint32 numColors_g = (gint32) numColors;
    return getStructHandle(env, gdk_rgb_cmap_new (colors_g, numColors_g), 
                           NULL, (JGFreeFunc)gdk_rgb_cmap_free);
}


#ifdef __cplusplus
}

#endif
