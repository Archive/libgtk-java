/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ListStore.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static jobject getTreeIter(JNIEnv *env, GtkTreeIter *iter)
{
	return getGBoxedHandle(env, iter, GTK_TYPE_TREE_ITER, NULL,
        		(GBoxedFreeFunc) gtk_tree_iter_free);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_list_store_get_type ();
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_newv
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1newv (JNIEnv *env, jclass cls, jint numColumns, jintArray types) 
{
    gint32 numColumns_g;
    GType *types_g;
    jint *types_g_g;
    jobject ret;
    numColumns_g = (gint32) numColumns;
    types_g = getGTypesFromJArray(env, numColumns, types, &types_g_g);
    ret = getGObjectHandle(env, (GObject *) gtk_list_store_newv (numColumns_g, types_g));
    (*env)->ReleaseIntArrayElements (env, types, types_g_g, 0);
    return ret;
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_set_column_types
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1set_1column_1types (JNIEnv 
*env, jclass cls, jobject listStore, jint numColumns, jintArray types) 
{
	gint32 numColumns_g;
	jint *types_g_g;
	GType *types_g;
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    numColumns_g = (gint32) numColumns;
    types_g = getGTypesFromJArray(env, numColumns, types, &types_g_g);
    gtk_list_store_set_column_types (listStore_g, numColumns_g, types_g);
    (*env)->ReleaseIntArrayElements (env, types, types_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_set_value
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1set_1value (JNIEnv *env, 
jclass cls, jobject listStore, jobject iter, jint column, jobject value) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    gint32 column_g = (gint32) column;
    GValue *value_g = (GValue *)getPointerFromHandle(env, value);
    gtk_list_store_set_value (listStore_g, iter_g, column_g, value_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_remove
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1remove (JNIEnv *env, jclass 
cls, jobject listStore, jobject iter) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    return gtk_list_store_remove (listStore_g, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_insert
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1insert (JNIEnv *env, jclass 
cls, jobject listStore, jint position) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    gint32 position_g = (gint32) position;
    gtk_list_store_insert (listStore_g, iter_g, position_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_insert_before
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1insert_1before (JNIEnv 
*env, jclass cls, jobject listStore, jobject sibling) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    GtkTreeIter *sibling_g = (GtkTreeIter *)getPointerFromHandle(env, sibling);
    gtk_list_store_insert_before (listStore_g, iter_g, sibling_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_insert_after
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1insert_1after (JNIEnv *env, 
jclass cls, jobject listStore, jobject sibling) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    GtkTreeIter *sibling_g = (GtkTreeIter *)getPointerFromHandle(env, sibling);
    gtk_list_store_insert_after (listStore_g, iter_g, sibling_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_prepend
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1prepend (JNIEnv *env, 
jclass cls, jobject listStore) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    gtk_list_store_prepend (listStore_g, iter_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_append
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1append (JNIEnv *env, jclass 
cls, jobject listStore) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    gtk_list_store_append (listStore_g, iter_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.ListStore
 * Method:    gtk_list_store_clear
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1clear (JNIEnv *env, jclass 
cls, jobject listStore) 
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    gtk_list_store_clear (listStore_g);
}

/*
 * Class:     org_gnu_gtk_ListStore
 * Method:    gtk_list_store_iter_is_valid
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1iter_1is_1valid
(JNIEnv *env, jclass cls, jobject listStore, jobject iter)
{
    GtkListStore *listStore_g = (GtkListStore *)getPointerFromHandle(env, listStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    return (jboolean)gtk_list_store_iter_is_valid(listStore_g, iter_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_ListStore
 * Method:    gtk_list_store_reorder
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1reorder
(JNIEnv *env, jclass cls, jobject store, jintArray order)
{
    GtkListStore *store_g = (GtkListStore *)getPointerFromHandle(env, store);
    gint* newOrder = (gint*)(*env)->GetIntArrayElements(env, order, NULL);
    gtk_list_store_reorder(store_g, newOrder);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_ListStore
 * Method:    gtk_list_store_swap
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1swap
(JNIEnv *env, jclass cls, jobject store, jobject a, jobject b)
{
    GtkListStore *store_g = (GtkListStore *)getPointerFromHandle(env, store);
    GtkTreeIter *a_g = (GtkTreeIter *)getPointerFromHandle(env, a);
    GtkTreeIter *b_g = (GtkTreeIter *)getPointerFromHandle(env, b);
    gtk_list_store_swap(store_g, a_g, b_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_ListStore
 * Method:    gtk_list_store_move_after
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1move_1after
(JNIEnv *env, jclass cls, jobject store, jobject iter, jobject pos)
{
    GtkListStore *store_g = (GtkListStore *)getPointerFromHandle(env, store);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    GtkTreeIter *pos_g = (GtkTreeIter *)getPointerFromHandle(env, pos);
    gtk_list_store_move_after(store_g, iter_g, pos_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_ListStore
 * Method:    gtk_list_store_move_before
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_gtk_1list_1store_1move_1before
(JNIEnv *env, jclass cls, jobject store, jobject iter, jobject pos)
{
    GtkListStore *store_g = (GtkListStore *)getPointerFromHandle(env, store);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    GtkTreeIter *pos_g = (GtkTreeIter *)getPointerFromHandle(env, pos);
    gtk_list_store_move_before(store_g, iter_g, pos_g);
}


JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_tree_1sortable_1set_1sort_1column_1id (JNIEnv *env, jclass 
    cls, jobject sortable, jint sort_column_id, jint order) 
{
	GtkTreeSortable* sortable_g = (GtkTreeSortable*)getPointerFromHandle(env, sortable);
	gtk_tree_sortable_set_sort_column_id(sortable_g, (gint)sort_column_id, (GtkSortType) order);
}


//GtkTreePath * GtkListStore_get_root (GtkListStore * cptr) 
//{
//    return cptr->root;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getRoot
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_getRoot (JNIEnv *env, jclass cls, jint cptr) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        return (jint)GtkListStore_get_root (cptr_g);
//    }
//}
//
//void GtkListStore_set_root (GtkListStore * cptr, GtkTreePath * root) 
//{
//    cptr->root = root;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setRoot
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setRoot (JNIEnv *env, jint cptr, jint root) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    GtkTreePath *root_g = (GtkTreePath *)root;
//    {
//        GtkListStore_set_root (cptr_g, root_g);
//    }
//}
//
//GtkTreePath * GtkListStore_get_tail (GtkListStore * cptr) 
//{
//    return cptr->tail;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getTail
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_getTail (JNIEnv *env, jclass cls, jint cptr) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        return (jint)GtkListStore_get_tail (cptr_g);
//    }
//}
//
//void GtkListStore_set_tail (GtkListStore * cptr, GtkTreePath * tail) 
//{
//    cptr->tail = tail;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setTail
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setTail (JNIEnv *env, jint cptr, jint tail) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    GtkTreePath *tail_g = (GtkTreePath *)tail;
//    {
//        GtkListStore_set_tail (cptr_g, tail_g);
//    }
//}
//
//gint32 GtkListStore_get_n_columns (GtkListStore * cptr) 
//{
//    return cptr->n_columns;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getNColumns
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_getNColumns (JNIEnv *env, jclass cls, jint 
//    cptr) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        jint result_j = (jint) (GtkListStore_get_n_columns (cptr_g));
//        return result_j;
//    }
//}
//
//void GtkListStore_set_n_columns (GtkListStore * cptr, gint32 n_columns) 
//{
//    cptr->n_columns = n_columns;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setNColumns
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setNColumns (JNIEnv *env, jint cptr, jint 
//    n_columns) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    gint32 n_columns_g = (gint32) n_columns;
//    {
//        GtkListStore_set_n_columns (cptr_g, n_columns_g);
//    }
//}
//GType * GtkListStore_get_column_headers (GtkListStore * cptr) 
//{
//    return cptr->column_headers;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getColumnHeaders
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_getColumnHeaders (JNIEnv *env, jclass cls, 
//    jint cptr) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        return (jint)GtkListStore_get_column_headers (cptr_g);
//    }
//}
//
//void GtkListStore_set_column_headers (GtkListStore * cptr, GType * column_headers) 
//{
//    cptr->column_headers = column_headers;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setColumnHeaders
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setColumnHeaders (JNIEnv *env, jint cptr, 
//    jint column_headers) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    GType *column_headers_g = (GType *)column_headers;
//    {
//        GtkListStore_set_column_headers (cptr_g, column_headers_g);
//    }
//}
//
//gint32 GtkListStore_get_length (GtkListStore * cptr) 
//{
//    return cptr->length;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getLength
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_ListStore_getLength (JNIEnv *env, jclass cls, jint cptr)
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        jint result_j = (jint) (GtkListStore_get_length (cptr_g));
//        return result_j;
//    }
//}
//
//void GtkListStore_set_length (GtkListStore * cptr, gint32 length) 
//{
//    cptr->length = length;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setLength
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setLength (JNIEnv *env, jint cptr, jint 
//    length) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    gint32 length_g = (gint32) length;
//    {
//        GtkListStore_set_length (cptr_g, length_g);
//    }
//}
//
//gboolean GtkListStore_get_columns_dirty (GtkListStore * cptr) 
//{
//    return cptr->columns_dirty;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    getColumnsDirty
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ListStore_getColumnsDirty (JNIEnv *env, jclass cls, 
//    jint cptr) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    {
//        jboolean result_j = (jboolean) (GtkListStore_get_columns_dirty (cptr_g));
//        return result_j;
//    }
//}
//
//void GtkListStore_set_columns_dirty (GtkListStore * cptr, gboolean columns_dirty) 
//{
//    cptr->columns_dirty = columns_dirty;
//}
//
///*
// * Class:     org.gnu.gtk.ListStore
// * Method:    setColumnsDirty
// * Signature: (IZ)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_ListStore_setColumnsDirty (JNIEnv *env, jint cptr, 
//    jboolean columns_dirty) 
//{
//    GtkListStore *cptr_g = (GtkListStore *)cptr;
//    gboolean columns_dirty_g = (gboolean) columns_dirty;
//    {
//        GtkListStore_set_columns_dirty (cptr_g, columns_dirty_g);
//    }
//}

#ifdef __cplusplus
}

#endif
