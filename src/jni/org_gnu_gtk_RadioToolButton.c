/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_RadioToolButton
#define _Included_org_gnu_gtk_RadioToolButton
#include "org_gnu_gtk_RadioToolButton.h"
#ifdef __cplusplus
extern "C" {
#endif


/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_radio_tool_button_get_type();
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1new
  (JNIEnv *env, jclass cls, jobjectArray group)
{
	GSList* list = getGSListFromHandles(env, group);
	return getGObjectHandle(env, (GObject *) gtk_radio_tool_button_new(list));
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_new_from_stock
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1new_1from_1stock
  (JNIEnv *env, jclass cls, jobjectArray group, jstring stockId)
{
	GSList* list = getGSListFromHandles(env, group);
	const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
	jobject handle = getGObjectHandle(env, (GObject *)
			gtk_radio_tool_button_new_from_stock(list, s));
	(*env)->ReleaseStringUTFChars(env, stockId, s);
	return handle;
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_new_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1new_1from_1widget
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkRadioToolButton* button_g = (GtkRadioToolButton*)getPointerFromHandle(env, button);
	return getGObjectHandle(env, (GObject *) gtk_radio_tool_button_new_from_widget(button_g));
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_new_with_stock_from_widget
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1new_1with_1stock_1from_1widget
  (JNIEnv *env, jclass cls, jobject button, jstring stockId)
{
	GtkRadioToolButton* button_g = (GtkRadioToolButton*)getPointerFromHandle(env, button);
	const gchar* s = (*env)->GetStringUTFChars(env, stockId, NULL);
	jobject handle = getGObjectHandle(env, (GObject *)
			gtk_radio_tool_button_new_with_stock_from_widget(button_g, s));
	(*env)->ReleaseStringUTFChars(env, stockId, s);
	return handle;
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_get_group
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1get_1group
  (JNIEnv *env, jclass cls, jobject button)
{
	GtkRadioToolButton* button_g = (GtkRadioToolButton*)getPointerFromHandle(env, button);
	return getGObjectHandlesFromGSList(env, gtk_radio_tool_button_get_group(button_g));
}

/*
 * Class:     org_gnu_gtk_RadioToolButton
 * Method:    gtk_radio_tool_button_set_group
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_RadioToolButton_gtk_1radio_1tool_1button_1set_1group
  (JNIEnv *env, jclass cls, jobject button, jobjectArray group)
{
	GtkRadioToolButton* button_g = (GtkRadioToolButton*)getPointerFromHandle(env, button);
	GSList* list = getGSListFromHandles(env, group);
	gtk_radio_tool_button_set_group(button_g, list);
}

#ifdef __cplusplus
}
#endif
#endif
