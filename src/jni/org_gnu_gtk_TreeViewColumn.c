/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeViewColumn.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_tree_view_column_get_type ();
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_tree_view_column_new ());
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_pack_start
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1pack_1start (JNIEnv *env, jclass cls, jobject treeColumn, jobject cell, jboolean expand) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkCellRenderer *cell_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gboolean expand_g = (gboolean) expand;
    gtk_tree_view_column_pack_start (treeColumn_g, cell_g, expand_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_pack_end
 * Signature: (IIZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1pack_1end (JNIEnv *env, jclass cls, jobject treeColumn, jobject cell, jboolean expand) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkCellRenderer *cell_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gboolean expand_g = (gboolean) expand;
    gtk_tree_view_column_pack_end (treeColumn_g, cell_g, expand_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_clear
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1clear (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gtk_tree_view_column_clear (treeColumn_g);
}

/*
 * Class:     org_gnu_gtk_TreeViewColumn
 * Method:    gtk_tree_view_column_get_cell_renderers
 * Signature: (I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1cell_1renderers(JNIEnv *env, jclass cls, jobject treeColumn)
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return getGObjectHandlesFromGList(env,gtk_tree_view_column_get_cell_renderers(treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_add_attribute
 * Signature: (II[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1add_1attribute (JNIEnv *env, jclass cls, jobject treeColumn, jobject cellRenderer, jstring attribute, jint column) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkCellRenderer *cellRenderer_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cellRenderer);
    gint32 column_g = (gint32) column;
    const gchar *attribute_g = (*env)->GetStringUTFChars(env, attribute, NULL);
    gtk_tree_view_column_add_attribute (treeColumn_g, cellRenderer_g, attribute_g, column_g);
    (*env)->ReleaseStringUTFChars( env, attribute, attribute_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_clear_attributes
 * Signature: (II)V
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1clear_1attributes (JNIEnv *env, jclass cls, jobject treeColumn, jobject cellRenderer) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkCellRenderer *cellRenderer_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cellRenderer);
    gtk_tree_view_column_clear_attributes (treeColumn_g, cellRenderer_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_spacing
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1spacing (JNIEnv *env, jclass cls, jobject treeColumn, jint spacing) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gint32 spacing_g = (gint32) spacing;
    gtk_tree_view_column_set_spacing (treeColumn_g, spacing_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_spacing
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1spacing (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_spacing (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_visible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1visible (JNIEnv *env, jclass cls, jobject treeColumn, jboolean visible) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean visible_g = (gboolean) visible;
    gtk_tree_view_column_set_visible (treeColumn_g, visible_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_visible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1visible (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_get_visible (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_resizable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1resizable (JNIEnv *env, jclass cls, jobject treeColumn, jboolean resizable) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean resizable_g = (gboolean) resizable;
    gtk_tree_view_column_set_resizable (treeColumn_g, resizable_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_resizable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1resizable (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_get_resizable (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_sizing
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1sizing (JNIEnv *env, jclass cls, jobject treeColumn, jint type) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkTreeViewColumnSizing type_g = (GtkTreeViewColumnSizing) type;
    gtk_tree_view_column_set_sizing (treeColumn_g, type_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_sizing
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1sizing (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_sizing (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_width
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1width (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_width (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_fixed_width
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1fixed_1width (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_fixed_width (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_fixed_width
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1fixed_1width (JNIEnv *env, jclass cls, jobject treeColumn, jint fixedWidth) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gint32 fixedWidth_g = (gint32) fixedWidth;
    gtk_tree_view_column_set_fixed_width (treeColumn_g, fixedWidth_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_min_width
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1min_1width (JNIEnv *env, jclass cls, jobject treeColumn, jint minWidth) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gint32 minWidth_g = (gint32) minWidth;
    gtk_tree_view_column_set_min_width (treeColumn_g, minWidth_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_min_width
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1min_1width (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_min_width (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_max_width
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1max_1width (JNIEnv *env, jclass cls, jobject treeColumn, jint maxWidth) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gint32 maxWidth_g = (gint32) maxWidth;
    gtk_tree_view_column_set_max_width (treeColumn_g, maxWidth_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_max_width
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1max_1width (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_max_width (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_clicked
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1clicked (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gtk_tree_view_column_clicked (treeColumn_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_title
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1title (JNIEnv *env, jclass cls, jobject treeColumn, jstring title) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    const gchar* title_g = (*env)->GetStringUTFChars(env, title, NULL);
    gtk_tree_view_column_set_title (treeColumn_g, title_g);
    (*env)->ReleaseStringUTFChars( env, title, title_g );
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_title
 * Signature: (I)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1title (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (*env)->NewStringUTF(env, (gchar*)gtk_tree_view_column_get_title (treeColumn_g) );
}

/*
 * Class:     org_gnu_gtk_TreeViewColumn
 * Method:    gtk_tree_view_column_set_expand
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1expand(JNIEnv *env, jclass cls, jobject treeColumn, jboolean expand)
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean expand_g = (gboolean)expand;
    gtk_tree_view_column_set_expand(treeColumn_g, expand_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeViewColumn
 * Method:    gtk_tree_view_column_get_expand
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1expand(JNIEnv *env, jclass cls, jobject treeColumn)
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean)gtk_tree_view_column_get_expand(treeColumn_g);
}
                                                                                         
/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_clickable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1clickable (JNIEnv *env, jclass cls, jobject treeColumn, jboolean clickable) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean clickable_g = (gboolean) clickable;
    gtk_tree_view_column_set_clickable (treeColumn_g, clickable_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_clickable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1clickable (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_get_clickable (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_widget
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1widget (JNIEnv *env, jclass cls, jobject treeColumn, jobject widget) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkWidget *widget_g = (GtkWidget *)getPointerFromHandle(env, widget);
    gtk_tree_view_column_set_widget (treeColumn_g, widget_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_widget
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1widget (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_column_get_widget (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_alignment
 * Signature: (ID)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1alignment (JNIEnv *env, jclass cls, jobject treeColumn, jdouble xalign) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gdouble xalign_g = (gdouble) xalign;
    gtk_tree_view_column_set_alignment (treeColumn_g, xalign_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_alignment
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1alignment (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jdouble) (gtk_tree_view_column_get_alignment (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_reorderable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1reorderable (JNIEnv *env, jclass cls, jobject treeColumn, jboolean reorderable) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean reorderable_g = (gboolean) reorderable;
    gtk_tree_view_column_set_reorderable (treeColumn_g, reorderable_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_reorderable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1reorderable (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_get_reorderable (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_sort_column_id
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1sort_1column_1id (JNIEnv *env, jclass cls, jobject treeColumn, jint sortColumnID) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gint32 sortColumnID_g = (gint32) sortColumnID;
    gtk_tree_view_column_set_sort_column_id (treeColumn_g, sortColumnID_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_sort_column_id
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1sort_1column_1id (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_sort_column_id (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_sort_indicator
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1sort_1indicator (JNIEnv *env, jclass cls, jobject treeColumn, jboolean setting) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    gboolean setting_g = (gboolean) setting;
    gtk_tree_view_column_set_sort_indicator (treeColumn_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_sort_indicator
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1sort_1indicator (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_get_sort_indicator (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_set_sort_order
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1set_1sort_1order (JNIEnv *env, jclass cls, jobject treeColumn, jint order) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkSortType order_g = (GtkSortType) order;
    gtk_tree_view_column_set_sort_order (treeColumn_g, order_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_get_sort_order
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1get_1sort_1order (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jint) (gtk_tree_view_column_get_sort_order (treeColumn_g));
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_cell_set_cell_data
 * Signature: (IIIZZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1cell_1set_1cell_1data (JNIEnv *env, jclass cls, jobject treeColumn, jobject treeModel, jobject iter, jboolean isExpander, jboolean isExpanded) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkTreeModel *treeModel_g = 
        (GtkTreeModel *)getPointerFromHandle(env, treeModel);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    gboolean isExpander_g = (gboolean) isExpander;
    gboolean isExpanded_g = (gboolean) isExpanded;
    gtk_tree_view_column_cell_set_cell_data (treeColumn_g, treeModel_g, iter_g,
                                             isExpander_g, isExpanded_g);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_cell_get_size
 * Signature: (I[Lint ;[Lint ;[Lint ;[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1cell_1get_1size (JNIEnv *env, jclass cls, jobject treeColumn, jobject cellRectangle, jintArray xOffset, jintArray yOffset, jintArray width, jintArray height) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GdkRectangle *cellArea_g = 
        (GdkRectangle *)getPointerFromHandle(env, cellRectangle);
    gint *xOffset_g = (gint *) (*env)->GetIntArrayElements (env, xOffset, NULL);
    gint *yOffset_g = (gint *) (*env)->GetIntArrayElements (env, yOffset, NULL);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_tree_view_column_cell_get_size (treeColumn_g, cellArea_g, 
                                        xOffset_g, yOffset_g, 
                                        width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, xOffset, (jint *) xOffset_g, 0);
    (*env)->ReleaseIntArrayElements (env, yOffset, (jint *) yOffset_g, 0);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.TreeViewColumn
 * Method:    gtk_tree_view_column_cell_is_visible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1cell_1is_1visible (JNIEnv *env, jclass cls, jobject treeColumn) 
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    return (jboolean) (gtk_tree_view_column_cell_is_visible (treeColumn_g));
}

/*
 * Class:     org_gnu_gtk_TreeViewColumn
 * Method:    gtk_tree_view_column_focus_cell
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeViewColumn_gtk_1tree_1view_1column_1focus_1cell(JNIEnv *env, jclass cls, jobject treeColumn, jobject cell)
{
    GtkTreeViewColumn *treeColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, treeColumn);
    GtkCellRenderer *cell_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gtk_tree_view_column_focus_cell(treeColumn_g, cell_g);
}

#ifdef __cplusplus
}

#endif
