/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_FontSelectionDialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static GtkFontSelection * GtkFontSelectionDialog_get_fontsel (GtkFontSelectionDialog * cptr) 
{
    return (GtkFontSelection*)cptr->fontsel;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getFontsel
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_getFontsel (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFontSelectionDialog_get_fontsel (cptr_g));
}

static GtkWidget * GtkFontSelectionDialog_get_main_vbox (GtkFontSelectionDialog * cptr) 
{
    return cptr->main_vbox;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getMainVbox
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_getMainVbox (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFontSelectionDialog_get_main_vbox (cptr_g));
}

static GtkButton * GtkFontSelectionDialog_get_ok_button (GtkFontSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->ok_button;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getOkButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_getOkButton (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFontSelectionDialog_get_ok_button (cptr_g));
}

static GtkButton * GtkFontSelectionDialog_get_apply_button (GtkFontSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->apply_button;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getApplyButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_getApplyButton (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFontSelectionDialog_get_apply_button (cptr_g));
}

static GtkButton * GtkFontSelectionDialog_get_cancel_button (GtkFontSelectionDialog * cptr) 
{
    return (GtkButton*)cptr->cancel_button;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getCancelButton
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_getCancelButton (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return getGObjectHandle(env, (GObject *) GtkFontSelectionDialog_get_cancel_button (cptr_g));
}

static gint32 GtkFontSelectionDialog_get_dialog_width (GtkFontSelectionDialog * cptr) 
{
    return cptr->dialog_width;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getDialogWidth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_FontSelectionDialog_getDialogWidth (JNIEnv *env, jclass 
    cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return (jint) (GtkFontSelectionDialog_get_dialog_width (cptr_g));
}

static gboolean GtkFontSelectionDialog_get_auto_resize (GtkFontSelectionDialog * cptr) 
{
    return cptr->auto_resize;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    getAutoResize
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_FontSelectionDialog_getAutoResize (JNIEnv *env, 
    jclass cls, jobject cptr) 
{
    GtkFontSelectionDialog *cptr_g = (GtkFontSelectionDialog *)getPointerFromHandle(env, cptr);
    return (jboolean) (GtkFontSelectionDialog_get_auto_resize (cptr_g));
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_font_selection_dialog_get_type ();
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1new (
    JNIEnv *env, jclass cls, jstring title) 
{
    gchar* title_g = (gchar*)(*env)->GetStringUTFChars(env, title, 0);
    jobject result = getGObjectHandle(env, (GObject *) gtk_font_selection_dialog_new (title_g));
    (*env)->ReleaseStringUTFChars(env, title, title_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_get_font_name
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1get_1font_1name (JNIEnv 
    *env, jclass cls, jobject fontsel) 
{
	GtkFontSelectionDialog* fontsel_g = (GtkFontSelectionDialog*)getPointerFromHandle(env, fontsel);
    gchar *result_g = gtk_font_selection_dialog_get_font_name(fontsel_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_set_font_name
 * Signature: (Ijava.lang.String;)Z
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1set_1font_1name (JNIEnv 
    *env, jclass cls, jobject fontsel, jstring fontname) 
{
	GtkFontSelectionDialog* fontsel_g = (GtkFontSelectionDialog*)getPointerFromHandle(env, fontsel);
    gchar* fontname_g = (gchar*)(*env)->GetStringUTFChars(env, fontname, 0);
    jboolean result_j = (jboolean) (gtk_font_selection_dialog_set_font_name (fontsel_g, 
                fontname_g));
  	(*env)->ReleaseStringUTFChars(env, fontname, fontname_g);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_get_preview_text
 */
JNIEXPORT jstring JNICALL 
Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1get_1preview_1text (JNIEnv 
    *env, jclass cls, jobject fontsel) 
{
	GtkFontSelectionDialog* fontsel_g = (GtkFontSelectionDialog*)getPointerFromHandle(env, fontsel);
    gchar *result_g = (gchar*)gtk_font_selection_dialog_get_preview_text (fontsel_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.FontSelectionDialog
 * Method:    gtk_font_selection_dialog_set_preview_text
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_FontSelectionDialog_gtk_1font_1selection_1dialog_1set_1preview_1text (JNIEnv 
    *env, jclass cls, jobject fontsel, jstring text) 
{
	GtkFontSelectionDialog* fontsel_g = (GtkFontSelectionDialog*)getPointerFromHandle(env, fontsel);
    gchar* text_g = (gchar*)(*env)->GetStringUTFChars(env, text, 0);
    gtk_font_selection_dialog_set_preview_text (fontsel_g, text_g);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
}


#ifdef __cplusplus
}

#endif
