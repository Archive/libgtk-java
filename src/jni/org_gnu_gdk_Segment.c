/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Segment.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gdk_Segment
 * Method:    gdk_segment_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Segment_gdk_1segment_1new
  (JNIEnv *env, jclass cls, jint x1, jint y1, jint x2, jint y2)
{
    GdkSegment *seg = g_new(GdkSegment, 1);
    seg->x1 = x1;
    seg->y1 = y1;
    seg->x2 = x2;
    seg->y2 = y2;
    return getStructHandle(env, seg, NULL, g_free);
}

static gint32 GdkSegment_get_x1 (GdkSegment * cptr) 
{
    return cptr->x1;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    getX1
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Segment_getX1 (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    return (jint) (GdkSegment_get_x1 (obj_g));
}

static void GdkSegment_set_x1 (GdkSegment * cptr, gint32 x1) 
{
    cptr->x1 = x1;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    setX1
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Segment_setX1 (JNIEnv *env, jclass klass, jobject obj, jint x1)
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    gint32 x1_g = (gint32) x1;
    GdkSegment_set_x1 (obj_g, x1_g);
}

static gint32 GdkSegment_get_y1 (GdkSegment * cptr) 
{
    return cptr->y1;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    getY1
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Segment_getY1 (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    return (jint) (GdkSegment_get_y1 (obj_g));
}

static void GdkSegment_set_y1 (GdkSegment * cptr, gint32 y1) 
{
    cptr->y1 = y1;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    setY1
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Segment_setY1 (JNIEnv *env, jclass klass, jobject obj, jint y1) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    gint32 y1_g = (gint32) y1;
    GdkSegment_set_y1 (obj_g, y1_g);
}

static gint32 GdkSegment_get_x2 (GdkSegment * cptr) 
{
    return cptr->x2;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    getX2
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Segment_getX2 (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    return (jint) (GdkSegment_get_x2 (obj_g));
}

static void GdkSegment_set_x2 (GdkSegment * cptr, gint32 x2) 
{
    cptr->x2 = x2;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    setX2
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Segment_setX2 (JNIEnv *env, jclass klass, jobject obj, jint x2) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    gint32 x2_g = (gint32) x2;
    GdkSegment_set_x2 (obj_g, x2_g);
}

static gint32 GdkSegment_get_y2 (GdkSegment * cptr) 
{
    return cptr->y2;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    getY2
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Segment_getY2 (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    return (jint) (GdkSegment_get_y2 (obj_g));
}

static void GdkSegment_set_y2 (GdkSegment * cptr, gint32 y2) 
{
    cptr->y2 = y2;
}

/*
 * Class:     org.gnu.gdk.Segment
 * Method:    setY2
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Segment_setY2 (JNIEnv *env, jclass klass, jobject obj, jint y2) 
{
    GdkSegment *obj_g = (GdkSegment *)getPointerFromHandle(env, obj);
    gint32 y2_g = (gint32) y2;
    GdkSegment_set_y2 (obj_g, y2_g);
}

#ifdef __cplusplus
}

#endif
