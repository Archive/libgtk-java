/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_Layout.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)pango_layout_get_type ();
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1new (JNIEnv *env, jclass cls, 
    jobject context) 
{
    PangoContext *context_g = (PangoContext *)getPointerFromHandle(env, context);
    return getGObjectHandle(env, (GObject *) pango_layout_new (context_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_copy
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1copy (JNIEnv *env, jclass cls, 
    jobject src) 
{
    PangoLayout *src_g = (PangoLayout *)getPointerFromHandle(env, src);
    return getGObjectHandle(env, G_OBJECT(pango_layout_copy (src_g)));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_context
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1context (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return getGObjectHandle(env, G_OBJECT(pango_layout_get_context (layout_g)));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_attributes
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1attributes (JNIEnv *env, 
    jclass cls, jobject layout, jobject attrs) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoAttrList *attrs_g = (PangoAttrList *)getPointerFromHandle(env, attrs);
    pango_layout_set_attributes (layout_g, attrs_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_attributes
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1attributes (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return getGBoxedHandle(env, pango_layout_get_attributes (layout_g),
                           PANGO_TYPE_ATTR_LIST,
                           (GBoxedCopyFunc) pango_attr_list_ref, 
                           (JGFreeFunc)pango_attr_list_unref);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1text (JNIEnv *env, jclass 
    cls, jobject layout, jstring text, jint length) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, 0);
    pango_layout_set_text (layout_g, text_g, (gint32)length);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1text (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    const gchar *result_g = pango_layout_get_text (layout_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1markup (JNIEnv *env, 
    jclass cls, jobject layout, jstring markup, jint length) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    const gchar* markup_g = (*env)->GetStringUTFChars(env, markup, 0);
    pango_layout_set_markup (layout_g, markup_g, (gint32)length);
    (*env)->ReleaseStringUTFChars(env, markup, markup_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_markup_with_accel
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1markup_1with_1accel (
    JNIEnv *env, jclass cls, jobject layout, jstring markup, jint length, jbyte accelMarker, 
    jbyte accelChar) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    const gchar* markup_g = (*env)->GetStringUTFChars(env, markup, 0);
    pango_layout_set_markup_with_accel (layout_g, markup_g, (gint32)length, (gunichar)accelMarker, 
	        (gunichar*)&accelChar);
   	(*env)->ReleaseStringUTFChars(env, markup, markup_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_font_description
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1font_1description (JNIEnv 
    *env, jclass cls, jobject layout, jobject desc) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoFontDescription *desc_g = (PangoFontDescription *)getPointerFromHandle(env, desc);
    pango_layout_set_font_description (layout_g, desc_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_width
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1width (JNIEnv *env, jclass 
    cls, jobject layout, jint width) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 width_g = (gint32) width;
    pango_layout_set_width (layout_g, width_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1width (JNIEnv *env, jclass 
    cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_width (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_wrap
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1wrap (JNIEnv *env, jclass 
    cls, jobject layout, jint wrap) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoWrapMode wrap_g = (PangoWrapMode) wrap;
    pango_layout_set_wrap (layout_g, wrap_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_wrap
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1wrap (JNIEnv *env, jclass 
    cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_wrap (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_indent
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1indent (JNIEnv *env, 
    jclass cls, jobject layout, jint indent) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 indent_g = (gint32) indent;
    pango_layout_set_indent (layout_g, indent_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_indent
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1indent (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_indent (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_spacing
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1spacing (JNIEnv *env, 
    jclass cls, jobject layout, jint spacing) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 spacing_g = (gint32) spacing;
    pango_layout_set_spacing (layout_g, spacing_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1spacing (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_spacing (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_justify
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1justify (JNIEnv *env, 
    jclass cls, jobject layout, jboolean justify) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gboolean justify_g = (gboolean) justify;
    pango_layout_set_justify (layout_g, justify_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_justify
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1justify (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jboolean) (pango_layout_get_justify (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_alignment
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1alignment (JNIEnv *env, 
    jclass cls, jobject layout, jint alignment) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoAlignment alignment_g = (PangoAlignment) alignment;
    pango_layout_set_alignment (layout_g, alignment_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_alignment
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1alignment (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_alignment (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_tabs
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1tabs (JNIEnv *env, jclass 
    cls, jobject layout, jobject tabs) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoTabArray *tabs_g = (PangoTabArray *)getPointerFromHandle(env, tabs);
    pango_layout_set_tabs (layout_g, tabs_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_tabs
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1tabs (JNIEnv *env, jclass 
    cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoTabArray *tabs = pango_layout_get_tabs (layout_g);
    if ( tabs ) {
        return getGBoxedHandle(env, tabs,
                               PANGO_TYPE_TAB_ARRAY, NULL,
                               (JGFreeFunc)pango_tab_array_free);
    } else {
        return NULL;
    }
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_set_single_paragraph_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1set_1single_1paragraph_1mode (
    JNIEnv *env, jclass cls, jobject layout, jboolean setting) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gboolean setting_g = (gboolean) setting;
    pango_layout_set_single_paragraph_mode (layout_g, setting_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_single_paragraph_mode
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_pango_Layout_pango_1layout_1get_1single_1paragraph_1mode (JNIEnv *env, jclass cls, 
    jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jboolean) (pango_layout_get_single_paragraph_mode (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_context_changed
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1context_1changed (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    pango_layout_context_changed (layout_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_index_to_pos
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1index_1to_1pos (JNIEnv *env, 
    jclass cls, jobject layout, jint index) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 index_g = (gint32) index;
    PangoRectangle *pos_g = g_new( PangoRectangle, 1 );
    pango_layout_index_to_pos (layout_g, index_g, pos_g);
    return getStructHandle(env, pos_g, NULL, g_free);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_cursor_pos_strong
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1cursor_1pos_1strong (JNIEnv *env, 
    jclass cls, jobject layout, jint index) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 index_g = (gint32) index;
    PangoRectangle *pos = g_new( PangoRectangle, 1 );
    pango_layout_get_cursor_pos(layout_g, index_g, pos, NULL);
    return getStructHandle(env, pos, NULL, g_free);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_cursor_pos_weak
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1cursor_1pos_1weak (JNIEnv *env, 
    jclass cls, jobject layout, jint index) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 index_g = (gint32) index;
    PangoRectangle *pos = g_new( PangoRectangle, 1 );
    pango_layout_get_cursor_pos(layout_g, index_g, NULL, pos);
    return getStructHandle(env, pos, NULL, g_free);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_move_cursor_visually
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1move_1cursor_1visually (JNIEnv 
    *env, jclass cls, jobject layout, jboolean strong, jint oldIndex, jint oldTrailing, jint 
    direction, jintArray newIndex, jintArray newTrailing) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gboolean strong_g = (gboolean) strong;
    gint32 oldIndex_g = (gint32) oldIndex;
    gint32 oldTrailing_g = (gint32) oldTrailing;
    gint32 direction_g = (gint32) direction;
    gint *newIndex_g = (gint *) (*env)->GetIntArrayElements (env, newIndex, NULL);
    gint *newTrailing_g = (gint *) (*env)->GetIntArrayElements (env, newTrailing, NULL);
    pango_layout_move_cursor_visually (layout_g, strong_g, oldIndex_g, oldTrailing_g, 
            direction_g, newIndex_g, newTrailing_g);
    (*env)->ReleaseIntArrayElements (env, newIndex, (jint *) newIndex_g, 0);
    (*env)->ReleaseIntArrayElements (env, newTrailing, (jint *) newTrailing_g, 0);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_xy_to_index
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_pango_Layout_pango_1layout_1xy_1to_1index (JNIEnv *env, 
    jclass cls, jobject layout, jint x, jint y, jintArray index, jintArray trailing) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 x_g = (gint32) x;
    gint32 y_g = (gint32) y;
    gint *index_g = (gint *) (*env)->GetIntArrayElements (env, index, NULL);
    gint *trailing_g = (gint *) (*env)->GetIntArrayElements (env, trailing, NULL);
    jboolean result_j = (jboolean) (pango_layout_xy_to_index (layout_g, x_g, y_g, index_g, 
                trailing_g));
    (*env)->ReleaseIntArrayElements (env, index, (jint *) index_g, 0);
    (*env)->ReleaseIntArrayElements (env, trailing, (jint *) trailing_g, 0);
    return result_j;
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_extents
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1extents (JNIEnv *env, 
    jclass cls, jobject layout, jobject inkRect, jobject logicalRect) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
    pango_layout_get_extents (layout_g, inkRect_g, logicalRect_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_pixel_extents
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1pixel_1extents (JNIEnv 
    *env, jclass cls, jobject layout, jobject inkRect, jobject logicalRect) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    PangoRectangle *inkRect_g = (PangoRectangle *)getPointerFromHandle(env, inkRect);
    PangoRectangle *logicalRect_g = (PangoRectangle *)getPointerFromHandle(env, logicalRect);
    pango_layout_get_pixel_extents (layout_g, inkRect_g, logicalRect_g);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_size
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1size (JNIEnv *env, jclass 
    cls, jobject layout, jintArray width, jintArray height) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    pango_layout_get_size (layout_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_pixel_size
 */
JNIEXPORT void JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1pixel_1size (JNIEnv *env, 
    jclass cls, jobject layout, jintArray width, jintArray height) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint *width_g = (gint *) (*env)->GetIntArrayElements (env, width, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    pango_layout_get_pixel_size (layout_g, width_g, height_g);
    (*env)->ReleaseIntArrayElements (env, width, (jint *) width_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_line_count
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1line_1count (JNIEnv *env, 
    jclass cls, jobject layout) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    return (jint) (pango_layout_get_line_count (layout_g));
}

/*
 * Class:     org.gnu.pango.Layout
 * Method:    pango_layout_get_line
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_Layout_pango_1layout_1get_1line (JNIEnv *env, jclass 
    cls, jobject layout, jint line) 
{
    PangoLayout *layout_g = (PangoLayout *)getPointerFromHandle(env, layout);
    gint32 line_g = (gint32) line;
    PangoLayoutLine *layoutline = pango_layout_get_line (layout_g, line_g);
    if ( line ) {
        return getStructHandle(env, layoutline,
                               (JGCopyFunc)pango_layout_line_ref, 
                               (JGFreeFunc)pango_layout_line_unref);
    } else {
        return NULL;
    }
}


#ifdef __cplusplus
}

#endif
