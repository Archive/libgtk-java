/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Colormap.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1get_1type 
	(JNIEnv *env, jclass cls) 
{
    return (jint)gdk_colormap_get_type ();
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1new 
	(JNIEnv *env, jclass cls, jobject visual, jboolean allocate) 
{
	GdkVisual *visual_g;
	GdkColormap *colormap;
	
    visual_g = (GdkVisual *)getPointerFromHandle(env, visual);
    colormap = gdk_colormap_new(visual_g, allocate);
    
    return getGObjectHandle(env, (GObject*)colormap);
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_get_system
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1get_1system 
	(JNIEnv *env, jclass cls) 
{
	GdkColormap *colormap;
	
	colormap = gdk_colormap_get_system();
	
    return getGObjectHandle(env, (GObject*)colormap);
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_alloc_colors
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1alloc_1colors 
	(JNIEnv *env, jclass cls, jobject colormap, jobjectArray colors, 
	jboolean writable, jboolean bestMatch, jbooleanArray success) 
{
	GdkColormap *colormap_g;
	GdkColor *colors_g;
    gboolean *success_g;
	jsize len;
    jint result;
    
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    colors_g = (GdkColor *)getPointerArrayFromHandles(env, colors);
    success_g = (gboolean *) (*env)->GetBooleanArrayElements(env, success, NULL);
    len = (*env)->GetArrayLength(env, colors);
    
    result = (jint)gdk_colormap_alloc_colors (colormap_g, colors_g, len, 
                writable, bestMatch, success_g);
    (*env)->ReleaseBooleanArrayElements(env, success, (jboolean *)success_g, 0);
    return result;
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_alloc_color
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1alloc_1color 
	(JNIEnv *env, jclass cls, jobject colormap, jobject color, 
	jboolean writable, jboolean best_match) 
{
	GdkColormap *colormap_g;
	GdkColor *color_g;
	jboolean result;
	
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    color_g = (GdkColor *)getPointerFromHandle(env, color);
    result = (jboolean) gdk_colormap_alloc_color (colormap_g, color_g, 
                writable, best_match);
    return result;
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_free_colors
 */
JNIEXPORT void JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1free_1colors 
	(JNIEnv *env, jclass cls, jobject colormap, jobjectArray colors) 
{
	GdkColormap *colormap_g;
	GdkColor *colors_g;
    
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    colors_g = (GdkColor *)getPointerArrayFromHandles(env, colors);
    gdk_colormap_free_colors (colormap_g, colors_g, (*env)->GetArrayLength(env, colors));
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_query_color
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1query_1color 
	(JNIEnv *env, jclass cls, jobject colormap, jlong pixel) 
{
	GdkColormap *colormap_g;
	GdkColor *result;
	
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    result = g_new(GdkColor, 1);
    gdk_colormap_query_color (colormap_g, pixel, result);
    return getGBoxedHandle(env, (GObject *) result, GDK_TYPE_COLOR, NULL,
    		(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gdk.Colormap
 * Method:    gdk_colormap_get_visual
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1get_1visual 
	(JNIEnv *env, jclass cls, jobject colormap) 
{
	GdkColormap *colormap_g;
	GdkVisual *visual;
	
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    visual = gdk_colormap_get_visual(colormap_g);
    
    return getGObjectHandle(env, (GObject*)visual);
}

/*
 * Class:     org_gnu_gdk_Colormap
 * Method:    gdk_colormap_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Colormap_gdk_1colormap_1get_1screen
  (JNIEnv *env, jclass cls, jobject colormap)
{
	GdkColormap *colormap_g;
	GdkScreen *screen;
	
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);
    screen = gdk_colormap_get_screen(colormap_g);
    
    return getGObjectHandle(env, (GObject*)screen);
}

static jobject getColorHandle( JNIEnv *env, gpointer color ) {
    return getGBoxedHandle(env, color, (GType) GDK_TYPE_COLOR, 
                           (GBoxedCopyFunc)gdk_color_copy, 
                           (GBoxedFreeFunc)gdk_color_free);
}

/*
 * Class:     org_gnu_gdk_Colormap
 * Method:    get_colors
 * Signature: (Lorg/gnu/glib/Handle;)[Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gdk_Colormap_get_1colors
  (JNIEnv *env, jclass cls, jobject colormap)
{
	GdkColormap *colormap_g;
	
    colormap_g = (GdkColormap *)getPointerFromHandle(env, colormap);

    return getGBoxedHandlesFromPointers(env, (void**)colormap_g->colors, 
                                        colormap_g->size, 
                                        (GetHandleFunc)getColorHandle);
}

#ifdef __cplusplus
}

#endif
