/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrLanguage.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.AttrLanguage
 * Method:    getValue
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrLanguage_getValue 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrLanguage *obj_g = 
        (PangoAttrLanguage *)getPointerFromHandle(env, obj);
    return getGBoxedHandle(env, obj_g->value, PANGO_TYPE_LANGUAGE,
                           NULL, NULL);
}

#ifdef __cplusplus
}

#endif
