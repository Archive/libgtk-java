/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellRendererPixbuf.h"
#ifdef __cplusplus
extern "C" 
{
#endif



JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererPixbuf_gtk_1setPixbuf (
    JNIEnv *env, jclass cls, jobject renderer, jobject pixbuf) 
{
    GtkCellRendererPixbuf *renderer_g = 
    		(GtkCellRendererPixbuf *)getPointerFromHandle(env, renderer);
	GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
	g_object_set(renderer_g, "pixbuf", pixbuf_g, NULL);
}

JNIEXPORT void JNICALL 
Java_org_gnu_gtk_CellRendererPixbuf_gtk_1setPixbufOpen (
    JNIEnv *env, jclass cls, jobject renderer, jobject pixbuf) 
{
    GtkCellRendererPixbuf *renderer_g = 
    		(GtkCellRendererPixbuf *)getPointerFromHandle(env, renderer);
	GdkPixbuf *pixbuf_g = (GdkPixbuf *)getPointerFromHandle(env, pixbuf);
	g_object_set(renderer_g, "pixbuf-expander-open", pixbuf_g, NULL);
}


/*
 * Class:     org.gnu.gtk.CellRendererPixbuf
 * Method:    gtk_cell_renderer_pixbuf_get_type
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_gtk_CellRendererPixbuf_gtk_1cell_1renderer_1pixbuf_1get_1type (JNIEnv *env, jclass 
    cls) 
{
    return (jint)gtk_cell_renderer_pixbuf_get_type ();
}

/*
 * Class:     org.gnu.gtk.CellRendererPixbuf
 * Method:    gtk_cell_renderer_pixbuf_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererPixbuf_gtk_1cell_1renderer_1pixbuf_1new (
    JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_cell_renderer_pixbuf_new ());
}


#ifdef __cplusplus
}

#endif
