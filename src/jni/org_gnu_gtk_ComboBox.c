/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_ComboBox
#define _Included_org_gnu_gtk_ComboBox
#include "org_gnu_gtk_ComboBox.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_combo_box_get_type();
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_combo_box_new());
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_new_with_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1new_1with_1model
  (JNIEnv *env, jclass cls, jobject model)
{
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	return getGObjectHandle(env, (GObject *) gtk_combo_box_new_with_model(model_g));
}

/*  
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_wrap_width 
 */ 
 JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1wrap_1width
    (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jint)gtk_combo_box_get_wrap_width(combo_g);
}
 
/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_wrap_width
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1wrap_1width
  (JNIEnv *env, jclass cls, jobject combo, jint width)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_wrap_width(combo_g, (gint)width);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_row_span_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1row_1span_1column
    (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jint)gtk_combo_box_get_row_span_column(combo_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_row_span_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1row_1span_1column
  (JNIEnv *env, jclass cls, jobject combo, jint rowSpan)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_row_span_column(combo_g, (gint)rowSpan);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_column_span_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1column_1span_1column
    (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jint)gtk_combo_box_get_column_span_column(combo_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_column_span_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1column_1span_1column
  (JNIEnv *env, jclass cls, jobject combo, jint columnSpan)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_column_span_column(combo_g, (gint)columnSpan);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_add_tearoffs
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1add_1tearoffs
    (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jboolean)gtk_combo_box_get_add_tearoffs(combo_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_add_tearoffs
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1add_1tearoffs
  (JNIEnv * env, jclass cls, jobject combo, jboolean addTearoffs)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_add_tearoffs(combo_g, addTearoffs);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_focus_on_click
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1focus_1on_1click
    (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jboolean)gtk_combo_box_get_focus_on_click(combo_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_focus_on_click
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1focus_1on_1click
  (JNIEnv * env, jclass cls, jobject combo, jboolean focusOnClick)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_focus_on_click(combo_g, focusOnClick);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_active
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1active
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (jint)gtk_combo_box_get_active(combo_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_active
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1active
  (JNIEnv *env, jclass cls, jobject combo, jint index)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_set_active(combo_g, (gint)index);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_active_iter
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1active_1iter
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox *combo_g;
	GtkTreeIter *iter_g;
	gboolean ret_g;
	combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
    iter_g = g_new( GtkTreeIter, 1 );
    ret_g = gtk_combo_box_get_active_iter(combo_g, iter_g);
    if (ret_g)	{
    	return getGBoxedHandle(env, iter_g, GTK_TYPE_TREE_ITER, NULL,
        		(GBoxedFreeFunc) gtk_tree_iter_free);
    }
    else	{
    	gtk_tree_iter_free(iter_g);
    	return NULL;
    }
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_active_iter
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1active_1iter
  (JNIEnv *env, jclass cls, jobject combo, jobject iter)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	GtkTreeIter* iter_g = (GtkTreeIter*)getPointerFromHandle(env, iter);
	gtk_combo_box_set_active_iter(combo_g, iter_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_set_model
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1model
  (JNIEnv *env, jclass cls, jobject combo, jobject model)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	gtk_combo_box_set_model(combo_g, model_g);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1model
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return getGObjectHandle(env, (GObject *) gtk_combo_box_get_model(combo_g));
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_new_text
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1new_1text
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_combo_box_new_text());
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_append_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1append_1text
  (JNIEnv *env, jclass cls, jobject combo, jstring text)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	const gchar* t = (gchar*)(*env)->GetStringUTFChars(env, text, NULL);
	gtk_combo_box_append_text(combo_g, t);
	(*env)->ReleaseStringUTFChars(env, text, t);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_insert_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1insert_1text
  (JNIEnv *env, jclass cls, jobject combo, int position, jstring text)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	const gchar* t = (gchar*)(*env)->GetStringUTFChars(env, text, NULL);
	gtk_combo_box_insert_text(combo_g, (gint)position, t);
	(*env)->ReleaseStringUTFChars(env, text, t);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_prepend_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1prepend_1text
  (JNIEnv *env, jclass cls, jobject combo, jstring text)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	const gchar* t = (gchar*)(*env)->GetStringUTFChars(env, text, NULL);
	gtk_combo_box_prepend_text(combo_g, t);
	(*env)->ReleaseStringUTFChars(env, text, t);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_remove_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1remove_1text
  (JNIEnv *env, jclass cls, jobject combo, jint position)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_remove_text(combo_g, position);
}

/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_active_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1active_1text
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return (*env)->NewStringUTF(env, gtk_combo_box_get_active_text(combo_g));
}


/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_popup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1popup
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_popup(combo_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_popdown
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1popdown
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	gtk_combo_box_popdown(combo_g);
}
                                                                                
/*
 * Class:     org_gnu_gtk_ComboBox
 * Method:    gtk_combo_box_get_popup_accessible
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1get_1popup_1accessible
  (JNIEnv *env, jclass cls, jobject combo)
{
	GtkComboBox* combo_g = (GtkComboBox*)getPointerFromHandle(env, combo);
	return getGObjectHandle(env, (GObject *) gtk_combo_box_get_popup_accessible(combo_g));
}

static gboolean treeViewRowSeparatorFunc( GtkTreeModel *model, 
                                          GtkTreeIter *iter, 
                                          gpointer data ) {
    jobject modelHandle;
    jobject iterHandle;
	JGFuncCallbackRef *ref;
	ref = (JGFuncCallbackRef*) data;
	modelHandle = getGObjectHandle(ref->env, (GObject *) model);
	iterHandle = getGBoxedHandle(ref->env, iter, GTK_TYPE_TREE_ITER, NULL,
			(GBoxedFreeFunc) gtk_tree_iter_free);
	return (* ref->env)->CallBooleanMethod(ref->env, ref->obj, ref->methodID,
			modelHandle, iterHandle);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_ComboBox_gtk_1combo_1box_1set_1row_1separator_1func
(JNIEnv *env, jclass cls, jobject combo_box, jobject box, jstring callback)
{
    GtkComboBox * combo_box_g = 
        (GtkComboBox *)getPointerFromHandle(env, combo_box);

    if ( !box ) {
        gtk_combo_box_set_row_separator_func(combo_box_g, NULL, NULL, NULL);
        return;
    }

    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->obj = (* env)->NewGlobalRef(env, box);

    const char *funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetMethodID(env, 
                            (*env)->GetObjectClass(env, ref->obj), 
                            funcname, "(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Z" );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, callback, funcname);

    gtk_combo_box_set_row_separator_func(combo_box_g, treeViewRowSeparatorFunc, ref, NULL);
}


#ifdef __cplusplus
}
#endif
#endif
