/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_HScrollBar.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.HScrollBar
 * Method:    gtk_hscrollbar_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HScrollBar_gtk_1hscrollbar_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_hscrollbar_get_type ();
}

/*
 * Class:     org.gnu.gtk.HScrollBar
 * Method:    gtk_hscrollbar_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_HScrollBar_gtk_1hscrollbar_1new (JNIEnv *env, jclass 
    cls, jobject adjustment) 
{
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    return getGObjectHandle(env, (GObject *) gtk_hscrollbar_new (adjustment_g));
}


#ifdef __cplusplus
}

#endif
