/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrShape.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static PangoRectangle *pango_rectangle_new( PangoRectangle *orig ) {
    PangoRectangle *nr = g_new( PangoRectangle, 1 );
    nr->x = orig->x;
    nr->y = orig->y;
    nr->width = orig->width;
    nr->height = orig->height;
    return nr;
}

/*
 * Class:     org.gnu.pango.AttrShape
 * Method:    getInkRect
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrShape_getInkRect 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrShape *obj_g = (PangoAttrShape *)getPointerFromHandle(env, obj);
    return getStructHandle(env, &obj_g->ink_rect, 
                           (JGCopyFunc)pango_rectangle_new, 
                           (JGFreeFunc)g_free);
}

/*
 * Class:     org.gnu.pango.AttrShape
 * Method:    getLogicalRect
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrShape_getLogicalRect 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrShape *obj_g = (PangoAttrShape *)getPointerFromHandle(env, obj);
    return getStructHandle(env, &obj_g->logical_rect, 
                           (JGCopyFunc)pango_rectangle_new, 
                           (JGFreeFunc)g_free);
}

#ifdef __cplusplus
}

#endif
