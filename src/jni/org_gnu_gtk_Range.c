/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Range.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Range_gtk_1range_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_range_get_type ();
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_update_policy
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1update_1policy (JNIEnv *env, 
    jclass cls, jobject range, jint policy) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    GtkUpdateType policy_g = (GtkUpdateType) policy;
    gtk_range_set_update_policy (range_g, policy_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_get_update_policy
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Range_gtk_1range_1get_1update_1policy (JNIEnv *env, 
    jclass cls, jobject range) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    return (jint) (gtk_range_get_update_policy (range_g));
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_adjustment
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1adjustment (JNIEnv *env, jclass 
    cls, jobject range, jobject adjustment) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    GtkAdjustment *adjustment_g = (GtkAdjustment *)getPointerFromHandle(env, adjustment);
    gtk_range_set_adjustment (range_g, adjustment_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_get_adjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Range_gtk_1range_1get_1adjustment (JNIEnv *env, jclass 
    cls, jobject range) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    return getGObjectHandle(env, (GObject *) gtk_range_get_adjustment (range_g));
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_inverted
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1inverted (JNIEnv *env, jclass 
    cls, jobject range, jboolean setting) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    gboolean setting_g = (gboolean) setting;
    gtk_range_set_inverted (range_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_get_inverted
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Range_gtk_1range_1get_1inverted (JNIEnv *env, 
    jclass cls, jobject range) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    return (jboolean) (gtk_range_get_inverted (range_g));
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_increments
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1increments (JNIEnv *env, jclass 
    cls, jobject range, jdouble step, jdouble page) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    gdouble step_g = (gdouble) step;
    gdouble page_g = (gdouble) page;
    gtk_range_set_increments (range_g, step_g, page_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_range
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1range (JNIEnv *env, jclass cls, 
    jobject range, jdouble min, jdouble max) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    gdouble min_g = (gdouble) min;
    gdouble max_g = (gdouble) max;
    gtk_range_set_range (range_g, min_g, max_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_set_value
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Range_gtk_1range_1set_1value (JNIEnv *env, jclass cls, 
    jobject range, jdouble value) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    gdouble value_g = (gdouble) value;
    gtk_range_set_value (range_g, value_g);
}

/*
 * Class:     org.gnu.gtk.Range
 * Method:    gtk_range_get_value
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_gtk_Range_gtk_1range_1get_1value (JNIEnv *env, jclass 
    cls, jobject range) 
{
    GtkRange *range_g = (GtkRange *)getPointerFromHandle(env, range);
    return (jdouble) (gtk_range_get_value (range_g));
}

#ifdef __cplusplus
}

#endif
