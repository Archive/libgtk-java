/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TextBuffer.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static jobject getTextIter(JNIEnv* env, GtkTextIter* iter)
{
	return getGBoxedHandle(env, iter, 	GTK_TYPE_TEXT_ITER, (GBoxedCopyFunc)
			gtk_text_iter_copy, (GBoxedFreeFunc) gtk_text_iter_free);	
}
/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_text_buffer_get_type ();
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_new
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1new (JNIEnv *env, jclass cls, jobject table) 
{
    GtkTextTagTable *table_g = 
        (GtkTextTagTable *)getPointerFromHandle(env, table);
    return getGObjectHandle(env, (GObject *) gtk_text_buffer_new (table_g));
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_line_count
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1line_1count (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return (jint)gtk_text_buffer_get_line_count (buffer_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_char_count
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1char_1count (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return (jint) gtk_text_buffer_get_char_count (buffer_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_tag_table
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1tag_1table (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return getGObjectHandle(env, (GObject *) gtk_text_buffer_get_tag_table (buffer_g));
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_set_text
 * Signature: (I[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1set_1text (JNIEnv *env, jclass cls, jobject buffer, jstring text) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *utf = (*env)->GetStringUTFChars(env, text, NULL);
    jint len = (*env)->GetStringUTFLength(env, text);
    gtk_text_buffer_set_text (buffer_g, 
                              (gchar*)utf, (gint32)len);
    (*env)->ReleaseStringUTFChars(env, text, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert
 * Signature: (II[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jstring text) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter* iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    const char *utf = (*env)->GetStringUTFChars(env, text, NULL);
    jint len = (*env)->GetStringUTFLength(env, text);
    gtk_text_buffer_insert (buffer_g, iter_g, 
                            (gchar*)utf, (gint32)len);
    (*env)->ReleaseStringUTFChars(env, text, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_at_cursor
 * Signature: (I[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1at_1cursor (JNIEnv *env, jclass cls, jobject buffer, jstring text) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *utf = (*env)->GetStringUTFChars(env, text, NULL);
    jint len = (*env)->GetStringUTFLength(env, text);
    gtk_text_buffer_insert_at_cursor (buffer_g, 
                                      (gchar*)utf, (gint32)len);
    (*env)->ReleaseStringUTFChars(env, text, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_interactive
 * Signature: (II[BIZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1interactive (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jstring text, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    const char *utf = (*env)->GetStringUTFChars(env, text, NULL);
    jint len = (*env)->GetStringUTFLength(env, text);
    gboolean defaultEditable_g = (gboolean) defaultEditable;
    jboolean result_j = 
        (jboolean) (gtk_text_buffer_insert_interactive (buffer_g, iter_g, 
                                                        (gchar*)utf, 
                                                        (gint32)len, 
                                                        defaultEditable_g));
    (*env)->ReleaseStringUTFChars(env, text, utf);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_interactive_at_cursor
 * Signature: (I[BIZ)Z
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1interactive_1at_1cursor (JNIEnv *env, jclass cls, jobject buffer, jstring text, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *utf = (*env)->GetStringUTFChars(env, text, NULL);
    jint len = (*env)->GetStringUTFLength(env, text);
    gboolean defaultEditable_g = (gboolean) defaultEditable;
    jboolean result_j = 
        (jboolean)
        (gtk_text_buffer_insert_interactive_at_cursor (buffer_g, 
                                                       (gchar*)utf, 
                                                       (gint32)len, 
                                                       defaultEditable_g));
    (*env)->ReleaseStringUTFChars(env, text, utf);
    return result_j;
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_range
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1range (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_insert_range (buffer_g, iter_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_range_interactive
 * Signature: (IIIIZ)Z
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1range_1interactive (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jobject start, jobject end, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gboolean defaultEditable_g = (gboolean) defaultEditable;
    return 
        (jboolean) 
        gtk_text_buffer_insert_range_interactive(buffer_g, 
                                                 iter_g, start_g, end_g,
                                                 defaultEditable_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_delete
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1delete (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_delete (buffer_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_delete_interactive
 * Signature: (IIIZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1delete_1interactive (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gboolean defaultEditable_g = (gboolean) defaultEditable;
    return (jboolean) gtk_text_buffer_delete_interactive (buffer_g, 
                                                          start_g, end_g, 
                                                          defaultEditable_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_text
 * Signature: (IIIZ)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1text (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end, jboolean includeHiddenChars) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gboolean includeHiddenChars_g = (gboolean)includeHiddenChars;
    gchar *result_g = gtk_text_buffer_get_text (buffer_g, start_g, 
                                                end_g, includeHiddenChars_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_slice
 * Signature: (IIIZ)[B
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1slice (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end, jboolean includeHiddenChars) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gboolean includeHiddenChars_g = (gboolean)includeHiddenChars;
    gchar *result_g = gtk_text_buffer_get_slice (buffer_g, start_g, 
                                                 end_g, includeHiddenChars_g);
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_pixbuf
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1pixbuf (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jobject pixbuf) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    GdkPixbuf *pixbuf_g = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
    gtk_text_buffer_insert_pixbuf (buffer_g, iter_g, pixbuf_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_insert_child_anchor
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1insert_1child_1anchor (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jobject anchor) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    GtkTextChildAnchor *anchor_g = 
        (GtkTextChildAnchor*)getPointerFromHandle(env, anchor);
    gtk_text_buffer_insert_child_anchor (buffer_g, iter_g, anchor_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_create_child_anchor
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1create_1child_1anchor (JNIEnv *env, jclass cls, jobject buffer, jobject iter) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    return 
        getGObjectHandleAndRef(env, (GObject *)
                             gtk_text_buffer_create_child_anchor (buffer_g, 
                                                                  iter_g));
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_create_mark
 * Signature: (I[BIZ)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1create_1mark (JNIEnv *env, jclass cls, jobject buffer, jstring markName, jobject where, jboolean leftGravity) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *where_g = (GtkTextIter*)getPointerFromHandle(env, where);
    const char *utf = (*env)->GetStringUTFChars(env, markName, NULL);
    GtkTextMark* result = gtk_text_buffer_create_mark (buffer_g, 
                                                       (gchar*)utf, 
                                                       where_g,
                                                       (gboolean)leftGravity);
    g_assert(result != NULL);
    (*env)->ReleaseStringUTFChars(env, markName, utf);
    return getGObjectHandleAndRef(env, (GObject *) result);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_move_mark
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1move_1mark (JNIEnv *env, jclass cls, jobject buffer, jobject mark, jobject where) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextMark *mark_g = (GtkTextMark*)getPointerFromHandle(env, mark);
    GtkTextIter *where_g = (GtkTextIter*)getPointerFromHandle(env, where);
    gtk_text_buffer_move_mark (buffer_g, 
                               mark_g, where_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_delete_mark
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1delete_1mark (JNIEnv *env, jclass cls, jobject buffer, jobject mark) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextMark *mark_g = (GtkTextMark*)getPointerFromHandle(env, mark);
    gtk_text_buffer_delete_mark (buffer_g, mark_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_mark
 * Signature: (I[B)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1mark (JNIEnv *env, jclass cls, jobject buffer, jstring name) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    GtkTextMark *mark = gtk_text_buffer_get_mark (buffer_g, 
                                                  (gchar*)utf);
    (*env)->ReleaseStringUTFChars(env, name, utf);
    return getGObjectHandleAndRef(env, (GObject *) mark);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_move_mark_by_name
 * Signature: (I[BI)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1move_1mark_1by_1name (JNIEnv *env, jclass cls, jobject buffer, jstring name, jobject where) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *where_g = (GtkTextIter*)getPointerFromHandle(env, where);
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_text_buffer_move_mark_by_name (buffer_g, 
                                       utf, where_g);
    (*env)->ReleaseStringUTFChars(env, name, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_delete_mark_by_name
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1delete_1mark_1by_1name (JNIEnv *env, jclass cls, jobject buffer, jstring name) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_text_buffer_delete_mark_by_name (buffer_g, utf);
    (*env)->ReleaseStringUTFChars(env, name, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_insert
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1insert (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return getGObjectHandleAndRef(env, (GObject *) gtk_text_buffer_get_insert (buffer_g));
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_selection_bound
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1selection_1bound (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return getGObjectHandleAndRef(env, (GObject *) gtk_text_buffer_get_selection_bound (buffer_g));
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_place_cursor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1place_1cursor (JNIEnv *env, jclass cls, jobject buffer, jobject where) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *where_g = (GtkTextIter*)getPointerFromHandle(env, where);
    gtk_text_buffer_place_cursor (buffer_g, where_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_apply_tag
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1apply_1tag (JNIEnv *env, jclass cls, jobject buffer, jobject tag, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextTag *tag_g = (GtkTextTag*)getPointerFromHandle(env, tag);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_apply_tag (buffer_g, tag_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_remove_tag
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1remove_1tag (JNIEnv *env, jclass cls, jobject buffer, jobject tag, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextTag *tag_g = (GtkTextTag*)getPointerFromHandle(env, tag);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_remove_tag (buffer_g, tag_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_apply_tag_by_name
 * Signature: (I[BII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1apply_1tag_1by_1name (JNIEnv *env, jclass cls, jobject buffer, jstring name, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_text_buffer_apply_tag_by_name (buffer_g, 
                                       utf, start_g, end_g);
    (*env)->ReleaseStringUTFChars(env, name, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_remove_tag_by_name
 * Signature: (I[BII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1remove_1tag_1by_1name (JNIEnv *env, jclass cls, jobject buffer, jstring name, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    const char *utf = (*env)->GetStringUTFChars(env, name, NULL);
    gtk_text_buffer_remove_tag_by_name (buffer_g, 
                                        utf, start_g, end_g);
    (*env)->ReleaseStringUTFChars(env, name, utf);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_remove_all_tags
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1remove_1all_1tags (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_remove_all_tags (buffer_g, start_g, end_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_iter_at_line_offset
 * Signature: (IIII)V
 */
//JNIEXPORT void JNICALL 
//Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1line_1offset (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jint lineNumber, jint charOffset) 
//{
//    GtkTextBuffer *buffer_g = 
//        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
//    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
//    gtk_text_buffer_get_iter_at_line_offset (buffer_g, iter_g,
//                                             (gint32)lineNumber, 
//                                             (gint32)charOffset);
//}

/* No java method */
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_iter_at_line_index
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1line_1index (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jint lineNumber, jint byteIndex) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    gtk_text_buffer_get_iter_at_line_index (buffer_g, iter_g,
                                            (gint32)lineNumber, 
                                            (gint32)byteIndex);
}
#endif

  /* Broken, and no visible API. commenting out*/
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_bounds
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1bounds (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    gtk_text_buffer_get_bounds (buffer_g, start_g, end_g);
}
#endif

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_modified
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1modified (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return (jboolean) gtk_text_buffer_get_modified(buffer_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_set_modified
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1set_1modified (JNIEnv *env, jclass cls, jobject buffer, jboolean setting) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    gtk_text_buffer_set_modified (buffer_g, 
                                  (gboolean)setting);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_add_selection_clipboard
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1add_1selection_1clipboard (JNIEnv *env, jclass cls, jobject buffer, jobject clipboard) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_text_buffer_add_selection_clipboard (buffer_g, clipboard_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_remove_selection_clipboard
 * Signature: (II)V
 */
JNIEXPORT void JNICALL 
Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1remove_1selection_1clipboard (JNIEnv *env, jclass cls, jobject buffer, jobject clipboard) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_text_buffer_remove_selection_clipboard (buffer_g, clipboard_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_cut_clipboard
 * Signature: (IIZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1cut_1clipboard (JNIEnv *env, jclass cls, jobject buffer, jobject clipboard, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_text_buffer_cut_clipboard (buffer_g, clipboard_g,
                                   (gboolean)defaultEditable);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_copy_clipboard
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1copy_1clipboard (JNIEnv *env, jclass cls, jobject buffer, jobject clipboard) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_text_buffer_copy_clipboard (buffer_g, clipboard_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_paste_clipboard
 * Signature: (IIIZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1paste_1clipboard (JNIEnv *env, jclass cls, jobject buffer, jobject clipboard, jobject overrideLocation, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    GtkTextIter *overrideLocation_g = 
        (GtkTextIter*)getPointerFromHandle(env, overrideLocation);
    gtk_text_buffer_paste_clipboard (buffer_g, clipboard_g,
                                     overrideLocation_g, 
                                     (gboolean)defaultEditable);
}

  /* Broken and not visible, commenting out */
#ifdef ZERO
/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_get_selection_bounds
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL 
Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1selection_1bounds (JNIEnv *env, jclass cls, jobject buffer, jobject start, jobject end) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter*)getPointerFromHandle(env, start);
    GtkTextIter *end_g = (GtkTextIter*)getPointerFromHandle(env, end);
    return (jboolean) gtk_text_buffer_get_selection_bounds(buffer_g, 
                                                           start_g, end_g);
}
#endif

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_delete_selection
 * Signature: (IZZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1delete_1selection (JNIEnv *env, jclass cls, jobject buffer, jboolean interactive, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return (jboolean) gtk_text_buffer_delete_selection (buffer_g, 
                                                        (gboolean)interactive, 
                                                        (gboolean)defaultEditable);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_begin_user_action
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1begin_1user_1action (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    gtk_text_buffer_begin_user_action (buffer_g);
}

/*
 * Class:     org.gnu.gtk.TextBuffer
 * Method:    gtk_text_buffer_end_user_action
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1end_1user_1action (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    gtk_text_buffer_end_user_action (buffer_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1select_1range (JNIEnv *env, jclass cls, jobject buffer, jobject ins, jobject bound) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *ins_g = (GtkTextIter*)getPointerFromHandle(env, ins);
    GtkTextIter *bound_g = (GtkTextIter*)getPointerFromHandle(env, bound);
    gtk_text_buffer_select_range ( buffer_g, ins_g, bound_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_insert_1with_1tag (JNIEnv *env, jclass cls, jobject buffer, jobject iter, jstring text, jstring tag) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter*)getPointerFromHandle(env, iter);
    const char *text_g = (*env)->GetStringUTFChars(env, text, NULL);
    const char *tag_g = (*env)->GetStringUTFChars(env, tag, NULL);
    gtk_text_buffer_insert_with_tags_by_name (buffer_g, iter_g, text_g, 
                                              -1, tag_g, NULL);
    (*env)->ReleaseStringUTFChars(env, text, text_g);
    (*env)->ReleaseStringUTFChars(env, tag, tag_g);
}
	
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_create_1tag
		(JNIEnv *env, jclass cls, jobject buffer, jstring name)
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    const char *name_g = (*env)->GetStringUTFChars(env, name, NULL);
    jobject retval = 
        getGObjectHandleAndRef(env, (GObject *) gtk_text_buffer_create_tag(buffer_g, 
                                                        name_g, NULL));
    (*env)->ReleaseStringUTFChars(env, name, name_g);
    return retval;
}
	
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1new_1noTable (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_text_buffer_new (NULL));
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1selection_1exists (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    return (jboolean)gtk_text_buffer_get_selection_bounds(buffer_g, NULL, NULL);
}
    
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1selection_1start
		(JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *start_g = (GtkTextIter *)g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_selection_bounds (buffer_g, 
                                          start_g, NULL);
    return getTextIter(env, start_g);
}
    
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1selection_1end
		(JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *end_g = (GtkTextIter *)g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_selection_bounds (buffer_g, 
                                          NULL, end_g);
    return getTextIter(env, end_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1line_1offset
		(JNIEnv *env, jclass cls, jobject buffer, jint lineNumber, jint charOffset) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter *) g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_iter_at_line_offset (buffer_g, 
                                             iter_g, 
                                             (gint32)lineNumber, 
                                             (gint32)charOffset);
    return getTextIter(env, iter_g);
}
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1offset
		(JNIEnv *env, jclass cls, jobject buffer, jint charOffset) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter *) g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_iter_at_offset (buffer_g, 
                                        iter_g, 
                                        (gint32)charOffset);
    return getTextIter(env, iter_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1line
		(JNIEnv *env, jclass cls, jobject buffer, jint lineNumber) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter iter_g;
    gtk_text_buffer_get_iter_at_line (buffer_g, 
                                      &iter_g, 
                                      (gint32)lineNumber);
    return getTextIter(env, &iter_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1mark
		(JNIEnv *env, jclass cls, jobject buffer, jobject mark) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextMark *mark_g = (GtkTextMark*)getPointerFromHandle(env, mark);
    GtkTextIter *iter_g = (GtkTextIter *) g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_iter_at_mark (buffer_g, 
                                      iter_g, 
                                      mark_g);
    return getTextIter(env, iter_g);
}
    
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1iter_1at_1child_1anchor
		(JNIEnv *env, jclass cls, jobject buffer, jobject anchor) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter *)g_malloc(sizeof(GtkTextIter));
    GtkTextChildAnchor *anchor_g = getPointerFromHandle(env, anchor);

    gtk_text_buffer_get_iter_at_child_anchor (buffer_g, 
                                              iter_g, 
                                              anchor_g);
    return getTextIter(env, iter_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1start_1iter
		(JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter *)g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_start_iter (buffer_g, iter_g);
    return getTextIter(env, iter_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1get_1end_1iter
		(JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkTextIter *iter_g = (GtkTextIter *)g_malloc(sizeof(GtkTextIter));
    gtk_text_buffer_get_end_iter (buffer_g, iter_g);
    return getTextIter(env, iter_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1paste_1clipboardInsertPosition
		(JNIEnv *env, jclass cls, jobject buffer, jobject clipboard, jboolean defaultEditable) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer*)getPointerFromHandle(env, buffer);
    GtkClipboard *clipboard_g = 
        (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_text_buffer_paste_clipboard (buffer_g, 
                                     clipboard_g, 
                                     NULL, 
                                     (gboolean)defaultEditable);
}

/* GTK 2.6 additions. */

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextBuffer_gtk_1text_1buffer_1backspace
(JNIEnv *env, jclass cls, jobject buffer, jobject iter, jboolean interactive, jboolean default_editable)
{
    GtkTextBuffer * buffer_g = 
        (GtkTextBuffer *)getPointerFromHandle(env, buffer);
    GtkTextIter * iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gboolean interactive_g = (gboolean)interactive;
    gboolean default_editable_g = (gboolean)default_editable;
    return (jboolean)gtk_text_buffer_backspace(buffer_g, iter_g, 
                                               interactive_g, 
                                               default_editable_g);
}

#ifdef __cplusplus
}

#endif
