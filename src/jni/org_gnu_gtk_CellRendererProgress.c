/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_CellRendererProgress
#define _Included_org_gnu_gtk_CellRendererProgress
#include "org_gnu_gtk_CellRendererProgress.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_CellRendererProgress
 * Method:    gtk_cell_renderer_progress_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellRendererProgress_gtk_1cell_1renderer_1progress_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_cell_renderer_progress_get_type();
}

/*
 * Class:     org_gnu_gtk_CellRendererProgress
 * Method:    gtk_cell_renderer_progress_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_CellRendererProgress_gtk_1cell_1renderer_1progress_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_cell_renderer_progress_new());
}

#ifdef __cplusplus
}
#endif
#endif
