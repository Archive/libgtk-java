/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_HBox.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.HBox
 * Method:    gtk_hbox_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HBox_gtk_1hbox_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_hbox_get_type ();
}

/*
 * Class:     org.gnu.gtk.HBox
 * Method:    gtk_hbox_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_HBox_gtk_1hbox_1new (JNIEnv *env, jclass cls, jboolean 
    homogenous, jint spacing) 
{
    gboolean homogenous_g = (gboolean) homogenous;
    gint32 spacing_g = (gint32) spacing;
    return getGObjectHandle(env, (GObject *) gtk_hbox_new (homogenous_g, spacing_g));
}


#ifdef __cplusplus
}

#endif
