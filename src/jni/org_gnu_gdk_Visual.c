/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Visual.h"
#ifdef __cplusplus
extern "C" 
{
#endif

static gint32 GdkVisual_get_depth (GdkVisual * cptr) 
{
    return cptr->depth;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getDepth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getDepth (JNIEnv *env, jclass cls, jobject obj) 
{
    GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_depth (obj_g));
}

static GdkByteOrder GdkVisual_get_byte_order (GdkVisual * cptr) 
{
    return cptr->byte_order;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getByteOrder
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getByteOrder (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_byte_order (obj_g));
}

static gint32 GdkVisual_get_colormap_size (GdkVisual * cptr) 
{
    return cptr->colormap_size;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getColormapSize
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getColormapSize (JNIEnv *env, jclass cls, jobject 
    obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_colormap_size (obj_g));
}

static gint32 GdkVisual_get_bits_per_rgb (GdkVisual * cptr) 
{
    return cptr->bits_per_rgb;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getBitsPerRgb
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getBitsPerRgb (JNIEnv *env, jclass cls, jobject obj)
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_bits_per_rgb (obj_g));
}

static guint32 GdkVisual_get_red_mask (GdkVisual * cptr) 
{
    return cptr->red_mask;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getRedMask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getRedMask (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_red_mask (obj_g));
}

static gint32 GdkVisual_get_red_shift (GdkVisual * cptr) 
{
    return cptr->red_shift;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getRedShift
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getRedShift (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_red_shift (obj_g));
}

static gint32 GdkVisual_get_red_prec (GdkVisual * cptr) 
{
    return cptr->red_prec;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getRedPrec
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getRedPrec (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_red_prec (obj_g));
}

static guint32 GdkVisual_get_green_mask (GdkVisual * cptr) 
{
    return cptr->green_mask;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getGreenMask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getGreenMask (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_green_mask (obj_g));
}

static gint32 GdkVisual_get_green_shift (GdkVisual * cptr) 
{
    return cptr->green_shift;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getGreenShift
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getGreenShift (JNIEnv *env, jclass cls, jobject obj)
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
	return (jint) (GdkVisual_get_green_shift (obj_g));
}

static gint32 GdkVisual_get_green_prec (GdkVisual * cptr) 
{
    return cptr->green_prec;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getGreenPrec
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getGreenPrec (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_green_prec (obj_g));
}

static guint32 GdkVisual_get_blue_mask (GdkVisual * cptr) 
{
    return cptr->blue_mask;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getBlueMask
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getBlueMask (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_blue_mask (obj_g));
}

static gint32 GdkVisual_get_blue_shift (GdkVisual * cptr) 
{
    return cptr->blue_shift;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getBlueShift
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getBlueShift (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_blue_shift (obj_g));
}

static gint32 GdkVisual_get_blue_prec (GdkVisual * cptr) 
{
    return cptr->blue_prec;
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    getBluePrec
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_getBluePrec (JNIEnv *env, jclass cls, jobject obj) 
{
	GdkVisual *obj_g = (GdkVisual *)getPointerFromHandle(env, obj);
    return (jint) (GdkVisual_get_blue_prec (obj_g));
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gdk_visual_get_type ();
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best_depth
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best_1depth (JNIEnv *env, 
    jclass cls) 
{
    return (jint) (gdk_visual_get_best_depth ());
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint) (gdk_visual_get_best_type ());
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_system
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1system (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, (GObject *)gdk_visual_get_system ());
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *)gdk_visual_get_best ());
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best_with_depth
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best_1with_1depth (JNIEnv 
    *env, jclass cls, jint depth) 
{
    gint32 depth_g = (gint32) depth;
    return getGObjectHandle(env, (GObject *)gdk_visual_get_best_with_depth (depth_g));
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best_with_type
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best_1with_1type (JNIEnv *env, 
    jclass cls, jint visualType) 
{
    GdkVisualType visualType_g = (GdkVisualType) visualType;
    return getGObjectHandle(env, (GObject *)gdk_visual_get_best_with_type (visualType_g));
}

/*
 * Class:     org.gnu.gdk.Visual
 * Method:    gdk_visual_get_best_with_both
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1best_1with_1both (JNIEnv *env, 
    jclass cls, jint depth, jint visualType) 
{
    gint32 depth_g = (gint32) depth;
    GdkVisualType visualType_g = (GdkVisualType) visualType;
    return getGObjectHandle(env, (GObject *)gdk_visual_get_best_with_both (depth_g, visualType_g));
}


/*
 * Class:     org_gnu_gdk_Visual
 * Method:    gdk_visual_get_screen
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Visual_gdk_1visual_1get_1screen
  (JNIEnv *env, jclass cls, jobject visual)
{
    GdkVisual *visual_g = (GdkVisual *)getPointerFromHandle(env, visual);
    return getGObjectHandle(env, (GObject *)gdk_visual_get_screen(visual_g));
}


#ifdef __cplusplus
}

#endif
