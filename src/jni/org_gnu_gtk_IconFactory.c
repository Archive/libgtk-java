/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IconFactory.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_icon_factory_get_type ();
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_icon_factory_new ());
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_add
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1add (JNIEnv *env, 
    jclass cls, jobject factory, jstring stockId, jobject iconSet) 
{
	GtkIconFactory* factory_g = (GtkIconFactory*)getPointerFromHandle(env, factory);
	GtkIconSet* iconSet_g = (GtkIconSet*)getPointerFromHandle(env, iconSet);
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    gtk_icon_factory_add (factory_g, stockId_g, iconSet_g);
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_lookup
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1lookup (JNIEnv *env, 
    jclass cls, jobject factory, jstring stockId) 
{
	GtkIconSet *icon_set;
	GtkIconFactory* factory_g = (GtkIconFactory*)getPointerFromHandle(env, factory);
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    icon_set = gtk_icon_factory_lookup (factory_g, stockId_g);
    jobject result = getGBoxedHandle(env, icon_set, GTK_TYPE_ICON_SET,
    		(GBoxedCopyFunc) gtk_icon_set_ref, (GBoxedFreeFunc) gtk_icon_set_unref);
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
    return result;
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_add_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1add_1default (JNIEnv 
    *env, jclass cls, jobject factory) 
{
	GtkIconFactory* factory_g = (GtkIconFactory*)getPointerFromHandle(env, factory);
    gtk_icon_factory_add_default (factory_g);
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_remove_default
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1remove_1default (JNIEnv 
    *env, jclass cls, jobject factory) 
{
	GtkIconFactory* factory_g = (GtkIconFactory*)getPointerFromHandle(env, factory);
    gtk_icon_factory_remove_default (factory_g);
}

/*
 * Class:     org.gnu.gtk.IconFactory
 * Method:    gtk_icon_factory_lookup_default
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconFactory_gtk_1icon_1factory_1lookup_1default (JNIEnv 
    *env, jclass cls, jstring stockId) 
{
	GtkIconSet *icon_set;
    gchar* stockId_g = (gchar*)(*env)->GetStringUTFChars(env, stockId, 0);
    icon_set = gtk_icon_factory_lookup_default (stockId_g);
    jobject result = getGBoxedHandle(env, icon_set, GTK_TYPE_ICON_SET,
    		(GBoxedCopyFunc) gtk_icon_set_ref, (GBoxedFreeFunc) gtk_icon_set_unref);
    (*env)->ReleaseStringUTFChars(env, stockId, stockId_g);
	return result;
}


#ifdef __cplusplus
}

#endif
