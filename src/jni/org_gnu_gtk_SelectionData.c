/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include <string.h>
#include "gtk_java.h"

#include "org_gnu_gtk_SelectionData.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getSelection
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SelectionData_getSelection
		(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return getHandleFromPointer(env, data_g->selection);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getTarget
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SelectionData_getTarget(JNIEnv *env,
		jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return getHandleFromPointer(env, data_g->target);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getType
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SelectionData_getType(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return getHandleFromPointer(env, data_g->type);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getFormat
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_SelectionData_getFormat(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return (jint)data_g->format;
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getData
 */
JNIEXPORT jbyteArray JNICALL Java_org_gnu_gtk_SelectionData_getData(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);

    if (data_g->length == -1) {
        return (*env)->NewByteArray(env, 0);
    }

    jbyteArray ret = (*env)->NewByteArray(env, data_g->length);
    (*env)->SetByteArrayRegion(env, ret, 0, data_g->length, data_g->data);
    return ret;

}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    setData
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_SelectionData_setData(JNIEnv *env, jclass cls, jobject selection_data, jobject type, jint format, jbyteArray data, jint length)
{
    guchar* data_g;
    GtkSelectionData *selection_data_g =
        (GtkSelectionData*)getPointerFromHandle(env, selection_data);
    GdkAtom type_g =
        (GdkAtom)getPointerFromHandle(env, type);

    data_g = (guchar*)(*env)->GetByteArrayElements(env, data, 0);

    gtk_selection_data_set(selection_data_g, type_g, (int) format, data_g, (int) length);

    (*env)->ReleaseByteArrayElements(env, data, (jbyte*)data_g, 0);
}


/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getLength
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_SelectionData_getLength(JNIEnv *env, jclass cls, jobject data) 
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return (jint) data_g->length;
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    getDisplay
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SelectionData_getDisplay(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return getGObjectHandle(env, (GObject *) data_g->display);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_get_text
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1get_1text(JNIEnv *env, jclass cls, jobject data) 
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    guchar *txt;
    jstring retval;
    txt = gtk_selection_data_get_text (data_g);
    if (txt != NULL){
        retval = (*env)->NewStringUTF( env, (char *) txt );
        g_free(txt);
    }else
        retval = (*env)->NewStringUTF( env, "" );
    return retval;
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_set_text
 * Signature: (ILjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1set_1text(JNIEnv *env, jclass cls, jobject data, jstring text) 
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    const gchar * text_g = (*env)->GetStringUTFChars(env, text, NULL);
    jboolean retval = 
        (jboolean) gtk_selection_data_set_text( data_g, text_g, -1 );
    (*env)->ReleaseStringUTFChars( env, text, text_g );
    return retval;
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_get_targets
 * Signature: (I)[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1get_1targets
		(JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    gboolean success;
    GdkAtom* entries = NULL;
    gint numEntries;
    gint index;
    jobjectArray array;
	
    success = gtk_selection_data_get_targets(data_g, &entries, &numEntries);
    if (FALSE == success)
        return NULL;
    //array = (*env)->NewIntArray(env, numEntries);
    array = getHandleArray(env, numEntries);
    for (index = 0; index < numEntries; index++) {
    	(*env)->SetObjectArrayElement(env, array, index, 
                                      getHandleFromPointer(env, entries[index]));
    }
    //(*env)->SetIntArrayRegion(env, array, 0, numEntries, (jint*)entries);
    return array;
}

  /* No corresponding java method */
#ifdef ZERO
/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_targets_include_text
 * Signature: (I)Z
 */ 
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SelectionData_gtk_0selection_1data_1targets_1include_1text (JNIEnv *env, jclass cls, jobject data)
{
    GtkSelectionData *data_g = 
        (GtkSelectionData*)getPointerFromHandle(env, data);
    return (jboolean)gtk_selection_data_targets_include_text(data_g);
}
#endif

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1targets_1include_1image
(JNIEnv *env, jclass cls, jobject selection_data, jboolean writable)
{
    GtkSelectionData * selection_data_g = 
        (GtkSelectionData *)getPointerFromHandle(env, selection_data);
    gboolean writable_g = (gboolean)writable;
    return (jboolean)gtk_selection_data_targets_include_image(selection_data_g, writable_g);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_set_pixbuf
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1set_1pixbuf
  (JNIEnv *env, jclass cls, jobject data, jobject pixbuf)
{
   GtkSelectionData *data_g = (GtkSelectionData*)getPointerFromHandle(env, data);
   GdkPixbuf *pixbuf_g = (GdkPixbuf*)getPointerFromHandle(env, pixbuf);
   return (jboolean)gtk_selection_data_set_pixbuf(data_g, pixbuf_g);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_get_pixbuf
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1get_1pixbuf
  (JNIEnv *env, jclass cls, jobject data)
{
   	GtkSelectionData *data_g = (GtkSelectionData*)getPointerFromHandle(env, data);
   	return getGObjectHandle(env, (GObject *) gtk_selection_data_get_pixbuf(data_g));
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_set_uris
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1set_1uris
  (JNIEnv *env, jclass cls, jobject data, jobjectArray uris)
{
   	GtkSelectionData *data_g = (GtkSelectionData*)getPointerFromHandle(env, data);
	gchar** uris_g = getStringArray(env, uris);
	jboolean ret = (jboolean)gtk_selection_data_set_uris(data_g, uris_g);
	freeStringArray(env, uris, uris_g);
	return ret;
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    gtk_selection_data_get_uris
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_SelectionData_gtk_1selection_1data_1get_1uris
  (JNIEnv *env, jclass cls, jobject data)
{
   GtkSelectionData *data_g = (GtkSelectionData*)getPointerFromHandle(env, data);
   const gchar** str = (const gchar**)gtk_selection_data_get_uris(data_g);
   return getJavaStringArray(env, str);
}

/*
 * Class:     org_gnu_gtk_SelectionData
 * Method:    setTarget
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_SelectionData_setTarget
  (JNIEnv *env, jclass cls, jobject data, jobject atom)
{
	GtkSelectionData *data_g = (GtkSelectionData*)getPointerFromHandle(env, data);
	GdkAtom atom_g = (GdkAtom) getPointerFromHandle(env, atom);
	data_g->target = atom_g;
}

#ifdef __cplusplus
}

#endif
