/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeSortableHelper.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef struct{
	JNIEnv *env;
	jobject store;
	jmethodID methodID;
	jint column;
} JGComparisonRef;

/*
 * Class:     org_gnu_gtk_TreeSortableHelper
 * Method:    gtk_tree_sortable_set_sort_column_id
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSortableHelper_gtk_1tree_1sortable_1set_1sort_1column_1id(JNIEnv *env, jclass cls, jobject treeSortable, jint column_id, jint order) {

    GtkTreeSortable * treeSortable_g = 
        (GtkTreeSortable *)getPointerFromHandle(env, treeSortable);
    gtk_tree_sortable_set_sort_column_id( treeSortable_g, 
                                          (gint32)column_id, 
                                          (GtkSortType)order );
}

/*
 * Class:     org_gnu_gtk_TreeSortableHelper
 * Method:    gtk_tree_sortable_get_sort_column_id
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSortableHelper_gtk_1tree_1sortable_1get_1sort_1column_1id(JNIEnv *env, jclass cls, jobject treeSortable) {
    gint column_id;
    GtkSortType order;
    GtkTreeSortable *treeSortable_g = 
        (GtkTreeSortable*)getPointerFromHandle(env, treeSortable);
    gtk_tree_sortable_get_sort_column_id( treeSortable_g,
                                          &column_id, &order );
    // If no real column_id is set, column_id will be -1, or -2.
    return (jint)column_id;
}
/*
 * Class:     org_gnu_gtk_TreeSortableHelper
 * Method:    gtk_tree_sortable_get_sort_column_order
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSortableHelper_gtk_1tree_1sortable_1get_1sort_1column_1order(JNIEnv *env, jclass cls, jobject treeSortable) {
    gint column_id;
    GtkSortType order;
    GtkTreeSortable *treeSortable_g = 
        (GtkTreeSortable*)getPointerFromHandle(env, treeSortable);
    gboolean ret = gtk_tree_sortable_get_sort_column_id( treeSortable_g,
                                                         &column_id, &order );
    if ( ret ) {
        return (jint)order;
    } else {
        return -1;
    }
}

static gint listSortFunction( GtkTreeModel *model, GtkTreeIter *a, 
                              GtkTreeIter *b, gpointer data ) {
    jobject model_handle;
    jobject iter_a;
    jobject iter_b;
    JGComparisonRef *ref = (JGComparisonRef*) data;
    model_handle = getGObjectHandleAndRef(ref->env, (GObject *) model);
    iter_a = getGBoxedHandle(ref->env, a, GTK_TYPE_TREE_ITER, (GBoxedCopyFunc)
    		gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free);
    iter_b = getGBoxedHandle(ref->env, b, GTK_TYPE_TREE_ITER, (GBoxedCopyFunc)
    		gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free);
    return (* ref->env)->CallIntMethod( ref->env, 
                                        ref->store, 
                                        ref->methodID,
                                        model_handle,
                                        iter_a, 
                                        iter_b, 
                                        ref->column );

}

/*
 * Class:     org_gnu_gtk_TreeSortableHelper
 * Method:    gtk_tree_sortable_set_sort_func
 * Signature: (ILorg/gnu/gtk/TreeSortable;Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeSortableHelper_gtk_1tree_1sortable_1set_1sort_1func(JNIEnv *env, jclass cls, jobject treeSortable, jobject sortable, jstring compareFunc, jint col) {

    JGComparisonRef *ref = g_new( JGComparisonRef, 1 );
    ref->env = env;
    ref->store = (* env)->NewGlobalRef(env, sortable);
    ref->column = col;

    const char *funcname = (*env)->GetStringUTFChars(env, compareFunc, NULL);
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetMethodID(env, 
                            (*env)->GetObjectClass(env, ref->store), 
                            funcname, "(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;I)I" );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, compareFunc, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, compareFunc, funcname);

    GtkTreeSortable *treeSortable_g = 
        (GtkTreeSortable*)getPointerFromHandle(env, treeSortable);
    gtk_tree_sortable_set_sort_func( treeSortable_g, 
                                     (gint32)col, 
                                     listSortFunction, ref, NULL );
}

/*
 * Class:     org_gnu_gtk_TreeSortableHelper
 * Method:    gtk_tree_sortable_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeSortableHelper_gtk_1tree_1sortable_1get_1type(JNIEnv *env, jclass cls) {
    return gtk_tree_sortable_get_type();
}

#ifdef __cplusplus
}
#endif
