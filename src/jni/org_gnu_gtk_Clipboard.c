/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Clipboard.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1get
		(JNIEnv *env, jclass cls, jobject selection) 
{
    GdkAtom selection_g = (GdkAtom)getPointerFromHandle(env, selection);
    GtkClipboard *clipboard = gtk_clipboard_get (selection_g);
    return getPersistentGObjectHandle(env, (GObject *) clipboard);
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_get
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1get_1for_1display
		(JNIEnv *env, jclass cls, jobject display, jobject selection) 
{
    GdkAtom selection_g;
    GdkDisplay *display_g;
    GtkClipboard *clipboard;
    selection_g =  (GdkAtom) getPointerFromHandle(env, selection);
    display_g = (GdkDisplay *) getPointerFromHandle(env, display);
    clipboard = gtk_clipboard_get_for_display (display_g, selection_g);
    return getPersistentGObjectHandle(env, (GObject *) clipboard);
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_get_display
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1get_1display(JNIEnv *env, jclass cls, 
    jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    return getGObjectHandleAndRef(env, (GObject *) gtk_clipboard_get_display(clipboard_g));
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_get_owner
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1get_1owner (JNIEnv *env, 
    jclass cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    return getGObjectHandleAndRef(env, (GObject *) gtk_clipboard_get_owner (clipboard_g));
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_clear
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1clear (JNIEnv *env, jclass 
    cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gtk_clipboard_clear (clipboard_g);
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_set_text
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1set_1text (JNIEnv *env, 
    jclass cls, jobject clipboard, jstring text, jint len) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    gint32 len_g = (gint32) len;
    const gchar* text_g = (*env)->GetStringUTFChars(env, text, NULL);
	gtk_clipboard_set_text (clipboard_g, text_g, len_g);
	(*env)->ReleaseStringUTFChars(env, text, text_g);
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_set_image
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1set_1image
  (JNIEnv *env, jclass cls, jobject clipboard, jobject image)
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	GdkPixbuf* image_g = (GdkPixbuf*)getPointerFromHandle(env, image);
	gtk_clipboard_set_image(clipboard_g, image_g);
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_wait_for_text
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1wait_1for_1text (JNIEnv 
    *env, jclass cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	return (*env)->NewStringUTF(env,gtk_clipboard_wait_for_text (clipboard_g));
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_wait_for_image
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1wait_1for_1image
  (JNIEnv *env, jclass cls, jobject clipboard)
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	return getGObjectHandle(env, (GObject *) gtk_clipboard_wait_for_image(clipboard_g));
}

/*
 * Class:     org.gnu.gtk.Clipboard
 * Method:    gtk_clipboard_wait_is_text_available
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1wait_1is_1text_1available (
    JNIEnv *env, jclass cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    return (jboolean) (gtk_clipboard_wait_is_text_available (clipboard_g));
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_wait_is_image_available
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1wait_1is_1image_1available
  (JNIEnv *env, jclass cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
    return (jboolean)gtk_clipboard_wait_is_image_available(clipboard_g);
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_wait_is_target_available
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1wait_1is_1target_1available
  (JNIEnv *env, jclass cls, jobject clipboard, jobject target) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	GdkAtom* target_g = (GdkAtom*)getPointerFromHandle(env, target);
    return (jboolean)gtk_clipboard_wait_is_target_available(clipboard_g, *target_g);
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_set_can_store
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1set_1can_1store
  (JNIEnv *env, jclass cls, jobject clipboard, jobjectArray targets)
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	GtkTargetEntry* targets_g = (GtkTargetEntry*)getPointerArrayFromHandles(env, targets);
	jint length = (*env)->GetArrayLength(env, targets);
	gtk_clipboard_set_can_store(clipboard_g, targets_g, length);
}

/*
 * Class:     org_gnu_gtk_Clipboard
 * Method:    gtk_clipboard_store
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Clipboard_gtk_1clipboard_1store
  (JNIEnv *env, jclass cls, jobject clipboard) 
{
	GtkClipboard* clipboard_g = (GtkClipboard*)getPointerFromHandle(env, clipboard);
	gtk_clipboard_store(clipboard_g);
}

#ifdef __cplusplus
}

#endif
