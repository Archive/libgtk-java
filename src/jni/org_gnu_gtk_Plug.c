/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Plug.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Plug
 * Method:    gtk_plug_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Plug_gtk_1plug_1get_1type (JNIEnv *env, jclass cls) 
{
#ifdef GTK_WINDOWING_X11
    return (jint)gtk_plug_get_type ();
#else
	JNU_ThrowByName(env, "java.lang.RuntimeException", "Plug: Unsupported platform");
    return 0;
#endif
}

/*
 * Class:     org.gnu.gtk.Plug
 * Method:    gtk_plug_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Plug_gtk_1plug_1new (JNIEnv *env, jclass cls, jint 
    socketId) 
{
#ifdef GTK_WINDOWING_X11
    gint32 socketId_g = (gint32) socketId;
    return getGObjectHandle(env, (GObject *) gtk_plug_new (socketId_g));
#else
	JNU_ThrowByName(env, "java.lang.RuntimeException", "Plug: Unsupported platform");
    return NULL;
#endif
}

/*
 * Class:     org.gnu.gtk.Plug
 * Method:    gtk_plug_construct
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_Plug_gtk_1plug_1construct (JNIEnv *env, jclass cls, 
    jobject plug, jint socketId) 
{
#ifdef GTK_WINDOWING_X11
    GtkPlug *plug_g = (GtkPlug *)getPointerFromHandle(env, plug);
    gint32 socketId_g = (gint32) socketId;
    gtk_plug_construct (plug_g, socketId_g);
#endif
}

/*
 * Class:     org.gnu.gtk.Plug
 * Method:    gtk_plug_get_id
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Plug_gtk_1plug_1get_1id (JNIEnv *env, jclass cls, jobject 
    plug) 
{
#ifdef GTK_WINDOWING_X11
    GtkPlug *plug_g = (GtkPlug *)getPointerFromHandle(env, plug);
    return (jint)gtk_plug_get_id (plug_g);
#else
	JNU_ThrowByName(env, "java.lang.RuntimeException", "Plug: Unsupported platform");
    return 0;
#endif
}

#ifdef __cplusplus
}

#endif
