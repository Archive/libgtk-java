/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include "jg_jnu.h"
#include <sys/types.h>
#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>

#include "org_gnu_glib_CustomEvents.h"
#ifdef __cplusplus
extern "C" {
#endif


static volatile GSource *source;
//static volatile GMainContext *context;
static volatile jclass class;
static volatile jmethodID mid;
static volatile GSourceFuncs *source_funcs;
static volatile gboolean events_pending = FALSE;
static volatile GMainContext *mainContext;

static gboolean function( gpointer data ){
    JNIEnv *env = JG_JNU_GetEnv();
    jboolean exceptionThrown;
    
    (*env)->CallStaticVoidMethod(env, class, mid);

    exceptionThrown = (*env)->ExceptionCheck(env);
    if (exceptionThrown) {
		// We cannot throw this exception, since this timer was called by GLib.
		(*env)->ExceptionDescribe(env);  // ExceptionDescribe clears exception
    }
	return TRUE;
}

static gboolean prepare(GSource    *source, gint       *timeout_){
	return events_pending;
}
static gboolean check    (GSource    *source){
	return events_pending;
}
static gboolean dispatch (GSource    *source,
			GSourceFunc callback,
			gpointer    user_data){
	return callback( user_data );
}

JNIEXPORT void JNICALL Java_org_gnu_glib_CustomEvents_init 
    (JNIEnv *env, jclass cls)
{
    mid = (*env)->GetStaticMethodID(env, cls, "runEvents", "()V");
	class = (jclass) (*env)->NewGlobalRef(env, cls);

	gdk_threads_enter();		
	source_funcs = (GSourceFuncs *) g_new( GSourceFuncs, 1 );
	source_funcs->prepare = &prepare;
	source_funcs->check = &check;
	source_funcs->dispatch = &dispatch;
	source_funcs->finalize = NULL;
	
	source = g_source_new( (GSourceFuncs *) source_funcs, sizeof( GSource ) );
	mainContext = g_main_context_default();
	g_source_set_can_recurse( (GSource *) source, FALSE );
	g_source_set_callback( (GSource *) source, (GSourceFunc) function, NULL, NULL );
	g_source_attach( (GSource *) source, (GMainContext *) mainContext );
	g_source_set_priority( (GSource *) source, G_PRIORITY_HIGH_IDLE );
	gdk_threads_leave();
}

JNIEXPORT void JNICALL Java_org_gnu_glib_CustomEvents_setEventsPending
    (JNIEnv *env, jclass cls, jboolean pending )
{
	gdk_threads_enter();
	events_pending = pending;
	g_main_context_wakeup( (GMainContext *) mainContext );
	gdk_threads_leave();
}


JNIEXPORT void JNICALL Java_org_gnu_glib_CustomEvents_cleanup
    ( JNIEnv *env, jclass cls )
{
	gdk_threads_enter();
	g_source_destroy( (GSource *) source );	
	g_free( (GSourceFuncs *) source_funcs );
	gdk_threads_leave();
	
	(*env)->DeleteGlobalRef(env, class);
}

#ifdef __cplusplus
}
#endif
