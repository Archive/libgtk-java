/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_CellLayoutHelper.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1get_1type
  (JNIEnv *env, jclass cls)
{
    return (jint)gtk_cell_layout_get_type();
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_pack_start
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1pack_1start
  (JNIEnv *env, jclass cls, jobject layout, jobject renderer, jboolean expand)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    GtkCellRenderer *renderer_g = 
        (GtkCellRenderer*)getPointerFromHandle(env, renderer);
    gboolean expand_g = (gboolean)expand;
    gtk_cell_layout_pack_start(layout_g, renderer_g, expand_g);
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_pack_end
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1pack_1end
  (JNIEnv *env, jclass cls, jobject layout, jobject renderer, jboolean expand)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    GtkCellRenderer *renderer_g = 
        (GtkCellRenderer*)getPointerFromHandle(env, renderer);
    gboolean expand_g = (gboolean)expand;
    gtk_cell_layout_pack_end(layout_g, renderer_g, expand_g);
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_clear
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1clear
  (JNIEnv *env, jclass cls, jobject layout)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    gtk_cell_layout_clear(layout_g);
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_add_attribute
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1add_1attribute
  (JNIEnv *env, jclass cls, jobject layout, jobject renderer, jstring attribute, jint column)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    GtkCellRenderer *renderer_g = 
        (GtkCellRenderer*)getPointerFromHandle(env, renderer);
    const gchar *attribute_g = 
        (const gchar*)(*env)->GetStringUTFChars(env, attribute, NULL);
    gint column_g = (gint)column;
    gtk_cell_layout_add_attribute(layout_g, renderer_g, attribute_g, column_g);
    (*env)->ReleaseStringUTFChars(env, attribute, attribute_g);
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_clear_attributes
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1clear_1attributes
  (JNIEnv *env, jclass cls, jobject layout, jobject renderer)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    GtkCellRenderer *renderer_g = 
        (GtkCellRenderer*)getPointerFromHandle(env, renderer);
    gtk_cell_layout_clear_attributes(layout_g, renderer_g);
}

/*
 * Class:     org_gnu_gtk_CellLayoutHelper
 * Method:    gtk_cell_layout_reorder
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_CellLayoutHelper_gtk_1cell_1layout_1reorder
  (JNIEnv *env, jclass cls, jobject layout, jobject renderer, jint position)
{
    GtkCellLayout *layout_g = 
        (GtkCellLayout*)getPointerFromHandle(env, layout);
    GtkCellRenderer *renderer_g = 
        (GtkCellRenderer*)getPointerFromHandle(env, renderer);
    gint position_g = (gint)position;
    gtk_cell_layout_reorder(layout_g, renderer_g, position_g);
}

#ifdef __cplusplus
}
#endif
