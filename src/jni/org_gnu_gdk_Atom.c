/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gdk/gdk.h>
#include "gtk_java.h"

#include "org_gnu_gdk_Atom.h"
#ifdef __cplusplus
extern "C" 
{
#endif


    // NOTE: An GdkAtom is just a struct or pointer.  We don't need to
    // do any memory management on it since the struct does not
    // contain any data.  It is only used as an index into a string
    // table on the X server.
	

/*
 * Class:     org.gnu.gdk.Atom
 * Method:    gdk_atom_intern
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gdk_Atom_gdk_1atom_1intern 
	(JNIEnv *env, jclass cls, jstring atomName, jboolean onlyIfExists) 
{
    const gchar* atomName_g = (*env)->GetStringUTFChars(env, atomName, 0);
    gboolean onlyIfExists_g = (gboolean) onlyIfExists;
    // MEM_MGT_NOTE: ok since we do not managed Atom's memory.
    jobject result = getHandleFromPointer(env, gdk_atom_intern (atomName_g, onlyIfExists_g) );
    (*env)->ReleaseStringUTFChars(env, atomName, atomName_g);
    return result;
}

/*
 * Class:     org.gnu.gdk.Atom
 * Method:    gdk_atom_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gdk_Atom_gdk_1atom_1name 
	(JNIEnv *env, jclass cls, jobject atom) 
{
    GdkAtom atom_g = (GdkAtom)getPointerFromHandle(env, atom);
    const gchar *result_g = gdk_atom_name (atom_g);
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
