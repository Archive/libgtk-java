/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TextView.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_TextView
 * Method:    getHAdjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_getHAdjustment
  (JNIEnv *env, jclass cls, jobject textView)
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
	return getGObjectHandle(env, (GObject *) textView_g->hadjustment);
}

/*
 * Class:     org_gnu_gtk_TextView
 * Method:    getVAdjustment
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_getVAdjustment
  (JNIEnv *env, jclass cls, jobject textView)
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return getGObjectHandle(env, (GObject *) textView_g->vadjustment);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_text_view_get_type ();
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1new (JNIEnv *env, jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_text_view_new ());
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_new_with_buffer
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1new_1with_1buffer (JNIEnv *env, jclass cls, jobject buffer) 
{
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer *)getPointerFromHandle(env, buffer);
    return getGObjectHandle(env, (GObject *) gtk_text_view_new_with_buffer (buffer_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_buffer
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1buffer (JNIEnv *env, jclass cls, jobject textView, jobject buffer) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextBuffer *buffer_g = 
        (GtkTextBuffer *)getPointerFromHandle(env, buffer);
    gtk_text_view_set_buffer (textView_g, buffer_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_buffer
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1buffer (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return getGObjectHandle(env, (GObject *) gtk_text_view_get_buffer (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_scroll_to_iter
 * Signature: (IIDDDD)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1scroll_1to_1iter (JNIEnv *env, jclass cls, jobject textView, jobject iter, jdouble withinMargin, jboolean useAlign, jdouble xalign, jdouble yalign) 
{
    GtkTextView *textView_g = (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gdouble withinMargin_g = (gdouble) withinMargin;
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    return (jboolean) (gtk_text_view_scroll_to_iter (textView_g, iter_g, 
                                                     withinMargin_g, useAlign, 
                                                     xalign_g, yalign_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_scroll_to_mark
 * Signature: (IIDDDD)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1scroll_1to_1mark (JNIEnv *env, jclass cls, jobject textView, jobject mark, jdouble withinMargin, jboolean useAlign, jdouble xalign, jdouble yalign) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextMark *mark_g = (GtkTextMark *)getPointerFromHandle(env, mark);
    gdouble withinMargin_g = (gdouble) withinMargin;
    gdouble xalign_g = (gdouble) xalign;
    gdouble yalign_g = (gdouble) yalign;
    gtk_text_view_scroll_to_mark (textView_g, mark_g, 
                                  withinMargin_g, useAlign, xalign_g, 
                                  yalign_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_scroll_mark_onscreen
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1scroll_1mark_1onscreen (JNIEnv *env, jclass cls, jobject textView, jobject mark) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextMark *mark_g = (GtkTextMark *)getPointerFromHandle(env, mark);
    gtk_text_view_scroll_mark_onscreen (textView_g, mark_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_move_mark_onscreen
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1move_1mark_1onscreen (JNIEnv *env, jclass cls, jobject textView, jobject mark) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextMark *mark_g = (GtkTextMark *)getPointerFromHandle(env, mark);
    return (jboolean) (gtk_text_view_move_mark_onscreen (textView_g, mark_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_place_cursor_onscreen
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1place_1cursor_1onscreen (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jboolean) (gtk_text_view_place_cursor_onscreen (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_visible_rect
 * Signature: (II)V
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1visible_1rect
(JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g;
    GdkRectangle *visibleRect_g;
    textView_g = (GtkTextView *)getPointerFromHandle(env, textView);
    visibleRect_g = g_new(GdkRectangle, 1);
    gtk_text_view_get_visible_rect (textView_g, visibleRect_g);
    return getGBoxedHandle(env, visibleRect_g, GDK_TYPE_RECTANGLE, NULL,
    		(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_cursor_visible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1cursor_1visible (JNIEnv *env, jclass cls, jobject textView, jboolean setting) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gboolean setting_g = (gboolean) setting;
    gtk_text_view_set_cursor_visible (textView_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_cursor_visible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1cursor_1visible (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jboolean) (gtk_text_view_get_cursor_visible (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_iter_location
 * Signature: (III)V
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1iter_1location
(JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g;
    GtkTextIter *iter_g;
    GdkRectangle *location_g;
    textView_g = (GtkTextView *)getPointerFromHandle(env, textView);
    iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    location_g = g_new(GdkRectangle, 1);
    gtk_text_view_get_iter_location (textView_g, iter_g, location_g);
    return getGBoxedHandle(env, location_g, gdk_rectangle_get_type(), NULL, 
    		(GBoxedFreeFunc) g_free);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_iter_at_location
 * Signature: (IIII)V
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1iter_1at_1location
(JNIEnv *env, jclass cls, jobject textView, jint x, jint y) 
{
    GtkTextView *textView_g;
    GtkTextIter iter_g;
    gint32 y_g;
    gint32 x_g;
    textView_g = (GtkTextView *)getPointerFromHandle(env, textView);
    x_g = (gint32) x;
    y_g = (gint32) y;
    gtk_text_view_get_iter_at_location (textView_g, &iter_g, x_g, y_g);
    return getGBoxedHandle(env, &iter_g, gtk_text_iter_get_type(), 
    		(GBoxedCopyFunc) gtk_text_iter_copy,  (GBoxedFreeFunc) gtk_text_iter_free);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_line_yrange
 * Signature: (II[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1line_1yrange
(JNIEnv *env, jclass cls, jobject textView, jobject iter, jintArray y, jintArray height) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint *y_g = (gint *) (*env)->GetIntArrayElements (env, y, NULL);
    gint *height_g = (gint *) (*env)->GetIntArrayElements (env, height, NULL);
    gtk_text_view_get_line_yrange (textView_g, iter_g, y_g, height_g);
    (*env)->ReleaseIntArrayElements (env, y, (jint *) y_g, 0);
    (*env)->ReleaseIntArrayElements (env, height, (jint *) height_g, 0);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_line_at_y
 * Signature: (III[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1line_1at_1y (JNIEnv *env, jclass cls, jobject textView, jobject targetIter, jint y, jintArray lneTop) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *targetIter_g = 
        (GtkTextIter *)getPointerFromHandle(env, targetIter);
    gint32 y_g = (gint32) y;
    gint *lneTop_g = (gint *) (*env)->GetIntArrayElements (env, lneTop, NULL);
    gtk_text_view_get_line_at_y (textView_g, targetIter_g, y_g, lneTop_g);
    (*env)->ReleaseIntArrayElements (env, lneTop, (jint *) lneTop_g, 0);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_buffer_to_window_coords
 * Signature: (IIII[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1buffer_1to_1window_1coords (JNIEnv *env, jclass cls, jobject textView, jint win, jint bufferX, jint bufferY, jintArray windowX, jintArray windowY) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextWindowType win_g = (GtkTextWindowType) win;
    gint32 bufferX_g = (gint32) bufferX;
    gint32 bufferY_g = (gint32) bufferY;
    gint *windowX_g = 
        (gint *) (*env)->GetIntArrayElements (env, windowX, NULL);
    gint *windowY_g = 
        (gint *) (*env)->GetIntArrayElements (env, windowY, NULL);
    gtk_text_view_buffer_to_window_coords (textView_g, win_g, 
                                           bufferX_g, bufferY_g, 
                                           windowX_g, windowY_g);
    (*env)->ReleaseIntArrayElements (env, windowX, (jint *) windowX_g, 0);
    (*env)->ReleaseIntArrayElements (env, windowY, (jint *) windowY_g, 0);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_window_to_buffer_coords
 * Signature: (IIII[Lint ;[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1window_1to_1buffer_1coords (JNIEnv *env, jclass cls, jobject textView, jint win, jint windowX, jint windowY, jintArray bufferX, jintArray bufferY) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextWindowType win_g = (GtkTextWindowType) win;
    gint32 windowX_g = (gint32) windowX;
    gint32 windowY_g = (gint32) windowY;
    gint *bufferX_g = 
        (gint *) (*env)->GetIntArrayElements (env, bufferX, NULL);
    gint *bufferY_g = 
        (gint *) (*env)->GetIntArrayElements (env, bufferY, NULL);
    gtk_text_view_window_to_buffer_coords (textView_g, win_g, 
                                           windowX_g, windowY_g, 
                                           bufferX_g, bufferY_g);
    (*env)->ReleaseIntArrayElements (env, bufferX, (jint *) bufferX_g, 0);
    (*env)->ReleaseIntArrayElements (env, bufferY, (jint *) bufferY_g, 0);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_window
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1window (JNIEnv *env, jclass cls, jobject textView, jint win) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextWindowType win_g = (GtkTextWindowType) win;
    return getGObjectHandleAndRef(env, (GObject *) gtk_text_view_get_window (textView_g, win_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_window_type
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1window_1type
		(JNIEnv *env, jclass cls, jobject textView, jobject window) 
{
    GtkTextView *textView_g = 
    		(GtkTextView *) getPointerFromHandle(env, textView);
    GdkWindow *window_g = (GdkWindow *) getPointerFromHandle(env, window);
    return (jint) (gtk_text_view_get_window_type (textView_g, window_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_border_window_size
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1border_1window_1size (JNIEnv *env, jclass cls, jobject textView, jint type, jint size) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextWindowType type_g = (GtkTextWindowType) type;
    gint32 size_g = (gint32) size;
    gtk_text_view_set_border_window_size (textView_g, type_g, size_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_border_window_size
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1border_1window_1size (JNIEnv *env, jclass cls, jobject textView, jint type) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextWindowType type_g = (GtkTextWindowType) type;
    return (jint) (gtk_text_view_get_border_window_size (textView_g, type_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_forward_display_line
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1forward_1display_1line (JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_view_forward_display_line (textView_g, iter_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_backward_display_line
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1backward_1display_1line (JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_view_backward_display_line (textView_g, iter_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_forward_display_line_end
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1forward_1display_1line_1end (JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_view_forward_display_line_end (textView_g, 
                                                               iter_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_backward_display_line_start
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1backward_1display_1line_1start (JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_view_backward_display_line_start (textView_g, 
                                                                  iter_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_starts_display_line
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1starts_1display_1line (JNIEnv *env, jclass cls, jobject textView, jobject iter) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    return (jboolean) (gtk_text_view_starts_display_line (textView_g, iter_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_move_visually
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1move_1visually (JNIEnv *env, jclass cls, jobject textView, jobject iter, jint count) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkTextIter *iter_g = (GtkTextIter *)getPointerFromHandle(env, iter);
    gint32 count_g = (gint32) count;
    return (jboolean) (gtk_text_view_move_visually (textView_g, iter_g, 
                                                    count_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_add_child_at_anchor
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1add_1child_1at_1anchor (JNIEnv *env, jclass cls, jobject textView, jobject child, jobject anchor) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    GtkTextChildAnchor *anchor_g = 
        (GtkTextChildAnchor *)getPointerFromHandle(env, anchor);
    gtk_text_view_add_child_at_anchor (textView_g, child_g, anchor_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_add_child_in_window
 * Signature: (IIIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1add_1child_1in_1window (JNIEnv *env, jclass cls, jobject textView, jobject child, jint whichWindow, jint xpos, jint ypos) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkWidget *child_g = (GtkWidget *)child;
    GtkTextWindowType whichWindow_g = (GtkTextWindowType) whichWindow;
    gint32 xpos_g = (gint32) xpos;
    gint32 ypos_g = (gint32) ypos;
    gtk_text_view_add_child_in_window (textView_g, child_g, whichWindow_g, xpos_g, ypos_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_move_child
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1move_1child (JNIEnv *env, jclass cls, jobject textView, jobject child, jint xpos, jint ypos) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkWidget *child_g = (GtkWidget *)getPointerFromHandle(env, child);
    gint32 xpos_g = (gint32) xpos;
    gint32 ypos_g = (gint32) ypos;
    gtk_text_view_move_child (textView_g, child_g, xpos_g, ypos_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_wrap_mode
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1wrap_1mode (JNIEnv *env, jclass cls, jobject textView, jint wrapMode) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkWrapMode wrapMode_g = (GtkWrapMode) wrapMode;
    gtk_text_view_set_wrap_mode (textView_g, wrapMode_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_wrap_mode
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1wrap_1mode (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_wrap_mode (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_editable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1editable (JNIEnv *env, jclass cls, jobject textView, jboolean setting) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gboolean setting_g = (gboolean) setting;
    gtk_text_view_set_editable (textView_g, setting_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_editable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1editable (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jboolean) (gtk_text_view_get_editable (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_pixels_above_lines
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1pixels_1above_1lines (JNIEnv *env, jclass cls, jobject textView, jint pixelsAboveLines) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 pixelsAboveLines_g = (gint32) pixelsAboveLines;
    gtk_text_view_set_pixels_above_lines (textView_g, pixelsAboveLines_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_pixels_above_lines
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1pixels_1above_1lines (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_pixels_above_lines (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_pixels_below_lines
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1pixels_1below_1lines (JNIEnv *env, jclass cls, jobject textView, jint pixelsBelowLines) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 pixelsBelowLines_g = (gint32) pixelsBelowLines;
    gtk_text_view_set_pixels_below_lines (textView_g, pixelsBelowLines_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_pixels_below_lines
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1pixels_1below_1lines (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_pixels_below_lines (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_pixels_inside_wrap
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1pixels_1inside_1wrap (JNIEnv *env, jclass cls, jobject textView, jint pixelsInsideWrap) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 pixelsInsideWrap_g = (gint32) pixelsInsideWrap;
    gtk_text_view_set_pixels_inside_wrap (textView_g, pixelsInsideWrap_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_pixels_inside_wrap
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1pixels_1inside_1wrap (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_pixels_inside_wrap (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_justification
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1justification (JNIEnv *env, jclass cls, jobject textView, jint justification) 
{
    GtkTextView *textview_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    GtkJustification justification_g = (GtkJustification) justification;
    gtk_text_view_set_justification (textview_g, justification_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_justification
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1justification (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_justification (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_left_margin
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1left_1margin (JNIEnv *env, jclass cls, jobject textView, jint leftMargin) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 leftMargin_g = (gint32) leftMargin;
    gtk_text_view_set_left_margin (textView_g, leftMargin_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_left_margin
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1left_1margin (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_left_margin (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_right_margin
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1right_1margin (JNIEnv *env, jclass cls, jobject textView, jint rightMargin) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 rightMargin_g = (gint32) rightMargin;
    gtk_text_view_set_right_margin (textView_g, rightMargin_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_right_margin
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1right_1margin (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_right_margin (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_indent
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1indent (JNIEnv *env, jclass cls, jobject textView, jint indent) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gint32 indent_g = (gint32) indent;
    gtk_text_view_set_indent (textView_g, indent_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_indent
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1indent (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jint) (gtk_text_view_get_indent (textView_g));
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_set_tabs
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1tabs (JNIEnv *env, jclass cls, jobject textView, jobject tabs) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    PangoTabArray *tabs_g = (PangoTabArray *)getPointerFromHandle(env, tabs);
    gtk_text_view_set_tabs (textView_g, tabs_g);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_tabs
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1tabs
		(JNIEnv *env, jclass cls, jobject textView) 
{
	PangoTabArray *tab_array;
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    tab_array = gtk_text_view_get_tabs (textView_g);
    return getGBoxedHandle(env, tab_array, PANGO_TYPE_TAB_ARRAY, NULL, 
    		(GBoxedFreeFunc) pango_tab_array_free);
}

/*
 * Class:     org.gnu.gtk.TextView
 * Method:    gtk_text_view_get_default_attributes
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1default_1attributes
		(JNIEnv *env, jclass cls, jobject textView) 
{
	GtkTextAttributes *attributes;
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    attributes = gtk_text_view_get_default_attributes (textView_g);
    return getGBoxedHandle(env, attributes,
    		GTK_TYPE_TEXT_ATTRIBUTES, NULL, (GBoxedFreeFunc)
    		gtk_text_attributes_unref);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1accepts_1tab (JNIEnv *env, jclass cls, jobject textView, jboolean setting) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gboolean setting_g = (gboolean)setting;
    gtk_text_view_set_accepts_tab( textView_g, setting_g );
}
	
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1accepts_1tab (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jboolean) gtk_text_view_get_accepts_tab( textView_g );
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1get_1overwrite (JNIEnv *env, jclass cls, jobject textView) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    return (jboolean) gtk_text_view_get_overwrite( textView_g );
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TextView_gtk_1text_1view_1set_1overwrite (JNIEnv *env, jclass cls, jobject textView, jboolean setting) 
{
    GtkTextView *textView_g = 
        (GtkTextView *)getPointerFromHandle(env, textView);
    gboolean setting_g = (gboolean)setting;
    gtk_text_view_set_overwrite( textView_g, setting_g );
}



#ifdef __cplusplus
}

#endif
