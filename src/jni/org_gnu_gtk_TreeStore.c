/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"
#include "gtk/gtktreemodel.h"
          
#include "org_gnu_gtk_TreeStore.h"
#ifdef __cplusplus
extern "C" 
{
#endif
  
static void free_if_java_object(JNIEnv *env, GtkTreeStore* treeStore, GtkTreeIter* iter, gint column);
static gboolean foreach_function_free_if_java_object(GtkTreeModel *model,
						     GtkTreePath *path,
						     GtkTreeIter *iter,
						     gpointer data);
static void free_if_java_object_all_cols(JNIEnv *env, 
					 GtkTreeStore* treeStore,
					 GtkTreeIter* iter);

static jobject getTreeIter(JNIEnv* env, GtkTreeIter* iter)
{
	return getGBoxedHandle(env, iter, GTK_TYPE_TREE_ITER, (GBoxedCopyFunc)
			gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free);	
} 

//GtkTreePath * GtkTreeStore_get_root (GtkTreeStore * cptr) 
//{
//    return cptr->root;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    getRoot
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_getRoot (JNIEnv *env, jclass cls, jint cptr) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    {
//        return (jint)GtkTreeStore_get_root (cptr_g);
//    }
//}
//
//void GtkTreeStore_set_root (GtkTreeStore * cptr, GtkTreePath * root) 
//{
//    cptr->root = root;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    setRoot
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_setRoot (JNIEnv *env, jint cptr, jint root) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    GtkTreePath *root_g = (GtkTreePath *)root;
//    {
//        GtkTreeStore_set_root (cptr_g, root_g);
//    }
//}
//
//GtkTreePath * GtkTreeStore_get_last (GtkTreeStore * cptr) 
//{
//    return cptr->last;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    getLast
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_getLast (JNIEnv *env, jclass cls, jint cptr) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    {
//        return (jint)GtkTreeStore_get_last (cptr_g);
//    }
//}
//
//void GtkTreeStore_set_last (GtkTreeStore * cptr, GtkTreePath * last) 
//{
//    cptr->last = last;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    setLast
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_setLast (JNIEnv *env, jint cptr, jint last) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    GtkTreePath *last_g = (GtkTreePath *)last;
//    {
//        GtkTreeStore_set_last (cptr_g, last_g);
//    }
//}
//
//gint32 GtkTreeStore_get_n_columns (GtkTreeStore * cptr) 
//{
//    return cptr->n_columns;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    getNColumns
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_getNColumns (JNIEnv *env, jclass cls, jint 
//    cptr) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    {
//        jint result_j = (jint) (GtkTreeStore_get_n_columns (cptr_g));
//        return result_j;
//    }
//}
//
//void GtkTreeStore_set_n_columns (GtkTreeStore * cptr, gint32 n_columns) 
//{
//    cptr->n_columns = n_columns;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    setNColumns
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_setNColumns (JNIEnv *env, jint cptr, jint 
//    n_columns) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    gint32 n_columns_g = (gint32) n_columns;
//    {
//        GtkTreeStore_set_n_columns (cptr_g, n_columns_g);
//    }
//}
//GType * GtkTreeStore_get_column_headers (GtkTreeStore * cptr) 
//{
//    return cptr->column_headers;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    getColumnHeaders
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_getColumnHeaders (JNIEnv *env, jclass cls, 
//    jint cptr) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    {
//        return (jint)GtkTreeStore_get_column_headers (cptr_g);
//    }
//}
//
//void GtkTreeStore_set_column_headers (GtkTreeStore * cptr, GType * column_headers) 
//{
//    cptr->column_headers = column_headers;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    setColumnHeaders
// * Signature: (II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_setColumnHeaders (JNIEnv *env, jint cptr, 
//    jint column_headers) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    GType *column_headers_g = (GType *)column_headers;
//    {
//        GtkTreeStore_set_column_headers (cptr_g, column_headers_g);
//    }
//}
//
//gboolean GtkTreeStore_get_columns_dirty (GtkTreeStore * cptr) 
//{
//    return cptr->columns_dirty;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    getColumnsDirty
// * Signature: (I)Z
// */
//JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeStore_getColumnsDirty (JNIEnv *env, jclass cls, 
//    jint cptr) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    {
//        jboolean result_j = (jboolean) (GtkTreeStore_get_columns_dirty (cptr_g));
//        return result_j;
//    }
//}
//
//void GtkTreeStore_set_columns_dirty (GtkTreeStore * cptr, gboolean columns_dirty) 
//{
//    cptr->columns_dirty = columns_dirty;
//}
//
///*
// * Class:     org.gnu.gtk.TreeStore
// * Method:    setColumnsDirty
// * Signature: (IZ)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_setColumnsDirty (JNIEnv *env, jint cptr, 
//    jboolean columns_dirty) 
//{
//    GtkTreeStore *cptr_g = (GtkTreeStore *)cptr;
//    gboolean columns_dirty_g = (gboolean) columns_dirty;
//    {
//        GtkTreeStore_set_columns_dirty (cptr_g, columns_dirty_g);
//    }
//}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_tree_store_get_type ();
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_newv
 * Signature: (I[Lint ;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1newv
		(JNIEnv *env, jclass cls, jint numColumns, jintArray types) 
{
    GType *types_g;
    jint *returnPointerArray;
    gint32 numColumns_g = (gint32) numColumns;
    types_g = getGTypesFromJArray(env, numColumns, types, &returnPointerArray);
    	
    // Don't release the types array since GTK still needs it.
    return getGObjectHandle(env, (GObject *) gtk_tree_store_newv (numColumns_g, types_g));
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_set_column_types
 * Signature: (II[Lint ;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1set_1column_1types
		(JNIEnv *env, jclass cls, jobject treeStore, jint numColumns, jintArray types) 
{
	GType *types_g;
	gint32 numColumns_g;
	jint *returnPointerArray;
    GtkTreeStore *treeStore_g = (GtkTreeStore *)treeStore;
    numColumns_g = (gint32) numColumns;
    types_g = getGTypesFromJArray(env, numColumns, types, &returnPointerArray);
    gtk_tree_store_set_column_types (treeStore_g, numColumns_g, types_g);
    // Don't release the types array since GTK still needs it.
    //    (*env)->ReleaseIntArrayElements (env, types, types_g_g, 0);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_set_value
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1set_1value (JNIEnv *env, jclass cls, jobject treeStore, jobject iter, jint columnt, jobject value) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    gint32 columnt_g = (gint32) columnt;
    GValue *value_g = (GValue *)getPointerFromHandle(env, value);
    
    // If we are about to override a java object free it first
    free_if_java_object(env, treeStore_g, iter_g, columnt_g);
    
    if ( G_VALUE_HOLDS_POINTER(value_g) ){
      gpointer java_pointer = g_value_get_pointer (value_g);
      if(java_pointer != NULL){
	g_value_set_pointer (value_g, (*env)->NewGlobalRef(env, (jobject)java_pointer));
      }
    }

    gtk_tree_store_set_value (treeStore_g, iter_g, columnt_g, value_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_remove
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1remove (JNIEnv *env, jclass cls, jobject treeStore, jobject iter) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);

    free_if_java_object_all_cols(env, treeStore_g, iter_g);

    gtk_tree_store_remove (treeStore_g, iter_g);
}


/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_insert
 * Signature: (III)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1insert
		(JNIEnv *env, jclass cls, jobject treeStore, jobject parent, jint position) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    gint32 position_g = (gint32) position;

    gtk_tree_store_insert (treeStore_g, iter_g, parent_g, position_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_insert_before
 * Signature: (III)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1insert_1before
		(JNIEnv *env, jclass cls, jobject treeStore, jobject parent, jobject sibling) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));;
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    GtkTreeIter *sibling_g = (GtkTreeIter *)getPointerFromHandle(env, sibling);
    gtk_tree_store_insert_before (treeStore_g, iter_g, parent_g, sibling_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_insert_after
 * Signature: (III)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1insert_1after (JNIEnv *env, jclass cls, jobject treeStore, jobject parent, jobject sibling) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));;
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    GtkTreeIter *sibling_g = (GtkTreeIter *)getPointerFromHandle(env, sibling);
    gtk_tree_store_insert_after (treeStore_g, iter_g, parent_g, sibling_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_prepend
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1prepend
		(JNIEnv *env, jclass cls, jobject treeStore, jobject parent) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));;
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    gtk_tree_store_prepend (treeStore_g, iter_g, parent_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_append
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1append 
(JNIEnv *env, jclass cls, jobject treeStore, jobject parent) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)g_malloc(sizeof(GtkTreeIter));
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    gtk_tree_store_append (treeStore_g, iter_g, parent_g);
    return getTreeIter(env, iter_g);
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_is_ancestor
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1is_1ancestor (JNIEnv *env, jclass cls, jobject treeStore, jobject iter, jobject descendant) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    GtkTreeIter *descendant_g = 
        (GtkTreeIter *)getPointerFromHandle(env, descendant);
    return (jboolean) (gtk_tree_store_is_ancestor (treeStore_g, iter_g, 
                                                   descendant_g));
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_iter_depth
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1iter_1depth (JNIEnv *env, jclass cls, jobject treeStore, jobject iter) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    return (jint) (gtk_tree_store_iter_depth (treeStore_g, iter_g));
}

/*
 * Class:     org.gnu.gtk.TreeStore
 * Method:    gtk_tree_store_clear
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1clear (JNIEnv *env, jclass cls, jobject treeStore) 
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    
    
    gtk_tree_model_foreach ((GtkTreeModel*) treeStore_g,
			    (GtkTreeModelForeachFunc)foreach_function_free_if_java_object ,
			    (gpointer)env);

    gtk_tree_store_clear (treeStore_g);
}

/*
 * Class:     org_gnu_gtk_TreeStore
 * Method:    gtk_tree_store_iter_is_valid
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1iter_1is_1valid(JNIEnv *env, jclass cls, jobject treeStore, jobject iter)
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    return (jboolean)gtk_tree_store_iter_is_valid(treeStore_g, iter_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeStore
 * Method:    gtk_tree_store_reorder
 * Signature: (II[I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1reorder(JNIEnv *env, jclass cls, jobject treeStore, jobject parent, jintArray order)
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *parent_g = (GtkTreeIter *)getPointerFromHandle(env, parent);
    gint* order_g = (gint*)(*env)->GetIntArrayElements(env, order, NULL);
    gtk_tree_store_reorder(treeStore_g, parent_g, order_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeStore
 * Method:    gtk_tree_store_swap
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1swap(JNIEnv *env, jclass cls, jobject treeStore, jobject a, jobject b)
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *a_g = (GtkTreeIter *)getPointerFromHandle(env, a);
    GtkTreeIter *b_g = (GtkTreeIter *)getPointerFromHandle(env, b);
    gtk_tree_store_swap(treeStore_g, a_g, b_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeStore
 * Method:    gtk_tree_store_move_before
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1move_1before(JNIEnv *env, jclass cls, jobject treeStore, jobject iter, jobject pos)
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    GtkTreeIter *pos_g = (GtkTreeIter *)getPointerFromHandle(env, pos);
    gtk_tree_store_move_before(treeStore_g, iter_g, pos_g);
}
                                                                                         
/*
 * Class:     org_gnu_gtk_TreeStore
 * Method:    gtk_tree_store_move_after
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeStore_gtk_1tree_1store_1move_1after(JNIEnv *env, jclass cls, jobject treeStore, jobject iter, jobject pos)
{
    GtkTreeStore *treeStore_g = 
        (GtkTreeStore *)getPointerFromHandle(env, treeStore);
    GtkTreeIter *iter_g = (GtkTreeIter *)getPointerFromHandle(env, iter);
    GtkTreeIter *pos_g = (GtkTreeIter *)getPointerFromHandle(env, pos);
    gtk_tree_store_move_after(treeStore_g, iter_g, pos_g);
}

/*
 * A standard GtkTreeModel foreach function. This function calls
 * free_if_java_object_all_cols
 */
static gboolean foreach_function_free_if_java_object(GtkTreeModel *model,
					      GtkTreePath *path,
					      GtkTreeIter *iter,
					      gpointer data){
  
  free_if_java_object_all_cols((JNIEnv*)data, (GtkTreeStore*)model, iter);
}


/*
 * Calls free_if_java_object for each column in the row pointed to
 * by iter
 */
static void free_if_java_object_all_cols(JNIEnv *env, GtkTreeStore* treeStore, GtkTreeIter* iter){
  gint n = gtk_tree_model_get_n_columns((GtkTreeModel*)treeStore);
  
  int i;
  for(i=0;i<n;i++){
    free_if_java_object(env, treeStore, iter, i);
  }
}

/*
 * Check if the given tree iter points to a java object
 * if so decrement the refrences to it.
 */
static void free_if_java_object(JNIEnv *env, GtkTreeStore* treeStore, GtkTreeIter* iter, gint column)
{
  //Check of the old value holds a java object which needs to be freed
  GValue* old_value = (GValue*)g_malloc(sizeof(GValue));
  old_value->g_type = 0;
  gtk_tree_model_get_value((GtkTreeModel*)treeStore, iter, column, old_value);
  if ( G_VALUE_HOLDS_POINTER(old_value) ){
    gpointer java_pointer = g_value_get_pointer (old_value);
    if(java_pointer != NULL){
      (*env)->DeleteGlobalRef(env, (jobject)java_pointer);
    }
  }
  g_free(old_value);
}

#ifdef __cplusplus
}

#endif
