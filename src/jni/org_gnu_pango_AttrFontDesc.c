/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_pango_AttrFontDesc.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.AttrFontDesc
 * Method:    getDesc
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_pango_AttrFontDesc_getDesc 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    PangoAttrFontDesc *obj_g = 
        (PangoAttrFontDesc *)getPointerFromHandle(env, obj);
    return getGBoxedHandle(env, obj_g->desc, 
                           PANGO_TYPE_FONT_DESCRIPTION,
                           (GBoxedCopyFunc)pango_font_description_copy, 
                           (JGFreeFunc)pango_font_description_free);
}


#ifdef __cplusplus
}

#endif
