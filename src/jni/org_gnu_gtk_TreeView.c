/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeView.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_tree_view_get_type();
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_new
 * Signature: ()I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1new(JNIEnv *env, jclass cls)
{
    return getGObjectHandle(env, (GObject *) gtk_tree_view_new ());
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_new_with_model
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1new_1with_1model(JNIEnv *env, jclass cls, jobject model)
{
    GtkTreeModel *model_g = (GtkTreeModel *)getPointerFromHandle(env, model);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_new_with_model(model_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_model
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1model(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_get_model(view_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_model
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1model(JNIEnv *env, jclass cls, jobject view, jobject model)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeModel *model_g = (GtkTreeModel *)getPointerFromHandle(env, model);
    gtk_tree_view_set_model(view_g, model_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_selection
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1selection
		(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GObject *object = (GObject *) gtk_tree_view_get_selection(view_g);
    return getGObjectHandleAndRef(env, object);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_hadjustment
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1hadjustment(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_get_hadjustment(view_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_hadjustment
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1hadjustment(JNIEnv *env, jclass cls, jobject view, jobject adj)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkAdjustment *adj_g = (GtkAdjustment *)getPointerFromHandle(env, adj);
    gtk_tree_view_set_hadjustment(view_g, adj_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_vadjustment
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1vadjustment(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_get_vadjustment(view_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_vadjustment
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1vadjustment(JNIEnv *env, jclass cls, jobject view, jobject adj)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkAdjustment *adj_g = (GtkAdjustment *)getPointerFromHandle(env, adj);
    gtk_tree_view_set_vadjustment(view_g, adj_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_headers_visible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1headers_1visible(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return (jboolean)gtk_tree_view_get_headers_visible(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_headers_visible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1headers_1visible(JNIEnv *env, jclass cls, jobject view, jboolean vis)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gboolean vis_g = (gboolean)vis;
    gtk_tree_view_set_headers_visible(view_g, vis_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_columns_autosize
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1columns_1autosize(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gtk_tree_view_columns_autosize(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_headers_clickable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1headers_1clickable(JNIEnv *env, jclass cls, jobject view, jboolean value)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gboolean value_g = (gboolean)value;
    gtk_tree_view_set_headers_clickable(view_g, value_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_rules_hint
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1rules_1hint(JNIEnv *env, jclass cls, jobject view, jboolean value)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gboolean value_g = (gboolean)value;
    gtk_tree_view_set_rules_hint(view_g, value_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_rules_hint
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1rules_1hint(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return (jboolean)gtk_tree_view_get_rules_hint(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_append_column
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1append_1column(JNIEnv *env, jclass cls, jobject view, jobject column)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    return (jint)gtk_tree_view_append_column(view_g, column_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_remove_column
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1remove_1column(JNIEnv *env, jclass cls, jobject view, jobject column)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    return (jint)gtk_tree_view_remove_column(view_g, column_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_insert_column
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1insert_1column(JNIEnv *env, jclass cls, jobject view, jobject column, jint pos)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gint pos_g = (gint)pos;
    return (jint)gtk_tree_view_insert_column(view_g, column_g, pos_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_column
 * Signature: (II)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1column(JNIEnv *env, jclass cls, jobject view, jint pos)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gint pos_g = (gint)pos;
    return getGObjectHandle(env, (GObject *)gtk_tree_view_get_column(view_g, pos_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_columns
 * Signature: (I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1columns(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandlesFromGListAndRef(env, gtk_tree_view_get_columns(view_g));
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_move_column_after
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1move_1column_1after(JNIEnv *env, jclass cls, jobject view, jobject column, jobject baseColumn)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    GtkTreeViewColumn *baseColumn_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, baseColumn);
    gtk_tree_view_move_column_after(view_g, column_g, baseColumn_g);
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_expander_column
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1expander_1column(JNIEnv *env, jclass cls, jobject view, jobject column)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gtk_tree_view_set_expander_column(view_g, column_g);
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_expander_column
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1expander_1column(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_get_expander_column(view_g));
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_scroll_to_point
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1scroll_1to_1point(JNIEnv *env, jclass cls, jobject view, jint x, jint y)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gint x_g = (gint)x;
    gint y_g = (gint)y;
    gtk_tree_view_scroll_to_point(view_g, x_g, y_g);
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_scroll_to_cell
 * Signature: (IIIZDD)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1scroll_1to_1cell(JNIEnv *env, jclass cls, jobject view, jobject path, jobject column, jboolean align, jdouble row, jdouble col)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gboolean align_g = (gboolean)align;
    gfloat row_g = (gfloat)row;
    gfloat col_g = (gfloat)col;
    gtk_tree_view_scroll_to_cell(view_g, path_g, column_g,
                                 align_g, row_g, col_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_row_activated
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1row_1activated(JNIEnv *env, jclass cls, jobject view, jobject path, jobject column)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gtk_tree_view_row_activated(view_g, path_g, column_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_expand_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1expand_1all(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gtk_tree_view_expand_all(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_collapse_all
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1collapse_1all(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gtk_tree_view_collapse_all(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_expand_row
 * Signature: (IIZ)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1expand_1row(JNIEnv *env, jclass cls, jobject view, jobject path, jboolean all)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    gboolean all_g = (gboolean)all;
    return (jboolean)gtk_tree_view_expand_row(view_g, path_g, all_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_collapse_row
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1collapse_1row(JNIEnv *env, jclass cls, jobject view, jobject path)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jboolean)gtk_tree_view_collapse_row(view_g, path_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_row_expanded
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1row_1expanded(JNIEnv *env, jclass cls, jobject view, jobject path)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    return (jboolean)gtk_tree_view_row_expanded(view_g, path_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_reorderable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1reorderable(JNIEnv *env, jclass cls, jobject view, jboolean value)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gtk_tree_view_set_reorderable(view_g, (gboolean)value);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_reorderable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1reorderable(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return (jboolean)gtk_tree_view_get_reorderable(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_cursor
 * Signature: (IIIZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1cursor(JNIEnv *env, jclass cls, jobject view, jobject path, jobject column, jboolean startEdit)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gboolean startEdit_g = (gboolean)startEdit;
    gtk_tree_view_set_cursor(view_g, path_g, column_g, startEdit_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_cursor_on_cell
 * Signature: (IIIIZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1cursor_1on_1cell(JNIEnv *env, jclass cls, jobject view, jobject path, jobject column, jobject cell, jboolean startEdit)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    GtkCellRenderer *cell_g = 
        (GtkCellRenderer *)getPointerFromHandle(env, cell);
    gboolean startEdit_g = (gboolean)startEdit;
    gtk_tree_view_set_cursor_on_cell(view_g, path_g, column_g, cell_g, startEdit_g);
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_cursor_path
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1cursor_1path
	(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g;
    GtkTreePath *path;
    view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    path = NULL;
    gtk_tree_view_get_cursor(view_g, &path, NULL);
    return getStructHandle(env, path, NULL, (JGFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_cursor_column
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1cursor_1column
	(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g;
    GtkTreeViewColumn *column;
    view_g = (GtkTreeView *) getPointerFromHandle(env, view);
    column = NULL; 
    gtk_tree_view_get_cursor(view_g, NULL, &column);
    return getGObjectHandle(env, (GObject *) column);
}
                                                                          

/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_bin_window
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1bin_1window(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return getGObjectHandle(env, (GObject *) gtk_tree_view_get_bin_window(view_g));
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_path_at_pos
 * Signature: (III[I[I[I[I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1path_1at_1pos(JNIEnv *env, jclass cls, jobject view, jint x, jint y, jobject path, jobject column, jint cellX, jint cellY)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gint x_g = (gint)x;
    gint y_g = (gint)y;
    GtkTreePath *path_g = (GtkTreePath *)getPointerFromHandle(env, path);
    GtkTreeViewColumn *column_g = 
        (GtkTreeViewColumn *)getPointerFromHandle(env, column);
    gint cellX_g = (gint)cellX;
    gint cellY_g = (gint)cellY;
    return gtk_tree_view_get_path_at_pos(view_g, x_g, y_g, &path_g, &column_g, 
                                         &cellX_g, &cellY_g);
}
                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_get_cell_area
// * Signature: (III)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1cell_1area
//  (JNIEnv *env, jclass cls, jobject view, jint path, jint column)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	GdkRectangle* rect = (GdkRectangle*)g_malloc(sizeof(GdkRectangle));
//	gtk_tree_view_get_cell_area(view_g, (GtkTreePath*)path, (GtkTreeViewColumn*)column, rect);
//	return (jint)rect;
//}
//                                                                             
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_get_background_area
// * Signature: (III)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1background_1area
//  (JNIEnv *env, jclass cls, jobject view, jint path, jint column)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	GdkRectangle* rect = (GdkRectangle*)g_malloc(sizeof(GdkRectangle));
//	gtk_tree_view_get_background_area(view_g, (GtkTreePath*)path, (GtkTreeViewColumn*)column, rect);
//	return (jint)rect;
//}
//                                                                             
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_get_visible_rect
// * Signature: (I)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1visible_1rect
//  (JNIEnv *env, jclass cls, jobject view)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	GdkRectangle* rect = (GdkRectangle*)g_malloc(sizeof(GdkRectangle));
//	gtk_tree_view_get_visible_rect(view_g, rect);
//	return (jint)rect;
//}
//                                                                             
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_widget_to_tree_coords
// * Signature: (III[I[I)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1widget_1to_1tree_1coords
//  (JNIEnv *env, jclass cls, jobject view, jint wx, jint wy, jintArray treeX, jintArray treeY)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	gint* tx = (gint*)(*env)->GetIntArrayElements(env, treeX, NULL);
//	gint* ty = (gint*)(*env)->GetIntArrayElements(env, treeY, NULL);
//	gtk_tree_view_widget_to_tree_coords(view_g, (gint)wx, (gint)wy, tx, ty);
//	(*env)->ReleaseIntArrayElements(env, treeX, (jint*)tx, 0);
//	(*env)->ReleaseIntArrayElements(env, treeY, (jint*)ty, 0);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_tree_to_widget_coords
// * Signature: (III[I[I)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1tree_1to_1widget_1coords
//  (JNIEnv *env, jclass cls, jobject view, jint tx, jint ty, jintArray winX, jintArray winY)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	gint* wx = (gint*)(*env)->GetIntArrayElements(env, winX, NULL);
//	gint* wy = (gint*)(*env)->GetIntArrayElements(env, winY, NULL);
//	gtk_tree_view_tree_to_widget_coords(view_g, (gint)tx, (gint)ty, wx, wy);
//	(*env)->ReleaseIntArrayElements(env, winX, (jint*)wx, 0);
//	(*env)->ReleaseIntArrayElements(env, winY, (jint*)wy, 0);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_enable_model_drag_source
// * Signature: (II[II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1enable_1model_1drag_1source
//  (JNIEnv *env, jclass cls, jobject view, jint mask, jintArray targets, jint action)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	GtkTargetEntry** entries;
//	jint* targs;
//	jsize len;
//	int index;
//
//	len = (*env)->GetArrayLength(env, targets);
//	entries = g_malloc(sizeof(GtkTargetEntry*)*len);
//	targs = (*env)->GetIntArrayElements(env, targets, NULL);
//	for (index = 0; index < len; index++)
//		entries[index] = (GtkTargetEntry*)targs[index];
//	gtk_tree_view_enable_model_drag_source(view_g, (GdkModifierType)mask, entries[0], len, (GdkDragAction)action);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_enable_model_drag_dest
// * Signature: (I[II)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1enable_1model_1drag_1dest
//  (JNIEnv *env, jclass cls, jobject view, jintArray targets, jint action)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	jsize len = (*env)->GetArrayLength(env, targets);
//	jint* targs;
//	int index;
//	GtkTargetEntry** entries = g_malloc(sizeof(GtkTargetEntry*)*len);
//		
//	targs = (*env)->GetIntArrayElements(env, targets, NULL);
//	for (index = 0; index < len; index++)
//		entries[index] = (GtkTargetEntry*)targs[index];
//	gtk_tree_view_enable_model_drag_dest(view_g, entries[0], len, (GdkDragAction)action);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_unset_rows_drag_source
// * Signature: (I)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1unset_1rows_1drag_1source
//  (JNIEnv *env, jclass cls, jobject view)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	gtk_tree_view_unset_rows_drag_source(view_g);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_unset_rows_drag_dest
// * Signature: (I)V
// */
//JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1unset_1rows_1drag_1dest
//  (JNIEnv *env, jclass cls, jobject view)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	gtk_tree_view_unset_rows_drag_dest(view_g);
//}
//                                                                          
///*
// * Class:     org_gnu_gtk_TreeView
// * Method:    gtk_tree_view_create_row_drag_icon
// * Signature: (II)I
// */
//JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1create_1row_1drag_1icon
//  (JNIEnv *env, jclass cls, jobject view, jint path)
//{
//    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
//	return (jint)gtk_tree_view_create_row_drag_icon(view_g, (GtkTreePath*)path);
//}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_enable_search
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1enable_1search(JNIEnv *env, jclass cls, jobject view, jboolean enable)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gboolean enable_g = (gboolean)enable;
    gtk_tree_view_set_enable_search(view_g, enable_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_enable_search
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1enable_1search(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return (jboolean)gtk_tree_view_get_enable_search(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_get_search_column
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1search_1column(JNIEnv *env, jclass cls, jobject view)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    return (jint)gtk_tree_view_get_search_column(view_g);
}
                                                                          
/*
 * Class:     org_gnu_gtk_TreeView
 * Method:    gtk_tree_view_set_search_column
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1search_1column(JNIEnv *env, jclass cls, jobject view, jint dataColumn)
{
    GtkTreeView *view_g = (GtkTreeView *)getPointerFromHandle(env, view);
    gint dataColumn_g = (gint)dataColumn;
    gtk_tree_view_set_search_column(view_g, dataColumn_g);
}
                                                                             
/* GTK 2.6 additions. */

JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1fixed_1height_1mode
(JNIEnv *env, jclass cls, jobject tree_view, jboolean enable)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    gboolean enable_g = (gboolean)enable;
    gtk_tree_view_set_fixed_height_mode(tree_view_g, enable_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1fixed_1height_1mode
(JNIEnv *env, jclass cls, jobject tree_view)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    return (jboolean)gtk_tree_view_get_fixed_height_mode(tree_view_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1hover_1selection
(JNIEnv *env, jclass cls, jobject tree_view, jboolean hover)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    gboolean hover_g = (gboolean)hover;
    gtk_tree_view_set_hover_selection(tree_view_g, hover_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1hover_1selection
(JNIEnv *env, jclass cls, jobject tree_view)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    return (jboolean)gtk_tree_view_get_hover_selection(tree_view_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1hover_1expand
(JNIEnv *env, jclass cls, jobject tree_view, jboolean expand)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    gboolean expand_g = (gboolean)expand;
    gtk_tree_view_set_hover_expand(tree_view_g, expand_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1hover_1expand
(JNIEnv *env, jclass cls, jobject tree_view)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    return (jboolean)gtk_tree_view_get_hover_expand(tree_view_g);
}

static gboolean treeViewRowSeparatorFunc( GtkTreeModel *model, 
                                          GtkTreeIter *iter, 
                                          gpointer data ) {
    jobject model_handle;
    jobject iter_handle;
	JGFuncCallbackRef *ref = (JGFuncCallbackRef*) data;
	model_handle = getGObjectHandleAndRef(ref->env, (GObject *) model);
	iter_handle = getGBoxedHandle(ref->env, iter, GTK_TYPE_TREE_ITER, (GBoxedCopyFunc)
			gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free);
	return (* ref->env)->CallBooleanMethod(ref->env, 
                                               ref->obj, 
                                               ref->methodID,
                                               model_handle,
                                               iter_handle);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1set_1row_1separator_1func
(JNIEnv *env, jclass cls, jobject tree_view, jobject tview, jstring callback)
{
    GtkTreeView * tree_view_g = 
        (GtkTreeView *)getPointerFromHandle(env, tree_view);

    if ( !tview ) {
        gtk_tree_view_set_row_separator_func(tree_view_g, NULL, NULL, NULL);
        return;
    }

    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->obj = (* env)->NewGlobalRef(env, tview);

    const char *funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetMethodID(env, 
                            (*env)->GetObjectClass(env, ref->obj), 
                            funcname, "(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Z" );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, callback, funcname);

    gtk_tree_view_set_row_separator_func(tree_view_g, treeViewRowSeparatorFunc, ref, NULL);
}

/*
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeView_gtk_1tree_1view_1get_1row_1separator_1func
(JNIEnv *env, jclass cls, jobject tree_view)
{
    GtkTreeView * tree_view_g = (GtkTreeView *)getPointerFromHandle(env, tree_view);
    return (jint)gtk_tree_view_get_row_separator_func(tree_view_g);
}

*/

#ifdef __cplusplus
}

#endif
