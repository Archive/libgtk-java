/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_Bin.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.Bin
 * Method:    gtk_bin_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_Bin_gtk_1bin_1get_1type (JNIEnv *env, jclass cls) 
{
    return (jint)gtk_bin_get_type ();
}

/*
 * Class:     org.gnu.gtk.Bin
 * Method:    gtk_bin_get_child
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_Bin_gtk_1bin_1get_1child (JNIEnv *env,
	jclass cls, jobject bin) 
{
    GtkBin *bin_g = (GtkBin *)getPointerFromHandle(env, bin);
    return getGObjectHandle(env, (GObject *) gtk_bin_get_child (bin_g));
}

#ifdef __cplusplus
}

#endif
