/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_MenuToolButton.h"
#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jint JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_menu_tool_button_get_type();
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1new
(JNIEnv *env, jclass cls, jobject icon_widget, jstring label)
{
    GtkWidget * icon_widget_g = 
        (GtkWidget *)getPointerFromHandle(env, icon_widget);
    const gchar * label_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, label, 0);
    jobject ret_g = getGObjectHandle(env, (GObject *)
    		gtk_menu_tool_button_new(icon_widget_g, label_g));
    (*env)->ReleaseStringUTFChars(env, label, label_g);
    return ret_g;
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1new_1from_1stock
(JNIEnv *env, jclass cls, jstring stock_id)
{
    const gchar * stock_id_g = (const gchar *)
    		(*env)->GetStringUTFChars(env, stock_id, 0);
    jobject ret_g = getGObjectHandle(env, (GObject *)
    		gtk_menu_tool_button_new_from_stock(stock_id_g));
    (*env)->ReleaseStringUTFChars(env, stock_id, stock_id_g);
    return ret_g;
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1set_1menu
(JNIEnv *env, jclass cls, jobject button, jobject menu)
{
    GtkMenuToolButton * button_g = 
        (GtkMenuToolButton *)getPointerFromHandle(env, button);
    GtkWidget * menu_g = (GtkWidget *)getPointerFromHandle(env, menu);
    gtk_menu_tool_button_set_menu(button_g, menu_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1get_1menu
(JNIEnv *env, jclass cls, jobject button)
{
    GtkMenuToolButton * button_g = 
        (GtkMenuToolButton *)getPointerFromHandle(env, button);
    return getGObjectHandle(env, (GObject *) gtk_menu_tool_button_get_menu(button_g));
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_MenuToolButton_gtk_1menu_1tool_1button_1set_1arrow_1tooltip
(JNIEnv *env, jclass cls, jobject button, jobject tooltips, jstring tip_text, jstring tip_private)
{
    GtkMenuToolButton * button_g = 
        (GtkMenuToolButton *)getPointerFromHandle(env, button);
    GtkTooltips * tooltips_g = 
        (GtkTooltips *)getPointerFromHandle(env, tooltips);
    const gchar * tip_text_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, tip_text, 0);
    const gchar * tip_private_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, tip_private, 0);
    gtk_menu_tool_button_set_arrow_tooltip(button_g, tooltips_g, 
                                           tip_text_g, tip_private_g);
    (*env)->ReleaseStringUTFChars(env, tip_text, tip_text_g);
    (*env)->ReleaseStringUTFChars(env, tip_private, tip_private_g);
}


#ifdef __cplusplus
}

#endif
