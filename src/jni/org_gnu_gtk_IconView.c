/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#ifndef _Included_org_gnu_gtk_IconView
#define _Included_org_gnu_gtk_IconView
#include "org_gnu_gtk_IconView.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)gtk_icon_view_get_type();
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1new
  (JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, (GObject *) gtk_icon_view_new());
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_new_with_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1new_1with_1model
  (JNIEnv *env, jclass cls, jobject model)
{
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	return getGObjectHandle(env, (GObject *) gtk_icon_view_new_with_model(model_g));
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_model
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1model
  (JNIEnv *env, jclass cls, jobject view, jobject model)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	GtkTreeModel* model_g = (GtkTreeModel*)getPointerFromHandle(env, model);
	gtk_icon_view_set_model(view_g, model_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_model
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1model
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return getGObjectHandle(env, (GObject *) gtk_icon_view_get_model(view_g));
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_text_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1text_1column
  (JNIEnv *env, jclass cls, jobject view, jint column)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_text_column(view_g, (gint)column);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_text_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1text_1column
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_text_column(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_markup_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1markup_1column
  (JNIEnv *env, jclass cls, jobject view, jint column)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_markup_column(view_g, (gint)column);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_markup_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1markup_1column
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_markup_column(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_pixbuf_column
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1pixbuf_1column
  (JNIEnv *env, jclass cls, jobject view, jint column)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_pixbuf_column(view_g, (gint)column);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_pixbuf_column
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1pixbuf_1column
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_pixbuf_column(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_orientation
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1orientation
  (JNIEnv *env, jclass cls, jobject view, jint orien)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_orientation(view_g, (GtkOrientation)orien);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_orientation
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1orientation
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_orientation(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_columns
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1columns
  (JNIEnv *env, jclass cls, jobject view, jint cols)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_columns(view_g, (gint)cols);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_columns
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1columns
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_columns(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_item_width
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1item_1width
  (JNIEnv *env, jclass cls, jobject view, jint width)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_item_width(view_g, (gint)width);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_item_width
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1item_1width
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_item_width(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1spacing
  (JNIEnv *env, jclass cls, jobject view, jint spacing)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_spacing(view_g, (gint)spacing);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1spacing
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_spacing(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_row_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1row_1spacing
  (JNIEnv *env, jclass cls, jobject view, jint spacing)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_row_spacing(view_g, (gint)spacing);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_row_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1row_1spacing
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_row_spacing(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_column_spacing
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1column_1spacing
  (JNIEnv *env, jclass cls, jobject view, jint spacing)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_column_spacing(view_g, (gint)spacing);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_column_spacing
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1column_1spacing
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_column_spacing(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_margin
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1margin
  (JNIEnv *env, jclass cls, jobject view, jint margin)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_margin(view_g, (gint)margin);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_margin
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1margin
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_margin(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_path_at_pos
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1path_1at_1pos
  (JNIEnv *env, jclass cls, jobject view, jint x, jint y)
{
	GtkTreePath *path;
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	path = gtk_icon_view_get_path_at_pos(view_g, (gint)x, (gint)y);
	return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, (GBoxedCopyFunc)
			gtk_tree_path_copy, (GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_selection_mode
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1selection_1mode
  (JNIEnv *env, jclass cls, jobject view, jint mode)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_set_selection_mode(view_g, (GtkSelectionMode)mode);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_selection_mode
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1selection_1mode
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return (jint)gtk_icon_view_get_selection_mode(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_select_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1select_1path
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	GtkTreePath* path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	gtk_icon_view_select_path(view_g, path_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_unselect_path
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1unselect_1path
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	GtkTreePath* path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	gtk_icon_view_unselect_path(view_g, path_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_path_is_selected
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1path_1is_1selected
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	GtkTreePath* path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	return (jboolean)gtk_icon_view_path_is_selected(view_g, path_g);
}

static jobject getTreePath(JNIEnv *env, gpointer path)	{
	return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, NULL,
			(GBoxedFreeFunc) gtk_tree_path_free);
}
/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_selected_items
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1selected_1items
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	return getGBoxedHandlesFromGList(env, gtk_icon_view_get_selected_items(view_g),
			(GetHandleFunc) getTreePath);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_select_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1select_1all
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_select_all(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_unselect_all
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1unselect_1all
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	gtk_icon_view_unselect_all(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_item_activated
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1item_1activated
  (JNIEnv *env, jclass cls, jobject view, jobject path)
{
	GtkIconView* view_g = (GtkIconView*)getPointerFromHandle(env, view);
	GtkTreePath* path_g = (GtkTreePath*)getPointerFromHandle(env, path);
	gtk_icon_view_item_activated(view_g, path_g);	
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_cell_renderer_at_pos
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1cell_1renderer_1at_1pos
  (JNIEnv *env, jclass cls, jobject view, jint x, jint y)
{
	GtkIconView *view_g;
	GtkCellRenderer **cell;
	gboolean result;
	
	cell = NULL;
	view_g = (GtkIconView *) getPointerFromHandle(env, view);
	result = gtk_icon_view_get_item_at_pos(view_g, x, y, NULL, cell);
	
	if (result == TRUE)	{
	  return getGObjectHandle(env, G_OBJECT(&cell[x][y]));
	}
	
	return NULL;
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_cursor_path
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1cursor_1path
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView *view_g;
	GtkTreePath *path;
	gboolean result;
	
	path = NULL;
	
	view_g = (GtkIconView *) getPointerFromHandle(env, view);
	result = gtk_icon_view_get_cursor(view_g, &path, NULL);
	
	if (result == TRUE)	{
		return getStructHandle(env, view_g, NULL, (JGFreeFunc) gtk_tree_path_free);
	}
	
	return NULL;
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_cursor_cell_renderer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1cursor_1cell_1renderer
  (JNIEnv *env, jclass cls, jobject view)
{
	GtkIconView *view_g;
	GtkCellRenderer *cell;
	gboolean result;
	
	cell = NULL;
	view_g = (GtkIconView *) getPointerFromHandle(env, view);
	result = gtk_icon_view_get_cursor(view_g, NULL, &cell);
	
	if (result == TRUE)	{
		return getGObjectHandle(env, (GObject *) cell);
	}
	
	return NULL;
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_reorderable 
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1reorderable
(JNIEnv *env, jclass cls, jobject view, jboolean reorderable) 
{
    GtkIconView* view_g;
    view_g = (GtkIconView *) getPointerFromHandle(env, view);
    gtk_icon_view_set_reorderable (view_g, (gboolean) reorderable);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_get_reorderable 
 * Signature: (I)I
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1get_1reorderable
(JNIEnv *env, jclass cls, jobject view)
{
    GtkIconView* view_g;
    view_g = (GtkIconView *) getPointerFromHandle(env, view);
    return (jboolean) gtk_icon_view_get_reorderable(view_g);
}

/*
 * Class:     org_gnu_gtk_IconView
 * Method:    gtk_icon_view_set_cursor 
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IconView_gtk_1icon_1view_1set_1cursor
(JNIEnv *env, jclass cls, jobject view, jobject path, jobject cell, jboolean
		startEditing)
{
    GtkIconView *view_g;
    GtkTreePath *path_g;
    GtkCellRenderer *cell_g;
    
    view_g = (GtkIconView *) getPointerFromHandle(env, view);
    path_g = (GtkTreePath *) getPointerFromHandle(env, path);
    cell_g = (GtkCellRenderer *) getPointerFromHandle(env, cell);
  
    gtk_icon_view_set_cursor(view_g, path_g, cell_g, (gboolean) startEditing);
}

#ifdef __cplusplus
}
#endif
#endif
