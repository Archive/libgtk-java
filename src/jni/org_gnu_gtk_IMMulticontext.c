/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_IMMulticontext.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.IMMulticontext
 * Method:    gtk_im_multicontext_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_IMMulticontext_gtk_1im_1multicontext_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_im_multicontext_get_type ();
}

/*
 * Class:     org.gnu.gtk.IMMulticontext
 * Method:    gtk_im_multicontext_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_IMMulticontext_gtk_1im_1multicontext_1new (JNIEnv *env, 
    jclass cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_im_multicontext_new ());
}

/*
 * Class:     org.gnu.gtk.IMMulticontext
 * Method:    gtk_im_multicontext_append_menuitems
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_IMMulticontext_gtk_1im_1multicontext_1append_1menuitems (
    JNIEnv *env, jclass cls, jobject context, jobject menushell) 
{
    GtkIMMulticontext *context_g = (GtkIMMulticontext *)getPointerFromHandle(env, context);
    GtkMenuShell *menushell_g = (GtkMenuShell *)getPointerFromHandle(env, menushell);
    gtk_im_multicontext_append_menuitems (context_g, menushell_g);
}


#ifdef __cplusplus
}

#endif
