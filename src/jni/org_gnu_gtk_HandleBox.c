/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_HandleBox.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1get_1type (JNIEnv *env, 
    jclass cls) 
{
    return (jint)gtk_handle_box_get_type ();
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1new (JNIEnv *env, jclass 
    cls) 
{
    return getGObjectHandle(env, (GObject *) gtk_handle_box_new ());
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_set_shadow_type
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1set_1shadow_1type (JNIEnv 
    *env, jclass cls, jobject handle_box, jint type) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    GtkShadowType type_g = (GtkShadowType) type;
    gtk_handle_box_set_shadow_type (handle_box_g, type_g);
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_get_shadow_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1get_1shadow_1type (JNIEnv 
    *env, jclass cls, jobject handle_box) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    return (jint) (gtk_handle_box_get_shadow_type (handle_box_g));
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_set_handle_position
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1set_1handle_1position (
    JNIEnv *env, jclass cls, jobject handle_box, jint position) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    GtkPositionType position_g = (GtkPositionType) position;
    gtk_handle_box_set_handle_position (handle_box_g, position_g);
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_get_handle_position
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1get_1handle_1position (
    JNIEnv *env, jclass cls, jobject handle_box) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    return (jint) (gtk_handle_box_get_handle_position (handle_box_g));
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_set_snap_edge
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1set_1snap_1edge (JNIEnv 
    *env, jclass cls, jobject handle_box, jint edge) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    GtkPositionType edge_g = (GtkPositionType) edge;
    gtk_handle_box_set_snap_edge (handle_box_g, edge_g);
}

/*
 * Class:     org.gnu.gtk.HandleBox
 * Method:    gtk_handle_box_get_snap_edge
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_HandleBox_gtk_1handle_1box_1get_1snap_1edge (JNIEnv 
    *env, jclass cls, jobject handle_box) 
{
    GtkHandleBox *handle_box_g = (GtkHandleBox *)getPointerFromHandle(env, handle_box);
    return (jint) (gtk_handle_box_get_snap_edge (handle_box_g));
}

#ifdef __cplusplus
}

#endif
