/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <pango/pango.h>
#include "jg_jnu.h"
#include "gtk_java.h"

#include "org_gnu_pango_FontFamily.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.pango.FontFamily
 * Method:    pango_font_family_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_pango_FontFamily_pango_1font_1family_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)pango_font_family_get_type ();
}

/*
 * Class:     org.gnu.pango.FontFamily
 * Method:    pango_font_family_list_faces
 */
JNIEXPORT jobjectArray JNICALL Java_org_gnu_pango_FontFamily_pango_1font_1family_1list_1faces (JNIEnv 
    *env, jclass cls, jobject family) 
{
    PangoFontFamily *family_g = 
    	(PangoFontFamily *)getPointerFromHandle(env, family);
    PangoFontFace **faces;
    gint numFaces;
    pango_font_family_list_faces (family_g, &faces, &numFaces);
    return getGObjectHandlesFromPointers(env, (void**)faces, numFaces);
}

/*
 * Class:     org.gnu.pango.FontFamily
 * Method:    pango_font_family_get_name
 */
JNIEXPORT jstring JNICALL Java_org_gnu_pango_FontFamily_pango_1font_1family_1get_1name (
    JNIEnv *env, jclass cls, jobject family) 
{
    gchar *result_g = (gchar *)pango_font_family_get_name (
    		(PangoFontFamily *)getPointerFromHandle(env, family));
    return (*env)->NewStringUTF(env, result_g);
}


#ifdef __cplusplus
}

#endif
