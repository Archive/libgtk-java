/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_MessageDialog.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.gtk.MessageDialog
 * Method:    gtk_message_dialog_get_type
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1get_1type (JNIEnv 
    *env, jclass cls) 
{
    return (jint)gtk_message_dialog_get_type ();
}

/*
 * Class:     org.gnu.gtk.MessageDialog
 * Method:    gtk_message_dialog_new
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1new (JNIEnv *env, 
    jclass cls, jobject parent, jint flags, jint type, jint buttons, jstring messageFormat) 
{
    GtkWindow *parent_g = (GtkWindow *)getPointerFromHandle(env, parent);
    GtkDialogFlags flags_g = (GtkDialogFlags) flags;
    GtkMessageType type_g = (GtkMessageType) type;
    GtkButtonsType buttons_g = (GtkButtonsType) buttons;
    const gchar* messageFormat_g = (*env)->GetStringUTFChars(env, messageFormat, NULL);
	jobject retval =  getGObjectHandle(env, (GObject *)
			gtk_message_dialog_new (parent_g, flags_g, type_g, buttons_g, messageFormat_g));
	(*env)->ReleaseStringUTFChars(env, messageFormat, messageFormat_g);
	return retval;
}

/*
 * Class:     org_gnu_gtk_MessageDialog
 * Method:    gtk_message_dialog_new_with_markup  
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1new_1with_1markup
  (JNIEnv *env, jclass cls, jobject parent, jint flags, jint type, jint buttons, jstring message)
{
    GtkWindow *parent_g = (GtkWindow *)getPointerFromHandle(env, parent);
	const gchar* m = (*env)->GetStringUTFChars(env, message, NULL);
	jobject handle = getGObjectHandle(env, (GObject *)
			gtk_message_dialog_new_with_markup(parent_g, (GtkDialogFlags)flags,
					(GtkMessageType)type, (GtkButtonsType)buttons, m));
	(*env)->ReleaseStringUTFChars(env, message, m);
	return handle;
}

/*  
 * Class:     org_gnu_gtk_MessageDialog
 * Method:    gtk_message_dialog_set_markup
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1set_1markup
  (JNIEnv *env, jclass cls, jobject mdialog, jstring message)
{
	GtkMessageDialog* mdialog_g = (GtkMessageDialog*)getPointerFromHandle(env, mdialog);
	const gchar* m = (*env)->GetStringUTFChars(env, message, NULL);
	gtk_message_dialog_set_markup(mdialog_g, m);
	(*env)->ReleaseStringUTFChars(env, message, m);
}

/* GTK 2.6 additions. */
JNIEXPORT void JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1format_1secondary_1text
(JNIEnv *env, jclass cls, jobject message_dialog, jstring message_format)
{
    GtkMessageDialog * message_dialog_g = 
        (GtkMessageDialog *)getPointerFromHandle(env, message_dialog);
    const gchar * message_format_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, message_format, 0);
    gtk_message_dialog_format_secondary_text(message_dialog_g, message_format_g);
    (*env)->ReleaseStringUTFChars(env, message_format, message_format_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtk_MessageDialog_gtk_1message_1dialog_1format_1secondary_1markup
(JNIEnv *env, jclass cls, jobject message_dialog, jstring message_format)
{
    GtkMessageDialog * message_dialog_g = 
        (GtkMessageDialog *)getPointerFromHandle(env, message_dialog);
    const gchar * message_format_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, message_format, 0);
    gtk_message_dialog_format_secondary_markup(message_dialog_g, message_format_g);
    (*env)->ReleaseStringUTFChars(env, message_format, message_format_g);
}

#ifdef __cplusplus
}

#endif
