/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_TreeModelFilter.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_new
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1new
		(JNIEnv *env, jclass cls, jobject model, jobject root) {
    GtkTreeModel *model_g = 
        (GtkTreeModel*)getPointerFromHandle(env, model);
    GtkTreePath *root_g = 
        (GtkTreePath*)getPointerFromHandle(env, root);

    return getGObjectHandle(env, (GObject *) gtk_tree_model_filter_new(model_g, root_g));
}

static gboolean treeModelFilterVisibleFunc(GtkTreeModel *model,
		GtkTreeIter *iter, gpointer data)
{
	jobject model_handle;
	jobject iter_handle;
	JGFuncCallbackRef *ref = (JGFuncCallbackRef*) data;
	model_handle = getGObjectHandleAndRef(ref->env, (GObject *) model);
	iter_handle = getGBoxedHandle(ref->env, iter, GTK_TYPE_TREE_ITER,
			(GBoxedCopyFunc) gtk_tree_iter_copy, (GBoxedFreeFunc) gtk_tree_iter_free); 
	return (*ref->env)->CallBooleanMethod(ref->env, ref->obj, ref->methodID,
			model_handle, iter_handle);
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_set_visible_func
 * Signature: (Lorg/gnu/glib/Handle;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1set_1visible_1func(JNIEnv *env, jclass class, jobject handle, jobject ftr, jstring func) {
    JGFuncCallbackRef *ref = g_new( JGFuncCallbackRef, 1 );
    ref->env = env;
    ref->obj = (* env)->NewGlobalRef(env, ftr);

    const char *funcname = (*env)->GetStringUTFChars(env, func, NULL);
    // Get method id for the callback method name.
    ref->methodID = 
        (*env)->GetMethodID(env, 
                            (*env)->GetObjectClass(env, ref->obj), 
                            funcname, "(Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Z" );
    if ( ref->methodID == NULL ) {
        (*env)->ReleaseStringUTFChars(env, func, funcname);
        g_free( ref );
        // Error!  Throw exception!
        return;
    }
    (*env)->ReleaseStringUTFChars(env, func, funcname);

    gtk_tree_model_filter_set_visible_func((GtkTreeModelFilter *) 
    		getPointerFromHandle(env,handle), treeModelFilterVisibleFunc, ref, NULL);
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_set_visible_column
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1set_1visible_1column(JNIEnv *env, jclass class, jobject filter, jint column) {
    GtkTreeModelFilter *filter_g = 
        (GtkTreeModelFilter*)getPointerFromHandle(env, filter);
    gtk_tree_model_filter_set_visible_column( filter_g, (gint)column );
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_get_model
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1get_1model(JNIEnv *env, jclass class, jobject handle) {
    GtkTreeModelFilter *handle_g = 
        (GtkTreeModelFilter*)getPointerFromHandle(env, handle);
    return getGObjectHandle(env, (GObject *) gtk_tree_model_filter_get_model( handle_g ));
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_refilter
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1refilter(JNIEnv *env, jclass class, jobject filter) {
    GtkTreeModelFilter *filter_g = 
        (GtkTreeModelFilter *)getPointerFromHandle(env, filter);
    gtk_tree_model_filter_refilter( filter_g );
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_clear_cache
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1clear_1cache(JNIEnv *env, jclass class, jobject filter) {
    GtkTreeModelFilter *filter_g = 
        (GtkTreeModelFilter *)getPointerFromHandle(env, filter);
    gtk_tree_model_filter_clear_cache( filter_g );
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_convert_child_path_to_path
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1convert_1child_1path_1to_1path
		(JNIEnv *env, jclass class, jobject filter, jobject child_path) {
	GtkTreePath *path;
    GtkTreeModelFilter *filter_g = 
        (GtkTreeModelFilter *)getPointerFromHandle(env, filter);
    GtkTreePath *child_path_g = 
        (GtkTreePath *)getPointerFromHandle(env, child_path);
    path = gtk_tree_model_filter_convert_child_path_to_path(filter_g, child_path_g);
    return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, NULL,
    		(GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_convert_path_to_child_path
 * Signature: (Lorg/gnu/glib/Handle;Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1convert_1path_1to_1child_1path
		(JNIEnv *env, jclass class, jobject filter, jobject filter_path) {
	GtkTreePath *path;
    GtkTreeModelFilter *filter_g = 
        (GtkTreeModelFilter *)getPointerFromHandle(env, filter);
    GtkTreePath *path_g = 
        (GtkTreePath *)getPointerFromHandle(env, filter_path);
    path = gtk_tree_model_filter_convert_path_to_child_path(filter_g, path_g);
    return getGBoxedHandle(env, path, GTK_TYPE_TREE_PATH, NULL,
    		(GBoxedFreeFunc) gtk_tree_path_free);
}

/*
 * Class:     org_gnu_gtk_TreeModelFilter
 * Method:    gtk_tree_model_filter_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_TreeModelFilter_gtk_1tree_1model_1filter_1get_1type(JNIEnv *env, jclass cls) {
    return gtk_tree_model_filter_get_type();
}

#ifdef __cplusplus
}
#endif
