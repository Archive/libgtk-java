/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <jg_jnu.h>
#include "gtk_java.h"

#include "org_gnu_gtk_ToolButton.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1type(JNIEnv *env, jclass cls)
{
    return (jint)gtk_tool_button_get_type();
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_new
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1new(JNIEnv *env, jclass cls, jobject iconWidget, jstring label)
{
    GtkWidget *iconWidget_g = 
        (GtkWidget*)getPointerFromHandle(env, iconWidget);
    const gchar* l = (*env)->GetStringUTFChars(env, label, NULL);
    jobject handle = 
        getGObjectHandle(env, (GObject *) gtk_tool_button_new(iconWidget_g, l));
    (*env)->ReleaseStringUTFChars(env, label, l);
    return handle;
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_new_from_stock
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1new_1from_1stock(JNIEnv *env, jclass cls, jstring stock)
{
    const gchar* s = (*env)->GetStringUTFChars(env, stock, NULL);
    jobject handle = 
        getGObjectHandle(env, (GObject *) gtk_tool_button_new_from_stock(s));
    (*env)->ReleaseStringUTFChars(env, stock, s);
    return handle;
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_set_label
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1set_1label
		(JNIEnv *env, jclass cls, jobject button, jstring label)
{
    GtkToolButton *button_g;
    const gchar* label_g;
    button_g = (GtkToolButton*)getPointerFromHandle(env, button);
    label_g = (*env)->GetStringUTFChars(env, label, NULL);
    gtk_tool_button_set_label(button_g, label_g);
    (*env)->ReleaseStringUTFChars(env, label, label_g);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_label
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1label(JNIEnv *env, jclass cls, jobject button)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    return (*env)->NewStringUTF(env, gtk_tool_button_get_label(button_g));
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_set_use_underline
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1set_1use_1underline(JNIEnv *env, jclass cls, jobject button, jboolean underline)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    gboolean underline_g = (gboolean)underline;
    gtk_tool_button_set_use_underline(button_g, underline_g);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_use_underline
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1use_1underline(JNIEnv *env, jclass cls, jobject button)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    return (jboolean)gtk_tool_button_get_use_underline(button_g);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_set_stock_id
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1set_1stock_1id(JNIEnv *env, jclass cls, jobject button, jstring stock)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    const gchar* s = (*env)->GetStringUTFChars(env, stock, NULL);
    gtk_tool_button_set_stock_id(button_g, s);
    (*env)->ReleaseStringUTFChars(env, stock, s);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_stock_id
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1stock_1id(JNIEnv *env, jclass cls, jobject button)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    return (*env)->NewStringUTF(env, gtk_tool_button_get_stock_id(button_g));
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_set_icon_widget
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1set_1icon_1widget(JNIEnv *env, jclass cls, jobject button, jobject icon)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    GtkWidget *icon_g = (GtkWidget*)getPointerFromHandle(env, icon);
    gtk_tool_button_set_icon_widget(button_g, icon_g);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_icon_widget
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1icon_1widget(JNIEnv *env, jclass cls, jobject button)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    return getGObjectHandle(env, (GObject *) gtk_tool_button_get_icon_widget(button_g));
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_set_label_widget
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1set_1label_1widget(JNIEnv *env, jclass cls, jobject button, jobject label)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    GtkWidget *label_g = (GtkWidget*)getPointerFromHandle(env, label);
    gtk_tool_button_set_label_widget(button_g, label_g);
}

/*
 * Class:     org_gnu_gtk_ToolButton
 * Method:    gtk_tool_button_get_label_widget
 * Signature: (I)I
 */
JNIEXPORT jobject JNICALL Java_org_gnu_gtk_ToolButton_gtk_1tool_1button_1get_1label_1widget(JNIEnv *env, jclass cls, jobject button)
{
    GtkToolButton *button_g = 
        (GtkToolButton*)getPointerFromHandle(env, button);
    return getGObjectHandle(env, (GObject *) gtk_tool_button_get_label_widget(button_g));
}

#ifdef __cplusplus
}
#endif
