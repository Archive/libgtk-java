/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is a base class providing alignment and padding to a number of
 * displayable widgets.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Misc</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Misc extends Widget {

    protected Misc(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static Misc getMisc(Handle handle) {
        if (handle == null)
            return null;

        Misc obj = (Misc) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Misc(handle);

        return obj;
    }

    /**
     * Sets the alignment of a widget.
     * 
     * @param xAlign
     *            The horizontal alignment from 0 (left) to 1 (right).
     * @param yAlign
     *            The vertical alignment from 0 (top) to 1 (bottom).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAlignment(double xAlign, double yAlign) {
        Misc.gtk_misc_set_alignment(getHandle(), xAlign, yAlign);
    }

    /**
     * Returns the horizontal alignment of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getXAlign() {
        double[] xAlign = new double[1];
        double[] yAlign = new double[1];
        Misc.gtk_misc_get_alignment(getHandle(), xAlign, yAlign);
        return xAlign[0];
    }

    /**
     * Returns the vertical alignment of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getYAlign() {
        double[] xAlign = new double[1];
        double[] yAlign = new double[1];
        Misc.gtk_misc_get_alignment(getHandle(), xAlign, yAlign);
        return yAlign[0];
    }

    /**
     * Sets the amount of space to add around the widget.
     * 
     * @param xPad
     *            The amount of space to add to the left and right of the
     *            widget.
     * @param yPad
     *            The amount of space to add to the top and bottom of the
     *            widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPadding(int xPad, int yPad) {
        Misc.gtk_misc_set_padding(getHandle(), xPad, yPad);
    }

    /**
     * Retrieve the amount of space added to the left and right of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getXPadding() {
        int[] xPad = new int[1];
        int[] yPad = new int[1];
        Misc.gtk_misc_get_padding(getHandle(), xPad, yPad);
        return xPad[0];
    }

    /**
     * Retrieve the amount of space added to the top and bottom of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getYPadding() {
        int[] xPad = new int[1];
        int[] yPad = new int[1];
        Misc.gtk_misc_get_padding(getHandle(), xPad, yPad);
        return yPad[0];
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_misc_get_type());
    }

    native static final protected int gtk_misc_get_type();

    native static final protected void gtk_misc_set_alignment(Handle misc,
            double xalign, double yalign);

    native static final protected void gtk_misc_get_alignment(Handle misc,
            double[] xalign, double[] yalign);

    native static final protected void gtk_misc_set_padding(Handle misc,
            int xpad, int ypad);

    native static final protected void gtk_misc_get_padding(Handle misc,
            int[] xpad, int[] ypad);

}
