/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Boxed;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This object manages a collection of {@link org.gnu.gtk.IconSet}; an IconSet
 * manages a set of variants of a particular icon. Icons in an IconFactory are
 * named by a stock ID, which is a simple string identifying the icon.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconFactory</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconFactory extends GObject {
    /**
     * Construct a new IconFactory object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconFactory() {
        super(gtk_icon_factory_new());
    }

    /**
     * Add the given IconSet to this IconFactory.
     * 
     * @param stockID
     *            The icon name
     * @param iconSet
     *            The IconSet to add to this factory.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addIconSet(String stockID, IconSet iconSet) {
        gtk_icon_factory_add(getHandle(), stockID, iconSet.getHandle());
    }

    /**
     * Looks up stockID from the icon factory, returning an icon set if found.
     * 
     * @param stockID
     *            The stockID to use for the search
     * @return The IconSet that matches the stockID or null if one is not found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSet lookupIconSet(String stockID) {
        Handle handle = gtk_icon_factory_lookup(getHandle(), stockID);
        if (handle == null) {
            return null;
        }
        IconSet iconSet = (IconSet) Boxed.getBoxedFromHandle(handle);
        if (iconSet == null) {
            iconSet = new IconSet(handle);
        }
        return iconSet;
    }

    /**
     * Adds this IconFactory to the list of icon factories searched by
     * Style.lookupIconSet. This means that objects that take stock IDs will be
     * able to find the icons in this factory. There will normally be an
     * IconFactory added for each library or application that comes with icons.
     * The default icon factories can be overridden by themes.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addDefault() {
        gtk_icon_factory_add_default(getHandle());
    }

    /**
     * Removes this icon factory from the list of default icon factories.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeDefault() {
        gtk_icon_factory_remove_default(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_icon_factory_get_type());
    }

    native static final protected int gtk_icon_factory_get_type();

    native static final protected Handle gtk_icon_factory_new();

    native static final protected void gtk_icon_factory_add(Handle factory,
            String stockId, Handle iconSet);

    native static final protected Handle gtk_icon_factory_lookup(
            Handle factory, String stockId);

    native static final protected void gtk_icon_factory_add_default(
            Handle factory);

    native static final protected void gtk_icon_factory_remove_default(
            Handle factory);

    native static final protected Handle gtk_icon_factory_lookup_default(
            String stockId);

}
