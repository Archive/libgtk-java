/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The GammaCurve widget displays an interactive window that enables the mouse
 * to insert and move points. The points can be plotted as curves or linearly.
 * Selecting the button with the gamma symbol enables you to alter the gamma
 * value.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.GammaCurve</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class GammaCurve extends VBox {
    public GammaCurve() {
        super(gtk_gamma_curve_new());
    }

    /**
     * Construct a new GammaCurve from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GammaCurve(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static GammaCurve getGammaCurve(Handle handle) {
        if (handle == null)
            return null;

        GammaCurve obj = (GammaCurve) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new GammaCurve(handle);

        return obj;
    }

    public double getGamma() {
        return getGamma(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_gamma_curve_get_type());
    }

    native static final protected Handle getTable(Handle cptr);

    native static final protected Handle getCurve(Handle cptr);

    native static final protected double getGamma(Handle cptr);

    native static final protected Handle getGammaDialog(Handle cptr);

    native static final protected Handle getGammaText(Handle cptr);

    native static final protected int gtk_gamma_curve_get_type();

    native static final protected Handle gtk_gamma_curve_new();
}
