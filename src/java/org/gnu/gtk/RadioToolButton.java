/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RadioToolButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RadioToolButton extends ToggleToolButton {

    public RadioToolButton(RadioToolButton[] group) {
        super(init1(group));
    }

    private static Handle init1(RadioToolButton[] group) {
        if (null == group)
            return gtk_radio_tool_button_new(null);
        else {
            Handle[] hndls = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                hndls[i] = group[i].getHandle();
            }
            return gtk_radio_tool_button_new(hndls);
        }
    }

    public RadioToolButton(RadioToolButton[] group, String stockId) {
        super(init2(group, stockId));
    }

    private static Handle init2(RadioToolButton[] group, String stockId) {
        if (null == group)
            return gtk_radio_tool_button_new_from_stock(null, stockId);
        else {
            Handle[] hndls = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                hndls[i] = group[i].getHandle();
            }
            return gtk_radio_tool_button_new_from_stock(hndls, stockId);
        }
    }

    public RadioToolButton(RadioToolButton button) {
        super(gtk_radio_tool_button_new_from_widget(button.getHandle()));
    }

    public RadioToolButton(RadioToolButton button, String stockId) {
        super(gtk_radio_tool_button_new_with_stock_from_widget(button
                .getHandle(), stockId));
    }

    private RadioToolButton(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static RadioToolButton getRadioToolButton(Handle handle) {
        if (handle == null)
            return null;

        RadioToolButton obj = (RadioToolButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new RadioToolButton(handle);

        return obj;
    }

    public RadioToolButton[] getGroup() {
        Handle[] hndls = gtk_radio_tool_button_get_group(getHandle());
        if (null == hndls)
            return null;
        RadioToolButton[] group = new RadioToolButton[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            group[i] = RadioToolButton.getRadioToolButton(hndls[i]);
        }
        return group;
    }

    public void setGroup(RadioToolButton[] group) {
        if (null == group)
            gtk_radio_tool_button_set_group(getHandle(), null);
        else {
            Handle[] hndls = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                hndls[i] = group[i].getHandle();
            }
            gtk_radio_tool_button_set_group(getHandle(), hndls);
        }
    }

    native static final protected int gtk_radio_tool_button_get_type();

    native static final protected Handle gtk_radio_tool_button_new(
            Handle[] group);

    native static final protected Handle gtk_radio_tool_button_new_from_stock(
            Handle[] group, String stockId);

    native static final protected Handle gtk_radio_tool_button_new_from_widget(
            Handle widget);

    native static final protected Handle gtk_radio_tool_button_new_with_stock_from_widget(
            Handle widget, String stockId);

    native static final protected Handle[] gtk_radio_tool_button_get_group(
            Handle button);

    native static final protected void gtk_radio_tool_button_set_group(
            Handle button, Handle[] group);

}
