/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

/**
 * Tells about the state of a GtkObject.
 * 
 * <pre>
 *  IN_DESTRUCTION
 *  The object is currently being destroyed.
 *  FLOATING
 *  The object is orphaned.
 *  RESERVED_1
 *  Reserved for future use
 *  RESERVED_2
 *  Reserved for future use
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ObjectFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ObjectFlags extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _IN_DESTRUCTION = 1 << 0;

    static final public org.gnu.gtk.ObjectFlags IN_DESTRUCTION = new org.gnu.gtk.ObjectFlags(
            _IN_DESTRUCTION);

    static final private int _FLOATING = 1 << 1;

    static final public org.gnu.gtk.ObjectFlags FLOATING = new org.gnu.gtk.ObjectFlags(
            _FLOATING);

    static final private int _RESERVED_1 = 1 << 2;

    static final public org.gnu.gtk.ObjectFlags RESERVED_1 = new org.gnu.gtk.ObjectFlags(
            _RESERVED_1);

    static final private int _RESERVED_2 = 1 << 3;

    static final public org.gnu.gtk.ObjectFlags RESERVED_2 = new org.gnu.gtk.ObjectFlags(
            _RESERVED_2);

    static final private org.gnu.gtk.ObjectFlags[] theInterned = new org.gnu.gtk.ObjectFlags[] {
            new org.gnu.gtk.ObjectFlags(0), IN_DESTRUCTION, FLOATING,
            new org.gnu.gtk.ObjectFlags(3), RESERVED_1,
            new org.gnu.gtk.ObjectFlags(5), new org.gnu.gtk.ObjectFlags(6),
            new org.gnu.gtk.ObjectFlags(7), RESERVED_2 }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ObjectFlags theSacrificialOne = new org.gnu.gtk.ObjectFlags(
            0);

    static public org.gnu.gtk.ObjectFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ObjectFlags already = (org.gnu.gtk.ObjectFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ObjectFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ObjectFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ObjectFlags or(org.gnu.gtk.ObjectFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ObjectFlags and(org.gnu.gtk.ObjectFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ObjectFlags xor(org.gnu.gtk.ObjectFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ObjectFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
