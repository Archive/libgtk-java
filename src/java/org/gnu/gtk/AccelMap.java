/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.KeyValue;
import org.gnu.gdk.ModifierType;
import org.gnu.glib.Struct;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class AccelMap extends GObject {
    private static AccelMap INSTANCE = new AccelMap(gtk_accel_map_get());

    private AccelMap(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AccelMap getAccelMap(Handle handle) {
        if (handle == null)
            return null;

        AccelMap obj = (AccelMap) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new AccelMap(handle);

        return obj;
    }

    public static AccelMap getAccelMap() {
        return INSTANCE;
    }

    /**
     * Loads a file previously saved with the same method, parses the file for
     * accelerator specifications, and propagates them accordingly.
     * 
     * @param filename
     *            The name of the file to read.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void load(String filename) {
        gtk_accel_map_load(filename);
    }

    /**
     * Saves the current accelerator specification (accelerator path, key, and
     * modifiers) to the filename specified.
     * 
     * @param filename
     *            The name of the file to write.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void save(String filename) {
        gtk_accel_map_save(filename);
    }

    /**
     * Locks the given accelerator path. If the accelerator map doesn't yet
     * contain an entry for accel_path, a new one is created. Locking an
     * accelerator path prevents its accelerator from being changed during
     * runtime. A locked accelerator path can be unlocked by
     * {@link #unlockPath(String)} Refer to
     * {@link #changeEntry(String, int, ModifierType, boolean)} for information
     * about runtime accelerator changes. If called more than once, accel_path
     * remains locked until {@link #unlockPath(String)} has been called an
     * equivalent number of times. Note that locking of individual accelerator
     * paths is independent from locking the {@link AccelGroup} containing them.
     * For runtime accelerator changes to be possible both the accelerator path
     * and its AccelGroup have to be unlocked.
     * 
     * @param path
     *            The path to lock
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void lockPath(String path) {
        gtk_accel_map_lock_path(path);
    }

    /**
     * Undoes the last call to {@link #lockPath(String)} on this accel_path.
     * Refer to {@link #lockPath(String)} for information about accelerator path
     * locking.
     * 
     * @param path
     *            The path to unlock
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void unlockPath(String path) {
        gtk_accel_map_unlock_path(path);
    }

    /**
     * Changes the key (from {@link KeyValue}) and {@link ModifierType}
     * currently associated with accelPath. Due to conflicts with other
     * accelerators, a change may not always be possible, replace indicates
     * whether other accelerators may be deleted to resolve such conflicts. A
     * change will only occur if all conflicts could be resolved (which might
     * not be the case if conflicting accelerators are locked). Successful
     * changes are indicated by a true return value.
     * 
     * @param accelPath
     *            The path to change
     * @param key
     *            The new KeySymbol for the path
     * @param mods
     *            The new modifiers to the path
     * @param replace
     *            If true, removes conflicting accelerators
     * @return true if the change succeeded, false, otherwise
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static boolean changeEntry(String accelPath, int key,
            ModifierType mods, boolean replace) {
        return gtk_accel_map_change_entry(accelPath, key, mods.getValue(),
                replace);
    }

    /**
     * Looks up the accelerator entry for accelPath and returns the AccelKey
     * 
     * @param accelPath
     *            The path to get the accelerator entry for
     * @return The AccelKey representing the accelerator
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AccelKey lookupEntry(String accelPath) {
        Handle hndl = Struct.getNullHandle();
        if (!gtk_accel_map_lookup_entry(accelPath, hndl))
            return null;

        return AccelKey.getAccelKey(hndl);
    }

    native static final Handle gtk_accel_map_get();

    native static final protected void gtk_accel_map_add_entry(
            String accelPath, int accelKey, int accelMods);

    native static final protected boolean gtk_accel_map_lookup_entry(
            String accelPath, Handle accelKey);

    native static final protected boolean gtk_accel_map_change_entry(
            String accelPath, int accekKey, int accelMods, boolean replace);

    native static final protected void gtk_accel_map_load(String fileName);

    native static final protected void gtk_accel_map_save(String fileName);

    native static final protected void gtk_accel_map_load_fd(int fd);

    native static final protected void gtk_accel_map_save_fd(int fd);

    native static final protected void gtk_accel_map_lock_path(String path);

    native static final protected void gtk_accel_map_unlock_path(String path);

    native static final protected void gtk_accel_map_add_filter(
            String filterPattern);

}
