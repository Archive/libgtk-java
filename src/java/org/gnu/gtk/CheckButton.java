/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A CheckButton widget displays a small button with a label next to it. The
 * button can be toggled on or off by the mouse, and will retain its state until
 * it is toggled again.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CheckButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CheckButton extends ToggleButton {
    /**
     * Creates a new CheckButton
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckButton() {
        super(gtk_check_button_new());
    }

    /**
     * Construct a check button using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckButton(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static CheckButton getCheckButton(Handle handle) {
        if (handle == null)
            return null;

        CheckButton obj = (CheckButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new CheckButton(handle);

        return obj;
    }

    /**
     * Creates a new Checkbutton with a label. If hasMnemonic is true, any
     * mnemonic characters in the {@link Label} will be interpreted. See the
     * description of the {@link Label} class for more details of mnemonic
     * characters.
     * 
     * @param label
     *            Text to be displayed in a label next to the check box.
     * @param hasMnemonic
     *            Determines whether to interpret Mnemonic characters
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckButton(String label, boolean hasMnemonic) {
        super(init(label, hasMnemonic));
    }

    private static Handle init(String label, boolean hasMnemonic) {
        if (hasMnemonic)
            return gtk_check_button_new_with_mnemonic(label);
        else
            return gtk_check_button_new_with_label(label);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_check_button_get_type());
    }

    native static final protected int gtk_check_button_get_type();

    native static final protected Handle gtk_check_button_new();

    native static final protected Handle gtk_check_button_new_with_label(
            String label);

    native static final protected Handle gtk_check_button_new_with_mnemonic(
            String label);

}
