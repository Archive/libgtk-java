/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class SignalRunType extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _FIRST = 1 << 0;

    static final public org.gnu.gtk.SignalRunType FIRST = new org.gnu.gtk.SignalRunType(
            _FIRST);

    static final private int _LAST = 1 << 1;

    static final public org.gnu.gtk.SignalRunType LAST = new org.gnu.gtk.SignalRunType(
            _LAST);

    static final private int _BOTH = 1 << 2;

    static final public org.gnu.gtk.SignalRunType BOTH = new org.gnu.gtk.SignalRunType(
            _BOTH);

    static final private int _NO_RECURSE = 1 << 3;

    static final public org.gnu.gtk.SignalRunType NO_RECURSE = new org.gnu.gtk.SignalRunType(
            _NO_RECURSE);

    static final private int _ACTION = 1 << 4;

    static final public org.gnu.gtk.SignalRunType ACTION = new org.gnu.gtk.SignalRunType(
            _ACTION);

    static final private int _NO_HOOKS = 1 << 5;

    static final public org.gnu.gtk.SignalRunType NO_HOOKS = new org.gnu.gtk.SignalRunType(
            _NO_HOOKS);

    static final private org.gnu.gtk.SignalRunType[] theInterned = new org.gnu.gtk.SignalRunType[] {
            new org.gnu.gtk.SignalRunType(0), FIRST, LAST,
            new org.gnu.gtk.SignalRunType(3), BOTH,
            new org.gnu.gtk.SignalRunType(5), new org.gnu.gtk.SignalRunType(6),
            new org.gnu.gtk.SignalRunType(7), NO_RECURSE,
            new org.gnu.gtk.SignalRunType(9),
            new org.gnu.gtk.SignalRunType(10),
            new org.gnu.gtk.SignalRunType(11),
            new org.gnu.gtk.SignalRunType(12),
            new org.gnu.gtk.SignalRunType(13),
            new org.gnu.gtk.SignalRunType(14),
            new org.gnu.gtk.SignalRunType(15), ACTION,
            new org.gnu.gtk.SignalRunType(17),
            new org.gnu.gtk.SignalRunType(18),
            new org.gnu.gtk.SignalRunType(19),
            new org.gnu.gtk.SignalRunType(20),
            new org.gnu.gtk.SignalRunType(21),
            new org.gnu.gtk.SignalRunType(22),
            new org.gnu.gtk.SignalRunType(23),
            new org.gnu.gtk.SignalRunType(24),
            new org.gnu.gtk.SignalRunType(25),
            new org.gnu.gtk.SignalRunType(26),
            new org.gnu.gtk.SignalRunType(27),
            new org.gnu.gtk.SignalRunType(28),
            new org.gnu.gtk.SignalRunType(29),
            new org.gnu.gtk.SignalRunType(30),
            new org.gnu.gtk.SignalRunType(31), NO_HOOKS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SignalRunType theSacrificialOne = new org.gnu.gtk.SignalRunType(
            0);

    static public org.gnu.gtk.SignalRunType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SignalRunType already = (org.gnu.gtk.SignalRunType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SignalRunType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SignalRunType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SignalRunType or(org.gnu.gtk.SignalRunType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SignalRunType and(org.gnu.gtk.SignalRunType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SignalRunType xor(org.gnu.gtk.SignalRunType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SignalRunType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
