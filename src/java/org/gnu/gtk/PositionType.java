/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * A property used to determine where items appear in widgets
 * 
 * @see Scale#setValuePosition(PositionType)
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.PositionType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class PositionType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LEFT = 0;

    static final public org.gnu.gtk.PositionType LEFT = new org.gnu.gtk.PositionType(
            _LEFT);

    static final private int _RIGHT = 1;

    static final public org.gnu.gtk.PositionType RIGHT = new org.gnu.gtk.PositionType(
            _RIGHT);

    static final private int _TOP = 2;

    static final public org.gnu.gtk.PositionType TOP = new org.gnu.gtk.PositionType(
            _TOP);

    static final private int _BOTTOM = 3;

    static final public org.gnu.gtk.PositionType BOTTOM = new org.gnu.gtk.PositionType(
            _BOTTOM);

    static final private org.gnu.gtk.PositionType[] theInterned = new org.gnu.gtk.PositionType[] {
            LEFT, RIGHT, TOP, BOTTOM }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.PositionType theSacrificialOne = new org.gnu.gtk.PositionType(
            0);

    static public org.gnu.gtk.PositionType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.PositionType already = (org.gnu.gtk.PositionType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.PositionType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PositionType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.PositionType or(org.gnu.gtk.PositionType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.PositionType and(org.gnu.gtk.PositionType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.PositionType xor(org.gnu.gtk.PositionType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.PositionType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
