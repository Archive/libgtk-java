/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Flags for searching text. Used by the text widgets, described in {@link
 * TextView}
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextSearchFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextSearchFlags extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _VISIBLE_ONLY = 0;

    static final public org.gnu.gtk.TextSearchFlags VISIBLE_ONLY = new org.gnu.gtk.TextSearchFlags(
            _VISIBLE_ONLY);

    static final private int _TEXT_ONLY = 1;

    static final public org.gnu.gtk.TextSearchFlags TEXT_ONLY = new org.gnu.gtk.TextSearchFlags(
            _TEXT_ONLY);

    static final private org.gnu.gtk.TextSearchFlags[] theInterned = new org.gnu.gtk.TextSearchFlags[] {
            VISIBLE_ONLY, TEXT_ONLY }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TextSearchFlags theSacrificialOne = new org.gnu.gtk.TextSearchFlags(
            0);

    static public org.gnu.gtk.TextSearchFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TextSearchFlags already = (org.gnu.gtk.TextSearchFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TextSearchFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TextSearchFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TextSearchFlags or(org.gnu.gtk.TextSearchFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TextSearchFlags and(org.gnu.gtk.TextSearchFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TextSearchFlags xor(org.gnu.gtk.TextSearchFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TextSearchFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
