/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * An AccelGroup represents a group of keyboard accelerators, typically attached
 * to a toplevel Window. Usually you will not need to create an AccelGroup
 * directoy; instead, when using ItemFactory, GTK+ automatically sets up the
 * accelerators for your menus in the item factory's AccelGroup.
 * <p>
 * Note that <i>accelerators</i> are different from <i>mnemonics</i>.
 * Accelerators are shortcuts for activating a menu item; they appear alongside
 * the menu item they're a shortcut for. For example, "Ctrl+Q" might appear
 * alongside the "Quit" menu item. Mnemonics are shortcuts for GUI elements such
 * as text entries or buttons; they appear as underlined characters.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.AccelGroup</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class AccelGroup extends GObject {
    /**
     * Create a new AccelGroup.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelGroup() {
        super(gtk_accel_group_new());
    }

    /**
     * Create a new AccelGroup from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelGroup(Handle handle) {
        super(handle);
    }

    /**
     * Create a new AccelGroup from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AccelGroup getAccelGroup(Handle handle) {
        if (handle == null)
            return null;

        AccelGroup accelGroup = (AccelGroup) getGObjectFromHandle(handle);
        if (accelGroup == null)
            accelGroup = new AccelGroup(handle);

        return accelGroup;
    }

    /**
     * Locks the accelerator group. Locking an acclerator group prevents the
     * accelerators contained within it to be changed during runtime. If this
     * method is called more than once, the AccelGroup remains locked until
     * unlock is called the equivalent number of times.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void lock() {
        gtk_accel_group_lock(getHandle());
    }

    /**
     * Undoes the last call to lock.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unlock() {
        gtk_accel_group_unlock(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_accel_group_get_type());
    }

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected int gtk_accel_group_get_type();

    native static final protected Handle gtk_accel_group_new();

    native static final protected void gtk_accel_group_lock(Handle accelGroup);

    native static final protected void gtk_accel_group_unlock(Handle accelGroup);

    native static final protected boolean gtk_accel_group_disconnect_key(
            Handle accelGroup, int accelKey, int accelMods);

    native static final protected boolean gtk_accel_groups_activate(
            Handle object, int accelKey, int accelMods);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
