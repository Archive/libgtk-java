/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class PathPriorityType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LOWEST = 0;

    static final public org.gnu.gtk.PathPriorityType LOWEST = new org.gnu.gtk.PathPriorityType(
            _LOWEST);

    static final private int _GTK = 4;

    static final public org.gnu.gtk.PathPriorityType GTK = new org.gnu.gtk.PathPriorityType(
            _GTK);

    static final private int _APPLICATION = 8;

    static final public org.gnu.gtk.PathPriorityType APPLICATION = new org.gnu.gtk.PathPriorityType(
            _APPLICATION);

    static final private int _THEME = 10;

    static final public org.gnu.gtk.PathPriorityType THEME = new org.gnu.gtk.PathPriorityType(
            _THEME);

    static final private int _RC = 12;

    static final public org.gnu.gtk.PathPriorityType RC = new org.gnu.gtk.PathPriorityType(
            _RC);

    static final private int _HIGHEST = 15;

    static final public org.gnu.gtk.PathPriorityType HIGHEST = new org.gnu.gtk.PathPriorityType(
            _HIGHEST);

    static final private org.gnu.gtk.PathPriorityType[] theInterned = new org.gnu.gtk.PathPriorityType[] {
            LOWEST, new org.gnu.gtk.PathPriorityType(1),
            new org.gnu.gtk.PathPriorityType(2),
            new org.gnu.gtk.PathPriorityType(3), GTK,
            new org.gnu.gtk.PathPriorityType(5),
            new org.gnu.gtk.PathPriorityType(6),
            new org.gnu.gtk.PathPriorityType(7), APPLICATION,
            new org.gnu.gtk.PathPriorityType(9), THEME,
            new org.gnu.gtk.PathPriorityType(11), RC,
            new org.gnu.gtk.PathPriorityType(13),
            new org.gnu.gtk.PathPriorityType(14), HIGHEST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.PathPriorityType theSacrificialOne = new org.gnu.gtk.PathPriorityType(
            0);

    static public org.gnu.gtk.PathPriorityType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.PathPriorityType already = (org.gnu.gtk.PathPriorityType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.PathPriorityType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PathPriorityType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.PathPriorityType or(org.gnu.gtk.PathPriorityType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.PathPriorityType and(org.gnu.gtk.PathPriorityType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.PathPriorityType xor(org.gnu.gtk.PathPriorityType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.PathPriorityType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
