/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.ToolBarEvent;
import org.gnu.gtk.event.ToolBarListener;
import org.gnu.glib.Handle;

/**
 * The ToolBar is used to construct a toolbar. It is the container that controls
 * the position of a set of icons that are mouse sensitive.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolBar extends Container {

    /**
     * Create a new ToolBar
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolBar() {
        super(gtk_toolbar_new());
    }

    public ToolBar(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToolBar getToolBar(Handle handle) {
        if (handle == null)
            return null;

        ToolBar obj = (ToolBar) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ToolBar(handle);

        return obj;
    }

    /**
     * Add a widget to the end of the toolbar.
     * 
     * @param widget
     *            The widget to add to the end of the ToolBar.
     * @param tooltip
     *            The text for the tooltip for this item.
     * @param helpText
     *            Context sensitive help about this item.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void appendWidget(Widget widget, String tooltip, String helpText) {
        ToolBar.gtk_toolbar_append_widget(getHandle(), widget.getHandle(),
                tooltip, helpText);
    }

    /**
     * Add a widget to the beginning of the toolbar.
     * 
     * @param widget
     *            The widget to add to the beginning of the ToolBar.
     * @param tooltip
     *            The text for the tooltip for this item.
     * @param helpText
     *            Context sensitive help about this item.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void prependWidget(Widget widget, String tooltip, String helpText) {
        ToolBar.gtk_toolbar_prepend_widget(getHandle(), widget.getHandle(),
                tooltip, helpText);
    }

    /**
     * Insert a Widget in the ToolBar at the given position
     * 
     * @param widget
     *            The widget to add to the ToolBar.
     * @param tooltip
     *            The text for the tooltip for this item.
     * @param helpText
     *            Context sensitive help about this item.
     * @param index
     *            The location in the ToolBar to insert this Widget.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insertWidget(Widget widget, String tooltip, String helpText,
            int index) {
        ToolBar.gtk_toolbar_insert_widget(getHandle(), widget.getHandle(),
                tooltip, helpText, index);
    }

    /**
     * Add a space to the end of the ToolBar.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void appendSpace() {
        ToolBar.gtk_toolbar_append_space(getHandle());
    }

    /**
     * Add a space to the beginning of the ToolBar.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void prependSpace() {
        ToolBar.gtk_toolbar_prepend_space(getHandle());
    }

    /**
     * Insert a space in the ToolBar at the specified location.
     * 
     * @param position
     *            The location to insert the space.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insertSpace(int position) {
        ToolBar.gtk_toolbar_insert_space(getHandle(), position);
    }

    /**
     * Remove a space from the ToolBar.
     * 
     * @param position
     *            The position of the space to remove.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeSpace(int position) {
        ToolBar.gtk_toolbar_remove_space(getHandle(), position);
    }

    /**
     * Set whether the ToolBar should appear horizontally or vertically.
     * 
     * @param orientation
     *            Specifies the direction of the ToolBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setOrientation(Orientation orientation) {
        ToolBar
                .gtk_toolbar_set_orientation(getHandle(), orientation
                        .getValue());
    }

    /**
     * Alter the view of the ToolBar to display either icons only, text only, or
     * both.
     * 
     * @param style
     *            Determines how to display the items.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStyle(ToolBarStyle style) {
        ToolBar.gtk_toolbar_set_style(getHandle(), style.getValue());
    }

    public boolean isToolTips() {
        return gtk_toolbar_get_tooltips(getHandle());
    }

    /**
     * Sets if the tooltips of the ToolBar should be active or not.
     * 
     * @param enable
     *            Should the tooltips be enabled.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setToolTips(boolean enable) {
        ToolBar.gtk_toolbar_set_tooltips(getHandle(), enable);
    }

    /**
     * Sets the size of the stock icons in the ToolBar.
     * 
     * @param iconSize
     *            The size of the icons.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconSize(IconSize iconSize) {
        ToolBar.gtk_toolbar_set_icon_size(getHandle(), iconSize.getValue());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_toolbar_get_type());
    }

    /**
     * Insert a ToolItem into the ToolBar at a specified position. If position
     * is 0 then the item is prepended to the start of the ToolBar. If position
     * is negative then it is appended to the end of the ToolBar.
     * 
     * @param item
     * @param position
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insert(ToolItem item, int position) {
        gtk_toolbar_insert(getHandle(), item.getHandle(), position);
    }

    /**
     * Return the position of an item on the ToolBar starting from 0.
     * 
     * @param item
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getItemIndex(ToolItem item) {
        return gtk_toolbar_get_item_index(getHandle(), item.getHandle());
    }

    /**
     * Return the number of items on the ToolBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getNumItems() {
        return gtk_toolbar_get_n_items(getHandle());
    }

    /**
     * Return the ToolItem on the ToolBar from the position specified.
     * 
     * @param position
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolItem getItem(int position) {
        Handle hndl = gtk_toolbar_get_nth_item(getHandle(), position);
        return ToolItem.getToolItem(hndl);
    }

    /**
     * Returns whether the ToolBar has an overflow menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getShowArrow() {
        return gtk_toolbar_get_show_arrow(getHandle());
    }

    /**
     * Sets whether to show an overflow menu when ToolBar doesn't have room for
     * all of its' items.
     * 
     * @param showArrow
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShowArrow(boolean showArrow) {
        gtk_toolbar_set_show_arrow(getHandle(), showArrow);
    }

    /**
     * Returns the ReliefStyle of Buttons on the ToolBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ReliefStyle getReliefStyle() {
        return ReliefStyle.intern(gtk_toolbar_get_relief_style(getHandle()));
    }

    /**
     * Returns the position corresponding to the indicated point on ToolBar.
     * This is useful when dragging items to the ToolBar: this functions returns
     * the position a new item would be inserted.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getDropIndex(int x, int y) {
        return gtk_toolbar_get_drop_index(getHandle(), x, y);
    }

    /**
     * Highlights ToolBar to give an idea of what it would look like if a
     * ToolItem was added to the ToolBar and the provided index.
     * 
     * @param item
     * @param index
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDropHighlightItem(ToolItem item, int index) {
        gtk_toolbar_set_drop_highlight_item(getHandle(), item.getHandle(),
                index);
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/

    /**
     * Listeners for handling events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector tbListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see ToolBarListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ToolBarListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(tbListeners, listener);
        if (i == -1) {
            if (null == tbListeners) {
                evtMap.initialize(this, ToolBarEvent.Type.ORIENTATION_CHANGED);
                evtMap.initialize(this, ToolBarEvent.Type.POPUP_CONTEXT_MENU);
                evtMap.initialize(this, ToolBarEvent.Type.STYLE_CHANGED);
                tbListeners = new Vector();
            }
            tbListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ToolBarListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ToolBarListener listener) {
        int i = findListener(tbListeners, listener);
        if (i > -1) {
            tbListeners.remove(i);
        }
        if (0 == tbListeners.size()) {
            evtMap.uninitialize(this, ToolBarEvent.Type.ORIENTATION_CHANGED);
            evtMap.uninitialize(this, ToolBarEvent.Type.POPUP_CONTEXT_MENU);
            evtMap.uninitialize(this, ToolBarEvent.Type.STYLE_CHANGED);
            tbListeners = null;
        }
    }

    protected void fireToolBarEvent(ToolBarEvent event) {
        if (null == tbListeners) {
            return;
        }
        int size = tbListeners.size();
        int i = 0;
        while (i < size) {
            ToolBarListener tbl = (ToolBarListener) tbListeners.elementAt(i);
            tbl.toolBarEvent(event);
            i++;
        }
    }

    private void handleOrientationChange(int orientation) {
        ToolBarEvent evt = new ToolBarEvent(this,
                ToolBarEvent.Type.ORIENTATION_CHANGED);
        evt.setOrientation(Orientation.intern(orientation));
        fireToolBarEvent(evt);
    }

    private void handleStyleChange(int style) {
        ToolBarEvent evt = new ToolBarEvent(this,
                ToolBarEvent.Type.STYLE_CHANGED);
        evt.setStyle(ToolBarStyle.intern(style));
        fireToolBarEvent(evt);
    }

    private boolean handlePopupContextMenu(int x, int y, int button) {
        ToolBarEvent evt = new ToolBarEvent(this,
                ToolBarEvent.Type.POPUP_CONTEXT_MENU);
        evt.setX(x);
        evt.setY(y);
        evt.setButtonNumber(button);
        fireToolBarEvent(evt);
        return true;
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("orientation_changed", "handleOrientationChange",
                ToolBarEvent.Type.ORIENTATION_CHANGED, ToolBarListener.class);
        anEvtMap.addEvent("style_changed", "handleStyleChange",
                ToolBarEvent.Type.STYLE_CHANGED, ToolBarListener.class);
        anEvtMap.addEvent("popup_context_menu", "handlePopupContextMenu",
                ToolBarEvent.Type.POPUP_CONTEXT_MENU, ToolBarListener.class);
    }

    native static final protected int gtk_toolbar_get_type();

    native static final protected Handle gtk_toolbar_new();

    native static final protected void gtk_toolbar_set_orientation(
            Handle toolbar, int orientation);

    native static final protected void gtk_toolbar_set_style(Handle toolbar,
            int style);

    native static final protected void gtk_toolbar_set_tooltips(Handle toolbar,
            boolean enable);

    native static final protected void gtk_toolbar_unset_style(Handle toolbar);

    native static final protected int gtk_toolbar_get_orientation(Handle toolbar);

    native static final protected int gtk_toolbar_get_style(Handle toolbar);

    native static final protected int gtk_toolbar_get_icon_size(Handle toolbar);

    native static final protected boolean gtk_toolbar_get_tooltips(
            Handle toolbar);

    // Deprecated Methods
    native static final protected void gtk_toolbar_append_space(Handle toolbar);

    native static final protected void gtk_toolbar_prepend_space(Handle toolbar);

    native static final protected void gtk_toolbar_insert_space(Handle toolbar,
            int position);

    native static final protected void gtk_toolbar_remove_space(Handle toolbar,
            int position);

    native static final protected void gtk_toolbar_append_widget(
            Handle Toolbar, Handle widget, String tooltipText,
            String tooltipPrivateText);

    native static final protected void gtk_toolbar_prepend_widget(
            Handle Toolbar, Handle widget, String tooltipText,
            String tooltipPrivateText);

    native static final protected void gtk_toolbar_insert_widget(
            Handle toolbar, Handle widget, String tooltipText,
            String tooltipPrivateText, int position);

    native static final protected void gtk_toolbar_set_icon_size(
            Handle toolbar, int iconSize);

    native static final protected void gtk_toolbar_unset_icon_size(
            Handle toolbar);

    // New Methods
    native static final protected void gtk_toolbar_insert(Handle toolbar,
            Handle item, int pos);

    native static final protected int gtk_toolbar_get_item_index(
            Handle toolbar, Handle item);

    native static final protected int gtk_toolbar_get_n_items(Handle toolbar);

    native static final protected Handle gtk_toolbar_get_nth_item(
            Handle toolbar, int pos);

    native static final protected boolean gtk_toolbar_get_show_arrow(
            Handle toolbar);

    native static final protected void gtk_toolbar_set_show_arrow(
            Handle toolbar, boolean show);

    native static final protected int gtk_toolbar_get_relief_style(
            Handle toolbar);

    native static final protected int gtk_toolbar_get_drop_index(
            Handle toolbar, int x, int y);

    native static final protected void gtk_toolbar_set_drop_highlight_item(
            Handle toolbar, Handle item, int index);
}
