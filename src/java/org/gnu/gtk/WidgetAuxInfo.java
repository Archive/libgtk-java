/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

public class WidgetAuxInfo extends MemStruct {
    native static final protected int getX(Handle cptr);

    native static final protected int getY(Handle cptr);

    native static final protected int getWidth(Handle cptr);

    native static final protected int getHeight(Handle cptr);

    native static final protected boolean getXSet(Handle cptr);

    native static final protected boolean getYSet(Handle cptr);
}
