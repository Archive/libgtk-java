/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Used to change the appearance of an outline typically provided by a
 * {@link Frame}.
 * 
 * <pre>
 *  GTK_SHADOW_NONE
 *  No outline.
 *  GTK_SHADOW_IN
 *  The outline is bevelled inwards.
 *  GTK_SHADOW_OUT
 *  The outline is bevelled outwards like a button.
 *  GTK_SHADOW_ETCHED_IN
 *  The outline itself is an inward bevel, but the frame does
 *  GTK_SHADOW_ETCHED_OUT
 * </pre>
 * 
 * TODO: update doc
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ShadowType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ShadowType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gtk.ShadowType NONE = new org.gnu.gtk.ShadowType(
            _NONE);

    static final private int _IN = 1;

    static final public org.gnu.gtk.ShadowType IN = new org.gnu.gtk.ShadowType(
            _IN);

    static final private int _OUT = 2;

    static final public org.gnu.gtk.ShadowType OUT = new org.gnu.gtk.ShadowType(
            _OUT);

    static final private int _ETCHED_IN = 3;

    static final public org.gnu.gtk.ShadowType ETCHED_IN = new org.gnu.gtk.ShadowType(
            _ETCHED_IN);

    static final private int _ETCHED_OUT = 4;

    static final public org.gnu.gtk.ShadowType ETCHED_OUT = new org.gnu.gtk.ShadowType(
            _ETCHED_OUT);

    static final private org.gnu.gtk.ShadowType[] theInterned = new org.gnu.gtk.ShadowType[] {
            NONE, IN, OUT, ETCHED_IN, ETCHED_OUT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ShadowType theSacrificialOne = new org.gnu.gtk.ShadowType(
            0);

    static public org.gnu.gtk.ShadowType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ShadowType already = (org.gnu.gtk.ShadowType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ShadowType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ShadowType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ShadowType or(org.gnu.gtk.ShadowType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ShadowType and(org.gnu.gtk.ShadowType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ShadowType xor(org.gnu.gtk.ShadowType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ShadowType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
