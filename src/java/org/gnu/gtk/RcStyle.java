/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;
import org.gnu.pango.FontDescription;

/**
 * A class that provides runtime routines for handling resource files.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RcStyle</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RcStyle extends GObject {
    /**
     * Construct a new RcStyle.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RcStyle() {
        super(gtk_rc_style_new());
    }

    /**
     * Construct a new RcStyle from a given handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RcStyle(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new RcStyle from a given handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static RcStyle getRcStyle(Handle handle) {
        if (handle == null)
            return null;

        RcStyle obj = (RcStyle) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new RcStyle(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_rc_style_get_type());
    }

    public FontDescription getFontDescription() {
        return new FontDescription(getFontDesc(getHandle()));
    }

    public String getName() {
        return getName(getHandle());
    }

    native static final protected String getName(Handle cptr);

    native static final protected Handle getFontDesc(Handle cptr);

    native static final protected int getXthickness(Handle cptr);

    native static final protected int getYthickness(Handle cptr);

    native static final protected int gtk_rc_style_get_type();

    native static final protected Handle gtk_rc_style_new();

    native static final protected Handle gtk_rc_style_copy(Handle orig);

    native static final protected void gtk_rc_style_ref(Handle rc_style);

    native static final protected void gtk_rc_style_unref(Handle rc_style);

}
