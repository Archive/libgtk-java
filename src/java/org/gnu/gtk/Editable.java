/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

/**
 * The Editable interface is used by widgets that edit text, such as
 * {@link Entry} widget. It cannot be instantiated by itself. This interface
 * contains functions for generically manipulating an editable widget, a large
 * number of action events used for key bindings, and several events that an
 * application can connect to to modify the behavior of a widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Editable</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface Editable {

    /**
     * Select a region of text. The characters that are selected are those
     * characters at positions from <i>start</i> up to, but not including
     * <i>end</i>. If <i>end</i> is negative, then the characters selected
     * will be those characters from <i>start</i> to the end of the text
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void selectRegion(int start, int end);

    // REDTAG: getSelectionBounds

    /**
     * Insert text at a given point
     * 
     * @param text
     *            The text to insert.
     * @param offset
     *            The offset into the buffer to begin the insert.
     * @return The current offset after the text has been inserted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int insertText(String text, int offset);

    /**
     * Delete a sequence of characters. The characters that are deleted are
     * those characters from position <i>start</i> up to, but not including
     * <i>end</i>. If <i>end</i> is a negative number then the characters to
     * be deleted are from <i>start</i> until the end of the text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deleteText(int start, int end);

    /**
     * Retrieve a sequence of characters. The characters that are retrieved are
     * those characters from position <i>start</i> up to, but not including
     * <i>end</i>. If <i>end</i> is negative, then the characters retrieved
     * will be those characters from <i>start</i> until the end of the text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getCharacters(int start, int end);

    /**
     * Causes the characters in the current selection to be copied to the
     * clipboard and then deleted from the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void cutClipboard();

    /**
     * Causes the characters in the current selection to be copied to the
     * clipboard.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void copyClipboard();

    /**
     * Causes the contents of the clipboard to be pasted into this widget at the
     * current cursor position.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void pasteClipboard();

    /**
     * Deletes the contents of the current selection.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deleteSelection();

    /**
     * Sets the cursor position
     * 
     * @param position
     *            The position of the cursor. The cursor is displayed before the
     *            character with the given (base 0) index in the widget. The
     *            value must be less than or equal to the number of characters
     *            in the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCursorPosition(int position);

    /**
     * Retrieves the current cursor position.
     * 
     * @return A 0 based integer that identifies the offset position of the
     *         cursor from the beginning of the text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getCursorPosition();

    /**
     * Determines if the user can edit the text contained in the widget.
     * 
     * @param isEditable
     *            true if the user can edit the text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEditable(boolean isEditable);

    /**
     * Retrieves whether the text contained in the widget is editable.
     * 
     * @return true if the text is editable.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEditable();
}
