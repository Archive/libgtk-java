/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A Viewport is a container that does not resize to fit in the space allocated
 * to it. Instead, it adds scrollbars as necessary so you can view any part of
 * the window.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Viewport</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Viewport extends Bin {
    /**
     * Construct a new Viewport with the given Adjustments.
     * 
     * @param hadj
     *            The horizontal Adjustment to use for the Viewport.
     * @param vadj
     *            The vertical Adjustment to use for the Viewport.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Viewport(Adjustment hadj, Adjustment vadj) {
        super(init(hadj, vadj));
    }

    private static Handle init(Adjustment hadj, Adjustment vadj) {
        Handle horizontal = null;
        Handle vertical = null;
        if (null != hadj) {
            horizontal = hadj.getHandle();
        }
        if (null != vadj) {
            vertical = vadj.getHandle();
        }
        return gtk_viewport_new(horizontal, vertical);
    }

    /**
     * Construct a Viewport using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Viewport(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Viewport getViewport(Handle handle) {
        if (handle == null)
            return null;

        Viewport obj = (Viewport) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Viewport(handle);

        return obj;
    }

    /**
     * Returns the horizontal Viewport's Adjustment. This can be used to connect
     * the horizontal scrollbar to the child Widget's horizontal scroll
     * functionality.
     * 
     * @return The horizontal Viewport's Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getHAdjustment() {
        Handle hndl = gtk_viewport_get_hadjustment(getHandle());
        return Adjustment.getAdjustment(hndl);
    }

    /**
     * Sets the horizontal Viewport's Adjustment.
     * 
     * @param hadj
     *            The horizontal Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHAdjustment(Adjustment hadj) {
        Viewport.gtk_viewport_set_hadjustment(getHandle(), hadj.getHandle());
    }

    /**
     * Returns the vertical Viewport's Adjustment. This can be used to connect
     * the vertical scrollbar to the child Widget's vertical scroll
     * functionality.
     * 
     * @return The vertical Viewport's Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getVAdjustment() {
        Handle hndl = gtk_viewport_get_vadjustment(getHandle());
        return Adjustment.getAdjustment(hndl);
    }

    /**
     * Sets the vertical Viewport's Adjustment.
     * 
     * @param vadj
     *            The vertical Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVAdjustment(Adjustment vadj) {
        Viewport.gtk_viewport_set_vadjustment(getHandle(), vadj.getHandle());
    }

    /**
     * Sets the shadow type of the viewport.
     * @param shadowType 
     */
    public void setShadowType(ShadowType shadowType){
    	gtk_viewport_set_shadow_type(getHandle(), shadowType.getValue() );
    }
    
    /**
     * Returns the ShadowType of the Viewport.
     * @return ShadowType of the Viewport.
     */
    public ShadowType getShadowType(){
        return ShadowType.intern(gtk_viewport_get_shadow_type(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_viewport_get_type());
    }

    native static final protected int gtk_viewport_get_type();

    native static final protected Handle gtk_viewport_new(Handle hadjustment,
            Handle vadjustment);

    native static final protected Handle gtk_viewport_get_hadjustment(
            Handle viewport);

    native static final protected void gtk_viewport_set_hadjustment(
            Handle viewport, Handle adjustment);

    native static final protected Handle gtk_viewport_get_vadjustment(
            Handle viewport);

    native static final protected void gtk_viewport_set_vadjustment(
            Handle viewport, Handle adjustment);

    native static final protected void gtk_viewport_set_shadow_type(
            Handle viewport, int type);

    native static final protected int gtk_viewport_get_shadow_type(
            Handle viewport);
}
