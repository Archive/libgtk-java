/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gtk.event.FileChooserListener;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileChooser</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface FileChooser {

    /*
     * Configuration
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setAction(FileChooserAction action);

    FileChooserAction getAction();

    void setLocalOnly(boolean localOnly);

    boolean getLocalOnly();

    void setSelectMultiple(boolean selectMultiple);

    boolean getSelectMultiple();

    /*
     * Filename manipulation
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setCurrentName(String name);

    String getFilename();

    boolean setFilename(String filename);

    boolean selectFilename(String filename);

    void unselectFilename(String filename);

    void selectAll();

    void unselectAll();

    String[] getFilenames();

    boolean setCurrentFolder(String folder);

    String getCurrentFolder();

    /*
     * URI manipulation
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    String getURI();

    boolean setURI(String uri);

    boolean selectURI(String uri);

    void unselectURI(String uri);

    String[] getURIs();

    boolean setCurrentFolderURI(String folder);

    String getCurrentFolderURI();

    /*
     * Preview widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setPreviewWidget(Widget previewWidget);

    Widget getPreviewWidget();

    void setPreviewWidgetActive(boolean active);

    boolean getPreviewWidgetActive();

    void setUsePreviewLabel(boolean useLabel);

    boolean getUsePreviewLabel();

    String getPreviewFilename();

    String getPreviewURI();

    /*
     * Extra widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setExtraWidget(Widget extraWidget);

    Widget getExtraWidget();

    /*
     * List of user selectable filters
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void addFilter(FileFilter filter);

    void removeFilter(FileFilter filter);

    FileFilter[] listFilters();

    /*
     * Current filter
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setFilter(FileFilter filter);

    FileFilter getFilter();

    /*
     * Per-application shortcut folders
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void addShortcutFolder(String folder) throws FileChooserException;

    void removeShortcutFolder(String folder) throws FileChooserException;

    String[] listShortcutFolders();

    void addShortcutURI(String uri) throws FileChooserException;

    void removeShortcutURI(String uri) throws FileChooserException;

    String[] listShortcutURIs();

    /*
     * Hidden.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setShowHidden(boolean hidden);

    boolean getShowHidden();

    /*
     * Listeners
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    void addListener(FileChooserListener listener);

    void removeListener(FileChooserListener listener);
}
