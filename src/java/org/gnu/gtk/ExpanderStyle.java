/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class ExpanderStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _COLLAPSED = 0;

    static final public org.gnu.gtk.ExpanderStyle COLLAPSED = new org.gnu.gtk.ExpanderStyle(
            _COLLAPSED);

    static final private int _SEMI_COLLAPSED = 1;

    static final public org.gnu.gtk.ExpanderStyle SEMI_COLLAPSED = new org.gnu.gtk.ExpanderStyle(
            _SEMI_COLLAPSED);

    static final private int _SEMI_EXPANDED = 2;

    static final public org.gnu.gtk.ExpanderStyle SEMI_EXPANDED = new org.gnu.gtk.ExpanderStyle(
            _SEMI_EXPANDED);

    static final private int _EXPANDED = 3;

    static final public org.gnu.gtk.ExpanderStyle EXPANDED = new org.gnu.gtk.ExpanderStyle(
            _EXPANDED);

    static final private org.gnu.gtk.ExpanderStyle[] theInterned = new org.gnu.gtk.ExpanderStyle[] {
            COLLAPSED, SEMI_COLLAPSED, SEMI_EXPANDED, EXPANDED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ExpanderStyle theSacrificialOne = new org.gnu.gtk.ExpanderStyle(
            0);

    static public org.gnu.gtk.ExpanderStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ExpanderStyle already = (org.gnu.gtk.ExpanderStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ExpanderStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ExpanderStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ExpanderStyle or(org.gnu.gtk.ExpanderStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ExpanderStyle and(org.gnu.gtk.ExpanderStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ExpanderStyle xor(org.gnu.gtk.ExpanderStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ExpanderStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
