/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class ScrollType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gtk.ScrollType NONE = new org.gnu.gtk.ScrollType(
            _NONE);

    static final private int _JUMP = 1;

    static final public org.gnu.gtk.ScrollType JUMP = new org.gnu.gtk.ScrollType(
            _JUMP);

    static final private int _STEP_BACKWARD = 2;

    static final public org.gnu.gtk.ScrollType STEP_BACKWARD = new org.gnu.gtk.ScrollType(
            _STEP_BACKWARD);

    static final private int _STEP_FORWARD = 3;

    static final public org.gnu.gtk.ScrollType STEP_FORWARD = new org.gnu.gtk.ScrollType(
            _STEP_FORWARD);

    static final private int _PAGE_BACKWARD = 4;

    static final public org.gnu.gtk.ScrollType PAGE_BACKWARD = new org.gnu.gtk.ScrollType(
            _PAGE_BACKWARD);

    static final private int _PAGE_FORWARD = 5;

    static final public org.gnu.gtk.ScrollType PAGE_FORWARD = new org.gnu.gtk.ScrollType(
            _PAGE_FORWARD);

    static final private int _STEP_UP = 6;

    static final public org.gnu.gtk.ScrollType STEP_UP = new org.gnu.gtk.ScrollType(
            _STEP_UP);

    static final private int _STEP_DOWN = 7;

    static final public org.gnu.gtk.ScrollType STEP_DOWN = new org.gnu.gtk.ScrollType(
            _STEP_DOWN);

    static final private int _PAGE_UP = 8;

    static final public org.gnu.gtk.ScrollType PAGE_UP = new org.gnu.gtk.ScrollType(
            _PAGE_UP);

    static final private int _PAGE_DOWN = 9;

    static final public org.gnu.gtk.ScrollType PAGE_DOWN = new org.gnu.gtk.ScrollType(
            _PAGE_DOWN);

    static final private int _STEP_LEFT = 10;

    static final public org.gnu.gtk.ScrollType STEP_LEFT = new org.gnu.gtk.ScrollType(
            _STEP_LEFT);

    static final private int _STEP_RIGHT = 11;

    static final public org.gnu.gtk.ScrollType STEP_RIGHT = new org.gnu.gtk.ScrollType(
            _STEP_RIGHT);

    static final private int _PAGE_LEFT = 12;

    static final public org.gnu.gtk.ScrollType PAGE_LEFT = new org.gnu.gtk.ScrollType(
            _PAGE_LEFT);

    static final private int _PAGE_RIGHT = 13;

    static final public org.gnu.gtk.ScrollType PAGE_RIGHT = new org.gnu.gtk.ScrollType(
            _PAGE_RIGHT);

    static final private int _START = 14;

    static final public org.gnu.gtk.ScrollType START = new org.gnu.gtk.ScrollType(
            _START);

    static final private int _END = 15;

    static final public org.gnu.gtk.ScrollType END = new org.gnu.gtk.ScrollType(
            _END);

    static final private org.gnu.gtk.ScrollType[] theInterned = new org.gnu.gtk.ScrollType[] {
            NONE, JUMP, STEP_BACKWARD, STEP_FORWARD, PAGE_BACKWARD,
            PAGE_FORWARD, STEP_UP, STEP_DOWN, PAGE_UP, PAGE_DOWN, STEP_LEFT,
            STEP_RIGHT, PAGE_LEFT, PAGE_RIGHT, START, END }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ScrollType theSacrificialOne = new org.gnu.gtk.ScrollType(
            0);

    static public org.gnu.gtk.ScrollType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ScrollType already = (org.gnu.gtk.ScrollType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ScrollType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ScrollType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ScrollType or(org.gnu.gtk.ScrollType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ScrollType and(org.gnu.gtk.ScrollType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ScrollType xor(org.gnu.gtk.ScrollType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ScrollType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
