/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * An interface for packing cells.
 * <p>
 * CellLayout is an interface to be implemented by all objects which want to
 * provide a TreeViewColumn-like API for packing cells, setting attributes and
 * data funcs.
 * 
 * @see ComboBox
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellLayout</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface CellLayout {

    /**
     * Get the native Handle of this CellLayout object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle getHandle();

    /**
     * Packs the cell into the beginning of the CellLayout. If expand is FALSE,
     * then the cell is allocated no more space than it needs. Any unused space
     * is divided evenly between cells for which expand is TRUE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void packStart(CellRenderer renderer, boolean expand);

    /**
     * Adds the cell to the end of the CellLayout. If expand is FALSE, then the
     * cell is allocated no more space than it needs. Any unused space is
     * divided evenly between cells for which expand is TRUE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void packEnd(CellRenderer renderer, boolean expand);

    /**
     * Unsets all the mappings on all renderers on the CellLayout and removes
     * all renderers from the CellLayout.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clear();

    /**
     * Adds an attribute mapping to the list in the CellLayout. The column is
     * the column of the model to get a value from, and the attribute is the
     * parameter on cell to be set from the value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addAttributeMapping(CellRenderer renderer,
            CellRendererAttribute attribute, DataColumn column);

    /**
     * Clears all existing attributes previously set with
     * {@link #addAttributeMapping}.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clearAttributeMappings(CellRenderer renderer);

    /**
     * Re-inserts the renderer at position. Note that the renderer has already
     * to be packed into the CellLayout for this to function properly.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void reorder(CellRenderer renderer, int position);

}
