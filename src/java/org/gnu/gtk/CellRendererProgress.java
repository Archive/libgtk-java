/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRendererProgress</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRendererProgress extends CellRenderer {

    public CellRendererProgress() {
        super(gtk_cell_renderer_progress_new());
    }

    public static class Attribute extends CellRenderer.Attribute {
	    private Attribute(String attr) {
		    super(attr);
	    }
	   
	    /**
	     * (String)
	     * The "text" property determines the label which 
	     * will be drawn over the progress bar. Setting this 
	     * property to NULL causes the default label to be 
	     * displayed. Setting this property to an empty string 
	     * causes no label to be displayed.
	     * Default value: NULL
	     */
	    public static final Attribute TEXT   = new Attribute("text");

	    /**
	     * (int)
	     * The "value" property determines the percentage to 
	     * which the progress bar will be "filled in". Allowed
	     * values: [0,100]
	     * Default value: 0
	     */
	    public static final Attribute VALUE  = new Attribute("value");

    }
    
    native static final protected int gtk_cell_renderer_progress_get_type();

    native static final protected Handle gtk_cell_renderer_progress_new();

}
