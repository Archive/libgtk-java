/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * The type of message to be displayed in a dialog.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MessageType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class MessageType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _INFO = 0;

    /**
     * Informational message.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.MessageType INFO = new org.gnu.gtk.MessageType(
            _INFO);

    static final private int _WARNING = 1;

    /**
     * Nonfatal warning message.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.MessageType WARNING = new org.gnu.gtk.MessageType(
            _WARNING);

    static final private int _QUESTION = 2;

    /**
     * Question requiring a choice.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.MessageType QUESTION = new org.gnu.gtk.MessageType(
            _QUESTION);

    static final private int _ERROR = 3;

    /**
     * Fatal error message.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.MessageType ERROR = new org.gnu.gtk.MessageType(
            _ERROR);

    static final private org.gnu.gtk.MessageType[] theInterned = new org.gnu.gtk.MessageType[] {
            INFO, WARNING, QUESTION, ERROR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.MessageType theSacrificialOne = new org.gnu.gtk.MessageType(
            0);

    static public org.gnu.gtk.MessageType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.MessageType already = (org.gnu.gtk.MessageType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.MessageType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private MessageType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.MessageType or(org.gnu.gtk.MessageType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.MessageType and(org.gnu.gtk.MessageType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.MessageType xor(org.gnu.gtk.MessageType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.MessageType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
