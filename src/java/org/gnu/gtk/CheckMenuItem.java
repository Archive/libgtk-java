/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.CheckMenuItemEvent;
import org.gnu.gtk.event.CheckMenuItemListener;
import org.gnu.glib.Handle;

/**
 * The CheckMenuItem is a menu item that maintains the state of a boolean value
 * in addition to the MenuItem's usual role of activating application code.
 * <p>
 * A check box indicating the state of boolean value is displayed on the left
 * side of the MenuItem. Activating the MenuItem toggles this value.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CheckMenuItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CheckMenuItem extends MenuItem {

    /**
     * Create a new CheckMenuItem
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckMenuItem() {
        super(CheckMenuItem.gtk_check_menu_item_new());
    }

    /**
     * Construct a CheckMenuItem using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckMenuItem(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static CheckMenuItem getCheckMenuItem(Handle handle) {
        if (handle == null)
            return null;

        CheckMenuItem obj = (CheckMenuItem) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new CheckMenuItem(handle);

        return obj;
    }

    /**
     * Create a new CheckMenuItem with the given label.
     * 
     * @param label
     *            The label to display for this CheckMenuItem.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CheckMenuItem(String label, boolean hasMnemonic) {
        super(init(label, hasMnemonic));
    }

    private static Handle init(String label, boolean hasMnemonic) {
        if (hasMnemonic)
            return gtk_check_menu_item_new_with_mnemonic(label);
        else
            return gtk_check_menu_item_new_with_label(label);
    }

    /**
     * Sets the active state of the CheckMenuItem's check box.
     * 
     * @param isActive
     *            true if the check box is checked.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setActive(boolean isActive) {
        CheckMenuItem.gtk_check_menu_item_set_active(getHandle(), isActive);
    }

    /**
     * Returns whether the CheckMenuItem is active.
     * 
     * @return true if the check box is active.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getActive() {
        return CheckMenuItem.gtk_check_menu_item_get_active(getHandle());
    }

    /**
     * This method will turn on a third state that indicates that the widget is
     * in an inconsistent state.
     * 
     * @param isInconsistent
     *            true to display an "inconsistent" third state.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setInconsistent(boolean isInconsistent) {
        gtk_check_menu_item_set_inconsistent(getHandle(), isInconsistent);
    }

    /**
     * Returns whether the CheckMenuItem is in the "inconsistent" state.
     * 
     * @return true if the check box is inconsistent.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getInconsistent() {
        return gtk_check_menu_item_get_inconsistent(getHandle());
    }

    /**
     * Sets whether the CheckMenuItem should be drawn as a radio control
     * 
     * @param drawAsRadio
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDrawAsRadio(boolean drawAsRadio) {
        gtk_check_menu_item_set_draw_as_radio(getHandle(), drawAsRadio);
    }

    /**
     * Gets whether the CheckMenuItem will be drawn as a radio control.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDrawAsRadio() {
        return gtk_check_menu_item_get_draw_as_radio(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_check_menu_item_get_type());
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/

    /**
     * Listeners for handling dialog events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector listeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see CheckMenuItemListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(CheckMenuItemListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(listeners, listener);
        if (i == -1) {
            if (null == listeners) {
                evtMap.initialize(this, CheckMenuItemEvent.Type.TOGGLED);
                listeners = new Vector();
            }
            listeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(CheckMenuItemListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(CheckMenuItemListener listener) {
        int i = findListener(listeners, listener);
        if (i > -1) {
            listeners.remove(i);
        }
        if (0 == listeners.size()) {
            evtMap.uninitialize(this, CheckMenuItemEvent.Type.TOGGLED);
            listeners = null;
        }
    }

    protected void fireCheckMenuItemEvent(CheckMenuItemEvent event) {
        if (null == listeners) {
            return;
        }
        int size = listeners.size();
        int i = 0;
        while (i < size) {
            CheckMenuItemListener cbl = (CheckMenuItemListener) listeners
                    .elementAt(i);
            cbl.checkMenuItemEvent(event);
            i++;
        }
    }

    private void handleToggled() {
        fireCheckMenuItemEvent(new CheckMenuItemEvent(this,
                CheckMenuItemEvent.Type.TOGGLED));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("toggled", "handleToggled",
                CheckMenuItemEvent.Type.TOGGLED, CheckMenuItemListener.class);
    }

    native static final protected int gtk_check_menu_item_get_type();

    native static final protected Handle gtk_check_menu_item_new();

    native static final protected Handle gtk_check_menu_item_new_with_label(
            String label);

    native static final protected Handle gtk_check_menu_item_new_with_mnemonic(
            String label);

    native static final protected void gtk_check_menu_item_set_active(
            Handle check_menu_item, boolean isActive);

    native static final protected boolean gtk_check_menu_item_get_active(
            Handle check_menu_item);

    native static final protected void gtk_check_menu_item_toggled(
            Handle check_menu_item);

    native static final protected void gtk_check_menu_item_set_inconsistent(
            Handle check_menu_item, boolean setting);

    native static final protected boolean gtk_check_menu_item_get_inconsistent(
            Handle check_menu_item);

    native static final protected void gtk_check_menu_item_set_draw_as_radio(
            Handle cmenu, boolean asRadio);

    native static final protected boolean gtk_check_menu_item_get_draw_as_radio(
            Handle cmenu);

    /*
     * Deprecated functions. native static final private void
     * gtk_check_menu_item_set_show_toggle(Handle menu_item, boolean always);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
