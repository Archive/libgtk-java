/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * An anchor for embedding widgets within TextView widgets. You would usually
 * only use the methods of {@link TextView} to sort out Child widgets.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextChildAnchor</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextChildAnchor extends GObject {

    /**
     * Constructs a new TextChildAnchor.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextChildAnchor() {
        super(gtk_text_child_anchor_new());
    }

    public TextChildAnchor(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static TextChildAnchor getTextChildAnchor(Handle handle) {
        if (handle == null)
            return null;

        TextChildAnchor obj = (TextChildAnchor) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TextChildAnchor(handle);

        return obj;
    }

    /**
     * Returns the associated widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getWidget() {
        Handle hndl = gtk_text_child_anchor_get_widgets(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Returns true if the anchor has been deleted from the textbuffer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDeleted() {
        return gtk_text_child_anchor_get_deleted(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_text_child_anchor_get_type());
    }

    native static final protected int gtk_text_child_anchor_get_type();

    native static final protected Handle gtk_text_child_anchor_new();

    native static final protected Handle gtk_text_child_anchor_get_widgets(
            Handle anchor);

    native static final protected boolean gtk_text_child_anchor_get_deleted(
            Handle anchor);
}
