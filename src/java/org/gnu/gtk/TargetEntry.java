/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Definition for sources and destinations of Drag and Drop operations.
 * 
 * If a drag and drop is allowed between a pair of widgets, they must have the
 * same TargetEntry in their drop source and destination lists.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TargetEntry</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TargetEntry extends MemStruct {
    /**
     * Constructs a new target entry
     * 
     * @param name
     *            Name of the drag and drop type. This must be the same in both
     *            the drag source and destination (possibly different
     *            applications).
     * @param flags
     * @param id
     *            Integer id for the drag type. This will be passed to the event
     *            handlers. It can be different to that used at the other end of
     *            the drag/drop operation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TargetEntry(String name, TargetFlags flags, int id) {
        super(newTargetEntry(name, flags.getValue(), id));
    }

    native static protected final Handle newTargetEntry(String name, int flags,
            int id);

}
