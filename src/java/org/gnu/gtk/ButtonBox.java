/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is the base class that holds the common functions for GtkHButtonBox and
 * GtkVButtonBox. These are container widgets specifically designed to manage a
 * collection of buttons. GtkButtonBox has a number of control functions that
 * you can use to set the sizes and positions of the buttons.
 * 
 * @see HButtonBox
 * @see VButtonBox
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ButtonBox</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public abstract class ButtonBox extends Box {
    protected ButtonBox(Handle handle) {
        super(handle);
    }

    /**
     * Returns the layout which is used to dictate how the buttons are
     * organised.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ButtonBoxStyle getLayout() {
        return ButtonBoxStyle.intern(gtk_button_box_get_layout(getHandle()));
    }

    /**
     * Changes the way buttons are arranged in their container.
     * 
     * @param layout
     *            The new layout style.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLayout(ButtonBoxStyle layout) {
        gtk_button_box_set_layout(getHandle(), layout.getValue());
    }

    /**
     * Sets whether child should appear in a secondary group of children. A
     * typical use of a secondary child is the help button in a dialog.
     * <p>
     * This group appears after the other children if the style is
     * ButtonBoxStyle.START, ButtonBoxStyle.SPREAD or ButtonBoxStyle.EDGE, and
     * before the the other children if the style is ButtonBoxStyle.END. For
     * horizontal button boxes, the definition of before/after depends on
     * direction of the widget (see widget.setDirection). If the style is
     * ButtonBoxStyle.START or ButtonBoxStyle.END, then the secondary children
     * are aligned at the other end of the button box from the main children.
     * For the other styles, they appear immediately next to the main children.
     * 
     * @param child
     *            A child widget of this button box.
     * @param isSecondary
     *            If TRUE, the child appears in a secondary group of the button
     *            box.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setChildSecondary(Widget child, boolean isSecondary) {
        gtk_button_box_set_child_secondary(getHandle(), child.getHandle(),
                isSecondary);
    }

    /**
     * Gets whether the child appears in a secondary group of children.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getChildSecondary(Widget child) {
        return gtk_button_box_get_child_secondary(getHandle(), child
                .getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_button_box_get_type());
    }

    native static final protected int gtk_button_box_get_type();

    native static final protected int gtk_button_box_get_layout(Handle widget);

    native static final protected void gtk_button_box_set_layout(Handle widget,
            int layoutStyle);

    native static final protected void gtk_button_box_set_child_secondary(
            Handle widget, Handle child, boolean isSecondary);

    native static final protected boolean gtk_button_box_get_child_secondary(
            Handle widget, Handle child);

    /*
     * Deprecated functions. native static final private void
     * gtk_button_box_set_child_size(Handle widget, int min_width, int
     * min_height); native static final private void
     * gtk_button_box_set_child_ipadding(Handle widget, int ipad_x, int ipad_y);
     * native static final private void gtk_button_box_get_child_size(Handle
     * widget, int[] min_width, int[] min_height); native static final private
     * void gtk_button_box_get_child_ipadding(Handle widget, int[] ipad_x, int[]
     * ipad_y);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
