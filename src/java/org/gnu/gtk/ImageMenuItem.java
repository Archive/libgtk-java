/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is a MenuItem that displays an Image.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ImageMenuItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ImageMenuItem extends MenuItem {
    /**
     * Construct a new ImageMenuItem.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ImageMenuItem() {
        super(gtk_image_menu_item_new());
    }

    /**
     * Construct a new ImageMenuItem with a label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ImageMenuItem(String label, boolean hasMnemonic) {
        super(init(label, hasMnemonic));
    }

    private static Handle init(String label, boolean hasMnemonic) {
        if (hasMnemonic)
            return gtk_image_menu_item_new_with_mnemonic(label);
        else
            return gtk_image_menu_item_new_with_label(label);
    }

    /**
     * Construct a new ImageMenuItem using a stock icon.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ImageMenuItem(String stockID, AccelGroup group) {
        super(gtk_image_menu_item_new_from_stock(stockID, group.getHandle()));
    }

    /**
     * Construct a ImageMenuItem using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ImageMenuItem(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ImageMenuItem getImageMenuItem(Handle handle) {
        if (handle == null)
            return null;

        ImageMenuItem obj = (ImageMenuItem) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ImageMenuItem(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_image_menu_item_get_type());
    }

    public void setImage(Image image) {
        gtk_image_menu_item_set_image(getHandle(), image.getHandle());
    }

    public Image getImage() {
        Handle hndl = gtk_image_menu_item_get_image(getHandle());
        return Image.getImage(hndl);
    }

    native static final protected int gtk_image_menu_item_get_type();

    native static final protected Handle gtk_image_menu_item_new();

    native static final protected Handle gtk_image_menu_item_new_with_label(
            String label);

    native static final protected Handle gtk_image_menu_item_new_with_mnemonic(
            String label);

    native static final protected Handle gtk_image_menu_item_new_from_stock(
            String stockId, Handle accelGroup);

    native static final protected void gtk_image_menu_item_set_image(
            Handle image_menu_item, Handle image);

    native static final protected Handle gtk_image_menu_item_get_image(
            Handle image_menu_item);

}
