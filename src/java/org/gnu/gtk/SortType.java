/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * todo: doc
 * 
 * @see TreeViewColumn
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SortType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SortType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ASCENDING = 0;

    static final public org.gnu.gtk.SortType ASCENDING = new org.gnu.gtk.SortType(
            _ASCENDING);

    static final private int _DESCENDING = 1;

    static final public org.gnu.gtk.SortType DESCENDING = new org.gnu.gtk.SortType(
            _DESCENDING);

    static final private org.gnu.gtk.SortType[] theInterned = new org.gnu.gtk.SortType[] {
            ASCENDING, DESCENDING }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SortType theSacrificialOne = new org.gnu.gtk.SortType(
            0);

    static public org.gnu.gtk.SortType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SortType already = (org.gnu.gtk.SortType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SortType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SortType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SortType or(org.gnu.gtk.SortType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SortType and(org.gnu.gtk.SortType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SortType xor(org.gnu.gtk.SortType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SortType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
