/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Rectangle;
import org.gnu.gdk.Window;
import org.gnu.glib.Boxed;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Style</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Style extends GObject {
    /**
     * Construct a new Style object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Style() {
        super(Style.gtk_style_new());
    }

    /**
     * Construct a new Style object from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Style(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new RcStyle from a given handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Style getStyle(Handle handle) {
        if (handle == null)
            return null;

        Style obj = (Style) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Style(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_style_get_type());
    }

    public Style attach(Window window) {
        return Style
                .getStyle(gtk_style_attach(getHandle(), window.getHandle()));
    }

    public void detach(Style style) {
        gtk_style_detach(style.getHandle());
    }

    public void setBackground(Window window, StateType stateType) {
        gtk_style_set_background(getHandle(), window.getHandle(), stateType
                .getValue());
    }

    public void applyDefaultBackground(Window window, boolean setBG,
            StateType stateType, Rectangle rect, int x, int y, int width,
            int height) {
        gtk_style_apply_default_background(getHandle(), window.getHandle(),
                setBG, stateType.getValue(), rect.getHandle(), x, y, width,
                height);
    }

    public IconSet lookupIconSet(String stockID) {
        Handle handle = gtk_style_lookup_icon_set(getHandle(), stockID);
        if (handle == null) {
            return null;
        }
        IconSet iconSet = (IconSet) Boxed.getBoxedFromHandle(handle);
        if (iconSet == null) {
            iconSet = new IconSet(handle);
        }
        return iconSet;
    }

    native static final protected int gtk_style_get_type();

    native static final protected Handle gtk_style_new();

    native static final protected Handle gtk_style_copy(Handle style);

    native static final protected Handle gtk_style_attach(Handle style,
            Handle window);

    native static final protected void gtk_style_detach(Handle style);

    native static final protected void gtk_style_set_background(Handle style,
            Handle window, int stateType);

    native static final protected void gtk_style_apply_default_background(
            Handle style, Handle window, boolean setBG, int stateType,
            Handle area, int x, int y, int width, int height);

    native static final protected Handle gtk_style_lookup_icon_set(
            Handle style, String stockId);

    native static final protected Handle gtk_style_render_icon(Handle style,
            Handle source, int direction, int state, int size, int[] widget,
            String detail);
}
