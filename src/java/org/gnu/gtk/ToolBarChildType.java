/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolBarChildType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolBarChildType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _SPACE = 0;

    static final public org.gnu.gtk.ToolBarChildType SPACE = new org.gnu.gtk.ToolBarChildType(
            _SPACE);

    static final private int _BUTTON = 1;

    static final public org.gnu.gtk.ToolBarChildType BUTTON = new org.gnu.gtk.ToolBarChildType(
            _BUTTON);

    static final private int _TOGGLEBUTTON = 2;

    static final public org.gnu.gtk.ToolBarChildType TOGGLEBUTTON = new org.gnu.gtk.ToolBarChildType(
            _TOGGLEBUTTON);

    static final private int _RADIOBUTTON = 3;

    static final public org.gnu.gtk.ToolBarChildType RADIOBUTTON = new org.gnu.gtk.ToolBarChildType(
            _RADIOBUTTON);

    static final private int _WIDGET = 4;

    static final public org.gnu.gtk.ToolBarChildType WIDGET = new org.gnu.gtk.ToolBarChildType(
            _WIDGET);

    static final private org.gnu.gtk.ToolBarChildType[] theInterned = new org.gnu.gtk.ToolBarChildType[] {
            SPACE, BUTTON, TOGGLEBUTTON, RADIOBUTTON, WIDGET }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ToolBarChildType theSacrificialOne = new org.gnu.gtk.ToolBarChildType(
            0);

    static public org.gnu.gtk.ToolBarChildType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ToolBarChildType already = (org.gnu.gtk.ToolBarChildType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ToolBarChildType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ToolBarChildType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ToolBarChildType or(org.gnu.gtk.ToolBarChildType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ToolBarChildType and(org.gnu.gtk.ToolBarChildType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ToolBarChildType xor(org.gnu.gtk.ToolBarChildType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ToolBarChildType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
