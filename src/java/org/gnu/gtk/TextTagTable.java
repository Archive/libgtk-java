/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * See {@link TextView} description for an overview of the related objects.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextTagTable</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextTagTable extends GObject {
    protected TextTagTable(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used only by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static TextTagTable getTextTagTable(Handle handle) {
        if (handle == null)
            return null;

        TextTagTable obj = (TextTagTable) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TextTagTable(handle);

        return obj;
    }

    /**
     * Constructs a new table, containing no tags
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextTagTable() {
        super(gtk_text_tag_table_new());
    }

    /**
     * Add a tag to the table. The tag is assigned the highest priority in the
     * table.
     * 
     * <p>
     * tag must not be in a tag table already, and may not have the same name as
     * an already-added tag.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void add(TextTag tag) {
        gtk_text_tag_table_add(getHandle(), tag.getHandle());
    }

    /**
     * Remove a tag from the table.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void remove(TextTag tag) {
        gtk_text_tag_table_remove(getHandle(), tag.getHandle());
    }

    /**
     * Look up a named tag.
     * 
     * @param name
     *            name of a tag
     * @return The tag, or <code>null</code> if no tag exists by that name.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextTag lookup(String name) {
        Handle tagHandle = gtk_text_tag_table_lookup(getHandle(), name);
        return TextTag.getTextTag(tagHandle);
    }

    /**
     * Returns the size of the table (number of tags)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getSize() {
        return gtk_text_tag_table_get_size(getHandle());
    }

    /*
     * TODO: - foreach function - signals: tag-added tag-changed tag-removed
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_text_tag_table_get_type());
    }

    native static final protected int gtk_text_tag_table_get_type();

    native static final protected Handle gtk_text_tag_table_new();

    native static final protected void gtk_text_tag_table_add(Handle table,
            Handle tag);

    native static final protected void gtk_text_tag_table_remove(Handle table,
            Handle tag);

    native static final protected Handle gtk_text_tag_table_lookup(
            Handle table, String name);

    native static final protected int gtk_text_tag_table_get_size(Handle table);
}
