/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This widget is the base class for GtkHRuler and GtkVRuler. It contains the
 * configuration and calculation capability, but has no ability to display the
 * results.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Ruler</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Ruler extends Widget {
    /**
     * Construct a Ruler from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Ruler(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Ruler getRuler(Handle handle) {
        if (handle == null)
            return null;

        Ruler obj = (Ruler) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Ruler(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_ruler_get_type());
    }

    native static final protected int getXsrc(Handle cptr);

    native static final protected int getYsrc(Handle cptr);

    native static final protected int getSliderSize(Handle cptr);

    native static final protected double getLower(Handle cptr);

    native static final protected double getUpper(Handle cptr);

    native static final protected double getPosition(Handle cptr);

    native static final protected double getMaxSize(Handle cptr);

    native static final protected int gtk_ruler_get_type();

    native static final protected void gtk_ruler_set_metric(Handle ruler,
            int metric);

    native static final protected void gtk_ruler_set_range(Handle ruler,
            double lower, double upper, double position, double maxSize);

    native static final protected void gtk_ruler_draw_ticks(Handle ruler);

    native static final protected void gtk_ruler_draw_pos(Handle ruler);

    native static final protected int gtk_ruler_get_metric(Handle ruler);

    native static final protected void gtk_ruler_get_range(Handle ruler,
            double[] lower, double[] upper, double[] position, double[] maxSize);

}
