/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class Visibility extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gtk.Visibility NONE = new org.gnu.gtk.Visibility(
            _NONE);

    static final private int _PARTIAL = 1;

    static final public org.gnu.gtk.Visibility PARTIAL = new org.gnu.gtk.Visibility(
            _PARTIAL);

    static final private int _FULL = 2;

    static final public org.gnu.gtk.Visibility FULL = new org.gnu.gtk.Visibility(
            _FULL);

    static final private org.gnu.gtk.Visibility[] theInterned = new org.gnu.gtk.Visibility[] {
            NONE, PARTIAL, FULL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.Visibility theSacrificialOne = new org.gnu.gtk.Visibility(
            0);

    static public org.gnu.gtk.Visibility intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.Visibility already = (org.gnu.gtk.Visibility) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.Visibility(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Visibility(int value) {
        value_ = value;
    }

    public org.gnu.gtk.Visibility or(org.gnu.gtk.Visibility other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.Visibility and(org.gnu.gtk.Visibility other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.Visibility xor(org.gnu.gtk.Visibility other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.Visibility other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
