/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class AccelFlags extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _VISIBLE = 1 << 0;

    static final public org.gnu.gtk.AccelFlags VISIBLE = new org.gnu.gtk.AccelFlags(
            _VISIBLE);

    static final private int _LOCKED = 1 << 1;

    static final public org.gnu.gtk.AccelFlags LOCKED = new org.gnu.gtk.AccelFlags(
            _LOCKED);

    static final private int _MASK = 0x07;

    static final public org.gnu.gtk.AccelFlags MASK = new org.gnu.gtk.AccelFlags(
            _MASK);

    static final private org.gnu.gtk.AccelFlags[] theInterned = new org.gnu.gtk.AccelFlags[] {
            new org.gnu.gtk.AccelFlags(0), VISIBLE, LOCKED,
            new org.gnu.gtk.AccelFlags(3), MASK }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.AccelFlags theSacrificialOne = new org.gnu.gtk.AccelFlags(
            0);

    static public org.gnu.gtk.AccelFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.AccelFlags already = (org.gnu.gtk.AccelFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.AccelFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private AccelFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.AccelFlags or(org.gnu.gtk.AccelFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.AccelFlags and(org.gnu.gtk.AccelFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.AccelFlags xor(org.gnu.gtk.AccelFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.AccelFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
