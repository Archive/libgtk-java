/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * An object representing the various orientations and growth directions for
 * progress bar widgets.
 * 
 * @see ProgressBar GTK_PROGRESS_LEFT_TO_RIGHTA horizontal progress bar growing
 *      from left to right.GTK_PROGRESS_RIGHT_TO_LEFTA horizontal progress bar
 *      growing from right to left.GTK_PROGRESS_BOTTOM_TO_TOPA vertical progress
 *      bar growing from bottom to top.GTK_PROGRESS_TOP_TO_BOTTOMA vertical
 *      progress bar growing from top to bottom.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ProgressBarOrientation</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ProgressBarOrientation extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LEFT_TO_RIGHT = 0;

    static final public org.gnu.gtk.ProgressBarOrientation LEFT_TO_RIGHT = new org.gnu.gtk.ProgressBarOrientation(
            _LEFT_TO_RIGHT);

    static final private int _RIGHT_TO_LEFT = 1;

    static final public org.gnu.gtk.ProgressBarOrientation RIGHT_TO_LEFT = new org.gnu.gtk.ProgressBarOrientation(
            _RIGHT_TO_LEFT);

    static final private int _BOTTOM_TO_TOP = 2;

    static final public org.gnu.gtk.ProgressBarOrientation BOTTOM_TO_TOP = new org.gnu.gtk.ProgressBarOrientation(
            _BOTTOM_TO_TOP);

    static final private int _TOP_TO_BOTTOM = 3;

    static final public org.gnu.gtk.ProgressBarOrientation TOP_TO_BOTTOM = new org.gnu.gtk.ProgressBarOrientation(
            _TOP_TO_BOTTOM);

    static final private org.gnu.gtk.ProgressBarOrientation[] theInterned = new org.gnu.gtk.ProgressBarOrientation[] {
            LEFT_TO_RIGHT, RIGHT_TO_LEFT, BOTTOM_TO_TOP, TOP_TO_BOTTOM }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ProgressBarOrientation theSacrificialOne = new org.gnu.gtk.ProgressBarOrientation(
            0);

    static public org.gnu.gtk.ProgressBarOrientation intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ProgressBarOrientation already = (org.gnu.gtk.ProgressBarOrientation) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ProgressBarOrientation(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ProgressBarOrientation(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ProgressBarOrientation or(
            org.gnu.gtk.ProgressBarOrientation other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ProgressBarOrientation and(
            org.gnu.gtk.ProgressBarOrientation other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ProgressBarOrientation xor(
            org.gnu.gtk.ProgressBarOrientation other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ProgressBarOrientation other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
