/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class Orientation extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _HORIZONTAL = 0;

    static final public org.gnu.gtk.Orientation HORIZONTAL = new org.gnu.gtk.Orientation(
            _HORIZONTAL);

    static final private int _VERTICAL = 1;

    static final public org.gnu.gtk.Orientation VERTICAL = new org.gnu.gtk.Orientation(
            _VERTICAL);

    static final private org.gnu.gtk.Orientation[] theInterned = new org.gnu.gtk.Orientation[] {
            HORIZONTAL, VERTICAL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.Orientation theSacrificialOne = new org.gnu.gtk.Orientation(
            0);

    static public org.gnu.gtk.Orientation intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.Orientation already = (org.gnu.gtk.Orientation) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.Orientation(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Orientation(int value) {
        value_ = value;
    }

    public org.gnu.gtk.Orientation or(org.gnu.gtk.Orientation other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.Orientation and(org.gnu.gtk.Orientation other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.Orientation xor(org.gnu.gtk.Orientation other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.Orientation other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
