/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The MenuShell class is an abstract base class used to derive the Menu and
 * MenuBar subclasses.
 * <p>
 * A MenuShell is a container of MenuItem objects arranged in a list which can
 * be navigated, selected, and activated by the user to perform application
 * functions. A MenuItem can have a submenu associated with it, allowing for
 * nested hierarchical menus.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MenuShell</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public abstract class MenuShell extends Container {

    protected MenuShell(Handle handle) {
        super(handle);
    }

    /**
     * Append a new MenuItem to the end of the MenuShell's item list.
     * 
     * @param child
     *            The MenuItem to add
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void append(MenuItem child) {
        MenuShell.gtk_menu_shell_append(getHandle(), child.getHandle());
    }

    /**
     * Adds a new MenuItem to the beginning of the MenuShell's item list.
     * 
     * @param child
     *            The MenuItem to add.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void prepend(MenuItem child) {
        MenuShell.gtk_menu_shell_prepend(getHandle(), child.getHandle());
    }

    /**
     * Adds a new MenuItem to the MenuShell's item list at the requested
     * position.
     * 
     * @param child
     *            The MenuItem to add.
     * @param position
     *            The position in the item list where <code>child</code> is
     *            added. Positions are zero based.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insert(MenuItem child, int position) {
        MenuShell.gtk_menu_shell_insert(getHandle(), child.getHandle(),
                position);
    }

    /**
     * Deactivates the MenuShell. Typically this results in the MenuShell being
     * erased from the screen.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deactivate() {
        MenuShell.gtk_menu_shell_deactivate(getHandle());
    }

    /**
     * Selects the MenuItem from the MenuShell.
     * 
     * @param menuItem
     *            The MenuItem to select.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void selectItem(MenuItem menuItem) {
        MenuShell.gtk_menu_shell_select_item(getHandle(), menuItem.getHandle());
    }

    /**
     * Deselects the currently selected item from the MenuShell.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deselect() {
        MenuShell.gtk_menu_shell_deselect(getHandle());
    }

    /**
     * Activates the MenuItem within the MenuShell.
     * 
     * @param menuItem
     *            The MenuItem to activate.
     * @param forceDeactivate
     *            If <code>true</code>, force the deactivation of the
     *            MenuShell after the MenuItem is activated.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void activateItem(MenuItem menuItem, boolean forceDeactivate) {
        MenuShell.gtk_menu_shell_activate_item(getHandle(), menuItem
                .getHandle(), forceDeactivate);
    }

    /**
     * Select the first visible or selectable child of the menu shell. It won't
     * select tearoff items unless the only item is a tearoff item.
     * 
     * @param searchSensitive
     *            If TRUE, search for the first selectable menu item, otherwise
     *            select nothing if the first item isn't sensitive. This should
     *            be FALSE if the menu is being popped up initially.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void selectFirst(boolean searchSensitive) {
        gtk_menu_shell_select_first(getHandle(), searchSensitive);
    }

    /**
     * Cancels the selection within the menu shell.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void cancel() {
        gtk_menu_shell_cancel(getHandle());
    }

    public boolean getTakeFocus() {
        return gtk_menu_shell_get_take_focus(getHandle());
    }

    public void setTakeFocus(boolean takeFocus) {
        gtk_menu_shell_set_take_focus(getHandle(), takeFocus);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_menu_shell_get_type());
    }

    native static final protected int gtk_menu_shell_get_type();

    native static final protected void gtk_menu_shell_append(Handle menu_shell,
            Handle child);

    native static final protected void gtk_menu_shell_prepend(
            Handle menu_shell, Handle child);

    native static final protected void gtk_menu_shell_insert(Handle menu_shell,
            Handle child, int position);

    native static final protected void gtk_menu_shell_deactivate(
            Handle menu_shell);

    native static final protected void gtk_menu_shell_select_item(
            Handle menu_shell, Handle menuItem);

    native static final protected void gtk_menu_shell_deselect(Handle menu_shell);

    native static final protected void gtk_menu_shell_activate_item(
            Handle menu_shell, Handle menuItem, boolean forceDeactivate);

    native static final private void gtk_menu_shell_select_first(
            Handle menu_shell, boolean search_sensitive);

    native static final private void gtk_menu_shell_cancel(Handle menu_shell);

    // new for gtk 2.7
    native static final private boolean gtk_menu_shell_get_take_focus(
            Handle menu_shell);

    native static final private void gtk_menu_shell_set_take_focus(
            Handle menu_shell, boolean take_focus);

}
