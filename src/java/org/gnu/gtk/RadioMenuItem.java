/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A RadioMenuItem is a CheckMenuItem that belongs to a group. At each instant
 * exactly one of the RadioMenuItems from the group is selected.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RadioMenuItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RadioMenuItem extends CheckMenuItem {

    public RadioMenuItem(RadioMenuItem[] group) {
        super(init1(group));
    }

    private static Handle init1(RadioMenuItem[] group) {
        Handle[] groupHandle;
        if (null != group) {
            groupHandle = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                groupHandle[i] = group[i].getHandle();
            }
        } else
            groupHandle = null;
        return gtk_radio_menu_item_new(groupHandle);
    }

    /**
     * Create a new RadioMenuItem object adding it to the same <i>group</i> as
     * the provided RadioMenuItem. If this is the first RadioMenuItem pass
     * <code>null</code> for this parameter.
     * 
     * @param group
     *            A RadioMenuItem that belongs to the group that we wish to add
     *            this newly constructed RadioMenuItem to. If this is the first
     *            RadioMenuItem in the group just pass <code>null</code>.
     * @param label
     *            The text label to assign to this RadioButton.
     * @param hasMnemonic
     *            An indicator to inform the widget if the label contains a
     *            mnemonic.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RadioMenuItem(RadioMenuItem[] group, String label,
            boolean hasMnemonic) {
        super(init2(group, label, hasMnemonic));
    }

    private static Handle init2(RadioMenuItem[] group, String label,
            boolean hasMnemonic) {
        Handle[] groupHandle;
        if (null != group) {
            groupHandle = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                groupHandle[i] = group[i].getHandle();
            }
        } else
            groupHandle = null;
        if (hasMnemonic) {
            return gtk_radio_menu_item_new_with_mnemonic(groupHandle, label);
        } else {
            return gtk_radio_menu_item_new_with_label(groupHandle, label);
        }
    }

    public RadioMenuItem(RadioMenuItem group) {
        super(init3(group));
    }

    private static Handle init3(RadioMenuItem group) {
        Handle hndl;
        if (null == group)
            hndl = null;
        else
            hndl = group.getHandle();
        return gtk_radio_menu_item_new_from_widget(hndl);
    }

    public RadioMenuItem(RadioMenuItem group, String label, boolean hasMnemonic) {
        super(init4(group, label, hasMnemonic));
    }

    private static Handle init4(RadioMenuItem group, String label,
            boolean hasMnemonic) {
        Handle hndl;
        if (null == group)
            hndl = null;
        else
            hndl = group.getHandle();
        if (hasMnemonic) {
            return gtk_radio_menu_item_new_with_mnemonic_from_widget(hndl,
                    label);
        } else {
            return gtk_radio_menu_item_new_with_label_from_widget(hndl, label);
        }
    }

    /**
     * Internal method. Construct a RadioMenuItem using a handle to a native
     * resource (this is public primarily so that LibGlade can construct them).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RadioMenuItem(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only. Construct a
     * RadioMenuItem using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static RadioMenuItem getRadioMenuItem(Handle handle) {
        if (handle == null)
            return null;

        RadioMenuItem obj = (RadioMenuItem) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new RadioMenuItem(handle);

        return obj;
    }

    public RadioMenuItem[] getGroup() {
        Handle[] hndls = gtk_radio_menu_item_get_group(getHandle());
        RadioMenuItem[] items = new RadioMenuItem[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            items[i] = RadioMenuItem.getRadioMenuItem(hndls[i]);
        }
        return items;
    }

    public void setGroup(RadioMenuItem[] group) {
        Handle[] hndls = new Handle[group.length];
        for (int i = 0; i < group.length; i++) {
            hndls[i] = group[i].getHandle();
        }
        gtk_radio_menu_item_set_group(getHandle(), hndls);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_radio_menu_item_get_type());
    }

    native static final protected int gtk_radio_menu_item_get_type();

    native static final protected Handle gtk_radio_menu_item_new(Handle[] group);

    native static final protected Handle gtk_radio_menu_item_new_with_label(
            Handle[] group, String label);

    native static final protected Handle gtk_radio_menu_item_new_with_mnemonic(
            Handle[] group, String label);

    native static final protected Handle gtk_radio_menu_item_new_from_widget(
            Handle group);

    native static final protected Handle gtk_radio_menu_item_new_with_mnemonic_from_widget(
            Handle group, String label);

    native static final protected Handle gtk_radio_menu_item_new_with_label_from_widget(
            Handle group, String label);

    native static final protected Handle[] gtk_radio_menu_item_get_group(
            Handle menu);

    native static final protected void gtk_radio_menu_item_set_group(
            Handle menu, Handle[] group);

}
