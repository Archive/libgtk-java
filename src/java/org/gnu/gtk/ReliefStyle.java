/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * The reliefStyle is used by a number of widgets, such as Button. All methods
 * of this calss are for internal use only. The developer uses ReliefStyle in
 * the following manner: <br>
 * <code>Button.setReliefStyle( ReliefStyle.HALF );</code> todo...
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ReliefStyle</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ReliefStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORMAL = 0;

    static final public org.gnu.gtk.ReliefStyle NORMAL = new org.gnu.gtk.ReliefStyle(
            _NORMAL);

    static final private int _HALF = 1;

    static final public org.gnu.gtk.ReliefStyle HALF = new org.gnu.gtk.ReliefStyle(
            _HALF);

    static final private int _NONE = 2;

    static final public org.gnu.gtk.ReliefStyle NONE = new org.gnu.gtk.ReliefStyle(
            _NONE);

    static final private org.gnu.gtk.ReliefStyle[] theInterned = new org.gnu.gtk.ReliefStyle[] {
            NORMAL, HALF, NONE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ReliefStyle theSacrificialOne = new org.gnu.gtk.ReliefStyle(
            0);

    static public org.gnu.gtk.ReliefStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ReliefStyle already = (org.gnu.gtk.ReliefStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ReliefStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ReliefStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ReliefStyle or(org.gnu.gtk.ReliefStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ReliefStyle and(org.gnu.gtk.ReliefStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ReliefStyle xor(org.gnu.gtk.ReliefStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ReliefStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
