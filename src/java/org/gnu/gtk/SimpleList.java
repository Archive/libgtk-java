/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a widget which contains a list of strings. It is a wrapper class for
 * the more powerful set of tree and list objects, described at {@link
 * TreeView}.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SimpleList</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SimpleList extends Widget {

    private int size = 0;

    /** The view widget */
    private TreeView view;

    /** Data store */
    private ListStore store;

    /** Column for displaying the data */
    private TreeViewColumn column;

    /** renderer for the strings */
    private CellRendererText renderer;

    private DataColumnString dataBlock;

    /**
     * Constructs a SimpleList object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SimpleList() {
        this(new TreeView());
    }

    private SimpleList(TreeView v) {
        super(v.getHandle());
        dataBlock = new DataColumnString();
        store = new ListStore(new DataColumn[] { dataBlock });
        view = v;
        view.setModel(store);
        column = new TreeViewColumn();
        renderer = new CellRendererText();
        column.packStart(renderer, false);
        column.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                dataBlock);
        view.appendColumn(column);
        view.setHeadersVisible(false);
        size = 0;
    }

    /**
     * Adds an element to the start of the list
     * 
     * @param text
     *            the text of the element
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addStart(String text) {
        TreeIter iter = store.prependRow();
        store.setValue(iter, dataBlock, text);
        size++;
    }

    /**
     * Adds an element to the end of the list
     * 
     * @param text
     *            The text of the element
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addEnd(String text) {
        TreeIter iter = store.appendRow();
        store.setValue(iter, dataBlock, text);
        size++;
    }

    /**
     * Adds a new element at the given position
     * 
     * @param text
     *            The text of the new element
     * @param position
     *            The position in which it should be placed. 0 is at the start
     *            of the list.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void add(String text, int position) {
        TreeIter iter = store.insertRow(position);
        store.setValue(iter, dataBlock, text);
        size++;
    }

    /**
     * Returns the string at the given position
     * 
     * @param position
     *            The position in the list to investigate
     * @return The value of the cell at that position
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getValue(int position) {
        return store.getValue(store.getIter("" + position), dataBlock);
    }

    /**
     * Returns true if the element at <code>index</code> is selected
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getSelected(int index) {
        return view.getSelection().getSelected(store.getIter("" + index));
    }

    /**
     * Selects the element at <code>index</code>
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void select(int index) {
        view.getSelection().select(store.getIter("" + index));
    }

    /**
     * Sets how many of the elements can be selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSelectable(SelectionMode setting) {
        view.getSelection().setMode(setting);
    }

    /**
     * Removes the element at the given position
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void remove(int position) {
        store.removeRow(store.getIter("" + position));
        size--;
    }

    /**
     * Remove the given element. This iterates over the elements contained in
     * the list until a matching element is found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void remove(String val) {
        TreeIter iter = store.getFirstIter();
        boolean found = false;
        while (iter != null && !found) {
            if (store.getValue(iter, dataBlock).equals(val))
                found = true;
            else
                iter = iter.getNextIter();
        }
        if (found) {
            store.removeRow(iter);
            size--;
        }
    }

    /**
     * Sets whether the user may edit the cell contents directly.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEditable(boolean setting) {
        renderer.setEditable(setting);
    }

    /**
     * Returns the number of items in the list
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getSize() {
        return size;
    }

    public String[] getSelected() {
        TreeSelection selection = view.getSelection();
        TreePath[] rows = selection.getSelectedRows();
        if (rows == null)
            return null;
        String[] values = new String[rows.length];
        for (int i = 0; i < rows.length; i++) {
            TreeIter anIter = store.getIter(rows[i]);
            values[i] = store.getValue(anIter, dataBlock);
        }
        return values;
    }

    public String[] getEntries() {
        TreeIter anIter = store.getFirstIter();
        List strings = new ArrayList();
        while (null != anIter) {
            strings.add(store.getValue(anIter, dataBlock));
            anIter.getNextIter();
        }
        String[] values = new String[strings.size()];
        for (int i = 0; i < strings.size(); i++)
            values[i] = (String) strings.get(i);
        return values;
    }

}
