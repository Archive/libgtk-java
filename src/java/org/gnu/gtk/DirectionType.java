/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class DirectionType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TAB_FORWARD = 0;

    static final public org.gnu.gtk.DirectionType TAB_FORWARD = new org.gnu.gtk.DirectionType(
            _TAB_FORWARD);

    static final private int _TAB_BACKWARD = 1;

    static final public org.gnu.gtk.DirectionType TAB_BACKWARD = new org.gnu.gtk.DirectionType(
            _TAB_BACKWARD);

    static final private int _UP = 2;

    static final public org.gnu.gtk.DirectionType UP = new org.gnu.gtk.DirectionType(
            _UP);

    static final private int _DOWN = 3;

    static final public org.gnu.gtk.DirectionType DOWN = new org.gnu.gtk.DirectionType(
            _DOWN);

    static final private int _LEFT = 4;

    static final public org.gnu.gtk.DirectionType LEFT = new org.gnu.gtk.DirectionType(
            _LEFT);

    static final private int _RIGHT = 5;

    static final public org.gnu.gtk.DirectionType RIGHT = new org.gnu.gtk.DirectionType(
            _RIGHT);

    static final private org.gnu.gtk.DirectionType[] theInterned = new org.gnu.gtk.DirectionType[] {
            TAB_FORWARD, TAB_BACKWARD, UP, DOWN, LEFT, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.DirectionType theSacrificialOne = new org.gnu.gtk.DirectionType(
            0);

    static public org.gnu.gtk.DirectionType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.DirectionType already = (org.gnu.gtk.DirectionType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.DirectionType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DirectionType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.DirectionType or(org.gnu.gtk.DirectionType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.DirectionType and(org.gnu.gtk.DirectionType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.DirectionType xor(org.gnu.gtk.DirectionType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.DirectionType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
