/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.List;

import org.gnu.glib.Error;
import org.gnu.glib.EventMap;
import org.gnu.glib.GObject;
import org.gnu.gtk.event.FileChooserEvent;
import org.gnu.gtk.event.FileChooserListener;
import org.gnu.glib.Handle;

/**
 * This class implements the native methods that are common with both
 * FileChooserDialog and FileChooserWidget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileChooserHelper</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
class FileChooserHelper {

    static boolean setFilename(Handle chooserHandle, String filename) {
        if (filename == null) {
            throw new RuntimeException("filename must not be null");
        }
        return gtk_file_chooser_set_filename(chooserHandle, filename);
    }

    static void addShortcutFolder(Handle chooserHandle, String folder)
            throws FileChooserException {
        Handle errorHandle = gtk_file_chooser_add_shortcut_folder(
                chooserHandle, folder);
        if (errorHandle != null) {
            Error error = new Error(errorHandle);
            throw new FileChooserException(error.getErrorCode());
        }
    }

    static void removeShortcutFolder(Handle chooserHandle, String folder)
            throws FileChooserException {
        Handle errorHandle = gtk_file_chooser_remove_shortcut_folder(
                chooserHandle, folder);
        if (errorHandle != null) {
            Error error = new Error(errorHandle);
            throw new FileChooserException(error.getErrorCode());
        }
    }

    static void addShortcutURI(Handle chooserHandle, String uri)
            throws FileChooserException {
        Handle errorHandle = gtk_file_chooser_add_shortcut_folder_uri(
                chooserHandle, uri);
        if (errorHandle != null) {
            Error error = new Error(errorHandle);
            throw new FileChooserException(error.getErrorCode());
        }
    }

    static void removeShortcutURI(Handle chooserHandle, String uri)
            throws FileChooserException {
        Handle errorHandle = gtk_file_chooser_remove_shortcut_folder_uri(
                chooserHandle, uri);
        if (errorHandle != null) {
            Error error = new Error(errorHandle);
            throw new FileChooserException(error.getErrorCode());
        }
    }

    static void fireCurrentFolderChanged(List listeners, FileChooserEvent event) {
        if (listeners == null)
            return;

        for (int i = 0, c = listeners.size(); i < c; ++i) {
            ((FileChooserListener) listeners.get(i))
                    .currentFolderChanged(event);
        }
    }

    static void fireFileActivated(List listeners, FileChooserEvent event) {
        if (listeners == null)
            return;

        for (int i = 0, c = listeners.size(); i < c; ++i) {
            ((FileChooserListener) listeners.get(i)).fileActivated(event);
        }
    }

    static void fireSelectionChanged(List listeners, FileChooserEvent event) {
        if (listeners == null)
            return;

        for (int i = 0, c = listeners.size(); i < c; ++i) {
            ((FileChooserListener) listeners.get(i)).selectionChanged(event);
        }
    }

    static void fireUpdatePreview(List listeners, FileChooserEvent event) {
        if (listeners == null)
            return;

        for (int i = 0, c = listeners.size(); i < c; ++i) {
            ((FileChooserListener) listeners.get(i)).updatePreview(event);
        }
    }

    static void addListener(List listeners, FileChooserListener listener,
            EventMap eventMap, GObject source) {
        if (listeners.size() == 0) {
            FileChooserEvent.Type[] types = FileChooserEvent.Type.getTypes();
            for (int i = 0; i < types.length; i++) {
                eventMap.initialize(source, types[i]);
            }
        } else if (listeners.contains(listener))
            return;

        listeners.add(listener);
    }

    static void removeListener(List listeners, FileChooserListener listener,
            EventMap eventMap, GObject source) {
        if (listeners == null) {
            return;
        }

        listeners.remove(listener);

        if (listeners.size() == 0) {
            FileChooserEvent.Type[] types = FileChooserEvent.Type.getTypes();
            for (int i = 0; i < types.length; i++) {
                eventMap.uninitialize(source, types[i]);
            }
        }
    }

    native static final void gtk_file_chooser_set_action(Handle chooser,
            int action);

    native static final int gtk_file_chooser_get_action(Handle chooser);

    native static final void gtk_file_chooser_set_local_only(Handle chooser,
            boolean localOnly);

    native static final boolean gtk_file_chooser_get_local_only(Handle chooser);

    native static final void gtk_file_chooser_set_select_multiple(
            Handle chooser, boolean selectMultiple);

    native static final boolean gtk_file_chooser_get_select_multiple(
            Handle chooser);

    native static final void gtk_file_chooser_set_current_name(Handle chooser,
            String name);

    native static final String gtk_file_chooser_get_filename(Handle chooser);

    native static final boolean gtk_file_chooser_set_filename(Handle chooser,
            String filename);

    native static final boolean gtk_file_chooser_select_filename(
            Handle chooser, String filename);

    native static final void gtk_file_chooser_unselect_filename(Handle chooser,
            String filename);

    native static final void gtk_file_chooser_select_all(Handle chooser);

    native static final void gtk_file_chooser_unselect_all(Handle chooser);

    native static final String[] gtk_file_chooser_get_filenames(Handle chooser);

    native static final boolean gtk_file_chooser_set_current_folder(
            Handle chooser, String folder);

    native static final String gtk_file_chooser_get_current_folder(
            Handle chooser);

    native static final String gtk_file_chooser_get_uri(Handle chooser);

    native static final boolean gtk_file_chooser_set_uri(Handle chooser,
            String uri);

    native static final boolean gtk_file_chooser_select_uri(Handle chooser,
            String uri);

    native static final void gtk_file_chooser_unselect_uri(Handle chooser,
            String uri);

    native static final String[] gtk_file_chooser_get_uris(Handle chooser);

    native static final boolean gtk_file_chooser_set_current_folder_uri(
            Handle chooser, String uri);

    native static final String gtk_file_chooser_get_current_folder_uri(
            Handle chooser);

    native static final void gtk_file_chooser_set_preview_widget(
            Handle chooser, Handle previewWidget);

    native static final Handle gtk_file_chooser_get_preview_widget(
            Handle chooser);

    native static final void gtk_file_chooser_set_preview_widget_active(
            Handle chooser, boolean active);

    native static final boolean gtk_file_chooser_get_preview_widget_active(
            Handle chooser);

    native static final void gtk_file_chooser_set_use_preview_label(
            Handle chooser, boolean useLabel);

    native static final boolean gtk_file_chooser_get_use_preview_label(
            Handle chooser);

    native static final String gtk_file_chooser_get_preview_filename(
            Handle chooser);

    native static final String gtk_file_chooser_get_preview_uri(Handle chooser);

    native static final void gtk_file_chooser_set_extra_widget(Handle chooser,
            Handle extraWidget);

    native static final Handle gtk_file_chooser_get_extra_widget(Handle chooser);

    native static final void gtk_file_chooser_add_filter(Handle chooser,
            Handle filter);

    native static final void gtk_file_chooser_remove_filter(Handle chooser,
            Handle filter);

    native static final Handle[] gtk_file_chooser_list_filters(Handle chooser);

    native static final void gtk_file_chooser_set_filter(Handle chooser,
            Handle filter);

    native static final Handle gtk_file_chooser_get_filter(Handle chooser);

    native static final Handle gtk_file_chooser_add_shortcut_folder(
            Handle chooser, String folder);

    native static final Handle gtk_file_chooser_remove_shortcut_folder(
            Handle chooser, String folder);

    native static final String[] gtk_file_chooser_list_shortcut_folders(
            Handle chooser);

    native static final Handle gtk_file_chooser_add_shortcut_folder_uri(
            Handle chooser, String uri);

    native static final Handle gtk_file_chooser_remove_shortcut_folder_uri(
            Handle chooser, String uri);

    native static final String[] gtk_file_chooser_list_shortcut_folder_uris(
            Handle chooser);

    native static final void gtk_file_chooser_set_show_hidden(Handle chooser,
            boolean show_hidden);

    native static final boolean gtk_file_chooser_get_show_hidden(Handle chooser);

    native static final private int gtk_file_chooser_get_type();
}
