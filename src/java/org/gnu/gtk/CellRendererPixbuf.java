/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A {@link CellRenderer} for pixbufs.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRendererPixbuf</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRendererPixbuf extends CellRenderer {
    /**
     * Creates a new CellRendererPixbuf
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CellRendererPixbuf() {
        super(gtk_cell_renderer_pixbuf_new());
    }

    /**
     * Attributes for the CellRenderer
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static class Attribute extends CellRenderer.Attribute {
        private Attribute(String attr) {
            super(attr);
        }

        /** The pixbuf to render. */
        public static final Attribute PIXBUF = new Attribute("pixbuf");

        /** Pixbuf for open expander. */
        public static final Attribute PIXBUF_EXPANDER_OPEN = new Attribute(
                "pixbuf-expander-open");

        /** Pixbuf for closed expander. */
        public static final Attribute PIXBUF_EXPANDER_CLOSED = new Attribute(
                "pixbuf-expander-closed");

        /** The stock icon to render. */
        public static final Attribute STOCK_ID = new Attribute("stock-id");

        /** The size of the rendered icon. */
        public static final Attribute STOCK_SIZE = new Attribute("stock-size");

    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixbuf(Pixbuf pixbuf) {
        gtk_setPixbuf(getHandle(), pixbuf.getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixbufExpanderOpen(Pixbuf pixbuf) {
        gtk_setPixbufOpen(getHandle(), pixbuf.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_cell_renderer_pixbuf_get_type());
    }

    native static final protected int gtk_cell_renderer_pixbuf_get_type();

    native static final protected Handle gtk_cell_renderer_pixbuf_new();

    native static final protected void gtk_setPixbuf(Handle cr, Handle pixbuf);

    native static final protected void gtk_setPixbufOpen(Handle cr, Handle pixbuf);

}
