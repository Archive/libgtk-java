/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class TargetFlags extends Flags {
    static final public org.gnu.gtk.TargetFlags NO_RESTRICTION = new org.gnu.gtk.TargetFlags(
            0);

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _SAME_APP = 1 << 0;

    static final public org.gnu.gtk.TargetFlags SAME_APP = new org.gnu.gtk.TargetFlags(
            _SAME_APP);

    static final private int _SAME_WIDGET = 1 << 1;

    static final public org.gnu.gtk.TargetFlags SAME_WIDGET = new org.gnu.gtk.TargetFlags(
            _SAME_WIDGET);

    static final private org.gnu.gtk.TargetFlags[] theInterned = new org.gnu.gtk.TargetFlags[] {
            new org.gnu.gtk.TargetFlags(0), SAME_APP, SAME_WIDGET }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TargetFlags theSacrificialOne = new org.gnu.gtk.TargetFlags(
            0);

    static public org.gnu.gtk.TargetFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TargetFlags already = (org.gnu.gtk.TargetFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TargetFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TargetFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TargetFlags or(org.gnu.gtk.TargetFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TargetFlags and(org.gnu.gtk.TargetFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TargetFlags xor(org.gnu.gtk.TargetFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TargetFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
