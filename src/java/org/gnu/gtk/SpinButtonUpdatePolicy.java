/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * This is the property which determines when a {@link SpinButton} widget should
 * be updated. TODO: doc ALWAYS When refreshing your GtkSpinButton, the value is
 * always displayed. IF_VALIDWhen refreshing your GtkSpinButton, the value is
 * only displayed if it is valid within the bounds of the spin button's
 * GtkAdjustment.
 * 
 * @see SpinButton
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SpinButtonUpdatePolicy</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SpinButtonUpdatePolicy extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ALWAYS = 0;

    static final public org.gnu.gtk.SpinButtonUpdatePolicy ALWAYS = new org.gnu.gtk.SpinButtonUpdatePolicy(
            _ALWAYS);

    static final private int _IF_VALID = 1;

    static final public org.gnu.gtk.SpinButtonUpdatePolicy IF_VALID = new org.gnu.gtk.SpinButtonUpdatePolicy(
            _IF_VALID);

    static final private org.gnu.gtk.SpinButtonUpdatePolicy[] theInterned = new org.gnu.gtk.SpinButtonUpdatePolicy[] {
            ALWAYS, IF_VALID }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SpinButtonUpdatePolicy theSacrificialOne = new org.gnu.gtk.SpinButtonUpdatePolicy(
            0);

    static public org.gnu.gtk.SpinButtonUpdatePolicy intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SpinButtonUpdatePolicy already = (org.gnu.gtk.SpinButtonUpdatePolicy) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SpinButtonUpdatePolicy(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SpinButtonUpdatePolicy(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SpinButtonUpdatePolicy or(
            org.gnu.gtk.SpinButtonUpdatePolicy other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SpinButtonUpdatePolicy and(
            org.gnu.gtk.SpinButtonUpdatePolicy other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SpinButtonUpdatePolicy xor(
            org.gnu.gtk.SpinButtonUpdatePolicy other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SpinButtonUpdatePolicy other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
