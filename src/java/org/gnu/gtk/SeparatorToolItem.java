/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SeparatorToolItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SeparatorToolItem extends ToolItem {

    public SeparatorToolItem() {
        super(gtk_separator_tool_item_new());
    }

    public SeparatorToolItem(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static SeparatorToolItem getSeparatorToolItem(Handle handle) {
        if (handle == null)
            return null;

        SeparatorToolItem obj = (SeparatorToolItem) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new SeparatorToolItem(handle);

        return obj;
    }

    /**
     * Sets whether the SeparatorToolItem is drawn as a line or just blank.
     * 
     * @param drawLine
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDrawLine(boolean drawLine) {
        gtk_separator_tool_item_set_draw(getHandle(), drawLine);
    }

    /**
     * Returns whether the SeparatorToolItem is drawn as a line or just blank.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDrawLine() {
        return gtk_separator_tool_item_get_draw(getHandle());
    }

    native static final protected int gtk_separator_tool_item_get_type();

    native static final protected Handle gtk_separator_tool_item_new();

    native static final protected boolean gtk_separator_tool_item_get_draw(
            Handle item);

    native static final protected void gtk_separator_tool_item_set_draw(
            Handle item, boolean draw);
}
