/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

public class ToolTipsData extends MemStruct {
    ToolTipsData(Handle handle) {
        super(handle);
    }

    static ToolTipsData getToolTipsData(Handle handle) {
        if (handle == null) {
            return null;
        }
        ToolTipsData tipsData = (ToolTipsData) MemStruct
                .getMemStructFromHandle(handle);
        if (tipsData == null) {
            tipsData = new ToolTipsData(handle);
        }
        return tipsData;
    }

    public ToolTips getToolTip() {
        Handle handle = getToolTip(getHandle());
        return ToolTips.getToolTips(handle);
    }

    public Widget getWidget() {
        Handle handle = getWidget(getHandle());
        return Widget.getWidget(handle);
    }

    public String getTipText() {
        return getTipText(getHandle());
    }

    native static final protected Handle getToolTip(Handle cptr);

    native static final protected Handle getWidget(Handle cptr);

    native static final protected String getTipText(Handle cptr);

    native static final protected Handle gtk_tooltips_data_get(Handle widget);

}
