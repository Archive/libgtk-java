/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import java.util.Vector;

import org.gnu.gdk.Color;
import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.GObject;
import org.gnu.gtk.event.ColorButtonEvent;
import org.gnu.gtk.event.ColorButtonListener;
import org.gnu.glib.Handle;

/**
 * The ColorButton is a widget in the form of a small button containing a swatch
 * representing the current selected color. When the button is clicked, a
 * ColorSelection dialog will open, allowing the user to select a color. The
 * swatch will be updated to reflect the new color when the user finishes.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ColorButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ColorButton extends Button {

    /**
     * Create a new ColorButton.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorButton() {
        super(gtk_color_button_new());
    }

    /**
     * Create a new ColorButton initialized with the provided color.
     * 
     * @param color
     *            A Color to set the current color with.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorButton(Color color) {
        super(gtk_color_button_new_with_color(color.getHandle()));
    }

    /**
     * For internal use by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorButton(Handle handle) {
        super(handle);
    }

    /**
     * For internal use by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ColorButton getColorButton(Handle handle) {
        if (handle == null) {
            return null;
        }

        ColorButton obj = (ColorButton) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new ColorButton(handle);
        }

        return obj;
    }

    /**
     * Set the current color for the widget.
     * 
     * @param color
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColor(Color color) {
        gtk_color_button_set_color(getHandle(), color.getHandle());
    }

    /**
     * Returns the current color value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color getColor() {
        return new Color(gtk_color_button_get_color(getHandle()));
    }

    /**
     * Sets the current opacity to be alpha
     * 
     * @param alpha
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAlpha(int alpha) {
        gtk_color_button_set_alpha(getHandle(), alpha);
    }

    /**
     * Returns the current alpha value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getAlpha() {
        return gtk_color_button_get_alpha(getHandle());
    }

    /**
     * Sets whether or not the ColorButton should use the alpha channel.
     * 
     * @param useAlpha
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseAlpha(boolean useAlpha) {
        gtk_color_button_set_use_alpha(getHandle(), useAlpha);
    }

    /**
     * Returns whether or not the ColorButton is using the alpha channel.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getUseAlpha() {
        return gtk_color_button_get_use_alpha(getHandle());
    }

    /**
     * Sets the color for the ColorSelectionDialog.
     * 
     * @param title
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gtk_color_button_set_title(getHandle(), title);
    }

    /**
     * Returns the title from the ColorSelectionDialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        return gtk_color_button_get_title(getHandle());
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/

    /**
     * Listeners for handling dialog events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector cbListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see ColorButtonListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ColorButtonListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(cbListeners, listener);
        if (i == -1) {
            if (null == cbListeners) {
                evtMap.initialize(this, ColorButtonEvent.Type.COLOR_SET);
                cbListeners = new Vector();
            }
            cbListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ColorButtonListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ColorButtonListener listener) {
        int i = findListener(cbListeners, listener);
        if (i > -1) {
            cbListeners.remove(i);
        }
        if (0 == cbListeners.size()) {
            evtMap.uninitialize(this, ColorButtonEvent.Type.COLOR_SET);
            cbListeners = null;
        }
    }

    protected void fireColorButtonEvent(ColorButtonEvent event) {
        if (null == cbListeners) {
            return;
        }
        int size = cbListeners.size();
        int i = 0;
        while (i < size) {
            ColorButtonListener cbl = (ColorButtonListener) cbListeners
                    .elementAt(i);
            cbl.colorButtonEvent(event);
            i++;
        }
    }

    private void handleColorSet() {
        fireColorButtonEvent(new ColorButtonEvent(this,
                ColorButtonEvent.Type.COLOR_SET));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("color_set", "handleColorSet",
                ColorButtonEvent.Type.COLOR_SET, ColorButtonListener.class);
    }

    native static final protected int gtk_color_button_get_type();

    native static final protected Handle gtk_color_button_new();

    native static final protected Handle gtk_color_button_new_with_color(
            Handle color);

    native static final protected void gtk_color_button_set_color(
            Handle button, Handle color);

    native static final protected void gtk_color_button_set_alpha(
            Handle button, int alpha);

    native static final protected Handle gtk_color_button_get_color(
            Handle button);

    native static final protected int gtk_color_button_get_alpha(Handle button);

    native static final protected void gtk_color_button_set_use_alpha(
            Handle button, boolean useAlpha);

    native static final protected boolean gtk_color_button_get_use_alpha(
            Handle button);

    native static final protected void gtk_color_button_set_title(
            Handle button, String title);

    native static final protected String gtk_color_button_get_title(
            Handle button);

}
