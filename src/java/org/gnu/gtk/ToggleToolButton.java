/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Handle;
import org.gnu.gtk.event.ToggleToolButtonEvent;
import org.gnu.gtk.event.ToggleToolButtonListener;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToggleToolButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToggleToolButton extends ToolButton {

    public ToggleToolButton() {
        super(gtk_toggle_tool_button_new());
    }

    public ToggleToolButton(String stockId) {
        super(gtk_toggle_tool_button_new_from_stock(stockId));
    }

    public ToggleToolButton(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToggleToolButton getToggleToolButton(Handle handle) {
        if (handle == null)
            return null;

        ToggleToolButton obj = (ToggleToolButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ToggleToolButton(handle);

        return obj;
    }

    public void setActive(boolean active) {
        gtk_toggle_tool_button_set_active(getHandle(), active);
    }

    public boolean getActive() {
        return gtk_toggle_tool_button_get_active(getHandle());
    }

    /*
     * EVENT HANDLING
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    /**
     * Listeners for handling events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector tbListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see ToggleToolButtonListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ToggleToolButtonListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(tbListeners, listener);
        if (i == -1) {
            if (null == tbListeners) {
                evtMap.initialize(this, ToggleToolButtonEvent.Type.TOGGLED);
                tbListeners = new Vector();
            }
            tbListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ToggleToolButtonListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ToggleToolButtonListener listener) {
        int i = findListener(tbListeners, listener);
        if (i > -1) {
            tbListeners.remove(i);
        }
        if (0 == tbListeners.size()) {
            evtMap.uninitialize(this, ToggleToolButtonEvent.Type.TOGGLED);
            tbListeners = null;
        }
    }

    protected void fireToggleToolButtonEvent(ToggleToolButtonEvent event) {
        if (null == tbListeners) {
            return;
        }
        int size = tbListeners.size();
        int i = 0;
        while (i < size) {
            ToggleToolButtonListener tbl = (ToggleToolButtonListener) tbListeners
                    .elementAt(i);
            tbl.toggleToolButtonEvent(event);
            i++;
        }
    }

    private void handleToggled() {
        ToggleToolButtonEvent evt = new ToggleToolButtonEvent(this,
                ToggleToolButtonEvent.Type.TOGGLED);
        fireToggleToolButtonEvent(evt);
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("toggled", "handleToggled",
                ToggleToolButtonEvent.Type.TOGGLED,
                ToggleToolButtonListener.class);
    }

    /*
     * BEGINNING OF JNI CODE
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    native static final protected int gtk_toggle_tool_button_get_type();

    native static final protected Handle gtk_toggle_tool_button_new();

    native static final protected Handle gtk_toggle_tool_button_new_from_stock(
            String stockId);

    native static final protected void gtk_toggle_tool_button_set_active(
            Handle button, boolean active);

    native static final protected boolean gtk_toggle_tool_button_get_active(
            Handle button);
    /*
     * END OF JNI CODE
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
