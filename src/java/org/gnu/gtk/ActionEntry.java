/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.MemStruct;
import org.gnu.gtk.event.ActionEntryListener;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ActionEntry</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ActionEntry extends MemStruct {

    /**
     * ActionEntryListener objects that are interested in events for this entry.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private List listeners = new ArrayList();

    public ActionEntry(String name, String stockId, String label) {
        this(name, stockId, label, null, null, null);
    }

    public ActionEntry(String name, String stockId, String label, String accel,
            String tooltip, ActionEntryListener listener) {

        // create the native object
        super(allocate());
        // set the values
        setName(getHandle(), name);
        setStockId(getHandle(), stockId);
        setLabel(getHandle(), label);
        setAccelerator(getHandle(), accel);
        setToolTip(getHandle(), tooltip);
        if (null != listener) {
            listeners.add(listener);
        }
    }

    public void addListener(ActionEntryListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ActionEntryListener listener) {
        listeners.remove(listener);
    }

    private void handleCallback(Handle action) {
        for (int i = 0; i < listeners.size(); i++) {
            ActionEntryListener l = (ActionEntryListener) listeners.get(i);
            l.actionEvent(Action.getAction(action));
        }
    }

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected Handle allocate();

    native static final protected void setName(Handle entry, String name);

    native static final protected void setStockId(Handle entry, String stockId);

    native static final protected void setLabel(Handle entry, String label);

    native static final protected void setAccelerator(Handle entry, String accel);

    native static final protected void setToolTip(Handle entry, String tooltip);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
