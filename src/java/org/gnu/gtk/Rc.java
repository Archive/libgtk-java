/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

public class Rc extends MemStruct {

    public void addDefautlFile(String filename) {
        gtk_rc_add_default_file(filename);
    }

    public void setDefaultFiles(String[] files) {
        gtk_rc_set_default_files(files);
    }

    public String[] getDefaultFiles() {
        return gtk_rc_get_default_files();
    }

    public Style getStyle(Widget widget) {
        return Style.getStyle(gtk_rc_get_style(widget.getHandle()));
    }

    public void parse(String filename) {
        gtk_rc_parse(filename);
    }

    public void parseString(String rcStr) {
        gtk_rc_parse_string(rcStr);
    }

    public String findModuleInPath(String modulefile) {
        return gtk_rc_find_module_in_path(modulefile);
    }

    public String getThemeDir() {
        return gtk_rc_get_theme_dir();
    }

    public String getModuleDir() {
        return gtk_rc_get_module_dir();
    }

    public String getIMModulePath() {
        return gtk_rc_get_im_module_path();
    }

    public String getIMModuleFile() {
        return gtk_rc_get_im_module_file();
    }

    static public void resetStyles(Settings settings) {
        gtk_rc_reset_styles(settings.getHandle());
    }

    native static final protected void gtk_rc_add_default_file(String filename);

    native static final protected void gtk_rc_set_default_files(
            String[] filenames);

    native static final protected String[] gtk_rc_get_default_files();

    native static final protected Handle gtk_rc_get_style(Handle widget);

    native static final protected Handle gtk_rc_get_style_by_paths(
            Handle settings, String widget_path, String class_path, int type);

    native static final protected boolean gtk_rc_reparse_all_for_settings(
            Handle settings, boolean forceLoad);

    native static final protected void gtk_rc_reset_styles(Handle settings);

    native static final protected void gtk_rc_parse(String filename);

    native static final protected void gtk_rc_parse_string(String rcString);

    native static final protected boolean gtk_rc_reparse_all();

    native static final protected String gtk_rc_find_module_in_path(
            String moduleFile);

    native static final protected String gtk_rc_get_theme_dir();

    native static final protected String gtk_rc_get_module_dir();

    native static final protected String gtk_rc_get_im_module_path();

    native static final protected String gtk_rc_get_im_module_file();

    /*
     * Deprecated functions. native static final private void
     * gtk_rc_add_widget_name_style(Handle rc_style, String pattern); native
     * static final private void gtk_rc_add_widget_class_style(Handle rc_style,
     * String pattern); native static final private void
     * gtk_rc_add_class_style(Handle rc_style, String pattern);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
