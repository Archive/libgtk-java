/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * Interface for Sortable models used by {@link TreeView}.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeSortable</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface TreeSortable {

    // These methods are implemented in each sortable class, unfortunately.
    // The class TreeSortableHelper has methods that help since Java doesn't
    // have multiple-inheritance.

    /**
     * Get the handle of the object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle getHandle();

    /**
     * Set the column in the model to sort on.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortColumn(DataColumn column, SortType order);

    /**
     * Get a DataColumn object representing the currently sorted column. This is
     * not the same DataColumn used to create the store. It is only of type
     * DataColumn (not DataColumnString, etc). It can be compared with another
     * DataColumn object using the <tt>{@link DataColumn#equals}</tt> method.
     * 
     * @return A DataColumn object representing the currently sorted column or
     *         <tt>null</tt> if the is no column currently sorted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataColumn getSortColumn();

    /**
     * Get the current sorting order of the store.
     * 
     * @return A SortType object defining the current sorting order of the store
     *         or <tt>null</tt> if there is no current sort order.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SortType getSortOrder();

    /**
     * Set the class used to sort the list according to the values stored in the
     * given DataColumn.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortMethod(TreeIterComparison method, DataColumn column);

    /**
     * Call-back method invoked by the JNI code when sorting is required. For
     * internal use only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int handleCompareFunc(Handle model, Handle aIter, Handle bIter,
            int col);

}
