/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A ScrolledWindow is a Bin subclass; it's a container that accepts a single
 * child widget. ScrolledWindow adds scrollbars to the child widget and
 * optionally draws a beveled frame around the child widget.
 * <p>
 * The ScrolledWindow works in two ways. Some Widgets have native scrolling
 * support; these widgets have "slots" for {@link Adjustment} objects.
 * <p>
 * For Widgets that lack native scrolling support the {@link Viewport} Widget
 * acts as an adaptor class, implementing scrollability for child Widgets that
 * lack their own scrolling capability.
 * <p>
 * If a Widget has native scrolling capabilities it can be added with the
 * <code>add</code> method. If a Widget does not, you must first add the
 * Widget to a <code>Viewport</code> and then add the <code>Viewport
 * </code>
 * to the ScrolledWindow. The convenience method <code>
 * addWithViewport</code>
 * does exactly this, so you can ignore the presence of the
 * <code>Viewport</code>.
 * <p>
 * The position of the scrollbars is controlled by the scroll adjustments. See
 * {@link Adjustment} for details on how to determine the position of the
 * layout.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ScrolledWindow</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ScrolledWindow extends Bin {

    /**
     * Create a new ScrolledWindow object. The two arguments are the
     * ScrolledWindow's <code>Adjustments</code>; these will be shared with
     * the scrollbars and the child widgets to keep the bars in sync with the
     * child.
     * 
     * @param hadj
     *            The horizontal Adjustment.
     * @param vadj
     *            The vertical Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ScrolledWindow(Adjustment hadj, Adjustment vadj) {
        super(init(hadj, vadj));
    }

    private static Handle init(Adjustment hadj, Adjustment vadj) {
        Handle horizontal = null;
        Handle vertical = null;
        if (null != hadj) {
            horizontal = hadj.getHandle();
        }
        if (null != vadj) {
            vertical = vadj.getHandle();
        }
        return gtk_scrolled_window_new(horizontal, vertical);
    }

    /**
     * Creates a new ScrolledWindow object. This constuctor generates {@link
     * Adjustment}s automatically.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ScrolledWindow() {
        super(gtk_scrolled_window_new(null, null));
    }

    /**
     * Construct a ScrolledWindow using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ScrolledWindow(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ScrolledWindow getScrolledWindow(Handle handle) {
        if (handle == null)
            return null;

        ScrolledWindow obj = (ScrolledWindow) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ScrolledWindow(handle);

        return obj;
    }

    /**
     * Returns the horizontal Scrollbar's Adjustment. This can be used to
     * connect the horizontal Scrollbar to the child Widget's horizontal scroll
     * functionality.
     * 
     * @return The horizontal Scrollbar's Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getHAdjustment() {
        Handle hndl = gtk_scrolled_window_get_hadjustment(getHandle());
        return Adjustment.getAdjustment(hndl);
    }

    /**
     * Sets the horizontal Scrollbar's Adjustment.
     * 
     * @param hadj
     *            The horizontal Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHAdjustment(Adjustment hadj) {
        gtk_scrolled_window_set_hadjustment(getHandle(), hadj.getHandle());
    }

    /**
     * Returns the vertical Scrollbar's Adjustment. This can be used to connect
     * the vertical Scrollbar to the child Widget's vertical scroll
     * functionality.
     * 
     * @return The vertical Scrollbar's Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getVAdjustment() {
        Handle hndl = gtk_scrolled_window_get_vadjustment(getHandle());
        return Adjustment.getAdjustment(hndl);
    }

    /**
     * Sets the vertical Scrollbar's Adjustment.
     * 
     * @param vadj
     *            The vertical Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVAdjustment(Adjustment vadj) {
        gtk_scrolled_window_set_vadjustment(getHandle(), vadj.getHandle());
    }

    /**
     * Sets the scrollbar policy for the horizontal and vertical scrollbars. The
     * policy determines when the scrollbar should appear.
     * 
     * @param hScrollBarPolicy
     *            The policy for the horizontal ScrollBar.
     * @param vScrollBarPolicy
     *            The policy for the vertical ScrollBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPolicy(PolicyType hScrollBarPolicy,
            PolicyType vScrollBarPolicy) {
        ScrolledWindow.gtk_scrolled_window_set_policy(getHandle(),
                hScrollBarPolicy.getValue(), vScrollBarPolicy.getValue());
    }

    /**
     * Used to add children without native scrolling capability. This is simply
     * a convenience method, it is equivalent to adding the unscrollable child
     * to a <i>Viewport</i>, then adding the <i>Viewport</i> to the
     * ScrolledWindow.
     * 
     * @param child
     *            The Widget to add to the ScrolledWindow.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addWithViewport(Widget child) {
        ScrolledWindow.gtk_scrolled_window_add_with_viewport(getHandle(), child
                .getHandle());
    }

    /**
     * Determines the location of the child widget with respect to the
     * scrollbars. The default is <i>CORNER_TOP_LEFT</i>, meaning the child is
     * in the top left, with the scrollbar underneath and to the right.
     * 
     * @param windowPlacement
     *            The placement for the child widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPlacement(CornerType windowPlacement) {
        ScrolledWindow.gtk_scrolled_window_set_placement(getHandle(),
                windowPlacement.getValue());
    }

    /**
     * Changes the type of shadow drawn around the contents of the
     * ScrolledWindow.
     * 
     * @param type
     *            The type of shadow to draw.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShadowType(ShadowType type) {
        ScrolledWindow.gtk_scrolled_window_set_shadow_type(getHandle(), type
                .getValue());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_scrolled_window_get_type());
    }

    /**
     * Return the horizontal ScrollBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public HScrollBar getHScrollBar() {
        return HScrollBar
                .getHScrollBar(gtk_scrolled_window_get_hscrollbar(getHandle()));
    }

    /**
     * Return the vertical ScrollBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScrollBar getVScrollBar() {
        return VScrollBar
                .getVScrollBar(gtk_scrolled_window_get_vscrollbar(getHandle()));
    }

    native static final protected int gtk_scrolled_window_get_type();

    native static final protected Handle gtk_scrolled_window_new(
            Handle hadjustment, Handle vadjustment);

    native static final protected void gtk_scrolled_window_set_hadjustment(
            Handle scrolled_window, Handle hadjustment);

    native static final protected void gtk_scrolled_window_set_vadjustment(
            Handle scrolled_window, Handle vadjustment);

    native static final protected Handle gtk_scrolled_window_get_hadjustment(
            Handle scrolled_window);

    native static final protected Handle gtk_scrolled_window_get_vadjustment(
            Handle scrolled_window);

    native static final protected void gtk_scrolled_window_set_policy(
            Handle scrolled_window, int hscrollbarPolicy, int vscrollbarPolicy);

    native static final protected void gtk_scrolled_window_get_policy(
            Handle scrolled_window, int[] hscrollbarPolicy,
            int[] vscrollbarPolicy);

    native static final protected void gtk_scrolled_window_set_placement(
            Handle scrolled_window, int windowPlacement);

    native static final protected int gtk_scrolled_window_get_placement(
            Handle scrolled_window);

    native static final protected void gtk_scrolled_window_set_shadow_type(
            Handle scrolled_window, int type);

    native static final protected int gtk_scrolled_window_get_shadow_type(
            Handle scrolled_window);

    native static final protected void gtk_scrolled_window_add_with_viewport(
            Handle scrolled_window, Handle child);

    // new for gtk 2.8
    native static final protected Handle gtk_scrolled_window_get_hscrollbar(
            Handle scrolled_window);

    native static final protected Handle gtk_scrolled_window_get_vscrollbar(
            Handle scrolled_window);

}
