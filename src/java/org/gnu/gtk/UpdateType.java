/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Controls how widgets should be updated
 * 
 * @see Range
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.UpdateType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class UpdateType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _CONTINUOUS = 0;

    /**
     * Anytime the range slider is moved, the range value will change and the
     * value_changed signal will be emitted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.UpdateType CONTINUOUS = new org.gnu.gtk.UpdateType(
            _CONTINUOUS);

    static final private int _DISCONTINUOUS = 1;

    /**
     * The value will only be updated when the user releases the button and ends
     * the slider drag operation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.UpdateType DISCONTINUOUS = new org.gnu.gtk.UpdateType(
            _DISCONTINUOUS);

    static final private int _DELAYED = 2;

    /**
     * The value will be updated after a brief timeout where no slider motion
     * occurs, so updates are spaced by a short time rather than continuous.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.UpdateType DELAYED = new org.gnu.gtk.UpdateType(
            _DELAYED);

    static final private org.gnu.gtk.UpdateType[] theInterned = new org.gnu.gtk.UpdateType[] {
            CONTINUOUS, DISCONTINUOUS, DELAYED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.UpdateType theSacrificialOne = new org.gnu.gtk.UpdateType(
            0);

    static public org.gnu.gtk.UpdateType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.UpdateType already = (org.gnu.gtk.UpdateType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.UpdateType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private UpdateType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.UpdateType or(org.gnu.gtk.UpdateType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.UpdateType and(org.gnu.gtk.UpdateType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.UpdateType xor(org.gnu.gtk.UpdateType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.UpdateType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
