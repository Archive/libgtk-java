/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ComboBoxEntry</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ComboBoxEntry extends ComboBox {

    // for libglade object creation
    public ComboBoxEntry(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ComboBoxEntry getComboBoxEntry(Handle handle) {
        if (handle == null) {
            return null;
        }

        ComboBoxEntry obj = (ComboBoxEntry) GObject
                .getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new ComboBoxEntry(handle);
        }

        return obj;
    }

    /**
     * Construct a new ComboBoxEntry that will contain only Strings. If you need
     * to include objects other than strings you must use the constructor that
     * takes a TreeModel. When you use this constructor you should use the
     * appendText, insertText, prependText, and removeText methods to add or
     * remove text from the comboBox.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ComboBoxEntry() {
        super(gtk_combo_box_entry_new_text());
    }

    /**
     * Create a new ComboBoxEntry with the provided model. If you use this
     * constructor you should not use the appendText, insertText, prependText,
     * or removeText methods. You should update the model when you need to
     * change the values in the ComboBox.
     * 
     * @param model
     * @param textColumn
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ComboBoxEntry(TreeModel model, int textColumn) {
        super(gtk_combo_box_entry_new_with_model(model.getHandle(), textColumn));
    }

    /**
     * Set the model column which the ComboBoxEntry should use to get the
     * strings.
     * 
     * @param textColumn
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTextColumn(int textColumn) {
        gtk_combo_box_entry_set_text_column(getHandle(), textColumn);
    }

    /**
     * Returns the column from the model used by this widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTextColumn() {
        return gtk_combo_box_entry_get_text_column(getHandle());
    }

    native static final protected int gtk_combo_box_entry_get_type();

    native static final protected Handle gtk_combo_box_entry_new();

    native static final protected Handle gtk_combo_box_entry_new_with_model(
            Handle model, int textColumn);

    native static final protected void gtk_combo_box_entry_set_text_column(
            Handle entry, int textColumn);

    native static final protected int gtk_combo_box_entry_get_text_column(
            Handle entry);

    native static final protected Handle gtk_combo_box_entry_new_text();

}
