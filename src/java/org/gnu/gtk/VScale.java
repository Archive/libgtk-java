/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;
import org.gnu.glib.Type;

/**
 * The VScale widget is a vertically slot containing a slider that can be moved
 * by the mouse.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.VScale</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class VScale extends Scale {
    /**
     * Creates a new VScale
     * 
     * @param adjustment
     *            The {@link Adjustment} which sets the range of the scale.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScale(Adjustment adjustment) {
        super(gtk_vscale_new(adjustment.getHandle()));
    }

    /**
     * Creates a new vertical scale widget that lets the user input a number
     * between min and max (including min and max) with the increment step. Step
     * must be nonzero; it's the distance the slider moves when using the arrow
     * keys to adjust the scale value.
     * 
     * @param min
     *            Minimum value
     * @param max
     *            Maximum value
     * @param step
     *            Step increment (tick size) used with keyboard shortcuts
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScale(double min, double max, double step) {
        super(gtk_vscale_new_with_range(min, max, step));
    }

    /**
     * Construct a VScale using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScale(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static VScale getVScale(Handle handle) {
        if (handle == null)
            return null;

        VScale obj = (VScale) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new VScale(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_vscale_get_type());
    }

    native static final protected int gtk_vscale_get_type();

    native static final protected Handle gtk_vscale_new(Handle adjustment);

    native static final protected Handle gtk_vscale_new_with_range(double min,
            double max, double step);
}
