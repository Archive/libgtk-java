/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Class for filtering the visible rows of a base {@link TreeModel}. This class
 * allows the underlying TreeModel to be filtered in 2 ways:
 * <ol>
 * <li><tt>{@link #setVisibleColumn}</tt> - designates a boolean data column
 * in the underlying TreeModel that serves as "visible" marker each row. If the
 * column's value is true, the row will be visible. If the column's value is
 * false, the row will not be visible.</li>
 * <li><tt>{@link #setVisibleMethod}</tt> - designates a special class which
 * can determine if a given row should be visible.</li>
 * </ol>
 * Once a visible column or method has been set, their is no way of unsetting
 * it.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeModelFilter</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TreeModelFilter extends TreeModel {
    private TreeModelFilterVisibleMethod visibleFunc = null;

    /** For internal use only. */
    protected TreeModelFilter(Handle handle) {
        super(handle);
    }

    /** For internal use only. */
    protected static TreeModelFilter getTreeModelFilter(Handle handle) {
        if (handle == null)
            return null;

        TreeModelFilter obj = (TreeModelFilter) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TreeModelFilter(handle);

        return obj;
    }

    /**
     * Create a new TreeModelFilter using the given TreeModel as the underlying
     * data model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeModelFilter(TreeModel model) {
        super(gtk_tree_model_filter_new(model.getHandle(), null));
    }

    /**
     * Create a new TreeModelFilter using the given TreeModel as the underlying
     * data model, and using root as the virtual root.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeModelFilter(TreeModel model, TreePath root) {
        super(gtk_tree_model_filter_new(model.getHandle(), root.getHandle()));
    }

    /**
     * Designate the given TreeModelFilterVisibleMethod object with the
     * responsibility of determining if the rows in the underlying TreeModel
     * should be visible.
     * <p>
     * <b>NOTE</b>: There is no unset method.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibleMethod(TreeModelFilterVisibleMethod method) {
        visibleFunc = method;
        gtk_tree_model_filter_set_visible_func(getHandle(), this,
                "handleVisibleFunc");
    }

    protected boolean handleVisibleFunc(Handle model, Handle iter) {
        TreeModel mod = TreeModel.getTreeModel(model);
        TreeIter it = TreeIter.getTreeIter(iter, mod);
        return visibleFunc.filter(mod, it);
    }

    /**
     * Designate a boolean data column in the underlying TreeModel that will
     * serve as the "visible" marker for each row. If the column's value is
     * true, the row will be visible. If the column's value is false, the row
     * will not be visible.
     * <p>
     * <b>NOTE</b>: There is no unset method.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibleColumn(DataColumnBoolean column) {
        gtk_tree_model_filter_set_visible_column(getHandle(), column
                .getColumn());
    }

    /**
     * Return the underlying data model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeModel getModel() {
        return (TreeModel) getGObjectFromHandle(gtk_tree_model_filter_get_model(getHandle()));
    }

    /**
     * Re-evaluate the visiblity of each row in the underlying data model. This
     * should be called after changing the values in the underlying data model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void refilter() {
        gtk_tree_model_filter_refilter(getHandle());
    }

    /**
     * Clear the filter of any cached iterators. This should almost never be
     * called.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clearCache() {
        gtk_tree_model_filter_clear_cache(getHandle());
    }

    /**
     * Convert the given child TreePath to a path relative to this filter.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreePath convertChildPathToPath(TreePath child) {
        Handle path = gtk_tree_model_filter_convert_child_path_to_path(
                getHandle(), child.getHandle());
        return TreePath.getTreePath(path);
    }

    /**
     * Convert the given TreePath to a path relative to the child model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreePath convertPathToChildPath(TreePath path) {
        Handle child = gtk_tree_model_filter_convert_path_to_child_path(
                getHandle(), path.getHandle());
        return TreePath.getTreePath(child);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_tree_model_filter_get_type());
    }

    native private static Handle gtk_tree_model_filter_new(Handle model,
            Handle treepath_root);

    native private static void gtk_tree_model_filter_set_visible_func(
            Handle filter_handle, TreeModelFilter filter, String callback);

    native private static void gtk_tree_model_filter_set_visible_column(
            Handle filter, int column);

    native private static Handle gtk_tree_model_filter_get_model(Handle filter);

    native private static void gtk_tree_model_filter_refilter(Handle filter);

    native private static void gtk_tree_model_filter_clear_cache(Handle filter);

    native private static Handle gtk_tree_model_filter_convert_child_path_to_path(
            Handle filter, Handle child_path);

    native private static Handle gtk_tree_model_filter_convert_path_to_child_path(
            Handle filter, Handle filter_path);

    native private static int gtk_tree_model_filter_get_type();

    // Not implemented.
    // native private static gtk_tree_model_filter_set_modify_func
    // native private static void
    // gtk_tree_model_filter_convert_child_iter_to_iter( Handle filter, Handle
    // filter_iter, Handle child_iter );
    // native private static void
    // gtk_tree_model_filter_convert_iter_to_child_iter( Handle filter, Handle
    // filter_iter, Handle child_iter );
}
