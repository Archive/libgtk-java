/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Used to dictate the style that a ButtonBox uses to layout the buttons it
 * contains.
 * 
 * <pre>
 *  GTK_BUTTONBOX_DEFAULT_STYLE
 *  Default packing.
 *  GTK_BUTTONBOX_SPREAD
 *  Buttons are evenly spread across the ButtonBox.
 *  GTK_BUTTONBOX_EDGE
 *  Buttons are placed at the edges of the ButtonBox.
 *  GTK_BUTTONBOX_START
 *  Buttons are grouped towards the start of box, (on the left for a HBox, or the top for a VBox).
 *  GTK_BUTTONBOX_END
 *  Buttons are grouped towards the end of a box, (on the right for a HBox, or the bottom for a VBox).
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ButtonBoxStyle</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ButtonBoxStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _DEFAULT_STYLE = 0;

    static final public org.gnu.gtk.ButtonBoxStyle DEFAULT_STYLE = new org.gnu.gtk.ButtonBoxStyle(
            _DEFAULT_STYLE);

    static final private int _SPREAD = 1;

    static final public org.gnu.gtk.ButtonBoxStyle SPREAD = new org.gnu.gtk.ButtonBoxStyle(
            _SPREAD);

    static final private int _EDGE = 2;

    static final public org.gnu.gtk.ButtonBoxStyle EDGE = new org.gnu.gtk.ButtonBoxStyle(
            _EDGE);

    static final private int _START = 3;

    static final public org.gnu.gtk.ButtonBoxStyle START = new org.gnu.gtk.ButtonBoxStyle(
            _START);

    static final private int _END = 4;

    static final public org.gnu.gtk.ButtonBoxStyle END = new org.gnu.gtk.ButtonBoxStyle(
            _END);

    static final private org.gnu.gtk.ButtonBoxStyle[] theInterned = new org.gnu.gtk.ButtonBoxStyle[] {
            DEFAULT_STYLE, SPREAD, EDGE, START, END }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ButtonBoxStyle theSacrificialOne = new org.gnu.gtk.ButtonBoxStyle(
            0);

    static public org.gnu.gtk.ButtonBoxStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ButtonBoxStyle already = (org.gnu.gtk.ButtonBoxStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ButtonBoxStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ButtonBoxStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ButtonBoxStyle or(org.gnu.gtk.ButtonBoxStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ButtonBoxStyle and(org.gnu.gtk.ButtonBoxStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ButtonBoxStyle xor(org.gnu.gtk.ButtonBoxStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ButtonBoxStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
