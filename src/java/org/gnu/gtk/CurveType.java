/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class CurveType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LINEAR = 0;

    static final public org.gnu.gtk.CurveType LINEAR = new org.gnu.gtk.CurveType(
            _LINEAR);

    static final private int _SPLINE = 1;

    static final public org.gnu.gtk.CurveType SPLINE = new org.gnu.gtk.CurveType(
            _SPLINE);

    static final private int _FREE = 2;

    static final public org.gnu.gtk.CurveType FREE = new org.gnu.gtk.CurveType(
            _FREE);

    static final private org.gnu.gtk.CurveType[] theInterned = new org.gnu.gtk.CurveType[] {
            LINEAR, SPLINE, FREE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.CurveType theSacrificialOne = new org.gnu.gtk.CurveType(
            0);

    static public org.gnu.gtk.CurveType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.CurveType already = (org.gnu.gtk.CurveType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.CurveType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CurveType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.CurveType or(org.gnu.gtk.CurveType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.CurveType and(org.gnu.gtk.CurveType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.CurveType xor(org.gnu.gtk.CurveType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.CurveType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
