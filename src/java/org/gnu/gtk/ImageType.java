/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Describes the image data represented by a {@link Image}.
 * 
 * <pre>
 *  EMPTY
 *  There is no Image displayed by the widget.
 *  PIXMAP
 *  The widget contains a {@link org.gnu.gdk.Pixmap}
 *  IMAGE
 *  The widget contains a {@link org.gnu.gdk.Image}
 *  PIXBUF
 *  The widget contains a {@link org.gnu.gdk.Pixbuf}
 *  STOCK
 *  The widget contains a stock icon name.
 *  ICON_SET
 *  The widget contains a {@link org.gnu.gtk.IconSet}
 *  ANIMATION
 *  The widget contains a {@link org.gnu.gdk.PixbufAnimation}
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ImageType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ImageType extends Enum {

    static final private int _EMPTY = 0;

    static final public org.gnu.gtk.ImageType EMPTY = new org.gnu.gtk.ImageType(
            _EMPTY);

    static final private int _PIXMAP = 1;

    static final public org.gnu.gtk.ImageType PIXMAP = new org.gnu.gtk.ImageType(
            _PIXMAP);

    static final private int _IMAGE = 2;

    static final public org.gnu.gtk.ImageType IMAGE = new org.gnu.gtk.ImageType(
            _IMAGE);

    static final private int _PIXBUF = 3;

    static final public org.gnu.gtk.ImageType PIXBUF = new org.gnu.gtk.ImageType(
            _PIXBUF);

    static final private int _STOCK = 4;

    static final public org.gnu.gtk.ImageType STOCK = new org.gnu.gtk.ImageType(
            _STOCK);

    static final private int _ICON_SET = 5;

    static final public org.gnu.gtk.ImageType ICON_SET = new org.gnu.gtk.ImageType(
            _ICON_SET);

    static final private int _ANIMATION = 6;

    static final public org.gnu.gtk.ImageType ANIMATION = new org.gnu.gtk.ImageType(
            _ANIMATION);

    static final private int _ICON_NAME = 6;

    static final public org.gnu.gtk.ImageType ICON_NAME = new org.gnu.gtk.ImageType(
            _ICON_NAME);

    static final private org.gnu.gtk.ImageType[] theInterned = new org.gnu.gtk.ImageType[] {
            EMPTY, PIXMAP, IMAGE, PIXBUF, STOCK, ICON_SET, ANIMATION, ICON_NAME }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ImageType theSacrificialOne = new org.gnu.gtk.ImageType(
            0);

    static public org.gnu.gtk.ImageType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ImageType already = (org.gnu.gtk.ImageType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ImageType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ImageType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ImageType or(org.gnu.gtk.ImageType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ImageType and(org.gnu.gtk.ImageType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ImageType xor(org.gnu.gtk.ImageType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ImageType other) {
        return (value_ & other.value_) == other.value_;
    }

}
