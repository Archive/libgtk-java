/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Represents directional movements within a menu.
 * 
 * <pre>
 *  PARENT
 *  To the parent menu shell
 *  CHILD
 *  To the submenu, if any, associated with the menu
 *  NEXT
 *  To the next menu item
 *  PREV
 *  To the previous menu item
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MenuDirectionType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class MenuDirectionType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _PARENT = 0;

    static final public org.gnu.gtk.MenuDirectionType PARENT = new org.gnu.gtk.MenuDirectionType(
            _PARENT);

    static final private int _CHILD = 1;

    static final public org.gnu.gtk.MenuDirectionType CHILD = new org.gnu.gtk.MenuDirectionType(
            _CHILD);

    static final private int _NEXT = 2;

    static final public org.gnu.gtk.MenuDirectionType NEXT = new org.gnu.gtk.MenuDirectionType(
            _NEXT);

    static final private int _PREV = 3;

    static final public org.gnu.gtk.MenuDirectionType PREV = new org.gnu.gtk.MenuDirectionType(
            _PREV);

    static final private org.gnu.gtk.MenuDirectionType[] theInterned = new org.gnu.gtk.MenuDirectionType[] {
            PARENT, CHILD, NEXT, PREV };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.MenuDirectionType theSacrificialOne = new org.gnu.gtk.MenuDirectionType(
            0);

    static public org.gnu.gtk.MenuDirectionType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.MenuDirectionType already = (org.gnu.gtk.MenuDirectionType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.MenuDirectionType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private MenuDirectionType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.MenuDirectionType or(org.gnu.gtk.MenuDirectionType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.MenuDirectionType and(org.gnu.gtk.MenuDirectionType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.MenuDirectionType xor(org.gnu.gtk.MenuDirectionType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.MenuDirectionType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
