/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRendererCombo</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRendererCombo extends CellRendererText {

    public CellRendererCombo() {
        super(gtk_cell_renderer_combo_new());
    }

    public TreeModel getModel() {
        return TreeModel.getTreeModel(getModel(getHandle()));
    }

    public void setModel(TreeModel model) {
        setModel(getHandle(), model.getHandle());
    }

    public int getTextColumn() {
        return getTextColumn(getHandle());
    }

    public void setTextColumn(int column) {
        setTextColumn(getHandle(), column);
    }

    public boolean getHasEntry() {
        return getHasEntry(getHandle());
    }

    public void setHasEntry(boolean hasEntry) {
        setHasEntry(getHandle(), hasEntry);
    }

    public static class Attribute extends CellRenderer.Attribute {
	    private Attribute(String attr) {
		    super(attr);
	    }
	   
	    /**
	     * (boolean)
	     * If TRUE, the cell renderer will include an entry and
	     * allow to enter values other than the ones in the
	     * popup list.
	     * Default value: TRUE
	     */
	    public static final Attribute HAS_ENTRY   = new Attribute("has-entry");
	
	    /**
	     * (TreeModel)
	     * Holds a tree model containing the possible values for
	     * the combo box. Use the text_column property to specify
	     * the column holding the values.
	     */
	    public static final Attribute MODEL       = new Attribute("model");
	    
	    /**
	     * (int)
	     * Specifies the model column which holds the possible
	     * values for the combo box. Note that this refers to the
	     * model specified in the model property, not the model 
	     * backing the tree view to which this cell renderer is 
	     * attached.
	     * GtkCellRendererCombo automatically adds a text cell 
	     * renderer for this column to its combo box. 
	     * Allowed values: >= -1
	     * Default value: -1
	     */
	    public static final Attribute TEXT_COLUMN = new Attribute("text-column");
    }
    
    native static final protected Handle getModel(Handle renderer);

    native static final protected void setModel(Handle renderer, Handle model);

    native static final protected int getTextColumn(Handle renderer);

    native static final protected void setTextColumn(Handle renderer,
            int textColumn);

    native static final protected boolean getHasEntry(Handle renderer);

    native static final protected void setHasEntry(Handle renderer,
            boolean hasEntry);

    native static final protected int gtk_cell_renderer_combo_get_type();

    native static final protected Handle gtk_cell_renderer_combo_new();

}
