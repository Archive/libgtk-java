/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * This class can influence the placement of a window. Note that only {@link
 * #CENTER_ALWAYS} will affect the window placement once the window is visible.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.WindowPosition</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */

public class WindowPosition extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    /** No influence is made on placement. */
    static final public org.gnu.gtk.WindowPosition NONE = new org.gnu.gtk.WindowPosition(
            _NONE);

    static final private int _CENTER = 1;

    /** Windows should be placed in the center of the screen. */
    static final public org.gnu.gtk.WindowPosition CENTER = new org.gnu.gtk.WindowPosition(
            _CENTER);

    static final private int _MOUSE = 2;

    /** Windows should be placed at the current mouse position. */
    static final public org.gnu.gtk.WindowPosition MOUSE = new org.gnu.gtk.WindowPosition(
            _MOUSE);

    static final private int _CENTER_ALWAYS = 3;

    /**
     * Keep windows centered as it changes size. This will move the window if it
     * is applied to a visible window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.WindowPosition CENTER_ALWAYS = new org.gnu.gtk.WindowPosition(
            _CENTER_ALWAYS);

    static final private int _CENTER_ON_PARENT = 4;

    /** Center the window on its transient parent. */
    static final public org.gnu.gtk.WindowPosition CENTER_ON_PARENT = new org.gnu.gtk.WindowPosition(
            _CENTER_ON_PARENT);

    static final private org.gnu.gtk.WindowPosition[] theInterned = new org.gnu.gtk.WindowPosition[] {
            NONE, CENTER, MOUSE, CENTER_ALWAYS, CENTER_ON_PARENT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.WindowPosition theSacrificialOne = new org.gnu.gtk.WindowPosition(
            0);

    static public org.gnu.gtk.WindowPosition intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.WindowPosition already = (org.gnu.gtk.WindowPosition) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.WindowPosition(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowPosition(int value) {
        value_ = value;
    }

    public org.gnu.gtk.WindowPosition or(org.gnu.gtk.WindowPosition other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.WindowPosition and(org.gnu.gtk.WindowPosition other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.WindowPosition xor(org.gnu.gtk.WindowPosition other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.WindowPosition other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
