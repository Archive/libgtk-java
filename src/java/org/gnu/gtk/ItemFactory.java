/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.gtk.event.MouseEvent;
import org.gnu.glib.Handle;

/**
 * Old class replaced by {@link UIManager}. This class is provided for
 * compatibility only - it will be removed in future releases of java-gnome. Do
 * not use in new code.
 * 
 * @deprecated 2.3
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ItemFactory</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ItemFactory extends GtkObject {

    /**
     * Create a new ItemFactory object.
     * 
     * @param containerType
     *            The type of menu to create. It can be a MenuBar, a Menu, or an
     *            OptionMenu.
     * @param path
     *            The factory path of the new item factory, a string of the form "<name>".
     * @param accelGroup
     *            A AccelGroup to which the accelerators to the menu items will
     *            be added, or null to create a new one.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ItemFactory(Type containerType, String path, AccelGroup accelGroup) {
        super(init(containerType, path, accelGroup));
    }

    /**
     * Create a new ItemFactory from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected ItemFactory(Handle handle) {
        super(handle);
    }

    static ItemFactory getItemFactory(Handle handle) {
        if (handle == null)
            return null;

        ItemFactory obj = (ItemFactory) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new ItemFactory(handle);

        return obj;
    }

    private static Handle init(Type containerType, String path,
            AccelGroup accelGroup) {
        Handle accelHandle = getNullHandle();
        if (null != accelGroup)
            accelHandle = accelGroup.getHandle();
        return gtk_item_factory_new(containerType.getTypeHandle(), path,
                accelHandle);
    }

    /**
     * Obtain the item factory from which a widget was created.
     * 
     * @param widget
     *            The widget to use for the search.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ItemFactory fromWidget(Widget widget) {
        return ItemFactory.getItemFactory(gtk_item_factory_from_widget(widget
                .getHandle()));
    }

    /**
     * Create an item for <i>entry</i>.
     * 
     * @param entry
     *            The IconFactoryEntry to create an item for.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void createItem(ItemFactoryEntry entry) {
        // create the item
        gtk_item_factory_create_item(getHandle(), entry.getHandle(), null, 0);
        // now add the listener to the item that was created.
        // REDTAG
    }

    /**
     * Create the items from the entries.
     * 
     * @param entries
     *            An array of IconFactoryEntry objects that describe the menus
     *            to be created.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void createItems(ItemFactoryEntry[] entries) {
        Handle[] handles = new Handle[entries.length];
        // get the handles for all of the entries
        for (int i = 0; i < entries.length; i++) {
            handles[i] = entries[i].getHandle();
        }
        // create the items
        gtk_item_factory_create_items(getHandle(), handles.length, handles, 0);
        // Now add the listener to the item that was created.
        // REDTAG
    }

    /**
     * Delete the menu item that was created for <i>path</i> by the item
     * factory.
     * 
     * @param path
     *            The path to the item to delete.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deleteItem(String path) {
        gtk_item_factory_delete_item(getHandle(), path);
    }

    /**
     * Delete the menu item that was created from <i>entry</i> by the item
     * factory.
     * 
     * @param entry
     *            The Entry to delete.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deleteEntry(ItemFactoryEntry entry) {
        gtk_item_factory_delete_entry(getHandle(), entry.getHandle());
    }

    /**
     * Delete the menu items which were created from the <i>entries</i> by the
     * item factory.
     * 
     * @param entries
     *            The entries to delete.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deleteEntries(ItemFactoryEntry[] entries) {
        Handle[] handles = new Handle[entries.length];
        for (int i = 0; i < entries.length; i++)
            handles[i] = entries[i].getHandle();
        gtk_item_factory_delete_entries(getHandle(), handles.length, handles);
    }

    /**
     * Obtain the menu item that corresponds to <i>path</i>.
     * 
     * @param path
     *            The path to the menu item.
     * @return The menu item for the given path or null if path doesn't exist.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getItem(String path) {
        Handle hndl = gtk_item_factory_get_item(getHandle(), path);
        return Widget.getWidget(hndl);
    }

    /**
     * Obtain the widget that corresponds to <i>path</i>.
     * 
     * @param path
     *            The path to the widget
     * @return The widget for the given path or null if path doesn't exist.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getWidget(String path) {
        Handle hndl = gtk_item_factory_get_widget(getHandle(), path);
        return Widget.getWidget(hndl);
    }

    /**
     * Pops up the menu constructed with the item factory.
     * 
     * @param x
     *            The x coordinate for the popup menu
     * @param y
     *            The y coordinate for the popup menu
     * @param mouseButton
     *            The mouse button which was pressed. These values are defined
     *            in {@link MouseEvent}.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void popup(int x, int y, int mouseButton) {
        if (mouseButton != MouseEvent.BUTTON1
                && mouseButton != MouseEvent.BUTTON2
                && mouseButton != MouseEvent.BUTTON3)
            return;
        gtk_item_factory_popup(getHandle(), x, y, mouseButton, 0);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_item_factory_get_type());
    }

    native static final protected int gtk_item_factory_get_type();

    native static final protected Handle gtk_item_factory_new(
            int containerType, String path, Handle accelGroup);

    native static final protected void gtk_item_factory_construct(
            Handle ifactory, int containerType, String path, Handle accelGroup);

    native static final protected void gtk_item_factory_add_foreign(
            Handle accelWidget, String fullPath, Handle accelGroup, int keyval,
            int modifiers);

    native static final protected Handle gtk_item_factory_from_widget(
            Handle widget);

    native static final protected String gtk_item_factory_path_from_widget(
            Handle widget);

    native static final protected Handle gtk_item_factory_get_item(
            Handle ifactory, String path);

    native static final protected Handle gtk_item_factory_get_widget(
            Handle ifactory, String path);

    native static final protected Handle gtk_item_factory_get_widget_by_action(
            Handle ifactory, Handle action);

    native static final protected Handle gtk_item_factory_get_item_by_action(
            Handle ifactory, Handle action);

    native static final protected void gtk_item_factory_create_item(
            Handle ifactory, Handle entry, Object callbackData, int callbackType);

    native static final protected void gtk_item_factory_create_items(
            Handle ifactory, int numEntries, Handle[] entries, int callbackData);

    native static final protected void gtk_item_factory_delete_item(
            Handle ifactory, String path);

    native static final protected void gtk_item_factory_delete_entry(
            Handle ifactory, Handle entry);

    native static final protected void gtk_item_factory_delete_entries(
            Handle ifactory, int numEnties, Handle[] entries);

    native static final protected void gtk_item_factory_popup(Handle ifactory,
            int x, int y, int mouseButton, int time);

}
