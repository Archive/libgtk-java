/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

public interface TreeDragDestListener {

    public boolean dragDataReceived(TreeDragDest destination, TreePath path,
            SelectionData selectionData);

    public boolean isRowDropPossible(TreeDragDest destingation, TreePath path,
            SelectionData selectionData);
}
