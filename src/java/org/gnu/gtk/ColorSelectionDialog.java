/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The ColorSelectionDialog provides a standard dialog which allows the user to
 * select a color much like the {@link FileSelection} provides a standard dialog
 * for file selection.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ColorSelectionDialog</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ColorSelectionDialog extends Dialog {
    /**
     * Construct a new ColorSelectionDialog.
     * 
     * @param title
     *            The text to display on the title bar of the dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorSelectionDialog(String title) {
        super(ColorSelectionDialog.gtk_color_selection_dialog_new(title));
    }

    /**
     * Construct a ColorSelectionDialog using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorSelectionDialog(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ColorSelectionDialog getColorSelectionDialog(Handle handle) {
        if (handle == null)
            return null;

        ColorSelectionDialog obj = (ColorSelectionDialog) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ColorSelectionDialog(handle);

        return obj;
    }

    /**
     * Return the OK Button widget for this Dialog.
     * 
     * @return The OK Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getOKButton() {
        Handle hndl = ColorSelectionDialog.getOkButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Cancel Button widget for this dialog.
     * 
     * @return The Cancel Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getCancelButton() {
        Handle hndl = ColorSelectionDialog.getCancelButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Help Button widget for this dialog.
     * 
     * @return The Help Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getHelpButton() {
        Handle hndl = ColorSelectionDialog.getHelpButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the ColorSelection widget for this dialog.
     * 
     * @return The ColorSelection widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorSelection getColorSelectionWidget() {
        Handle hndl = ColorSelectionDialog.getColorsel(getHandle());
        return ColorSelection.getColorSelection(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_color_selection_dialog_get_type());
    }

    native static final protected Handle getColorsel(Handle cptr);

    native static final protected Handle getOkButton(Handle cptr);

    native static final protected Handle getCancelButton(Handle cptr);

    native static final protected Handle getHelpButton(Handle cptr);

    native static final protected int gtk_color_selection_dialog_get_type();

    native static final protected Handle gtk_color_selection_dialog_new(
            String title);

}
