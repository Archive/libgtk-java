/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Determines when a scrollbar is visible.
 * 
 * <pre>
 *  ALWAYS
 *  The scrollbar is always visible
 *  AUTOMATIC
 *  The scrollbar will appear and disappear as needed
 *  NEVER
 *  The scrollbar will never appear
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.PolicyType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class PolicyType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ALWAYS = 0;

    static final public org.gnu.gtk.PolicyType ALWAYS = new org.gnu.gtk.PolicyType(
            _ALWAYS);

    static final private int _AUTOMATIC = 1;

    static final public org.gnu.gtk.PolicyType AUTOMATIC = new org.gnu.gtk.PolicyType(
            _AUTOMATIC);

    static final private int _NEVER = 2;

    static final public org.gnu.gtk.PolicyType NEVER = new org.gnu.gtk.PolicyType(
            _NEVER);

    static final private org.gnu.gtk.PolicyType[] theInterned = new org.gnu.gtk.PolicyType[] {
            ALWAYS, AUTOMATIC, NEVER };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.PolicyType theSacrificialOne = new org.gnu.gtk.PolicyType(
            0);

    static public org.gnu.gtk.PolicyType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.PolicyType already = (org.gnu.gtk.PolicyType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.PolicyType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PolicyType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.PolicyType or(org.gnu.gtk.PolicyType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.PolicyType and(org.gnu.gtk.PolicyType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.PolicyType xor(org.gnu.gtk.PolicyType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.PolicyType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
