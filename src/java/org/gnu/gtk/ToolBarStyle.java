/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class ToolBarStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ICONS = 0;

    static final public org.gnu.gtk.ToolBarStyle ICONS = new org.gnu.gtk.ToolBarStyle(
            _ICONS);

    static final private int _TEXT = 1;

    static final public org.gnu.gtk.ToolBarStyle TEXT = new org.gnu.gtk.ToolBarStyle(
            _TEXT);

    static final private int _BOTH = 2;

    static final public org.gnu.gtk.ToolBarStyle BOTH = new org.gnu.gtk.ToolBarStyle(
            _BOTH);

    static final private int _BOTH_HORIZ = 3;

    static final public org.gnu.gtk.ToolBarStyle BOTH_HORIZ = new org.gnu.gtk.ToolBarStyle(
            _BOTH_HORIZ);

    static final private org.gnu.gtk.ToolBarStyle[] theInterned = new org.gnu.gtk.ToolBarStyle[] {
            ICONS, TEXT, BOTH, BOTH_HORIZ }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ToolBarStyle theSacrificialOne = new org.gnu.gtk.ToolBarStyle(
            0);

    static public org.gnu.gtk.ToolBarStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ToolBarStyle already = (org.gnu.gtk.ToolBarStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ToolBarStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ToolBarStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ToolBarStyle or(org.gnu.gtk.ToolBarStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ToolBarStyle and(org.gnu.gtk.ToolBarStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ToolBarStyle xor(org.gnu.gtk.ToolBarStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ToolBarStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
