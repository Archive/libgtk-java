/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The AccelLabel widget is a subclass of Label that also displays an
 * accelerator key on the right side of the label text, e.g. 'Ctrl+S'. It is
 * commonly used in menus to show the keyboard shortcut for the command.
 * <p>
 * The accelerator key to display is not set explicitly. Instead, the AccelLabel
 * displays the accelerator which have been added to a particular widget. This
 * widget is set by calling setAccelWidget().
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.AccelLabel</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class AccelLabel extends Label {
    /**
     * Create an AccelLabel.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelLabel(String str) {
        super(gtk_accel_label_new(str));
    }

    /**
     * Construct a AccelLabel using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelLabel(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AccelLabel getAccelLabel(Handle handle) {
        if (handle == null)
            return null;

        AccelLabel obj = (AccelLabel) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new AccelLabel(handle);

        return obj;
    }

    /**
     * Set the widget for this AccelLabel
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAccelWidget(Widget accelWidget) {
        gtk_accel_label_set_accel_widget(getHandle(), accelWidget.getHandle());
    }

    /**
     * Retrieve the widget for this AccelLabel
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getAccelWidget() {
        Handle hndl = gtk_accel_label_get_accel_widget(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Returns the width needed to display the accelerator key(s). This is used
     * by menus to align all of the MenuItem widgets and shouldn't be needed by
     * applications.
     * 
     * @return The width needed to display the accelerator keys.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getAccelWidth() {
        return gtk_accel_label_get_accel_width(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_accel_label_get_type());
    }

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    native static final protected int gtk_accel_label_get_type();

    native static final protected Handle gtk_accel_label_new(String str);

    native static final protected Handle gtk_accel_label_get_accel_widget(
            Handle accel_label);

    native static final protected void gtk_accel_label_set_accel_widget(
            Handle accel_label, Handle accelWidget);

    native static final protected int gtk_accel_label_get_accel_width(
            Handle accel_label);

    native static final protected boolean gtk_accel_label_refetch(
            Handle accel_label);
    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
