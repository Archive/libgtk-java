/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Screen;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The Invisible widget displays a blank window. It can act as a placeholder.
 * For example, it can be sized with a call to setMinimumSize, and used inside a
 * container to help position other widgets.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Invisible</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Invisible extends Widget {

    /**
     * Construct a new Invisible object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Invisible() {
        super(Invisible.gtk_invisible_new());
    }

    public Invisible(Screen screen) {
        super(gtk_invisible_new_for_screen(screen.getHandle()));
    }

    public void setScreen(Screen screen) {
        gtk_invisible_set_screen(getHandle(), screen.getHandle());
    }

    public Screen getScreen() {
        Handle hndl = gtk_invisible_get_screen(getHandle());
        GObject obj = getGObjectFromHandle(hndl);
        if (null != obj)
            return (Screen) obj;
        return new Screen(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_invisible_get_type());
    }

    native static final protected int gtk_invisible_get_type();

    native static final protected Handle gtk_invisible_new();

    native static final protected Handle gtk_invisible_new_for_screen(
            Handle screen);

    native static final protected void gtk_invisible_set_screen(
            Handle invisible, Handle screen);

    native static final protected Handle gtk_invisible_get_screen(
            Handle invisible);

}
