/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import org.gnu.gdk.EventKey;
import org.gnu.gdk.KeySymbol;
import org.gnu.gdk.ModifierType;
import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Key bindings for individual widgets. TODO: Complete javadocs.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.BindingSet</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class BindingSet extends MemStruct {

    /**
     * Construct a new BindingSet using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private BindingSet(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new BindingSet.
     * 
     * @param setName
     *            The name of the BindingSet
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public BindingSet(String setName) {
        super(gtk_binding_set_new(setName));
    }

    /**
     * Finds a BindingSet object given the name.
     * 
     * @param setName
     *            The name of the BindingSet to find
     * @return A BindingSet object
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public BindingSet findBindingSet(String setName) {
        Handle handle = gtk_binding_set_find(setName);
        return BindingSet.getBindingSet(handle);
    }

    private static BindingSet getBindingSet(Handle handle) {
        if (handle == null) {
            return null;
        }
        BindingSet bSet = (BindingSet) getMemStructFromHandle(handle);
        if (bSet == null) {
            bSet = new BindingSet(handle);
        }

        return bSet;
    }

    /**
     * 
     * @param keyval
     *            The key value for the BindingSet (from
     *            {@link org.gnu.gdk.KeyValue}).
     * @param modifier
     *            The key modifier for the BindingSet
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean activateBindings(int keyval, ModifierType modifier) {
        return gtk_bindings_activate(getHandle(), keyval, modifier.getValue());
    }

    /**
     * 
     * @param keyval
     *            The key value for the BindingSet.
     * @param modifier
     *            The key modifier for the BindingSet
     * @deprecated
     * @see #activateBindings(int, ModifierType)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean activateBindings(KeySymbol keyval, ModifierType modifier) {
        return gtk_bindings_activate(getHandle(), keyval.getValue(), modifier
                .getValue());
    }

    /**
     * 
     * @param keyval
     *            The key value for the BindingSet.
     * @param modifier
     *            The key modifier for the BindingSet
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean activateBindingSet(int keyval, ModifierType modifier) {
        return gtk_binding_set_activate(getHandle(), keyval, modifier
                .getValue(), getHandle());
    }

    /**
     * 
     * @param keyval
     *            The key value for the BindingSet.
     * @param modifier
     *            The key modifier for the BindingSet
     * @deprecated
     * @see #activateBindingSet(int, ModifierType)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean activateBindingSet(KeySymbol keyval, ModifierType modifier) {
        return gtk_binding_set_activate(getHandle(), keyval.getValue(),
                modifier.getValue(), getHandle());
    }

    /**
     * Clears a BindingSet entry.
     * 
     * @param keyval
     *            The key value for the BindingSet.
     * @param modifier
     *            The key modifier for the BindingSet
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clearEntry(int keyval, ModifierType modifier) {
        gtk_binding_entry_clear(getHandle(), keyval, modifier.getValue());
    }

    /**
     * Clears a BindingSet entry.
     * 
     * @param keyval
     *            The key value for the BindingSet.
     * @param modifier
     *            The key modifier for the BindingSet
     * @deprecated
     * @see #clearEntry(int, ModifierType)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clearEntry(KeySymbol keyval, ModifierType modifier) {
        gtk_binding_entry_clear(getHandle(), keyval.getValue(), modifier
                .getValue());
    }

    /**
     * 
     * @param pathType
     * @param pathPattern
     * @param priority
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addPath(PathType pathType, String pathPattern,
            PathPriorityType priority) {
        gtk_binding_set_add_path(getHandle(), pathType.getValue(), pathPattern,
                priority.getValue());
    }

    /**
     * Looks up key bindings for this BindingSet to find one that matches
     * <tt>event</tt>, and if one was found, activate it.
     * 
     * @return TRUE if a matching key binding was found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean activateEvent(EventKey event) {
        return gtk_bindings_activate_event(getHandle(), event.getHandle());
    }

    /**
     * Removes an entry from the BindingSet
     * 
     * @param keyval
     * @param modifier
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeEntry(int keyval, ModifierType modifier) {
        gtk_binding_entry_remove(getHandle(), keyval, modifier.getValue());
    }

    /**
     * @deprecated
     * @see #removeEntry(int, ModifierType)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeEntry(KeySymbol keyval, ModifierType modifier) {
        gtk_binding_entry_remove(getHandle(), keyval.getValue(), modifier
                .getValue());
    }

    native static final protected Handle gtk_binding_set_new(String setName);

    native static final protected Handle gtk_binding_set_find(String setName);

    native static final protected boolean gtk_bindings_activate(Handle object,
            int keyval, int modifier);

    native static final protected boolean gtk_binding_set_activate(
            Handle bindingSet, int keyval, int modifier, Handle object);

    native static final protected void gtk_binding_entry_clear(
            Handle bindingSet, int keyval, int modifier);

    native static final protected void gtk_binding_set_add_path(
            Handle bindingSet, int pathType, String pathPattern, int priority);

    native static final private boolean gtk_bindings_activate_event(
            Handle object, Handle event);

    native static final private void gtk_binding_entry_remove(
            Handle binding_set, int keyval, int modifiers);

}
