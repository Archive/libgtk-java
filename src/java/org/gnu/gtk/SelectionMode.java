/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Determines how elements of a {@link TreeView} widget may be selected. This is
 * set using {@link TreeSelection#setMode(SelectionMode)} on the tree selection
 * of the tree view.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SelectionMode</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SelectionMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final private int _SINGLE = 1;

    static final private int _BROWSE = 2;

    static final private int _MULTIPLE = 3;

    /**
     * No items can be selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.SelectionMode NONE = new org.gnu.gtk.SelectionMode(
            _NONE);

    /**
     * No more than one item can be selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.SelectionMode SINGLE = new org.gnu.gtk.SelectionMode(
            _SINGLE);

    /**
     * Exactly one item is always selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.SelectionMode BROWSE = new org.gnu.gtk.SelectionMode(
            _BROWSE);

    /**
     * Anything between no item and all items can be selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.SelectionMode MULTIPLE = new org.gnu.gtk.SelectionMode(
            _MULTIPLE);

    static final private org.gnu.gtk.SelectionMode[] theInterned = new org.gnu.gtk.SelectionMode[] {
            NONE, SINGLE, BROWSE, MULTIPLE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SelectionMode theSacrificialOne = new org.gnu.gtk.SelectionMode(
            0);

    static public org.gnu.gtk.SelectionMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SelectionMode already = (org.gnu.gtk.SelectionMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SelectionMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SelectionMode(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SelectionMode or(org.gnu.gtk.SelectionMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SelectionMode and(org.gnu.gtk.SelectionMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SelectionMode xor(org.gnu.gtk.SelectionMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SelectionMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
