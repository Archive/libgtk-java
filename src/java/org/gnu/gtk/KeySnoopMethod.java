/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.EventKey;

/**
 * Interface that can be used to intercept all key events before they are
 * delivered normally. It can be used to implement custom key event handling.
 * 
 * @see org.gnu.gtk.Gtk#setKeySnoopMethod
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.KeySnoopMethod</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface KeySnoopMethod {

    /**
     * Method invoked when a key event occurs. This method is called before
     * normal event delivery and can be used to implement custom key event
     * handling.
     * 
     * @param widget
     *            The widget to which the event will be delivered.
     * @param event
     *            The key event.
     * @return TRUE to stop further processing of event, FALSE to continue.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean keyEvent(Widget widget, EventKey event);

}
