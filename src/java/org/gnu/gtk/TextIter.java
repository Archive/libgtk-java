/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * The Text Editor objects are discussed in detail in the {@link TextView}
 * widget overview.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextIter</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextIter extends Boxed {

    protected TextIter(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static TextIter getTextIter(Handle handle) {
        if (handle == null)
            return null;

        TextIter textIter = (TextIter) Boxed.getBoxedFromHandle(handle);
        if (textIter == null)
            textIter = new TextIter(handle);

        return textIter;
    }

    /**
     * Returns the associated buffer
     * 
     * @return associated buffer
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextBuffer getBuffer() {
        return TextBuffer.getTextBuffer(gtk_text_iter_get_buffer(getHandle()));
    }

    /**
     * Returns the character offset of an iterator. Each character in a
     * GtkTextBuffer has an offset, starting with 0 for the first character in
     * the buffer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getOffset() {
        return gtk_text_iter_get_offset(getHandle());
    }

    /**
     * Returns the line number containing the iterator. Lines in a GtkTextBuffer
     * are numbered beginning with 0 for the first line in the buffer.
     * 
     * @return the line number.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLineNumber() {
        return gtk_text_iter_get_line(getHandle());
    }

    /**
     * Returns the character offset of the iterator, counting from the start of
     * a newline-terminated line. The first character on the line has offset 0.
     * 
     * @return Character offset from start of line.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLineOffset() {
        return gtk_text_iter_get_line_offset(getHandle());
    }

    /**
     * Returns the offset in characters from the start of the line to the given
     * iter, not counting characters that are invisible due to tags with the
     * "invisible" flag toggled on.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getVisibleLineOffset() {
        return gtk_text_iter_get_visible_line_offset(getHandle());
    }

    /**
     * Returns the Unicode character at this iterator. (Equivalent to operator*
     * on a C++ iterator.) If the iterator points at a non-character element,
     * such as an image embedded in the buffer, the Unicode "unknown" character
     * 0xFFFC is returned. If invoked on the end iterator, zero is returned;
     * zero is not a valid Unicode character.
     * 
     * @return a Unicode character, or 0 if iter is not dereferenceable
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public char getChar() {
        return gtk_text_iter_get_char(getHandle());
    }

    /**
     * If the location pointed to by iter contains a pixbuf, the pixbuf is
     * returned (with no new reference count added). Otherwise,
     * <code>null</code> is returned.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Pixbuf getPixbuf() {
        if (isPixbuf()) {
            return new org.gnu.gdk.Pixbuf(gtk_text_iter_get_pixbuf(getHandle()));
        } else {
            return null;
        }
    }

    /**
     * Returns true if iter points to a pixbuf.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isPixbuf() {
        return gtk_text_iter_isPixbuf(getHandle());
    }

    /**
     * If the location pointed to by iter contains a child anchor, the anchor is
     * returned (with no new reference count added). Otherwise,
     * <code>null</code> is returned.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextChildAnchor getChildAnchor() {
        if (isChildAnchor()) {
            return TextChildAnchor
                    .getTextChildAnchor(gtk_text_iter_get_child_anchor(getHandle()));
        } else {
            return null;
        }
    }

    /**
     * Returns true is this location is a child anchor.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isChildAnchor() {
        return gtk_text_iter_isChildAnchor(getHandle());
    }

    /**
     * Returns TRUE if tag is toggled on at exactly this point. Note that this
     * returns TRUE if iter is the start of the tagged range; {@link
     * #hasTag(TextTag)} tells you whether an iterator is within a tagged range.
     * 
     * @param tag
     *            A TextTag
     * @return Whether iter is the start of a range tagged with tag
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean beginsTag(TextTag tag) {
        return gtk_text_iter_begins_tag(getHandle(), tag.getHandle());
    }

    /**
     * Returns TRUE if tag is toggled off at exactly this point. Note that this
     * returns TRUE if iter is the end of the tagged range; {@link
     * #hasTag(TextTag)} tells you whether an iterator is within a tagged range.
     * 
     * @param tag
     *            A TextTag
     * @return Whether iter is the end of a range tagged with tag
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean endsTag(TextTag tag) {
        return gtk_text_iter_ends_tag(getHandle(), tag.getHandle());
    }

    /**
     * Returns TRUE if iter is within a range tagged with tag.
     * 
     * @return whether iter is tagged with tag
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean hasTag(TextTag tag) {
        return gtk_text_iter_has_tag(getHandle(), tag.getHandle());
    }

    /**
     * Returns whether the character at iter is within an editable region of
     * text. Non-editable text is "locked" and can't be changed by the user via
     * GtkTextView. If no tags applied to this text affect editability,
     * <code>defaultSetting</code> will be returned.
     * 
     * <p>
     * You don't want to use this function to decide whether text can be
     * inserted at iter, because for insertion you don't want to know whether
     * the char at iter is inside an editable range, you want to know whether a
     * new character inserted at iter would be inside an editable range. Use
     * {@link #getCanInsert(boolean)} to handle this case.
     * 
     * @param defaultSetting
     *            TRUE if text is editable by default
     * @return Whether iter is inside an editable range
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEditable(boolean defaultSetting) {
        return gtk_text_iter_editable(getHandle(), defaultSetting);
    }

    /**
     * Considering the default editability of the buffer, and tags that affect
     * editability, determines whether text inserted at iter would be editable.
     * If text inserted at iter would be editable then the user should be
     * allowed to insert text at iter.
     * 
     * @param defaultSetting
     *            TRUE if text is editable by default
     * @return Whether text inserted at iter would be editable
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getCanInsert(boolean defaultSetting) {
        return gtk_text_iter_can_insert(getHandle(), defaultSetting);
    }

    /**
     * Determines whether iter begins a natural-language word. Word breaks are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango word break algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getStartsWord() {
        return gtk_text_iter_starts_word(getHandle());
    }

    /**
     * Determines whether iter ends a natural-language word. Word breaks are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango word break algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEndsWord() {
        return gtk_text_iter_ends_word(getHandle());
    }

    /**
     * Determines whether iter is inside a natural-language word (as opposed to
     * say inside some whitespace). Word breaks are determined by Pango and
     * should be correct for nearly any language (if not, the correct fix would
     * be to the Pango word break algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getInsideWord() {
        return gtk_text_iter_inside_word(getHandle());
    }

    /**
     * Returns TRUE if iter begins a paragraph, ie if {@link #getLineOffset()}
     * would return 0. However this function is potentially more efficient than
     * {@link #getLineOffset()} because it doesn't have to compute the offset,
     * it just has to see whether it's 0.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getStartsLine() {
        return gtk_text_iter_starts_line(getHandle());
    }

    /**
     * Returns TRUE if iter points to the start of the paragraph delimiter
     * characters for a line (delimiters will be either a newline, a carriage
     * return, a carriage return followed by a newline, or a Unicode paragraph
     * separator character). Note that an iterator pointing to the \n of a \r\n
     * pair will not be counted as the end of a line, the line ends before the
     * \r. The end iterator is considered to be at the end of a line, even
     * though there are no paragraph delimiter chars there.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEndsLine() {
        return gtk_text_iter_ends_line(getHandle());
    }

    /**
     * Determines whether iter begins a sentence. Sentence boundaries are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango text boundary algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getStartsSentence() {
        return gtk_text_iter_starts_sentence(getHandle());
    }

    /**
     * Determines whether iter ends a sentence. Sentence boundaries are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango text boundary algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEndsSentence() {
        return gtk_text_iter_ends_sentence(getHandle());
    }

    /**
     * Determines whether iter is inside a sentence (as opposed to in between
     * two sentences, eg after a period and before the first letter of the next
     * sentence). Sentence boundaries are determined by Pango and should be
     * correct for nearly any language (if not, the correct fix would be to the
     * Pango text boundary algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getInsideSentence() {
        return gtk_text_iter_inside_sentence(getHandle());
    }

    /**
     * @return TRUE if the cursor can be placed at iter
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getIsCursorPosition() {
        return gtk_text_iter_is_cursor_position(getHandle());
    }

    /**
     * Returns the number of characters in the line containing iter, including
     * the paragraph delimiters.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getCharsInLine() {
        return gtk_text_iter_get_chars_in_line(getHandle());
    }

    /**
     * Computes the effect of any tags applied to this spot in the text. The
     * values parameter should be initialized to the default settings you wish
     * to use if no tags are in effect. You'd typically obtain the defaults from
     * {@link TextView#getDefaultAttributes()}.
     * 
     * <p>
     * This will modify values, applying the effects of any tags present at
     * iter. If any tags affected values, the function returns TRUE.
     * 
     * 
     * @param values
     *            A TextAttributes to be filled in
     * @return TRUE if values was modified
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getAttributes(TextAttributes values) {
        return gtk_text_iter_get_attributes(getHandle(), values.getHandle());
    }

    /**
     * A convenience wrapper which returns the language in effect at iter. If no
     * tags affecting language apply to iter, the return value is identical to
     * that of gtk_get_default_language().
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.pango.Language getLanguage() {
        return new org.gnu.pango.Language(
                gtk_text_iter_get_language(getHandle()));
    }

    /**
     * Returns TRUE if iter is the end iterator, ie one past the last
     * dereferenceable iterator in the buffer. This is the most efficient way to
     * check whether an iterator is the end iterator.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getIsEndIter() {
        return gtk_text_iter_is_end(getHandle());
    }

    /**
     * Returns TRUE if iter is the first iterator in the buffer, that is if iter
     * has a character offset of 0.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getIsStartIter() {
        return gtk_text_iter_is_start(getHandle());
    }

    /**
     * Moves iter forward by one character offset. Note that images embedded in
     * the buffer occupy 1 character slot, so gtk_text_iter_forward_char() may
     * actually move onto an image instead of a character, if you have images in
     * your buffer. If iter is the end iterator or one character before it, iter
     * will now point at the end iterator, and gtk_text_iter_forward_char()
     * returns FALSE for convenience when writing loops.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardChar() {
        return gtk_text_iter_forward_char(getHandle());
    }

    /**
     * Moves backward by one character offset. Returns TRUE if movement was
     * possible; if iter was the first in the buffer (character offset 0),
     * gtk_text_iter_backward_char() returns FALSE for convenience when writing
     * loops.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardChar() {
        return gtk_text_iter_backward_char(getHandle());
    }

    /**
     * Moves count characters if possible (if count would move past the start or
     * end of the buffer, moves to the start or end of the buffer). The return
     * value indicates whether the new position of iter is different from its
     * original position, and dereferenceable (the last iterator in the buffer
     * is not dereferenceable). If count is 0, the function does nothing and
     * returns FALSE.
     * 
     * @param count
     *            Number of characters to move, may be negative
     * @return Whether iter moved and is dereferenceable
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardChar(int count) {
        return gtk_text_iter_forward_chars(getHandle(), count);
    }

    /**
     * Moves count characters backward, if possible (if count would move past
     * the start or end of the buffer, moves to the start or end of the buffer).
     * The return value indicates whether the iterator moved onto a
     * dereferenceable position; if the iterator didn't move, or moved onto the
     * end iterator, then FALSE is returned. If count is 0, the function does
     * nothing and returns FALSE.
     * 
     * @param count
     *            Number of characters to move
     * @return Whether iter moved and is dereferenceable
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardChar(int count) {
        return gtk_text_iter_backward_chars(getHandle(), count);
    }

    /**
     * Moves iter to the start of the next line. Returns TRUE if there was a
     * next line to move to, and FALSE if iter was simply moved to the end of
     * the buffer and is now not dereferenceable, or if iter was already at the
     * end of the buffer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardLine() {
        return gtk_text_iter_forward_line(getHandle());
    }

    /**
     * Moves iter to the start of the previous line. Returns TRUE if iter could
     * be moved; i.e. if iter was at character offset 0, this function returns
     * FALSE. Therefore if iter was already on line 0, but not at the start of
     * the line, iter is snapped to the start of the line and the function
     * returns TRUE. (Note that this implies that in a loop calling this
     * function, the line number may not change on every iteration, if your
     * first iteration is on line 0.)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardLine() {
        return gtk_text_iter_backward_line(getHandle());
    }

    /**
     * Moves count lines forward, if possible (if count would move past the
     * start or end of the buffer, moves to the start or end of the buffer). The
     * return value indicates whether the iterator moved onto a dereferenceable
     * position; if the iterator didn't move, or moved onto the end iterator,
     * then FALSE is returned. If count is 0, the function does nothing and
     * returns FALSE. If count is negative, moves backward by 0 - count lines.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardLine(int count) {
        return gtk_text_iter_forward_lines(getHandle(), count);
    }

    /**
     * Moves count lines backward, if possible (if count would move past the
     * start or end of the buffer, moves to the start or end of the buffer). The
     * return value indicates whether the iterator moved onto a dereferenceable
     * position; if the iterator didn't move, or moved onto the end iterator,
     * then FALSE is returned. If count is 0, the function does nothing and
     * returns FALSE. If count is negative, moves forward by 0 - count lines.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardLine(int count) {
        return gtk_text_iter_backward_lines(getHandle(), count);
    }

    /**
     * Calls {@link #moveForwardWordEnd()} up to <code>count</code> times.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardWordEnd(int count) {
        return gtk_text_iter_forward_word_ends(getHandle(), count);
    }

    /**
     * Calls {@link #moveBackwardWordStart()} up to <code>count</code> times.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardWordStart(int count) {
        return gtk_text_iter_backward_word_starts(getHandle(), count);
    }

    /**
     * Moves forward to the next word end. (If iter is currently on a word end,
     * moves forward to the next one after that.) Word breaks are determined by
     * Pango and should be correct for nearly any language (if not, the correct
     * fix would be to the Pango word break algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardWordEnd() {
        return gtk_text_iter_forward_word_end(getHandle());
    }

    /**
     * Moves backward to the previous word start. (If iter is currently on a
     * word start, moves backward to the next one after that.) Word breaks are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango word break algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardWordStart() {
        return gtk_text_iter_backward_word_start(getHandle());
    }

    /**
     * Moves iter forward by a single cursor position. Cursor positions are
     * (unsurprisingly) positions where the cursor can appear. Perhaps
     * surprisingly, there may not be a cursor position between all characters.
     * The most common example for European languages would be a carriage
     * return/newline sequence. For some Unicode characters, the equivalent of
     * say the letter "a" with an accent mark will be represented as two
     * characters, first the letter then a "combining mark" that causes the
     * accent to be rendered; so the cursor can't go between those two
     * characters. See also the PangoLogAttr structure and pango_break()
     * function.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardCursorPosition() {
        return gtk_text_iter_forward_cursor_position(getHandle());
    }

    /**
     * Like {@link #moveForwardCursorPosition()}, but moves backward.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardCursorPosition() {
        return gtk_text_iter_backward_cursor_position(getHandle());
    }

    /**
     * Moves up to count cursor positions. See {@link
     * #moveForwardCursorPosition()} for details.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardCursorPosition(int count) {
        return gtk_text_iter_forward_cursor_positions(getHandle(), count);
    }

    /**
     * Moves up to count cursor positions. See {@link
     * #moveBackwardCursorPosition()} for details.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardCursorPosition(int count) {
        return gtk_text_iter_backward_cursor_positions(getHandle(), count);
    }

    /**
     * Moves backward to the previous sentence start; if iter is already at the
     * start of a sentence, moves backward to the next one. Sentence boundaries
     * are determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango text boundary algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardSentenceStart() {
        return gtk_text_iter_backward_sentence_start(getHandle());
    }

    /**
     * Calls {@link #moveBackwardSentenceStart()} up to <code>count</code>
     * times, or until it returns FALSE. If count is negative, moves forward
     * instead of backward.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardSentenceStart(int count) {
        return gtk_text_iter_backward_sentence_starts(getHandle(), count);
    }

    /**
     * Moves forward to the next sentence end. (If iter is at the end of a
     * sentence, moves to the next end of sentence.) Sentence boundaries are
     * determined by Pango and should be correct for nearly any language (if
     * not, the correct fix would be to the Pango text boundary algorithms).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardSentenceEnd() {
        return gtk_text_iter_forward_sentence_end(getHandle());
    }

    /**
     * Calls {@link #moveForwardSentenceEnd()} <code>count</code> times (or
     * until it returns FALSE). If count is negative, moves backward instead of
     * forward.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardSentenceEnd(int count) {
        return gtk_text_iter_forward_sentence_ends(getHandle(), count);
    }

    /**
     * Sets iter to point to <code>charOffset</code>. The charOffset counts
     * from the start of the entire text buffer, starting with 0.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCharOffset(int charOffset) {
        gtk_text_iter_set_offset(getHandle(), charOffset);
    }

    /**
     * Moves iterator iter to the start of the line <code>lineNumber</code>.
     * If lineNumber is negative or larger than the number of lines in the
     * buffer, moves iter to the start of the last line in the buffer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLine(int lineNumber) {
        gtk_text_iter_set_line(getHandle(), lineNumber);
    }

    /**
     * Moves iter within a line, to a new character (not byte) offset. The given
     * character offset must be less than or equal to the number of characters
     * in the line; if equal, iter moves to the start of the next line.
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLineOffset(int charOnLine) {
        gtk_text_iter_set_line_offset(getHandle(), charOnLine);
    }

    /**
     * Like {@link #setLineOffset(int)}, but the offset is in visible
     * characters, ie text with a tag making it invisible is not counted in the
     * offset.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibleLineOffset(int charOnLine) {
        gtk_text_iter_set_visible_line_offset(getHandle(), charOnLine);
    }

    /**
     * Moves iter forward to the "end iterator," which points one past the last
     * valid character in the buffer. {@link #getChar()} called on the end
     * iterator returns 0, which is convenient for writing loops.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveToEnd() {
        gtk_text_iter_forward_to_end(getHandle());
    }

    /**
     * Moves the iterator to point to the paragraph delimiter characters, which
     * will be either a newline, a carriage return, a carriage return/newline in
     * sequence, or the Unicode paragraph separator character. If the iterator
     * is already at the paragraph delimiter characters, moves to the paragraph
     * delimiter characters for the next line. If iter is on the last line in
     * the buffer, which does not end in paragraph delimiters, moves to the end
     * iterator (end of the last line), and returns FALSE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveToLineEnd() {
        return gtk_text_iter_forward_to_line_end(getHandle());
    }

    /**
     * Moves forward to the next toggle (on or off) of the TextTag
     * <code>tag</code>. If no matching tag toggles are found, returns FALSE,
     * otherwise TRUE. Does not return toggles located at iter, only toggles
     * after iter. Sets iter to the location of the toggle, or to the end of the
     * buffer if no toggle is found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveForwardToggle(TextTag tag) {
        return gtk_text_iter_forward_to_tag_toggle(getHandle(), tag.getHandle());
    }

    /**
     * Moves backward to the next toggle (on or off) of the TextTag
     * <code>tag</code>. If no matching tag toggles are found, returns FALSE,
     * otherwise TRUE. Does not return toggles located at iter, only toggles
     * before iter. Sets iter to the location of the toggle, or the start of the
     * buffer if no toggle is found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean moveBackwardToggle(TextTag tag) {
        return gtk_text_iter_backward_to_tag_toggle(getHandle(), tag
                .getHandle());
    }

    /**
     * Searches forward for <code>str</code>. Any match is returned by
     * setting <code>matchStart</code> to the first character of the match and
     * <code>matchEnd</code> to the first character after the match. The
     * search will not continue past limit. Note that a search is a linear or
     * O(n) operation, so you may wish to use limit to avoid locking up your UI
     * on large buffers.
     * 
     * <p>
     * If the TextSearchFlags.VISIBLE flag is present, the match may have
     * invisible text interspersed in str. i.e. str will be a
     * possibly-noncontiguous subsequence of the matched range. similarly, if
     * you specify TextSeachFlags.ONLY, the match may have pixbufs or child
     * widgets mixed inside the matched range. If these flags are not given, the
     * match must be exact; the special 0xFFFC character in str will match
     * embedded pixbufs or child widgets.
     * 
     * @param str
     *            A search string
     * @param flags
     *            Flags affecting how the search is done
     * @param matchStart
     *            Return location for start of match
     * @param matchEnd
     *            Return location for end of match
     * @param limit
     *            Bound for the searchof the buffer
     * @return Whether a match was found
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean searchForward(String str, TextSearchFlags flags,
            TextIter matchStart, TextIter matchEnd, TextIter limit) {
        return gtk_text_iter_forward_search(getHandle(), str, flags.getValue(),
                matchStart.getHandle(), matchEnd.getHandle(), limit.getHandle());
    }

    /**
     * Same as {@link #searchForward(String, TextSearchFlags, TextIter ,
     * TextIter , TextIter)}, but moves backward.
     * 
     * @param str
     *            A search string
     * @param flags
     *            Flags affecting how the search is done
     * @param matchStart
     *            Return location for start of match
     * @param matchEnd
     *            Return location for end of match
     * @param limit
     *            Bound for the searchof the buffer
     * @return Whether a match was found
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean searchBackward(String str, TextSearchFlags flags,
            TextIter matchStart, TextIter matchEnd, TextIter limit) {
        return gtk_text_iter_backward_search(getHandle(), str,
                flags.getValue(), matchStart.getHandle(), matchEnd.getHandle(),
                limit.getHandle());
    }

    /**
     * Tests whether two iterators are equal, using the fastest possible
     * mechanism. This function is very fast; you can expect it to perform
     * better than e.g. getting the character offset for each iterator and
     * comparing the offsets yourself.
     * 
     * @param other
     *            Iterator to test
     * @return TRUE if the iterators point to the same place in the buffer
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equals(TextIter other) {
        return gtk_text_iter_equal(getHandle(), other.getHandle());
    }

    /**
     * A qsort()-style function that returns negative if lhs is less than rhs,
     * positive if lhs is greater than rhs, and 0 if they're equal. Ordering is
     * in character offset order, i.e. the first character in the buffer is less
     * than the second character in the buffer.
     * 
     * @param other
     *            Iterator to test
     * @return -1 if lhs is less than rhs, 1 if lhs is greater, 0 if they are
     *         equal
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int cmp(TextIter other) {
        return gtk_text_iter_compare(getHandle(), other.getHandle());
    }

    /**
     * Checks whether iter falls in the range [start, end). The start and end
     * must be in ascending order.
     * 
     * @param start
     *            Start of range
     * @param end
     *            End of range
     * @return TRUE if iter is in the range
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean inRange(TextIter start, TextIter end) {
        return gtk_text_iter_in_range(getHandle(), start.getHandle(), end
                .getHandle());
    }

    native static final protected Handle gtk_text_iter_get_buffer(Handle iter);

    native static final protected void gtk_text_iter_free(Handle iter);

    native static final protected int gtk_text_iter_get_offset(Handle iter);

    native static final protected int gtk_text_iter_get_line(Handle iter);

    native static final protected int gtk_text_iter_get_line_offset(Handle iter);

    native static final protected int gtk_text_iter_get_line_index(Handle iter);

    native static final protected int gtk_text_iter_get_visible_line_offset(
            Handle iter);

    native static final protected int gtk_text_iter_get_visible_line_index(
            Handle iter);

    native static final protected char gtk_text_iter_get_char(Handle iter);

    native static final protected String gtk_text_iter_get_slice(Handle start,
            Handle end);

    native static final protected String gtk_text_iter_get_text(Handle start,
            Handle end);

    native static final protected String gtk_text_iter_get_visible_slice(
            Handle start, Handle end);

    native static final protected String gtk_text_iter_get_visible_text(
            Handle start, Handle end);

    native static final protected Handle gtk_text_iter_get_pixbuf(Handle iter);

    native static final protected Handle gtk_text_iter_get_child_anchor(
            Handle iter);

    native static final protected boolean gtk_text_iter_begins_tag(Handle iter,
            Handle tag);

    native static final protected boolean gtk_text_iter_ends_tag(Handle iter,
            Handle tag);

    native static final protected boolean gtk_text_iter_toggles_tag(
            Handle iter, Handle tag);

    native static final protected boolean gtk_text_iter_has_tag(Handle iter,
            Handle tag);

    native static final protected boolean gtk_text_iter_editable(Handle iter,
            boolean defaultSetting);

    native static final protected boolean gtk_text_iter_can_insert(Handle iter,
            boolean defaultEditablity);

    native static final protected boolean gtk_text_iter_starts_word(Handle iter);

    native static final protected boolean gtk_text_iter_ends_word(Handle iter);

    native static final protected boolean gtk_text_iter_inside_word(Handle iter);

    native static final protected boolean gtk_text_iter_starts_sentence(
            Handle iter);

    native static final protected boolean gtk_text_iter_ends_sentence(
            Handle iter);

    native static final protected boolean gtk_text_iter_inside_sentence(
            Handle iter);

    native static final protected boolean gtk_text_iter_starts_line(Handle iter);

    native static final protected boolean gtk_text_iter_ends_line(Handle iter);

    native static final protected boolean gtk_text_iter_is_cursor_position(
            Handle iter);

    native static final protected int gtk_text_iter_get_chars_in_line(
            Handle iter);

    native static final protected int gtk_text_iter_get_bytes_in_line(
            Handle iter);

    native static final protected boolean gtk_text_iter_get_attributes(
            Handle iter, Handle values);

    native static final protected Handle gtk_text_iter_get_language(Handle iter);

    native static final protected boolean gtk_text_iter_is_end(Handle iter);

    native static final protected boolean gtk_text_iter_is_start(Handle iter);

    native static final protected boolean gtk_text_iter_forward_char(Handle iter);

    native static final protected boolean gtk_text_iter_backward_char(
            Handle iter);

    native static final protected boolean gtk_text_iter_forward_chars(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_backward_chars(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_forward_line(Handle iter);

    native static final protected boolean gtk_text_iter_backward_line(
            Handle iter);

    native static final protected boolean gtk_text_iter_forward_lines(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_backward_lines(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_forward_word_end(
            Handle iter);

    native static final protected boolean gtk_text_iter_backward_word_start(
            Handle iter);

    native static final protected boolean gtk_text_iter_forward_word_ends(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_backward_word_starts(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_forward_sentence_end(
            Handle iter);

    native static final protected boolean gtk_text_iter_backward_sentence_start(
            Handle iter);

    native static final protected boolean gtk_text_iter_forward_sentence_ends(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_backward_sentence_starts(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_forward_cursor_position(
            Handle iter);

    native static final protected boolean gtk_text_iter_backward_cursor_position(
            Handle iter);

    native static final protected boolean gtk_text_iter_forward_cursor_positions(
            Handle iter, int count);

    native static final protected boolean gtk_text_iter_backward_cursor_positions(
            Handle iter, int count);

    native static final protected void gtk_text_iter_set_offset(Handle iter,
            int charOffset);

    native static final protected void gtk_text_iter_set_line(Handle iter,
            int lineNumber);

    native static final protected void gtk_text_iter_set_line_offset(
            Handle iter, int charOnLine);

    native static final protected void gtk_text_iter_set_line_index(
            Handle iter, int byteOnLine);

    native static final protected void gtk_text_iter_forward_to_end(Handle iter);

    native static final protected boolean gtk_text_iter_forward_to_line_end(
            Handle iter);

    native static final protected void gtk_text_iter_set_visible_line_offset(
            Handle iter, int charOnLine);

    native static final protected void gtk_text_iter_set_visible_line_index(
            Handle iter, int byteOnLine);

    native static final protected boolean gtk_text_iter_forward_to_tag_toggle(
            Handle iter, Handle tag);

    native static final protected boolean gtk_text_iter_backward_to_tag_toggle(
            Handle iter, Handle tag);

    native static final protected boolean gtk_text_iter_forward_search(
            Handle iter, String str, int flags, Handle matchStart,
            Handle matchEnd, Handle limit);

    native static final protected boolean gtk_text_iter_backward_search(
            Handle iter, String str, int flags, Handle matchStart,
            Handle matchEnd, Handle limit);

    native static final protected boolean gtk_text_iter_equal(Handle lhs,
            Handle rhs);

    native static final protected int gtk_text_iter_compare(Handle lhs,
            Handle rhs);

    native static final protected boolean gtk_text_iter_in_range(Handle iter,
            Handle start, Handle end);

    native static final protected void gtk_text_iter_order(Handle first,
            Handle second);

    native static final protected boolean gtk_text_iter_isChildAnchor(
            Handle handle);

    native static final protected boolean gtk_text_iter_isPixbuf(Handle handle);
}
