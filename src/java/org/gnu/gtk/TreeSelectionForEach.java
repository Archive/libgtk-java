/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

/**
 * This interface is used in combination with some methods of this class to
 * iterate over a number of items in the selection.
 * 
 * @see TreeSelection#forEachSelected(TreeSelectionForEach)
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeSelectionForEach</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface TreeSelectionForEach {
    /**
     * This is called for each of the items.
     * 
     * @param model
     *            The TreeModel being viewed
     * @param path
     *            The TreePath of a selected row
     * @param iter
     *            A TreeIter pointing to a selected row
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void forEach(TreeModel model, TreePath path, TreeIter iter);
}
