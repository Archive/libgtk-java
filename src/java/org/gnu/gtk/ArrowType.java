/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Used to indicate the direction in which an Arrow should point.
 * 
 * @see Arrow
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ArrowType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ArrowType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _UP = 0;

    static final public org.gnu.gtk.ArrowType UP = new org.gnu.gtk.ArrowType(
            _UP);

    static final private int _DOWN = 1;

    static final public org.gnu.gtk.ArrowType DOWN = new org.gnu.gtk.ArrowType(
            _DOWN);

    static final private int _LEFT = 2;

    static final public org.gnu.gtk.ArrowType LEFT = new org.gnu.gtk.ArrowType(
            _LEFT);

    static final private int _RIGHT = 3;

    static final public org.gnu.gtk.ArrowType RIGHT = new org.gnu.gtk.ArrowType(
            _RIGHT);

    static final private org.gnu.gtk.ArrowType[] theInterned = new org.gnu.gtk.ArrowType[] {
            UP, DOWN, LEFT, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ArrowType theSacrificialOne = new org.gnu.gtk.ArrowType(
            0);

    static public org.gnu.gtk.ArrowType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ArrowType already = (org.gnu.gtk.ArrowType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ArrowType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ArrowType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ArrowType or(org.gnu.gtk.ArrowType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ArrowType and(org.gnu.gtk.ArrowType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ArrowType xor(org.gnu.gtk.ArrowType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ArrowType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
