/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Defines the prebuilt set of buttons for a Dialog. If none of these choices
 * are appropriate, simply use NONE and then add you own buttons.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ButtonsType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ButtonsType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    /**
     * No buttons at all.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType NONE = new org.gnu.gtk.ButtonsType(
            _NONE);

    static final private int _OK = 1;

    /**
     * An OK button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType OK = new org.gnu.gtk.ButtonsType(
            _OK);

    static final private int _CLOSE = 2;

    /**
     * A Close button
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType CLOSE = new org.gnu.gtk.ButtonsType(
            _CLOSE);

    static final private int _CANCEL = 3;

    /**
     * A Cancel button
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType CANCEL = new org.gnu.gtk.ButtonsType(
            _CANCEL);

    static final private int _YES_NO = 4;

    /**
     * Yes and No buttons.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType YES_NO = new org.gnu.gtk.ButtonsType(
            _YES_NO);

    static final private int _OK_CANCEL = 5;

    /**
     * OK and Cancel buttons.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ButtonsType OK_CANCEL = new org.gnu.gtk.ButtonsType(
            _OK_CANCEL);

    static final private org.gnu.gtk.ButtonsType[] theInterned = new org.gnu.gtk.ButtonsType[] {
            NONE, OK, CLOSE, CANCEL, YES_NO, OK_CANCEL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ButtonsType theSacrificialOne = new org.gnu.gtk.ButtonsType(
            0);

    static public org.gnu.gtk.ButtonsType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ButtonsType already = (org.gnu.gtk.ButtonsType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ButtonsType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ButtonsType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ButtonsType or(org.gnu.gtk.ButtonsType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ButtonsType and(org.gnu.gtk.ButtonsType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ButtonsType xor(org.gnu.gtk.ButtonsType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ButtonsType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
