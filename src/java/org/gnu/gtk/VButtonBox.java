/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * VButtonBox is a container designed to display a collection of buttons
 * vertically.
 * <p>
 * The methods for manipulating this widget are all stored in it's super class,
 * ButtonBox.
 * 
 * @see ButtonBox
 * @see HButtonBox
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.VButtonBox</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class VButtonBox extends ButtonBox {
    /**
     * Creates a new VButtonBox Object, ready to have widgets added to it.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VButtonBox() {
        super(gtk_vbutton_box_new());
    }

    /**
     * Construct a new VButtonBox from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VButtonBox(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static VButtonBox getVButtonBox(Handle handle) {
        if (handle == null)
            return null;

        VButtonBox obj = (VButtonBox) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new VButtonBox(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_vbutton_box_get_type());
    }

    native static final protected int gtk_vbutton_box_get_type();

    native static final protected Handle gtk_vbutton_box_new();

    /*
     * native static final private int gtk_vbutton_box_get_spacing_default();
     * native static final private void gtk_vbutton_box_set_spacing_default(int
     * spacing); native static final private int
     * gtk_vbutton_box_get_layout_default(); native static final private void
     * gtk_vbutton_box_set_layout_default(int layout);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
