/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This widget displays a grid and traces a graph on it following a set of
 * points. There are three kinds of traces available. You can add new points,
 * and move existing points, interactively with the mouse.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Curve</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Curve extends DrawingArea {
    /**
     * Create a new Curve object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Curve() {
        super(gtk_curve_new());
    }

    /**
     * Construct a new Curve from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Curve(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Curve getCurve(Handle handle) {
        if (handle == null)
            return null;

        Curve obj = (Curve) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Curve(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_curve_get_type());
    }

    native static final protected int gtk_curve_get_type();

    native static final protected Handle gtk_curve_new();

    native static final protected void gtk_curve_reset(Handle curve);

    native static final protected void gtk_curve_set_gamma(Handle curve,
            double gamma);

    native static final protected void gtk_curve_set_range(Handle curve,
            double minX, double maxX, double minY, double maxY);

    native static final protected void gtk_curve_get_vector(Handle curve,
            int veclen, double[] vector);

    native static final protected void gtk_curve_set_vector(Handle curve,
            int veclen, double[] vector);

    native static final protected void gtk_curve_set_curve_type(Handle curve,
            int type);

}
