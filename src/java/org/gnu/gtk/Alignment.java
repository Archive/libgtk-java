/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is a container widget that is capable of controlling the size of a
 * single child widget. The scale values are from 0.0 to 1.0, indicating the
 * maximum amount the child can expand to fill the space allocated to the
 * Alignment widget. The align values determine the x and y positions relative
 * to the top left and bottom right corners of the Alignment rectangle. The
 * align values are from 0.0 to the top or left side, and 1.0 for the bottom or
 * right side.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Alignment</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Alignment extends Bin {

    /**
     * Construct a new Alignment.
     * 
     * @param xAlign
     *            The horizontal alignment of the child widget from 0 (left) to
     *            1 (right).
     * @param yAlign
     *            The vertical alignment of the child widget from 0 (top) to 1
     *            (bottom).
     * @param xScale
     *            The amount that the child widget expands horizontally to fill
     *            up unused space, from 0 to 1. A value of 0 indicates that the
     *            child widget should never expand. A value of 1 indicates that
     *            the child widget will expand to fill all of the space
     *            allocated for the Alignment.
     * @param yScale
     *            The amount that the child widget expands vertically to fill up
     *            unused space, from 0 to 1. The values are similar to xScale.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Alignment(double xAlign, double yAlign, double xScale, double yScale) {
        super(gtk_alignment_new(xAlign, yAlign, xScale, yScale));
    }

    /**
     * Construct an alignment using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Alignment(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Alignment getAlignment(Handle handle) {
        if (handle == null)
            return null;

        Alignment obj = (Alignment) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Alignment(handle);

        return obj;
    }

    /**
     * Set the alignment and scale for an already created Alignment object.
     * 
     * @param xAlign
     *            The horizontal alignment of the child widget from 0 (left) to
     *            1 (right).
     * @param yAlign
     *            The vertical alignment of the child widget from 0 (top) to 1
     *            (bottom).
     * @param xScale
     *            The amount that the child widget expands horizontally to fill
     *            up unused space, from 0 to 1. A value of 0 indicates that the
     *            child widget should never expand. A value of 1 indicates that
     *            the child widget will expand to fill all of the space
     *            allocated for the Alignment.
     * @param yScale
     *            The amount that the child widget expands vertically to fill up
     *            unused space, from 0 to 1. The values are similar to xScale.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(double xAlign, double yAlign, double xScale, double yScale) {
        Alignment
                .gtk_alignment_set(getHandle(), xAlign, yAlign, xScale, yScale);
    }

    /**
     * Sets the padding on the different sides of the widget. The padding adds
     * blank space to the sides of the widget. For instance, this can be used to
     * indent the child widget towards the right by adding padding on the left.
     * 
     * @param top
     *            the padding at the top of the widget
     * @param bottom
     *            the padding at the bottom of the widget
     * @param left
     *            the padding at the left of the widget
     * @param right
     *            the padding at the right of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPadding(int top, int bottom, int left, int right) {
        gtk_alignment_set_padding(getHandle(), top, bottom, left, right);
    }

    /**
     * Gets the size of the padding at the top of the widget.
     * 
     * @return The padding at the top of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTopPadding() {
        int[] top = new int[1];
        int[] bottom = new int[1];
        int[] left = new int[1];
        int[] right = new int[1];
        gtk_alignment_get_padding(getHandle(), top, bottom, left, right);
        return top[0];
    }

    /**
     * Gets the size of the padding at the bottom of the widget.
     * 
     * @return The padding at the bottom of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getBottomPadding() {
        int[] top = new int[1];
        int[] bottom = new int[1];
        int[] left = new int[1];
        int[] right = new int[1];
        gtk_alignment_get_padding(getHandle(), top, bottom, left, right);
        return bottom[0];
    }

    /**
     * Gets the size of the padding at the left of the widget.
     * 
     * @return The padding at the left of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLeftPadding() {
        int[] top = new int[1];
        int[] bottom = new int[1];
        int[] left = new int[1];
        int[] right = new int[1];
        gtk_alignment_get_padding(getHandle(), top, bottom, left, right);
        return left[0];
    }

    /**
     * Gets the size of the padding at the right of the widget.
     * 
     * @return The padding at the right of the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRightPadding() {
        int[] top = new int[1];
        int[] bottom = new int[1];
        int[] left = new int[1];
        int[] right = new int[1];
        gtk_alignment_get_padding(getHandle(), top, bottom, left, right);
        return right[0];
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_alignment_get_type());
    }

    native static final protected int gtk_alignment_get_type();

    native static final protected Handle gtk_alignment_new(double xalign,
            double yalign, double xscale, double yscale);

    native static final protected void gtk_alignment_set(Handle alignment,
            double xalign, double yalign, double xscale, double yscale);

    native static final protected void gtk_alignment_set_padding(
            Handle alignment, int top, int bottom, int left, int right);

    native static final protected void gtk_alignment_get_padding(
            Handle alignment, int[] top, int[] bottom, int[] left, int[] right);

}
