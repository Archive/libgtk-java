/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The Frame widget is a Bin that surrounds its child with a decorative frame
 * and an optional label. If present, the label is drawn in the gap in the top
 * side of the frame by default.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Frame</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Frame extends Bin {

    /**
     * Creates a new frame with no label
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Frame() {
        super(gtk_frame_new(""));
    }

    /**
     * Construct a frame using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Frame(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Frame getFrame(Handle handle) {
        if (handle == null)
            return null;

        Frame obj = (Frame) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Frame(handle);

        return obj;
    }

    /**
     * Creates a new frame, with the text of label being displayed in the top
     * left.
     * 
     * @param label
     *            Text to be displayed in the border of the frame.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Frame(String label) {
        super(gtk_frame_new(label));
    }

    /**
     * Changes the label which is displayed in the frame border.
     * 
     * @param label
     *            The text to display
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLabel(String label) {
        gtk_frame_set_label(getHandle(), label);
    }

    /**
     * Removes the label from the frame
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeLabel() {
        gtk_frame_set_label(getHandle(), null);
    }

    /**
     * Returns the text of the label which is being displayed at the top of the
     * frame.
     * 
     * @return The text of the label for the frame, if any.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getLabel() {
        return gtk_frame_get_label(getHandle());
    }

    /**
     * Sets the widget to use as the label for the frame. If you want a simple
     * label, use the {@link #setLabel(String)} method
     * 
     * @param labelWidget
     *            The widget to use in the label position of the frame.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLabelWidget(Widget labelWidget) {
        gtk_frame_set_label_widget(getHandle(), labelWidget.getHandle());
    }

    /**
     * Returns the widget being used as the label of the frame. If the frame has
     * just been constructed with a label string, then this will be a {@link
     * Label} widget. This method allows you to customise the widget.
     * 
     * @return The widget currently being used as the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getLabelWidget() {
        Handle hndl = gtk_frame_get_label_widget(getHandle());
        return Widget.getWidget(hndl);
    }

    // TODO? Change the following to use different parameters?
    /**
     * Sets the alignment of the label widget along the top edge of the frame. A
     * vale of 0.0 means full left align; 0.5 means centered; 1.0 is full right
     * align
     * 
     * @param align
     *            A number representing the alignment of the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLabelAlign(double align) {
        // Y-Align is currently unimplemnted in gtk.
        gtk_frame_set_label_align(getHandle(), align, 0);
    }

    /**
     * Returns a number representing the alignment of the label
     * 
     * @see #setLabelAlign(double)
     * @return The alignment of the label on the top of the frame.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getLabelAlign() {
        double[] x = new double[1];
        double[] y = new double[1];
        gtk_frame_get_label_align(getHandle(), x, y);
        return x[0];
    }

    /**
     * Sets the {@link ShadowType} to be displayed for the frame.
     * 
     * @param shadowType
     *            the shadow type to be used.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShadow(ShadowType shadowType) {
        gtk_frame_set_shadow_type(getHandle(), shadowType.getValue());
    }

    /**
     * Returns the shadow type in use
     * 
     * @return shadowType the shadow type being displayed
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ShadowType getShadow() {
        return ShadowType.intern(gtk_frame_get_shadow_type(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_frame_get_type());
    }

    native static final protected int gtk_frame_get_type();

    native static final protected Handle gtk_frame_new(String label);

    native static final protected void gtk_frame_set_label(Handle frame,
            String label);

    native static final protected String gtk_frame_get_label(Handle frame);

    native static final protected void gtk_frame_set_label_widget(Handle frame,
            Handle labelWidget);

    native static final protected Handle gtk_frame_get_label_widget(Handle frame);

    native static final protected void gtk_frame_set_label_align(Handle frame,
            double xalign, double yalign);

    native static final protected void gtk_frame_get_label_align(Handle frame,
            double[] xalign, double[] yalign);

    native static final protected void gtk_frame_set_shadow_type(Handle frame,
            int type);

    native static final protected int gtk_frame_get_shadow_type(Handle frame);
}
