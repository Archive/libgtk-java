/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

/**
 * {@link CellRenderer}s have attributes which can be set based on data from
 * the {@link TreeModel} (currently only implemented by {@link ListStore} and
 * {@link TreeStore}). Mapping renderer attributes to TreeModel dataBlocks is
 * done via the {@link TreeViewColumn} object.
 * 
 * <p>
 * For a full overview of the tree and list objects, please see the {@link
 * TreeView} widget description.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRendererAttribute</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRendererAttribute {
    private String attrib;

    public String toString() {
        return attrib;
    }

    protected CellRendererAttribute(String attr) {
        attrib = attr;
    }
}
