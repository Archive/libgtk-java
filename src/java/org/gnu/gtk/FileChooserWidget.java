/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.EventMap;
import org.gnu.glib.Handle;
import org.gnu.gtk.event.FileChooserEvent;
import org.gnu.gtk.event.FileChooserListener;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileChooserWidget</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileChooserWidget extends VBox implements FileChooser {
    private List listeners;

    private static EventMap eventMap = new EventMap();

    static {
        addEvents(eventMap);
    }

    public FileChooserWidget(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static FileChooserWidget getFileChooserWidget(Handle handle) {
        if (handle == null)
            return null;

        FileChooserWidget obj = (FileChooserWidget) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FileChooserWidget(handle);

        return obj;
    }

    public FileChooserWidget(FileChooserAction action) {
        super(gtk_file_chooser_widget_new(action.getValue()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setAction(org.gnu.gtk.FileChooserAction)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAction(FileChooserAction action) {
        FileChooserHelper.gtk_file_chooser_set_action(getHandle(), action
                .getValue());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getAction()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileChooserAction getAction() {
        return FileChooserAction.intern(FileChooserHelper
                .gtk_file_chooser_get_action(getHandle()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setLocalOnly(boolean)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLocalOnly(boolean localOnly) {
        FileChooserHelper.gtk_file_chooser_set_local_only(getHandle(),
                localOnly);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getLocalOnly()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getLocalOnly() {
        return FileChooserHelper.gtk_file_chooser_get_local_only(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setSelectMultiple(boolean)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSelectMultiple(boolean selectMultiple) {
        FileChooserHelper.gtk_file_chooser_set_select_multiple(getHandle(),
                selectMultiple);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getSelectMultiple()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getSelectMultiple() {
        return FileChooserHelper
                .gtk_file_chooser_get_select_multiple(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setCurrentName(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCurrentName(String name) {
        FileChooserHelper.gtk_file_chooser_set_current_name(getHandle(), name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getFilename()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFilename() {
        return FileChooserHelper.gtk_file_chooser_get_filename(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setFilename(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setFilename(String filename) {
        return FileChooserHelper.setFilename(getHandle(), filename);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#selectFilename(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean selectFilename(String filename) {
        return FileChooserHelper.gtk_file_chooser_select_filename(getHandle(),
                filename);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#unselectFilename(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unselectFilename(String filename) {
        FileChooserHelper.gtk_file_chooser_unselect_filename(getHandle(),
                filename);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#selectAll()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void selectAll() {
        FileChooserHelper.gtk_file_chooser_select_all(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#unselectAll()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unselectAll() {
        FileChooserHelper.gtk_file_chooser_unselect_all(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getFilenames()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getFilenames() {
        return FileChooserHelper.gtk_file_chooser_get_filenames(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setCurrentFolder(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setCurrentFolder(String folder) {
        return FileChooserHelper.gtk_file_chooser_set_current_folder(
                getHandle(), folder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getCurrentFolder()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getCurrentFolder() {
        return FileChooserHelper
                .gtk_file_chooser_get_current_folder(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getURI()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getURI() {
        return FileChooserHelper.gtk_file_chooser_get_uri(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setURI(String uri) {
        return FileChooserHelper.gtk_file_chooser_set_uri(getHandle(), uri);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#selectURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean selectURI(String uri) {
        return FileChooserHelper.gtk_file_chooser_select_uri(getHandle(), uri);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#unselectURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unselectURI(String uri) {
        FileChooserHelper.gtk_file_chooser_unselect_uri(getHandle(), uri);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getURIs()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getURIs() {
        return FileChooserHelper.gtk_file_chooser_get_uris(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setCurrentFolderURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setCurrentFolderURI(String folder) {
        return FileChooserHelper.gtk_file_chooser_set_current_folder_uri(
                getHandle(), folder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getCurrentFolderURI()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getCurrentFolderURI() {
        return FileChooserHelper
                .gtk_file_chooser_get_current_folder_uri(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setPreviewWidget(org.gnu.gtk.Widget)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPreviewWidget(Widget previewWidget) {
        FileChooserHelper.gtk_file_chooser_set_preview_widget(getHandle(),
                previewWidget.getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getPreviewWidget()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getPreviewWidget() {
        Handle hndl = FileChooserHelper
                .gtk_file_chooser_get_preview_widget(getHandle());
        return Widget.getWidget(hndl);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setUsePreviewLabel()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUsePreviewLabel(boolean useLabel) {
        FileChooserHelper.gtk_file_chooser_set_use_preview_label(getHandle(),
                useLabel);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getUsePreviewLabel()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getUsePreviewLabel() {
        return FileChooserHelper
                .gtk_file_chooser_get_use_preview_label(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setPreviewWidgetActive(boolean)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPreviewWidgetActive(boolean active) {
        FileChooserHelper.gtk_file_chooser_set_preview_widget_active(
                getHandle(), active);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getPreviewWidgetActive()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getPreviewWidgetActive() {
        return FileChooserHelper
                .gtk_file_chooser_get_preview_widget_active(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getPreviewFilename()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getPreviewFilename() {
        return FileChooserHelper
                .gtk_file_chooser_get_preview_filename(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getPreviewURI()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getPreviewURI() {
        return FileChooserHelper.gtk_file_chooser_get_preview_uri(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setExtraWidget(org.gnu.gtk.Widget)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setExtraWidget(Widget extraWidget) {
        FileChooserHelper.gtk_file_chooser_set_extra_widget(getHandle(),
                extraWidget.getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getExtraWidget()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getExtraWidget() {
        Handle hndl = FileChooserHelper
                .gtk_file_chooser_get_extra_widget(getHandle());
        return Widget.getWidget(hndl);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#addFilter(org.gnu.gtk.FileFilter)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addFilter(FileFilter filter) {
        FileChooserHelper.gtk_file_chooser_add_filter(getHandle(), filter
                .getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#removeFilter(org.gnu.gtk.FileFilter)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeFilter(FileFilter filter) {
        FileChooserHelper.gtk_file_chooser_remove_filter(getHandle(), filter
                .getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#listFilters()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileFilter[] listFilters() {
        Handle[] hndls = FileChooserHelper
                .gtk_file_chooser_list_filters(getHandle());
        FileFilter[] filters = new FileFilter[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            filters[i] = FileFilter.getFileFilter(hndls[i]);
        }
        return filters;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#setFilter(org.gnu.gtk.FileFilter)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFilter(FileFilter filter) {
        FileChooserHelper.gtk_file_chooser_set_filter(getHandle(), filter
                .getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#getFilter()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileFilter getFilter() {
        Handle hndl = FileChooserHelper
                .gtk_file_chooser_get_filter(getHandle());
        return FileFilter.getFileFilter(hndl);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#addShortcutFolder(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addShortcutFolder(String folder) throws FileChooserException {
        FileChooserHelper.addShortcutFolder(getHandle(), folder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#removeShortcutFolder(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeShortcutFolder(String folder) throws FileChooserException {
        FileChooserHelper.removeShortcutFolder(getHandle(), folder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#listShortcutFolders()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] listShortcutFolders() {
        return FileChooserHelper
                .gtk_file_chooser_list_shortcut_folders(getHandle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#addShortCutURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addShortcutURI(String uri) throws FileChooserException {
        FileChooserHelper.addShortcutURI(getHandle(), uri);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#removeShortcutURI(java.lang.String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeShortcutURI(String uri) throws FileChooserException {
        FileChooserHelper.removeShortcutURI(getHandle(), uri);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.FileChooser#listShortcutURIs()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] listShortcutURIs() {
        return FileChooserHelper
                .gtk_file_chooser_list_shortcut_folder_uris(getHandle());
    }

    /**
     * Sets whether hidden files and folders are displayed in the file selector.
     * 
     * @param hidden
     *            <tt>true</tt> if hidden files and folders should be
     *            displayed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShowHidden(boolean hidden) {
        FileChooserHelper.gtk_file_chooser_set_show_hidden(getHandle(), hidden);
    }

    /**
     * Sets whether hidden files and folders are displayed in the file selector.
     * 
     * @return <tt>true</tt> if hidden files and folders should be displayed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getShowHidden() {
        return FileChooserHelper.gtk_file_chooser_get_show_hidden(getHandle());
    }

    public void addListener(FileChooserListener listener) {
        if (listeners == null) {
            listeners = new ArrayList();
        }
        FileChooserHelper.addListener(listeners, listener, eventMap, this);
    }

    public void removeListener(FileChooserListener listener) {
        FileChooserHelper.removeListener(listeners, listener, eventMap, this);
        if (listeners.size() == 0) {
            listeners = null;
        }
    }

    private static void addEvents(EventMap eventMap) {
        FileChooserEvent.Type[] types = FileChooserEvent.Type.getTypes();
        for (int i = 0; i < types.length; ++i) {
            eventMap.addEvent(types[i], FileChooserWidget.class);
        }
    }

    private void handleCurrentFolderChanged() {
        FileChooserEvent event = new FileChooserEvent(this,
                FileChooserEvent.Type.CURRENT_FOLDER_CHANGED);
        FileChooserHelper.fireCurrentFolderChanged(listeners, event);
    }

    private void handleFileActivated() {
        FileChooserEvent event = new FileChooserEvent(this,
                FileChooserEvent.Type.CURRENT_FOLDER_CHANGED);

        FileChooserHelper.fireFileActivated(listeners, event);
    }

    private void handleSelectionChanged() {
        FileChooserEvent event = new FileChooserEvent(this,
                FileChooserEvent.Type.CURRENT_FOLDER_CHANGED);
        FileChooserHelper.fireSelectionChanged(listeners, event);
    }

    private void handleUpdatePreview() {
        FileChooserEvent event = new FileChooserEvent(this,
                FileChooserEvent.Type.CURRENT_FOLDER_CHANGED);
        FileChooserHelper.fireUpdatePreview(listeners, event);
    }

    native static final protected int gtk_file_chooser_widget_get_type();

    native static final protected Handle gtk_file_chooser_widget_new(int action);

}
