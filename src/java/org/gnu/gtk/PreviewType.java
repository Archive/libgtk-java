/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.PreviewType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class PreviewType extends Enum {
    static final private int _COLOR = 0;

    static final public PreviewType COLOR = new PreviewType(_COLOR);

    static final private int _GRAYSCALE = 1;

    static final public PreviewType GRAYSCALE = new PreviewType(_GRAYSCALE);

    static final private PreviewType[] theInterned = new PreviewType[] { COLOR,
            GRAYSCALE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private PreviewType theSacrificialOne = new PreviewType(0);

    static public PreviewType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        PreviewType already = (PreviewType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new PreviewType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PreviewType(int value) {
        value_ = value;
    }

    public PreviewType or(PreviewType other) {
        return intern(value_ | other.value_);
    }

    public PreviewType and(PreviewType other) {
        return intern(value_ & other.value_);
    }

    public PreviewType xor(PreviewType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(PreviewType other) {
        return (value_ & other.value_) == other.value_;
    }

}
