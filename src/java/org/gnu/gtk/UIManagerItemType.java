/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.UIManagerItemType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class UIManagerItemType extends Enum {

    static final private int _AUTO = 0;

    static final public UIManagerItemType AUTO = new UIManagerItemType(_AUTO);

    static final private int _MENUBAR = 1;

    static final public UIManagerItemType MENUBAR = new UIManagerItemType(
            _MENUBAR);

    static final private int _MENU = 2;

    static final public UIManagerItemType MENU = new UIManagerItemType(_MENU);

    static final private int _TOOLBAR = 3;

    static final public UIManagerItemType TOOLBAR = new UIManagerItemType(
            _TOOLBAR);

    static final private int _PLACEHOLDER = 4;

    static final public UIManagerItemType PLACEHOLDER = new UIManagerItemType(
            _PLACEHOLDER);

    static final private int _POPUP = 5;

    static final public UIManagerItemType POPUP = new UIManagerItemType(_POPUP);

    static final private int _MENUITEM = 6;

    static final public UIManagerItemType MENUITEM = new UIManagerItemType(
            _MENUITEM);

    static final private int _TOOLITEM = 7;

    static final public UIManagerItemType TOOLITEM = new UIManagerItemType(
            _TOOLITEM);

    static final private int _SEPARATOR = 8;

    static final public UIManagerItemType SEPARATOR = new UIManagerItemType(
            _SEPARATOR);

    static final private int _ACCELERATOR = 9;

    static final public UIManagerItemType ACCELERATOR = new UIManagerItemType(
            _ACCELERATOR);

    static final private UIManagerItemType[] theInterned = new UIManagerItemType[] {
            AUTO, MENUBAR, MENU, TOOLBAR, PLACEHOLDER, POPUP, MENUITEM,
            TOOLITEM, SEPARATOR, ACCELERATOR };

    static private java.util.Hashtable theInternedExtras;

    static final private UIManagerItemType theSacrificialOne = new UIManagerItemType(
            0);

    static public UIManagerItemType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        UIManagerItemType already = (UIManagerItemType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new UIManagerItemType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private UIManagerItemType(int value) {
        value_ = value;
    }

    public UIManagerItemType or(UIManagerItemType other) {
        return intern(value_ | other.value_);
    }

    public UIManagerItemType and(UIManagerItemType other) {
        return intern(value_ & other.value_);
    }

    public UIManagerItemType xor(UIManagerItemType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(UIManagerItemType other) {
        return (value_ & other.value_) == other.value_;
    }
}
