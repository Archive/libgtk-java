/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;

/**
 * ToolTips are the messages that appear next to a widget when the mouse pointer
 * is held over it for a short period of time.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolTips</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolTips extends GtkObject {
    /**
     * Construct a new ToolTips object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolTips() {
        super(gtk_tooltips_new());
    }

    public ToolTips(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToolTips getToolTips(Handle handle) {
        if (handle == null)
            return null;

        ToolTips obj = (ToolTips) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new ToolTips(handle);

        return obj;
    }

    /**
     * Adds a tooltip to a Widget.
     * 
     * @param widget
     *            The Widget to add to tooltips.
     * @param tipText
     *            The text to display for the tooltip.
     * @param additionalInfo
     *            Additional information that might be useful to the user if
     *            they get stuck.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTip(Widget widget, String tipText, String additionalInfo) {
        ToolTips.gtk_tooltips_set_tip(getHandle(), widget.getHandle(), tipText,
                additionalInfo);
    }

    /**
     * Enables the ToolTips.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void enable() {
        ToolTips.gtk_tooltips_enable(getHandle());
    }

    /**
     * Disables the ToolTips.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void disable() {
        ToolTips.gtk_tooltips_disable(getHandle());
    }

    /**
     * Retrieve the ToolTipsData associated with the provided widget
     * 
     * @param widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToolTipsData getData(Widget widget) {
        Handle handle = gtk_tooltips_data_get(widget.getHandle());
        return ToolTipsData.getToolTipsData(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_tooltips_get_type());
    }

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected int gtk_tooltips_get_type();

    native static final protected Handle gtk_tooltips_new();

    native static final protected void gtk_tooltips_enable(Handle tooltips);

    native static final protected void gtk_tooltips_disable(Handle tooltips);

    native static final protected void gtk_tooltips_set_tip(Handle tooltips,
            Handle widget, String tipText, String tipPrivate);

    native static final protected void gtk_tooltips_force_window(Handle tooltips);

    native static final protected Handle gtk_tooltips_data_get(Handle widget);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
