/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class TextWindowType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _PRIVATE = 0;

    static final public org.gnu.gtk.TextWindowType PRIVATE = new org.gnu.gtk.TextWindowType(
            _PRIVATE);

    static final private int _WIDGET = 1;

    static final public org.gnu.gtk.TextWindowType WIDGET = new org.gnu.gtk.TextWindowType(
            _WIDGET);

    static final private int _TEXT = 2;

    static final public org.gnu.gtk.TextWindowType TEXT = new org.gnu.gtk.TextWindowType(
            _TEXT);

    static final private int _LEFT = 3;

    static final public org.gnu.gtk.TextWindowType LEFT = new org.gnu.gtk.TextWindowType(
            _LEFT);

    static final private int _RIGHT = 4;

    static final public org.gnu.gtk.TextWindowType RIGHT = new org.gnu.gtk.TextWindowType(
            _RIGHT);

    static final private int _TOP = 5;

    static final public org.gnu.gtk.TextWindowType TOP = new org.gnu.gtk.TextWindowType(
            _TOP);

    static final private int _BOTTOM = 6;

    static final public org.gnu.gtk.TextWindowType BOTTOM = new org.gnu.gtk.TextWindowType(
            _BOTTOM);

    static final private org.gnu.gtk.TextWindowType[] theInterned = new org.gnu.gtk.TextWindowType[] {
            PRIVATE, WIDGET, TEXT, LEFT, RIGHT, TOP, BOTTOM }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TextWindowType theSacrificialOne = new org.gnu.gtk.TextWindowType(
            0);

    static public org.gnu.gtk.TextWindowType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TextWindowType already = (org.gnu.gtk.TextWindowType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TextWindowType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TextWindowType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TextWindowType or(org.gnu.gtk.TextWindowType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TextWindowType and(org.gnu.gtk.TextWindowType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TextWindowType xor(org.gnu.gtk.TextWindowType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TextWindowType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
