/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class AnchorType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _CENTER = 0;

    static final public org.gnu.gtk.AnchorType CENTER = new org.gnu.gtk.AnchorType(
            _CENTER);

    static final private int _NORTH = 1;

    static final public org.gnu.gtk.AnchorType NORTH = new org.gnu.gtk.AnchorType(
            _NORTH);

    static final private int _NORTH_WEST = 2;

    static final public org.gnu.gtk.AnchorType NORTH_WEST = new org.gnu.gtk.AnchorType(
            _NORTH_WEST);

    static final private int _NORTH_EAST = 3;

    static final public org.gnu.gtk.AnchorType NORTH_EAST = new org.gnu.gtk.AnchorType(
            _NORTH_EAST);

    static final private int _SOUTH = 4;

    static final public org.gnu.gtk.AnchorType SOUTH = new org.gnu.gtk.AnchorType(
            _SOUTH);

    static final private int _SOUTH_WEST = 5;

    static final public org.gnu.gtk.AnchorType SOUTH_WEST = new org.gnu.gtk.AnchorType(
            _SOUTH_WEST);

    static final private int _SOUTH_EAST = 6;

    static final public org.gnu.gtk.AnchorType SOUTH_EAST = new org.gnu.gtk.AnchorType(
            _SOUTH_EAST);

    static final private int _WEST = 7;

    static final public org.gnu.gtk.AnchorType WEST = new org.gnu.gtk.AnchorType(
            _WEST);

    static final private int _EAST = 8;

    static final public org.gnu.gtk.AnchorType EAST = new org.gnu.gtk.AnchorType(
            _EAST);

    static final private org.gnu.gtk.AnchorType[] theInterned = new org.gnu.gtk.AnchorType[] {
            CENTER, NORTH, NORTH_WEST, NORTH_EAST, SOUTH, SOUTH_WEST,
            SOUTH_EAST, WEST, EAST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.AnchorType theSacrificialOne = new org.gnu.gtk.AnchorType(
            0);

    static public org.gnu.gtk.AnchorType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.AnchorType already = (org.gnu.gtk.AnchorType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.AnchorType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private AnchorType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.AnchorType or(org.gnu.gtk.AnchorType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.AnchorType and(org.gnu.gtk.AnchorType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.AnchorType xor(org.gnu.gtk.AnchorType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.AnchorType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
