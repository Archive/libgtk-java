/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Modes for {@link SizeGroup} objects.
 * 
 * @see SizeGroup
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SizeGroupMode</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SizeGroupMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    /** group has no effect */
    static final public org.gnu.gtk.SizeGroupMode NONE = new org.gnu.gtk.SizeGroupMode(
            _NONE);

    static final private int _HORIZONTAL = 1;

    /** group effects horizontal requisition */
    static final public org.gnu.gtk.SizeGroupMode HORIZONTAL = new org.gnu.gtk.SizeGroupMode(
            _HORIZONTAL);

    static final private int _VERTICAL = 2;

    /** group effects vertical requisition * */
    static final public org.gnu.gtk.SizeGroupMode VERTICAL = new org.gnu.gtk.SizeGroupMode(
            _VERTICAL);

    static final private int _BOTH = 3;

    /** group effects both horizontal and vertical requisition */
    static final public org.gnu.gtk.SizeGroupMode BOTH = new org.gnu.gtk.SizeGroupMode(
            _BOTH);

    static final private org.gnu.gtk.SizeGroupMode[] theInterned = new org.gnu.gtk.SizeGroupMode[] {
            NONE, HORIZONTAL, VERTICAL, BOTH }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SizeGroupMode theSacrificialOne = new org.gnu.gtk.SizeGroupMode(
            0);

    static public org.gnu.gtk.SizeGroupMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SizeGroupMode already = (org.gnu.gtk.SizeGroupMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SizeGroupMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SizeGroupMode(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SizeGroupMode or(org.gnu.gtk.SizeGroupMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SizeGroupMode and(org.gnu.gtk.SizeGroupMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SizeGroupMode xor(org.gnu.gtk.SizeGroupMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SizeGroupMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
