/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class CellRendererMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _INVERT = 0;

    static final public org.gnu.gtk.CellRendererMode INVERT = new org.gnu.gtk.CellRendererMode(
            _INVERT);

    static final private int _ACTIVATABLE = 1;

    static final public org.gnu.gtk.CellRendererMode ACTIVATABLE = new org.gnu.gtk.CellRendererMode(
            _ACTIVATABLE);

    static final private int _EDITABLE = 2;

    static final public org.gnu.gtk.CellRendererMode EDITABLE = new org.gnu.gtk.CellRendererMode(
            _EDITABLE);

    static final private org.gnu.gtk.CellRendererMode[] theInterned = new org.gnu.gtk.CellRendererMode[] {
            INVERT, ACTIVATABLE, EDITABLE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.CellRendererMode theSacrificialOne = new org.gnu.gtk.CellRendererMode(
            0);

    static public org.gnu.gtk.CellRendererMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.CellRendererMode already = (org.gnu.gtk.CellRendererMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.CellRendererMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CellRendererMode(int value) {
        value_ = value;
    }

    public org.gnu.gtk.CellRendererMode or(org.gnu.gtk.CellRendererMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.CellRendererMode and(org.gnu.gtk.CellRendererMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.CellRendererMode xor(org.gnu.gtk.CellRendererMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.CellRendererMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
