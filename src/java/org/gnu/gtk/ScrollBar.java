/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The ScrollBar widget is an abstract base class for {@link HScrollBar} and
 * {@link VScrollBar}. It is not very useful in itself.
 * 
 * <p>
 * The position of the thumb in a scrollbar is controlled by the scroll
 * adjustments. See {@link Adjustment} for the fields in an adjustment - for
 * ScrollBar, the "value" field represents the position of the scrollbar, which
 * must be between the "lower" field and "upper - pageSize." The "pageSize"
 * field represents the size of the visible scrollable area. The "stepIncrement"
 * and "pageIncrement" fields are used when the user asks to step down (using
 * the small stepper arrows) or page down (using for example the PageDown key).
 * 
 * @see VScrollBar
 * @see HScrollBar
 * @see ScrolledWindow
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ScrollBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public abstract class ScrollBar extends Range {
    /**
     * Construct a ScrollBar from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected ScrollBar(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_scrollbar_get_type());
    }

    native static final protected int gtk_scrollbar_get_type();
}
