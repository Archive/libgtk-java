/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

/**
 * Stock items are combinations of icons and text which are used in various
 * widgets, such as gtk.Button. They have been created as a standard part of gtk
 * so that gtk themes have even more control over the display of applications,
 * by allowing them to change standard icons. The use of stock items is highly
 * recommended.
 * <p>
 * In these Java bindings for Gtk, we have created a GtkStockItem object which
 * should be passed to widget methods, rather than the stock id string as is
 * done in gtk. This should help prevent errors due to typing, and also allows
 * stronger typing of many methods.
 * <p>
 * TODO: give a link to details of all the stock items.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.GtkStockItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class GtkStockItem {
    protected String stockID;

    /**
     * Creates a new StockItem object, based on the id string for that object.
     * It is highly recommended that you use the static final strings which are
     * part of this class rather than quoting the strings directly.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GtkStockItem(String id) {
        stockID = id;
    }

    public String getString() {
        return stockID;
    }

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final public GtkStockItem ABOUT = new GtkStockItem("gtk-about");

    static final public GtkStockItem ADD = new GtkStockItem("gtk-add");

    static final public GtkStockItem APPLY = new GtkStockItem("gtk-apply");

    static final public GtkStockItem BOLD = new GtkStockItem("gtk-bold");

    static final public GtkStockItem CANCEL = new GtkStockItem("gtk-cancel");

    static final public GtkStockItem CDROM = new GtkStockItem("gtk-cdrom");

    static final public GtkStockItem CLEAR = new GtkStockItem("gtk-clear");

    static final public GtkStockItem CLOSE = new GtkStockItem("gtk-close");

    static final public GtkStockItem COLOR_PICKER = new GtkStockItem(
            "gtk-color-picker");

    static final public GtkStockItem CONVERT = new GtkStockItem("gtk-convert");

    static final public GtkStockItem CONNECT = new GtkStockItem("gtk-connect");

    static final public GtkStockItem COPY = new GtkStockItem("gtk-copy");

    static final public GtkStockItem CUT = new GtkStockItem("gtk-cut");

    static final public GtkStockItem DELETE = new GtkStockItem("gtk-delete");

    static final public GtkStockItem DIALOG_AUTHENTICATION = new GtkStockItem(
            "gtk-dialog-authentication");

    static final public GtkStockItem DIALOG_ERROR = new GtkStockItem(
            "gtk-dialog-error");

    static final public GtkStockItem DIALOG_INFO = new GtkStockItem(
            "gtk-dialog-info");

    static final public GtkStockItem DIALOG_QUESTION = new GtkStockItem(
            "gtk-dialog-question");

    static final public GtkStockItem DIALOG_WARNING = new GtkStockItem(
            "gtk-dialog-warning");

    static final public GtkStockItem DIRECTORY = new GtkStockItem(
            "gtk-directory");

    static final public GtkStockItem DISCONNECT = new GtkStockItem(
            "gtk-disconnect");

    static final public GtkStockItem DND = new GtkStockItem("gtk-dnd");

    static final public GtkStockItem DND_MULTIPLE = new GtkStockItem(
            "gtk-dnd-multiple");

    static final public GtkStockItem EDIT = new GtkStockItem("gtk-edit");

    static final public GtkStockItem EXECUTE = new GtkStockItem("gtk-execute");

    static final public GtkStockItem FIND = new GtkStockItem("gtk-find");

    static final public GtkStockItem FIND_AND_REPLACE = new GtkStockItem(
            "gtk-find-and-replace");

    static final public GtkStockItem FLOPPY = new GtkStockItem("gtk-floppy");

    static final public GtkStockItem FULLSCREEN = new GtkStockItem(
            "gtk-fullscreen");

    static final public GtkStockItem GOTO_BOTTOM = new GtkStockItem(
            "gtk-goto-bottom");

    static final public GtkStockItem GOTO_FIRST = new GtkStockItem(
            "gtk-goto-first");

    static final public GtkStockItem GOTO_LAST = new GtkStockItem(
            "gtk-goto-last");

    static final public GtkStockItem GOTO_TOP = new GtkStockItem("gtk-goto-top");

    static final public GtkStockItem GO_BACK = new GtkStockItem("gtk-go-back");

    static final public GtkStockItem GO_DOWN = new GtkStockItem("gtk-go-down");

    static final public GtkStockItem GO_FORWARD = new GtkStockItem(
            "gtk-go-forward");

    static final public GtkStockItem GO_UP = new GtkStockItem("gtk-go-up");

    static final public GtkStockItem HARDDISK = new GtkStockItem("gtk-harddisk");

    static final public GtkStockItem HELP = new GtkStockItem("gtk-help");

    static final public GtkStockItem HOME = new GtkStockItem("gtk-home");

    static final public GtkStockItem INFO = new GtkStockItem("gtk-info");

    static final public GtkStockItem INDEX = new GtkStockItem("gtk-index");

    static final public GtkStockItem INDENT = new GtkStockItem("gtk-indent");

    static final public GtkStockItem ITALIC = new GtkStockItem("gtk-italic");

    static final public GtkStockItem JUMP_TO = new GtkStockItem("gtk-jump-to");

    static final public GtkStockItem JUSTIFY_CENTER = new GtkStockItem(
            "gtk-justify-center");

    static final public GtkStockItem JUSTIFY_FILL = new GtkStockItem(
            "gtk-justify-fill");

    static final public GtkStockItem JUSTIFY_LEFT = new GtkStockItem(
            "gtk-justify-left");

    static final public GtkStockItem JUSTIFY_RIGHT = new GtkStockItem(
            "gtk-justify-right");

    static final public GtkStockItem LEAVE_FULLSCREEN = new GtkStockItem(
            "gtk-leave-fullscreen");

    static final public GtkStockItem MEDIA_FORWARD = new GtkStockItem(
            "gtk-media-forward");

    static final public GtkStockItem MEDIA_NEXT = new GtkStockItem(
            "gtk-media-next");

    static final public GtkStockItem MEDIA_PAUSE = new GtkStockItem(
            "gtk-media-pause");

    static final public GtkStockItem MEDIA_PLAY = new GtkStockItem(
            "gtk-media-play");

    static final public GtkStockItem MEDIA_PREVIOUS = new GtkStockItem(
            "gtk-media-previous");

    static final public GtkStockItem MEDIA_RECORD = new GtkStockItem(
            "gtk-media-record");

    static final public GtkStockItem MEDIA_REWIND = new GtkStockItem(
            "gtk-media-rewind");

    static final public GtkStockItem MEDIA_STOP = new GtkStockItem(
            "gtk-media-stop");

    static final public GtkStockItem MISSING_IMAGE = new GtkStockItem(
            "gtk-missing-image");

    static final public GtkStockItem NETWORK = new GtkStockItem("gtk-network");

    static final public GtkStockItem NEW = new GtkStockItem("gtk-new");

    static final public GtkStockItem NO = new GtkStockItem("gtk-no");

    static final public GtkStockItem OK = new GtkStockItem("gtk-ok");

    static final public GtkStockItem OPEN = new GtkStockItem("gtk-open");

    static final public GtkStockItem PASTE = new GtkStockItem("gtk-paste");

    static final public GtkStockItem PREFERENCES = new GtkStockItem(
            "gtk-preferences");

    static final public GtkStockItem PRINT = new GtkStockItem("gtk-print");

    static final public GtkStockItem PRINT_PREVIEW = new GtkStockItem(
            "gtk-print-preview");

    static final public GtkStockItem PROPERTIES = new GtkStockItem(
            "gtk-properties");

    static final public GtkStockItem QUIT = new GtkStockItem("gtk-quit");

    static final public GtkStockItem REDO = new GtkStockItem("gtk-redo");

    static final public GtkStockItem REFRESH = new GtkStockItem("gtk-refresh");

    static final public GtkStockItem REMOVE = new GtkStockItem("gtk-remove");

    static final public GtkStockItem REVERT_TO_SAVED = new GtkStockItem(
            "gtk-revert-to-saved");

    static final public GtkStockItem SAVE = new GtkStockItem("gtk-save");

    static final public GtkStockItem SAVE_AS = new GtkStockItem("gtk-save-as");

    static final public GtkStockItem SELECT_COLOR = new GtkStockItem(
            "gtk-select-color");

    static final public GtkStockItem SELECT_FONT = new GtkStockItem(
            "gtk-select-font");

    static final public GtkStockItem SORT_ASCENDING = new GtkStockItem(
            "gtk-sort-ascending");

    static final public GtkStockItem SORT_DESCENDING = new GtkStockItem(
            "gtk-sort-descending");

    static final public GtkStockItem SPELL_CHECK = new GtkStockItem(
            "gtk-spell-check");

    static final public GtkStockItem STOP = new GtkStockItem("gtk-stop");

    static final public GtkStockItem STRIKETHROUGH = new GtkStockItem(
            "gtk-strikethrough");

    static final public GtkStockItem UNDELETE = new GtkStockItem("gtk-undelete");

    static final public GtkStockItem UNDERLINE = new GtkStockItem(
            "gtk-underline");

    static final public GtkStockItem UNDO = new GtkStockItem("gtk-undo");

    static final public GtkStockItem UNINDENT = new GtkStockItem("gtk-unindent");

    static final public GtkStockItem YES = new GtkStockItem("gtk-yes");

    static final public GtkStockItem ZOOM_100 = new GtkStockItem("gtk-zoom-100");

    static final public GtkStockItem ZOOM_FIT = new GtkStockItem("gtk-zoom-fit");

    static final public GtkStockItem ZOOM_IN = new GtkStockItem("gtk-zoom-in");

    static final public GtkStockItem ZOOM_OUT = new GtkStockItem("gtk-zoom-out");

    /**
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public GtkStockItem UNIDENT = new GtkStockItem("gtk-unindent");
    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
