/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Used for justifying the text inside a Label widget.
 * 
 * <pre>
 *  JUSTIFY_LEFT 
 *  The text is placed at the left edge of the label.
 *  JUSTIFY_RIGHT 
 *  The text is placed at the right edge of the label.
 *  JUSTIFY_CENTER 
 *  The text is placed in the center of the label.
 *  JUSTIFY_FILL 
 *  The text is placed is distributed across the label.
 * </pre>
 * 
 * @see org.gnu.gtk.Adjustment todo: update doc
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Justification</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Justification extends Enum {

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LEFT = 0;

    static final public org.gnu.gtk.Justification LEFT = new org.gnu.gtk.Justification(
            _LEFT);

    static final private int _RIGHT = 1;

    static final public org.gnu.gtk.Justification RIGHT = new org.gnu.gtk.Justification(
            _RIGHT);

    static final private int _CENTER = 2;

    static final public org.gnu.gtk.Justification CENTER = new org.gnu.gtk.Justification(
            _CENTER);

    static final private int _FILL = 3;

    static final public org.gnu.gtk.Justification FILL = new org.gnu.gtk.Justification(
            _FILL);

    static final private org.gnu.gtk.Justification[] theInterned = new org.gnu.gtk.Justification[] {
            LEFT, RIGHT, CENTER, FILL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.Justification theSacrificialOne = new org.gnu.gtk.Justification(
            0);

    static public org.gnu.gtk.Justification intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.Justification already = (org.gnu.gtk.Justification) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.Justification(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Justification(int value) {
        value_ = value;
    }

    public org.gnu.gtk.Justification or(org.gnu.gtk.Justification other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.Justification and(org.gnu.gtk.Justification other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.Justification xor(org.gnu.gtk.Justification other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.Justification other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
