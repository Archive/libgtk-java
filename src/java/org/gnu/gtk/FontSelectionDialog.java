/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The FontSelectionDialog widget is a dialog for selecting a font. Filters can
 * be used to limit the fonts shown. There are 2 filters in the
 * FileSelectionDialog - a base filter and a user filter. The base filter cannot
 * be changed by the user, so this can be used when the user must choose from
 * the restricted set of fonts. The user filter can be changed or reset by the
 * user, using the 'Reset Filter' button or changing the options on the 'Filter'
 * page of the dialog.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FontSelectionDialog</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FontSelectionDialog extends Dialog {

    /**
     * Construct a new FontSelectionDialog object.
     * 
     * @param title
     *            The title for the dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontSelectionDialog(String title) {
        super(gtk_font_selection_dialog_new(title));
    }

    /**
     * Construct a FontSelectionDialog using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontSelectionDialog(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static FontSelectionDialog getFontSelectionDialog(Handle handle) {
        if (handle == null)
            return null;

        FontSelectionDialog obj = (FontSelectionDialog) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FontSelectionDialog(handle);

        return obj;
    }

    /**
     * Gets the currently selected font name.
     * 
     * @return The name of the currently selected font.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFontName() {
        return FontSelectionDialog
                .gtk_font_selection_dialog_get_font_name(getHandle());
    }

    /**
     * Sets the currently selected font.
     * 
     * @param fontName
     *            The name of the font to set as the currently selected font.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFontname(String fontName) {
        FontSelectionDialog.gtk_font_selection_dialog_set_font_name(
                getHandle(), fontName);
    }

    /**
     * Sets the text displayed in the preview area.
     * 
     * @param text
     *            The text to display in the preview area.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPreviewText(String text) {
        FontSelectionDialog.gtk_font_selection_dialog_set_preview_text(
                getHandle(), text);
    }

    /**
     * Return the OK Button widget for this Dialog.
     * 
     * @return The OK Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getOKButton() {
        Handle hndl = FontSelectionDialog.getOkButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Apply Button widget for this Dialog.
     * 
     * @return The Apply Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getApplyButton() {
        Handle hndl = FontSelectionDialog.getApplyButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Cancel Button widget for this dialog.
     * 
     * @return The Cancel Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getCancelButton() {
        Handle hndl = FontSelectionDialog.getCancelButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_font_selection_dialog_get_type());
    }

    native static final protected Handle getFontsel(Handle cptr);

    native static final protected Handle getMainVbox(Handle cptr);

    native static final protected Handle getOkButton(Handle cptr);

    native static final protected Handle getApplyButton(Handle cptr);

    native static final protected Handle getCancelButton(Handle cptr);

    native static final protected int getDialogWidth(Handle cptr);

    native static final protected boolean getAutoResize(Handle cptr);

    native static final protected int gtk_font_selection_dialog_get_type();

    native static final protected Handle gtk_font_selection_dialog_new(
            String title);

    native static final protected String gtk_font_selection_dialog_get_font_name(
            Handle fontsel);

    native static final protected boolean gtk_font_selection_dialog_set_font_name(
            Handle fontsel, String fontname);

    native static final protected String gtk_font_selection_dialog_get_preview_text(
            Handle fontsel);

    native static final protected void gtk_font_selection_dialog_set_preview_text(
            Handle fontsel, String text);

}
