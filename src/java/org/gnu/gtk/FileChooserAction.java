/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import java.util.Hashtable;

import org.gnu.glib.Enum;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileChooserAction</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileChooserAction extends Enum {
    static final private int _ACTION_OPEN = 0;

    static final public FileChooserAction ACTION_OPEN = new FileChooserAction(
            _ACTION_OPEN);

    static final private int _ACTION_SAVE = 1;

    static final public FileChooserAction ACTION_SAVE = new FileChooserAction(
            _ACTION_SAVE);

    static final private int _ACTION_SELECT_FOLDER = 2;

    static final public FileChooserAction ACTION_SELECT_FOLDER = new FileChooserAction(
            _ACTION_SELECT_FOLDER);

    static final private int _ACTION_CREATE_FOLDER = 3;

    static final public FileChooserAction ACTION_CREATE_FOLDER = new FileChooserAction(
            _ACTION_CREATE_FOLDER);

    static final private FileChooserAction[] theInterned = new FileChooserAction[] {
            ACTION_OPEN, ACTION_SAVE, ACTION_SELECT_FOLDER,
            ACTION_CREATE_FOLDER };

    static private java.util.Hashtable theInternedExtras;

    static final private FileChooserAction theSacrificialOne = new FileChooserAction(
            0);

    static public FileChooserAction intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new Hashtable();
        }
        FileChooserAction already = (FileChooserAction) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileChooserAction(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileChooserAction(int value) {
        value_ = value;
    }

    public FileChooserAction or(FileChooserAction other) {
        return intern(value_ | other.value_);
    }

    public FileChooserAction and(FileChooserAction other) {
        return intern(value_ & other.value_);
    }

    public FileChooserAction xor(FileChooserAction other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileChooserAction other) {
        return (value_ & other.value_) == other.value_;
    }
}
