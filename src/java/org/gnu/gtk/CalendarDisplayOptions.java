/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

/**
 * Defines the appearance of a Calendar object.
 * 
 * <pre>
 *  SHOW_HEADING
 *  Specifies that the month and year should be displayed.
 *  SHOW_DAY_NAMES
 *  Specifies that three letter day descriptions should be displayed.
 *  NO_MONTH_CHANGE
 *  Prevents the user from switching months with the calendar
 *  SHOW_WEEK_NUMBERS
 *  Displays each week numbers of the current year down the left side of
 *  the calendar
 *  WEEK_START_MONDAY
 *  Starts the calendar week on Monday, instead of the default Sunday.
 * </pre>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CalendarDisplayOptions</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CalendarDisplayOptions extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _SHOW_HEADING = 1 << 0;

    static final public org.gnu.gtk.CalendarDisplayOptions SHOW_HEADING = new org.gnu.gtk.CalendarDisplayOptions(
            _SHOW_HEADING);

    static final private int _SHOW_DAY_NAMES = 1 << 1;

    static final public org.gnu.gtk.CalendarDisplayOptions SHOW_DAY_NAMES = new org.gnu.gtk.CalendarDisplayOptions(
            _SHOW_DAY_NAMES);

    static final private int _NO_MONTH_CHANGE = 1 << 2;

    static final public org.gnu.gtk.CalendarDisplayOptions NO_MONTH_CHANGE = new org.gnu.gtk.CalendarDisplayOptions(
            _NO_MONTH_CHANGE);

    static final private int _SHOW_WEEK_NUMBERS = 1 << 3;

    static final public org.gnu.gtk.CalendarDisplayOptions SHOW_WEEK_NUMBERS = new org.gnu.gtk.CalendarDisplayOptions(
            _SHOW_WEEK_NUMBERS);

    static final private int _WEEK_START_MONDAY = 1 << 4;

    /**
     * Currently the locale determines if the week begins on Monday. This value
     * should not be used.
     * 
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.CalendarDisplayOptions WEEK_START_MONDAY = new org.gnu.gtk.CalendarDisplayOptions(
            _WEEK_START_MONDAY);

    static final private org.gnu.gtk.CalendarDisplayOptions[] theInterned = new org.gnu.gtk.CalendarDisplayOptions[] {
            new org.gnu.gtk.CalendarDisplayOptions(0), SHOW_HEADING,
            SHOW_DAY_NAMES, new org.gnu.gtk.CalendarDisplayOptions(3),
            NO_MONTH_CHANGE, new org.gnu.gtk.CalendarDisplayOptions(5),
            new org.gnu.gtk.CalendarDisplayOptions(6),
            new org.gnu.gtk.CalendarDisplayOptions(7), SHOW_WEEK_NUMBERS,
            new org.gnu.gtk.CalendarDisplayOptions(9),
            new org.gnu.gtk.CalendarDisplayOptions(10),
            new org.gnu.gtk.CalendarDisplayOptions(11),
            new org.gnu.gtk.CalendarDisplayOptions(12),
            new org.gnu.gtk.CalendarDisplayOptions(13),
            new org.gnu.gtk.CalendarDisplayOptions(14),
            new org.gnu.gtk.CalendarDisplayOptions(15), WEEK_START_MONDAY }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.CalendarDisplayOptions theSacrificialOne = new org.gnu.gtk.CalendarDisplayOptions(
            0);

    static public org.gnu.gtk.CalendarDisplayOptions intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.CalendarDisplayOptions already = (org.gnu.gtk.CalendarDisplayOptions) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.CalendarDisplayOptions(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CalendarDisplayOptions(int value) {
        value_ = value;
    }

    public org.gnu.gtk.CalendarDisplayOptions or(
            org.gnu.gtk.CalendarDisplayOptions other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.CalendarDisplayOptions and(
            org.gnu.gtk.CalendarDisplayOptions other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.CalendarDisplayOptions xor(
            org.gnu.gtk.CalendarDisplayOptions other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.CalendarDisplayOptions other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
