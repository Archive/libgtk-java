/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.HashMap;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Class for sorting the rows of a base {@link TreeModel}. The underlying
 * TreeModel us sorted using a comparison function. The comparison function is
 * set with the <tt>{@link #setSortMethod}</tt> method.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeModelSort</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TreeModelSort extends TreeModel implements TreeSortable {

    // Hash to store the TreeIterComparison objects used to sort a column.
    protected HashMap sortMethods = null;

    /** For internal use only. */
    protected TreeModelSort(Handle handle) {
        super(handle);
    }

    /** For internal use only. */
    protected static TreeModelSort getTreeModelSort(Handle handle) {
        if (handle == null)
            return null;

        TreeModelSort obj = (TreeModelSort) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TreeModelSort(handle);

        return obj;
    }

    /**
     * Create a new TreeModelSort using the given TreeModel as the underlying
     * data model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeModelSort(TreeModel model) {
        super(gtk_tree_model_sort_new_with_model(model.getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_tree_model_sort_get_type());
    }

    /**
     * Return the underlying data model.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeModel getModel() {
        return (TreeModel) getGObjectFromHandle(gtk_tree_model_sort_get_model(getHandle()));
    }

    /**
     * Set the column in the list to sort on.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortColumn(DataColumn column, SortType order) {
        TreeSortableHelper.setSortColumn(this, column, order);
    }

    /**
     * Get a DataColumn object representing the currently sorted column. This is
     * not the same DataColumn used to create the store. It is only of type
     * DataColumn (not DataColumnString, etc). It can be compared with another
     * DataColumn object using the <tt>{@link DataColumn#equals}</tt> method.
     * 
     * @return A DataColumn object representing the currently sorted column or
     *         <tt>null</tt> if there is no column currently sorted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataColumn getSortColumn() {
        return TreeSortableHelper.getSortColumn(this);
    }

    /**
     * Get the current sorting order of the store.
     * 
     * @return A SortType object defining the current sorting order of the store
     *         or <tt>null</tt> if there is no current sort order.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SortType getSortOrder() {
        return TreeSortableHelper.getSortOrder(this);
    }

    /**
     * Set the class used to sort the list according to the values stored in the
     * given DataColumn.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortMethod(TreeIterComparison method, DataColumn column) {
        TreeSortableHelper.setSortMethod(this, method, column);
    }

    /**
     * Call-back method invoked by the JNI code when sorting is required. This
     * is for internal use only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int handleCompareFunc(Handle model, Handle aIter, Handle bIter,
            int col) {
        TreeIterComparison method = TreeSortableHelper.getMethod(this, col);
        if (method != null) {
            TreeModel mdl = (TreeModel) getGObjectFromHandle(model);
            TreeIter a = TreeIter.getTreeIter(aIter, mdl);
            TreeIter b = TreeIter.getTreeIter(bIter, mdl);
            return method.compareTreeIters(mdl, a, b);
        } else {
            return 0;
        }
    }

    /**
     * Converts childPath to a path relative to the TreeModelSort.
     * That is, childPath points to a path in the child model.
     * The returned path will point to the same row in the sorted 
     * model. If childPath isn't a valid path on the child model,
     * then NULL is returned.
     * @param childPath A TreePath to convert.
     * @return TreePath, or NULL
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreePath convertChildPathToPath(TreePath childPath){
	    Handle path = gtk_tree_model_sort_convert_child_path_to_path(getHandle(), childPath.getHandle());
	    return TreePath.getTreePath(path);
    }
    
    /**
     * Converts sortedPath to a path on the child model of 
     * the TreeModelSort. That is, sortedPath points to a location
     * in the TreeModelSort.
     * The returned path will point to the same location in the
     * model not being sorted. If sortedPath does not point to a
     * location in the child model, NULL is returned.
     * @param sortedPath A TreePath to convert.
     * @return A GtkTreePath, or NULL
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreePath convertPathToChildPath(TreePath sortedPath){
	    Handle childPath = gtk_tree_model_sort_convert_path_to_child_path(getHandle(), sortedPath.getHandle());
	    return TreePath.getTreePath(childPath);
    }
        
    native static final protected int gtk_tree_model_sort_get_type();

    native static final protected Handle gtk_tree_model_sort_new_with_model(
            Handle childModel);

    native static final protected Handle gtk_tree_model_sort_get_model(
            Handle treeModel);

    native static final protected Handle gtk_tree_model_sort_convert_child_path_to_path(
            Handle treeModel, Handle childPath);

    native static final protected Handle gtk_tree_model_sort_convert_path_to_child_path(
		    Handle treeModel, Handle sortedPath);

    
    //  Implemented in C, but not in Java.
    native static final protected void gtk_tree_model_sort_convert_iter_to_child_iter(
		    Handle treeModel, Handle childIter, Handle sortedIter);

    native static final protected void gtk_tree_model_sort_convert_child_iter_to_iter(
	            Handle treeModel, Handle childIter, Handle sortIter);

    native static final protected void gtk_tree_model_sort_clear_cache(
	            Handle treeModel);

    
    // Not implemented.
    // native static final protected void
    // gtk_tree_model_sort_reset_default_sort_func(Handle treeModel);
    // native static final protected boolean
    // gtk_tree_model_sort_iter_is_valid(Handle treeModel, Handle iter);
}
