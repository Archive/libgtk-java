/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Atom;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * Data used in Drag and drop operations
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SelectionData</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SelectionData extends Boxed {
    public SelectionData(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static SelectionData getSelectionData(Handle handle) {
        if (handle == null)
            return null;

        SelectionData obj = (SelectionData) Boxed.getBoxedFromHandle(handle);
        if (obj == null)
            obj = new SelectionData(handle);

        return obj;
    }

    public void setTarget(Atom target) {
        setTarget(getHandle(), target.getHandle());
    }

    public Atom getSelection() {
        return new Atom(getSelection(getHandle()));
    }

    public Atom getTarget() {
        return new Atom(getTarget(getHandle()));
    }

    public Atom getType() {
        Handle handle = getType(getHandle());
        return handle == null ? null : new Atom(handle);
    }

    public int getFormat() {
        return getFormat(getHandle());
    }

    public int getLength() {
        return getLength(getHandle());
    }

    public byte[] getData()
    {
        return getData(getHandle());
    }

    public void setData(byte[] data, String type)
    {
        setData(getHandle(), new Atom(type, false).getHandle(), 8, data, data.length);
    }

    public String getText() {
        return gtk_selection_data_get_text(getHandle());
    }

    public boolean setText(String text) {
        setTarget(new Atom("UTF8_STRING", false));
        return gtk_selection_data_set_text(getHandle(), text);
    }

    /**
     * Given that this SelectionData object holds a list of targets, determines
     * if any of the targets in targets can be used to provide text.
     * 
     * @return TRUE if this SelectionData holds a list of targets, and a
     *         suitable target for text is included, otherwise FALSE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean targetsIncludText() {
        return gtk_selection_data_targets_include_text(getHandle());
    }

    /**
     * Given that this SelectionData object holds a list of targets, determines
     * if any of the targets in targets can be used to provide a
     * {@link org.gnu.gdk.Pixbuf}.
     * 
     * @param writable
     *            Whether to accept only targets for which GTK+ knows how to
     *            convert a pixbuf into the format.
     * @return TRUE if this SelectionData holds a list of targets, and a
     *         suitable target for images is included, otherwise FALSE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean targetsIncludeImage(boolean writable) {
        return gtk_selection_data_targets_include_image(getHandle(), writable);
    }

    public Atom[] getTargets() {
        Handle hndls[] = gtk_selection_data_get_targets(getHandle());
        if (null == hndls)
            return null;
        Atom[] value = new Atom[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            value[i] = new Atom(hndls[i]);
        }
        return value;
    }

    public boolean setPixbuf(Pixbuf pixbuf) {
        return gtk_selection_data_set_pixbuf(getHandle(), pixbuf.getHandle());
    }

    public Pixbuf getPixbuf() {
        Handle handle = gtk_selection_data_get_pixbuf(getHandle());
        return handle == null ? null : new Pixbuf(handle);
    }

    public boolean setUris(String[] uris) {
        return gtk_selection_data_set_uris(getHandle(), uris);
    }

    public String[] getUris() {
        return gtk_selection_data_get_uris(getHandle());
    }

    public int getInt() {
        return Integer.parseInt(getText());
    }

    public double getDouble() {
        return Double.parseDouble(getText());
    }

    public long getLong() {
        return Long.parseLong(getText());
    }

    public void setInt(int data) {
        setText(String.valueOf(data));
    }

    public void setLong(long data) {
        setText(String.valueOf(data));
    }

    public void setDouble(double data) {
        setText(String.valueOf(data));
    }

    native static protected final Handle getSelection(Handle selectionData);

    native static protected final Handle getTarget(Handle selectionData);

    native static protected final Handle getType(Handle selectionData);

    native static protected final int getFormat(Handle selectionData);

    native static protected final int getLength(Handle selectionData);

    native static protected final Handle getDisplay(Handle selectionData);

    native static protected final byte[] getData(Handle selectionData);

    native static protected final void setData(Handle selectionData, Handle type, int format, byte[] data, int length);

    native static protected final String gtk_selection_data_get_text(
            Handle selectionData);

    native static protected final boolean gtk_selection_data_set_text(
            Handle selectionData, String str);

    native static protected final Handle[] gtk_selection_data_get_targets(
            Handle selectionData);

    native static protected final boolean gtk_selection_data_targets_include_text(
            Handle selectionData);

    native static final private boolean gtk_selection_data_targets_include_image(
            Handle selection_data, boolean writable);

    native static protected final boolean gtk_selection_data_set_pixbuf(
            Handle selectionData, Handle pixbuf);

    native static protected final Handle gtk_selection_data_get_pixbuf(
            Handle selectionData);

    native static protected final boolean gtk_selection_data_set_uris(
            Handle selectionData, String[] uris);

    native static protected final String[] gtk_selection_data_get_uris(
            Handle selectionData);

    native static private final void setTarget(Handle selectionData, Handle atom);
}