/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.CellRendererTextEvent;
import org.gnu.gtk.event.CellRendererTextListener;
import org.gnu.glib.Handle;
import org.gnu.pango.FontDescription;

/**
 * A {@link CellRenderer} for displaying Strings.
 * <p>
 * See the {@link TreeView} description for an overview fo the tree and list
 * objects.
 * 
 * <p>
 * Gtk provides many properties for this renderer. Not all of them have been
 * implemented in the Java-Gnome system (they're not all useful). If you have
 * requirements for one of them which isn't implemented, please contact us and
 * we may add support for it.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRendererText</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRendererText extends CellRenderer {
    /**
     * Constructs a new text renderer
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CellRendererText() {
        super(gtk_cell_renderer_text_new());
    }

    public CellRendererText(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static CellRendererText getCellRendererText(Handle handle) {
        if (handle == null)
            return null;

        CellRendererText obj = (CellRendererText) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new CellRendererText(handle);

        return obj;
    }

    /**
     * Attributes which may be used to connect to data in a {@link TreeModel}
     * via methods in the {@link TreeViewColumn} object.
     * 
     * <p>
     * Note that these should usually be set to PANGO constants.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static class Attribute extends CellRenderer.Attribute {
        private Attribute(String attrib) {
            super(attrib);
        }

        /** (String) Text to render */
        public static final Attribute TEXT = new Attribute("text");

        /** (String) Marked up text to render */
        public static final Attribute MARKUP = new Attribute("markup");

        /**
         * (pango.AttrList) A list of style attributes to apply to the text of
         * the renderer
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute ATTRIBUTES = new Attribute("attributes");

        /** (String) Background color */
        public static final Attribute BACKGROUND = new Attribute("background");

        /** (gdk.Color) Background color */
        public static final Attribute BACKGROUND_GDK = new Attribute(
                "background_gdk");

        /** (String) Foreground color */
        public static final Attribute FOREGROUND = new Attribute("foreground");

        /** (gdk.Color) Foreground color */
        public static final Attribute FOREGROUND_GDK = new Attribute(
                "foreground_gdk");

        /** (boolean) Whether the text can be modified by the user */
        public static final Attribute EDITABLE = new Attribute("editable");

        /** (String) Font description as a string */
        public static final Attribute FONT = new Attribute("font");

        /**
         * (String) Name of the font family, e.g. Sans, Helvetica, Times,
         * Monospace
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute FONT_FAMILY = new Attribute("family");

        /** (Int) Font Weight (see pango.Weight) */
        public static final Attribute WEIGHT = new Attribute("weight");

        /** (int) Font Size */
        public static final Attribute SIZE = new Attribute("size");

        /** (double) Font scaling factor */
        public static final Attribute SCALE = new Attribute("scale");

        /**
         * (int) Offset of text above the baseline (below the baseline if rise
         * is negative)
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute RISE = new Attribute("rise");

        /** (boolean) Whether to strike through the text */
        public static final Attribute STRIKETHROUGH = new Attribute(
                "strikethrough");

        /** (pango.Underline) Style of underline for this text */
        public static final Attribute UNDERLINE = new Attribute("underline");
       
    }

    /**
     * Sets the pango attributes for the renderer.
     * 
     * @param attr
     *            PangoAttributeList to use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAttributes(org.gnu.pango.AttrList attr) {
        gtk_setAttributes(getHandle(), attr.getHandle());
    }

    /**
     * Sets the font to use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFont(org.gnu.pango.FontDescription font) {
        gtk_setFont(getHandle(), font.getHandle());
    }

    /**
     * Returns the font being used.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription getFont() {
        return new FontDescription(gtk_getFont(getHandle()));
    }

    /**
     * Sets whether the user may edit the text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEditable(boolean editable) {
        gtk_setProperty(getHandle(), "editable", editable);
    }

    /**
     * Sets the underline of all cells
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUnderline(org.gnu.pango.Underline underline) {
        gtk_setUnderLine(getHandle(), underline.getValue());
    }

    /***************************************************************************
     * Event Handling
     **************************************************************************/

    /** Listeners for handling CellRendererText events */
    private Vector CellRendererTextListeners = null;

    /**
     * Register an object to handle CellRendererText events.
     * 
     * @see org.gnu.gtk.event.CellRendererTextListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(CellRendererTextListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = Widget.findListener(CellRendererTextListeners, listener);
        if (i == -1) {
            if (null == CellRendererTextListeners) {
                evtMap.initialize(this, CellRendererTextEvent.Type.EDITED);
                CellRendererTextListeners = new Vector();
            }
            CellRendererTextListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(CellRendererTextListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(CellRendererTextListener listener) {
        int i = Widget.findListener(CellRendererTextListeners, listener);
        if (i > -1)
            CellRendererTextListeners.remove(i);
        if (0 == CellRendererTextListeners.size()) {
            evtMap.uninitialize(this, CellRendererTextEvent.Type.EDITED);
            CellRendererTextListeners = null;
        }
    }

    protected void fireCellRendererTextEvent(CellRendererTextEvent event) {
        if (null == CellRendererTextListeners)
            return;
        int size = CellRendererTextListeners.size();
        int i = 0;
        while (i < size) {
            CellRendererTextListener sl = (CellRendererTextListener) CellRendererTextListeners
                    .elementAt(i);
            sl.cellRendererTextEvent(event);
            i++;
        }
    }

    private void handleEdited(String arg1, String arg2) {
        fireCellRendererTextEvent(new CellRendererTextEvent(this, arg1, arg2));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("edited", "handleEdited",
                CellRendererTextEvent.Type.EDITED,
                CellRendererTextListener.class);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_cell_renderer_text_get_type());
    }

    native static final protected int gtk_cell_renderer_text_get_type();

    native static final protected Handle gtk_cell_renderer_text_new();

    native static final protected void gtk_cell_renderer_text_set_fixed_height_from_font(
            Handle renderer, int numberOfRows);

    native static final protected void gtk_setAttributes(Handle handle,
            Handle attr);

    native static final protected void gtk_setFont(Handle handle, Handle font);

    native static final protected Handle gtk_getFont(Handle handle);

    native static final protected void gtk_setProperty(Handle handle,
            String property, boolean setting);

    native static final protected void gtk_setUnderLine(Handle handle,
            int underline);

}
