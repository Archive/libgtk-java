/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * The Progress widget is the base class of {@link ProgressBar}. The Progress
 * class exists in Gtk for compatibility with programs written prior to Gtk2.
 * All methods have been deprecated and so are not present in Java-Gnome
 * 
 * @see ProgressBar
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Progress</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Progress extends Widget {
    Progress(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Progress getProgress(Handle handle) {
        if (handle == null)
            return null;

        Progress obj = (Progress) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Progress(handle);

        return obj;
    }

    //
    // This class is all deprecated as of Gnome 2
    //

    native static final protected double getXAlign(Handle cptr);

    native static final protected double getYAlign(Handle cptr);

    native static final protected boolean getShowText(Handle cptr);

    native static final protected boolean getActivityMode(Handle cptr);

    native static final protected boolean getUseTextFormat(Handle cptr);

    native static final protected String getFormat(Handle cptr);

    native static final protected Handle getAdjustment(Handle cptr);

    native static final protected Handle getOffscreenPixmap(Handle cptr);
}
