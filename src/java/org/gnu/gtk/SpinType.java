/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * A property used to determine how to sping a {@link SpinButton} widget.
 * 
 * todo: docs GTK_SPIN_STEP_FORWARD, GTK_SPIN_STEP_BACKWARD,
 * GTK_SPIN_PAGE_FORWARD, GTK_SPIN_PAGE_BACKWARDThese values spin a
 * GtkSpinButton by the relevant values of the spin button's
 * GtkAdjustment.GTK_SPIN_HOME, GTK_SPIN_ENDThese set the spin button's value to
 * the minimum or maxmimum possible values, (set by it's GtkAdjustment),
 * respectively.GTK_SPIN_USER_DEFINEDThe programmer must specify the exact
 * amount to spin the GtkSpinButton.
 * 
 * @see SpinButton
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SpinType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SpinType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _STEP_FORWARD = 0;

    static final public org.gnu.gtk.SpinType STEP_FORWARD = new org.gnu.gtk.SpinType(
            _STEP_FORWARD);

    static final private int _STEP_BACKWARD = 1;

    static final public org.gnu.gtk.SpinType STEP_BACKWARD = new org.gnu.gtk.SpinType(
            _STEP_BACKWARD);

    static final private int _PAGE_FORWARD = 2;

    static final public org.gnu.gtk.SpinType PAGE_FORWARD = new org.gnu.gtk.SpinType(
            _PAGE_FORWARD);

    static final private int _PAGE_BACKWARD = 3;

    static final public org.gnu.gtk.SpinType PAGE_BACKWARD = new org.gnu.gtk.SpinType(
            _PAGE_BACKWARD);

    static final private int _HOME = 4;

    static final public org.gnu.gtk.SpinType HOME = new org.gnu.gtk.SpinType(
            _HOME);

    static final private int _END = 5;

    static final public org.gnu.gtk.SpinType END = new org.gnu.gtk.SpinType(
            _END);

    static final private int _USER_DEFINED = 6;

    static final public org.gnu.gtk.SpinType USER_DEFINED = new org.gnu.gtk.SpinType(
            _USER_DEFINED);

    static final private org.gnu.gtk.SpinType[] theInterned = new org.gnu.gtk.SpinType[] {
            STEP_FORWARD, STEP_BACKWARD, PAGE_FORWARD, PAGE_BACKWARD, HOME,
            END, USER_DEFINED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SpinType theSacrificialOne = new org.gnu.gtk.SpinType(
            0);

    static public org.gnu.gtk.SpinType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SpinType already = (org.gnu.gtk.SpinType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SpinType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SpinType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SpinType or(org.gnu.gtk.SpinType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SpinType and(org.gnu.gtk.SpinType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SpinType xor(org.gnu.gtk.SpinType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SpinType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
