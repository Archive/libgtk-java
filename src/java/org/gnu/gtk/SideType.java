/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class SideType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TOP = 0;

    static final public org.gnu.gtk.SideType TOP = new org.gnu.gtk.SideType(
            _TOP);

    static final private int _BOTTOM = 1;

    static final public org.gnu.gtk.SideType BOTTOM = new org.gnu.gtk.SideType(
            _BOTTOM);

    static final private int _LEFT = 2;

    static final public org.gnu.gtk.SideType LEFT = new org.gnu.gtk.SideType(
            _LEFT);

    static final private int _RIGHT = 3;

    static final public org.gnu.gtk.SideType RIGHT = new org.gnu.gtk.SideType(
            _RIGHT);

    static final private org.gnu.gtk.SideType[] theInterned = new org.gnu.gtk.SideType[] {
            TOP, BOTTOM, LEFT, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SideType theSacrificialOne = new org.gnu.gtk.SideType(
            0);

    static public org.gnu.gtk.SideType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SideType already = (org.gnu.gtk.SideType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SideType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SideType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SideType or(org.gnu.gtk.SideType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SideType and(org.gnu.gtk.SideType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SideType xor(org.gnu.gtk.SideType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SideType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
