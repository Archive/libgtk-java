/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class ResponseType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = -1;

    static final public org.gnu.gtk.ResponseType NONE = new org.gnu.gtk.ResponseType(
            _NONE);

    static final private int _REJECT = -2;

    static final public org.gnu.gtk.ResponseType REJECT = new org.gnu.gtk.ResponseType(
            _REJECT);

    static final private int _ACCEPT = -3;

    static final public org.gnu.gtk.ResponseType ACCEPT = new org.gnu.gtk.ResponseType(
            _ACCEPT);

    static final private int _DELETE_EVENT = -4;

    static final public org.gnu.gtk.ResponseType DELETE_EVENT = new org.gnu.gtk.ResponseType(
            _DELETE_EVENT);

    static final private int _OK = -5;

    static final public org.gnu.gtk.ResponseType OK = new org.gnu.gtk.ResponseType(
            _OK);

    static final private int _CANCEL = -6;

    static final public org.gnu.gtk.ResponseType CANCEL = new org.gnu.gtk.ResponseType(
            _CANCEL);

    static final private int _CLOSE = -7;

    static final public org.gnu.gtk.ResponseType CLOSE = new org.gnu.gtk.ResponseType(
            _CLOSE);

    static final private int _YES = -8;

    static final public org.gnu.gtk.ResponseType YES = new org.gnu.gtk.ResponseType(
            _YES);

    static final private int _NO = -9;

    static final public org.gnu.gtk.ResponseType NO = new org.gnu.gtk.ResponseType(
            _NO);

    static final private int _APPLY = -10;

    static final public org.gnu.gtk.ResponseType APPLY = new org.gnu.gtk.ResponseType(
            _APPLY);

    static final private int _HELP = -11;

    static final public org.gnu.gtk.ResponseType HELP = new org.gnu.gtk.ResponseType(
            _HELP);

    static final private org.gnu.gtk.ResponseType[] theInterned = new org.gnu.gtk.ResponseType[] {
            null, NONE, REJECT, ACCEPT, DELETE_EVENT, OK, CANCEL, CLOSE, YES,
            NO, APPLY, HELP }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ResponseType theSacrificialOne = new org.gnu.gtk.ResponseType(
            0);

    static public org.gnu.gtk.ResponseType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[-value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ResponseType already = (org.gnu.gtk.ResponseType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ResponseType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ResponseType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ResponseType or(org.gnu.gtk.ResponseType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ResponseType and(org.gnu.gtk.ResponseType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ResponseType xor(org.gnu.gtk.ResponseType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ResponseType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
