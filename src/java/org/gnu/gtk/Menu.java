/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A Menu is a MenuShell that implements a drop down menu consisting of a list
 * of MenuItem objects which can be navigated and activated by the user to
 * perform application functions.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Menu</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Menu extends MenuShell {

    /**
     * Create a new Menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Menu() {
        super(gtk_menu_new());
    }

    /**
     * Create a new Menu from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Menu(Handle handle) {
        super(handle);
    }

    /**
     * Create a new Menu from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Menu getMenu(Handle handle) {
        if (handle == null)
            return null;

        Menu obj = (Menu) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Menu(handle);

        return obj;
    }

    /**
     * Move a MenuItem to a new position within the Menu.
     * 
     * @param child
     *            The MenuItem to be moved.
     * @param position
     *            The new position to place the MenuItem.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void reorderMenuItem(MenuItem child, int position) {
        Menu.gtk_menu_reorder_child(getHandle(), child.getHandle(), position);
    }

    /**
     * Displays a menu and makes it available for selection. Applications can
     * use this method to provide context-sensitive menus.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void popup() {
        Menu.gtk_menu_popup(getHandle());
    }

    /**
     * Detaches the menu from the widget to which it had been attached.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void detach() {
        Menu.gtk_menu_detach(getHandle());
    }

    /**
     * Set the AccelGroup which holds global accelerators for the menu. The
     * accelerator group needs to also be added to all windows that this menu is
     * being used in.
     * 
     * @param accelGroup
     *            The AccelGroup to be associated with this Menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAccelGroup(AccelGroup accelGroup) {
        Menu.gtk_menu_set_accel_group(getHandle(), accelGroup.getHandle());
    }

    /**
     * Sets the title string for this Menu. The title is displayed when the menu
     * is shown as a tearoff menu.
     * 
     * @param title
     *            The title for the Menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        Menu.gtk_menu_set_title(getHandle(), title);
    }

    /**
     * Returns the title of the menu.
     * 
     * @return The title for the menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        return Menu.gtk_menu_get_title(getHandle());
    }

    /**
     * Returns a list of the menus which are attached to this widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ArrayList getAttachedMenus() {
        Handle[] handles = gtk_menu_get_for_attach_widget(getHandle());
        ArrayList ret = new ArrayList(handles.length);
        for (int i = 0; i < handles.length; i++) {
            ret.add(Menu.getMenu(handles[i]));
        }
        return ret;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_menu_get_type());
    }

    native static final protected void gtk_menu_popup(Handle menu);

    native static final protected int gtk_menu_get_type();

    native static final protected Handle gtk_menu_new();

    native static final protected void gtk_menu_reposition(Handle menu);

    native static final protected void gtk_menu_popdown(Handle menu);

    native static final protected Handle gtk_menu_get_active(Handle menu);

    native static final protected void gtk_menu_set_active(Handle menu,
            int index);

    native static final protected void gtk_menu_set_accel_group(Handle menu,
            Handle accelGroup);

    native static final protected Handle gtk_menu_get_accel_group(Handle menu);

    native static final protected void gtk_menu_set_accel_path(Handle menu,
            String accelPath);

    native static final protected void gtk_menu_detach(Handle menu);

    native static final protected Handle gtk_menu_get_attach_widget(Handle menu);

    native static final protected void gtk_menu_set_tearoff_state(Handle menu,
            boolean tornOff);

    native static final protected boolean gtk_menu_get_tearoff_state(Handle menu);

    native static final protected void gtk_menu_set_title(Handle menu,
            String title);

    native static final protected String gtk_menu_get_title(Handle menu);

    native static final protected void gtk_menu_reorder_child(Handle menu,
            Handle child, int position);

    native static final protected void gtk_menu_set_screen(Handle menu,
            Handle screen);

    native static final protected void gtk_menu_attach(Handle menu,
            Handle child, int left, int right, int top, int bottom);

    native static final protected void gtk_menu_set_monitor(Handle menu,
            int monitorNum);

    native static final private Handle[] gtk_menu_get_for_attach_widget(
            Handle widget);

}
