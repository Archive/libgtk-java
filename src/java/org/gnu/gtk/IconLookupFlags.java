/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconLookupFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconLookupFlags extends Flags {

    static final private int _NO_SVG = 1 << 0;

    static final public IconLookupFlags NO_SVG = new IconLookupFlags(_NO_SVG);

    static final private int _FORCE_SVG = 1 << 1;

    static final public IconLookupFlags FORCE_SVG = new IconLookupFlags(
            _FORCE_SVG);

    static final private int _USE_BUILTIN = 1 << 2;

    static final public IconLookupFlags USE_BUILTIN = new IconLookupFlags(
            _USE_BUILTIN);

    static final private IconLookupFlags[] theInterned = new IconLookupFlags[] {
            new IconLookupFlags(0), NO_SVG, FORCE_SVG, new IconLookupFlags(3),
            USE_BUILTIN };

    static private java.util.Hashtable theInternedExtras;

    static final private IconLookupFlags theSacrificialOne = new IconLookupFlags(
            0);

    static public IconLookupFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        IconLookupFlags already = (IconLookupFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new IconLookupFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private IconLookupFlags(int value) {
        value_ = value;
    }

    public IconLookupFlags or(IconLookupFlags other) {
        return intern(value_ | other.value_);
    }

    public IconLookupFlags and(IconLookupFlags other) {
        return intern(value_ & other.value_);
    }

    public IconLookupFlags xor(IconLookupFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(IconLookupFlags other) {
        return (value_ & other.value_) == other.value_;
    }

}
