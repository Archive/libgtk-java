/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Handle;

/**
 * Display information about an application.
 * <p>
 * Gtk's AboutDialog offers a simple way to display information about a program
 * like its logo, name, copyright, website and license. It is also possible to
 * give credits to the authors, documenters, translators and artists who have
 * worked on the program. An about dialog is typically opened when the user
 * selects the About option from the Help menu. All parts of the dialog are
 * optional.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.AboutDialog</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class AboutDialog extends Dialog {

    private static AboutDialogActivateLinkMethod urlMethod = null;

    private static AboutDialogActivateLinkMethod emailMethod = null;

    /**
     * Internal constructor.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AboutDialog(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AboutDialog getAboutDialog(Handle handle) {
        if (handle == null)
            return null;

        AboutDialog obj = (AboutDialog) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new AboutDialog(handle);

        return obj;
    }

    /**
     * Create a new AboutDialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AboutDialog() {
        super(gtk_about_dialog_new());
    }

    /**
     * Sets the name to display in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setName(String val) {
        gtk_about_dialog_set_name(getHandle(), val);
    }

    /**
     * Gets the name displayed in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return gtk_about_dialog_get_name(getHandle());
    }

    /**
     * Sets the version string to display in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVersion(String val) {
        gtk_about_dialog_set_version(getHandle(), val);
    }

    /**
     * Gets the version string displayed in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getVersion() {
        return gtk_about_dialog_get_version(getHandle());
    }

    /**
     * Sets the copyright string to display in the about dialog. This should be
     * a short string of one or two lines.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCopyright(String val) {
        gtk_about_dialog_set_copyright(getHandle(), val);
    }

    /**
     * Gets the copyright string displayed in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getCopyright() {
        return gtk_about_dialog_get_copyright(getHandle());
    }

    /**
     * Sets the comments string to display in the about dialog. This should be a
     * short string of one or two lines.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setComments(String val) {
        gtk_about_dialog_set_comments(getHandle(), val);
    }

    /**
     * Gets the comments string displayed in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getComments() {
        return gtk_about_dialog_get_comments(getHandle());
    }

    /**
     * Sets the license information to be displayed in the secondary license
     * dialog. If license is null, the license button is hidden.
     * 
     * @param val
     *            The license information or NULL.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLicense(String val) {
        gtk_about_dialog_set_license(getHandle(), val);
    }

    /**
     * Gets the license information displayed in the secondary license dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getLicense() {
        return gtk_about_dialog_get_license(getHandle());
    }

    /**
     * Sets whether the license text is automatically wrapped.
     * 
     * @param wrapLicense
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWrapLicense(boolean wrapLicense) {
        gtk_about_dialog_set_wrap_license(getHandle(), wrapLicense);
    }

    /**
     * Gets whether the license text will be automatically wrapped.
     * 
     * @return true if the license text will be wrapped automatically
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getWrapLicense() {
        return gtk_about_dialog_get_wrap_license(getHandle());
    }

    /**
     * Sets the URL to use for the website link.
     * 
     * @param website
     *            A URL string starting with "http://".
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWebsite(String website) {
        gtk_about_dialog_set_website(getHandle(), website);
    }

    /**
     * Gets the URL used for the website link.
     * 
     * @deprecated Use {@link #getWebsite()}.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String setWebsite() {
        return gtk_about_dialog_get_website(getHandle());
    }

    /**
     * Gets the URL used for the website link.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getWebsite() {
        return gtk_about_dialog_get_website(getHandle());
    }

    /**
     * Sets the label to be used for the website link. It defaults to the
     * website URL.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWebsiteLabel(String val) {
        gtk_about_dialog_set_website_label(getHandle(), val);
    }

    /**
     * Sets the label to be used for the website link. It defaults to the
     * website URL.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getWebsiteLabel() {
        return gtk_about_dialog_get_website_label(getHandle());
    }

    /**
     * Sets the strings which are displayed in the authors tab of the secondary
     * credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAuthors(String[] val) {
        gtk_about_dialog_set_authors(getHandle(), val);
    }

    /**
     * Gets the strings which are displayed in the authors tab of the secondary
     * credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getAuthors() {
        return gtk_about_dialog_get_authors(getHandle());
    }

    /**
     * Sets the strings which are displayed in the artists tab of the secondary
     * credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDocumenters(String[] val) {
        gtk_about_dialog_set_documenters(getHandle(), val);
    }

    /**
     * Gets the strings which are displayed in the artists tab of the secondary
     * credits dialog.
     * 
     * @deprecated Use {@link #getDocumenters()} instead.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] setDocumenters() {
        return gtk_about_dialog_get_documenters(getHandle());
    }

    /**
     * Gets the strings which are displayed in the artists tab of the secondary
     * credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getDocumenters() {
        return gtk_about_dialog_get_documenters(getHandle());
    }

    /**
     * Sets the strings which are displayed in the documenters tab of the
     * secondary credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setArtists(String[] val) {
        gtk_about_dialog_set_artists(getHandle(), val);
    }

    /**
     * Gets the strings which are displayed in the documenters tab of the
     * secondary credits dialog.
     * 
     * @deprecated Use {@link #getArtists()} instead.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] setArtists() {
        return gtk_about_dialog_get_artists(getHandle());
    }

    /**
     * Gets the strings which are displayed in the documenters tab of the
     * secondary credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getArtists() {
        return gtk_about_dialog_get_artists(getHandle());
    }

    /**
     * Sets the translator credits string which is displayed in the translators
     * tab of the secondary credits dialog.
     * 
     * The intended use for this string is to display the translator of the
     * language which is currently used in the user interface.
     * 
     * It is a good idea to use the customary msgid "translator-credits" for
     * this purpose, since translators will already know the purpose of that
     * msgid, and since AboutDialog will detect if "translator-credits" is
     * untranslated and hide the tab.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTranslatorCredits(String val) {
        gtk_about_dialog_set_translator_credits(getHandle(), val);
    }

    /**
     * Gets the translator credits string which is displayed in the translators
     * tab of the secondary credits dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getTranslatorCredits() {
        return gtk_about_dialog_get_translator_credits(getHandle());
    }

    /**
     * Sets the pixbuf to be displayed as logo in the about dialog. If it is
     * <tt>null</tt>, the default window icon set with
     * {@link Window#setDefaultIcon} will be used.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLogo(Pixbuf val) {
        Handle pixbufHandle = (val == null ? null : val.getHandle());
        gtk_about_dialog_set_logo(getHandle(), pixbufHandle);
    }

    /**
     * Gets the pixbuf displayed as logo in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getLogo() {
        Handle handle = gtk_about_dialog_get_logo(getHandle());
        Pixbuf pixbuf = (Pixbuf) getGObjectFromHandle(handle);
        if (pixbuf == null)
            pixbuf = new Pixbuf(handle);
        return pixbuf;
    }

    /**
     * Sets the icon name displayed as logo in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLogoIconName(String val) {
        gtk_about_dialog_set_logo_icon_name(getHandle(), val);
    }

    /**
     * Gets the icon name displayed as logo in the about dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getLogoIconName() {
        return gtk_about_dialog_get_logo_icon_name(getHandle());
    }

    /**
     * Set a handler object to be used whenever the user activates a URL link in
     * the authors, documenters or artists sections.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void setURLHook(AboutDialogActivateLinkMethod method) {
        urlMethod = method;
        gtk_about_dialog_set_url_hook(null, "handleURLActivate");
    }

    /**
     * Set a handler object to be used whenever the user activates an Email link
     * in the authors, documenters or artists sections.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void setEmailHook(AboutDialogActivateLinkMethod method) {
        emailMethod = method;
        gtk_about_dialog_set_email_hook(null, "handleEmailActivate");
    }

    protected static void handleURLActivate(Handle about, String link) {
        if (urlMethod != null) {
            urlMethod.linkActivated(AboutDialog.getAboutDialog(about), link);
        }
    }

    protected static void handleEmailActivate(Handle about, String link) {
        if (emailMethod != null) {
            emailMethod.linkActivated(AboutDialog.getAboutDialog(about), link);
        }
    }

    native static final private int gtk_about_dialog_get_type();

    native static final private Handle gtk_about_dialog_new();

    native static final private String gtk_about_dialog_get_name(Handle about);

    native static final private void gtk_about_dialog_set_name(Handle about,
            String name);

    native static final private String gtk_about_dialog_get_version(Handle about);

    native static final private void gtk_about_dialog_set_version(Handle about,
            String version);

    native static final private String gtk_about_dialog_get_copyright(
            Handle about);

    native static final private void gtk_about_dialog_set_copyright(
            Handle about, String copyright);

    native static final private String gtk_about_dialog_get_comments(
            Handle about);

    native static final private void gtk_about_dialog_set_comments(
            Handle about, String comments);

    native static final private String gtk_about_dialog_get_license(Handle about);

    native static final private void gtk_about_dialog_set_license(Handle about,
            String license);

    native static final private boolean gtk_about_dialog_get_wrap_license(
            Handle about);

    native static final private void gtk_about_dialog_set_wrap_license(
            Handle about, boolean wrap);

    native static final private String gtk_about_dialog_get_website(Handle about);

    native static final private void gtk_about_dialog_set_website(Handle about,
            String website);

    native static final private String gtk_about_dialog_get_website_label(
            Handle about);

    native static final private void gtk_about_dialog_set_website_label(
            Handle about, String website_label);

    native static final private String[] gtk_about_dialog_get_authors(
            Handle about);

    native static final private void gtk_about_dialog_set_authors(Handle about,
            String[] authors);

    native static final private String[] gtk_about_dialog_get_documenters(
            Handle about);

    native static final private void gtk_about_dialog_set_documenters(
            Handle about, String[] documentors);

    native static final private String[] gtk_about_dialog_get_artists(
            Handle about);

    native static final private void gtk_about_dialog_set_artists(Handle about,
            String[] artists);

    native static final private String gtk_about_dialog_get_translator_credits(
            Handle about);

    native static final private void gtk_about_dialog_set_translator_credits(
            Handle about, String translator_credits);

    native static final private Handle gtk_about_dialog_get_logo(Handle about);

    native static final private void gtk_about_dialog_set_logo(Handle about,
            Handle logo);

    native static final private String gtk_about_dialog_get_logo_icon_name(
            Handle about);

    native static final private void gtk_about_dialog_set_logo_icon_name(
            Handle about, String icon_name);

    native static final private void gtk_about_dialog_set_email_hook(
            AboutDialog about, String callback);

    native static final private void gtk_about_dialog_set_url_hook(
            AboutDialog about, String callback);

    /*
     * Not implemented. native static final private void
     * gtk_show_about_dialog(Handle parent, String[] properties);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
