/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Color;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellView</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellView extends Widget {

    public CellView() {
        super(gtk_cell_view_new());
    }

    public CellView(String text, boolean hasMarkup) {
        super(init(text, hasMarkup));
    }

    public CellView(Pixbuf pixbuf) {
        super(gtk_cell_view_new_with_pixbuf(pixbuf.getHandle()));
    }

    private static Handle init(String text, boolean hasMarkup) {
        if (hasMarkup)
            return gtk_cell_view_new_with_markup(text);
        else
            return gtk_cell_view_new_with_text(text);
    }

    public void setModel(TreeModel model) {
        gtk_cell_view_set_model(getHandle(), model.getHandle());
    }

    public void setDisplayedRow(TreePath path) {
        gtk_cell_view_set_displayed_row(getHandle(), path.getHandle());
    }

    public TreePath getDisplayedRow() {
        Handle handle = gtk_cell_view_get_displayed_row(getHandle());
        return TreePath.getTreePath(handle);
    }

    public void setBackgroundColor(Color color) {
        gtk_cell_view_set_background_color(getHandle(), color.getHandle());
    }

    public Requisition getSizeOfRow(TreePath path) {
        Handle handle = gtk_cell_view_get_size_of_row(getHandle(), path
                .getHandle());
        return Requisition.getRequisition(handle);
    }

    public CellRenderer[] getCellRenderers() {
        Handle[] hndls = gtk_cell_view_get_cell_renderers(getHandle());
        if (null == hndls)
            return null;
        CellRenderer[] renderers = new CellRenderer[hndls.length];
        for (int i = 0; i < hndls.length; i++)
            renderers[i] = CellRenderer.getCellRenderer(hndls[i]);
        return renderers;
    }

    native static final protected int gtk_cell_view_get_type();

    native static final protected Handle gtk_cell_view_new();

    native static final protected Handle gtk_cell_view_new_with_text(String text);

    native static final protected Handle gtk_cell_view_new_with_markup(
            String markup);

    native static final protected Handle gtk_cell_view_new_with_pixbuf(
            Handle pixbuf);

    native static final protected void gtk_cell_view_set_model(Handle view,
            Handle model);

    native static final protected void gtk_cell_view_set_displayed_row(
            Handle view, Handle path);

    native static final protected Handle gtk_cell_view_get_displayed_row(
            Handle view);

    native static final protected Handle gtk_cell_view_get_size_of_row(
            Handle view, Handle path);

    native static final protected void gtk_cell_view_set_background_color(
            Handle view, Handle color);

    native static final protected Handle[] gtk_cell_view_get_cell_renderers(
            Handle view);

}
