/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.gdk.Rectangle;
import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconInfo</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconInfo extends Boxed {

    private IconInfo() {
    }

    IconInfo(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static IconInfo getIconInfo(Handle handle) {
        if (handle == null)
            return null;

        IconInfo obj = (IconInfo) Boxed.getBoxedFromHandle(handle);
        if (obj == null)
            obj = new IconInfo(handle);

        return obj;
    }

    public int getBaseSize() {
        return gtk_icon_info_get_base_size(getHandle());
    }

    public String getFilename() {
        return gtk_icon_info_get_filename(getHandle());
    }

    public Pixbuf getBuiltinPixbuf() {
        return new Pixbuf(gtk_icon_info_get_builtin_pixbuf(getHandle()));
    }

    public Pixbuf loadIcon() {
        return new Pixbuf(gtk_icon_info_load_icon(getHandle(), new int[] {}));
    }

    public String getDisplayName() {
        return gtk_icon_info_get_display_name(getHandle());
    }

    public boolean getEmbeddedRect(Rectangle rect) {
        return gtk_icon_info_get_embedded_rect(getHandle(), rect.getHandle());
    }

    native static final protected int gtk_icon_info_get_type();

    native static final protected void gtk_icon_info_free(Handle info);

    native static final protected int gtk_icon_info_get_base_size(Handle info);

    native static final protected String gtk_icon_info_get_filename(Handle info);

    native static final protected Handle gtk_icon_info_get_builtin_pixbuf(
            Handle info);

    native static final protected Handle gtk_icon_info_load_icon(Handle info,
            int[] error);

    native static final protected void gtk_icon_info_set_raw_coordinates(
            Handle info, boolean rawCoordinates);

    native static final protected boolean gtk_icon_info_get_embedded_rect(
            Handle info, Handle rect);

    native static final protected boolean gtk_icon_info_get_attach_points(
            Handle info, Handle[] points, int numPoints);

    native static final protected String gtk_icon_info_get_display_name(
            Handle info);

}
