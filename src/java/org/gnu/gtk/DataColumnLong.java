/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;

/**
 * Represents a block in which <code>long</code> data can be stored in a
 * {@link TreeModel}.
 * <p>
 * See parent class {@link DataColumn} description for more information.
 * 
 * @since 2.8.5
 * @author Andrew Cowie;
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.DataColumnLong</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class DataColumnLong extends DataColumn {

    /**
     * Constructs a new datablock for storing <code>long</code> data.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataColumnLong() {
        type = Type.LONG();
    }
}
