/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Direction for text display.
 * 
 * @see TreeView
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextDirection</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextDirection extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gtk.TextDirection NONE = new org.gnu.gtk.TextDirection(
            _NONE);

    static final private int _LTR = 1;

    static final public org.gnu.gtk.TextDirection LTR = new org.gnu.gtk.TextDirection(
            _LTR);

    static final private int _RTL = 2;

    static final public org.gnu.gtk.TextDirection RTL = new org.gnu.gtk.TextDirection(
            _RTL);

    static final private org.gnu.gtk.TextDirection[] theInterned = new org.gnu.gtk.TextDirection[] {
            NONE, LTR, RTL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TextDirection theSacrificialOne = new org.gnu.gtk.TextDirection(
            0);

    static public org.gnu.gtk.TextDirection intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TextDirection already = (org.gnu.gtk.TextDirection) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TextDirection(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TextDirection(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TextDirection or(org.gnu.gtk.TextDirection other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TextDirection and(org.gnu.gtk.TextDirection other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TextDirection xor(org.gnu.gtk.TextDirection other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TextDirection other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
