/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Read only text appearance properties, as returned by methods relating to the
 * text view widgets.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextAppearance</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextAppearance extends MemStruct {

    protected TextAppearance(Handle handle) {
        super(handle);
    }

    protected static TextAppearance getTextAppearance(Handle handle) {
        if (handle == null) {
            return null;
        }
        TextAppearance tAppearance = (TextAppearance) MemStruct
                .getMemStructFromHandle(handle);
        if (tAppearance == null) {
            tAppearance = new TextAppearance(handle);
        }
        return tAppearance;
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Color getBgColor() {
        return new org.gnu.gdk.Color(getBgColor(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Color getFgColor() {
        return new org.gnu.gdk.Color(getFgColor(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Bitmap getBgStipple() {
        return new org.gnu.gdk.Bitmap(getBgStipple(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Bitmap getFgStipple() {
        return new org.gnu.gdk.Bitmap(getFgStipple(getHandle()));
    }

    /**
     * returns super/subscript rise, can be negative
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRise() {
        return getRise(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.pango.Underline getUnderline() {
        return org.gnu.pango.Underline.intern(getUnderline(getHandle()));
    }

    /**
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getStrikethrough() {
        return getStrikethrough(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDrawBg() {
        return getDrawBg(getHandle());
    }

    native static final protected Handle getBgColor(Handle cptr);

    native static final protected Handle getFgColor(Handle cptr);

    native static final protected Handle getBgStipple(Handle cptr);

    native static final protected Handle getFgStipple(Handle cptr);

    native static final protected int getRise(Handle cptr);

    native static final protected int getUnderline(Handle cptr);

    native static final protected boolean getStrikethrough(Handle cptr);

    native static final protected boolean getDrawBg(Handle cptr);

    native static final protected boolean getInsideSelection(Handle cptr);

    native static final protected boolean getIsText(Handle cptr);

}
