/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * SizeGroup provides a mechanism for grouping a number of widgets together so
 * they all request the same amount of space. This is typically useful when you
 * want a column of widgets to have the same size, but you can't use a {@link
 * Table} widget.
 * 
 * <p>
 * In detail, the size requested for each widget in a SizeGroup is the maximum
 * of the sizes that would have been requested for each widget in the size group
 * if they were not in the size group. The mode of the size group (see
 * {@link #setMode(SizeGroupMode)} determines whether this applies to the
 * horizontal size, the vertical size, or both sizes.
 * 
 * <p>
 * Note that size groups only affect the amount of space requested, not the size
 * that the widgets finally receive. If you want the widgets in a SizeGroup to
 * actually be the same size, you need to pack them in such a way that they get
 * the size they request and not more. For example, if you are packing your
 * widgets into a table, you would not include the GTK_FILL flag.
 * 
 * <p>
 * Widgets can be part of multiple size groups; GTK+ will compute the horizontal
 * size of a widget from the horizontal requisition of all widgets that can be
 * reached from the widget by a chain of size groups of type
 * GTK_SIZE_GROUP_HORIZONTAL or GTK_SIZE_GROUP_BOTH, and the vertical size from
 * the vertical requisition of all widgets that can be reached from the widget
 * by a chain of size groups of type GTK_SIZE_GROUP_VERTICAL or
 * GTK_SIZE_GROUP_BOTH.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.SizeGroup</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class SizeGroup extends GObject {

    /**
     * Create a new SizeGroup
     * 
     * @param mode
     *            The mode for the new Size Group
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SizeGroup(SizeGroupMode mode) {
        super(gtk_size_group_new(mode.getValue()));
    }

    /**
     * Sets the {@link SizeGroupMode} of the size group. The mode of the size
     * group determines whether the widgets in the size group should all have
     * the same horizontal requisition (SizeGroupMode.HORIZONTAL) all have the
     * same vertical requisition (SizeGroupMode.VERTICAL), or should all have
     * the same requisition in both directions (SizeGroupMode.BOTH).
     * 
     * @param mode
     *            The mode to set for the SizeGroup
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMode(SizeGroupMode mode) {
        gtk_size_group_set_mode(getHandle(), mode.getValue());
    }

    /**
     * Adds a widget to the SizeGroup. In the future, the requisition of the
     * widget will be determined as the maximum of its requisition and the
     * requisition of the other widgets in the size group. Whether this applies
     * horizontally, vertically, or in both directions depends on the mode of
     * the size group. See {@link #setMode(SizeGroupMode)}.
     * 
     * @param widget
     *            The Widget to add.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addWidget(Widget widget) {
        gtk_size_group_add_widget(getHandle(), widget.getHandle());
    }

    /**
     * Removes a widget from the group
     * 
     * @param widget
     *            The widget to remove
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeWidget(Widget widget) {
        gtk_size_group_remove_widget(getHandle(), widget.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_size_group_get_type());
    }

    native static final protected int gtk_size_group_get_type();

    native static final protected Handle gtk_size_group_new(int mode);

    native static final protected void gtk_size_group_set_mode(
            Handle size_group, int mode);

    native static final protected int gtk_size_group_get_mode(Handle size_group);

    native static final protected void gtk_size_group_add_widget(
            Handle size_group, Handle widget);

    native static final protected void gtk_size_group_remove_widget(
            Handle size_group, Handle widget);
}
