/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Atom;
import org.gnu.gdk.Display;
import org.gnu.gdk.Pixbuf;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * The Clipboard object represents a clipboard of data shared between different
 * processes or between different widgets in the same process.
 * 
 * <p>
 * TODO: This requires gdk.Selections
 * <p>
 * TODO: callback functions
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Clipboard</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Clipboard extends GObject {
    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Clipboard(Handle handle) {
        super(handle);
    }

    protected static Clipboard getClipboard(Handle handle) {
        if (handle == null)
            return null;

        Clipboard clipboard = (Clipboard) getGObjectFromHandle(handle);
        if (clipboard == null)
            clipboard = new Clipboard(handle);

        return clipboard;
    }

    /**
     * Clear the contents of the clipboard.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clear() {
        gtk_clipboard_clear(getHandle());
    }

    /**
     * Returns the clipboard object for the given selection. See
     * {@link Clipboard#getForDisplay(Display, Atom)} for complete details.
     * 
     * @param atom
     *            a {@link Atom} which identifies the clipboard to use
     * @return the appropriate clipboard object. If no clipboard already exists,
     *         a new one will be created. Once a clipboard object has been
     *         created, it is persistent for all time and cannot be freed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public Clipboard get(Atom atom) {
        Handle handle = gtk_clipboard_get(atom.getHandle());
        return Clipboard.getClipboard(handle);
    }

    /**
     * 
     * @param display
     *            the display for which the clipboard is to be retrieved or
     *            created
     * @param atom
     *            a {@link Atom} which identifies the clipboard to use
     * @return he appropriate clipboard object. If no clipboard already exists,
     *         a new one will be created. Once a clipboard object has been
     *         created, it is persistent for all time and cannot be freed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public Clipboard getForDisplay(Display display, Atom atom) {
        return getClipboard(gtk_clipboard_get_for_display(display.getHandle(),
                atom.getHandle()));
    }

    /**
     * Gets the {@link Display} associated with clipboard.
     * 
     * @return the {@link Display} associated with clipboard
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Display getDisplay() {
        return Display
                .getDisplayFromHandle(gtk_clipboard_get_display(getHandle()));
    }

    /**
     * Requests the contents of the clipboard as {@link SelectionData}
     * 
     * @param atom
     *            an {@link Atom} representing the form into which the clipboard
     *            owner should convert the selection
     * @return a newly-allocated {@link SelectionData} object or
     *         <code>NULL</code> if retrieving the given target failed
     * 
     * TODO: write the JNI
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SelectionData getContents(Atom atom) {
        return null;
    }

    public boolean isTextAvailable() {
        return gtk_clipboard_wait_is_text_available(getHandle());
    }

    public boolean isImageAvailable() {
        return gtk_clipboard_wait_is_image_available(getHandle());
    }

    /**
     * Set the contents of the clipboard.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setText(String text) {
        gtk_clipboard_set_text(getHandle(), text, text.length());
    }

    /**
     * Requests the contents of the clipboard as text.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getText() {
        return gtk_clipboard_wait_for_text(getHandle());
    }

    public void setImage(Pixbuf pixbuf) {
        gtk_clipboard_set_image(getHandle(), pixbuf.getHandle());
    }

    public Pixbuf getImage() {
        Handle handle = gtk_clipboard_wait_for_image(getHandle());
        return Pixbuf.getPixbufFromHandle(handle);
    }

    public void setCanStore(TargetEntry[] targets) {
        if (null == targets)
            return;
        Handle[] hndls = new Handle[targets.length];
        for (int i = 0; i < targets.length; i++)
            hndls[i] = targets[i].getHandle();
        gtk_clipboard_set_can_store(getHandle(), hndls);
    }

    public void store() {
        gtk_clipboard_store(getHandle());
    }

    // The underlying c object should not be freed, therefore we override
    // the finalize method to do nothing
    protected void finalize() throws Throwable {
    }

    native static final protected Handle gtk_clipboard_get(Handle selection);

    native static final protected Handle gtk_clipboard_get_display(
            Handle clipboard);

    native static final protected Handle gtk_clipboard_get_owner(
            Handle clipboard);

    native static final protected void gtk_clipboard_clear(Handle clipboard);

    native static final protected void gtk_clipboard_set_text(Handle clipboard,
            String text, int len);

    native static final protected void gtk_clipboard_set_image(
            Handle clipboard, Handle pixbuf);

    native static final protected String gtk_clipboard_wait_for_text(
            Handle clipboard);

    native static final protected Handle gtk_clipboard_wait_for_image(
            Handle clipboard);

    native static final protected boolean gtk_clipboard_wait_is_text_available(
            Handle clipboard);

    native static final protected boolean gtk_clipboard_wait_is_image_available(
            Handle clipboard);

    native static final protected boolean gtk_clipboard_wait_is_target_available(
            Handle clipboard, Handle target);

    native static final protected void gtk_clipboard_set_can_store(
            Handle clipboard, Handle[] targets);

    native static final protected void gtk_clipboard_store(Handle clipboard);

    native static final private Handle gtk_clipboard_get_for_display(
            Handle display, Handle selection);
}
