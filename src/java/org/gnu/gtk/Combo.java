/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The Combo box displays a single item and provides a pull-down list of items
 * that can be selected. The drop-down list is displayed when the user clicks on
 * a small arrow button to the right of the entry field.
 * <p>
 * By default, the user can step through the items in the list by using the
 * arrow keys, though this behavior can be turned off with the setUseArrows()
 * method.
 * <p>
 * Normally the arrow keys are only active when the contents of the text entry
 * field matches on of the items in the list. If the contents of the entry field
 * do not match any of the items in the list items, then pressing the arrow keys
 * does nothing. However, by calling the setUseArrowsAlways() method you can
 * specify that the arrow keys be active always.
 * 
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Combo</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Combo extends HBox {

    /**
     * Construct a new Combo widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Combo() {
        super(gtk_combo_new());
    }

    /**
     * Construct a new Combo from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Combo(Handle handle) {
        super(handle);
    }

    /**
     * Convenience method to set all of the items in the popupdown list.
     * 
     * @param values
     *            The array of values to put into the popupdown list.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPopupdownStrings(String[] values) {
        Combo.gtk_combo_set_popdown_strings(getHandle(), values);
    }

    /**
     * Specifies whether the value entered in the text entry field must match
     * one of the values in the list. If this is set then the user will not be
     * able to perform any other action until a valid value has been entered.
     * 
     * @param val
     *            true if the value entered must match one of the values in the
     *            list.
     * @param okifEmpty
     *            true if an empty value is considered valid.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValueInList(boolean val, boolean okifEmpty) {
        Combo.gtk_combo_set_value_in_list(getHandle(), val, okifEmpty);
    }

    /**
     * Specifies if the arrow (cursor) keys can be used to step through the
     * items in the list. This is on by default.
     * 
     * @param val
     *            true if the arrow keys can be used to step through the items
     *            in the list.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseArrows(boolean val) {
        Combo.gtk_combo_set_use_arrows(getHandle(), val);
    }

    /**
     * Specifies if the arrow keys will still work even if the current contents
     * of the Entry field do not match any of the items in the list.
     * 
     * @param val
     *            true if the arrows should still work.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseArrorwsAlways(boolean val) {
        Combo.gtk_combo_set_use_arrows_always(getHandle(), val);
    }

    /**
     * Specifies whether the text entered into the Entry field and the text in
     * the line items are case sensitive.
     * <p>
     * This may be useful when you have called setValueInList() to limit the
     * values entered but are not worried about case differences.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCaseSensitive(boolean val) {
        Combo.gtk_combo_set_case_sensitive(getHandle(), val);
    }

    /**
     * Sets the string to place in the Entry field when a particular item is
     * selected. This is needed if the list item is not a simple label.
     * 
     * @param item
     *            The item to add to the list.
     * @param itemValue
     *            The string value to display in the Entry if item is selected
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setItemString(Item item, String itemValue) {
        Combo.gtk_combo_set_item_string(getHandle(), item.getHandle(),
                itemValue);
    }

    /**
     * Stops the Combo widget from showing the popup list when the Entry emits
     * the "activate" signal, i.e., when the return key is pressed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void disableActivate() {
        Combo.gtk_combo_disable_activate(getHandle());
    }

    /**
     * Get the Entry field that is a part of this combo.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Entry getEntry() {
        Handle hndl = getEntry(getHandle());
        return Entry.getEntry(hndl);
    }

    /**
     * Retrieve the text from the combo.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getText() {
        return getEntry().getText();
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_combo_get_type());
    }

    native static final protected Handle getEntry(Handle cptr);

    native static final protected int gtk_combo_get_type();

    native static final protected Handle gtk_combo_new();

    native static final protected void gtk_combo_set_value_in_list(
            Handle combo, boolean val, boolean okIfEmpty);

    native static final protected void gtk_combo_set_use_arrows(Handle combo,
            boolean val);

    native static final protected void gtk_combo_set_use_arrows_always(
            Handle combo, boolean val);

    native static final protected void gtk_combo_set_case_sensitive(
            Handle combo, boolean val);

    native static final protected void gtk_combo_set_item_string(Handle combo,
            Handle item, String itemValue);

    native static final protected void gtk_combo_set_popdown_strings(
            Handle combo, String[] strings);

    native static final protected void gtk_combo_disable_activate(Handle combo);

}
