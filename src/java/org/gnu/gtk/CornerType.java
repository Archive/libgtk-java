/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Specifies which corner a child widget should be placed in when packed into a
 * ScrolledWindow. This is effectively the opposite of where the scroll bars are
 * placed.
 * 
 * <pre>
 *  TOP_LEFT
 *  Places the scroll bars on the right and bottom of the widget.
 *  BOTTOM_LEFT
 *  Places the scroll bars on the top and right of the widget.
 *  TOP_RIGHT
 *  Places the scroll bars on the left and bottom of the widget.
 *  BOTTOM_RIGHT
 *  Places the scroll bars on the top and left of the widget.
 * </pre>
 * 
 * @see ScrolledWindow
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CornerType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CornerType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TOP_LEFT = 0;

    static final public org.gnu.gtk.CornerType TOP_LEFT = new org.gnu.gtk.CornerType(
            _TOP_LEFT);

    static final private int _BOTTOM_LEFT = 1;

    static final public org.gnu.gtk.CornerType BOTTOM_LEFT = new org.gnu.gtk.CornerType(
            _BOTTOM_LEFT);

    static final private int _TOP_RIGHT = 2;

    static final public org.gnu.gtk.CornerType TOP_RIGHT = new org.gnu.gtk.CornerType(
            _TOP_RIGHT);

    static final private int _BOTTOM_RIGHT = 3;

    static final public org.gnu.gtk.CornerType BOTTOM_RIGHT = new org.gnu.gtk.CornerType(
            _BOTTOM_RIGHT);

    static final private org.gnu.gtk.CornerType[] theInterned = new org.gnu.gtk.CornerType[] {
            TOP_LEFT, BOTTOM_LEFT, TOP_RIGHT, BOTTOM_RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.CornerType theSacrificialOne = new org.gnu.gtk.CornerType(
            0);

    static public org.gnu.gtk.CornerType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.CornerType already = (org.gnu.gtk.CornerType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.CornerType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CornerType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.CornerType or(org.gnu.gtk.CornerType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.CornerType and(org.gnu.gtk.CornerType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.CornerType xor(org.gnu.gtk.CornerType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.CornerType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
