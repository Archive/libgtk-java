/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Bitmap;
import org.gnu.gdk.Pixbuf;
import org.gnu.gdk.PixbufAnimation;
import org.gnu.gdk.Pixmap;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The Image widget displays an image. Various kinds of objects can be displayed
 * as an image; most typically you would create a {@link org.gnu.gdk.Pixbuf}
 * from a file and then display that.
 * <p>
 * If the file isn't loaded successfully, the image will contain a "broken
 * image" icon similar to that used in many web browsers.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Image</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Image extends Misc {

    /**
     * Construct a new image passing the filename of an image.
     * 
     * @param filename
     *            The filename for the image to load.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(String filename) {
        super(gtk_image_new_from_file(filename));
    }

    /**
     * Create an Image widget displaying a <i>image</i> and a <i>mask</i>. A
     * org.gnu.gdk.Image is a client-side image buffer in the pixel format of
     * the current display.
     * 
     * @param image
     *            The org.gnu.gdk.Image to use for this object creation.
     * @param mask
     *            The org.gnu.gdk.Mask to apply to the image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(org.gnu.gdk.Image image, Bitmap mask) {
        super(gtk_image_new_from_image(image.getHandle(), mask.getHandle()));
    }

    /**
     * Creates an Image from a {@link org.gnu.gdk.Pixbuf}.
     * 
     * @param pixbuf
     *            The Pixbuf to use for the Image creation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(Pixbuf pixbuf) {
        super(gtk_image_new_from_pixbuf(pixbuf.getHandle()));
    }

    /**
     * Create an Image from a {@link org.gnu.gdk.Pixmap}.
     * 
     * @param pixmap
     *            The Pixmap to use for the Image creation.
     * @param mask
     *            The mask to apply to the created Image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(Pixmap pixmap, Bitmap mask) {
        super(gtk_image_new_from_pixmap(pixmap.getHandle(), mask.getHandle()));
    }

    /**
     * Creates an Image displaying the given PixbufAnimation.
     * 
     * @param animation
     *            The PixbufAnimation to use for this object creation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(PixbufAnimation animation) {
        super(gtk_image_new_from_animation(animation.getHandle()));
    }

    /**
     * Create an Image
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(String iconName, IconSize size) {
        super(gtk_image_new_from_icon_name(iconName, size.getValue()));
    }

    /**
     * Construct an Image from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(Handle handle) {
        super(handle);
    }

    /**
     * Construct an Image from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Image getImage(Handle handle) {
        if (handle == null)
            return null;

        Image obj = (Image) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new Image(handle);

        return obj;
    }

    /**
     * Creates an Image displaying an IconSet.
     * 
     * @param iconSet
     *            The IconSet to use for the Image.
     * @param size
     *            The IconSize to use for the Image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(IconSet iconSet, IconSize size) {
        super(gtk_image_new_from_icon_set(iconSet.getHandle(), size.getValue()));
    }

    /**
     * Create an Image displaying a Stock icon.
     * 
     * @param stockId
     *            The id of the Stock icon to display.
     * @param size
     *            The size of the icon.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(GtkStockItem stockId, IconSize size) {
        super(gtk_image_new_from_stock(stockId.getString(), size.getValue()));
    }

    /**
     * Constructs a new Image. This will be displayed as a "no image" icon until
     * an image is set using one of the set* methods.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image() {
        super(gtk_image_new());
    }

    /**
     * Gets the type of representation being used by the Image to store image
     * data.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ImageType getStorageType() {
        return ImageType.intern(gtk_image_get_storage_type(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_image_get_type());
    }

    /**
     * Sets the image to the given {@link org.gnu.gdk.Pixmap}.
     * 
     * @param pixmap
     *            The Pixmap to use for the Image.
     * @param mask
     *            The mask to apply to the Image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(Pixmap pixmap, Bitmap mask) {
        gtk_image_set_from_pixmap(getHandle(), pixmap.getHandle(), mask
                .getHandle());
    }

    /**
     * Sets the image widget to displaying a <i>image</i> and a <i>mask</i>. A
     * org.gnu.gdk.Image is a client-side image buffer in the pixel format of
     * the current display.
     * 
     * @param image
     *            The org.gnu.gdk.Image to use for this object.
     * @param mask
     *            The org.gnu.gdk.Mask to apply to the image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(org.gnu.gdk.Image image, Bitmap mask) {
        Image.gtk_image_set_from_image(getHandle(), image.getHandle(), mask
                .getHandle());
    }

    /**
     * Sets the Image from a {@link org.gnu.gdk.Pixbuf}.
     * 
     * @param pixbuf
     *            The Pixbuf to use for the Image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(Pixbuf pixbuf) {
        Image.gtk_image_set_from_pixbuf(getHandle(), pixbuf.getHandle());
    }

    /**
     * Sets to image to the given PixbufAnimation.
     * 
     * @param animation
     *            The PixbufAnimation to use for this object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(PixbufAnimation animation) {
        Image.gtk_image_set_from_animation(getHandle(), animation.getHandle());
    }

    public void set(String iconName, IconSize size) {
        gtk_image_set_from_icon_name(getHandle(), iconName, size.getValue());
    }

    /**
     * Sets the image to displayian IconSet.
     * 
     * @param iconSet
     *            The IconSet to use for the Image.
     * @param size
     *            The IconSize to use for the Image.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(IconSet iconSet, IconSize size) {
        Image.gtk_image_set_from_icon_set(getHandle(), iconSet.getHandle(),
                size.getValue());
    }

    /**
     * Set the image to a Stock icon.
     * 
     * @param stockId
     *            The id of the Stock icon to display.
     * @param size
     *            The size of the icon.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(GtkStockItem stockId, IconSize size) {
        Image.gtk_image_set_from_stock(getHandle(), stockId.getString(), size
                .getValue());
    }

    /**
     * Sets the image from a file
     * 
     * @param filename
     *            The filename for the image to load.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(String filename) {
        Image.gtk_image_set_from_file(getHandle(), filename);
    }

    public void setPixelSize(int pixelSize) {
        gtk_image_set_pixel_size(getHandle(), pixelSize);
    }

    public int getPixelSize() {
        return gtk_image_get_pixel_size(getHandle());
    }

    /**
     * Gets the {@link Pixbuf} being displayed by the Image. The storage type of
     * the image must be GTK_IMAGE_EMPTY or GTK_IMAGE_PIXBUF.
     * 
     * @return The pixbuf being used by the image.
     * @see #getStorageType()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getPixbuf() {
        Handle hndl = gtk_image_get_pixbuf(getHandle());

        if (hndl == null)
            return null;
        return Pixbuf.getPixbufFromHandle(hndl);
    }

    /**
     * Gets the {@link org.gnu.gdk.Image} being displayed by this. The storage
     * type of the image must be GTK_IMAGE_EMPTY or GTK_IMAGE_IMAGE.
     * 
     * @return The image being displayed by the image
     * @see #getStorageType()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.gdk.Image getImage() {
        Handle hndl = gtk_image_get_image(this.getHandle());

        if (hndl == null)
            return null;

        org.gnu.gdk.Image im = (org.gnu.gdk.Image) GObject
                .getGObjectFromHandle(hndl);

        if (im == null)
            im = new org.gnu.gdk.Image(hndl);

        return im;
    }

    /**
     * Gets the {@link Bitmap} being used as a map by the Image The storage type
     * of the image must be GTK_IMAGE_EMPTY or GTK_IMAGE_IMAGE.
     * 
     * @return The map being used by the image
     * @see #getStorageType()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Bitmap getImageMask() {
        Handle hndl = gtk_image_get_image_mask(this.getHandle());

        if (hndl == null)
            return null;

        Bitmap bm = (Bitmap) GObject.getGObjectFromHandle(hndl);

        if (bm == null)
            bm = new Bitmap(hndl);

        return bm;
    }

    public String getIconName() {
        String name = new String();
        int[] size = new int[1];
        gtk_image_get_icon_name(getHandle(), name, size);
        return name;
    }

    native static final protected int gtk_image_get_type();

    native static final protected Handle gtk_image_new();

    native static final protected Handle gtk_image_new_from_pixmap(
            Handle pixmap, Handle mask);

    native static final protected Handle gtk_image_new_from_image(Handle image,
            Handle mask);

    native static final protected Handle gtk_image_new_from_file(String filename);

    native static final protected Handle gtk_image_new_from_pixbuf(Handle pixbuf);

    native static final protected Handle gtk_image_new_from_stock(
            String stockId, int size);

    native static final protected Handle gtk_image_new_from_icon_set(
            Handle iconSet, int size);

    native static final protected Handle gtk_image_new_from_animation(
            Handle animation);

    native static final protected Handle gtk_image_new_from_icon_name(
            String iconName, int size);

    native static final protected void gtk_image_set_from_pixmap(Handle image,
            Handle pixmap, Handle mask);

    native static final protected void gtk_image_set_from_image(Handle image,
            Handle gdkImage, Handle mask);

    native static final protected void gtk_image_set_from_file(Handle image,
            String filename);

    native static final protected void gtk_image_set_from_pixbuf(Handle image,
            Handle pixbuf);

    native static final protected void gtk_image_set_from_stock(Handle image,
            String stockId, int size);

    native static final protected void gtk_image_set_from_icon_set(
            Handle image, Handle iconSet, int size);

    native static final protected void gtk_image_set_from_animation(
            Handle image, Handle animation);

    native static final protected void gtk_image_set_from_icon_name(
            Handle image, String iconName, int size);

    native static final protected void gtk_image_set_pixel_size(Handle image,
            int pixelSize);

    native static final protected int gtk_image_get_storage_type(Handle image);

    native static final protected void gtk_image_get_pixmap(Handle image,
            Handle pixmap, Handle mask);

    native static final protected Handle gtk_image_get_image(Handle image);

    native static final protected Handle gtk_image_get_image_mask(Handle image);

    native static final protected Handle gtk_image_get_pixbuf(Handle image);

    native static final protected void gtk_image_get_stock(Handle image,
            String[] stockId, int[] size);

    native static final protected void gtk_image_get_icon_set(Handle image,
            Handle iconSet, int[] size);

    native static final protected Handle gtk_image_get_animation(Handle image);

    native static final protected void gtk_image_get_icon_name(Handle image,
            String iconName, int[] size);

    native static final protected int gtk_image_get_pixel_size(Handle image);

    /*
     * Deprecated functions. native static final private void
     * gtk_image_set(Handle image, Handle val, Handle mask); native static final
     * private void gtk_image_get(Handle image, Handle val, Handle mask);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
