/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToggleAction</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToggleAction extends Action {

    public ToggleAction(String name, String label, String tooltip,
            String stockId) {
        super(gtk_toggle_action_new(name, label, tooltip, stockId));
    }

    public ToggleAction(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToggleAction getToggleAction(Handle handle) {
        if (handle == null)
            return null;

        ToggleAction obj = (ToggleAction) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ToggleAction(handle);

        return obj;
    }

    public void setActive(boolean active) {
        gtk_toggle_action_set_active(getHandle(), active);
    }

    public boolean getActive() {
        return gtk_toggle_action_get_active(getHandle());
    }

    public void setDrawAsRadio(boolean drawAsRadio) {
        gtk_toggle_action_set_draw_as_radio(getHandle(), drawAsRadio);
    }

    public boolean getDrawAsRadio() {
        return gtk_toggle_action_get_draw_as_radio(getHandle());
    }

    native static final protected int gtk_toggle_action_get_type();

    native static final protected Handle gtk_toggle_action_new(String name,
            String label, String tooltip, String stockId);

    native static final protected void gtk_toggle_action_toggled(Handle action);

    native static final protected void gtk_toggle_action_set_active(
            Handle action, boolean isActive);

    native static final protected boolean gtk_toggle_action_get_active(
            Handle action);

    native static final protected void gtk_toggle_action_set_draw_as_radio(
            Handle action, boolean radio);

    native static final protected boolean gtk_toggle_action_get_draw_as_radio(
            Handle action);
}
