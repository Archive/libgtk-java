/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * This is the listener interface for receiving keyboard events on a Widget.
 * Objects that are interested in keyboard events should implement this
 * Interface and then register with the <code>Widget</code> using the
 * <code>{@link org.gnu.gtk.Widget#addListener(KeyListener)}</code> method.
 * <p>
 * An example of using this to trap the Escape key:
 * 
 * <pre>
 *  Window window;
 *  ...
 *  window.addListener(new KeyListener() {
 *       public boolean keyEvent(KeyEvent event) {
 *      		int key = event.getKeyval();
 *      		if (key == KeyValue.Escape) {
 *      			window.hide();
 *      			return true;
 *      		} else {
 *      			return false;
 *      		}
 *      	}
 *  });
 * </pre>
 * 
 * @author Jeffrey S. Morgan
 * @author Andrew Cowie
 * @see KeyEvent
 * @see KeyEvent.Type The list of KeyEvent types
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface KeyListener {
    /**
     * Process a keystroke.
     * 
     * @param event
     *            gives you access to the various methods which help you
     *            identify the keystroke. See {@link KeyEvent#getKeyval()}
     * @return Return true if you have handled the keystroke and <B>do not</B>
     *         want it to propagate to the default GTK handlers. Returning false
     *         will allow the keystroke to go upstream after you're done doing
     *         whatever you're doing.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean keyEvent(KeyEvent event);

}
