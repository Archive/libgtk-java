/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * This event is used to identify life cycle events that effect a widget.
 * 
 * @see LifeCycleListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class LifeCycleEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * This event indicates that a widget has been realized.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type REALIZE = new Type(1, "REALIZE");

        /**
         * This event indicates that a widget has been unrealized
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type UNREALIZE = new Type(2, "UNREALIZE");

        /**
         * This event indicates that a widget has been deleted
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DELETE = new Type(3, "DELETE");

        /**
         * This event indicates that a widget has been destroyed
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DESTROY = new Type(4, "DESTROY");

        /**
         * This event indicates that a widget has become visible.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type SHOW = new Type(5, "SHOW");

        /**
         * This event indicates that a widget has become invisible.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type HIDE = new Type(6, "HIDE");

        /**
         * This event indicates that a widget has been mapped.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MAP = new Type(7, "MAP");

        /**
         * This event indicates that a widget has been unmapped.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type UNMAP = new Type(8, "UNMAP");
    }

    /**
     * Construct a LifeCycleEvent object.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public LifeCycleEvent(Object source, LifeCycleEvent.Type type) {
        super(source, type);
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(LifeCycleEvent.Type test) {
        return type.getID() == test.getID();
    }
}
