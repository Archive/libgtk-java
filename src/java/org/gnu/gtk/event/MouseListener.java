/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * @author Jeffrey S. Morgan
 * 
 * This is the listener interface for receiving mouse button events and mouse
 * enter and leave events.
 * 
 * @see MouseEvent
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface MouseListener {

    /**
     * A mouse event has been called. Return <code>true</code> if you have
     * handled the event and so don't want it passed on to other listeners.
     * Since this is usually the first callback to be received, returning
     * <code>true</code> will stop common actions, such as a toggle button
     * being toggled or an item being selected in a tree.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean mouseEvent(MouseEvent event);

}
