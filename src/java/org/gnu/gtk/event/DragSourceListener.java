/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.DragContext;
import org.gnu.gtk.SelectionData;
import org.gnu.gtk.Widget;

/**
 * @deprecated Use {@link DragOriginListener} instead.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface DragSourceListener {

    public void dragBegin(Widget widget, DragContext dc);

    public void dragMotion(Widget widget, DragContext dc, int x, int y);

    public void getDragData(Widget widget, DragContext dc, SelectionData sd,
            int info);

    public void deleteDragData(Widget widget, DragContext dc);

    public void dragDrop(Widget widget, DragContext dc, int x, int y);

    public void dragEnd(Widget widget, DragContext dc);

}
