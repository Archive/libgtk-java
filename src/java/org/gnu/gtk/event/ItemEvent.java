/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * An event represeting action by a {@link org.gnu.gtk.Item} widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ItemEvent extends GtkEvent {

    /**
     * Type of a ItemEvent
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type SELECT = new Type(1, "SELECT");

        public static final Type DESELECT = new Type(2, "DESELECT");

        public static final Type TOGGLE = new Type(3, "TOGGLE");

    }

    /**
     * Creates a new Button Event. This is used internally by java-gnome. Users
     * only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ItemEvent(Object source, ItemEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ItemEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
