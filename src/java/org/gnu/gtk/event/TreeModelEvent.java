/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreePath;

/**
 * An event represeting action by a {@link org.gnu.gtk.TreeModel} class.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class TreeModelEvent extends GtkEvent {

    private TreePath treePath;

    private TreeIter treeIter;

    private int[] newOrder;

    /**
     * Type of a TreeModelEvent
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type ROW_CHANGED = new Type(1, "ROW_CHANGED");

        public static final Type ROW_INSERTED = new Type(2, "ROW_INSERTED");

        public static final Type ROW_HAS_CHILD_TOGGLED = new Type(3,
                "ROW_HAS_CHILD_TOGGLED");

        public static final Type ROW_DELETED = new Type(4, "ROW_DELETED");

        public static final Type ROWS_REORDERED = new Type(5, "ROWS_REORDERED");

    }

    /**
     * Creates a new TreeModelEvent. This is used internally by java-gnome.
     * Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreeModelEvent(Object source, TreeModelEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(TreeModelEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    /**
     * @return Returns the treeIter.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreeIter getTreeIter() {
        return treeIter;
    }

    /**
     * @param treeIter
     *            The treeIter to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setTreeIter(TreeIter treeIter) {
        this.treeIter = treeIter;
    }

    /**
     * @return Returns the treePath.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreePath getTreePath() {
        return treePath;
    }

    /**
     * @param treePath
     *            The treePath to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setTreePath(TreePath treePath) {
        this.treePath = treePath;
    }

    /**
     * @return Returns the newOrder.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int[] getNewOrder() {
        return newOrder;
    }

    /**
     * @param newOrder
     *            The newOrder to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setNewOrder(int[] newOrder) {
        this.newOrder = newOrder;
    }
}
