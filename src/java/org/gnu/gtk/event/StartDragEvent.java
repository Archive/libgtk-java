/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * @author Ismael Juma <ismael@juma.me.uk>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
import org.gnu.gdk.DragContext;
import org.gnu.glib.EventType;
import org.gnu.gtk.Widget;

public class StartDragEvent extends DragEvent {
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static Type START = new Type(0, "START");
    }

    public StartDragEvent(Widget source, EventType type, DragContext context) {
        super(source, type, context);
    }
}
