/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * This event represents TextBuffer events
 * 
 * @see TextBufferListener
 * @see org.gnu.gtk.TextBuffer
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class TextBufferEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type APPLY_TAG = new Type(1, "APPLY_TAG");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CHANGED = new Type(2, "CHANGED");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DELETE_RANGE = new Type(3, "DELETE_RANGE");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type INSERT_CHILD_ANCHOR = new Type(4,
                "INSERT_CHILD_ANCHOR");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type INSERT_PIXBUF = new Type(5, "INSERT_PIXBUF");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type INSERT_TEXT = new Type(6, "INSERT_TEXT");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MARK_DELETED = new Type(7, "MARK_DELETED");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MARK_SET = new Type(8, "MARK_SET");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MODIFIED_CHANGED = new Type(9,
                "MODIFIED_CHANGED");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type REMOVE_TAG = new Type(10, "REMOVE_TAG");
    }

    public TextBufferEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * Compares to event types for equality
     * 
     * @return true if the Types are the same
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(TextBufferEvent.Type test) {
        return type.getID() == test.getID();
    }
}
