/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.Adjustment;
import org.gnu.gtk.TreePath;

/**
 * This event is used to identify when a IconView receives an event
 * 
 * @see IconViewListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class IconViewEvent extends GtkEvent {

    private Adjustment horizontalAdjustment;

    private Adjustment verticalAdjustment;

    private TreePath path;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type SET_SCROLL_ADJUSTMENTS = new Type(1,
                "SET_SCROLL_ADJUSTMENTS");

        public static final Type ITEM_ACTIVATED = new Type(2, "ITEM_ACTIVATED");

        public static final Type SELECTION_CHANGED = new Type(3,
                "SELECTION_CHANGED");

    }

    /**
     * Constructor for IconViewEvent.
     * 
     * @param source
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public IconViewEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(IconViewEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    public Adjustment getHorizontalAdjustment() {
        return horizontalAdjustment;
    }

    public void setHorizontalAdjustment(Adjustment horizontalAdjustment) {
        this.horizontalAdjustment = horizontalAdjustment;
    }

    public TreePath getPath() {
        return path;
    }

    public void setPath(TreePath path) {
        this.path = path;
    }

    public Adjustment getVerticalAdjustment() {
        return verticalAdjustment;
    }

    public void setVerticalAdjustment(Adjustment verticalAdjustment) {
        this.verticalAdjustment = verticalAdjustment;
    }
}
