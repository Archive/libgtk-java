/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * This is the listener interface for receiving life cycle realted events for a
 * Widget.
 * <p>
 * An example of using this class to manage the life cycle events of an
 * application would be as follows:
 * 
 * <pre>
 * public class MyExample {
 * 
 * public MyExample() {
 *     Window window = new Window();
 *     window.setTitle(&quot;AboutDialogExample&quot;);
 *     window.addListener(new LifeCycleListener() {
 *         public void lifeCycleEvent(LifeCycleEvent event) {}
 *         
 *         public boolean lifeCycleQuery(LifeCycleEvent event) {
 *      if (event.isOfType(LifeCycleEvent.Type.DELETE) {
 *          Gtk.mainQuit();
 *      }
 *      return false;
 *         }
 *     }
 *        
 *     window.showAll();
 * }
 *
 * public static void main(String[] args) {
 *         Gtk.init(args);
 *         new MyExample();
 *         Gtk.main();
 *     }
 * }
 * </pre>
 * 
 * If, on the other hand, you are trying to intercept window closure, you listen
 * for the DELETE signal, do your alternate logic, and then <b>return true</b>.
 * This tells GTK that you have handled the event-signal and that it is not to
 * further propagate the signal which means that the default handler (which in
 * turn emits a DESTROY signal and releases the resources) will not be called.
 * 
 * @see LifeCycleEvent
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface LifeCycleListener {

    /**
     * This method is for all void-returning life cycle related event signals.
     * This covers most of the signals in {@link LifeCycleEvent}, with the
     * exception of "delete" and "destroy"; see
     * {@link #lifeCycleQuery(LifeCycleEvent) below}.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    void lifeCycleEvent(LifeCycleEvent event);

    /**
     * This method is for the "delete" (represented by
     * {@link LifeCycleEvent.Type#DELETE}) and "destroy" (represented by
     * {@link LifeCycleEvent.Type#DESTROY}) event signals.
     * 
     * @return true if you want to tell GTK you have handled the signal and that
     *         it should stop propagating it. For example, if you want to stop
     *         the Window from closing, perhaps in response to running a dialog
     *         saying "do you wish to quit", return true if they do not. In more
     *         normal circumstances you wish to do some cleanup before closing,
     *         then do it here, then return false to let GTK destroy the Window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    boolean lifeCycleQuery(LifeCycleEvent event);
}
