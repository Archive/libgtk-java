/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * An event representing action by a {@link org.gnu.gtk.CellRendererText} widget.
 * 
 * @see CellRendererTextListener
 * @see org.gnu.gtk.CellRendererText
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class CellRendererTextEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * This event indicates that the menu item has been activated.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type EDITED = new Type(1, "EDITED");
    }

    protected String index, text;

    /**
     * Creates a new event. This is used internally by java-gnome. Users only
     * have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public CellRendererTextEvent(Object source, String index, String text) {
        super(source, Type.EDITED);
        this.index = index;
        this.text = text;
    }

    /**
     * Returns the TreeIter index of which the text has been changed.
     * 
     * @return the TreeIter index
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public String getIndex() {
        return this.index;
    }

    /**
     * Returns the new user entered text.
     * 
     * @return the user entered text
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public String getText() {
        return this.text;
    }


    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(CellRendererTextEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
