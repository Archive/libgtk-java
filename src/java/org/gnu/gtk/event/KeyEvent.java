/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.EventKey;
import org.gnu.gdk.ModifierType;
import org.gnu.glib.EventType;

/**
 * 
 * This event object is used to identify when a key has been pressed and
 * released. In particular, it allows you to get at <i>which</i> key was
 * pressed - see {@link #getKeyval()}.
 * 
 * @author Jeffrey S. Morgan
 * @author Andrew Cowie
 * 
 * @see KeyListener An example of how to listen for KeyEvents
 * @see org.gnu.gdk.KeyValue
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class KeyEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * This event indicates that a key has been pressed.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type KEY_PRESSED = new Type(1, "KEY_PRESSED");

        /**
         * This event indicates that a key has been released.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type KEY_RELEASED = new Type(2, "KEY_RELEASED");
    }

    /**
     * Represents the state of the modifier keys (e.g. Control, Shift, and Alt)
     * and the pointer buttons.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private ModifierType modifierKey;

    /**
     * The key that was pressed or released. This value can be compared against
     * the values contained in org.gnu.gdk.KeySymbols.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int keyval;

    /**
     * The length of the string
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int length;

    /**
     * A null-terminated multi-byte string containing the composed characters
     * resulting from the key press.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private String string;

    /**
     * Construct a KeyEvent object.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public KeyEvent(Object source, KeyEvent.Type type, EventKey gdkEvent) {
        super(source, type);
        this.modifierKey = gdkEvent.getModifierKey();
        this.keyval = gdkEvent.getKeyVal();
        this.length = gdkEvent.getLength();
        this.string = gdkEvent.getString();
    }

    /**
     * Test to compare events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(KeyEvent.Type test) {
        return type.getID() == test.getID();
    }

    /**
     * Returns the key value. Example:
     * 
     * <pre>
     *       if (key == KeyValue.Return) {
     *           ...
     *       }
     * </pre>
     * 
     * @return an int, the key value. See {@link org.gnu.gdk.KeyValue} for the
     *         comprehensive list of values.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getKeyval() {
        return keyval;
    }

    /**
     * Returns the length of the String that resulted from this keypress (see
     * {@link #getString()}).
     * 
     * @return int
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getLength() {
        return length;
    }

    /**
     * Returns the modifier key used.
     * 
     * @return (see {@link org.gnu.gdk.ModifierType})
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ModifierType getModifierKey() {
        return modifierKey;
    }

    /**
     * Get the "null-terminated multi-byte string containing the composed
     * characters resulting from the key press".
     * <p>
     * <b>FIXME</b>: <i>Isn't null termination hidden in Java? And, in any
     * case, characters in Strings in Java <i>are</i> multi-byte. So does this
     * work right nor not?</i>
     * 
     * @return String the character(s) resulting from the key press.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public String getString() {
        return string;
    }

}
