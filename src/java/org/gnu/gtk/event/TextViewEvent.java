/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.Menu;
import org.gnu.gtk.MovementStep;

/**
 * This event is used to identify events of the TextView widget.
 * 
 * @see org.gnu.gtk.TextView
 * @see TextViewListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class TextViewEvent extends GtkEvent {

    /**
     * This menu item is set when the POPULATE_POPUP event type is triggered.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private Menu menu;

    /**
     * Initialized when the MOVE_CURSOR event type is triggered
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private MovementStep movementStep;

    /**
     * Initialized when the MOVE_CURSOR and DELETE_FROM_CURSOR event types are
     * triggered
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int count;

    /**
     * Used to describe the type of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * Data has been copied from the widget to the clipboard.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type COPY_CLIPBOARD = new Type(1, "COPY_CLIPBOARD");

        /**
         * Data has been cut from the widget to the clipboard.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CUT_CLIPBOARD = new Type(2, "CUT_CLIPBOARD");

        /**
         * Data has been pasted from the clipboard to the widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type PASTE_CLIPBOARD = new Type(3,
                "PASTE_CLIPBOARD");

        public static final Type DELETE_FROM_CURSOR = new Type(4,
                "DELETE_FROM_CURSOR");

        public static final Type INSERT_AT_CURSOR = new Type(5,
                "INSERT_AT_CURSOR");

        /**
         * The insertion point has been moved.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MOVE_CURSOR = new Type(6, "MOVE_CURSOR");

        public static final Type MOVE_FOCUS = new Type(7, "MOVE_FOCUS");

        /**
         * This event type will probably be deprecated in GTK release 2.2
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type PAGE_HORIZONTALLY = new Type(8,
                "PAGE_HORIZONTALLY");

        /**
         * Gives the developer the oportunity to populate a context sensitive
         * popup menu.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type POPULATE_POPUP = new Type(9, "POPULATE_POPUP");

        /**
         * The anchor has been moved to the cursor position.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type SET_ANCHOR = new Type(10, "SET_ANCHOR");

        public static final Type SET_SCROLL_ADJUSTMENTS = new Type(11,
                "SET_SCROLL_ADJUSTMENTS");

        /**
         * Overwrite was toggled.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type TOGGLE_OVERWRITE = new Type(12,
                "TOGGLE_OVERWRITE");
    }

    /**
     * Creates a new text view event
     * 
     * @param source
     *            The object that triggered the event.
     * @param type
     *            The identification of the type of event that occurred.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TextViewEvent(Object source, TextViewEvent.Type type) {
        super(source, type);
    }

    /**
     * Comparison of TextViewEvent types
     * 
     * @param test
     *            The TreeViewEvent type to compare with this object.
     * @return true if the types are the same.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(TextViewEvent.Type test) {
        return type.getID() == test.getID();
    }

    /**
     * Returns the menu.
     * 
     * @return The menu item that was set when the POPULATE_POPUP event type is
     *         triggered.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Returns the count.
     * 
     * @return int
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the menu.
     * 
     * @param menu
     *            The menu to set
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * Returns the movementStep.
     * 
     * @return MovementStep
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public MovementStep getMovementStep() {
        return movementStep;
    }

    /**
     * Sets the count.
     * 
     * @param count
     *            The count to set
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Sets the movementStep.
     * 
     * @param movementStep
     *            The movementStep to set
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setMovementStep(MovementStep movementStep) {
        this.movementStep = movementStep;
    }

}
