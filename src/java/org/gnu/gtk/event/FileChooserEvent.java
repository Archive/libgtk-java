package org.gnu.gtk.event;

import org.gnu.glib.EventType;

public class FileChooserEvent extends GtkEvent {
    public static class Type extends EventType {

        private Type(int id, String name) {
            super(id, name);
        }

        public static Type[] getTypes() {
            return types;
        }

        public static final Type CURRENT_FOLDER_CHANGED = new Type(0,
                "current-folder-changed");

        public static final Type FILE_ACTIVATED = new Type(1, "file-activated");

        public static final Type SELECTION_CHANGED = new Type(2,
                "selection-changed");

        public static final Type UPDATE_PREVIEW = new Type(3, "update-preview");

        private static Type[] types = new Type[] {
                FileChooserEvent.Type.CURRENT_FOLDER_CHANGED,
                FileChooserEvent.Type.FILE_ACTIVATED,
                FileChooserEvent.Type.SELECTION_CHANGED,
                FileChooserEvent.Type.UPDATE_PREVIEW };
    }

    public FileChooserEvent(Object source, FileChooserEvent.Type type) {
        super(source, type);
    }

    public boolean isOfType(FileChooserEvent.Type type) {
        return super.type.getID() == type.getID();
    }
}
