/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * @author Jeffrey S. Morgan
 * 
 * This is the listener interface for receiving focus events on a Widget.
 * Objects that are interested in focus events should implement this Interface
 * and then register with the <code>Widget</code> using the
 * <code>addFocusListener()</code> method.
 * 
 * @see FocusEvent
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface FocusListener {

    /**
     * Indicates that the <code>Widget</code> has gained or lost focus
     * 
     * @param event
     *            The event that identifies the details of the event
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean focusEvent(FocusEvent event);

}
