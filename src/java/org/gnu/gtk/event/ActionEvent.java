package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * An event representing the activation of an {@link org.gnu.gtk.Action} object
 * 
 * @since 2.8
 * @author ajocksch
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ActionEvent extends GtkEvent {

    /**
     * The type of the ActionEvent
     * 
     * @author ajocksch
     * 
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * The Action was activated
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static Type ACTIVATE = new Type(1, "ACTIVATE");
    }

    /**
     * creates a new ActionEvent
     * 
     * @param source
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ActionEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * Compares this event's type with the provided type
     * 
     * @param aType
     *            The type to test against
     * @return true if this event has the same type as aType
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ActionEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

}
