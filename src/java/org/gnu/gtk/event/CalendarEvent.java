/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * An event represeting action by a {@link org.gnu.gtk.Calendar} widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class CalendarEvent extends GtkEvent {

    /**
     * Type of a CalendarEvent
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * Emitted when the user selects a day.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DAY_SELECTED = new Type(1, "DAY_SELECTED");

        /**
         * Emitted when the user double-clicks on a day.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DAY_SELECTED_DOUBLE_CLICK = new Type(2,
                "DAY_SELECTED_DOUBLE_CLICK");

        /**
         * Emitted when the user clicks a button to change the selected month.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MONTH_CHANGED_NEXT = new Type(3,
                "MONTH_CHANGED_NEXT");

        /**
         * Emitted when the user clicks a button to change the selected month.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MONTH_CHANGED_PREV = new Type(4,
                "MONTH_CHANGED_PREV");

        /**
         * Emitted when the user clicks a button to change the selected year.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type YEAR_CHANGED_NEXT = new Type(5,
                "YEAR_CHANGED_NEXT");

        /**
         * Emitted when the user clicks a button to change the selected year.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type YEAR_CHANGED_PREV = new Type(6,
                "YEAR_CHANGED_PREV");
    }

    /**
     * Creates a new Calendar Event. This is used internally by java-gnome.
     * Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public CalendarEvent(Object source, CalendarEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(CalendarEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
