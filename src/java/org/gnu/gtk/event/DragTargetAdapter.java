/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * @author Ismael Juma <ismael@juma.me.uk>
 */

public class DragTargetAdapter implements DragTargetListener {

    public void destinationLeft(LeaveDragDestinationEvent event) {
    }

    public boolean dropped(DropDragEvent event) {
        return false;
    }

    public void dataReceived(ReceiveDragDataEvent event) {
    }

    public boolean motionOcurred(DragMotionEvent event) {
        return false;
    }
}
