/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.MovementStep;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreePath;
import org.gnu.gtk.TreeViewColumn;

/**
 * An event represeting action by a {@link org.gnu.gtk.TreeView} widget.
 * 
 * @see org.gnu.gtk.TreeView
 * @author Andrew Cowie
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class TreeViewEvent extends GtkEvent {

    private TreePath path;

    private TreeIter iter;

    private TreeViewColumn treeColumn;

    private boolean startEditing;

    private boolean logical;

    private boolean expand;

    private boolean openAll;

    /*
     * For move-cursor
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private MovementStep movementStep;

    private int howMany;

    /**
     * Which type of TreeModelEvent was received
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type ROW_ACTIVATED = new Type(1, "ROW_ACTIVATED");

        public static final Type ROW_EXPANDED = new Type(2, "ROW_EXPANDED");

        public static final Type ROW_COLLAPSED = new Type(3, "ROW_COLLAPSED");

        public static final Type COLUMNS_CHANGED = new Type(4,
                "COLUMNS_CHANGED");

        public static final Type SELECT_ALL = new Type(5, "SELECT_ALL");

        public static final Type UNSELECT_ALL = new Type(6, "UNSELECT_ALL");

        public static final Type SELECT_CURSOR_ROW = new Type(7,
                "SELECT_CURSOR_ROW");

        public static final Type TOGGLE_CURSOR_ROW = new Type(8,
                "TOGGLE_CURSOR_ROW");

        public static final Type EXPAND_COLLAPSE_CURSOR_ROW = new Type(9,
                "EXPAND_COLLAPSE_CURSOR_ROW");

        public static final Type SELECT_CURSOR_PARENT = new Type(10,
                "SELECT_CURSOR_PARENT");

        public static final Type START_INTERACTIVE_SEARCH = new Type(11,
                "START_INTERACTIVE_SEARCH");

        public static final Type MOVE_CURSOR = new Type(12, "MOVE_CURSOR");
    }

    /**
     * Creates a new TreeView Event. This is used internally by java-gnome.
     * Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreeViewEvent(Object source, TreeViewEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(TreeViewEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    /**
     * @return Returns the expand.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isExpand() {
        return expand;
    }

    /**
     * @param expand
     *            The expand to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    /**
     * @return Returns the iter.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreeIter getTreeIter() {
        return iter;
    }

    /**
     * @param iter
     *            The iter to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setTreeIter(TreeIter iter) {
        this.iter = iter;
    }

    /**
     * @return Returns the logical.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isLogical() {
        return logical;
    }

    /**
     * @param logical
     *            The logical to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setLogical(boolean logical) {
        this.logical = logical;
    }

    /**
     * @return Returns the openAll.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOpenAll() {
        return openAll;
    }

    /**
     * @param openAll
     *            The openAll to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setOpenAll(boolean openAll) {
        this.openAll = openAll;
    }

    /**
     * @return Returns the path.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreePath getTreePath() {
        return path;
    }

    /**
     * @param path
     *            The path to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setTreePath(TreePath path) {
        this.path = path;
    }

    /**
     * @return Returns the startEditing.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isStartEditing() {
        return startEditing;
    }

    /**
     * @param startEditing
     *            The startEditing to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setStartEditing(boolean startEditing) {
        this.startEditing = startEditing;
    }

    /**
     * @return Returns the treeColumn.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public TreeViewColumn getTreeColumn() {
        return treeColumn;
    }

    /**
     * @param treeColumn
     *            The treeColumn to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setTreeColumn(TreeViewColumn treeColumn) {
        this.treeColumn = treeColumn;
    }

    /**
     * If the event signal was GTK's "move-cursor" (ie
     * {@link TreeViewEvent.Type#MOVE_CURSOR MOVE_CURSOR}) then
     * 
     * @return the MovementSteps type that occured. You can expect
     *         {@link MovementStep#DISPLAY_LINES} for an Up/Down arrow press,
     *         {@link MovementStep#PAGES} for a PageUp/PageDown press, and
     *         {@link MovementStep#BUFFER_ENDS} for Home/End being presesd.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public MovementStep getMovementStep() {
        return movementStep;
    }

    /**
     * Internal method for use by event handlers in {@link org.gnu.gtk.TreeView}
     * only.
     * 
     * @param movementStep
     *            set the MovementStep object you created based on
     *            <code>GtkMovementStep arg1</code> value passed up from GTK
     *            on a "move-cursor" event signal.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setMovementStep(MovementStep movementStep) {
        this.movementStep = movementStep;
    }

    /**
     * If the event signal was GTK's "move-cursor" (ie
     * {@link TreeViewEvent.Type#MOVE_CURSOR MOVE_CURSOR}) then
     * 
     * @return the number of MovementSteps that occured. This is typically 1.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getHowMany() {
        return howMany;
    }

    /**
     * Internal method for use by event handlers in {@link org.gnu.gtk.TreeView}
     * only.
     * 
     * @param howMany
     *            set how many Movement Steps occured based on the
     *            <code>gint arg2</code> value passed up from GTK on a
     *            "move-cursor" event signal.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setHowMany(int howMany) {
        this.howMany = howMany;
    }
}
