package org.gnu.gtk.event;

public abstract class FileChooserAdapter implements FileChooserListener {

    public void currentFolderChanged(FileChooserEvent event) {
    }

    public void fileActivated(FileChooserEvent event) {
    }

    public void selectionChanged(FileChooserEvent event) {
    }

    public void updatePreview(FileChooserEvent event) {
    }
}
