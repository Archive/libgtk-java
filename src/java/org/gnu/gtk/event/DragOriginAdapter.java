/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * @author Ismael Juma <ismael@juma.me.uk>
 */

public class DragOriginAdapter implements DragOriginListener {

    public void dragStarted(StartDragEvent event) {
    }

    public void dragEnded(EndDragEvent event) {
    }

    public void dataRequested(RequestDragDataEvent event) {
    }

    public void dataDeleted(DeleteDragDataEvent event) {
    }
}
