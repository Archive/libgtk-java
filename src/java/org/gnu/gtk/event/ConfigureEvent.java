/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.EventConfigure;
import org.gnu.gdk.Window;
import org.gnu.glib.EventType;

/**
 * This event is used to identify a Configure event on a Widget.
 * 
 * @see ConfigureListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */

public class ConfigureEvent extends GtkEvent {

    private int x;

    private int y;

    private int width;

    private int height;

    private Window window;

    private boolean sendEvent;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CONFIGURE = new Type(1, "CONFIGURE");

    }

    /**
     * Constructor for ConfigureEvent.
     * 
     * @param source
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ConfigureEvent(Object source, EventType type, EventConfigure event) {
        super(source, type);
        this.x = event.getX();
        this.y = event.getY();
        this.width = event.getWidth();
        this.height = event.getHeight();
        this.window = event.getWindow();
        this.sendEvent = event.getSendEvent();
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ConfigureEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    /**
     * @return Returns the height.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return Returns the width.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return Returns the x.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getX() {
        return x;
    }

    /**
     * @return Returns the y.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getY() {
        return y;
    }

    /**
     * @return Returns the window
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Window getWindow() {
        return window;
    }
}
