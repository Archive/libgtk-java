/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.EventExpose;
import org.gnu.gdk.EventNoExpose;
import org.gnu.gdk.Rectangle;
import org.gnu.gdk.Region;
import org.gnu.gdk.Window;
import org.gnu.glib.EventType;

/**
 * This event object is used to identify how expose has changed.
 * 
 * @author Jonas Berlin
 * @see ExposeListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ExposeEvent extends GtkEvent {

    private final Window window;

    private final boolean sendEvent;

    private final Rectangle area;

    private final Region region;

    private final int count;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * Generated when all or part of a window becomes visible and needs to
         * be redrawn.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type EXPOSE = new Type(1, "EXPOSE");

        /**
         * Generated when the area of a Drawable being copied, with
         * Drawable.drawDrawable() or Window.copyArea(), was completely
         * available.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type NO_EXPOSE = new Type(2, "NO_EXPOSE");
    }

    public ExposeEvent(Object source, EventExpose gdkEvent) {
        super(source, Type.EXPOSE);
        window = gdkEvent.getWindow();
        sendEvent = gdkEvent.getSendEvent();
        area = gdkEvent.getArea();
        region = gdkEvent.getRegion();
        count = gdkEvent.getCount();
    }

    public ExposeEvent(Object source, EventNoExpose gdkEvent) {
        super(source, Type.NO_EXPOSE);
        window = gdkEvent.getWindow();
        sendEvent = gdkEvent.getSendEvent();
        area = null;
        region = null;
        count = -1;
    }

    /**
     * The window which received the event.
     * 
     * @return the window which received the event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * Check if the event was sent explicitly (eg using XSendEvent).
     * 
     * @return true if the event was sent explicitly (e.g. using XSendEvent),
     *         false otherwise.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean getSendEvent() {
        return sendEvent;
    }

    /**
     * Returns the bounding box of region.
     * 
     * @return the bounding box of region.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Rectangle getArea() {
        return area;
    }

    /**
     * Returns the region that needs to be redrawn.
     * 
     * @return the region that needs to be redrawn.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Region getRegion() {
        return region;
    }

    /**
     * Returns the number of contiguous EXPOSE events following this one. The
     * only use for this is "exposure compression", i.e. handling all contiguous
     * EXPOSE events in one go, though GDK performs some exposure compression so
     * this is not normally needed.
     * 
     * @return the number of contiguous EXPOSE events following this one.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getCount() {
        return count;
    }

    /**
     * Test to compare events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ExposeEvent.Type test) {
        return type.getID() == test.getID();
    }
}
