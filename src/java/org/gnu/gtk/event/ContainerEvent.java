/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.Widget;

/**
 * @author Tom Ball
 * 
 * This event is fired when container-specific changes are made, such as adding
 * a child widget to a container.
 * 
 * @see ContainerListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ContainerEvent extends GtkEvent {

    private Widget child;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * This event indicates that a child widget has been added to the
         * container.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type ADD = new Type(1, "ADD");

        /**
         * This event indicates that ...
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CHECK_RESIZE = new Type(2, "CHECK_RESIZE");

        /**
         * This event indicates that a child widget has been removed to the
         * container.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type REMOVE = new Type(3, "REMOVE");

        /**
         * This event indicates that a child widget has been given focus.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type SET_FOCUS_CHILD = new Type(4,
                "SET_FOCUS_CHILD");
    }

    /**
     * Construct a ContainerEvent object.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ContainerEvent(Object source, ContainerEvent.Type type) {
        this(source, type, null);
    }

    /**
     * Construct a ContainerEvent object with a specified child.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ContainerEvent(Object source, ContainerEvent.Type type, Widget child) {
        super(source, type);
        this.child = child;
    }

    /**
     * Return the child widget associated with this ContainerEvent, or null if
     * no child is involved. The CHECK_RESIZE event does not have an associated
     * child widget.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Widget getChild() {
        return child;
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ContainerEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
