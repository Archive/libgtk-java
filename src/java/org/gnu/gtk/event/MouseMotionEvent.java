/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2002 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.Device;
import org.gnu.gdk.EventMotion;
import org.gnu.gdk.ModifierType;
import org.gnu.gdk.Window;
import org.gnu.glib.EventType;

/**
 * This event object is used to identify how mouseMotion has changed. Note:
 * normally motion events are sent just when some mouse button is pressed. If
 * you want events also when no button is pressed, you should retrieve the
 * widget's GdkWindow using Widget.getWindow() and do:
 * 
 * <pre>
 * window.setEvents(window.getEvents().or(EventMask.POINTER_MOTION_MASK));
 * </pre>
 * 
 * @author Jonas Berlin
 * @see org.gnu.gtk.Widget#getWindow()
 * @see MouseMotionListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class MouseMotionEvent extends GtkEvent {

    private final Window window;

    private final boolean sendEvent;

    private final int timeMillis;

    private final double x;

    private final double y;

    // private final double[] axes;
    private final ModifierType state;

    private final boolean isHint;

    private final Device device;

    private final double xRoot;

    private final double yRoot;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * Pointer motion.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MOTION = new Type(1, "MOTION");
    }

    public MouseMotionEvent(Object source, EventMotion gdkEvent) {
        super(source, Type.MOTION);
        window = gdkEvent.getWindow();
        sendEvent = gdkEvent.getSendEvent();
        timeMillis = gdkEvent.getTimeMillis();
        x = gdkEvent.getX();
        y = gdkEvent.getY();
        state = gdkEvent.getState();
        isHint = gdkEvent.isHint();
        device = gdkEvent.getDevice();
        xRoot = gdkEvent.getXRoot();
        yRoot = gdkEvent.getYRoot();
    }

    /**
     * The window which received the event.
     * 
     * @return the window which received the event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * Check if the event was sent explicitly (eg using XSendEvent).
     * 
     * @return true if the event was sent explicitly (e.g. using XSendEvent),
     *         false otherwise.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean getSendEvent() {
        return sendEvent;
    }

    /**
     * Returns the time of the event in milliseconds.
     * 
     * @return the time of the event in milliseconds.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getTimeMillis() {
        return timeMillis;
    }

    /**
     * Returns the x coordinate of the pointer relative to the window.
     * 
     * @return the x coordinate of the pointer relative to the window.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the y coordinate of the pointer relative to the window.
     * 
     * @return the y coordinate of the pointer relative to the window.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getY() {
        return y;
    }

    /**
     * Returns x, y translated to the axes of device, or NULL if device is the
     * mouse.
     * 
     * @return x, y translated to the axes of device, or NULL if device is the
     *         mouse.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    /*
     * public double[] getAxes() { return axes; }
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */

    /**
     * Returns a bit-mask representing the state of the modifier keys (e.g.
     * Control, Shift and Alt) and the pointer buttons.
     * 
     * @return a bit-mask representing the state of the modifier keys and the
     *         pointer buttons.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ModifierType getState() {
        return state;
    }

    /**
     * Check if this event is just a hint.
     * 
     * @return true if this event is just a hint, false otherwise.
     * @see org.gnu.gdk.EventMask#POINTER_MOTION_HINT_MASK
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isHint() {
        return isHint;
    }

    /**
     * Returns the device where the event originated.
     * 
     * @return the device where the event originated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Device getDevice() {
        return device;
    }

    /**
     * Returns the x coordinate of the pointer relative to the root of the
     * screen.
     * 
     * @return the x coordinate of the pointer relative to the root of the
     *         screen.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getXRoot() {
        return xRoot;
    }

    /**
     * Returns the y coordinate of the pointer relative to the root of the
     * screen.
     * 
     * @return the y coordinate of the pointer relative to the root of the
     *         screen.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getYRoot() {
        return yRoot;
    }

    /**
     * Test to compare events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(MouseMotionEvent.Type test) {
        return type.getID() == test.getID();
    }

    /**
     * In the case that this event is a hint, this method triggers the next
     * event to be fired. If this method isn't called for cases where isHint
     * returns true, no further MouseMotionEvents will be fired
     * 
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void refireIfHint() {
        if (this.isHint())
            this.window.getPointerWindow();
    }
}
