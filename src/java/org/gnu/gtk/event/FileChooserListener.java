package org.gnu.gtk.event;

public interface FileChooserListener {
    public void currentFolderChanged(FileChooserEvent event);

    public void fileActivated(FileChooserEvent event);

    public void selectionChanged(FileChooserEvent event);

    public void updatePreview(FileChooserEvent event);
}
