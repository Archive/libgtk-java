/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.Action;
import org.gnu.gtk.Widget;

/**
 * An event represeting action by a {@link org.gnu.gtk.UIManager} object.
 * 
 * @see UIManagerListener
 * @see org.gnu.gtk.UIManager
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class UIManagerEvent extends GtkEvent {

    private Widget widget;

    private Action action;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type ADD_WIDGET = new Type(1, "ADD_WIDGET");

        public static final Type ACTIONS_CHANGED = new Type(2,
                "ACTIONS_CHANGED");

        public static final Type CONNECT_PROXY = new Type(3, "CONNECT_PROXY");

        public static final Type DISCONNECT_PROXY = new Type(4,
                "DISCONNECT_PROXY");

        public static final Type PRE_ACTIVATE = new Type(5, "PRE_ACTIVATE");

        public static final Type POST_ACTIVATE = new Type(6, "POST_ACTIVATE");
    }

    /**
     * Construct a UIManagerEvent object.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public UIManagerEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(UIManagerEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    /**
     * @return Returns the action.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action
     *            The action to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /**
     * @return Returns the widget.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Widget getWidget() {
        return widget;
    }

    /**
     * @param widget
     *            The widget to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setWidget(Widget widget) {
        this.widget = widget;
    }
}
