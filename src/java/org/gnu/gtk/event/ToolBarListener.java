/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

/**
 * This is the listener interface for receiving ToolBar events.
 * 
 * @see ToolBarEvent
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use signals. In particular, Listener
 *             interfaces and Event classes have been collapsed to a single
 *             interface in the new design.
 */
public interface ToolBarListener {

    /**
     * Indicates that the <code>ToolBar</code> has fired an event.
     * 
     * @param event
     *            The event that identifies the details of the event.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the project API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean toolBarEvent(ToolBarEvent event);
}
