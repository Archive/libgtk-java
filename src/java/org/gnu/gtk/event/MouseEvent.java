/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.gdk.EventButton;
import org.gnu.gdk.EventCrossing;
import org.gnu.gdk.EventScroll;
import org.gnu.gdk.ModifierType;
import org.gnu.gdk.ScrollDirection;
import org.gnu.gdk.Window;
import org.gnu.glib.EventType;

/**
 * This event represents mouse events.
 * 
 * @see MouseListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class MouseEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * This event indicates that the mouse has enter the widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type ENTER = new Type(1, "ENTER");

        /**
         * This event indicates that the mouse has left the widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type LEAVE = new Type(2, "LEAVE");

        /**
         * This event indicates that a mouse button has been pressed
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type BUTTON_PRESS = new Type(3, "BUTTON_PRESS");

        /**
         * This event indicates that a mouse button has been released
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type BUTTON_RELEASE = new Type(4, "BUTTON_RELEASE");

        /**
         * This event idicates that a mouse wheel was scrolled either up or
         * down.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type WHEEL_SCROLL = new Type(5, "WHEEL_SCROLL");
    }

    /*
     * Used to identify which button was pressed.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static final int BUTTON1 = 1;

    public static final int BUTTON2 = 2;

    public static final int BUTTON3 = 3;

    /*
     * Used to identify number of clicks
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static final int SINGLE_CLICK = 4;

    public static final int DOUBLE_CLICK = 5;

    public static final int TRIPLE_CLICK = 6;

    /**
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static final int TRIPPLE_CLICK = 6;

    /**
     * Represents which modifier keys (e.g. Control, Shift, and Alt) were held
     * when the mouse click occurred.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private ModifierType modifierKey;

    /**
     * Identifies what button was pressed for the BUTTON_PRESS or BUTTON_RELEASE
     * events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int buttonPressed = -1;

    /**
     * Provides the X coordinate for the BUTTON_PRESS and BUTTON_RELEASE events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private double x = 0;

    /**
     * Provides the Y coordinate for the BUTTON_PRESS and BUTTON_RELEASE events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private double y = 0;

    /**
     * Indicates single, double, or triple clicks
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int clickType = -1;

    /**
     * Indicates which way the mouse wheel scrolled for a WHEEL_SCROLL event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private ScrollDirection direction;

    private final Window window;

    /**
     * Construct a LifeCycleEvent object for the BUTTON event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public MouseEvent(Object source, Type type, EventButton gdkEvent) {
        super(source, type);
        this.x = gdkEvent.getX();
        this.y = gdkEvent.getY();
        this.buttonPressed = gdkEvent.getButton();
        this.clickType = gdkEvent.getType();
        this.modifierKey = gdkEvent.getModifierKey();
        this.window = gdkEvent.getWindow();
    }

    /**
     * Construct a LifeCycleEvent object for the WHEEL_SCROLL event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public MouseEvent(Object source, Type type, EventScroll gdkEvent) {
        super(source, type);
        this.direction = gdkEvent.getScrollDirection();
        this.window = gdkEvent.getWindow();
    }

    /**
     * Construct a LifeCycleEvent object for the ENTER and LEAVE event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public MouseEvent(Object source, Type type, EventCrossing gdkEvent) {
        super(source, type);
        this.window = gdkEvent.getWindow();
    }

    /**
     * Test to compare events.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(MouseEvent.Type test) {
        return type.getID() == test.getID();
    }

    /**
     * Returns the buttonPressed.
     * 
     * @return int
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getButtonPressed() {
        return buttonPressed;
    }

    /**
     * Returns the direction.
     * 
     * @return ScrollDirection
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ScrollDirection getDirection() {
        return direction;
    }

    /**
     * Returns the x.
     * 
     * @return double
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the y.
     * 
     * @return double
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public double getY() {
        return y;
    }

    /**
     * Returns the click type (single, double, triple). The return value should
     * be compared against one of the constants included in this class
     * (SINGLE_CLICK, DOUBLE_CLICK, or TRIPLE_CLICK).
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getClickType() {
        return clickType;
    }

    /**
     * Returns the modifier key used.
     * 
     * @return (see {@link org.gnu.gdk.ModifierType})
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ModifierType getModifierKey() {
        return modifierKey;
    }

    public Window getWindow() {
        return window;
    }
}
