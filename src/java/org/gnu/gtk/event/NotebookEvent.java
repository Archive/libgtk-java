package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/**
 * An event represeting action by a {@link org.gnu.gtk.Notebook} widget.
 * 
 * @see NotebookListener
 * @see org.gnu.gtk.Notebook
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */

public class NotebookEvent extends GtkEvent {

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type SELECT_PAGE = new Type(1, "SELECT_PAGE");

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type SWITCH_PAGE = new Type(2, "SWITCH_PAGE");
    }

    protected int pageNumber = -1;

    /**
     * Creates a new Notebook Event. This is used internally by java-gnome.
     * Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public NotebookEvent(Object source, Type type) {
        super(source, type);
    }

    /**
     * Creates a new Notebook Event. This is used internally by java-gnome.
     * Users only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public NotebookEvent(Object source, Type type, int pageNumber) {
        super(source, type);
        this.pageNumber = pageNumber;
    }

    /**
     * Return the page number on which this event occurred.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(NotebookEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
