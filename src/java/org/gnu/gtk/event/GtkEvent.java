/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import java.io.Serializable;

import org.gnu.glib.EventType;

/**
 * @author Jeffrey S. Morgan
 * 
 * The base event class for all Gtk events.
 * 
 * @see FocusEvent
 * @see KeyEvent
 * @see LifeCycleEvent
 * @see MouseEvent
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class GtkEvent implements Serializable {
    /** The type of event */
    protected EventType type;

    /** The object on which the event initially occurred. */
    protected Object source;

    /**
     * Construct a GtkEvent object with the specified source object and type.
     * 
     * @param source
     *            the object where the event originated.
     * @param type
     *            the event type.
     * 
     * @throws IllegalArgumentException
     *             if the source object is null
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public GtkEvent(Object source, EventType type) {
        if (null == source)
            throw new IllegalArgumentException("null source");
        this.source = source;
        this.type = type;
    }

    /**
     * Returns the object on which the event originally occured
     * 
     * @return Object on which the event originally occured
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Object getSource() {
        return source;
    }

    /**
     * @return The type of the event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public EventType getType() {
        return type;
    }

    /**
     * Generates a string representation of the event. Useful for debugging
     * applications.
     * 
     * @return string representation of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public String toString() {
        return getClass().getName() + "[source=" + source + ",id="
                + type.getName() + "]";
    }
}
