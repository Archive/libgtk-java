/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.DeleteType;
import org.gnu.gtk.MovementStep;

/**
 * This event is used to identify when an entry receives an event
 * 
 * @see EntryListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class EntryEvent extends GtkEvent {

    /**
     * The starting position of the deleted text. Applies to the DELETE_TEXT
     * type of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int startPosition;

    /**
     * The ending position of the deleted text. Applies to the DELETE_TEXT type
     * of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int endPosition;

    /**
     * The text that was inserted into the widget. Applies to the INSERT_TEXT
     * and INSET_AT_CURSOR type of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private String text;

    /**
     * The position at which the next text was inserted. Applies to the
     * INSERT_TEXT type of event.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int insertPosition;

    /**
     * deleteType is set when the DELETE_FROM_CURSOR event is invoked.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private DeleteType deleteType;

    /**
     * count is set when the DELETE_FROM_CURSOR or MOVE_CURSOR events are
     * invoked.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private int count;

    /**
     * movementStep is set when the MOVE_CURSOR event is invoked.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private MovementStep movementStep;

    /**
     * extendedSelection is set when the MOVE_CURSOR event is invoked.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    private boolean extendedSelection;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * Indicates that the user has pressed the enter key while in the widget
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type ACTIVATE = new Type(1, "ACTIVATE");

        /**
         * Indicates that the user has moved the cursor in the widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type MOVE_CURSOR = new Type(2, "MOVE_CURSOR");

        /**
         * Indicates that the user has inserted text at the cursor.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type INSERT_AT_CURSOR = new Type(3,
                "INSERT_AT_CURSOR");

        /**
         * Indicates that the user has deleted text starting at the cursor.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DELETE_FROM_CURSOR = new Type(4,
                "DELETE_FROM_CURSOR");

        /**
         * Clipboard related events
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CUT_CLIPBOARD = new Type(5, "CUT_CLIPBOARD");

        public static final Type COPY_CLIPBOARD = new Type(6, "COPY_CLIPBOARD");

        public static final Type PASTE_CLIPBOARD = new Type(7,
                "PASTE_CLIPBOARD");

        public static final Type TOGGLE_OVERWRITE = new Type(8,
                "TOGGLE_OVERWRITE");

        /**
         * Indicates that the user has changed the contents of the widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CHANGED = new Type(9, "CHANGED");

        /**
         * This type of event is emitted when the user deletes text from the
         * widget
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type DELETE_TEXT = new Type(10, "DELETE_TEXT");

        /**
         * This type of event is emitted when the user inserts text into the
         * widget.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type INSERT_TEXT = new Type(11, "INSERT_TEXT");

    }

    /**
     * Constructor for EditableEvent.
     * 
     * @param source
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public EntryEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(EntryEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setDeleteType(DeleteType type) {
        deleteType = type;
    }

    public DeleteType getDeleteType() {
        return deleteType;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setMovementStep(MovementStep step) {
        movementStep = step;
    }

    public MovementStep getMovementStep() {
        return movementStep;
    }

    public void setExtendedSelection(boolean extendedSelection) {
        this.extendedSelection = extendedSelection;
    }

    public boolean getExtendedSelection() {
        return extendedSelection;
    }

    /**
     * @return Returns the endPosition.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getEndPosition() {
        return endPosition;
    }

    /**
     * @param endPosition
     *            The endPosition to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    /**
     * @return Returns the insertPosition.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getInsertPosition() {
        return insertPosition;
    }

    /**
     * @param insertPosition
     *            The insertPosition to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setInsertPosition(int insertPosition) {
        this.insertPosition = insertPosition;
    }

    /**
     * @return Returns the startPosition.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getStartPosition() {
        return startPosition;
    }

    /**
     * @param startPosition
     *            The startPosition to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

}
