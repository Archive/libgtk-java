/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;

/**
 * An event representing action by a {@link org.gnu.gtk.Button} widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ButtonEvent extends GtkEvent {

    /**
     * Type of a ButtonEvent
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        /**
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type ACTIVATE = new Type(1, "ACTIVATE");

        /**
         * Emitted when a button clicked on by the mouse and the cursor stays on
         * the button. If the cursor is not on the button when the mouse button
         * is released, the signal is not emitted.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type CLICK = new Type(2, "CLICK");

        /**
         * Emitted when the mouse cursor enters the region of the button.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type ENTER = new Type(3, "ENTER");

        /**
         * Emitted when the mouse cursor leaves the region of the button.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type LEAVE = new Type(4, "LEAVE");

        /**
         * Emitted when the button is initially pressed.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type PRESS = new Type(5, "PRESS");

        /**
         * Emitted when a button which is pressed is released, no matter where
         * the mouse cursor is.
         * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
         *             interfaces each with a single method corresponding to the
         *             signature of the underlying callback.
         */
        public static final Type RELEASE = new Type(6, "RELEASE");
    }

    /**
     * Creates a new Button Event. This is used internally by java-gnome. Users
     * only have to deal with listeners.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ButtonEvent(Object source, ButtonEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ButtonEvent.Type aType) {
        return (type.getID() == aType.getID());
    }
}
