/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.Orientation;
import org.gnu.gtk.ToolBarStyle;

/**
 * This event is used to identify when a ToolBar receives an event
 * 
 * @see ToolBarListener
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. Signal handling an connection has been completely 
 *             re-implemented in java-gnome 4.0, so you will need to refactor
 *             any code attempting to use this class.
 */
public class ToolBarEvent extends GtkEvent {

    private Orientation orientation;

    private ToolBarStyle style;

    private int x;

    private int y;

    private int buttonNumber;

    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }

        public static final Type ORIENTATION_CHANGED = new Type(1,
                "ORIENTATION_CHANGED");

        public static final Type STYLE_CHANGED = new Type(2, "STYLE_CHANGED");

        public static final Type POPUP_CONTEXT_MENU = new Type(3,
                "POPUP_CONTEXT_MENU");
    }

    /**
     * Constructor for ToolBarEvent.
     * 
     * @param source
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ToolBarEvent(Object source, EventType type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public boolean isOfType(ToolBarEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    /**
     * @return Returns the buttonNumber.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getButtonNumber() {
        return buttonNumber;
    }

    /**
     * @param buttonNumber
     *            The buttonNumber to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setButtonNumber(int buttonNumber) {
        this.buttonNumber = buttonNumber;
    }

    /**
     * @return Returns the orientation.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation
     *            The orientation to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return Returns the style.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public ToolBarStyle getStyle() {
        return style;
    }

    /**
     * @param style
     *            The style to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setStyle(ToolBarStyle style) {
        this.style = style;
    }

    /**
     * @return Returns the x.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getX() {
        return x;
    }

    /**
     * @param x
     *            The x to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return Returns the y.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public int getY() {
        return y;
    }

    /**
     * @param y
     *            The y to set.
     * @deprecated Superceeded by java-gnome 4.0; Signals all have individual
     *             interfaces each with a single method corresponding to the
     *             signature of the underlying callback.
     */
    public void setY(int y) {
        this.y = y;
    }

}
