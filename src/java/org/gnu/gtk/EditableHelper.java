/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * This class implements the native methods that are used by classes that
 * implement {@link Editable}. Applications normally shouldn't use this class.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.EditableHelper</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class EditableHelper {

    /** Private constructor so we don't instantiate this class. */
    private EditableHelper() {
    }

    native static final int gtk_editable_get_type();

    native static final void gtk_editable_select_region(Handle editable,
            int start, int end);

    native static final boolean gtk_editable_get_selection_bounds(
            Handle editable, int[] start, int[] end);

    native static final void gtk_editable_insert_text(Handle editable,
            String newText, int newTextLength, int[] position);

    native static final void gtk_editable_delete_text(Handle editable,
            int startPos, int endPos);

    native static final String gtk_editable_get_chars(Handle editable,
            int start, int end);

    native static final void gtk_editable_cut_clipboard(Handle editable);

    native static final void gtk_editable_copy_clipboard(Handle editable);

    native static final void gtk_editable_paste_clipboard(Handle editable);

    native static final void gtk_editable_delete_selection(Handle editable);

    native static final void gtk_editable_set_position(Handle editable,
            int position);

    native static final int gtk_editable_get_position(Handle editable);

    native static final void gtk_editable_set_editable(Handle editable,
            boolean isEditable);

    native static final boolean gtk_editable_get_editable(Handle editable);

}
