/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class DebugFlag extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _MISC = 1 << 0;

    static final public org.gnu.gtk.DebugFlag MISC = new org.gnu.gtk.DebugFlag(
            _MISC);

    static final private int _PLUGSOCKET = 1 << 1;

    static final public org.gnu.gtk.DebugFlag PLUGSOCKET = new org.gnu.gtk.DebugFlag(
            _PLUGSOCKET);

    static final private int _TEXT = 1 << 2;

    static final public org.gnu.gtk.DebugFlag TEXT = new org.gnu.gtk.DebugFlag(
            _TEXT);

    static final private int _TREE = 1 << 3;

    static final public org.gnu.gtk.DebugFlag TREE = new org.gnu.gtk.DebugFlag(
            _TREE);

    static final private int _UPDATES = 1 << 4;

    static final public org.gnu.gtk.DebugFlag UPDATES = new org.gnu.gtk.DebugFlag(
            _UPDATES);

    static final private org.gnu.gtk.DebugFlag[] theInterned = new org.gnu.gtk.DebugFlag[] {
            new org.gnu.gtk.DebugFlag(0), MISC, PLUGSOCKET,
            new org.gnu.gtk.DebugFlag(3), TEXT, new org.gnu.gtk.DebugFlag(5),
            new org.gnu.gtk.DebugFlag(6), new org.gnu.gtk.DebugFlag(7), TREE,
            new org.gnu.gtk.DebugFlag(9), new org.gnu.gtk.DebugFlag(10),
            new org.gnu.gtk.DebugFlag(11), new org.gnu.gtk.DebugFlag(12),
            new org.gnu.gtk.DebugFlag(13), new org.gnu.gtk.DebugFlag(14),
            new org.gnu.gtk.DebugFlag(15), UPDATES }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.DebugFlag theSacrificialOne = new org.gnu.gtk.DebugFlag(
            0);

    static public org.gnu.gtk.DebugFlag intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.DebugFlag already = (org.gnu.gtk.DebugFlag) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.DebugFlag(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DebugFlag(int value) {
        value_ = value;
    }

    public org.gnu.gtk.DebugFlag or(org.gnu.gtk.DebugFlag other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.DebugFlag and(org.gnu.gtk.DebugFlag other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.DebugFlag xor(org.gnu.gtk.DebugFlag other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.DebugFlag other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
