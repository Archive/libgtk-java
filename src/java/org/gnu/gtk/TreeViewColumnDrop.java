/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

public interface TreeViewColumnDrop {

    /**
     * Determins whether column can be dropped in a particular spot (as
     * determined by prevColumn and nextColumn). In left to right locales,
     * prevColumn is on the left of the potential drop spot, and nextColumn is
     * on the right. In right to left mode, this is reversed. This method should
     * return TRUE if the spot is a valid drop spot. Please note that returning
     * TRUE does not actually indicate that the column drop was made, but is
     * meant only to indicate a possible drop spot to the user.
     * 
     * @param treeView
     *            A TreeView
     * @param column
     *            The TreeViewColumn being dragged
     * @param prevColumn
     *            A TreeViewColumn on one side of column
     * @param nextColumn
     *            A TreeViewColumn on the other side of column
     * 
     * @return TRUE, if column can be dropped in this spot
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean allowColumnDrop(TreeView treeView, TreeViewColumn column,
            TreeViewColumn prevColumn, TreeViewColumn nextColumn);

}
