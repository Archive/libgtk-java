/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Paned is the base class for widgets with two panes arranged either
 * horizontally (HPaned) or vertically (VPaned). The division between the two
 * children is set by default from the size request of the children but can be
 * adjusted by the user.
 * 
 * @see HPaned
 * @see VPaned
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Paned</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Paned extends Container {

    /**
     * Construct a new Paned from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Paned(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Paned getPaned(Handle handle) {
        if (handle == null)
            return null;

        Paned obj = (Paned) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Paned(handle);

        return obj;
    }

    /**
     * Add a child to the top or left pane with default parameters.
     * 
     * @param child
     *            The child Widget to add.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void add1(Widget child) {
        Paned.gtk_paned_add1(getHandle(), child.getHandle());
    }

    /**
     * Add a child to the bottom or right pane with default parameters.
     * 
     * @param child
     *            The child Widget to add.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void add2(Widget child) {
        Paned.gtk_paned_add2(getHandle(), child.getHandle());
    }

    /**
     * Adds a child to the top or left pane.
     * 
     * @param child
     *            The child Widget to add.
     * @param resize
     *            Should this child expand when the widget is resized.
     * @param shrink
     *            can this child be made smaller than its request.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void pack1(Widget child, boolean resize, boolean shrink) {
        Paned.gtk_paned_pack1(getHandle(), child.getHandle(), resize, shrink);
    }

    /**
     * Adds a child to the bottom or right pane.
     * 
     * @param child
     *            The child Widget to add.
     * @param resize
     *            Should this child expand when the widget is resized.
     * @param shrink
     *            can this child be made smaller than its request.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void pack2(Widget child, boolean resize, boolean shrink) {
        Paned.gtk_paned_pack2(getHandle(), child.getHandle(), resize, shrink);
    }

    /**
     * Set the position of the divider between the two panes.
     * 
     * @param position
     *            The pixel position of the divider; a negative value means that
     *            the position is unset.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPosition(int position) {
        Paned.gtk_paned_set_position(getHandle(), position);
    }

    /**
     * Get the position of the divider between the two panes.
     * 
     * @return The position of the divider between the two panes.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPosition() {
        return Paned.gtk_paned_get_position(getHandle());
    }

    public Widget getChild1() {
        Handle hndl = gtk_paned_get_child1(getHandle());
        return Widget.getWidget(hndl);
    }

    public Widget getChild2() {
        Handle hndl = gtk_paned_get_child2(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_paned_get_type());
    }

    native static final protected int gtk_paned_get_type();

    native static final protected void gtk_paned_add1(Handle paned, Handle child);

    native static final protected void gtk_paned_add2(Handle paned, Handle child);

    native static final protected void gtk_paned_pack1(Handle paned,
            Handle child, boolean resize, boolean shrink);

    native static final protected void gtk_paned_pack2(Handle paned,
            Handle child, boolean resize, boolean shirnk);

    native static final protected int gtk_paned_get_position(Handle paned);

    native static final protected void gtk_paned_set_position(Handle paned,
            int position);

    native static final protected Handle gtk_paned_get_child1(Handle paned);

    native static final protected Handle gtk_paned_get_child2(Handle paned);
    /*
     * Undocumented functions. native static final private void
     * gtk_paned_compute_position(Handle paned, int allocation, int child1_req,
     * int child2_req);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

}
