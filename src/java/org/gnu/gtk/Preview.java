/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.GC;
import org.gnu.gdk.Window;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Preview</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Preview extends Widget {

    /**
     * Construct a new PreviewType object
     * 
     * @param previewType
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Preview(PreviewType previewType) {
        super(gtk_preview_new(previewType.getValue()));
    }

    /**
     * Set the size for this Preview.
     * 
     * @param width
     * @param height
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(int width, int height) {
        gtk_preview_size(getHandle(), width, height);
    }

    public void put(Window win, GC gc, int srcX, int srcY, int destX,
            int destY, int width, int height) {
        gtk_preview_put(getHandle(), win.getHandle(), gc.getHandle(), srcX,
                srcY, destX, destY, width, height);
    }

    public void drawRow(byte[] data, int x, int y, int w) {
        gtk_preview_draw_row(getHandle(), data, x, y, w);
    }

    public void setExpand(boolean expand) {
        gtk_preview_set_expand(getHandle(), expand);
    }

    public void setGamma(double gamma) {
        gtk_preview_set_gama(gamma);
    }

    public void setColorCube(int numRedShades, int numGreenShades,
            int numBlueShades, int numGrayShades) {
        gtk_preview_set_color_cube(numRedShades, numGreenShades, numBlueShades,
                numGrayShades);
    }

    public void reset() {
        gtk_preview_reset();
    }

    native static protected int gtk_preview_get_type();

    native static protected void gtk_preview_uninit();

    native static protected Handle gtk_preview_new(int type);

    native static protected void gtk_preview_size(Handle preview, int width,
            int height);

    native static protected void gtk_preview_put(Handle preview, Handle window,
            Handle gc, int srcx, int srcy, int destx, int desty, int width,
            int height);

    native static protected void gtk_preview_draw_row(Handle preview,
            byte[] data, int x, int y, int w);

    native static protected void gtk_preview_set_expand(Handle preview,
            boolean expand);

    native static protected void gtk_preview_set_gama(double gamma);

    native static protected void gtk_preview_set_color_cube(int nredShades,
            int ngreenShades, int nblueShades, int ngrayShades);

    native static protected void gtk_preview_set_install_cmap(int installCmap);

    native static protected void gtk_preview_set_reserved(int nreserved);

    native static protected void gtk_preview_set_dither(Handle preview,
            int dither);

    native static protected void gtk_preview_reset();

}
