/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * A read only store of text attributes as returned by {@link
 * TextIter#getAttributes(TextAttributes)}.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextAttributes</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextAttributes extends Boxed {
    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected TextAttributes(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static TextAttributes getTextAttributes(Handle handle) {
        if (handle == null)
            return null;

        TextAttributes textAttributes = (TextAttributes) Boxed
                .getBoxedFromHandle(handle);

        if (textAttributes == null)
            textAttributes = new TextAttributes(handle);

        return textAttributes;
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextAppearance getAppearance() {
        Handle handle = getAppearance(getHandle());
        return TextAppearance.getTextAppearance(handle);
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Justification getJustification() {
        return Justification.intern(getJustification(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextDirection getDirection() {
        return TextDirection.intern(getDirection(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.pango.FontDescription getFontDescription() {
        return new org.gnu.pango.FontDescription(getFont(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getFontScale() {
        return getFontScale(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLeftMargin() {
        return getLeftMargin(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getIndent() {
        return getIndent(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRightMargin() {
        return getRightMargin(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPixelsAboveLines() {
        return getPixelsAboveLines(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPixelsBelowLines() {
        return getPixelsBelowLines(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPixelsInsideWrap() {
        return getPixelsInsideWrap(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.pango.TabArray getTabArray() {
        return new org.gnu.pango.TabArray(getTabs(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public WrapMode getWrapMode() {
        return WrapMode.intern(getWrapMode(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public org.gnu.pango.Language getLanguage() {
        return new org.gnu.pango.Language(getLanguage(getHandle()));
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getInvisible() {
        return getInvisible(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getBgFullHeight() {
        return getBgFullHeight(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getEditable() {
        return getEditable(getHandle());
    }

    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getRealized() {
        return getRealized(getHandle());
    }

    native static final protected Handle getAppearance(Handle cptr);

    native static final protected int getJustification(Handle cptr);

    native static final protected int getDirection(Handle cptr);

    native static final protected Handle getFont(Handle cptr);

    native static final protected double getFontScale(Handle cptr);

    native static final protected int getLeftMargin(Handle cptr);

    native static final protected int getIndent(Handle cptr);

    native static final protected int getRightMargin(Handle cptr);

    native static final protected int getPixelsAboveLines(Handle cptr);

    native static final protected int getPixelsBelowLines(Handle cptr);

    native static final protected int getPixelsInsideWrap(Handle cptr);

    native static final protected Handle getTabs(Handle cptr);

    native static final protected int getWrapMode(Handle cptr);

    native static final protected Handle getLanguage(Handle cptr);

    native static final protected boolean getInvisible(Handle cptr);

    native static final protected boolean getBgFullHeight(Handle cptr);

    native static final protected boolean getEditable(Handle cptr);

    native static final protected boolean getRealized(Handle cptr);

    native static final protected boolean getPad1(Handle cptr);

    native static final protected boolean getPad2(Handle cptr);

    native static final protected boolean getPad3(Handle cptr);

    native static final protected boolean getPad4(Handle cptr);

    native static final protected Handle gtk_text_attributes_new();

    native static final protected Handle gtk_text_attributes_copy(Handle src);

    native static final protected void gtk_text_attributes_unref(Handle values);

    native static final protected void gtk_text_attributes_ref(Handle values);
}
