/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class PackDirection extends Enum {

    static final private int _LTR = 0;

    static final public PackDirection LTR = new PackDirection(_LTR);

    static final private int _RTL = 1;

    static final public PackDirection RTL = new PackDirection(_RTL);

    static final private int _TTB = 2;

    static final public PackDirection TTB = new PackDirection(_TTB);

    static final private int _BTT = 3;

    static final public PackDirection BTT = new PackDirection(_BTT);

    static final private PackDirection[] theInterned = new PackDirection[] {
            LTR, RTL, TTB, BTT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private PackDirection theSacrificialOne = new PackDirection(0);

    static public PackDirection intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        PackDirection already = (PackDirection) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new PackDirection(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PackDirection(int value) {
        value_ = value;
    }

    public PackDirection or(PackDirection other) {
        return intern(value_ | other.value_);
    }

    public PackDirection and(PackDirection other) {
        return intern(value_ & other.value_);
    }

    public PackDirection xor(PackDirection other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(PackDirection other) {
        return (value_ & other.value_) == other.value_;
    }

}
