/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;

/**
 * The VScrollbar displays a vertical scrollbar with a slider and a pair of
 * arrow buttons. To create a scrollbar, you must specify the beginning and
 * ending values, the size of the slider, an initial slider position, and how
 * far the slider should move when the mouse clicks the slot.
 * 
 * @see HScrollBar
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.VScrollBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class VScrollBar extends ScrollBar {
    /**
     * Creates a new ScrollBar
     * 
     * @param adjustment
     *            The GtkAdjustment to use, or null to create a new adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScrollBar(Adjustment adjustment) {
        super(gtk_vscrollbar_new(adjustment == null ? null : adjustment
                .getHandle()));
    }

    /**
     * Construct a VScrollBar from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public VScrollBar(Handle handle) {
        super(handle);
    }

    /**
     * Construct a VScrollBar from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static VScrollBar getVScrollBar(Handle handle) {
        if (handle == null)
            return null;

        VScrollBar obj = (VScrollBar) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new VScrollBar(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_vscrollbar_get_type());
    }

    native static final protected int gtk_vscrollbar_get_type();

    native static final protected Handle gtk_vscrollbar_new(Handle adjustment);
}
