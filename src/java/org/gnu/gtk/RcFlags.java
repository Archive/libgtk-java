/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class RcFlags extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _FG = 1 << 0;

    static final public org.gnu.gtk.RcFlags FG = new org.gnu.gtk.RcFlags(_FG);

    static final private int _BG = 1 << 1;

    static final public org.gnu.gtk.RcFlags BG = new org.gnu.gtk.RcFlags(_BG);

    static final private int _TEXT = 1 << 2;

    static final public org.gnu.gtk.RcFlags TEXT = new org.gnu.gtk.RcFlags(
            _TEXT);

    static final private int _BASE = 1 << 3;

    static final public org.gnu.gtk.RcFlags BASE = new org.gnu.gtk.RcFlags(
            _BASE);

    static final private org.gnu.gtk.RcFlags[] theInterned = new org.gnu.gtk.RcFlags[] {
            new org.gnu.gtk.RcFlags(0), FG, BG, new org.gnu.gtk.RcFlags(3),
            TEXT, new org.gnu.gtk.RcFlags(5), new org.gnu.gtk.RcFlags(6),
            new org.gnu.gtk.RcFlags(7), BASE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.RcFlags theSacrificialOne = new org.gnu.gtk.RcFlags(
            0);

    static public org.gnu.gtk.RcFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.RcFlags already = (org.gnu.gtk.RcFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.RcFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private RcFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.RcFlags or(org.gnu.gtk.RcFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.RcFlags and(org.gnu.gtk.RcFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.RcFlags xor(org.gnu.gtk.RcFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.RcFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
