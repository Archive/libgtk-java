/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A {@link ToolItem} containing a button with an additional dropdown menu.
 * <p>
 * A MenuToolButton is a {@link ToolItem} that contains a button and a small
 * additional button with an arrow. When clicked, the arrow button pops up a
 * dropdown menu.
 * 
 * @since 2.6
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MenuToolButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class MenuToolButton extends ToolButton {

    /**
     * Construct a MenuToolButton from a native handle. For internal use only!
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static MenuToolButton getMenuToolButton(Handle handle) {
        if (handle == null)
            return null;

        MenuToolButton obj = (MenuToolButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new MenuToolButton(handle);

        return obj;
    }

    /**
     * Create a new MenuToolButton. No icon widget or label will be set.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton() {
        super(gtk_menu_tool_button_new(null, null));
    }

    /**
     * Create a new MenuToolButton with the given label. No icon widget will be
     * set.
     * 
     * @param label
     *            A String that will be used as label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton(String label) {
        super(gtk_menu_tool_button_new((Handle) null, label));
    }

    /**
     * Create a new MenuToolButton with the given icon widget. No label will be
     * set.
     * 
     * @param icon
     *            A Widget that will be used as icon widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton(Widget icon) {
        super(gtk_menu_tool_button_new(icon.getHandle(), (String) null));
    }

    /**
     * Create a new MenuToolButton with the given icon widget and label.
     * 
     * @param icon
     *            A Widget that will be used as icon widget.
     * @param label
     *            A String that will be used as label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton(Widget icon, String label) {
        super(gtk_menu_tool_button_new(icon.getHandle(), label));
    }

    /**
     * Create a new MenuToolButton from stock. The new MenuToolButton will
     * contain an icon and label from the stock item indicated by
     * <tt>stockid</tt>.
     * 
     * @param stockid
     *            The {@link GtkStockItem} to create.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuToolButton(GtkStockItem stockid) {
        super(gtk_menu_tool_button_new_from_stock(stockid.getString()));
    }

    /**
     * Sets the {@link Menu} that is popped up when the user clicks on the
     * arrow. If <tt>menu</tt> is <tt>null</tt>, the arrow button becomes
     * insensitive.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMenu(Menu menu) {
        if (menu == null) {
            gtk_menu_tool_button_set_menu(getHandle(), null);
        } else {
            gtk_menu_tool_button_set_menu(getHandle(), menu.getHandle());
        }
    }

    /**
     * Gets the {@link Menu} associated with this MenuToolButton.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Menu getMenu() {
        Handle handle = gtk_menu_tool_button_get_menu(getHandle());
        return Menu.getMenu(handle);
    }

    /**
     * Set the tooltip associated with the arrow menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setArrowToolTip(ToolTips tooltips, String tip_text,
            String tip_private) {
        gtk_menu_tool_button_set_arrow_tooltip(getHandle(), tooltips
                .getHandle(), tip_text, tip_private);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_menu_tool_button_get_type());
    }

    native static final private int gtk_menu_tool_button_get_type();

    native static final private Handle gtk_menu_tool_button_new(
            Handle icon_widget, String label);

    native static final private Handle gtk_menu_tool_button_new_from_stock(
            String stock_id);

    native static final private void gtk_menu_tool_button_set_menu(
            Handle button, Handle menu);

    native static final private Handle gtk_menu_tool_button_get_menu(
            Handle button);

    native static final private void gtk_menu_tool_button_set_arrow_tooltip(
            Handle button, Handle tooltips, String tip_text, String tip_private);

}
