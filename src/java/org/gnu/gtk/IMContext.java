/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Rectangle;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IMContext</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IMContext extends GObject {
    public IMContext(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static IMContext getIMContext(Handle handle) {
        if (handle == null)
            return null;

        IMContext obj = (IMContext) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new IMContext(handle);

        return obj;
    }

    /**
     * Sets the client window for the input context.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setClientWindow(org.gnu.gdk.Window window) {
        gtk_im_context_set_client_window(getHandle(), window.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_im_context_get_type());
    }

    public void focusIn() {
        gtk_im_context_focus_in(getHandle());
    }

    public void focusOut() {
        gtk_im_context_focus_out(getHandle());
    }

    public void reset() {
        gtk_im_context_reset(getHandle());
    }

    public void setCursorLocation(Rectangle rect) {
        gtk_im_context_set_cursor_location(getHandle(), rect.getHandle());
    }

    public void setUsePreedit(boolean usePreedit) {
        gtk_im_context_set_use_preedit(getHandle(), usePreedit);
    }

    native static final protected int gtk_im_context_get_type();

    native static final protected void gtk_im_context_set_client_window(
            Handle context, Handle window);

    native static final protected void gtk_im_context_get_preedit_string(
            Handle context, String[] str, int[] attrs, int[] cursorPos);

    native static final protected boolean gtk_im_context_filter_keypress(
            Handle context, Handle event);

    native static final protected void gtk_im_context_focus_in(Handle context);

    native static final protected void gtk_im_context_focus_out(Handle context);

    native static final protected void gtk_im_context_reset(Handle context);

    native static final protected void gtk_im_context_set_cursor_location(
            Handle context, Handle area);

    native static final protected void gtk_im_context_set_use_preedit(
            Handle context, boolean usePreedit);

    native static final protected void gtk_im_context_set_surrounding(
            Handle context, String text, int len, int cursorIndex);

    native static final protected boolean gtk_im_context_get_surrounding(
            Handle context, String[] text, int[] cursorIndex);

    native static final protected boolean gtk_im_context_delete_surrounding(
            Handle context, int offset, int numChars);

}
