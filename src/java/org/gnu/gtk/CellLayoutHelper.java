/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * Internal helper class for classes that implement the {@link CellLayout}
 * interface. Applications normally shouldn't use this class.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellLayoutHelper</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellLayoutHelper {

    public static void packStart(CellLayout layout, CellRenderer renderer,
            boolean expand) {
        gtk_cell_layout_pack_start(layout.getHandle(), renderer.getHandle(),
                expand);
    }

    public static void packEnd(CellLayout layout, CellRenderer renderer,
            boolean expand) {
        gtk_cell_layout_pack_end(layout.getHandle(), renderer.getHandle(),
                expand);
    }

    public static void clear(CellLayout layout) {
        gtk_cell_layout_clear(layout.getHandle());
    }

    public static void addAttributeMapping(CellLayout layout,
            CellRenderer renderer, CellRendererAttribute attribute,
            DataColumn column) {
        gtk_cell_layout_add_attribute(layout.getHandle(), renderer.getHandle(),
                attribute.toString(), column.getColumn());
    }

    public static void clearAttributeMappings(CellLayout layout,
            CellRenderer renderer) {
        gtk_cell_layout_clear_attributes(layout.getHandle(), renderer
                .getHandle());
    }

    public static void reorder(CellLayout layout, CellRenderer renderer,
            int position) {
        gtk_cell_layout_reorder(layout.getHandle(), renderer.getHandle(),
                position);
    }

    native static final private int gtk_cell_layout_get_type();

    native static final private void gtk_cell_layout_pack_start(Handle layout,
            Handle renderer, boolean expand);

    native static final private void gtk_cell_layout_pack_end(Handle layout,
            Handle renderer, boolean expand);

    native static final private void gtk_cell_layout_clear(Handle layout);

    native static final private void gtk_cell_layout_add_attribute(
            Handle layout, Handle renderer, String attribute, int column);

    native static final private void gtk_cell_layout_clear_attributes(
            Handle layout, Handle renderer);

    native static final private void gtk_cell_layout_reorder(Handle layout,
            Handle renderer, int position);

    // native static final private void gtk_cell_layout_set_attributes(Handle
    // layout);
    // native static final private void
    // gtk_cell_layout_set_cell_data_func(Handle layout);
}
