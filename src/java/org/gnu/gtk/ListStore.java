/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.glib.Value;

/**
 * The ListStore is the gtk Model used for constructing Lists and tables to be
 * displayed within {@link TreeView} widgets. For full details of what objects
 * are needed to construct lists and tables, please see the {@link TreeView}
 * description.
 * 
 * <p>
 * The list store has a number of {@link DataColumn}s, or data 'columns'
 * associated with it. These do <em>not</em> correspond to the organisation of
 * the output into columns. The data store can in fact be used with any number
 * of treeView widgets, each displaying a different part of the data. In these
 * Java bindings, we will try to use the term dataBlock for the data in the
 * ListStore and Column to refer to the columns to be displayed. The data to be
 * displayed is set in the {@link TreeViewColumn} objects, by associating
 * properties of {@link CellRenderer}'s with the data block used in this
 * object.
 * 
 * @author Mark Howard &lt;mh@debian.org&gt;
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ListStore</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ListStore extends TreeModel implements TreeSortable,
        TreeDragSource, TreeDragDest {
    // Hash to store the TreeIterComparison objects used to sort a column.
    protected HashMap sortMethods = null;

    private List columnTypes = new ArrayList();

    /**
     * Constructs a new ListStore object for storing data in a list style
     * manner. The data is stored in a set of numbered data blocks, each one of
     * which can then be displayed in a column by setting the appropriate
     * attribute mapping of the CellRenderers.
     * 
     * @param dataColumns
     *            a list of dataColumns for the store. These should be created
     *            as private variables of the required type - they will be
     *            needed later.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ListStore(DataColumn[] dataColumns) {
        super(initListStore(dataColumns));

        for (int i = 0; i < dataColumns.length; i++) {
            columnTypes.add(dataColumns[i]);
        }
    }

    private static Handle initListStore(DataColumn[] dataColumns) {
        int[] intTypes = new int[dataColumns.length];
        for (int i = 0; i < intTypes.length; i++) {
            intTypes[i] = dataColumns[i].getType().getTypeHandle();
            dataColumns[i].setColumn(i);
        }
        return gtk_list_store_newv(dataColumns.length, intTypes);
    }

    /**
     * Adds a row to the list. This method can be used to easily add data to the
     * list. Each item in the DataRow must match the {@link DataColumn} type
     * specified in the constructor of ListStore. For example, if it was passed
     * <code>{new DataColumnBoolean(), new DataColumnString()}</code> to the
     * constructor, then the DataRow must contain a Boolean and a String object,
     * respectively.
     * 
     * @param aRow
     *            a row to be added to the list
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addRow(DataRow aRow) {

        TreeIter rowIter = this.appendRow();
        int size = columnTypes.size();

        for (int i = 0; i < size; i++) {

            DataColumn dataCol = getDataColumn(i);
            Object rowItem = aRow.get(i);

            // TODO: should use gtk_list_store_set_valist instead

            trySetValue(rowIter, dataCol, rowItem);
        }
    }

    /**
     * Adds a row to the list, at the specified position. This method can be
     * used to easily add data to the list. Each item in the DataRow must match
     * the {@link DataColumn} type specified in the constructor of ListStore.
     * For example, if it was passed
     * <code>{new DataColumnBoolean(), new DataColumnString()}</code> to the
     * constructor, then the DataRow must contain a Boolean and a String object,
     * respectively.
     * 
     * @param aRow
     *            a row to be added to the list
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addRowAt(DataRow aRow, int aPosition) {

        TreeIter rowIter = this.insertRow(aPosition);
        int size = columnTypes.size();

        for (int i = 0; i < size; i++) {

            DataColumn dataCol = getDataColumn(i);
            Object rowItem = aRow.get(i);

            // TODO: should use gtk_list_store_set_valist instead

            trySetValue(rowIter, dataCol, rowItem);
        }
    }

    /**
     * @param rowIter
     * @param dataCol
     * @param rowItem
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private void trySetValue(TreeIter rowIter, DataColumn dataCol,
            Object rowItem) {

        if (dataCol instanceof DataColumnBoolean) {
            setValue(rowIter, (DataColumnBoolean) dataCol, ((Boolean) rowItem)
                    .booleanValue());

        } else if (dataCol instanceof DataColumnDouble) {
            setValue(rowIter, (DataColumnDouble) dataCol, ((Double) rowItem)
                    .doubleValue());

        } else if (dataCol instanceof DataColumnIconSize) {
            setValue(rowIter, (DataColumnIconSize) dataCol,
                    ((IconSize) rowItem));

        } else if (dataCol instanceof DataColumnInt) {
            setValue(rowIter, (DataColumnInt) dataCol, ((Integer) rowItem)
                    .intValue());

        } else if (dataCol instanceof DataColumnObject) {
            setValue(rowIter, (DataColumnObject) dataCol, rowItem);

        } else if (dataCol instanceof DataColumnPixbuf) {
            setValue(rowIter, (DataColumnPixbuf) dataCol, ((Pixbuf) rowItem));

        } else if (dataCol instanceof DataColumnStockItem) {
            setValue(rowIter, (DataColumnStockItem) dataCol,
                    ((GtkStockItem) rowItem));

        } else if (dataCol instanceof DataColumnString) {
            setValue(rowIter, (DataColumnString) dataCol, ((String) rowItem));
        }
    }

    /**
     * Gets all the values in the row indicated by <code>aIter</code>. There
     * are several ways you can get a <code>TreeIter</code> to pass; for
     * example: using methods like <code>getIter</code> and
     * <code>getFirstIter()</code>, or from a <code>TreeSelection</code>.
     * 
     * @param aIter
     *            the iter pointing to the row
     * @return a <code>DataRow</code> filled with the values of the row.
     * @see TreeModel#getIter(String)
     * @see TreeModel#getIter(TreePath)
     * @see TreeModel#getFirstIter()
     * @see TreeSelection
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataRow getRowAt(TreeIter aIter) {

        DataRow row = new DataRow();
        int size = columnTypes.size();

        for (int i = 0; i < size; i++) {

            // TODO: should use gtk_tree_model_get_valist ()
            // instead of calling getValue for each column

            DataColumn dataCol = getDataColumn(i);

            if (dataCol instanceof DataColumnBoolean) {
                row.add(Boolean.valueOf(getValue(aIter,
                        (DataColumnBoolean) dataCol)));

            } else if (dataCol instanceof DataColumnDouble) {
                row
                        .add(new Double(getValue(aIter,
                                (DataColumnDouble) dataCol)));

            } else if (dataCol instanceof DataColumnIconSize) {
                row.add(new Integer(getValue(aIter,
                        (DataColumnIconSize) dataCol)));

            } else if (dataCol instanceof DataColumnInt) {
                row.add(new Integer(getValue(aIter, (DataColumnInt) dataCol)));

            } else if (dataCol instanceof DataColumnObject) {
                row.add(getValue(aIter, (DataColumnObject) dataCol));

            } else if (dataCol instanceof DataColumnPixbuf) {
                row.add(getValue(aIter, (DataColumnPixbuf) dataCol));

            } else if (dataCol instanceof DataColumnStockItem) {
                row.add(getValue(aIter, (DataColumnStockItem) dataCol));

            } else if (dataCol instanceof DataColumnString) {
                row.add(getValue(aIter, (DataColumnString) dataCol));
            }
        }

        return row;
    }

    /**
     * Sets all columns pointed by <code>aIter</code> to the values stored in
     * <code>aRow</code>.
     * 
     * @param aRow
     *            a row with items
     * @param aIter
     *            a <code>TreeIter</code> pointing to a row in the tree.
     * @see #getRowAt(TreeIter)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRowAt(DataRow aRow, TreeIter aIter) {

        // TODO: should use gtk_list_store_set_valist instead

        int size = columnTypes.size();

        for (int i = 0; i < size; i++) {
            trySetValue(aIter, getDataColumn(i), aRow.get(i));
        }
    }

    /**
     * This function is meant primarily for GObjects that inherit from
     * ListStore, and should only be used when constructing a new ListStore. It
     * will not function after a row has been added, or a method on the
     * TreeModel interface is called.
     * 
     * @param dataColumns
     *            the data columns to be set
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColumnTypes(DataColumn[] dataColumns) {
        int[] intTypes = new int[dataColumns.length];
        for (int i = 0; i < intTypes.length; i++) {
            intTypes[i] = dataColumns[i].getType().getTypeHandle();
            dataColumns[i].setColumn(i);
            columnTypes.add(dataColumns[i]);
        }

        gtk_list_store_set_column_types(getHandle(), dataColumns.length,
                intTypes);
    }

    /**
     * Returns the type of the column <code>aCol</code> as a
     * <code>DataColumn</code>. This is similar to calling
     * <code>getType(int)</code>; the difference is that this method returns
     * the type as a DataColumn, which is a more high-level structure.
     * 
     * @param aCol
     *            the column index
     * @return the corresponding DataColumn
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataColumn getDataColumn(int aCol) {
        return (DataColumn) columnTypes.get(aCol);
    }

    /**
     * Returns a <code>List</code> with the types for every column as a
     * <code>DataColumn</code> object.
     * 
     * @return the list of <code>DataColumn</code>'s
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public List getAllDataColumns() {
        return columnTypes;
    }

    /**
     * Sets a value in the data store. To display the data in the widget, you
     * need to associate the datablock with the renderer, using methods of the
     * {@link TreeViewColumn}.
     * 
     * @param iter
     *            A valid iterator which specifies the row in which the data
     *            should be set. Iterators can be gained by using methods such
     *            as {@link #appendRow()}.
     * @param dataBlock
     *            The data block to store the value in.
     * @param value
     *            The value to store. This <em>must</em> be of the same type
     *            for the column as that set in the constructor to the
     *            ListStore.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnString dataBlock, String value) {
        Value val = new Value(Type.STRING());
        val.setString(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the dataStore. The type of the value <em>must</em>
     * match the type set for that dataBlock in the constructor.
     * <p>
     * This does not make the data visible in any of the widgets which use the
     * class - to do that, you have to construct a {@link TreeViewColumn} and
     * add it to the {@link TreeView}; construct and add a {@link CellRenderer}
     * to that; and finally associate the properties of the CellRenderer with
     * the dataBlocks, using the
     * {@link TreeViewColumn#addAttributeMapping(CellRenderer, CellRendererAttribute,
     * DataColumn)} method.
     * 
     * @param dataBlock
     *            The datablock in which the data should be stored.
     * @param iter
     *            Valid iterator for the data row in which the value is to be
     *            set. These can be gotten using methods such as
     *            {@link #appendRow()}.
     * @param value
     *            The value to be set.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnStockItem dataBlock,
            GtkStockItem value) {
        Value val = new Value(Type.STRING());
        val.setString(value.getString());
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the dataStore. The type of the value <em>must</em>
     * match the type set for that dataBlock in the constructor.
     * <p>
     * This does not make the data visible in any of the widgets which use the
     * class - to do that, you have to construct a {@link TreeViewColumn} and
     * add it to the {@link TreeView}; construct and add a {@link CellRenderer}
     * to that; and finally associate the properties of the CellRenderer with
     * the dataBlocks, using the
     * {@link TreeViewColumn#addAttributeMapping(CellRenderer, CellRendererAttribute,
     * DataColumn)} method.
     * 
     * @param dataBlock
     *            The datablock in which the data should be stored.
     * @param iter
     *            Valid iterator for the data row in which the value is to be
     *            set. These can be gotten using methods such as
     *            {@link #appendRow()}.
     * @param value
     *            The value to be set.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnIconSize dataBlock,
            IconSize value) {
        Value val = new Value(Type.INT());
        val.setInteger(value.getValue());
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the data store. To display the data in the widget, you
     * need to associate the datablock with the renderer, using methods of the
     * {@link TreeViewColumn}.
     * 
     * @param iter
     *            A valid iterator which specifies the row in which the data
     *            should be set. Iterators can be gained by using methods such
     *            as {@link #appendRow()}.
     * @param dataBlock
     *            The data block to store the value in.
     * @param value
     *            The value to store. This <em>must</em> be of the same type
     *            for the column as that set in the constructor to the
     *            ListStore.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnInt dataBlock, int value) {
        Value val = new Value(Type.INT());
        val.setInteger(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the dataStore. The type of the value <em>must</em>
     * match the type set for that dataBlock in the constructor.
     * <p>
     * This does not make the data visible in any of the widgets which use the
     * class - to do that, you have to construct a {@link TreeViewColumn} and
     * add it to the {@link TreeView}; construct and add a {@link CellRenderer}
     * to that; and finally associate the properties of the CellRenderer with
     * the dataBlocks, using the
     * {@link TreeViewColumn#addAttributeMapping(CellRenderer, CellRendererAttribute,
     * DataColumn)} method.
     * 
     * @param dataBlock
     *            The datablock in which the data should be stored.
     * @param iter
     *            Valid iterator for the data row in which the value is to be
     *            set. These can be gotten using methods such as
     *            {@link #appendRow(TreeIter)}.
     * @param value
     *            The <code>long</code> value to set into this row.
     * @since 2.8.5
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnLong dataBlock, long value) {
        Value val = new Value(Type.LONG());
        val.setLong(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the data store. To display the data in the widget, you
     * need to associate the datablock with the renderer, using methods of the
     * {@link TreeViewColumn}.
     * 
     * @param iter
     *            A valid iterator which specifies the row in which the data
     *            should be set. Iterators can be gained by using methods such
     *            as {@link #appendRow()}.
     * @param dataBlock
     *            The data block to store the value in.
     * @param value
     *            The value to store. This <em>must</em> be of the same type
     *            for the column as that set in the constructor to the
     *            ListStore.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnBoolean dataBlock,
            boolean value) {
        Value val = new Value(Type.BOOLEAN());
        val.setBoolean(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the data store. To display the data in the widget, you
     * need to associate the datablock with the renderer, using methods of the
     * {@link TreeViewColumn}.
     * 
     * @param iter
     *            A valid iterator which specifies the row in which the data
     *            should be set. Iterators can be gained by using methods such
     *            as {@link #appendRow()}.
     * @param dataBlock
     *            The data block to store the value in.
     * @param value
     *            The value to store.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnDouble dataBlock, double value) {
        Value val = new Value(Type.DOUBLE());
        val.setDouble(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the dataStore. The type of the value <em>must</em>
     * match the type set for that dataBlock in the constructor.
     * 
     * @param dataBlock
     *            The datablock in which the data should be stored.
     * @param iter
     *            Valid iterator for the data row in which the value is to be
     *            set. These can be gotten using methods such as
     *            {@link #appendRow()}.
     * @param value
     *            The value to be set.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnObject dataBlock, Object value) {
        Value val = new Value(Type.JAVA_OBJECT());
        val.setJavaObject(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Sets a value in the dataStore. The type of the value <em>must</em>
     * match the type set for that dataBlock in the constructor.
     * <p>
     * This does not make the data visible in any of the widgets which use the
     * class
     * 
     * @param dataBlock
     *            The datablock in which the data should be stored.
     * @param iter
     *            Valid iterator for the data row in which the value is to be
     *            set. These can be gotten using methods such as
     *            {@link #appendRow()}.
     * @param value
     *            The value to be set. This <em>must</em> match the type for
     *            that dataBlock, as set in the constructor.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValue(TreeIter iter, DataColumnPixbuf dataBlock,
            Pixbuf value) {
        Value val = new Value(Type.OBJECT());
        val.setJavaObject(value);
        gtk_list_store_set_value(getHandle(), iter.getHandle(), dataBlock
                .getColumn(), val.getHandle());
    }

    /**
     * Removes the given row from the list store. After being removed,
     * <code>iter</code> is set to be the next valid row, or invalidated if it
     * pointed to the last row in this store.
     * 
     * @param iter
     *            iterator for the row to be removed.
     * @return TRUE if iter is valid, FALSE if not.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean removeRow(TreeIter iter) {

        // TODO: gotta check if iter is really being set to the next row

        return gtk_list_store_remove(getHandle(), iter.getHandle());
    }

    /**
     * Creates a new row at <code>position</code>. If position is larger than
     * the number of rows on the list, then the new row will be appended to the
     * list.
     * 
     * @param position
     *            The position to place the new row, starting at 0.
     * @return Iterator for the new row
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeIter insertRow(int position) {
        return TreeIter.getTreeIter(
                gtk_list_store_insert(getHandle(), position), this);
    }

    /**
     * Inserts a new row before sibling. If sibling is NULL, then the row will
     * be appended to the end of the list. iter will be changed to point to this
     * new row.
     * 
     * @param sibling
     * @return Iterator for the new row
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeIter insertRowBefore(TreeIter sibling) {
        Handle siblingHandle = (sibling == null ? null : sibling.getHandle());
        return TreeIter.getTreeIter(gtk_list_store_insert_before(getHandle(),
                siblingHandle), this);
    }

    /**
     * Inserts a new row after sibling. If sibling is NULL, then the row will be
     * prepended to the beginning of the list. iter will be changed to point to
     * this new row.
     * 
     * @param sibling
     * @return Iterator for the new row.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeIter insertRowAfter(TreeIter sibling) {
        Handle siblingHandle = (sibling == null ? null : sibling.getHandle());
        return TreeIter.getTreeIter(gtk_list_store_insert_after(getHandle(),
                siblingHandle), this);
    }

    /**
     * Prepends a new row to list_store. The <tt>iter</tt> will be changed to
     * point to this new row. The row will be empty after this function is
     * called.
     * 
     * @return Iterator for the new row
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeIter prependRow() {
        return TreeIter.getTreeIter(gtk_list_store_prepend(getHandle()), this);
    }

    /**
     * Appends a new row to the store
     * 
     * @return Iterator for the new row
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TreeIter appendRow() {
        return TreeIter.getTreeIter(gtk_list_store_append(getHandle()), this);
    }

    /**
     * Removes all rows from the list store.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clear() {
        gtk_list_store_clear(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_list_store_get_type());
    }

    // 
    // TreeSortable interface.
    //

    /**
     * Set the column in the list to sort on.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortColumn(DataColumn column, SortType order) {
        TreeSortableHelper.setSortColumn(this, column, order);
    }

    /**
     * Get a DataColumn object representing the currently sorted column. This is
     * not the same DataColumn used to create the store. It is only of type
     * DataColumn (not DataColumnString, etc). It can be compared with another
     * DataColumn object using the <tt>{@link DataColumn#equals}</tt> method.
     * 
     * @return A DataColumn object representing the currently sorted column or
     *         <tt>null</tt> if there is no column currently sorted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataColumn getSortColumn() {
        return TreeSortableHelper.getSortColumn(this);
    }

    /**
     * Get the current sorting order of the store.
     * 
     * @return A SortType object defining the current sorting order of the store
     *         or <tt>null</tt> if there is no current sort order.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public SortType getSortOrder() {
        return TreeSortableHelper.getSortOrder(this);
    }

    /**
     * Set the class used to sort the list according to the values stored in the
     * given DataColumn.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSortMethod(TreeIterComparison method, DataColumn column) {
        TreeSortableHelper.setSortMethod(this, method, column);
    }

    /**
     * Call-back method invoked by the JNI code when sorting is required. This
     * is for internal use only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int handleCompareFunc(Handle model, Handle aIter, Handle bIter,
            int col) {
        TreeIterComparison method = TreeSortableHelper.getMethod(this, col);
        if (method != null) {
            TreeModel mdl = (TreeModel) getGObjectFromHandle(model);
            TreeIter a = TreeIter.getTreeIter(aIter, mdl);
            TreeIter b = TreeIter.getTreeIter(bIter, mdl);
            return method.compareTreeIters(mdl, a, b);
        } else {
            return 0;
        }
    }

    /**
     * This method has not yet been implemented.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDragDestListener(TreeDragDestListener listener) {
        // TODO
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * This method has not yet been implemented.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDragSourceListener(TreeDragSourceListener listener) {
        // TODO
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Swaps a and b in the same level of tree_store. Note that this function
     * only works with unsorted stores.
     * 
     * @since 2.2
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void swapRows(TreeIter a, TreeIter b) {
        gtk_list_store_swap(getHandle(), a.getHandle(), b.getHandle());
    }

    /**
     * Moves iter to the end of the model
     * 
     * @since 2.2
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveRowToEnd(TreeIter iter) {
        gtk_list_store_move_before(getHandle(), iter.getHandle(), null);
    }

    /**
     * Moves iter in this store to the position after position. The
     * <tt>iter</tt> and <tt>position</tt> should be in the same level. Note
     * that this function only works with unsorted stores.
     * 
     * @since 2.2
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveRowAfter(TreeIter iter, TreeIter position) {
        gtk_list_store_move_after(getHandle(), iter.getHandle(), position
                .getHandle());
    }

    /**
     * Moves iter in this store to the start of the store.
     * 
     * @since 2.2
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveRowToStart(TreeIter iter) {
        gtk_list_store_move_after(getHandle(), iter.getHandle(), null);
    }

    /**
     * Moves iter in store to the position before position. The <tt>iter</tt>
     * and <tt>position</tt> should be in the same level. Note that this
     * function only works with unsorted stores.
     * 
     * @since 2.2
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveRowBefore(TreeIter iter, TreeIter position) {
        gtk_list_store_move_before(getHandle(), iter.getHandle(), position
                .getHandle());
    }

    public boolean isIterValid(TreeIter iter) {
        if (iter == null)
            return false;

        return gtk_list_store_iter_is_valid(getHandle(), iter.getHandle());
    }

    public void reorder(int[] newOrder) {
        gtk_list_store_reorder(getHandle(), newOrder);
    }

    native static final protected int gtk_list_store_get_type();

    native static final protected Handle gtk_list_store_newv(int numColumns,
            int[] types);

    native static final protected void gtk_list_store_set_column_types(
            Handle listStore, int numColumns, int[] types);

    native static final protected void gtk_list_store_set_value(
            Handle listStore, Handle iter, int column, Handle value);

    native static final protected boolean gtk_list_store_remove(
            Handle listStore, Handle iter);

    native static final protected Handle gtk_list_store_insert(
            Handle listStore, int position);

    native static final protected Handle gtk_list_store_insert_before(
            Handle listStore, Handle sibling);

    native static final protected Handle gtk_list_store_insert_after(
            Handle listStore, Handle sibling);

    native static final protected Handle gtk_list_store_prepend(Handle listStore);

    native static final protected Handle gtk_list_store_append(Handle listStore);

    native static final protected void gtk_list_store_clear(Handle listStore);

    native static final protected boolean gtk_list_store_iter_is_valid(
            Handle listStore, Handle iter);

    native static final protected void gtk_list_store_reorder(Handle listStore,
            int[] newOrder);

    native static final protected void gtk_list_store_swap(Handle listStore,
            Handle iterA, Handle iterB);

    native static final protected void gtk_list_store_move_after(
            Handle listStore, Handle iter, Handle position);

    native static final protected void gtk_list_store_move_before(
            Handle listStore, Handle iter, Handle position);

    native static protected void tree_sortable_set_sort_column_id(
            Handle sortable, int col, int order);

    // native static final protected int getRoot (int cptr);
    // native final protected void setRoot (int cptr, int root);
    // native static final protected int getTail (int cptr);
    // native final protected void setTail (int cptr, int tail);
    // native static final protected int getNColumns (int cptr);
    // native final protected void setNColumns (int cptr, int n_columns);
    // native static final protected int getColumnHeaders (int cptr);
    // native final protected void setColumnHeaders (int cptr, int
    // column_headers);
    // native static final protected int getLength (int cptr);
    // native final protected void setLength (int cptr, int length);
    // native static final protected boolean getColumnsDirty (int cptr);
    // native final protected void setColumnsDirty (int cptr, boolean
    // columns_dirty);
}
