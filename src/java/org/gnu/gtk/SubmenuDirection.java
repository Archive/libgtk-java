/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class SubmenuDirection extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LEFT = 0;

    static final public org.gnu.gtk.SubmenuDirection LEFT = new org.gnu.gtk.SubmenuDirection(
            _LEFT);

    static final private int _RIGHT = 1;

    static final public org.gnu.gtk.SubmenuDirection RIGHT = new org.gnu.gtk.SubmenuDirection(
            _RIGHT);

    static final private org.gnu.gtk.SubmenuDirection[] theInterned = new org.gnu.gtk.SubmenuDirection[] {
            LEFT, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SubmenuDirection theSacrificialOne = new org.gnu.gtk.SubmenuDirection(
            0);

    static public org.gnu.gtk.SubmenuDirection intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SubmenuDirection already = (org.gnu.gtk.SubmenuDirection) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SubmenuDirection(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SubmenuDirection(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SubmenuDirection or(org.gnu.gtk.SubmenuDirection other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SubmenuDirection and(org.gnu.gtk.SubmenuDirection other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SubmenuDirection xor(org.gnu.gtk.SubmenuDirection other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SubmenuDirection other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
