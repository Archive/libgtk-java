/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixmap;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;
import org.gnu.pango.FontDescription;
import org.gnu.pango.Scale;
import org.gnu.pango.Stretch;
import org.gnu.pango.Style;
import org.gnu.pango.TabArray;
import org.gnu.pango.Underline;
import org.gnu.pango.Variant;
import org.gnu.pango.Weight;

/**
 * See the {@link TextBuffer} description for an overview of these classes.
 * <p>
 * TextTags should be created using the {@link TextBuffer#createTag(String)}
 * method.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextTag</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextTag extends GObject {
    /**
     * Construct a new TextTag
     * 
     * @param name
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextTag(String name) {
        super(gtk_text_tag_new(name));
    }

    protected TextTag(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static TextTag getTextTag(Handle handle) {
        if (handle == null)
            return null;

        TextTag obj = (TextTag) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TextTag(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_text_tag_get_type());
    }

    /**
     * Returns the name of the tag
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return getName(getHandle());
    }

    /**
     * Sets the priority of the Tag. Valid priorities are start at 0 and go to
     * one less than {@link TextTagTable#getSize()}. Each tag in a table has a
     * unique priority; setting the priority of one tag shifts the priorities of
     * all the other tags in the table to maintain a unique priority for each
     * tag. Higher priority tags "win" if two tags both set the same text
     * attribute. When adding a tag to a tag table, it will be assigned the
     * highest priority in the table by default; so normally the precedence of a
     * set of tags is the order in which they were added to the table, or
     * created with {@link TextBuffer#createTag(String)}, which adds the tag to
     * the buffer's table automatically.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPriority(int priority) {
        gtk_text_tag_set_priority(getHandle(), priority);
    }

    /**
     * Returns the priority of the tag.
     * 
     * @see #setPriority(int)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPriority() {
        return gtk_text_tag_get_priority(getHandle());
    }

    /**
     * Sets the background color as a string.
     * 
     * @param background
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackground(String background) {
        setStringProperty(getHandle(), "background", background);
    }

    /**
     * Whether the background color fills the entire line height or only the
     * height of the tagged characters.
     * 
     * @param fullHeight
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackgroundFullHeight(boolean fullHeight) {
        setBooleanProperty(getHandle(), "background-full-height", fullHeight);
    }

    /**
     * Bitmap to use as a mask when drawing the text background.
     * 
     * @param stipple
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackgroundStipple(Pixmap stipple) {
        setPixmapProperty(getHandle(), "background-stipple", stipple
                .getHandle());
    }

    /**
     * Text direction, for example right-to-left or left-to-right.
     * 
     * @param direction
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDirection(TextDirection direction) {
        setIntProperty(getHandle(), "direction", direction.getValue());
    }

    /**
     * Whether the text can be modified by the user.
     * 
     * @param editable
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEditable(boolean editable) {
        setBooleanProperty(getHandle(), "editable", editable);
    }

    /**
     * Name of the font family, e.g. Sans, Helvetica, Times, Monospace.
     * 
     * @param family
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFamily(String family) {
        setStringProperty(getHandle(), "family", family);
    }

    /**
     * Font description as a string, e.g. "Sans Italic 12".
     * 
     * @param font
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFont(String font) {
        setStringProperty(getHandle(), "font", font);
    }

    /**
     * Font description as a FontDescription.
     * 
     * @param fontDesc
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFontDescription(FontDescription fontDesc) {
        setFontDesc(getHandle(), fontDesc.getHandle());
    }

    /**
     * Foreground color as a string
     * 
     * @param foreground
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setForeground(String foreground) {
        setStringProperty(getHandle(), "foreground", foreground);
    }

    /**
     * Bitmap to use as a mask when drawing the text foreground.
     * 
     * @param stipple
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setForegroundStipple(Pixmap stipple) {
        setPixmapProperty(getHandle(), "foreground-stipple", stipple
                .getHandle());
    }

    /**
     * Amount to indent the paragraph, in pixels.
     * 
     * @param indent
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIndent(int indent) {
        setIntProperty(getHandle(), "indent", indent);
    }

    /**
     * Left, right, or center justification.
     * 
     * @param justification
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setJustification(Justification justification) {
        setIntProperty(getHandle(), "justification", justification.getValue());
    }

    /**
     * The language this text is in, as an ISO code. Pango can use this as a
     * hint when rendering the text. If not set, an appropriate default will be
     * used.
     * 
     * @param language
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLanguage(String language) {
        setStringProperty(getHandle(), "language", language);
    }

    /**
     * Width of the left margin in pixels.
     * 
     * @param margin
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLeftMargin(int margin) {
        setIntProperty(getHandle(), "left-margin", margin);
    }

    /**
     * Pixels of blank space above paragraphs.
     * 
     * @param setting
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixelsAboveLines(int setting) {
        setIntProperty(getHandle(), "pixels_above_lines", setting);
    }

    /**
     * Pixels of blank space below paragraphs.
     * 
     * @param setting
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixelsBelowLines(int setting) {
        setIntProperty(getHandle(), "pixels_below_lines", setting);
    }

    /**
     * Pixels of blank space between wrapped lines in a paragraph.
     * 
     * @param setting
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixelsInsideWrap(int setting) {
        setIntProperty(getHandle(), "pixels_inside_wrap", setting);
    }

    /**
     * Width of the right margin in pixels.
     * 
     * @param margin
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRightMargin(int margin) {
        setIntProperty(getHandle(), "right-margin", margin);
    }

    /**
     * Offset of text above the baseline (below the baseline if rise is
     * negative) in pixels.
     * 
     * @param rise
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRise(int rise) {
        setIntProperty(getHandle(), "rise", rise);
    }

    /**
     * Font size as a scale factor relative to the default font size. This
     * properly adapts to theme changes etc. so is recommended.
     * 
     * @param scale
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setScale(Scale scale) {
        setDoubleProperty(getHandle(), "scale", scale.getValue());
    }

    /**
     * Font size in Pango units.
     * 
     * @param size
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(int size) {
        setIntProperty(getHandle(), "size", size * 1024);
    }

    /**
     * Font size in points.
     * 
     * @param points
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSizePoints(double points) {
        setDoubleProperty(getHandle(), "size-points", points);
    }

    /**
     * Font stretch as a Stretch.
     * 
     * @param stretch
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStretch(Stretch stretch) {
        setIntProperty(getHandle(), "stretch", stretch.getValue());
    }

    /**
     * Whether to strike through the text.
     * 
     * @param strikethrough
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStrikethrough(boolean strikethrough) {
        setBooleanProperty(getHandle(), "strikethrough", strikethrough);
    }

    /**
     * Font Style
     * 
     * @param style
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStyle(Style style) {
        setIntProperty(getHandle(), "style", style.getValue());
    }

    /**
     * Custom tabs for this text.
     * 
     * @param tabs
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTabs(TabArray tabs) {
        setTabs(getHandle(), tabs.getHandle());
    }

    /**
     * Style of underline for this text.
     * 
     * @param underline
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUnderline(Underline underline) {
        setIntProperty(getHandle(), "underline", underline.getValue());
    }

    /**
     * Font variant as a Variant.
     * 
     * @param variant
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVariant(Variant variant) {
        setIntProperty(getHandle(), "variant", variant.getValue());
    }

    /**
     * Font weight as an integer, see predefined values in Weight
     * 
     * @param weight
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWeight(Weight weight) {
        setIntProperty(getHandle(), "weight", weight.getValue());
    }

    /**
     * Whether to wrap lines never, at word boundaries, or at character
     * boundaries.
     * 
     * @param mode
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWrapMode(WrapMode mode) {
        setIntProperty(getHandle(), "wrap_mode", mode.getValue());
    }

    // "background" gchararray : Write
    // "background-full-height" gboolean : Read / Write
    // "background-full-height-set" gboolean : Read / Write
    // "background-gdk" GdkColor : Read / Write
    // "background-set" gboolean : Read / Write
    // "background-stipple" GdkPixmap : Read / Write
    // "background-stipple-set" gboolean : Read / Write
    // "direction" GtkTextDirection : Read / Write
    // "editable" gboolean : Read / Write
    // "editable-set" gboolean : Read / Write
    // "family" gchararray : Read / Write
    // "family-set" gboolean : Read / Write
    // "font" gchararray : Read / Write
    // "font-desc" PangoFontDescription : Read / Write
    // "foreground" gchararray : Write
    // "foreground-gdk" GdkColor : Read / Write
    // "foreground-set" gboolean : Read / Write
    // "foreground-stipple" GdkPixmap : Read / Write
    // "foreground-stipple-set" gboolean : Read / Write
    // "indent" gint : Read / Write
    // "indent-set" gboolean : Read / Write
    // "invisible" gboolean : Read / Write
    // "invisible-set" gboolean : Read / Write
    // "justification" GtkJustification : Read / Write
    // "justification-set" gboolean : Read / Write
    // "language" gchararray : Read / Write
    // "language-set" gboolean : Read / Write
    // "left-margin" gint : Read / Write
    // "left-margin-set" gboolean : Read / Write
    // "name" gchararray : Read / Write / Construct Only
    // "pixels-above-lines" gint : Read / Write
    // "pixels-above-lines-set" gboolean : Read / Write
    // "pixels-below-lines" gint : Read / Write
    // "pixels-below-lines-set" gboolean : Read / Write
    // "pixels-inside-wrap" gint : Read / Write
    // "pixels-inside-wrap-set" gboolean : Read / Write
    // "right-margin" gint : Read / Write
    // "right-margin-set" gboolean : Read / Write
    // "rise" gint : Read / Write
    // "rise-set" gboolean : Read / Write
    // "scale" gdouble : Read / Write
    // "scale-set" gboolean : Read / Write
    // "size" gint : Read / Write
    // "size-points" gdouble : Read / Write
    // "size-set" gboolean : Read / Write
    // "stretch" PangoStretch : Read / Write
    // "stretch-set" gboolean : Read / Write
    // "strikethrough" gboolean : Read / Write
    // "strikethrough-set" gboolean : Read / Write
    // "style" PangoStyle : Read / Write
    // "style-set" gboolean : Read / Write
    // "tabs" PangoTabArray : Read / Write
    // "tabs-set" gboolean : Read / Write
    // "underline" PangoUnderline : Read / Write
    // "underline-set" gboolean : Read / Write
    // "variant" PangoVariant : Read / Write
    // "variant-set" gboolean : Read / Write
    // "weight" gint : Read / Write
    // "weight-set" gboolean : Read / Write
    // "wrap-mode" GtkWrapMode : Read / Write
    // "wrap-mode-set" gboolean : Read / Write

    native static final protected String getName(Handle cptr);

    // native static final protected boolean getBgColorSet (Handle cptr);
    // native static final protected boolean getBgStippleSet (Handle cptr);
    // native static final protected boolean getFgColorSet (Handle cptr);
    // native static final protected boolean getScaleSet (Handle cptr);
    // native static final protected boolean getFgStippleSet (Handle cptr);
    // native static final protected boolean getJustificationSet (Handle cptr);
    // native static final protected boolean getLeftMarginSet (Handle cptr);
    // native static final protected boolean getIndentSet (Handle cptr);
    // native static final protected boolean getRiseSet (Handle cptr);
    // native static final protected boolean getStrikethroughSet (Handle cptr);
    // native static final protected boolean getRightMarginSet (Handle cptr);
    // native static final protected boolean getPixelsAboveLinesSet (Handle
    // cptr);
    // native static final protected boolean getPixelsBelowLinesSet (Handle
    // cptr);
    // native static final protected boolean getPixelsInsideWrapSet (Handle
    // cptr);
    // native static final protected boolean getTabsSet (Handle cptr);
    // native static final protected boolean getUnderlineSet (Handle cptr);
    // native static final protected boolean getWrapModeSet (Handle cptr);
    // native static final protected boolean getBgFullHeightSet (Handle cptr);
    // native static final protected boolean getInvisibleSet (Handle cptr);
    // native static final protected boolean getEditableSet (Handle cptr);
    // native static final protected boolean getLanguageSet (Handle cptr);
    // native static final protected boolean getPad1 (Handle cptr);
    // native static final protected boolean getPad2 (Handle cptr);
    // native static final protected boolean getPad3 (Handle cptr);
    native static final protected int gtk_text_tag_get_type();

    native static final protected Handle gtk_text_tag_new(String name);

    native static final protected int gtk_text_tag_get_priority(Handle tag);

    native static final protected void gtk_text_tag_set_priority(Handle tag,
            int priority);

    // native static final protected boolean gtk_text_tag_event (Handle tag, int
    // eventObject, int event,
    // Handle iter);
    native static final protected void setStringProperty(Handle handle,
            String property, String setting);

    native static final protected void setBooleanProperty(Handle handle,
            String property, boolean setting);

    native static final protected void setIntProperty(Handle handle,
            String property, int setting);

    native static final protected void setDoubleProperty(Handle handle,
            String property, double setting);

    // TODO: implement this method
    native static final protected void setPixmapProperty(Handle handle,
            String property, Handle pixmap);

    native static final protected void setTabs(Handle handle, Handle tabArray);

    native static final protected void setFontDesc(Handle handle,
            Handle fontDesc);
}
