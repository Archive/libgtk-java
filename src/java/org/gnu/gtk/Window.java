/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Geometry;
import org.gnu.gdk.Gravity;
import org.gnu.gdk.Pixbuf;
import org.gnu.gdk.Point;
import org.gnu.gdk.Screen;
import org.gnu.gdk.WindowHints;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;

/**
 * A Window is a window that can be displayed on the screen. It also is a
 * container that can hold a single child widget. It is common to have the
 * window in Window supply the margin around the window of the contained widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Window</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Window extends Bin {

    private boolean destroyed = false;

    /**
     * Constructs a top-level Window. This is the same as calling
     * <code>new Window(WindowType.TOPLEVEL)</code>
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window() {
        this(WindowType.TOPLEVEL);
    }

    /**
     * Construct a new Window object.
     * 
     * @param windowType
     *            The type of window to create
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window(WindowType windowType) {
        super(Window.gtk_window_new(windowType.getValue()));
    }

    /**
     * Create a new Window with a handle to a native resource returned from a
     * call to the native libraries.
     * 
     * @param handle
     *            The handle that represents a pointer to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window(Handle handle) {
        super(handle);
    }

    /**
     * Create a new Window with a handle to a native resource returned from a
     * call to the native libraries.
     * 
     * @param handle
     *            The handle that represents a pointer to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Window getWindow(Handle handle) {
        if (handle == null)
            return null;

        Window obj = (Window) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Window(handle);

        return obj;
    }

    /**
     * Set the title for this Window object
     * 
     * @param title
     *            A string containing the title for this Window
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        checkState();
        if (title != null) {
            Window.gtk_window_set_title(getHandle(), title);
        } else {
            Window.gtk_window_set_title(getHandle(), "");
        }
    }

    /**
     * Returns the title for the Window object
     * 
     * @return A string containing the title.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        checkState();
        return Window.gtk_window_get_title(getHandle());
    }

    /**
     * Sets whether the user can resize the window. Windows are user resizable
     * by default.
     * 
     * @param resizable
     *            Determines if the window can be resized.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setResizable(boolean resizable) {
        checkState();
        Window.gtk_window_set_resizable(getHandle(), resizable);
    }

    /**
     * Associate <code>accesGroup</code> with the window.
     * 
     * @param accelGroup
     *            The AccelGroup to associate with this window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addAccelGroup(AccelGroup accelGroup) {
        checkState();
        Window.gtk_window_add_accel_group(getHandle(), accelGroup.getHandle());
    }

    /**
     * Reverses the effect of <code>addAccelGroup</code>.
     * 
     * @param accelGroup
     *            The AccelGroup to disassociate with this window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeAccelGroup(AccelGroup accelGroup) {
        checkState();
        Window.gtk_window_remove_accel_group(getHandle(), accelGroup
                .getHandle());
    }

    /**
     * Sets a window modal or non-modal.
     * 
     * @param modal
     *            Indicates whether the window should be modal or not.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setModal(boolean modal) {
        checkState();
        Window.gtk_window_set_modal(getHandle(), modal);
    }

    /**
     * Sets the default size for a Window. If a Window's natural size is larger
     * than the default, the default will be ignored.
     * 
     * @param width
     *            The width to set for the default size.
     * @param height
     *            the height to set for the default size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDefaultSize(int width, int height) {
        checkState();
        Window.gtk_window_set_default_size(getHandle(), width, height);
    }

    /**
     * This method sets up hints about how a Window can be resized by the user.
     * You can set a minimum and maximum size, allowable resize increments,
     * aspect ratios and more.
     * 
     * @param geometryWidget
     *            The Widget the geometry hints will be applied to.
     * @param geometry
     *            The geometry information.
     * @param geomMask
     *            Mask indicating which fields should be paid attention to.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setGeometryHints(Widget geometryWidget, Geometry geometry,
            WindowHints geomMask) {
        checkState();
        Window.gtk_window_set_geometry_hints(getHandle(), geometryWidget
                .getHandle(), geometry.getHandle(), geomMask.getValue());
    }

    /**
     * Window gravity defines the meaning of coordinates passed to
     * <code>move()</code>.
     * 
     * @param gravity
     *            The window gravity.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setGravity(Gravity gravity) {
        checkState();
        Window.gtk_window_set_gravity(getHandle(), gravity.getValue());
    }

    /**
     * Gets the value set by <code>setGravity()</code>.
     * 
     * @return The Gravity for the Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Gravity getGravity() {
        checkState();
        int val = Window.gtk_window_get_gravity(getHandle());
        return Gravity.intern(val);
    }

    /**
     * Sets a position constraint for this window. If the old or new constraint
     * is {@link WindowPosition#CENTER_ALWAYS}, this will also cause the window
     * to be repositioned to satisfy the new constraint.
     * 
     * @param position
     *            A position constraint.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPosition(WindowPosition position) {
        checkState();
        Window.gtk_window_set_position(getHandle(), position.getValue());
    }

    /**
     * Retrieves the current focused Widget within the window.
     * 
     * @return The Widget that has focus.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getFocusWidget() {
        checkState();
        Handle hndl = gtk_window_get_focus(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * If <code>focus</code> is not the current focus widget and is focusable,
     * set it as the focus widget for the window.
     * 
     * @param focus
     *            The widget to receive focus for the Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFocusWidget(Widget focus) {
        checkState();
        Window.gtk_window_set_focus(getHandle(), focus.getHandle());
    }

    /**
     * The default widget is the widget that is activated when the user presses
     * the Enter key. This method will set defaultWidget to the default widget
     * for the Window.
     * 
     * @param defaultWidget
     *            The widget that should become the default widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDefaultWidget(Widget defaultWidget) {
        checkState();
        Window.gtk_window_set_default(getHandle(), defaultWidget.getHandle());
    }

    /**
     * Presents a window to the user. This may mean raising the window in the
     * stack order, deiconifying it, moving it to the current desktop, and/or
     * giving it the keyboard focus, possibly dependent on the user's platform,
     * window manager, and preferences. If the Window is hidden it will also
     * call <code>
     * show</code> as well.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void present() {
        checkState();
        Window.gtk_window_present(getHandle());
    }

    /**
     * Asks to iconify the Window. Note that you shouldn't assume the Window is
     * iconified afterward because other entities could deiconify it again or
     * there may not be a window manager in which case iconification is not
     * possible.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void iconify() {
        checkState();
        Window.gtk_window_iconify(getHandle());
    }

    /**
     * Asks to deiconify the specified Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deiconify() {
        checkState();
        Window.gtk_window_deiconify(getHandle());
    }

    /**
     * Asks to stick the window. This means that the window appear on all user
     * desktops. Note that you shouldn't assume that the Window is definitely
     * stuck after calling this method. Other entities could unstick the Window
     * or the window manager may not support this feature.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void stick() {
        checkState();
        Window.gtk_window_stick(getHandle());
    }

    /**
     * Asks to unstick the window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unstick() {
        checkState();
        Window.gtk_window_unstick(getHandle());
    }

    /**
     * Asks to maximize the Window so it becomes full-screen.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void maximize() {
        checkState();
        Window.gtk_window_maximize(getHandle());
    }

    /**
     * Asks to unmaximize the Window so it becomes it normal size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unmaximize() {
        checkState();
        Window.gtk_window_unmaximize(getHandle());
    }

    /**
     * By default Windows are decorated by a titlebar, resize controls, etc.
     * Some window managers allow you to disable these decorations, creating a
     * borderless Window. This method allows you to change the decorated setting
     * for the Window.
     * 
     * @param setting
     *            Determines if the Window should be decorated.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDecorated(boolean setting) {
        checkState();
        Window.gtk_window_set_decorated(getHandle(), setting);
    }

    /**
     * Returns the current size of a Window. If the window is not onscreen, it
     * returns the size that will be suggested to the window manager for the
     * initial window size.
     * 
     * @return The size of the Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Requisition getSize() {
        checkState();
        int[] width = new int[1];
        int[] height = new int[1];
        Window.gtk_window_get_size(getHandle(), width, height);
        return new Requisition(width[0], height[0]);
    }

    /**
     * Retuns the current position of the window.
     * 
     * @return The position of the window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Point getPosition() {
        checkState();
        int[] x = new int[1];
        int[] y = new int[1];
        Window.gtk_window_get_position(getHandle(), x, y);
        return new Point(x[0], y[0]);
    }

    /**
     * Ask the window manager to move the Window to a given location. Window
     * managers are free to ignore this request. Most window managers ignore the
     * request for the initial window position but honor the request after the
     * window has been shown.
     * 
     * @param x
     *            The x coordinate for the move.
     * @param y
     *            The y coordinate for the move.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void move(int x, int y) {
        checkState();
        Window.gtk_window_move(getHandle(), x, y);
    }

    /**
     * Resizes the Window as if the user had done so, obeying the geometry
     * constraints.
     * 
     * @param width
     *            The width for the resized Window.
     * @param height
     *            The height for the resized Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void resize(int width, int height) {
        checkState();
        Window.gtk_window_resize(getHandle(), width, height);
    }

    /**
     * Sets up the icon representing a Window. The icon is used when the Window
     * is minimized.
     * 
     * @param icon
     *            The Icon to use for this Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIcon(Pixbuf icon) {
        checkState();
        Window.gtk_window_set_icon(getHandle(), icon.getHandle());
    }

    /**
     * Returns the icon representing the Window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getIcon() {
        checkState();
        Handle hndl = gtk_window_get_icon(getHandle());
        return Pixbuf.getPixbufFromHandle(hndl);

    }

    /**
     * Sets up the icon representing a GtkWindow. The icon is used when the
     * window is minimized (also known as iconified). Some window managers or
     * desktop environments may also place it in the window frame, or display it
     * in other contexts.
     * 
     * setIconList allows you to pass in the same icon in several hand-drawn
     * sizes. The list should contain the natural sizes your icon is available
     * in; that is, don't scale the image before passing it. Scaling is
     * postponed until the last minute, when the desired final size is known, to
     * allow best quality.
     * 
     * By passing several sizes, you may improve the final image quality of the
     * icon, by reducing or eliminating automatic image scaling.
     * 
     * Recommended sizes to provide: 16x16, 32x32, 48x48 at minimum, and larger
     * images (64x64, 128x128) if you have them.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconList(Pixbuf[] icons) {
        checkState();
        Handle[] hndls = new Handle[icons.length];
        for (int i = 0; i < icons.length; i++)
            hndls[i] = icons[i].getHandle();
        gtk_window_set_icon_list(getHandle(), hndls);
    }

    /**
     * Retrieves the list of icons set by setIconList(). The list is copied, but
     * the reference count on each member won't be incremented.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf[] getIconList() {
        checkState();
        Handle[] hndls = gtk_window_get_icon_list(getHandle());
        if (hndls == null)
            return null;
        Pixbuf[] pix = new Pixbuf[hndls.length];
        for (int i = 0; i < hndls.length; i++)
            pix[i] = Pixbuf.getPixbufFromHandle(hndls[i]);
        return pix;
    }

    /**
     * Set to true to keep this window from appearing in the task bar.
     * 
     * @param skipHint
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSkipTaskbarHint(boolean skipHint) {
        checkState();
        gtk_window_set_skip_taskbar_hint(getHandle(), skipHint);
    }

    /**
     * Returns whether this window should appear in the task bar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getSkipTaskbarHint() {
        checkState();
        return gtk_window_get_skip_taskbar_hint(getHandle());
    }

    /**
     * Set to true to keep the window from appearing in the pager.
     * 
     * @param skipHint
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSkipPagerHint(boolean skipHint) {
        checkState();
        gtk_window_set_skip_pager_hint(getHandle(), skipHint);
    }

    /**
     * Returns whether the window should appear in the pager.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getSkipPagerHint() {
        checkState();
        return gtk_window_get_skip_pager_hint(getHandle());
    }

    /**
     * Windows may set a hint asking the desktop environment not to receive the
     * input focus. This won't necessarily be honoured, but if you're building a
     * quick utility window then you might want to set this property.
     * 
     * @param acceptFocus
     *            true to set this hint.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAcceptFocus(boolean acceptFocus) {
        checkState();
        gtk_window_set_accept_focus(getHandle(), acceptFocus);
    }

    public boolean getAcceptFocus() {
        checkState();
        return gtk_window_get_accept_focus(getHandle());
    }

    /**
     * Sets the Screen where the window is displayed; if the window is already
     * mapped, it will be unmapped, and then remapped on the new screen.
     * 
     * @param screen
     *            the new screen.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public void setScreen(Screen screen) {
        checkState();
        gtk_window_set_screen(getHandle(), screen.getHandle());
    }

    /**
     * Returns the GdkScreen associated with this window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public Screen getScreen() {
        checkState();
        Handle hndl = gtk_window_get_screen(getHandle());
        return Screen.getScreenFromHandle(hndl);
    }

    /**
     * Returns whether the window is part of the current active toplevel. (That
     * is, the toplevel window receiving keystrokes.) The return value is TRUE
     * if the window is active toplevel itself, but also if it is, say, a
     * {@link Plug} embedded in the active toplevel. You might use this method
     * if you wanted to draw a widget differently in an active window from a
     * widget in an inactive window.
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public boolean isActive() {
        checkState();
        return gtk_window_is_active(getHandle());
    }

    /**
     * Returns whether the input focus is within this Window. For real toplevel
     * windows, this is identical to <code>isActive()</code>, but for
     * embedded windows, like {@link Plug}, the results will differ.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public boolean hasToplevelFocus() {
        checkState();
        return gtk_window_has_toplevel_focus(getHandle());
    }

    /**
     * Sets the icon for this window.
     * 
     * @param filename
     *            the full path to the icon
     * @return <code>true</code> if the icon was successfully set,
     *         <code>false</code> otherwise.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public boolean setIconFromFile(String filename) {
        checkState();
        return gtk_window_set_icon_from_file(getHandle(), filename);
    }

    /**
     * Sets an icon to be used as fallback for windows that haven't had
     * setIconList() called on them from a file on disk.
     * 
     * @see #setIconName(String)
     * 
     * @param filename
     *            the path to the icon
     * @return <code>true</code> if it was successfully set,
     *         <code>false</code> otherwise.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public static boolean setDefaultIconFromFile(String filename) {
        return gtk_window_set_default_icon_from_file(filename);
    }

    public static void setDefaultIcon(Pixbuf icon) {
        gtk_window_set_default_icon(icon.getHandle());
    }

    public static void setAutoStartupNotification(boolean setting) {
        gtk_window_set_auto_startup_notification(setting);
    }

    public void fullscreen() {
        checkState();
        gtk_window_fullscreen(getHandle());
    }

    public void unfullscreen() {
        checkState();
        gtk_window_unfullscreen(getHandle());
    }

    public void setKeepAbove(boolean setting) {
        checkState();
        gtk_window_set_keep_above(getHandle(), setting);
    }

    public void setKeepBelow(boolean setting) {
        checkState();
        gtk_window_set_keep_below(getHandle(), setting);
    }

    /**
     * Sets up the icon representing a Window. The icon is used when the window
     * is minimized (also known as iconified). Some window managers or desktop
     * environments may also place it in the window frame, or display it in
     * other contexts.
     * 
     * This method allows you to pass in the same icon in several hand-drawn
     * sizes. The list should contain the natural sizes your icon is available
     * in; that is, don't scale the image before passing it to GTK+. Scaling is
     * postponed until the last minute, when the desired final size is known, to
     * allow best quality.
     * 
     * By passing several sizes, you may improve the final image quality of the
     * icon, by reducing or eliminating automatic image scaling.
     * 
     * Recommended sizes to provide: 16x16, 32x32, 48x48 at minimum, and larger
     * images (64x64, 128x128) if you have them.
     * 
     * Note that this method sets the icon for all windows in your application
     * in one go.
     * 
     * @param icons
     *            the list of icons to use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public static void setDefaultIconList(Pixbuf[] icons) {
        if (null == icons)
            return;
        Handle[] hndls = new Handle[icons.length];
        for (int i = 0; i < icons.length; i++) {
            hndls[i] = icons[i].getHandle();
        }
        gtk_window_set_default_icon_list(hndls);
    }

    /**
     * Gets the icons set by <code>setDefaultIconList</code>.
     * 
     * @return the list of icons
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public static Pixbuf[] getDefaultIconList() {
        Handle[] hndls = gtk_window_get_default_icon_list();
        Pixbuf[] pbs = new Pixbuf[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            pbs[i] = Pixbuf.getPixbufFromHandle(hndls[i]);
        }
        return pbs;
    }

    /**
     * Returns a list of all existing toplevel windows.
     * 
     * @return the array containing the windows
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    public static Window[] listToplevelWindows() {
        Handle[] hndls = gtk_window_list_toplevels();
        Window[] wins = new Window[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            wins[i] = Window.getWindow(hndls[i]);
        }
        return wins;
    }

    /**
     * Windows may set a hint asking the desktop environment not to receive the
     * input focus when the window is mapped. This function sets this hint.
     * 
     * @param setting
     *            TRUE to let this window receive input focus on map.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFocusOnMap(boolean setting) {
        checkState();
        gtk_window_set_focus_on_map(getHandle(), setting);
    }

    /**
     * Gets the value set by {@link #setFocusOnMap}.
     * 
     * @return TRUE if window should receive the input focus when mapped.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getFocusOnMap() {
        checkState();
        return gtk_window_get_focus_on_map(getHandle());
    }

    /**
     * Sets the icon for the window from a named themed icon. See the docs for
     * {@link IconTheme} for more details.
     * <p>
     * Note that this has nothing to do with the WM_ICON_NAME property which is
     * mentioned in the ICCCM.
     * 
     * @param name
     *            The name of the themed icon.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconName(String name) {
        checkState();
        gtk_window_set_icon_name(getHandle(), name);
    }

    /**
     * Returns the name of the themed icon for the window.
     * 
     * @return The icon name or NULL if the window has no themed icon.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getIconName() {
        checkState();
        return gtk_window_get_icon_name(getHandle());
    }

    /**
     * Sets an icon to be used as fallback for windows that haven't had
     * <code>setIconList</code> called on them from a named themed icon.
     * 
     * @see #setIconName(String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDefaultIconName(String name) {
        checkState();
        gtk_window_set_default_icon_name(name);
    }

    /**
     * Fetches the transient parent of this window.
     * 
     * @return The transient parent for this window, or null if no transient
     *         parent has been set.
     * @see #setTransientParent(Window)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getTransientParent() {
        checkState();
        Handle handle = gtk_window_get_transient_for(getHandle());
        return Window.getWindow(handle);
    }

    /**
     * Dialog windows should be set transient for the main application window
     * they were spawned from. This allows window managers to, for example, keep
     * the dialog on top of the main window, or center the dialog over the main
     * window. On Windows, this method will put the child window on top of the
     * parent, the same way the window manager would have done in X.
     * 
     * @param parent
     *            Transient parent window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTransientParent(Window parent) {
        checkState();
        Handle parentHandle;
        if (parent == null) {
            parentHandle = getNullHandle();
        } else {
            parentHandle = parent.getHandle();
        }
        gtk_window_set_transient_for(getHandle(), parentHandle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_window_get_type());
    }

    /**
     * Destroys a window and releases its native resources. When a window is
     * destroyed, it will break any references it holds to other objects and it
     * will be removed from the list of toplevels. It is safe to call this
     * method multiple times (the native resources will only be released once).
     * However, no other methods on a window should be called after destroy is
     * called. An ObjectDestroyedException is thrown if this takes place.
     * 
     * @see ObjectDestroyedException
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void destroy() {
        if (isDestroyed()) {
            return;
        }
        gtk_window_destroy(getHandle());
        destroyed = true;
    }

    /**
     * Checks if the object has been destroyed and throws
     * ObjectDestroyedException if this is the case.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected void checkState() {
        if (isDestroyed()) {
            throw new ObjectDestroyedException();
        }
    }

    /**
     * Returns true if destroy has previously been called on this object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isDestroyed() {
        return destroyed;
    }

    // Override finalize as we don't want automatic collection to take place
    // here
    protected void finalize() {
    }

    native static final protected int gtk_window_get_type();

    native static final protected Handle gtk_window_new(int type);

    native static final protected void gtk_window_set_title(Handle window,
            String title);

    native static final protected String gtk_window_get_title(Handle window);

    native static final protected void gtk_window_set_wmclass(Handle window,
            String wmclassName, String wmclassClass);

    native static final protected void gtk_window_set_role(Handle window,
            String role);

    native static final protected String gtk_window_get_role(Handle window);

    native static final protected void gtk_window_add_accel_group(
            Handle window, Handle accelGroup);

    native static final protected void gtk_window_remove_accel_group(
            Handle window, Handle accelGroup);

    native static final protected void gtk_window_set_position(Handle window,
            int position);

    native static final protected int gtk_window_activate_focus(Handle window);

    native static final protected void gtk_window_set_focus(Handle window,
            Handle focus);

    native static final protected Handle gtk_window_get_focus(Handle window);

    native static final protected void gtk_window_set_default(Handle window,
            Handle defaultWidget);

    native static final protected boolean gtk_window_activate_default(
            Handle window);

    native static final protected void gtk_window_set_transient_for(
            Handle window, Handle parent);

    native static final protected Handle gtk_window_get_transient_for(
            Handle window);

    native static final protected void gtk_window_set_type_hint(Handle window,
            int hint);

    native static final protected int gtk_window_get_type_hint(Handle window);

    native static final protected void gtk_window_set_destroy_with_parent(
            Handle window, boolean setting);

    native static final protected boolean gtk_window_get_destroy_with_parent(
            Handle window);

    native static final protected void gtk_window_set_resizable(Handle window,
            boolean resizable);

    native static final protected boolean gtk_window_get_resizable(Handle window);

    native static final protected void gtk_window_set_gravity(Handle window,
            int gravity);

    native static final protected int gtk_window_get_gravity(Handle window);

    native static final protected void gtk_window_set_geometry_hints(
            Handle window, Handle geometryWidget, Handle geometry, int geomMask);

    native static final protected void gtk_window_set_has_frame(Handle window,
            boolean setting);

    native static final protected boolean gtk_window_get_has_frame(Handle window);

    native static final protected void gtk_window_set_frame_dimensions(
            Handle window, int left, int top, int right, int bottom);

    native static final protected void gtk_window_get_frame_dimensions(
            Handle window, int[] left, int[] top, int[] right, int[] bottom);

    native static final protected void gtk_window_set_decorated(Handle window,
            boolean setting);

    native static final protected boolean gtk_window_get_decorated(Handle window);

    native static final protected void gtk_window_set_icon_list(Handle window,
            Handle[] list);

    native static final protected Handle[] gtk_window_get_icon_list(
            Handle window);

    native static final protected void gtk_window_set_icon(Handle window,
            Handle icon);

    native static final protected Handle gtk_window_get_icon(Handle window);

    native static final protected void gtk_window_set_default_icon_list(
            Handle[] list);

    native static final protected Handle[] gtk_window_get_default_icon_list();

    native static final protected void gtk_window_set_modal(Handle window,
            boolean modal);

    native static final protected boolean gtk_window_get_modal(Handle window);

    native static final protected Handle[] gtk_window_list_toplevels();

    native static final protected void gtk_window_add_mnemonic(Handle window,
            int keyval, Handle target);

    native static final protected void gtk_window_remove_mnemonic(
            Handle window, int keyval, Handle target);

    native static final protected boolean gtk_window_mnemonic_activate(
            Handle window, int keyval, int modifier);

    native static final protected void gtk_window_set_mnemonic_modifier(
            Handle window, int modifier);

    native static final protected int gtk_window_get_mnemonic_modifier(
            Handle window);

    native static final protected void gtk_window_present(Handle window);

    native static final protected void gtk_window_iconify(Handle window);

    native static final protected void gtk_window_deiconify(Handle window);

    native static final protected void gtk_window_stick(Handle window);

    native static final protected void gtk_window_unstick(Handle window);

    native static final protected void gtk_window_maximize(Handle window);

    native static final protected void gtk_window_unmaximize(Handle window);

    native static final protected void gtk_window_begin_resize_drag(
            Handle window, int edge, int button, int rootX, int rootY,
            int timestamp);

    native static final protected void gtk_window_begin_move_drag(
            Handle window, int button, int rootX, int rootY, int timestamp);

    native static final protected void gtk_window_set_default_size(
            Handle window, int width, int height);

    native static final protected void gtk_window_get_default_size(
            Handle window, int[] width, int[] height);

    native static final protected void gtk_window_resize(Handle window,
            int width, int height);

    native static final protected void gtk_window_get_size(Handle window,
            int[] width, int[] height);

    native static final protected void gtk_window_move(Handle window, int x,
            int y);

    native static final protected void gtk_window_get_position(Handle window,
            int[] rootX, int[] rootY);

    native static final protected boolean gtk_window_parse_geometry(
            Handle window, String geometry);

    native static final protected void gtk_window_set_skip_taskbar_hint(
            Handle window, boolean skip);

    native static final protected boolean gtk_window_get_skip_taskbar_hint(
            Handle window);

    native static final protected void gtk_window_set_skip_pager_hint(
            Handle window, boolean skip);

    native static final protected boolean gtk_window_get_skip_pager_hint(
            Handle window);

    native static final protected void gtk_window_set_accept_focus(
            Handle window, boolean setting);

    native static final protected boolean gtk_window_get_accept_focus(
            Handle window);

    native static final protected void gtk_window_set_screen(Handle window,
            Handle screen);

    native static final protected Handle gtk_window_get_screen(Handle window);

    native static final protected boolean gtk_window_is_active(Handle window);

    native static final protected boolean gtk_window_has_toplevel_focus(
            Handle window);

    native static final protected boolean gtk_window_set_icon_from_file(
            Handle window, String filename);

    native static final protected boolean gtk_window_set_default_icon_from_file(
            String filename);

    native static final protected void gtk_window_set_default_icon(Handle icon);

    native static final protected void gtk_window_set_auto_startup_notification(
            boolean setting);

    native static final protected void gtk_window_fullscreen(Handle window);

    native static final protected void gtk_window_unfullscreen(Handle window);

    native static final protected void gtk_window_set_keep_above(Handle window,
            boolean setting);

    native static final protected void gtk_window_set_keep_below(Handle window,
            boolean setting);

    // GTK 2.6 additions.
    native static final private void gtk_window_set_focus_on_map(Handle window,
            boolean setting);

    native static final private boolean gtk_window_get_focus_on_map(
            Handle window);

    native static final private void gtk_window_set_icon_name(Handle window,
            String name);

    native static final private String gtk_window_get_icon_name(Handle window);

    native static final private void gtk_window_set_default_icon_name(
            String name);

    native static final private void gtk_window_destroy(Handle window);
}
