/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is the base class of all widgets and a few non-widget objects.
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.GtkObject</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class GtkObject extends GObject {

    protected GtkObject(Handle handle) {
        super(handle);
    }

    /**
     * This method is now a no-op. Native resources for a specific java object
     * are automatically reclaimed during the finalization of that object.
     * <p>
     * 
     * Note: In some rare cases, subclasses may need an explicit call to destroy
     * in order to release their native resources. These classes may override
     * this method to provide this functionality.
     * <p>
     * 
     * Currently, the only example of this is {@link Window}. See its
     * documentation for more details.
     * 
     * @see Window#destroy()
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void destroy() {
        // gtk_object_destroy(getHandle());
    }

    /**
     * Removes the floating reference from a GtkObject, if it exists. Otherwise
     * does nothing.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void sink() {
        checkState();
        gtk_object_sink(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_object_get_type());
    }

    /**
     * Checks the state of the object and may throw an exception. Subclasses are
     * responsible for overriding this and performing additional checks if
     * required.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected void checkState() {
        // At the moment, no checking takes place here
    }

    native static final protected int getFlags(Handle obj);

    native static final protected void setFlags(Handle obj, int flags);

    native static final protected int gtk_object_get_type();

    native static final protected void gtk_object_sink(Handle object);

    native static final protected void gtk_object_destroy(Handle object);

    /*
     * Deprecated functions. native static final private Handle
     * gtk_object_new(int type, String first_property_name); native static final
     * private Handle gtk_object_ref(Handle object); native static final private
     * void gtk_object_unref(Handle object); native static final private void
     * gtk_object_weakref(Handle object, int notify, gpointer data); native
     * static final private void gtk_object_weakunref(Handle object, int notify,
     * gpointer data); native static final private void
     * gtk_object_set_data(Handle object, String key, gpointer data); native
     * static final private void gtk_object_set_data_full(Handle object, String
     * key, gpointer data, int destroy); native static final private void
     * gtk_object_remove_data(Handle object, String key); native static final
     * private gpointer gtk_object_get_data(Handle object, String key); native
     * static final private void gtk_object_remove_no_notify(Handle object,
     * String key); native static final private void
     * gtk_object_set_user_data(Handle object, gpointer data); native static
     * final private gpointer gtk_object_get_user_data(Handle object); native
     * static final private void gtk_object_set_data_by_id(Handle object, GQuark
     * data_id, gpointer data); native static final private void
     * gtk_object_set_data_by_id_full(Handle object, GQuark data_id, gpointer
     * data, int destroy); native static final private gpointer
     * gtk_object_get_data_by_id(Handle object, GQuark data_id); native static
     * final private void gtk_object_remove_data_by_id(Handle object, GQuark
     * data_id); native static final private void
     * gtk_object_remove_no_notify_by_id(Handle object, GQuark key_id); native
     * static final private void gtk_object_get(Handle object, String
     * first_property_name); native static final private void
     * gtk_object_set(Handle object, String first_property_name); native static
     * final private void gtk_object_add_arg_type(String arg_name, int arg_type,
     * int arg_flags, int arg_id);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
