/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.gtk.event.ToolButtonEvent;
import org.gnu.gtk.event.ToolButtonListener;
import org.gnu.glib.Handle;

/**
 * A {@link ToolItem} subclass that displays buttons.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolButton extends ToolItem {

    public ToolButton(Widget iconWidget, String label) {
        super(init(iconWidget, label));
    }

    private static Handle init(Widget iconWidget, String label) {
        Handle iconHndl = null;
        if (null != iconWidget)
            iconHndl = iconWidget.getHandle();
        return gtk_tool_button_new(iconHndl, label);
    }

    /**
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolButton(String stockId) {
        super(gtk_tool_button_new_from_stock(stockId));
    }

    public ToolButton(GtkStockItem stockItem) {
        super(gtk_tool_button_new_from_stock(stockItem.getString()));
    }

    public ToolButton(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToolButton getToolButton(Handle handle) {
        if (handle == null)
            return null;

        ToolButton obj = (ToolButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ToolButton(handle);

        return obj;
    }

    public void setLabel(String label) {
        gtk_tool_button_set_label(getHandle(), label);
    }

    public String getLabel() {
        return gtk_tool_button_get_label(getHandle());
    }

    public void setUseUnderline(boolean useUnderline) {
        gtk_tool_button_set_use_underline(getHandle(), useUnderline);
    }

    public boolean getUseUnderline() {
        return gtk_tool_button_get_use_underline(getHandle());
    }

    public void setStockId(String stockId) {
        gtk_tool_button_set_stock_id(getHandle(), stockId);
    }

    public String getStockId() {
        return gtk_tool_button_get_stock_id(getHandle());
    }

    public void setIconWidget(Widget iconWidget) {
        gtk_tool_button_set_icon_widget(getHandle(), iconWidget.getHandle());
    }

    public Widget getIconWidget() {
        Handle hndl = gtk_tool_button_get_icon_widget(getHandle());
        return Widget.getWidget(hndl);
    }

    public void setLabelWidget(Label labelWidget) {
        gtk_tool_button_set_label_widget(getHandle(), labelWidget.getHandle());
    }

    public Label getLabelWidget() {
        Handle hndl = gtk_tool_button_get_label_widget(getHandle());
        return Label.getLabel(hndl);
    }

    /***************************************************************************
     * EVENT HANDLING
     **************************************************************************/
    /**
     * Listeners for handling events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector tbListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see ToolButtonListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ToolButtonListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(tbListeners, listener);
        if (i == -1) {
            if (null == tbListeners) {
                evtMap.initialize(this, ToolButtonEvent.Type.CLICKED);
                tbListeners = new Vector();
            }
            tbListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ToolButtonListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ToolButtonListener listener) {
        int i = findListener(tbListeners, listener);
        if (i > -1) {
            tbListeners.remove(i);
        }
        if (0 == tbListeners.size()) {
            evtMap.uninitialize(this, ToolButtonEvent.Type.CLICKED);
            tbListeners = null;
        }
    }

    protected void fireToolButtonEvent(ToolButtonEvent event) {
        if (null == tbListeners) {
            return;
        }
        int size = tbListeners.size();
        int i = 0;
        while (i < size) {
            ToolButtonListener tbl = (ToolButtonListener) tbListeners
                    .elementAt(i);
            tbl.toolButtonEvent(event);
            i++;
        }
    }

    private void handleClicked() {
        ToolButtonEvent evt = new ToolButtonEvent(this,
                ToolButtonEvent.Type.CLICKED);
        fireToolButtonEvent(evt);
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("clicked", "handleClicked",
                ToolButtonEvent.Type.CLICKED, ToolButtonListener.class);
    }

    native static final protected int gtk_tool_button_get_type();

    native static final protected Handle gtk_tool_button_new(Handle icon,
            String label);

    native static final protected Handle gtk_tool_button_new_from_stock(
            String stockId);

    native static final protected void gtk_tool_button_set_label(Handle button,
            String label);

    native static final protected String gtk_tool_button_get_label(Handle button);

    native static final protected void gtk_tool_button_set_use_underline(
            Handle button, boolean useUnderline);

    native static final protected boolean gtk_tool_button_get_use_underline(
            Handle button);

    native static final protected void gtk_tool_button_set_stock_id(
            Handle button, String stockId);

    native static final protected String gtk_tool_button_get_stock_id(
            Handle button);

    native static final protected void gtk_tool_button_set_icon_widget(
            Handle button, Handle widget);

    native static final protected Handle gtk_tool_button_get_icon_widget(
            Handle button);

    native static final protected void gtk_tool_button_set_label_widget(
            Handle button, Handle widget);

    native static final protected Handle gtk_tool_button_get_label_widget(
            Handle button);
}
