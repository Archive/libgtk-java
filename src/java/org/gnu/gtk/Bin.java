/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This is a base class for a container widget that has only one child. This
 * class contains code common to all widgets that contain only a single child
 * widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Bin</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Bin extends Container {
    protected Bin(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static Bin getBin(Handle handle) {
        if (handle == null)
            return null;

        Bin obj = (Bin) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Bin(handle);

        return obj;
    }

    /**
     * Returns the child of this Bin or null if the bin contains no child.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    // Checks if the Handle has an object associated with it, and if not, try
    // to create the correct subclass of Widget by using reflection. If this
    // fails, then simply create a Widget and return that.
    public Widget getChild() {
        checkState();
        Handle hndl = gtk_bin_get_child(getHandle());
        if (hndl == null) {
            return null;
        }

        Widget widget = (Widget) getGObjectFromHandle(hndl);

        if (widget != null) {
            return widget;
        }

        try {
            return Widget.makeWidget(hndl);
        } catch (ClassNotFoundException cnfe) {
            return new Widget(hndl);
        }
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_bin_get_type());
    }

    native static final protected int gtk_bin_get_type();

    native static final protected Handle gtk_bin_get_child(Handle bin);
}
