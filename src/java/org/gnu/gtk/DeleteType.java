/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class DeleteType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _CHARS = 0;

    static final public org.gnu.gtk.DeleteType CHARS = new org.gnu.gtk.DeleteType(
            _CHARS);

    static final private int _WORD_ENDS = 1;

    static final public org.gnu.gtk.DeleteType WORD_ENDS = new org.gnu.gtk.DeleteType(
            _WORD_ENDS);

    static final private int _WORDS = 2;

    static final public org.gnu.gtk.DeleteType WORDS = new org.gnu.gtk.DeleteType(
            _WORDS);

    static final private int _DISPLAY_LINES = 3;

    static final public org.gnu.gtk.DeleteType DISPLAY_LINES = new org.gnu.gtk.DeleteType(
            _DISPLAY_LINES);

    static final private int _DISPLAY_LINE_ENDS = 4;

    static final public org.gnu.gtk.DeleteType DISPLAY_LINE_ENDS = new org.gnu.gtk.DeleteType(
            _DISPLAY_LINE_ENDS);

    static final private int _PARAGRAPH_ENDS = 5;

    static final public org.gnu.gtk.DeleteType PARAGRAPH_ENDS = new org.gnu.gtk.DeleteType(
            _PARAGRAPH_ENDS);

    static final private int _PARAGRAPHS = 6;

    static final public org.gnu.gtk.DeleteType PARAGRAPHS = new org.gnu.gtk.DeleteType(
            _PARAGRAPHS);

    static final private int _WHITESPACE = 7;

    static final public org.gnu.gtk.DeleteType WHITESPACE = new org.gnu.gtk.DeleteType(
            _WHITESPACE);

    static final private org.gnu.gtk.DeleteType[] theInterned = new org.gnu.gtk.DeleteType[] {
            CHARS, WORD_ENDS, WORDS, DISPLAY_LINES, DISPLAY_LINE_ENDS,
            PARAGRAPH_ENDS, PARAGRAPHS, WHITESPACE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.DeleteType theSacrificialOne = new org.gnu.gtk.DeleteType(
            0);

    static public org.gnu.gtk.DeleteType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.DeleteType already = (org.gnu.gtk.DeleteType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.DeleteType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DeleteType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.DeleteType or(org.gnu.gtk.DeleteType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.DeleteType and(org.gnu.gtk.DeleteType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.DeleteType xor(org.gnu.gtk.DeleteType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.DeleteType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
