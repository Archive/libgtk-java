/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.ModifierType;
import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

public class AccelKey extends MemStruct {
    protected AccelKey() {

    }

    /**
     * Creates a new AccelKey from an existing native resource
     * 
     * @param hndl
     *            The native resource from which to create the object
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelKey(Handle hndl) {
        super(hndl);
    }

    /**
     * Get the KeySymbol associated with this object
     * 
     * @return The KeySymbol associated with the AccelKey
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getKey() {
        return accel_key_get_key(this.getHandle());
    }

    void setKey(int val) {
        accel_key_set_key(this.getHandle(), val);
    }

    /**
     * Returns the modifiers for this object
     * 
     * @return The ModifierType associated with the AccelKey
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ModifierType getModifier() {
        return ModifierType.intern(accel_key_get_mods(this.getHandle()));
    }

    void setModifier(ModifierType type) {
        accel_key_set_mods(this.getHandle(), type.getValue());
    }

    /**
     * Returns the flags for this object
     * 
     * @return The AccelFlags associated with the AccelKey
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AccelFlags getAccelFlags() {
        return AccelFlags.intern(accel_key_get_flags(this.getHandle()));
    }

    void setAccelFlags(AccelFlags val) {
        accel_key_set_flags(this.getHandle(), val.getValue());
    }

    /**
     * Returns a new AccelKey object with the given native resouce
     * 
     * @param hndl
     *            The native resource to use
     * @return a new AccelKey
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AccelKey getAccelKey(Handle hndl) {
        if (hndl == null)
            return null;

        MemStruct struct = MemStruct.getMemStructFromHandle(hndl);
        if (struct == null)
            return new AccelKey(hndl);
        else
            return (AccelKey) struct;
    }

    native static final protected Handle accel_key_new();

    native static final protected int accel_key_get_key(Handle cptr);

    native static final protected void accel_key_set_key(Handle cptr, int accel_key);

    native static final protected int accel_key_get_mods(Handle cptr);

    native static final protected void accel_key_set_mods(Handle cptr, int accel_mods);

    native static final protected int accel_key_get_flags(Handle cptr);

    native static final protected void accel_key_set_flags(Handle cptr, int accel_flags);
}
