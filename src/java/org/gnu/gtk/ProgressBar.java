/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;
import org.gnu.pango.EllipsizeMode;

/**
 * The ProgressBar is typically used to display the progress of a long running
 * operation. It provides a visual clue that processing is underway. The
 * ProgressBar can be used in two different modes: percentage mode and activity
 * mode.
 * 
 * <p>
 * When an application can determine how much work needs to take place (e.g.
 * read a fixed number of bytes from a file) and can monitor its progress, it
 * can use the ProgressBar in percentage mode and the user sees a growing bar
 * indicating the percentage of the work that has been completed. In this mode,
 * the application is required to call {@link #setFraction(double)} periodically
 * to update the progress bar.
 * 
 * <p>
 * When an application has no accurate way of knowing the amount of work to do,
 * it can use the ProgressBar in activity mode, which shows activity by a block
 * moving back and forth within the progress area. In this mode, the application
 * is required to call {@link #pulse()} periodically to update the progress bar.
 * 
 * <p>
 * There is quite a bit of flexibility provided to control the appearance of the
 * ProgressBar. Functions are provided to control the orientation of the bar,
 * optional text can be displayed along with the bar, and the step size used in
 * activity mode can be set.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ProgressBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ProgressBar extends Progress {
    /**
     * Constructs a new ProgressBar widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ProgressBar() {
        super(gtk_progress_bar_new());
    }

    /**
     * Construct a new ProgressBar passing a handle to a native widget resource.
     * 
     * @param handle
     *            The handle to the native ProgressBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ProgressBar(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ProgressBar getProgressBar(Handle handle) {
        if (handle == null) {
            return null;
        }

        ProgressBar obj = (ProgressBar) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new ProgressBar(handle);
        }

        return obj;
    }

    /**
     * Indicates that some progress is made, but you don't know how much. Causes
     * the progress bar to enter "activity mode," where a block bounces back and
     * forth. Each call to pulse() causes the block to move by a little bit (the
     * amount of movement per pulse is determined by
     * {@link #setPulseStep(double)}).
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void pulse() {
        gtk_progress_bar_pulse(getHandle());
    }

    /**
     * Causes the given text to appear superimposed on the progress bar.
     * 
     * @param text
     *            The string to appear on the progress bar
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setText(String text) {
        gtk_progress_bar_set_text(getHandle(), text);
    }

    /**
     * Causes the progress bar to "fill in" the given fraction of the bar. The
     * fraction should be between 0.0 and 1.0, inclusive. Any value less than
     * 0.0 will be considered as 0.0, and any value bigger than 1.0 will be
     * considered as 1.0. The reason why this is done is because Java has a
     * strict implementation of doubles and floats, and sometimes that can cause
     * unwanted rounding of numbers. <br>
     * <br>
     * For example, you may think '0.9 + 0.1 = 1.0', but the fact is that could
     * actually result in 1.00001, or anything like that.
     * 
     * @param fraction
     *            The fraction of the progress bar which should be filled in
     * @throws IllegalArgumentException
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFraction(double fraction) {

        double auxFraction = fraction;

        if (auxFraction < 0.0d) {
            auxFraction = 0.0d;

        } else if (auxFraction > 1.0d) {
            auxFraction = 1.0d;
        }

        gtk_progress_bar_set_fraction(getHandle(), auxFraction);
    }

    /**
     * Returns the current value of the display. This value will be returned as
     * a value between 0.0 and 1.0.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getFraction() {
        return gtk_progress_bar_get_fraction(getHandle());
    }

    /**
     * Sets the fraction of total progress bar length to move the bouncing block
     * for each call to {@link #pulse()}.
     * 
     * @param fraction
     *            fraction between 0.0 and 1.0
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPulseStep(double fraction) {
        gtk_progress_bar_set_pulse_step(getHandle(), fraction);
    }

    /**
     * Causes the progress bar to switch to a different orientation
     * (left-to-right, right-to-left, top-to-bottom, or bottom-to-top).
     * 
     * @see ProgressBarOrientation
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setOrientation(ProgressBarOrientation orientation) {
        gtk_progress_bar_set_orientation(getHandle(), orientation.getValue());
    }

    /**
     * Sets the mode used to ellipsize (add an ellipsis: "...") the text if
     * there is not enough space to render the entire string.
     * 
     * @since 2.6
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEllipsize(EllipsizeMode mode) {
        gtk_progress_bar_set_ellipsize(getHandle(), mode.getValue());
    }

    /**
     * Returns the ellipsizing position.
     * 
     * @since 2.6
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EllipsizeMode getEllipsize() {
        return EllipsizeMode
                .intern(gtk_progress_bar_get_ellipsize(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_progress_bar_get_type());
    }

    native static final protected int gtk_progress_bar_get_type();

    native static final protected Handle gtk_progress_bar_new();

    native static final protected void gtk_progress_bar_pulse(Handle pbar);

    native static final protected void gtk_progress_bar_set_text(Handle pbar,
            String text);

    native static final protected void gtk_progress_bar_set_fraction(
            Handle pbar, double fraction);

    native static final protected void gtk_progress_bar_set_pulse_step(
            Handle pbar, double fraction);

    native static final protected void gtk_progress_bar_set_orientation(
            Handle pbar, int orientation);

    native static final protected String gtk_progress_bar_get_text(Handle pbar);

    native static final protected double gtk_progress_bar_get_fraction(
            Handle pbar);

    native static final protected double gtk_progress_bar_get_pulse_step(
            Handle pbar);

    native static final protected int gtk_progress_bar_get_orientation(
            Handle pbar);

    // GTK 2.6 additions.
    native static final private void gtk_progress_bar_set_ellipsize(
            Handle pbar, int mode);

    native static final private int gtk_progress_bar_get_ellipsize(Handle pbar);

    /*
     * Deprecated functions. native static final private Handle
     * gtk_progress_bar_new_with_adjustment(Handle adjustment); native static
     * final private void gtk_progress_bar_set_bar_style(Handle pbar, int
     * style); native static final private void
     * gtk_progress_bar_set_discrete_blocks(Handle pbar, int blocks); native
     * static final private void gtk_progress_bar_set_activity_step(Handle pbar,
     * int step); native static final private void
     * gtk_progress_bar_set_activity_blocks(Handle pbar, int blocks); native
     * static final private void gtk_progress_bar_update(Handle pbar, double
     * percentage);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
