/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The MenuBar is a subclass of MenuShell which contains one to many MenuItem.
 * The result is a standard menu bar which can hold many menu items. MenuBar
 * allows for a shadow type to be set for aesthetic purposes.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MenuBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class MenuBar extends MenuShell {

    /**
     * Creates a new menubar from a handle to internal resources. This should
     * only be used internally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuBar(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static MenuBar getMenuBar(Handle handle) {
        if (handle == null)
            return null;

        MenuBar obj = (MenuBar) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new MenuBar(handle);

        return obj;
    }

    /**
     * Create a new MenuBar.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuBar() {
        super(gtk_menu_bar_new());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_menu_bar_get_type());
    }

    /**
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PackDirection getPackDirection() {
        return PackDirection
                .intern(gtk_menu_bar_get_pack_direction(getHandle()));
    }

    /**
     * 
     * @param packDirection
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPackDirection(PackDirection packDirection) {
        gtk_menu_bar_set_pack_direction(getHandle(), packDirection.getValue());
    }

    /**
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PackDirection getChildPackDirection() {
        return PackDirection
                .intern(gtk_menu_bar_get_child_pack_direction(getHandle()));
    }

    public void setChildPackDirection(PackDirection packDirection) {
        gtk_menu_bar_set_child_pack_direction(getHandle(), packDirection
                .getValue());
    }

    native static final protected int gtk_menu_bar_get_type();

    native static final protected Handle gtk_menu_bar_new();

    // new in gtk 2.8
    native static final protected int gtk_menu_bar_get_pack_direction(Handle mb);

    native static final protected void gtk_menu_bar_set_pack_direction(
            Handle mb, int pack);

    native static final protected int gtk_menu_bar_get_child_pack_direction(
            Handle mb);

    native static final protected void gtk_menu_bar_set_child_pack_direction(
            Handle mb, int pack);

}
