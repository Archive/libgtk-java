/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class MovementStep extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _LOGICAL_POSITIONS = 0;

    static final public org.gnu.gtk.MovementStep LOGICAL_POSITIONS = new org.gnu.gtk.MovementStep(
            _LOGICAL_POSITIONS);

    static final private int _VISUAL_POSITIONS = 1;

    static final public org.gnu.gtk.MovementStep VISUAL_POSITIONS = new org.gnu.gtk.MovementStep(
            _VISUAL_POSITIONS);

    static final private int _WORDS = 2;

    static final public org.gnu.gtk.MovementStep WORDS = new org.gnu.gtk.MovementStep(
            _WORDS);

    static final private int _DISPLAY_LINES = 3;

    static final public org.gnu.gtk.MovementStep DISPLAY_LINES = new org.gnu.gtk.MovementStep(
            _DISPLAY_LINES);

    static final private int _DISPLAY_LINE_ENDS = 4;

    static final public org.gnu.gtk.MovementStep DISPLAY_LINE_ENDS = new org.gnu.gtk.MovementStep(
            _DISPLAY_LINE_ENDS);

    static final private int _PARAGRAPHS = 5;

    static final public org.gnu.gtk.MovementStep PARAGRAPHS = new org.gnu.gtk.MovementStep(
            _PARAGRAPHS);

    static final private int _PARAGRAPH_ENDS = 6;

    static final public org.gnu.gtk.MovementStep PARAGRAPH_ENDS = new org.gnu.gtk.MovementStep(
            _PARAGRAPH_ENDS);

    static final private int _PAGES = 7;

    static final public org.gnu.gtk.MovementStep PAGES = new org.gnu.gtk.MovementStep(
            _PAGES);

    static final private int _BUFFER_ENDS = 8;

    static final public org.gnu.gtk.MovementStep BUFFER_ENDS = new org.gnu.gtk.MovementStep(
            _BUFFER_ENDS);

    static final private org.gnu.gtk.MovementStep[] theInterned = new org.gnu.gtk.MovementStep[] {
            LOGICAL_POSITIONS, VISUAL_POSITIONS, WORDS, DISPLAY_LINES,
            DISPLAY_LINE_ENDS, PARAGRAPHS, PARAGRAPH_ENDS, PAGES, BUFFER_ENDS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.MovementStep theSacrificialOne = new org.gnu.gtk.MovementStep(
            0);

    static public org.gnu.gtk.MovementStep intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.MovementStep already = (org.gnu.gtk.MovementStep) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.MovementStep(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private MovementStep(int value) {
        value_ = value;
    }

    public org.gnu.gtk.MovementStep or(org.gnu.gtk.MovementStep other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.MovementStep and(org.gnu.gtk.MovementStep other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.MovementStep xor(org.gnu.gtk.MovementStep other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.MovementStep other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
