/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The HScrollBar displays a horizontal scrollbar with a slider and a pair of
 * arrow buttons. To create a scrollbar, you must specify the beginning and
 * ending values, the size of the slider, an initial slider position, and how
 * far the slider moves when a mouse clicks the slot.
 * 
 * @see VScrollBar
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.HScrollBar</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class HScrollBar extends ScrollBar {
    /**
     * Creates a new ScrollBar
     * 
     * @param adjustment
     *            The GtkAdjustment to use, or null to create a new adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public HScrollBar(Adjustment adjustment) {
        super(gtk_hscrollbar_new(adjustment == null ? null : adjustment
                .getHandle()));
    }

    /**
     * Construct a HScrollBar from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public HScrollBar(Handle handle) {
        super(handle);
    }

    /**
     * Construct a HScrollBar from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static HScrollBar getHScrollBar(Handle handle) {
        if (handle == null)
            return null;

        HScrollBar obj = (HScrollBar) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new HScrollBar(handle);

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_hscrollbar_get_type());
    }

    native static final protected int gtk_hscrollbar_get_type();

    native static final protected Handle gtk_hscrollbar_new(Handle adjustment);

}
