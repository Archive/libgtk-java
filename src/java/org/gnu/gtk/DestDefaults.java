/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class DestDefaults extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _MOTION = 1 << 0;

    static final public org.gnu.gtk.DestDefaults MOTION = new org.gnu.gtk.DestDefaults(
            _MOTION);

    static final private int _HIGHLIGHT = 1 << 1;

    static final public org.gnu.gtk.DestDefaults HIGHLIGHT = new org.gnu.gtk.DestDefaults(
            _HIGHLIGHT);

    static final private int _DROP = 1 << 2;

    static final public org.gnu.gtk.DestDefaults DROP = new org.gnu.gtk.DestDefaults(
            _DROP);

    static final private int _ALL = 0x07;

    static final public org.gnu.gtk.DestDefaults ALL = new org.gnu.gtk.DestDefaults(
            _ALL);

    static final private org.gnu.gtk.DestDefaults[] theInterned = new org.gnu.gtk.DestDefaults[] {
            new org.gnu.gtk.DestDefaults(0), MOTION, HIGHLIGHT,
            new org.gnu.gtk.DestDefaults(3), DROP,
            new org.gnu.gtk.DestDefaults(5), new org.gnu.gtk.DestDefaults(6),
            new org.gnu.gtk.DestDefaults(7), ALL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.DestDefaults theSacrificialOne = new org.gnu.gtk.DestDefaults(
            0);

    static public org.gnu.gtk.DestDefaults intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.DestDefaults already = (org.gnu.gtk.DestDefaults) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.DestDefaults(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DestDefaults(int value) {
        value_ = value;
    }

    public org.gnu.gtk.DestDefaults or(org.gnu.gtk.DestDefaults other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.DestDefaults and(org.gnu.gtk.DestDefaults other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.DestDefaults xor(org.gnu.gtk.DestDefaults other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.DestDefaults other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
