/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class TreeViewDropPosition extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _BEFORE = 0;

    static final public org.gnu.gtk.TreeViewDropPosition BEFORE = new org.gnu.gtk.TreeViewDropPosition(
            _BEFORE);

    static final private int _AFTER = 1;

    static final public org.gnu.gtk.TreeViewDropPosition AFTER = new org.gnu.gtk.TreeViewDropPosition(
            _AFTER);

    static final private int _INTO_OR_BEFORE = 2;

    static final public org.gnu.gtk.TreeViewDropPosition INTO_OR_BEFORE = new org.gnu.gtk.TreeViewDropPosition(
            _INTO_OR_BEFORE);

    static final private int _INTO_OR_AFTER = 3;

    static final public org.gnu.gtk.TreeViewDropPosition INTO_OR_AFTER = new org.gnu.gtk.TreeViewDropPosition(
            _INTO_OR_AFTER);

    static final private org.gnu.gtk.TreeViewDropPosition[] theInterned = new org.gnu.gtk.TreeViewDropPosition[] {
            BEFORE, AFTER, INTO_OR_BEFORE, INTO_OR_AFTER }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TreeViewDropPosition theSacrificialOne = new org.gnu.gtk.TreeViewDropPosition(
            0);

    static public org.gnu.gtk.TreeViewDropPosition intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TreeViewDropPosition already = (org.gnu.gtk.TreeViewDropPosition) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TreeViewDropPosition(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TreeViewDropPosition(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TreeViewDropPosition or(
            org.gnu.gtk.TreeViewDropPosition other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TreeViewDropPosition and(
            org.gnu.gtk.TreeViewDropPosition other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TreeViewDropPosition xor(
            org.gnu.gtk.TreeViewDropPosition other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TreeViewDropPosition other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
