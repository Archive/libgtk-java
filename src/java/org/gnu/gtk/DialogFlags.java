/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Flags used to influence dialog construction.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.DialogFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class DialogFlags extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _MODAL = 0;

    /**
     * Make the constructed dialog modal.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.DialogFlags MODAL = new org.gnu.gtk.DialogFlags(
            _MODAL);

    static final private int _DESTROY_WITH_PARENT = 1;

    /**
     * Destroy the dialog when its parent is destroyed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.DialogFlags DESTROY_WITH_PARENT = new org.gnu.gtk.DialogFlags(
            _DESTROY_WITH_PARENT);

    static final private int _NO_SEPARATOR = 2;

    /**
     * Don't put a separator between the action area and the dialog content.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.DialogFlags NO_SEPARATOR = new org.gnu.gtk.DialogFlags(
            _NO_SEPARATOR);

    static final private org.gnu.gtk.DialogFlags[] theInterned = new org.gnu.gtk.DialogFlags[] {
            MODAL, DESTROY_WITH_PARENT, NO_SEPARATOR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.DialogFlags theSacrificialOne = new org.gnu.gtk.DialogFlags(
            0);

    static public org.gnu.gtk.DialogFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.DialogFlags already = (org.gnu.gtk.DialogFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.DialogFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DialogFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.DialogFlags or(org.gnu.gtk.DialogFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.DialogFlags and(org.gnu.gtk.DialogFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.DialogFlags xor(org.gnu.gtk.DialogFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.DialogFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
