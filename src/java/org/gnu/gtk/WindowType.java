/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class WindowType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TOPLEVEL = 0;

    static final public org.gnu.gtk.WindowType TOPLEVEL = new org.gnu.gtk.WindowType(
            _TOPLEVEL);

    static final private int _POPUP = 1;

    static final public org.gnu.gtk.WindowType POPUP = new org.gnu.gtk.WindowType(
            _POPUP);

    static final private org.gnu.gtk.WindowType[] theInterned = new org.gnu.gtk.WindowType[] {
            TOPLEVEL, POPUP }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.WindowType theSacrificialOne = new org.gnu.gtk.WindowType(
            0);

    static public org.gnu.gtk.WindowType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.WindowType already = (org.gnu.gtk.WindowType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.WindowType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.WindowType or(org.gnu.gtk.WindowType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.WindowType and(org.gnu.gtk.WindowType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.WindowType xor(org.gnu.gtk.WindowType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.WindowType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
