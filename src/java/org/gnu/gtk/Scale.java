/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.ScaleEvent;
import org.gnu.gtk.event.ScaleListener;
import org.gnu.glib.Handle;

/**
 * A Scale is a slider control used to select a numeric value. To use it, you'll
 * probably want to investigate the methods on its base class, {@link Range},
 * in addition to the methods for Scale itself. To set the value of a scale, you
 * would normally use {@link Range#setValue(double)}. To detect changes to the
 * value, add RangeListener object to the widget.
 * 
 * <p>
 * The GtkScale widget is an abstract class, used only for deriving the
 * subclasses {@link HScale} and {@link VScale}.
 * 
 * @see HScale
 * @see VScale
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Scale</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public abstract class Scale extends Range {

    protected Scale(Handle handle) {
        super(handle);
    }

    /**
     * Sets the number of decimal places that are displayed in the value. Also
     * causes the value of the adjustment to be rounded off to this number of
     * digits, so the retrieved value matches the value the user saw.
     * 
     * @param digits
     *            The number of decimal places to display, e.g. use 1 to display
     *            1.0, 2 to display 1.00 etc.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDigits(int digits) {
        gtk_scale_set_digits(getHandle(), digits);
    }

    /**
     * Sets the position in which the current value is displayed.
     * 
     * @param pos
     *            The position in which the current value is displayed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setValuePosition(PositionType pos) {
        gtk_scale_set_value_pos(getHandle(), pos.getValue());
    }

    /**
     * Specifies whether the current value is displayed as a string next to the
     * slider.
     * 
     * @param setting
     *            If true, the value is displayed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDrawValue(boolean setting) {
        gtk_scale_set_draw_value(getHandle(), setting);
    }

    /**
     * Returns whether the current value is displayed as a string next to the
     * slider.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDrawValue() {
        return gtk_scale_get_draw_value(getHandle());
    }

    /*
     * EVENT HANDLING
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    ScaleListener formatListener = null;

    /**
     * Sets a listener to be used when a format-value request is called.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFormatListener(ScaleListener listener) {
        if (null == formatListener)
            evtMap.initialize(this, ScaleEvent.Type.FORMAT_VALUE);
        // removeFormatListener();
        formatListener = listener;
    }

    /**
     * Removes the format listener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeFormatListener() {
        evtMap.uninitialize(this, ScaleEvent.Type.FORMAT_VALUE);
        formatListener = null;
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("format_value", "handleFormatValue",
                ScaleEvent.Type.FORMAT_VALUE, ScaleListener.class);
    }

    private String handleFormatValue(double value) {
        if (null == formatListener) {
            return (java.lang.Double.toString(value));
        } else {
            return (formatListener
                    .formatScaleValue(new ScaleEvent(this), value));
        }
    }

    /**
     * Gets the {@link org.gnu.pango.Layout} used to display the scale.
     * 
     * @return The {@link org.gnu.pango.Layout} for this scale, or NULL if the
     *         {@link #getDrawValue} is FALSE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Layout getLayout() {
        Handle layout = gtk_scale_get_layout(getHandle());
        return Layout.getLayout(layout);
    }

    /**
     * Obtains the X coordinate where the scale will draw the
     * {@link org.gnu.pango.Layout} representing the text in the scale.
     * <p>
     * If {@link #getDrawValue} is FALSE, the return value is undefined.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLayoutOffsetX() {
        int x[] = new int[1];
        int y[] = new int[1];
        gtk_scale_get_layout_offsets(getHandle(), x, y);
        return x[0];
    }

    /**
     * Obtains the Y coordinate where the scale will draw the
     * {@link org.gnu.pango.Layout} representing the text in the scale.
     * <p>
     * If {@link #getDrawValue} is FALSE, the return value is undefined.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getLayoutOffsetY() {
        int x[] = new int[1];
        int y[] = new int[1];
        gtk_scale_get_layout_offsets(getHandle(), x, y);
        return y[0];
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_scale_get_type());
    }

    native static final protected int gtk_scale_get_type();

    native static final protected void gtk_scale_set_digits(Handle scale,
            int digits);

    native static final protected int gtk_scale_get_digits(Handle scale);

    native static final protected void gtk_scale_set_draw_value(Handle scale,
            boolean drawValue);

    native static final protected boolean gtk_scale_get_draw_value(Handle scale);

    native static final protected void gtk_scale_set_value_pos(Handle scale,
            int pos);

    native static final protected int gtk_scale_get_value_pos(Handle scale);

    native static final private Handle gtk_scale_get_layout(Handle scale);

    native static final private void gtk_scale_get_layout_offsets(Handle scale,
            int[] x, int[] y);

}
