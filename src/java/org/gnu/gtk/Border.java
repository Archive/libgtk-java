/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * @deprecated
 */
public class Border extends Boxed {

    public int getLeft() {
        return getLeft(getHandle());
    }

    public int getRight() {
        return getRight(getHandle());
    }

    public int getTop() {
        return getTop(getHandle());
    }

    public int getBottom() {
        return getBottom(getHandle());
    }

    native static final protected int getLeft(Handle cptr);

    native static final protected int getRight(Handle cptr);

    native static final protected int getTop(Handle cptr);

    native static final protected int getBottom(Handle cptr);

}
