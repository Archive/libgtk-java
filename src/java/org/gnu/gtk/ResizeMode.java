/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * The resize mode of a {@link org.gnu.gtk.Container} determines whether a
 * resize request will be passed to the container's parent, queued for later
 * execution or executed immediately. FIXME: This is poorly documented by GTK;
 * can we do better?
 * 
 * @author Andrew Cowie
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ResizeMode</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ResizeMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _PARENT = 0;

    /**
     * Pass resize request to the parent.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ResizeMode PARENT = new org.gnu.gtk.ResizeMode(
            _PARENT);

    static final private int _QUEUE = 1;

    /**
     * Queue resizes on this widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ResizeMode QUEUE = new org.gnu.gtk.ResizeMode(
            _QUEUE);

    static final private int _IMMEDIATE = 2;

    /**
     * Perform the resizes now.
     * 
     * @deprecated per GTK API documentation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gtk.ResizeMode IMMEDIATE = new org.gnu.gtk.ResizeMode(
            _IMMEDIATE);

    static final private org.gnu.gtk.ResizeMode[] theInterned = new org.gnu.gtk.ResizeMode[] {
            PARENT, QUEUE, IMMEDIATE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ResizeMode theSacrificialOne = new org.gnu.gtk.ResizeMode(
            0);

    static public org.gnu.gtk.ResizeMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ResizeMode already = (org.gnu.gtk.ResizeMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ResizeMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ResizeMode(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ResizeMode or(org.gnu.gtk.ResizeMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ResizeMode and(org.gnu.gtk.ResizeMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ResizeMode xor(org.gnu.gtk.ResizeMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ResizeMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
