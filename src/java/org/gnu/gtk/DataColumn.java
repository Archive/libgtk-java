/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 *
 */

package org.gnu.gtk;

import org.gnu.glib.Type;

/**
 * Represents a block in which data can be stored in a {@link TreeModel} (such
 * as {@link ListStore} or {@link TreeStore}). In Gtk, these are called columns
 * and are referenced by integers. We have renamed them to DataBlocks to avoid
 * confusion with {@link TreeViewColumn}s and created these objects to use
 * rather than integers. This allows type checking and should make programs more
 * maintainable. This is an abstract class - there is an implementation of it
 * for each type of data accepted by the two tree models.
 * 
 * @see ListStore
 * @see TreeStore
 * 
 * @author Mark Howard &lt;mh@debian.org&gt;
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.DataColumn</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public abstract class DataColumn {

    protected Type type = null;

    protected int column = -1;

    /**
     * Returns the column number of this dataBlock used internally by Gtk.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getColumn() {
        if (column >= 0)
            return column;
        else
            throw new RuntimeException(
                    "The column field has not been set - this DataColumn has not yet been passed to a TreeModel");
    }

    /**
     * Sets the integer column number
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    void setColumn(int column) {
        this.column = column;
    }

    /**
     * Returns the glib Type of data contained in this column. This can only be
     * called once. (it is only needed once - in the constructor of the
     * listStore/TreeStore)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Type getType() {
        if (type != null) {
            Type t = type;
            type = null;
            return t;
        } else
            throw new RuntimeException(
                    "The type either hasn't been set, or has already been requested and destroyed");
    }

    /**
     * Determine if the given DataColumn represents the same column in the store
     * as the current object. This compares the column ids using the
     * {@link #getColumn} method.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equals(DataColumn col) {
        return (getColumn() == col.getColumn());
    }
}
