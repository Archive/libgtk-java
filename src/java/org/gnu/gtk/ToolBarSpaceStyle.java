/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * @deprecated
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolBarSpaceStyle</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolBarSpaceStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _EMPTY = 0;

    static final public org.gnu.gtk.ToolBarSpaceStyle EMPTY = new org.gnu.gtk.ToolBarSpaceStyle(
            _EMPTY);

    static final private int _LINE = 1;

    static final public org.gnu.gtk.ToolBarSpaceStyle LINE = new org.gnu.gtk.ToolBarSpaceStyle(
            _LINE);

    static final private org.gnu.gtk.ToolBarSpaceStyle[] theInterned = new org.gnu.gtk.ToolBarSpaceStyle[] {
            EMPTY, LINE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ToolBarSpaceStyle theSacrificialOne = new org.gnu.gtk.ToolBarSpaceStyle(
            0);

    static public org.gnu.gtk.ToolBarSpaceStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ToolBarSpaceStyle already = (org.gnu.gtk.ToolBarSpaceStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ToolBarSpaceStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ToolBarSpaceStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ToolBarSpaceStyle or(org.gnu.gtk.ToolBarSpaceStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ToolBarSpaceStyle and(org.gnu.gtk.ToolBarSpaceStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ToolBarSpaceStyle xor(org.gnu.gtk.ToolBarSpaceStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ToolBarSpaceStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
