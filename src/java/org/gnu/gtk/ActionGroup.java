/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.GObject;
import org.gnu.gtk.event.RadioActionEntryListener;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ActionGroup</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ActionGroup extends GObject {

    /**
     * Construct a new ActionGroup object.
     * 
     * @param name
     *            Used when associating keybindings with the actions.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ActionGroup(String name) {
        super(gtk_action_group_new(name));
    }

    ActionGroup(Handle hndl) {
        super(hndl);
    }

    static ActionGroup getActionGroup(Handle handle) {
        if (handle == null)
            return null;

        ActionGroup obj = (ActionGroup) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ActionGroup(handle);

        return obj;
    }

    /**
     * Get the name of the action group.
     * 
     * @return the name of the action group
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return gtk_action_group_get_name(getHandle());
    }

    public boolean getSensitive() {
        return gtk_action_group_get_sensitive(getHandle());
    }

    public void setSensitive(boolean sensitive) {
        gtk_action_group_set_sensitive(getHandle(), sensitive);
    }

    public boolean getVisible() {
        return gtk_action_group_get_visible(getHandle());
    }

    public void setVisible(boolean visible) {
        gtk_action_group_set_visible(getHandle(), visible);
    }

    public String translateString(String str) {
        return gtk_action_group_translate_string(getHandle(), str);
    }

    /**
     * Look up an Action in the ActionGroup by name.
     * 
     * @param actionName
     * @return the Action or null if no Action by that name exists.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Action getAction(String actionName) {
        Handle hndl = gtk_action_group_get_action(getHandle(), actionName);
        return Action.getAction(hndl);
    }

    /**
     * Add an Action object to the ActionGroup
     * 
     * @param anAction
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addAction(Action anAction) {
        gtk_action_group_add_action(getHandle(), anAction.getHandle());
    }

    /**
     * Add an array of ActonEntry objects to the ActionGroup
     * 
     * @param entries
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addActions(ActionEntry[] entries) {
        Handle[] hndls = new Handle[entries.length];
        for (int i = 0; i < entries.length; i++) {
            hndls[i] = entries[i].getHandle();
        }
        addActions(getHandle(), hndls, entries);
    }

    /**
     * Add an array of ToggleActonEntry objects to the ActionGroup
     * 
     * @param entries
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addToggleActions(ToggleActionEntry[] entries) {
        Handle[] hndls = new Handle[entries.length];
        for (int i = 0; i < entries.length; i++) {
            hndls[i] = entries[i].getHandle();
        }
        addToggleActions(getHandle(), hndls, entries);
    }

    /**
     * Add an array of RadioActionEntry objects to the ActionGroup and setup the
     * event handling.
     * 
     * @param entries
     * @param listener
     * @param initialValue
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addRadioActions(RadioActionEntry[] entries, int initialValue,
            RadioActionEntryListener listener) {
        Handle[] hndls = new Handle[entries.length];
        for (int i = 0; i < entries.length; i++) {
            hndls[i] = entries[i].getHandle();
        }
        addRadioActions(getHandle(), hndls, initialValue, this);
        // TODO: handle radio events - collect listeners
    }

    /**
     * Remove an Action object from the ActionGroup
     * 
     * @param anAction
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeAction(Action anAction) {
        gtk_action_group_remove_action(getHandle(), anAction.getHandle());
    }

    /**
     * List the Actions in the ActionGroup
     * 
     * @return A list of Action objects.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public List listActions() {
        Handle[] actions = gtk_action_group_list_actions(getHandle());
        List objs = new ArrayList();
        for (int i = 0; i < actions.length; i++) {
            objs.add(Action.getAction(actions[i]));
        }
        return objs;
    }

    private void handleRadioAction(Handle action, int current) {

    }

    native static final protected int gtk_action_group_get_type();

    native static final protected Handle gtk_action_group_new(String name);

    native static final protected String gtk_action_group_get_name(Handle group);

    native static final protected boolean gtk_action_group_get_sensitive(
            Handle group);

    native static final protected void gtk_action_group_set_sensitive(
            Handle group, boolean sensitive);

    native static final protected boolean gtk_action_group_get_visible(
            Handle group);

    native static final protected void gtk_action_group_set_visible(
            Handle group, boolean visible);

    native static final protected Handle gtk_action_group_get_action(
            Handle group, String actionName);

    native static final protected Handle[] gtk_action_group_list_actions(
            Handle group);

    native static final protected void gtk_action_group_add_action(
            Handle group, Handle action);

    native static final protected void gtk_action_group_remove_action(
            Handle group, Handle action);

    native static final protected void gtk_action_group_set_translation_domain(
            Handle group, String domain);

    native static final protected String gtk_action_group_translate_string(
            Handle group, String str);

    native static final protected void gtk_action_group_add_action_with_accel(
            Handle group, Handle action, String accelerator);

    native static final protected void addActions(Handle group,
            Handle[] entries, Object[] cbObjs);

    native static final protected void addToggleActions(Handle group,
            Handle[] entries, Object[] cbObjs);

    native static final protected void addRadioActions(Handle group,
            Handle[] entries, int value, ActionGroup listener);
}
