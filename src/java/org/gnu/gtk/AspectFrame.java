/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This container widget wraps a single widget in a frame with a label, and
 * imposes an aspect ratio on the child widget. It is useful when you want to
 * pack a widget so that it can be resized but always retain the same aspect
 * ratio.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.AspectFrame</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class AspectFrame extends Frame {

    /**
     * Construct a new AspectFrame.
     * 
     * @param label
     *            The text label for the frame.
     * @param xAlign
     *            The horizontal alignment for the child within the allocation
     *            of the AspectFrame. This ranges from 0.0 (left aligned) to 1.0
     *            (right aligned).
     * @param yAlign
     *            The vertical alignment for the child within the allocation of
     *            the AspectFrame. This ranges from 0.0 (top aligned) to 1.0
     *            (bottom aligned).
     * @param ratio
     *            The desired aspect ration.
     * @param obeyChild
     *            If true the ratio is ignored and the aspect ratio is taken
     *            from the requisition of the child.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AspectFrame(String label, double xAlign, double yAlign,
            double ratio, boolean obeyChild) {
        super(gtk_aspect_frame_new(label, xAlign, yAlign, ratio, obeyChild));
    }

    /**
     * Construct an AspectFrame using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AspectFrame(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AspectFrame getAspectFrame(Handle handle) {
        if (handle == null)
            return null;

        AspectFrame obj = (AspectFrame) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new AspectFrame(handle);

        return obj;
    }

    /**
     * Set the aspect for an already created AspectFrame.
     * 
     * @param xAlign
     *            The horizontal alignment for the child within the allocation
     *            of the AspectFrame. This ranges from 0.0 (left aligned) to 1.0
     *            (right aligned).
     * @param yAlign
     *            The vertical alignment for the child within the allocation of
     *            the AspectFrame. This ranges from 0.0 (top aligned) to 1.0
     *            (bottom aligned).
     * @param ratio
     *            The desired aspect ration.
     * @param obeyChild
     *            If true the ratio is ignored and the aspect ratio is taken
     *            from the requisition of the child.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAspect(double xAlign, double yAlign, double ratio,
            boolean obeyChild) {
        AspectFrame.gtk_aspect_frame_set(getHandle(), xAlign, yAlign, ratio,
                obeyChild);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_aspect_frame_get_type());
    }

    native static final protected int gtk_aspect_frame_get_type();

    native static final protected Handle gtk_aspect_frame_new(String label,
            double xalign, double yalign, double ratio, boolean obeyChild);

    native static final protected void gtk_aspect_frame_set(
            Handle aspect_frame, double xalign, double yalign, double ratio,
            boolean obeyChild);

}
