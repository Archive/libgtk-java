/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Class that provides the ability to group several Windows together.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.WindowGroup</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class WindowGroup extends GObject {
    /**
     * Construct a new WindowGroup
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public WindowGroup() {
        super(gtk_window_group_new());
    }

    /**
     * Add a Window to this WindowGroup
     * 
     * @param window
     *            The Window to be added to this WindowGroup
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addWindow(Window window) {
        gtk_window_group_add_window(getHandle(), window.getHandle());
    }

    /**
     * Remove a Window from this WindowGroup
     * 
     * @param window
     *            The Window to be removed from this WindowGroup.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeWindow(Window window) {
        gtk_window_group_remove_window(getHandle(), window.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_window_group_get_type());
    }

    native static final protected int gtk_window_group_get_type();

    native static final protected Handle gtk_window_group_new();

    native static final protected void gtk_window_group_add_window(
            Handle windowGroup, Handle window);

    native static final protected void gtk_window_group_remove_window(
            Handle windowGroup, Handle window);
}
