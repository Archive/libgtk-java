/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileFilterFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileFilterFlags extends Flags {
    static final private int _FILENAME = 1 << 0;

    static final public FileFilterFlags FILENAME = new FileFilterFlags(
            _FILENAME);

    static final private int _URI = 1 << 1;

    static final public FileFilterFlags URI = new FileFilterFlags(_URI);

    static final private int _DISPLAY_NAME = 1 << 2;

    static final public FileFilterFlags DISPLAY_NAME = new FileFilterFlags(
            _DISPLAY_NAME);

    static final private int _MIME_TYPE = 1 << 3;

    static final public FileFilterFlags MIME_TYPE = new FileFilterFlags(
            _MIME_TYPE);

    static final private FileFilterFlags[] theInterned = new FileFilterFlags[] {
            FILENAME, URI, DISPLAY_NAME, MIME_TYPE };

    static private java.util.Hashtable theInternedExtras;

    static final private FileFilterFlags theSacrificialOne = new FileFilterFlags(
            0);

    static public FileFilterFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FileFilterFlags already = (FileFilterFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileFilterFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileFilterFlags(int value) {
        value_ = value;
    }

    public FileFilterFlags or(FileFilterFlags other) {
        return intern(value_ | other.value_);
    }

    public FileFilterFlags and(FileFilterFlags other) {
        return intern(value_ & other.value_);
    }

    public FileFilterFlags xor(FileFilterFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileFilterFlags other) {
        return (value_ & other.value_) == other.value_;
    }
}
