/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class SubmenuPlacement extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TOP_BOTTOM = 0;

    static final public org.gnu.gtk.SubmenuPlacement TOP_BOTTOM = new org.gnu.gtk.SubmenuPlacement(
            _TOP_BOTTOM);

    static final private int _LEFT_RIGHT = 1;

    static final public org.gnu.gtk.SubmenuPlacement LEFT_RIGHT = new org.gnu.gtk.SubmenuPlacement(
            _LEFT_RIGHT);

    static final private org.gnu.gtk.SubmenuPlacement[] theInterned = new org.gnu.gtk.SubmenuPlacement[] {
            TOP_BOTTOM, LEFT_RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.SubmenuPlacement theSacrificialOne = new org.gnu.gtk.SubmenuPlacement(
            0);

    static public org.gnu.gtk.SubmenuPlacement intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.SubmenuPlacement already = (org.gnu.gtk.SubmenuPlacement) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.SubmenuPlacement(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SubmenuPlacement(int value) {
        value_ = value;
    }

    public org.gnu.gtk.SubmenuPlacement or(org.gnu.gtk.SubmenuPlacement other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.SubmenuPlacement and(org.gnu.gtk.SubmenuPlacement other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.SubmenuPlacement xor(org.gnu.gtk.SubmenuPlacement other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.SubmenuPlacement other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
