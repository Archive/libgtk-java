/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class WidgetFlags extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _DESTROYED = 1 << 0;

    static final public org.gnu.gtk.WidgetFlags DESTROYED = new org.gnu.gtk.WidgetFlags(
            _DESTROYED);

    static final private int _FLOATING = 1 << 1;

    static final public org.gnu.gtk.WidgetFlags FLOATING = new org.gnu.gtk.WidgetFlags(
            _FLOATING);

    static final private int _CONNECTED = 1 << 2;

    static final public org.gnu.gtk.WidgetFlags CONNECTED = new org.gnu.gtk.WidgetFlags(
            _CONNECTED);

    static final private int _CONSTRUCTED = 1 << 3;

    static final public org.gnu.gtk.WidgetFlags CONSTRUCTED = new org.gnu.gtk.WidgetFlags(
            _CONSTRUCTED);

    static final private int _TOPLEVEL = 1 << 4;

    static final public org.gnu.gtk.WidgetFlags TOPLEVEL = new org.gnu.gtk.WidgetFlags(
            _TOPLEVEL);

    static final private int _NO_WINDOW = 1 << 5;

    static final public org.gnu.gtk.WidgetFlags NO_WINDOW = new org.gnu.gtk.WidgetFlags(
            _NO_WINDOW);

    static final private int _REALIZED = 1 << 6;

    static final public org.gnu.gtk.WidgetFlags REALIZED = new org.gnu.gtk.WidgetFlags(
            _REALIZED);

    static final private int _MAPPED = 1 << 7;

    static final public org.gnu.gtk.WidgetFlags MAPPED = new org.gnu.gtk.WidgetFlags(
            _MAPPED);

    static final private int _VISIBLE = 1 << 8;

    static final public org.gnu.gtk.WidgetFlags VISIBLE = new org.gnu.gtk.WidgetFlags(
            _VISIBLE);

    static final private int _SENSITIVE = 1 << 9;

    static final public org.gnu.gtk.WidgetFlags SENSITIVE = new org.gnu.gtk.WidgetFlags(
            _SENSITIVE);

    static final private int _PARENT_SENSITIVE = 1 << 10;

    static final public org.gnu.gtk.WidgetFlags PARENT_SENSITIVE = new org.gnu.gtk.WidgetFlags(
            _PARENT_SENSITIVE);

    static final private int _CAN_FOCUS = 1 << 11;

    static final public org.gnu.gtk.WidgetFlags CAN_FOCUS = new org.gnu.gtk.WidgetFlags(
            _CAN_FOCUS);

    static final private int _HAS_FOCUS = 1 << 12;

    static final public org.gnu.gtk.WidgetFlags HAS_FOCUS = new org.gnu.gtk.WidgetFlags(
            _HAS_FOCUS);

    static final private int _CAN_DEFAULT = 1 << 13;

    static final public org.gnu.gtk.WidgetFlags CAN_DEFAULT = new org.gnu.gtk.WidgetFlags(
            _CAN_DEFAULT);

    static final private int _HAS_DEFAULT = 1 << 14;

    static final public org.gnu.gtk.WidgetFlags HAS_DEFAULT = new org.gnu.gtk.WidgetFlags(
            _HAS_DEFAULT);

    static final private int _HAS_GRAB = 1 << 15;

    static final public org.gnu.gtk.WidgetFlags HAS_GRAB = new org.gnu.gtk.WidgetFlags(
            _HAS_GRAB);

    static final private int _RC_STYLE = 1 << 16;

    static final public org.gnu.gtk.WidgetFlags RC_STYLE = new org.gnu.gtk.WidgetFlags(
            _RC_STYLE);

    static final private int _COMPOSITE_CHILD = 1 << 17;

    static final public org.gnu.gtk.WidgetFlags COMPOSITE_CHILD = new org.gnu.gtk.WidgetFlags(
            _COMPOSITE_CHILD);

    static final private int _NO_REPARENT = 1 << 18;

    static final public org.gnu.gtk.WidgetFlags NO_REPARENT = new org.gnu.gtk.WidgetFlags(
            _NO_REPARENT);

    static final private int _APP_PAINTABLE = 1 << 19;

    static final public org.gnu.gtk.WidgetFlags APP_PAINTABLE = new org.gnu.gtk.WidgetFlags(
            _APP_PAINTABLE);

    static final private int _RECEIVES_DEFAULT = 1 << 20;

    static final public org.gnu.gtk.WidgetFlags RECEIVES_DEFAULT = new org.gnu.gtk.WidgetFlags(
            _RECEIVES_DEFAULT);

    static final private int _DOUBLE_BUFFERED = 1 << 21;

    static final public org.gnu.gtk.WidgetFlags DOUBLE_BUFFERED = new org.gnu.gtk.WidgetFlags(
            _DOUBLE_BUFFERED);

    static final private org.gnu.gtk.WidgetFlags[] theInterned = new org.gnu.gtk.WidgetFlags[] {
            new org.gnu.gtk.WidgetFlags(0), DESTROYED, FLOATING,
            new org.gnu.gtk.WidgetFlags(3), CONNECTED,
            new org.gnu.gtk.WidgetFlags(5), new org.gnu.gtk.WidgetFlags(6),
            new org.gnu.gtk.WidgetFlags(7), CONSTRUCTED,
            new org.gnu.gtk.WidgetFlags(9), new org.gnu.gtk.WidgetFlags(10),
            new org.gnu.gtk.WidgetFlags(11), new org.gnu.gtk.WidgetFlags(12),
            new org.gnu.gtk.WidgetFlags(13), new org.gnu.gtk.WidgetFlags(14),
            new org.gnu.gtk.WidgetFlags(15), TOPLEVEL,
            new org.gnu.gtk.WidgetFlags(17), new org.gnu.gtk.WidgetFlags(18),
            new org.gnu.gtk.WidgetFlags(19), new org.gnu.gtk.WidgetFlags(20),
            new org.gnu.gtk.WidgetFlags(21), new org.gnu.gtk.WidgetFlags(22),
            new org.gnu.gtk.WidgetFlags(23), new org.gnu.gtk.WidgetFlags(24),
            new org.gnu.gtk.WidgetFlags(25), new org.gnu.gtk.WidgetFlags(26),
            new org.gnu.gtk.WidgetFlags(27), new org.gnu.gtk.WidgetFlags(28),
            new org.gnu.gtk.WidgetFlags(29), new org.gnu.gtk.WidgetFlags(30),
            new org.gnu.gtk.WidgetFlags(31), NO_WINDOW,
            new org.gnu.gtk.WidgetFlags(33), new org.gnu.gtk.WidgetFlags(34),
            new org.gnu.gtk.WidgetFlags(35), new org.gnu.gtk.WidgetFlags(36),
            new org.gnu.gtk.WidgetFlags(37), new org.gnu.gtk.WidgetFlags(38),
            new org.gnu.gtk.WidgetFlags(39), new org.gnu.gtk.WidgetFlags(40),
            new org.gnu.gtk.WidgetFlags(41), new org.gnu.gtk.WidgetFlags(42),
            new org.gnu.gtk.WidgetFlags(43), new org.gnu.gtk.WidgetFlags(44),
            new org.gnu.gtk.WidgetFlags(45), new org.gnu.gtk.WidgetFlags(46),
            new org.gnu.gtk.WidgetFlags(47), new org.gnu.gtk.WidgetFlags(48),
            new org.gnu.gtk.WidgetFlags(49), new org.gnu.gtk.WidgetFlags(50),
            new org.gnu.gtk.WidgetFlags(51), new org.gnu.gtk.WidgetFlags(52),
            new org.gnu.gtk.WidgetFlags(53), new org.gnu.gtk.WidgetFlags(54),
            new org.gnu.gtk.WidgetFlags(55), new org.gnu.gtk.WidgetFlags(56),
            new org.gnu.gtk.WidgetFlags(57), new org.gnu.gtk.WidgetFlags(58),
            new org.gnu.gtk.WidgetFlags(59), new org.gnu.gtk.WidgetFlags(60),
            new org.gnu.gtk.WidgetFlags(61), new org.gnu.gtk.WidgetFlags(62),
            new org.gnu.gtk.WidgetFlags(63), REALIZED,
            new org.gnu.gtk.WidgetFlags(65), new org.gnu.gtk.WidgetFlags(66),
            new org.gnu.gtk.WidgetFlags(67), new org.gnu.gtk.WidgetFlags(68),
            new org.gnu.gtk.WidgetFlags(69), new org.gnu.gtk.WidgetFlags(70),
            new org.gnu.gtk.WidgetFlags(71), new org.gnu.gtk.WidgetFlags(72),
            new org.gnu.gtk.WidgetFlags(73), new org.gnu.gtk.WidgetFlags(74),
            new org.gnu.gtk.WidgetFlags(75), new org.gnu.gtk.WidgetFlags(76),
            new org.gnu.gtk.WidgetFlags(77), new org.gnu.gtk.WidgetFlags(78),
            new org.gnu.gtk.WidgetFlags(79), new org.gnu.gtk.WidgetFlags(80),
            new org.gnu.gtk.WidgetFlags(81), new org.gnu.gtk.WidgetFlags(82),
            new org.gnu.gtk.WidgetFlags(83), new org.gnu.gtk.WidgetFlags(84),
            new org.gnu.gtk.WidgetFlags(85), new org.gnu.gtk.WidgetFlags(86),
            new org.gnu.gtk.WidgetFlags(87), new org.gnu.gtk.WidgetFlags(88),
            new org.gnu.gtk.WidgetFlags(89), new org.gnu.gtk.WidgetFlags(90),
            new org.gnu.gtk.WidgetFlags(91), new org.gnu.gtk.WidgetFlags(92),
            new org.gnu.gtk.WidgetFlags(93), new org.gnu.gtk.WidgetFlags(94),
            new org.gnu.gtk.WidgetFlags(95), new org.gnu.gtk.WidgetFlags(96),
            new org.gnu.gtk.WidgetFlags(97), new org.gnu.gtk.WidgetFlags(98),
            new org.gnu.gtk.WidgetFlags(99), new org.gnu.gtk.WidgetFlags(100),
            new org.gnu.gtk.WidgetFlags(101), new org.gnu.gtk.WidgetFlags(102),
            new org.gnu.gtk.WidgetFlags(103), new org.gnu.gtk.WidgetFlags(104),
            new org.gnu.gtk.WidgetFlags(105), new org.gnu.gtk.WidgetFlags(106),
            new org.gnu.gtk.WidgetFlags(107), new org.gnu.gtk.WidgetFlags(108),
            new org.gnu.gtk.WidgetFlags(109), new org.gnu.gtk.WidgetFlags(110),
            new org.gnu.gtk.WidgetFlags(111), new org.gnu.gtk.WidgetFlags(112),
            new org.gnu.gtk.WidgetFlags(113), new org.gnu.gtk.WidgetFlags(114),
            new org.gnu.gtk.WidgetFlags(115), new org.gnu.gtk.WidgetFlags(116),
            new org.gnu.gtk.WidgetFlags(117), new org.gnu.gtk.WidgetFlags(118),
            new org.gnu.gtk.WidgetFlags(119), new org.gnu.gtk.WidgetFlags(120),
            new org.gnu.gtk.WidgetFlags(121), new org.gnu.gtk.WidgetFlags(122),
            new org.gnu.gtk.WidgetFlags(123), new org.gnu.gtk.WidgetFlags(124),
            new org.gnu.gtk.WidgetFlags(125), new org.gnu.gtk.WidgetFlags(126),
            new org.gnu.gtk.WidgetFlags(127), MAPPED,
            new org.gnu.gtk.WidgetFlags(129), new org.gnu.gtk.WidgetFlags(130),
            new org.gnu.gtk.WidgetFlags(131), new org.gnu.gtk.WidgetFlags(132),
            new org.gnu.gtk.WidgetFlags(133), new org.gnu.gtk.WidgetFlags(134),
            new org.gnu.gtk.WidgetFlags(135), new org.gnu.gtk.WidgetFlags(136),
            new org.gnu.gtk.WidgetFlags(137), new org.gnu.gtk.WidgetFlags(138),
            new org.gnu.gtk.WidgetFlags(139), new org.gnu.gtk.WidgetFlags(140),
            new org.gnu.gtk.WidgetFlags(141), new org.gnu.gtk.WidgetFlags(142),
            new org.gnu.gtk.WidgetFlags(143), new org.gnu.gtk.WidgetFlags(144),
            new org.gnu.gtk.WidgetFlags(145), new org.gnu.gtk.WidgetFlags(146),
            new org.gnu.gtk.WidgetFlags(147), new org.gnu.gtk.WidgetFlags(148),
            new org.gnu.gtk.WidgetFlags(149), new org.gnu.gtk.WidgetFlags(150),
            new org.gnu.gtk.WidgetFlags(151), new org.gnu.gtk.WidgetFlags(152),
            new org.gnu.gtk.WidgetFlags(153), new org.gnu.gtk.WidgetFlags(154),
            new org.gnu.gtk.WidgetFlags(155), new org.gnu.gtk.WidgetFlags(156),
            new org.gnu.gtk.WidgetFlags(157), new org.gnu.gtk.WidgetFlags(158),
            new org.gnu.gtk.WidgetFlags(159), new org.gnu.gtk.WidgetFlags(160),
            new org.gnu.gtk.WidgetFlags(161), new org.gnu.gtk.WidgetFlags(162),
            new org.gnu.gtk.WidgetFlags(163), new org.gnu.gtk.WidgetFlags(164),
            new org.gnu.gtk.WidgetFlags(165), new org.gnu.gtk.WidgetFlags(166),
            new org.gnu.gtk.WidgetFlags(167), new org.gnu.gtk.WidgetFlags(168),
            new org.gnu.gtk.WidgetFlags(169), new org.gnu.gtk.WidgetFlags(170),
            new org.gnu.gtk.WidgetFlags(171), new org.gnu.gtk.WidgetFlags(172),
            new org.gnu.gtk.WidgetFlags(173), new org.gnu.gtk.WidgetFlags(174),
            new org.gnu.gtk.WidgetFlags(175), new org.gnu.gtk.WidgetFlags(176),
            new org.gnu.gtk.WidgetFlags(177), new org.gnu.gtk.WidgetFlags(178),
            new org.gnu.gtk.WidgetFlags(179), new org.gnu.gtk.WidgetFlags(180),
            new org.gnu.gtk.WidgetFlags(181), new org.gnu.gtk.WidgetFlags(182),
            new org.gnu.gtk.WidgetFlags(183), new org.gnu.gtk.WidgetFlags(184),
            new org.gnu.gtk.WidgetFlags(185), new org.gnu.gtk.WidgetFlags(186),
            new org.gnu.gtk.WidgetFlags(187), new org.gnu.gtk.WidgetFlags(188),
            new org.gnu.gtk.WidgetFlags(189), new org.gnu.gtk.WidgetFlags(190),
            new org.gnu.gtk.WidgetFlags(191), new org.gnu.gtk.WidgetFlags(192),
            new org.gnu.gtk.WidgetFlags(193), new org.gnu.gtk.WidgetFlags(194),
            new org.gnu.gtk.WidgetFlags(195), new org.gnu.gtk.WidgetFlags(196),
            new org.gnu.gtk.WidgetFlags(197), new org.gnu.gtk.WidgetFlags(198),
            new org.gnu.gtk.WidgetFlags(199), new org.gnu.gtk.WidgetFlags(200),
            new org.gnu.gtk.WidgetFlags(201), new org.gnu.gtk.WidgetFlags(202),
            new org.gnu.gtk.WidgetFlags(203), new org.gnu.gtk.WidgetFlags(204),
            new org.gnu.gtk.WidgetFlags(205), new org.gnu.gtk.WidgetFlags(206),
            new org.gnu.gtk.WidgetFlags(207), new org.gnu.gtk.WidgetFlags(208),
            new org.gnu.gtk.WidgetFlags(209), new org.gnu.gtk.WidgetFlags(210),
            new org.gnu.gtk.WidgetFlags(211), new org.gnu.gtk.WidgetFlags(212),
            new org.gnu.gtk.WidgetFlags(213), new org.gnu.gtk.WidgetFlags(214),
            new org.gnu.gtk.WidgetFlags(215), new org.gnu.gtk.WidgetFlags(216),
            new org.gnu.gtk.WidgetFlags(217), new org.gnu.gtk.WidgetFlags(218),
            new org.gnu.gtk.WidgetFlags(219), new org.gnu.gtk.WidgetFlags(220),
            new org.gnu.gtk.WidgetFlags(221), new org.gnu.gtk.WidgetFlags(222),
            new org.gnu.gtk.WidgetFlags(223), new org.gnu.gtk.WidgetFlags(224),
            new org.gnu.gtk.WidgetFlags(225), new org.gnu.gtk.WidgetFlags(226),
            new org.gnu.gtk.WidgetFlags(227), new org.gnu.gtk.WidgetFlags(228),
            new org.gnu.gtk.WidgetFlags(229), new org.gnu.gtk.WidgetFlags(230),
            new org.gnu.gtk.WidgetFlags(231), new org.gnu.gtk.WidgetFlags(232),
            new org.gnu.gtk.WidgetFlags(233), new org.gnu.gtk.WidgetFlags(234),
            new org.gnu.gtk.WidgetFlags(235), new org.gnu.gtk.WidgetFlags(236),
            new org.gnu.gtk.WidgetFlags(237), new org.gnu.gtk.WidgetFlags(238),
            new org.gnu.gtk.WidgetFlags(239), new org.gnu.gtk.WidgetFlags(240),
            new org.gnu.gtk.WidgetFlags(241), new org.gnu.gtk.WidgetFlags(242),
            new org.gnu.gtk.WidgetFlags(243), new org.gnu.gtk.WidgetFlags(244),
            new org.gnu.gtk.WidgetFlags(245), new org.gnu.gtk.WidgetFlags(246),
            new org.gnu.gtk.WidgetFlags(247), new org.gnu.gtk.WidgetFlags(248),
            new org.gnu.gtk.WidgetFlags(249), new org.gnu.gtk.WidgetFlags(250),
            new org.gnu.gtk.WidgetFlags(251), new org.gnu.gtk.WidgetFlags(252),
            new org.gnu.gtk.WidgetFlags(253), new org.gnu.gtk.WidgetFlags(254),
            new org.gnu.gtk.WidgetFlags(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.WidgetFlags theSacrificialOne = new org.gnu.gtk.WidgetFlags(
            0);

    static public org.gnu.gtk.WidgetFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.WidgetFlags already = (org.gnu.gtk.WidgetFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.WidgetFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WidgetFlags(int value) {
        value_ = value;
    }

    public org.gnu.gtk.WidgetFlags or(org.gnu.gtk.WidgetFlags other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.WidgetFlags and(org.gnu.gtk.WidgetFlags other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.WidgetFlags xor(org.gnu.gtk.WidgetFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.WidgetFlags other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
