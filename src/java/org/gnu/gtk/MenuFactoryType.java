/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class MenuFactoryType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _MENU = 0;

    static final public org.gnu.gtk.MenuFactoryType MENU = new org.gnu.gtk.MenuFactoryType(
            _MENU);

    static final private int _MENU_BAR = 1;

    static final public org.gnu.gtk.MenuFactoryType MENU_BAR = new org.gnu.gtk.MenuFactoryType(
            _MENU_BAR);

    static final private int _OPTION_MENU = 2;

    static final public org.gnu.gtk.MenuFactoryType OPTION_MENU = new org.gnu.gtk.MenuFactoryType(
            _OPTION_MENU);

    static final private org.gnu.gtk.MenuFactoryType[] theInterned = new org.gnu.gtk.MenuFactoryType[] {
            MENU, MENU_BAR, OPTION_MENU }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.MenuFactoryType theSacrificialOne = new org.gnu.gtk.MenuFactoryType(
            0);

    static public org.gnu.gtk.MenuFactoryType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.MenuFactoryType already = (org.gnu.gtk.MenuFactoryType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.MenuFactoryType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private MenuFactoryType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.MenuFactoryType or(org.gnu.gtk.MenuFactoryType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.MenuFactoryType and(org.gnu.gtk.MenuFactoryType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.MenuFactoryType xor(org.gnu.gtk.MenuFactoryType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.MenuFactoryType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
