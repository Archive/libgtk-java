/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A CellRenderer is an object used to render data in a {@link TreeView} widget.
 * This is for all types of Table, Tree and list. Each renderer has one or more
 * {@link CellRendererAttribute}. These attributes are for data to be rendered.
 * This data is usually stored in a {@link TreeModel} object; the two current
 * implementations of {@link TreeModel} being {@link ListStore} (for tables and
 * lists) and {@link TreeStore} for trees.
 * <p>
 * The choice of data to be rendered is particular to the TreeView widget - any
 * number of TreeView widgets may share the same data and choose to display only
 * a selection of it. Data is displayed in a {@link TreeView} widget by
 * attaching {@link TreeViewColumn}s and packing any number of {@link
 * CellRenderer}s into each column. Links are then made between the renderers
 * and the data in the model via the {@link
 * TreeViewColumn#addAttributeMapping(CellRenderer, CellRendererAttribute,
 * DataColumn)} method.
 * 
 * <p>
 * This class is a base class for the renderers, either home grown or any of the
 * following standard renderers:
 * <ul>
 * <li>{@link CellRendererPixbuf}
 * <li>{@link CellRendererText}
 * <li>{@link CellRendererToggle}
 * </ul>
 * 
 * <p>
 * For a full overview of the tree and list objects, see the {@link TreeView}
 * description.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.CellRenderer</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class CellRenderer extends GtkObject {
    CellRenderer(Handle hndl) {
        super(hndl);
    }

    static CellRenderer getCellRenderer(Handle handle) {
        if (handle == null)
            return null;

        CellRenderer obj = (CellRenderer) GObject.getGObjectFromHandle(handle);

        if (obj == null)
            obj = new CellRenderer(handle);

        return obj;
    }

    /**
     * Attributes which may be used to connect to data in a {@link TreeModel}
     * via methods in the {@link TreeViewColumn} object.
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static class Attribute extends CellRendererAttribute {
        protected Attribute(String attrib) {
            super(attrib);
        }
        
        /** (String) Cell background color as a string. */
        public static final Attribute CELL_BACKGROUND     = new Attribute("cell-background");
        
        /** (gdk.color) Cell background color as a gdk.Color.*/
        public static final Attribute CELL_BACKGROUND_GDK = new Attribute("cell-background-gdk");

        /** (boolean) Whether this tag affects the cell background color. */
        public static final Attribute CELL_BACKGROUND_SET = new Attribute("cell-background-set"); 

        /**
         * (int)
         * The fixed height.
         * Allowed values: >= -1
         * Default value: -1
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute HEIGHT              = new Attribute("height");              

        /**
         * (boolean)
         * Row is an expander row, and is expanded.
         * Default value: FALSE
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute IS_EXPANDED         = new Attribute( "is-expanded");        
       
        /**
         * (boolean)
         * Row is an expander row, and is expanded.
         * Default value: FALSE
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute IS_EXPANDER         = new Attribute("is-expander");         

        /**
         * (CellRendererMode)
         * Editable mode of the CellRenderer.
         * Default value: CellRendererMode.INERT
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute MODE                = new Attribute("mode");               

        /**
         * (boolean)
         * Display the cell sensitive.
         * Default value: TRUE
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute SENSITIVE           = new Attribute("sensitive");           
        
        /**
         * (boolean)
         * Display the cell.
         * Default value: TRUE
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute VISIBLE             = new Attribute("visible");             
         
        /**
         * (int)
         * The fixed width.
         * Allowed values: >= -1
         * Default value: -1
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute WIDTH               = new Attribute("width");               

        /**
         * (double)
         * The x-align.
         * Allowed values: [0,1]
         * Default value: 0.5
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute XALIGN              = new Attribute("xalign");              
        
        /**
         * (int)
         * The xpad.
         * Default value: 0
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute XPAD                = new Attribute("xpad");                

	/**
	 * (double)
	 * The y-align.
	 * Allowed values: [0,1]
	 * Default value: 0.5
	 */
        public static final Attribute YALIGN              = new Attribute("yalign");              

        /**
         * (int)
         * The ypad.
         * Default value: 0
         * @deprecated Superceeded by java-gnome 4.0; a method along these lines
         *             may well exist in the new bindings, but if it does it likely
         *             has a different name or signature due to the shift to an
         *             algorithmic mapping of the underlying native libraries.
         */
        public static final Attribute YPAD                = new Attribute("ypad");                
    }
    
    /**
     * fixme: to allow users to create cellrenderers, something will have to be
     * done here. Anyone interested?
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_cell_renderer_get_type());
    }

    native static final protected int gtk_cell_renderer_get_type();

    native static final protected void gtk_cell_renderer_get_size(Handle cell,
            Handle widget, Handle cellArea, int[] xOffset, int[] yOffset,
            int[] width, int[] height);

    native static final protected void gtk_cell_renderer_render(Handle cell,
            Handle window, Handle widget, Handle backgroundArea,
            Handle cellArea, Handle exposedArea, int flags);

    native static final protected boolean gtk_cell_renderer_activate(
            Handle cell, Handle event, Handle widget, String path,
            Handle backgroundArea, Handle cellArea, int flags);

    native static final protected Handle gtk_cell_renderer_start_editing(
            Handle cell, Handle event, Handle widget, String path,
            Handle backgroundArea, Handle cellArea, int flags);

    native static final protected void gtk_cell_renderer_set_fixed_size(
            Handle cell, int width, int height);

    native static final protected void gtk_cell_renderer_get_fixed_size(
            Handle cell, int[] width, int[] height);

    native static final protected void gtk_cell_renderer_stop_editing(
            Handle cell, boolean canceled);

    /*
     * Deprecated method. native static final private void
     * gtk_cell_renderer_editing_canceled(Handle cell);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
