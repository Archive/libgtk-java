/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Word wrapping mode for {@link TextBuffer} objects.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.WrapMode</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */

public class WrapMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gtk.WrapMode NONE = new org.gnu.gtk.WrapMode(
            _NONE);

    static final private int _CHAR = 1;

    static final public org.gnu.gtk.WrapMode CHAR = new org.gnu.gtk.WrapMode(
            _CHAR);

    static final private int _WORD = 2;

    static final public org.gnu.gtk.WrapMode WORD = new org.gnu.gtk.WrapMode(
            _WORD);

    static final private org.gnu.gtk.WrapMode[] theInterned = new org.gnu.gtk.WrapMode[] {
            NONE, CHAR, WORD }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.WrapMode theSacrificialOne = new org.gnu.gtk.WrapMode(
            0);

    static public org.gnu.gtk.WrapMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.WrapMode already = (org.gnu.gtk.WrapMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.WrapMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WrapMode(int value) {
        value_ = value;
    }

    public org.gnu.gtk.WrapMode or(org.gnu.gtk.WrapMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.WrapMode and(org.gnu.gtk.WrapMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.WrapMode xor(org.gnu.gtk.WrapMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.WrapMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
