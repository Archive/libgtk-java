/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class RcTokenType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _INVALID = 0;

    static final public org.gnu.gtk.RcTokenType INVALID = new org.gnu.gtk.RcTokenType(
            _INVALID);

    static final private int _INCLUDE = 1;

    static final public org.gnu.gtk.RcTokenType INCLUDE = new org.gnu.gtk.RcTokenType(
            _INCLUDE);

    static final private int _NORMAL = 2;

    static final public org.gnu.gtk.RcTokenType NORMAL = new org.gnu.gtk.RcTokenType(
            _NORMAL);

    static final private int _ACTIVE = 3;

    static final public org.gnu.gtk.RcTokenType ACTIVE = new org.gnu.gtk.RcTokenType(
            _ACTIVE);

    static final private int _PRELIGHT = 4;

    static final public org.gnu.gtk.RcTokenType PRELIGHT = new org.gnu.gtk.RcTokenType(
            _PRELIGHT);

    static final private int _SELECTED = 5;

    static final public org.gnu.gtk.RcTokenType SELECTED = new org.gnu.gtk.RcTokenType(
            _SELECTED);

    static final private int _INSENSITIVE = 6;

    static final public org.gnu.gtk.RcTokenType INSENSITIVE = new org.gnu.gtk.RcTokenType(
            _INSENSITIVE);

    static final private int _FG = 7;

    static final public org.gnu.gtk.RcTokenType FG = new org.gnu.gtk.RcTokenType(
            _FG);

    static final private int _BG = 8;

    static final public org.gnu.gtk.RcTokenType BG = new org.gnu.gtk.RcTokenType(
            _BG);

    static final private int _TEXT = 9;

    static final public org.gnu.gtk.RcTokenType TEXT = new org.gnu.gtk.RcTokenType(
            _TEXT);

    static final private int _BASE = 10;

    static final public org.gnu.gtk.RcTokenType BASE = new org.gnu.gtk.RcTokenType(
            _BASE);

    static final private int _XTHICKNESS = 11;

    static final public org.gnu.gtk.RcTokenType XTHICKNESS = new org.gnu.gtk.RcTokenType(
            _XTHICKNESS);

    static final private int _YTHICKNESS = 12;

    static final public org.gnu.gtk.RcTokenType YTHICKNESS = new org.gnu.gtk.RcTokenType(
            _YTHICKNESS);

    static final private int _FONT = 13;

    static final public org.gnu.gtk.RcTokenType FONT = new org.gnu.gtk.RcTokenType(
            _FONT);

    static final private int _FONTSET = 14;

    static final public org.gnu.gtk.RcTokenType FONTSET = new org.gnu.gtk.RcTokenType(
            _FONTSET);

    static final private int _FONT_NAME = 15;

    static final public org.gnu.gtk.RcTokenType FONT_NAME = new org.gnu.gtk.RcTokenType(
            _FONT_NAME);

    static final private int _BG_PIXMAP = 16;

    static final public org.gnu.gtk.RcTokenType BG_PIXMAP = new org.gnu.gtk.RcTokenType(
            _BG_PIXMAP);

    static final private int _PIXMAP_PATH = 17;

    static final public org.gnu.gtk.RcTokenType PIXMAP_PATH = new org.gnu.gtk.RcTokenType(
            _PIXMAP_PATH);

    static final private int _STYLE = 18;

    static final public org.gnu.gtk.RcTokenType STYLE = new org.gnu.gtk.RcTokenType(
            _STYLE);

    static final private int _BINDING = 19;

    static final public org.gnu.gtk.RcTokenType BINDING = new org.gnu.gtk.RcTokenType(
            _BINDING);

    static final private int _BIND = 20;

    static final public org.gnu.gtk.RcTokenType BIND = new org.gnu.gtk.RcTokenType(
            _BIND);

    static final private int _WIDGET = 21;

    static final public org.gnu.gtk.RcTokenType WIDGET = new org.gnu.gtk.RcTokenType(
            _WIDGET);

    static final private int _WIDGET_CLASS = 22;

    static final public org.gnu.gtk.RcTokenType WIDGET_CLASS = new org.gnu.gtk.RcTokenType(
            _WIDGET_CLASS);

    static final private int _CLASS = 23;

    static final public org.gnu.gtk.RcTokenType CLASS = new org.gnu.gtk.RcTokenType(
            _CLASS);

    static final private int _LOWEST = 24;

    static final public org.gnu.gtk.RcTokenType LOWEST = new org.gnu.gtk.RcTokenType(
            _LOWEST);

    static final private int _GTK = 25;

    static final public org.gnu.gtk.RcTokenType GTK = new org.gnu.gtk.RcTokenType(
            _GTK);

    static final private int _APPLICATION = 26;

    static final public org.gnu.gtk.RcTokenType APPLICATION = new org.gnu.gtk.RcTokenType(
            _APPLICATION);

    static final private int _THEME = 27;

    static final public org.gnu.gtk.RcTokenType THEME = new org.gnu.gtk.RcTokenType(
            _THEME);

    static final private int _RC = 28;

    static final public org.gnu.gtk.RcTokenType RC = new org.gnu.gtk.RcTokenType(
            _RC);

    static final private int _HIGHEST = 29;

    static final public org.gnu.gtk.RcTokenType HIGHEST = new org.gnu.gtk.RcTokenType(
            _HIGHEST);

    static final private int _ENGINE = 30;

    static final public org.gnu.gtk.RcTokenType ENGINE = new org.gnu.gtk.RcTokenType(
            _ENGINE);

    static final private int _MODULE_PATH = 31;

    static final public org.gnu.gtk.RcTokenType MODULE_PATH = new org.gnu.gtk.RcTokenType(
            _MODULE_PATH);

    static final private int _IM_MODULE_PATH = 32;

    static final public org.gnu.gtk.RcTokenType IM_MODULE_PATH = new org.gnu.gtk.RcTokenType(
            _IM_MODULE_PATH);

    static final private int _IM_MODULE_FILE = 33;

    static final public org.gnu.gtk.RcTokenType IM_MODULE_FILE = new org.gnu.gtk.RcTokenType(
            _IM_MODULE_FILE);

    static final private int _STOCK = 34;

    static final public org.gnu.gtk.RcTokenType STOCK = new org.gnu.gtk.RcTokenType(
            _STOCK);

    static final private int _LTR = 35;

    static final public org.gnu.gtk.RcTokenType LTR = new org.gnu.gtk.RcTokenType(
            _LTR);

    static final private int _RTL = 36;

    static final public org.gnu.gtk.RcTokenType RTL = new org.gnu.gtk.RcTokenType(
            _RTL);

    static final private int _LAST = 37;

    static final public org.gnu.gtk.RcTokenType LAST = new org.gnu.gtk.RcTokenType(
            _LAST);

    static final private org.gnu.gtk.RcTokenType[] theInterned = new org.gnu.gtk.RcTokenType[] {
            INVALID, INCLUDE, NORMAL, ACTIVE, PRELIGHT, SELECTED, INSENSITIVE,
            FG, BG, TEXT, BASE, XTHICKNESS, YTHICKNESS, FONT, FONTSET,
            FONT_NAME, BG_PIXMAP, PIXMAP_PATH, STYLE, BINDING, BIND, WIDGET,
            WIDGET_CLASS, CLASS, LOWEST, GTK, APPLICATION, THEME, RC, HIGHEST,
            ENGINE, MODULE_PATH, IM_MODULE_PATH, IM_MODULE_FILE, STOCK, LTR,
            RTL, LAST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.RcTokenType theSacrificialOne = new org.gnu.gtk.RcTokenType(
            0);

    static public org.gnu.gtk.RcTokenType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.RcTokenType already = (org.gnu.gtk.RcTokenType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.RcTokenType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private RcTokenType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.RcTokenType or(org.gnu.gtk.RcTokenType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.RcTokenType and(org.gnu.gtk.RcTokenType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.RcTokenType xor(org.gnu.gtk.RcTokenType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.RcTokenType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
