/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The FileSelection widget lists all the files and directories in the current
 * working directory and enables the user to select one of the file names. There
 * are a number of navigation controls that enable the user to browse through
 * the entire file system. There also are buttons that create directories,
 * delete files, and rename files.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileSelection</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileSelection extends Dialog {

    /**
     * construct a new FileSelection dialog.
     * 
     * @param title
     *            The string that is displayed in the title bar of the dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileSelection(String title) {
        super(gtk_file_selection_new(title));
    }

    /**
     * Construct a FileSelection using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileSelection(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static FileSelection getFileSelection(Handle handle) {
        if (handle == null)
            return null;

        FileSelection obj = (FileSelection) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FileSelection(handle);

        return obj;
    }

    /**
     * Sets the default path for the file requestor. If filename includes a
     * directory path the requestor will open with that path set as its current
     * working directory.
     * 
     * @param filename
     *            The default path for the widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFilename(String filename) {
        FileSelection.gtk_file_selection_set_filename(getHandle(), filename);
    }

    /**
     * Returns the selected filename.
     * 
     * @return The file that is selected in the dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFilename() {
        return FileSelection.gtk_file_selection_get_filename(getHandle());
    }

    /**
     * Will attempt to match <code>pattern</code> to a valid filename or
     * subdirectory in the current directory. If a match can be made the matched
     * filename will appear in the text entry field in the file selection
     * dialog. If a partial match can be made the "Files" list will contain
     * those file names which have been partially matched and the "Directories"
     * list will list those directories which have been partially matched.
     * 
     * @param pattern
     *            The pattern to use for matching.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void complete(String pattern) {
        FileSelection.gtk_file_selection_complete(getHandle(), pattern);
    }

    /**
     * Shows the file operation buttons, if they have previously been hidden.
     * The rest of the widgets in the dialog will be resized accordingly.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void showFileopButtons() {
        FileSelection.gtk_file_selection_show_fileop_buttons(getHandle());
    }

    /**
     * Hides the file operation buttons that normally appear at the top of the
     * dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void hideFileopButtons() {
        FileSelection.gtk_file_selection_hide_fileop_buttons(getHandle());
    }

    /**
     * Return the OK Button widget for this Dialog.
     * 
     * @return The OK Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getOKButton() {
        Handle hndl = FileSelection.getOkButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Cancel Button widget for this dialog.
     * 
     * @return The Cancel Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getCancelButton() {
        Handle hndl = FileSelection.getCancelButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the Help Button widget for this dialog.
     * 
     * @return The Help Button.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Button getHelpButton() {
        Handle hndl = FileSelection.getHelpButton(getHandle());
        return Button.getButton(hndl);
    }

    /**
     * Return the directory list for this widget
     * 
     * @return The Directory List
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getDirList() {
        Handle hndl = getDirList(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Returns the the file list for this widget
     * 
     * @return The File List
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getFileList() {
        Handle hndl = getFileList(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Returns the selection entry
     * 
     * @return selection entry
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Widget getSelectionEntry() {
        Handle hndl = getSelectionEntry(getHandle());
        return Widget.getWidget(hndl);
    }

    /**
     * Retrieves the list of file selections the user has made in the dialog
     * box. This function is intended for use when the user can select multiple
     * files in the file list.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String[] getSelections() {
        return gtk_file_selection_get_selections(getHandle());
    }

    /**
     * Sets whether the user is allowed to select multiple files in the file
     * list. Use {@link #getSelections} to get the list of selected files.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSelectMultiple(boolean selectMultiple) {
        gtk_file_selection_set_select_multiple(getHandle(), selectMultiple);
    }

    /**
     * Determines whether or not the user is allowed to select multiple files in
     * the file list.
     * 
     * @see #setSelectMultiple
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getSelectMultiple() {
        return gtk_file_selection_get_select_multiple(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_file_selection_get_type());
    }

    native static final protected Handle getDirList(Handle cptr);

    native static final protected Handle getFileList(Handle cptr);

    native static final protected Handle getSelectionEntry(Handle cptr);

    native static final protected Handle getSelectionText(Handle cptr);

    native static final protected Handle getMainVbox(Handle cptr);

    native static final protected Handle getOkButton(Handle cptr);

    native static final protected Handle getCancelButton(Handle cptr);

    native static final protected Handle getHelpButton(Handle cptr);

    native static final protected Handle getHistoryPulldown(Handle cptr);

    native static final protected Handle getHistoryMenu(Handle cptr);

    native static final protected Handle getButtonArea(Handle cptr);

    native static final protected int gtk_file_selection_get_type();

    native static final protected Handle gtk_file_selection_new(String title);

    native static final protected void gtk_file_selection_set_filename(
            Handle filesel, String filename);

    native static final protected String gtk_file_selection_get_filename(
            Handle filesel);

    native static final protected void gtk_file_selection_complete(
            Handle filesel, String pattern);

    native static final protected void gtk_file_selection_show_fileop_buttons(
            Handle filesel);

    native static final protected void gtk_file_selection_hide_fileop_buttons(
            Handle filesel);

    native static final private String[] gtk_file_selection_get_selections(
            Handle filesel);

    native static final private void gtk_file_selection_set_select_multiple(
            Handle filesel, boolean select_multiple);

    native static final private boolean gtk_file_selection_get_select_multiple(
            Handle filesel);

}
