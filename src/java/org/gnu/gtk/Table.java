/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A container that creates a grid of flexible static rectangles that you can
 * use to position and size widgets.
 * <p>
 * The table cells are referenced by rows and columns. The top left of the table
 * is row 0, column 0.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Table</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Table extends Container {
    /**
     * Constructs a new table widget. An initial size must be given by
     * specifying how many rows and columns the table should have, although this
     * can be changed later with the resize method. Rows and columns must both
     * be in the range 0 .. 65535
     * 
     * @param rows
     *            The initial number of rows in the table
     * @param columns
     *            The initial number of columns for the table
     * @param homogenous
     *            If set to TRUE, all table cells are resized to the size of the
     *            cell containing the largest widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Table(int rows, int columns, boolean homogenous) {
        super(gtk_table_new(rows, columns, homogenous));
    }

    /**
     * Construct a new Table from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Table(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Table getTable(Handle handle) {
        if (handle == null) {
            return null;
        }

        Table obj = (Table) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new Table(handle);
        }

        return obj;
    }

    /**
     * Changes the size of the table after it has been created. The parameters
     * define the requested total number of rows/columns after the resize.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void resize(int rows, int columns) {
        gtk_table_resize(getHandle(), rows, columns);
    }

    /**
     * Attaches a child widget to a position in the table
     * 
     * @param child
     *            The widget to add.
     * @param leftAttach
     *            The column number to attach the left side of a child widget
     *            to.
     * @param rightAttach
     *            The column number to attach the right side of a child widget
     *            to.
     * @param topAttach
     *            The row number to attach the top of a child widget to.
     * @param bottomAttach
     *            The row number to attach the bottom of a child widget to.
     * @param xOptions
     *            Used to specify the properties of the child widget when the
     *            table is resized.
     * @param yOptions
     *            The same as xOptions, except this field determines behaviour
     *            of vertical resizing.
     * @param xPadding
     *            An integer value specifying the padding on the left and right
     *            of the widget being added to the table.
     * @param yPadding
     *            The amount of padding above and below the child widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void attach(Widget child, int leftAttach, int rightAttach,
            int topAttach, int bottomAttach, AttachOptions xOptions,
            AttachOptions yOptions, int xPadding, int yPadding) {
        gtk_table_attach(getHandle(), child.getHandle(), leftAttach,
                rightAttach, topAttach, bottomAttach, xOptions.getValue(),
                yOptions.getValue(), xPadding, yPadding);
    }

    /**
     * Attaches a child widget to a position in the table. This is a convenience
     * function provides the programmer with a means to add children to a table
     * with identical padding and expansion options.
     * 
     * @param child
     *            The widget to add.
     * @param leftAttach
     *            The column number to attach the left side of a child widget
     *            to.
     * @param rightAttach
     *            The column number to attach the right side of a child widget
     *            to.
     * @param topAttach
     *            The row number to attach the top of a child widget to.
     * @param bottomAttach
     *            The row number to attach the bottom of a child widget to.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void attach(Widget child, int leftAttach, int rightAttach,
            int topAttach, int bottomAttach) {
        gtk_table_attach_defaults(getHandle(), child.getHandle(), leftAttach,
                rightAttach, topAttach, bottomAttach);
    }

    /**
     * Changes the space between a given table column and its surrounding
     * columns.
     * 
     * @param column
     *            Column number whose spacing will be changed.
     * @param spacing
     *            Number of pixels that the spacing should take up.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColumnSpacing(int column, int spacing) {
        gtk_table_set_col_spacing(getHandle(), column, spacing);
    }

    /**
     * Changes the space between a given table row and its surrounding rows.
     * 
     * @param row
     *            Row number whose spacing will be changed.
     * @param spacing
     *            Number of pixels that the spacing should take up.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRowSpacing(int row, int spacing) {
        gtk_table_set_row_spacing(getHandle(), row, spacing);
    }

    /**
     * Gets the amount of space between row <code>row</code>, and row
     * <code>row + 1</code>.
     * 
     * @param row
     *            A row in the table, 0 indicates the first row.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRowSpacing(int row) {
        return gtk_table_get_row_spacing(getHandle(), row);
    }

    /**
     * Gets the amount of space between column <code>column</code>, and
     * column <code>column + 1</code>.
     * 
     * @param column
     *            A row in the table, 0 indicates the first row.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getColumnSpacing(int column) {
        return gtk_table_get_col_spacing(getHandle(), column);
    }

    /**
     * returns the default spacing between rows. The only way to change this is
     * to use <code>setRowSpacings</code>, which changes the spacing of
     * <em>all</em> cells, regardless of whether they have been set
     * independently.
     * 
     * @see Table#setRowSpacing(int, int)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getDefaultRowSpacing() {
        return gtk_table_get_default_row_spacing(getHandle());
    }

    /**
     * returns the default spacing between columns. The only way to change this
     * is to use <code>setColumnSpacings</code>, which changes the spacing of
     * <em>all</em> cells, regardless of whether they have been set
     * independently.
     * 
     * @see Table#setColumnSpacing(int, int)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getDefaultColumnSpacing() {
        return gtk_table_get_default_col_spacing(getHandle());
    }

    /**
     * Sets the space between <em>every</em> row in the table. This overrides
     * any previous changes to particular cells.
     * 
     * @param spacing
     *            The number of pixels of space to place between every row in
     *            the table.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRowSpacing(int spacing) {
        gtk_table_set_row_spacings(getHandle(), spacing);
    }

    /**
     * Sets the space between <em>every</em> column in the table. This
     * overrides any previous changes to particular cells.
     * 
     * @param spacing
     *            The number of pixels of space to place between every column in
     *            the table.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColumnSpacing(int spacing) {
        gtk_table_set_col_spacings(getHandle(), spacing);
    }

    /**
     * Changes the homogenous property of table cells (ie whether all cells are
     * an equal size or not).
     * 
     * @param homogeneous
     *            Set to TRUE to ensure all table cells are the same size. Set
     *            to FALSE if this is not your desired behaviour.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHomogeneous(boolean homogeneous) {
        gtk_table_set_homogeneous(getHandle(), homogeneous);
    }

    /**
     * Returns whether the table cells are all constrained to the same width and
     * height
     * 
     * @return TRUE if the cells are all constrained to the same size
     * @see Table#setHomogeneous(boolean)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getHomogeneous() {
        return gtk_table_get_homogeneous(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_table_get_type());
    }

    native static final protected int gtk_table_get_type();

    native static final protected Handle gtk_table_new(int rows, int columns,
            boolean homogenous);

    native static final protected void gtk_table_resize(Handle table, int rows,
            int cols);

    native static final protected void gtk_table_attach(Handle table,
            Handle child, int leftAttach, int rightAttach, int topAttach,
            int bottomAttach, int xoptions, int yoptions, int xpadding,
            int ypadding);

    native static final protected void gtk_table_attach_defaults(Handle table,
            Handle child, int leftAttach, int rightAttach, int topAttach,
            int bottomAttach);

    native static final protected void gtk_table_set_row_spacing(Handle table,
            int row, int spacing);

    native static final protected int gtk_table_get_row_spacing(Handle table,
            int row);

    native static final protected void gtk_table_set_col_spacing(Handle table,
            int column, int spacing);

    native static final protected int gtk_table_get_col_spacing(Handle table,
            int column);

    native static final protected void gtk_table_set_row_spacings(Handle table,
            int spacing);

    native static final protected int gtk_table_get_default_row_spacing(
            Handle table);

    native static final protected void gtk_table_set_col_spacings(Handle table,
            int spacing);

    native static final protected int gtk_table_get_default_col_spacing(
            Handle table);

    native static final protected void gtk_table_set_homogeneous(Handle table,
            boolean homogeneous);

    native static final protected boolean gtk_table_get_homogeneous(Handle table);
}
