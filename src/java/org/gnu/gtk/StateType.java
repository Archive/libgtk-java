/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class StateType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORMAL = 0;

    static final public org.gnu.gtk.StateType NORMAL = new org.gnu.gtk.StateType(
            _NORMAL);

    static final private int _ACTIVE = 1;

    static final public org.gnu.gtk.StateType ACTIVE = new org.gnu.gtk.StateType(
            _ACTIVE);

    static final private int _PRELIGHT = 2;

    static final public org.gnu.gtk.StateType PRELIGHT = new org.gnu.gtk.StateType(
            _PRELIGHT);

    static final private int _SELECTED = 3;

    static final public org.gnu.gtk.StateType SELECTED = new org.gnu.gtk.StateType(
            _SELECTED);

    static final private int _INSENSITIVE = 4;

    static final public org.gnu.gtk.StateType INSENSITIVE = new org.gnu.gtk.StateType(
            _INSENSITIVE);

    static final private org.gnu.gtk.StateType[] theInterned = new org.gnu.gtk.StateType[] {
            NORMAL, ACTIVE, PRELIGHT, SELECTED, INSENSITIVE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.StateType theSacrificialOne = new org.gnu.gtk.StateType(
            0);

    static public org.gnu.gtk.StateType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.StateType already = (org.gnu.gtk.StateType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.StateType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private StateType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.StateType or(org.gnu.gtk.StateType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.StateType and(org.gnu.gtk.StateType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.StateType xor(org.gnu.gtk.StateType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.StateType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
