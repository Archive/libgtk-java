/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

public class CellRendererState extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _SELECTED = 1 << 0;

    static final public org.gnu.gtk.CellRendererState SELECTED = new org.gnu.gtk.CellRendererState(
            _SELECTED);

    static final private int _PRELIT = 1 << 1;

    static final public org.gnu.gtk.CellRendererState PRELIT = new org.gnu.gtk.CellRendererState(
            _PRELIT);

    static final private int _INSENSITIVE = 1 << 2;

    static final public org.gnu.gtk.CellRendererState INSENSITIVE = new org.gnu.gtk.CellRendererState(
            _INSENSITIVE);

    static final private int _SORTED = 1 << 3;

    static final public org.gnu.gtk.CellRendererState SORTED = new org.gnu.gtk.CellRendererState(
            _SORTED);

    static final private org.gnu.gtk.CellRendererState[] theInterned = new org.gnu.gtk.CellRendererState[] {
            new org.gnu.gtk.CellRendererState(0), SELECTED, PRELIT,
            new org.gnu.gtk.CellRendererState(3), INSENSITIVE,
            new org.gnu.gtk.CellRendererState(5),
            new org.gnu.gtk.CellRendererState(6),
            new org.gnu.gtk.CellRendererState(7), SORTED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.CellRendererState theSacrificialOne = new org.gnu.gtk.CellRendererState(
            0);

    static public org.gnu.gtk.CellRendererState intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.CellRendererState already = (org.gnu.gtk.CellRendererState) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.CellRendererState(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CellRendererState(int value) {
        value_ = value;
    }

    public org.gnu.gtk.CellRendererState or(org.gnu.gtk.CellRendererState other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.CellRendererState and(org.gnu.gtk.CellRendererState other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.CellRendererState xor(org.gnu.gtk.CellRendererState other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.CellRendererState other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
