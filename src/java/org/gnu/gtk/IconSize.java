/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;
import org.gnu.glib.Handle;

/**
 * Defines the standard GTK icon sizes.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconSize</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconSize extends Enum {
    static final private int _INVALID = 0;

    static final public IconSize INVALID = new IconSize(_INVALID);

    static final private int _MENU = 1;

    static final public IconSize MENU = new IconSize(_MENU);

    static final private int _SMALL_TOOLBAR = 2;

    static final public IconSize SMALL_TOOLBAR = new IconSize(_SMALL_TOOLBAR);

    static final private int _LARGE_TOOLBAR = 3;

    static final public IconSize LARGE_TOOLBAR = new IconSize(_LARGE_TOOLBAR);

    static final private int _BUTTON = 4;

    static final public IconSize BUTTON = new IconSize(_BUTTON);

    static final private int _DND = 5;

    static final public IconSize DND = new IconSize(_DND);

    static final private int _DIALOG = 6;

    static final public IconSize DIALOG = new IconSize(_DIALOG);

    static final private IconSize[] theInterned = new IconSize[] { INVALID,
            MENU, SMALL_TOOLBAR, LARGE_TOOLBAR, BUTTON, DND, DIALOG }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private IconSize theSacrificialOne = new IconSize(0);

    static public IconSize intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        IconSize already = (IconSize) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new IconSize(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private IconSize(int value) {
        value_ = value;
    }

    public IconSize or(IconSize other) {
        return intern(value_ | other.value_);
    }

    public IconSize and(IconSize other) {
        return intern(value_ & other.value_);
    }

    public IconSize xor(IconSize other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(IconSize other) {
        return (value_ & other.value_) == other.value_;
    }

    native static final private boolean gtk_icon_size_lookup(int size,
            int[] width, int[] height);

    native static final private boolean gtk_icon_size_lookup_for_settings(
            Handle settings, int size, int[] width, int[] height);

    native static final private int gtk_icon_size_register(String name,
            int width, int height);

    native static final private void gtk_icon_size_register_alias(String alias,
            int target);

    native static final private int gtk_icon_size_from_name(String name);

    native static final private String gtk_icon_size_get_name(int size);

}
