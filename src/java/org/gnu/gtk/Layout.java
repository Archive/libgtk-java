/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This widget is similar to DrawingArea in that it's a "blank slate" and
 * doesn't do anything but paint a blank background by default. It's different
 * in that it supports scrolling natively and it contains child widgets since it
 * is a Container.
 * 
 * @see Container
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Layout</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Layout extends Container {

    /**
     * Construct a new Layout object.
     * 
     * @param hAdj
     *            The horizontal Adjustment object.
     * @param vAdj
     *            The vertical Adjustment object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Layout(Adjustment hAdj, Adjustment vAdj) {
        super(Layout.gtk_layout_new(hAdj.getHandle(), vAdj.getHandle()));
    }

    /**
     * Construct a new Layout from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Layout(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new Layout from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Layout getLayout(Handle handle) {
        if (handle == null)
            return null;

        Layout obj = (Layout) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Layout(handle);

        return obj;
    }

    /**
     * Add a child Widget to this Layout.
     * 
     * @param child
     *            The child Widget to add to this Layout.
     * @param x
     *            The X coordinate to position the child.
     * @param y
     *            The Y coordinate to position the child.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addChild(Widget child, int x, int y) {
        Layout.gtk_layout_put(getHandle(), child.getHandle(), x, y);
    }

    /**
     * Move a child Widget to a new position
     * 
     * @param child
     *            The child Widget to move.
     * @param x
     *            The X coordinate for the new position.
     * @param y
     *            The Y coordinate for the new position.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveChild(Widget child, int x, int y) {
        Layout.gtk_layout_move(getHandle(), child.getHandle(), x, y);
    }

    /**
     * Set the size of the scrollable area of the layout.
     * 
     * @param width
     *            The width for the new size.
     * @param height
     *            The height for the new size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(int width, int height) {
        Layout.gtk_layout_set_size(getHandle(), width, height);
    }

    /**
     * Retrieve the size of the scrollabel area of the layout.
     * 
     * @return The size of the scrollabel area.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Requisition getSize() {
        int[] width = new int[1];
        int[] height = new int[1];
        Layout.gtk_layout_get_size(getHandle(), width, height);
        return new Requisition(width[0], height[0]);
    }

    /**
     * Return the Horizontal Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getHorizontalAdjustment() {
        Handle han = Layout.gtk_layout_get_hadjustment(getHandle());
        return Adjustment.getAdjustment(han);
    }

    /**
     * Set the Horizontal Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHorizontalAdjustment(Adjustment adjust) {
        Layout.gtk_layout_set_hadjustment(getHandle(), adjust.getHandle());
    }

    /**
     * Return the Vertical Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Adjustment getVerticalAdjustment() {
        Handle han = Layout.gtk_layout_get_vadjustment(getHandle());
        return Adjustment.getAdjustment(han);
    }

    /**
     * Set the Vertical Adjustment.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVerticalAdjustment(Adjustment adjust) {
        Layout.gtk_layout_set_vadjustment(getHandle(), adjust.getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_layout_get_type());
    }

    /**
      * Return the Layout's bin window, into which content is drawn.
      * @deprecated Superceeded by java-gnome 4.0; a method along these lines
      *             may well exist in the new bindings, but if it does it likely
      *             has a different name or signature due to the shift to an
      *             algorithmic mapping of the underlying native libraries.
      */
    public org.gnu.gdk.Window getBinWindow() {
        return org.gnu.gdk.Window.getWindowFromHandle(
                gtk_layout_get_bin_window(getHandle()));
    }

    native static final protected int gtk_layout_get_type();

    native static final protected Handle gtk_layout_new(Handle hadjustment,
            Handle vadjustment);

    native static final protected void gtk_layout_put(Handle layout,
            Handle childWidget, int x, int y);

    native static final protected void gtk_layout_move(Handle layout,
            Handle childWidget, int x, int y);

    native static final protected void gtk_layout_set_size(Handle layout,
            int width, int height);

    native static final protected void gtk_layout_get_size(Handle layout,
            int[] width, int[] height);

    native static final protected Handle gtk_layout_get_hadjustment(
            Handle layout);

    native static final protected Handle gtk_layout_get_vadjustment(
            Handle layout);

    native static final protected void gtk_layout_set_hadjustment(
            Handle layout, Handle adjustment);

    native static final protected void gtk_layout_set_vadjustment(
            Handle layout, Handle adjustment);

    native static final protected Handle gtk_layout_get_bin_window(
            Handle layout);

    /*
     * Deprecated functions. native static final private void
     * gtk_layout_freeze(Handle layout); native static final private void
     * gtk_layout_thaw(Handle layout);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
