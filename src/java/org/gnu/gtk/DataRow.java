/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * A DataRow is simply a list of data, which represent a row that can be added
 * to store components, such as {@link org.gnu.gtk.ListStore} and
 * {@link org.gnu.gtk.TreeStore}.
 * </p>
 * <p>
 * To create a DataRow and add it to a store is pretty straight forward:
 * </p>
 * <p>
 * <code>store.addRow(new DataRow() .add(name) .add(type) .add(icon));</code>
 * </p>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.DataRow</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class DataRow {

    private List items = new ArrayList();

    /**
     * Get the data at column <code>aCol</code>
     * 
     * @param aCol
     *            the column number, starting from 0.
     * @return the data at that column
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object get(int aCol) {
        return items.get(aCol);
    }

    /**
     * Get all data in this row.
     * 
     * @return a List, with the data of each row.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public List getAll() {
        return items;
    }

    /**
     * Adds a data to this row. This will fill the columns of the row
     * sequentially: the first call is the first column, and so it goes.
     * 
     * @param aData
     *            a data to be added to the row
     * @return this DataRow. The reason why it returns this object is so you can
     *         make useful constructions, such as:
     *         <code>new DataRow() .add(name) .add(type) .add(icon)</code>
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DataRow add(Object aData) {
        items.add(aData);

        return this;
    }
}
