/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

/**
 * When the top-level widget has determined how much space its child would 
 * like to have, the second phase of the size negotiation, size allocation, 
 * begins. 
 * A child's size allocation is represented by an Allocation object. 
 * This class contains not only a width and height, but also a position 
 * (i.e. X and Y coordinates), so that containers can tell their children 
 * not only how much space they have gotten, but also where they are 
 * positioned inside the space available to the container.
 *
 * @author Sando Bodo-Merle
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Allocation</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public final class Allocation
{
	private final int x, y, width, height;
	
	/**
	 * Construct a new Allocation.
	 * 
	 * @param x the X position of the widget's area relative to its parents 
	 * allocation.
	 * @param y the Y position of the widget's area relative to its parents
	 * allocation.
	 * @param width the width of the widget's allocated area.
	 * @param height the height of the widget's allocated area.
	 * 
	 * @see org.gnu.gtk.Widget#getAllocation() 
	 */
	public Allocation(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * @return the X position of the widget's area relative to its parents 
	 * allocation.
	 */
	public final int getX() {
		return x;
	}

	/**
	 * @return the Y position of the widget's area relative to its parents 
	 * allocation.
	 */
	public final int getY() {
		return y;
	}
	
	/**
	 * @return the width of the widget's allocated area.
	 */
	public final int getWidth() {
		return width;
	}
	
	/**
	 * @return the height of the widget's allocated area.
	 */
	public final int getHeight() {
		return height;
	}
}

