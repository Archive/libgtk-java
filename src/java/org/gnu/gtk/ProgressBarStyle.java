/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class ProgressBarStyle extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _CONTINUOUS = 0;

    static final public org.gnu.gtk.ProgressBarStyle CONTINUOUS = new org.gnu.gtk.ProgressBarStyle(
            _CONTINUOUS);

    static final private int _DISCRETE = 1;

    static final public org.gnu.gtk.ProgressBarStyle DISCRETE = new org.gnu.gtk.ProgressBarStyle(
            _DISCRETE);

    static final private org.gnu.gtk.ProgressBarStyle[] theInterned = new org.gnu.gtk.ProgressBarStyle[] {
            CONTINUOUS, DISCRETE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.ProgressBarStyle theSacrificialOne = new org.gnu.gtk.ProgressBarStyle(
            0);

    static public org.gnu.gtk.ProgressBarStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.ProgressBarStyle already = (org.gnu.gtk.ProgressBarStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.ProgressBarStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ProgressBarStyle(int value) {
        value_ = value;
    }

    public org.gnu.gtk.ProgressBarStyle or(org.gnu.gtk.ProgressBarStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.ProgressBarStyle and(org.gnu.gtk.ProgressBarStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.ProgressBarStyle xor(org.gnu.gtk.ProgressBarStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.ProgressBarStyle other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
