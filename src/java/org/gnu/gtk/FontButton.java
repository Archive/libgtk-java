/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.gtk.event.FontButtonEvent;
import org.gnu.gtk.event.FontButtonListener;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FontButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FontButton extends Button {

    // for libglade
    public FontButton(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static FontButton getFontButton(Handle handle) {
        if (handle == null)
            return null;

        FontButton obj = (FontButton) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FontButton(handle);

        return obj;
    }

    /**
     * Construct a new FontButton widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontButton() {
        super(gtk_font_button_new());
    }

    /**
     * Create a FontButton widget initialized with the provided font.
     * 
     * @param fontName
     *            Name of the font to display in the font selection dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontButton(String fontName) {
        super(gtk_font_button_new_with_font(fontName));
    }

    /**
     * Set the title for the font selection dialog.
     * 
     * @param title
     *            The title for the dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gtk_font_button_set_title(getHandle(), title);
    }

    /**
     * Get the title for the font selection dialog.
     * 
     * @return The title for the font selection dialog.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getTitle() {
        return gtk_font_button_get_title(getHandle());
    }

    /**
     * Set whether the selected font is used in the label.
     * 
     * @param useFont
     *            If true the font name will be written using the chosen font.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseFont(boolean useFont) {
        gtk_font_button_set_use_font(getHandle(), useFont);
    }

    /**
     * Returns whether the selected font is used in the label.
     * 
     * @return true if the selected font is used in the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getUseFont() {
        return gtk_font_button_get_use_font(getHandle());
    }

    /**
     * Set whether the selected size is used in the label.
     * 
     * @param useSize
     *            If true the font name will be written using the chosen font
     *            size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseSize(boolean useSize) {
        gtk_font_button_set_use_size(getHandle(), useSize);
    }

    /**
     * Returns whether the selected size is used in the label.
     * 
     * @return true if the selected size is used in the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getUseSize() {
        return gtk_font_button_get_use_size(getHandle());
    }

    /**
     * Sets whether the name of the font style will be shown in the label.
     * 
     * @param showStyle
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShowStyle(boolean showStyle) {
        gtk_font_button_set_show_style(getHandle(), showStyle);
    }

    /**
     * Returns whether the name of the font style will be shown in the label.
     * 
     * @return true if the font style will be shown in the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getShowStyle() {
        return gtk_font_button_get_show_style(getHandle());
    }

    /**
     * Sets whether the size of the font style will be shown in the label.
     * 
     * @param showSize
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setShowSize(boolean showSize) {
        gtk_font_button_set_show_size(getHandle(), showSize);
    }

    /**
     * Returns whether the name of the font style will be shown in the label.
     * 
     * @return true if the font style will be shown in the label.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getShowSize() {
        return gtk_font_button_get_show_size(getHandle());
    }

    /**
     * Sets or changes the currently selected font.
     * 
     * @param fontName
     *            The name of the font to be selected.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFont(String fontName) {
        gtk_font_button_set_font_name(getHandle(), fontName);
    }

    /**
     * Returns the name of the currently selected font.
     * 
     * @return The name of the currently selected font.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFont() {
        return gtk_font_button_get_font_name(getHandle());
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/

    /**
     * Listeners for handling dialog events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector fbListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see FontButtonListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(FontButtonListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(fbListeners, listener);
        if (i == -1) {
            if (null == fbListeners) {
                evtMap.initialize(this, FontButtonEvent.Type.FONT_SET);
                fbListeners = new Vector();
            }
            fbListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(FontButtonListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(FontButtonListener listener) {
        int i = findListener(fbListeners, listener);
        if (i > -1) {
            fbListeners.remove(i);
        }
        if (0 == fbListeners.size()) {
            evtMap.uninitialize(this, FontButtonEvent.Type.FONT_SET);
            fbListeners = null;
        }
    }

    protected void fireFontButtonEvent(FontButtonEvent event) {
        if (null == fbListeners) {
            return;
        }
        int size = fbListeners.size();
        int i = 0;
        while (i < size) {
            FontButtonListener fbl = (FontButtonListener) fbListeners
                    .elementAt(i);
            fbl.fontButtonEvent(event);
            i++;
        }
    }

    private void handleFontSet() {
        fireFontButtonEvent(new FontButtonEvent(this,
                FontButtonEvent.Type.FONT_SET));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("font_set", "handleFontSet",
                FontButtonEvent.Type.FONT_SET, FontButtonListener.class);
    }

    native static final protected int gtk_font_button_get_type();

    native static final protected Handle gtk_font_button_new();

    native static final protected Handle gtk_font_button_new_with_font(
            String fontname);

    native static final protected String gtk_font_button_get_title(
            Handle fontButton);

    native static final protected void gtk_font_button_set_title(
            Handle fontButton, String title);

    native static final protected boolean gtk_font_button_get_use_font(
            Handle fontButton);

    native static final protected void gtk_font_button_set_use_font(
            Handle fontButton, boolean useFont);

    native static final protected boolean gtk_font_button_get_use_size(
            Handle fontButton);

    native static final protected void gtk_font_button_set_use_size(
            Handle fontButton, boolean useSize);

    native static final protected String gtk_font_button_get_font_name(
            Handle fontButton);

    native static final protected boolean gtk_font_button_set_font_name(
            Handle fontButton, String fontname);

    native static final protected boolean gtk_font_button_get_show_style(
            Handle fontButton);

    native static final protected void gtk_font_button_set_show_style(
            Handle fontButton, boolean showStyle);

    native static final protected boolean gtk_font_button_get_show_size(
            Handle fontButton);

    native static final protected void gtk_font_button_set_show_size(
            Handle fontButton, boolean showSize);

}
