/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * You may wish to begin by reading the {@link TextView} overview which gives an
 * overview of all the objects and data types related to the text widget and how
 * they work together.
 * 
 * <p>
 * A TextMark is like a bookmark in a text buffer; it preserves a position in
 * the text. You can convert the mark to an iterator using {@link
 * TextBuffer#getIter(TextMark)}. Unlike iterators, marks remain valid across
 * buffer mutations, because their behavior is defined when text is inserted or
 * deleted. When text containing a mark is deleted, the mark remains in the
 * position originally occupied by the deleted text. When text is inserted at a
 * mark, a mark with left gravity will be moved to the beginning of the
 * newly-inserted text, and a mark with right gravity will be moved to the end.
 * 
 * <p>
 * Marks optionally have names; these can be convenient to avoid passing the
 * TextMark object around.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TextMark</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TextMark extends GObject {
    /**
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected TextMark(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static TextMark getTextMark(Handle handle) {
        if (handle == null)
            return null;

        TextMark obj = (TextMark) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new TextMark(handle);

        return obj;
    }

    /**
     * Sets the visibility of mark; the insertion point is normally visible, ie
     * you can see it as a vertical bar. Also, the text widget uses a visible
     * mark to indicate where a drop will occur when dragging-and-dropping text.
     * Most other marks are not visible. Marks are not visible by default.
     * 
     * @param setting
     *            Visibility of mark
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibility(boolean setting) {
        gtk_text_mark_set_visible(getHandle(), setting);
    }

    /**
     * Returns TRUE if the mark is visible (ie a cursor is displayed for it)
     * 
     * @return true if it is visible
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getVisibility() {
        return gtk_text_mark_get_visible(getHandle());
    }

    /**
     * Returns TRUE if the mark has been removed from its buffer with
     * {@link TextBuffer#deleteMark(TextMark)}. Marks can't be used once
     * deleted.
     * 
     * @return True if deleted
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getDeleted() {
        return gtk_text_mark_get_deleted(getHandle());
    }

    /**
     * Returns the mark name; returns NULL for anonymous marks.
     * 
     * @return The name of the mark
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return gtk_text_mark_get_name(getHandle());
    }

    /**
     * Gets the buffer this mark is located inside, or NULL if the mark is
     * deleted.
     * 
     * @return The buffer/
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextBuffer getBuffer() {
        Handle hndl = gtk_text_mark_get_buffer(getHandle());
        return TextBuffer.getTextBuffer(hndl);
    }

    /**
     * Determines whether the mark has left gravity.
     * 
     * @return TRUE if the mark has left gravity, FALSE otherwise
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getLeftGravity() {
        return gtk_text_mark_get_left_gravity(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_text_mark_get_type());
    }

    native static final protected int gtk_text_mark_get_type();

    native static final protected void gtk_text_mark_set_visible(Handle mark,
            boolean setting);

    native static final protected boolean gtk_text_mark_get_visible(Handle mark);

    native static final protected String gtk_text_mark_get_name(Handle mark);

    native static final protected boolean gtk_text_mark_get_deleted(Handle mark);

    native static final protected Handle gtk_text_mark_get_buffer(Handle mark);

    native static final protected boolean gtk_text_mark_get_left_gravity(
            Handle mark);
}
