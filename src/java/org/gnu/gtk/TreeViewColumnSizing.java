/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Determines how columns of a tree or table are resized.
 * 
 * @see TreeViewColumn
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeViewColumnSizing</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TreeViewColumnSizing extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _GROW_ONLY = 0;

    static final public org.gnu.gtk.TreeViewColumnSizing GROW_ONLY = new org.gnu.gtk.TreeViewColumnSizing(
            _GROW_ONLY);

    static final private int _AUTOSIZE = 1;

    static final public org.gnu.gtk.TreeViewColumnSizing AUTOSIZE = new org.gnu.gtk.TreeViewColumnSizing(
            _AUTOSIZE);

    static final private int _FIXED = 2;

    /** */
    static final public org.gnu.gtk.TreeViewColumnSizing FIXED = new org.gnu.gtk.TreeViewColumnSizing(
            _FIXED);

    static final private org.gnu.gtk.TreeViewColumnSizing[] theInterned = new org.gnu.gtk.TreeViewColumnSizing[] {
            GROW_ONLY, AUTOSIZE, FIXED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.TreeViewColumnSizing theSacrificialOne = new org.gnu.gtk.TreeViewColumnSizing(
            0);

    static public org.gnu.gtk.TreeViewColumnSizing intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.TreeViewColumnSizing already = (org.gnu.gtk.TreeViewColumnSizing) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.TreeViewColumnSizing(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TreeViewColumnSizing(int value) {
        value_ = value;
    }

    public org.gnu.gtk.TreeViewColumnSizing or(
            org.gnu.gtk.TreeViewColumnSizing other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.TreeViewColumnSizing and(
            org.gnu.gtk.TreeViewColumnSizing other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.TreeViewColumnSizing xor(
            org.gnu.gtk.TreeViewColumnSizing other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.TreeViewColumnSizing other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
