/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.OptionMenuEvent;
import org.gnu.gtk.event.OptionMenuListener;
import org.gnu.glib.Handle;

/**
 * deprecated menu widget replaced by {@link ComboBox} in gtk 2.3.
 * 
 * An OptionMenu is a widget that allows the user to choose from a list of valid
 * choices. When activated the OptionMenu displays a popup Menu which allows the
 * user to make a new choice.
 * 
 * @deprecated Replaced by {@link ComboBox} in gtk 2.3
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.OptionMenu</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class OptionMenu extends Button {

    /**
     * Create a new OptionMenu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public OptionMenu() {
        super(gtk_option_menu_new());
    }

    /**
     * Construct an OptionMenu using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public OptionMenu(Handle handle) {
        super(handle);
    }

    /**
     * Provides the Menu that is popped up to allow the user to choose a new
     * value. You should provide a simple menu avoiding the use of tearoff menu
     * items, submenus, and accelerators.
     * 
     * @param menu
     *            The Menu to add to the OptionMenu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMenu(Menu menu) {
        OptionMenu.gtk_option_menu_set_menu(getHandle(), menu.getHandle());
    }

    /**
     * Returns the Menu associated with this OptionMenu.
     * 
     * @return The Menu associated with this OptionMenu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Menu getMenu() {
        Handle hndl = OptionMenu.gtk_option_menu_get_menu(getHandle());
        return Menu.getMenu(hndl);
    }

    /**
     * Removes the Menu from the OptionMenu
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeMenu() {
        OptionMenu.gtk_option_menu_remove_menu(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_option_menu_get_type());
    }

    /**
     * Retrieves the index of the currently selected menu item. The menu items
     * are numbered from top to bottom, starting with 0.
     * 
     * @return index of the selected menu item, or -1 if there are no menu items
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHistory() {
        return gtk_option_menu_get_history(getHandle());
    }

    /**
     * Selects the menu item specified by <code>index</code> making it the
     * newly selected value for the option menu.
     * 
     * @param index
     *            the index of the menu item to select. Index values are from 0
     *            to n-1.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHistory(int index) {
        gtk_option_menu_set_history(getHandle(), index);
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/

    /**
     * Listeners for handling button events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector optionMenuListeners = null;

    /**
     * Register an object to handle optionMenu events.
     * 
     * @see org.gnu.gtk.event.OptionMenuListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(OptionMenuListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(optionMenuListeners, listener);
        if (i == -1) {
            if (null == optionMenuListeners) {
                evtMap.initialize(this, OptionMenuEvent.Type.CHANGE);
                optionMenuListeners = new Vector();
            }
            optionMenuListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(OptionMenuListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(OptionMenuListener listener) {
        int i = findListener(optionMenuListeners, listener);
        if (i > -1) {
            optionMenuListeners.remove(i);
        }
        if (0 == optionMenuListeners.size()) {
            evtMap.uninitialize(this, OptionMenuEvent.Type.CHANGE);
            optionMenuListeners = null;
        }
    }

    protected void fireOptionMenuEvent(OptionMenuEvent event) {
        if (null == optionMenuListeners) {
            return;
        }
        int size = optionMenuListeners.size();
        int i = 0;
        while (i < size) {
            OptionMenuListener bl = (OptionMenuListener) optionMenuListeners
                    .elementAt(i);
            bl.optionMenuEvent(event);
            i++;
        }
    }

    private void handleChange() {
        fireOptionMenuEvent(new OptionMenuEvent(this,
                OptionMenuEvent.Type.CHANGE));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected static void addEvents(EventMap evtMap) {
        evtMap.addEvent("changed", "handleChange", OptionMenuEvent.Type.CHANGE,
                OptionMenuListener.class);
    }

    native static final protected int gtk_option_menu_get_type();

    native static final protected Handle gtk_option_menu_new();

    native static final protected Handle gtk_option_menu_get_menu(
            Handle option_menu);

    native static final protected void gtk_option_menu_set_menu(
            Handle option_menu, Handle menu);

    native static final protected void gtk_option_menu_remove_menu(
            Handle option_menu);

    native static final protected int gtk_option_menu_get_history(
            Handle option_menu);

    native static final protected void gtk_option_menu_set_history(
            Handle option_menu, int index);

}
