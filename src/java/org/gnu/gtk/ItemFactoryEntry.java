/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.gtk.event.MenuItemListener;
import org.gnu.glib.Handle;

/**
 * Old class replaced by {@link UIManager}. This class is provided for
 * compatibility only - it will be removed in future releases of java-gnome. Do
 * not use in new code.
 * 
 * @deprecated 2.3
 * 
 * This class contains the data necessary for an {@link org.gnu.gtk.ItemFactory}
 * to create a menu.
 * @see org.gnu.gtk.ItemFactory
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ItemFactoryEntry</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ItemFactoryEntry extends MemStruct {

    /**
     * holds the listener for MenuEvents from this widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private MenuItemListener listener;

    protected ItemFactoryEntry(Handle handle) {
        super(handle);
    }

    /**
     * Construct a new ItemFactoryEntry
     * 
     * @param path
     *            A string that defines both the name and the path for a menu
     *            item, for example "/File/Open" would be the name of a menu
     *            item which would come under the ItemFactory entry with the
     *            path "/File". Note however that "/File/Open" would be
     *            displayed in the File menu as "Open". A letter preceded by an
     *            underscore indicates an accelerator key once the menu is open.
     * @param accelerator
     *            A string that indicates a key combination that can be used as
     *            a shortcut to that menu item. The string can be made up of
     *            either a single character, or a combination of modifier keys
     *            with a single character. The available modifier keys are:<BR>
     * 
     * <pre>
     *  &quot;&lt;ALT&gt;&quot; - alt
     *  &quot;&lt;CTL&gt;&quot; or &quot;&lt;CTRL&gt;&quot; or &quot;&lt;CONTROL&gt;&quot; - control
     *  &lt;MOD1&gt;&quot; to &quot;&lt;MOD5&gt;&quot; - modn
     *  &quot;&lt;SHFT&gt;&quot; or &quot;&lt;SHIFT&gt;&quot; - shift
     * </pre>
     * 
     * @param listener
     *            A menu listener that will receive the MenuEvents fired from
     *            the created widgets.
     * @param itemType
     *            A string that defines what type of widget is packed into the
     *            menu items container. It can be:<BR>
     * 
     * <pre>
     *  null or &quot;&lt;Item&gt;&quot; - create a simple item
     *  &quot;&lt;Title&gt;&quot;        - create a title item
     *  &quot;&lt;CheckItem&gt;&quot;    - create a check item
     *  &quot;&lt;ToggleItem&gt;&quot;   - create a toggle item
     *  &quot;&lt;RadioItem&gt;&quot;    - create a (root) radio item
     *  &quot;Path&quot;                 - create a sister radio item
     *  &quot;&lt;Tearoff&gt;&quot;      - create a tearoff
     *  &quot;&lt;Separator&gt;&quot;    - create a separator
     *  &quot;&lt;Branch&gt;&quot;       - create an item to hold submenus
     *  &quot;&lt;LastBranch&gt;&quot;   - create a right justified branch
     * </pre>
     * 
     * Note that "&lt;LastBranch&gt;" is only useful for one submenu
     *            of a menu.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ItemFactoryEntry construct(String path, String accelerator,
            MenuItemListener listener, String itemType) {
        Handle handle = gtk_item_factory_entry_new(path, accelerator, itemType);
        ItemFactoryEntry ife = (ItemFactoryEntry) getMemStructFromHandle(handle);
        if (ife == null) {
            ife = new ItemFactoryEntry(handle);
        }
        ife.listener = listener;
        return ife;
    }

    /**
     * Retrieve the MenuListener associated with this item.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuItemListener getMenuListener() {
        return listener;
    }

    // REDTAG: native method doesn't exist at this time!!
    protected native static final Handle gtk_item_factory_entry_new(
            String path, String accel, String type);

    native static final protected String getPath(Handle cptr);

    native static final protected String getAccelerator(Handle cptr);

    native static final protected String getItemType(Handle cptr);

}
