/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A widget that lists the available fonts, styles and sizes, allowing the user
 * to select a font. It is used in the {@link FontSelectionDialog} widget to
 * provide a dialog box for selecting fonts.
 * 
 * @see FontSelectionDialog
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FontSelection</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FontSelection extends VBox {
    /**
     * Creates a new font selection widget
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontSelection() {
        super(gtk_font_selection_new());
    }

    /**
     * Construct a new FontSelection from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontSelection(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static FontSelection getFontSelection(Handle handle) {
        if (handle == null)
            return null;

        FontSelection obj = (FontSelection) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FontSelection(handle);

        return obj;
    }

    /**
     * Exception used when a request was made to set a font which is not
     * available.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    class FontNotFoundException extends Exception {
    }

    /**
     * Sets the currently-selected font.
     * 
     * @param font
     *            The name of the font to be selected
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFont(String font) throws FontNotFoundException {
        if (!gtk_font_selection_set_font_name(getHandle(), font)) {
            throw new FontNotFoundException();
        }
    }

    /**
     * Returns the name of the currently selected font.
     * 
     * @return The name of the selected font.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFont() {
        return gtk_font_selection_get_font_name(getHandle());
    }

    /**
     * Returns the text which appears in the preview box.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getPreviewText() {
        return gtk_font_selection_get_preview_text(getHandle());
    }

    /**
     * Sets the text to appear in the preview box.
     * 
     * @param previewText
     *            The text which should appear in the preview box
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void getPreviewText(String previewText) {
        gtk_font_selection_set_preview_text(getHandle(), previewText);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_font_selection_get_type());
    }

    native static final protected int gtk_font_selection_get_type();

    native static final protected Handle gtk_font_selection_new();

    native static final protected String gtk_font_selection_get_font_name(
            Handle fontsel);

    native static final protected boolean gtk_font_selection_set_font_name(
            Handle fontsel, String fontname);

    native static final protected String gtk_font_selection_get_preview_text(
            Handle fontsel);

    native static final protected void gtk_font_selection_set_preview_text(
            Handle fontsel, String text);

    /*
     * Deprecated functions. native static final private Handle
     * gtk_font_selection_get_font(Handle fontsel);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

}
