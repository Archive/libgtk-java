/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

public class IconSource extends Boxed {
    /**
     * Construct a new IconSource object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSource() {
        super(gtk_icon_source_new());
    }

    /**
     * Sets the source filename
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFilename(String filename) {
        gtk_icon_source_set_filename(getHandle(), filename);
    }

    /**
     * Retrieves the source filename or null if one does not exist.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFilename() {
        return gtk_icon_source_get_filename(getHandle());
    }

    /**
     * Sets the source pixbuf.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPixbuf(Pixbuf pixbuf) {
        gtk_icon_source_set_pixbuf(getHandle(), pixbuf.getHandle());
    }

    /**
     * Retrieves the source pixbuf or null if one does not exist.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getPixbuf() {
        Handle hndl = gtk_icon_source_get_pixbuf(getHandle());
        if (hndl == null) {
            return null;
        }
        return new Pixbuf(hndl);
    }

    /**
     * Set the icon size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(IconSize iconSize) {
        gtk_icon_source_set_size(getHandle(), iconSize.getValue());
    }

    /**
     * Return the icon size.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSize getSize() {
        return IconSize.intern(gtk_icon_source_get_size(getHandle()));
    }

    /**
     * Set the icon state.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setState(StateType state) {
        gtk_icon_source_set_state(getHandle(), state.getValue());
    }

    /**
     * Return the icon state.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public StateType getState() {
        return StateType.intern(gtk_icon_source_get_state(getHandle()));
    }

    /**
     * Set the text direction the icon source is intended to be used with.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDirection(TextDirection direction) {
        gtk_icon_source_set_direction(getHandle(), direction.getValue());
    }

    /**
     * Returns the text direction for the icon source.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TextDirection getDirection() {
        return TextDirection.intern(gtk_icon_source_get_direction(getHandle()));
    }

    /**
     * Sets the name of an icon to look up in the current icon theme to use as a
     * base image when creating icon variants for {@link org.gnu.gtk.IconSet}.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconName(String iconName) {
        gtk_icon_source_set_icon_name(getHandle(), iconName);
    }

    /**
     * Retrieves the source icon name, or NULL if none is set. The icon_name is
     * not a copy, and should not be modified or expected to persist beyond the
     * lifetime of the icon source.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getIconName() {
        return gtk_icon_source_get_icon_name(getHandle());
    }

    native static final protected Handle gtk_icon_source_new();

    native static final protected void gtk_icon_source_free(Handle source);

    native static final protected void gtk_icon_source_set_filename(
            Handle source, String filename);

    native static final protected void gtk_icon_source_set_pixbuf(
            Handle source, Handle pixbuf);

    native static final protected String gtk_icon_source_get_filename(
            Handle source);

    native static final protected Handle gtk_icon_source_get_pixbuf(
            Handle source);

    native static final protected void gtk_icon_source_set_direction_wildcarded(
            Handle source, boolean setting);

    native static final protected void gtk_icon_source_set_state_wildcarded(
            Handle source, boolean setting);

    native static final protected void gtk_icon_source_set_size_wildcarded(
            Handle source, boolean setting);

    native static final protected boolean gtk_icon_source_get_size_wildcarded(
            Handle source);

    native static final protected boolean gtk_icon_source_get_state_wildcarded(
            Handle source);

    native static final protected boolean gtk_icon_source_get_direction_wildcarded(
            Handle source);

    native static final protected void gtk_icon_source_set_direction(
            Handle source, int direction);

    native static final protected void gtk_icon_source_set_state(Handle source,
            int state);

    native static final protected void gtk_icon_source_set_size(Handle source,
            int size);

    native static final protected int gtk_icon_source_get_direction(
            Handle source);

    native static final protected int gtk_icon_source_get_state(Handle source);

    native static final protected int gtk_icon_source_get_size(Handle source);

    native static final private int gtk_icon_source_get_type();

    native static final private void gtk_icon_source_set_icon_name(
            Handle source, String icon_name);

    native static final private String gtk_icon_source_get_icon_name(
            Handle source);

}
