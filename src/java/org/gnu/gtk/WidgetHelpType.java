/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class WidgetHelpType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TOOLTIP = 0;

    static final public org.gnu.gtk.WidgetHelpType TOOLTIP = new org.gnu.gtk.WidgetHelpType(
            _TOOLTIP);

    static final private int _WHATIS_THIS = 1;

    static final public org.gnu.gtk.WidgetHelpType WHATIS_THIS = new org.gnu.gtk.WidgetHelpType(
            _WHATIS_THIS);

    static final private org.gnu.gtk.WidgetHelpType[] theInterned = new org.gnu.gtk.WidgetHelpType[] {
            TOOLTIP, WHATIS_THIS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.WidgetHelpType theSacrificialOne = new org.gnu.gtk.WidgetHelpType(
            0);

    static public org.gnu.gtk.WidgetHelpType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.WidgetHelpType already = (org.gnu.gtk.WidgetHelpType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.WidgetHelpType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WidgetHelpType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.WidgetHelpType or(org.gnu.gtk.WidgetHelpType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.WidgetHelpType and(org.gnu.gtk.WidgetHelpType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.WidgetHelpType xor(org.gnu.gtk.WidgetHelpType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.WidgetHelpType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
