/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RadioActionEntry</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RadioActionEntry extends MemStruct {

    public RadioActionEntry(String name, String stockId, String label,
            String accel, String tooltip, int value) {

        // create the native object
        super(allocate());
        // set the values
        setName(getHandle(), name);
        setStockId(getHandle(), stockId);
        setLabel(getHandle(), label);
        setAccelerator(getHandle(), accel);
        setToolTip(getHandle(), tooltip);
        setValue(getHandle(), value);
    }

    native static final protected Handle allocate();

    native static final protected void setName(Handle entry, String name);

    native static final protected void setStockId(Handle entry, String stockId);

    native static final protected void setLabel(Handle entry, String label);

    native static final protected void setAccelerator(Handle entry, String accel);

    native static final protected void setToolTip(Handle entry, String tooltip);

    native static final protected void setValue(Handle entry, int value);

}
