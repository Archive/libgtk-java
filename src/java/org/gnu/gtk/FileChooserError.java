/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import java.util.Hashtable;

import org.gnu.glib.Enum;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileChooserError</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileChooserError extends Enum {
    static final private int _ERROR_NONEXISTENT = 0;

    static final public FileChooserError ERROR_NONEXISTENT = new FileChooserError(
            _ERROR_NONEXISTENT);

    static final private int _BAD_FILENAME = 1;

    static final public FileChooserError BAD_FILENAME = new FileChooserError(
            _BAD_FILENAME);

    static final private FileChooserError[] theInterned = new FileChooserError[] {
            ERROR_NONEXISTENT, BAD_FILENAME };

    static private java.util.Hashtable theInternedExtras;

    static final private FileChooserError theSacrificialOne = new FileChooserError(
            0);

    static public FileChooserError intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new Hashtable();
        }
        FileChooserError already = (FileChooserError) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new FileChooserError(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FileChooserError(int value) {
        value_ = value;
    }

    public FileChooserError or(FileChooserError other) {
        return intern(value_ | other.value_);
    }

    public FileChooserError and(FileChooserError other) {
        return intern(value_ & other.value_);
    }

    public FileChooserError xor(FileChooserError other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FileChooserError other) {
        return (value_ & other.value_) == other.value_;
    }
}
