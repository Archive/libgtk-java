/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The EventBox widget is a container with a seethrough window that can receive
 * events for the container widget - normally one that cannot recvieve its own.
 * Events are sent to the windows, so a widget without windows cannot recieve
 * events.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.EventBox</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class EventBox extends Bin {
    /**
     * Construct a new EventBox object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventBox() {
        super(gtk_event_box_new());
    }

    /**
     * Construct an eventbox using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventBox(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static EventBox getEventBox(Handle handle) {
        if (handle == null)
            return null;

        EventBox obj = (EventBox) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new EventBox(handle);

        return obj;
    }

    /**
     * Sets whether the EventBox has a visible window.
     * 
     * @param isVisible
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWindowVisible(boolean isVisible) {
        gtk_event_box_set_visible_window(getHandle(), isVisible);
    }

    /**
     * Returns whether the EventBox has a visible window
     * 
     * @return true if the EventBox has a visible window.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getWindowVisible() {
        return gtk_event_box_get_visible_window(getHandle());
    }

    /**
     * Returns whether the EventBox is above or below the windows of its' child.
     * 
     * @param aboveChild
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAboveChild(boolean aboveChild) {
        gtk_event_box_set_above_child(getHandle(), aboveChild);
    }

    /**
     * Returns whether the EventBox is above the window of its' child.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getAboveChild() {
        return gtk_event_box_get_above_child(getHandle());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_event_box_get_type());
    }

    native static final protected int gtk_event_box_get_type();

    native static final protected Handle gtk_event_box_new();

    native static final protected void gtk_event_box_set_visible_window(
            Handle ebox, boolean visible);

    native static final protected boolean gtk_event_box_get_visible_window(
            Handle ebox);

    native static final protected void gtk_event_box_set_above_child(
            Handle ebox, boolean above);

    native static final protected boolean gtk_event_box_get_above_child(
            Handle ebox);
}
