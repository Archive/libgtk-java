/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class PathType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _WIDGET = 0;

    static final public org.gnu.gtk.PathType WIDGET = new org.gnu.gtk.PathType(
            _WIDGET);

    static final private int _WIDGET_CLASS = 1;

    static final public org.gnu.gtk.PathType WIDGET_CLASS = new org.gnu.gtk.PathType(
            _WIDGET_CLASS);

    static final private int _CLASS = 2;

    static final public org.gnu.gtk.PathType CLASS = new org.gnu.gtk.PathType(
            _CLASS);

    static final private org.gnu.gtk.PathType[] theInterned = new org.gnu.gtk.PathType[] {
            WIDGET, WIDGET_CLASS, CLASS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.PathType theSacrificialOne = new org.gnu.gtk.PathType(
            0);

    static public org.gnu.gtk.PathType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.PathType already = (org.gnu.gtk.PathType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.PathType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PathType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.PathType or(org.gnu.gtk.PathType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.PathType and(org.gnu.gtk.PathType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.PathType xor(org.gnu.gtk.PathType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.PathType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
