/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Vector;

import org.gnu.gdk.Color;
import org.gnu.glib.EventMap;
import org.gnu.glib.EventType;
import org.gnu.glib.Type;
import org.gnu.gtk.event.ColorSelectionEvent;
import org.gnu.gtk.event.ColorSelectionListener;
import org.gnu.glib.Handle;

/**
 * This widget displays color information in such a way that the user can
 * specify, with the mouse, any available color.
 * 
 * @see ColorSelectionDialog
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ColorSelection</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ColorSelection extends VBox {

    /**
     * Creates a new gtk.ColorSelection.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorSelection() {
        super(gtk_color_selection_new());
    }

    /**
     * construct a ColorSelection from a handle to a native peer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ColorSelection(Handle handle) {
        super(handle);
    }

    /**
     * construct a ColorSelection from a handle to a native peer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ColorSelection getColorSelection(Handle handle) {
        if (handle == null)
            return null;

        ColorSelection obj = (ColorSelection) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ColorSelection(handle);

        return obj;
    }

    /**
     * Sets the widget to have an opacity control
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHasOpacity(boolean hasOpacity) {
        gtk_color_selection_set_has_opacity_control(getHandle(), hasOpacity);
    }

    /**
     * Sets the current color to be color. The first time this is called, it
     * will also set the original color to be color too.
     * 
     * @param color
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCurrentColor(Color color) {
        gtk_color_selection_set_current_color(getHandle(), color.getHandle());
    }

    /**
     * Returns the color which is set in the widget
     * 
     * @return the set colour
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color getCurrentColor() {
        Handle colorHandle = gtk_color_selection_get_current_color(getHandle());
        return Color.getColorFromHandle(colorHandle);
    }

    /**
     * Sets the ColorSelection to use or not to use the opacity control.
     * 
     * @param hasOpacity
     *            true if this widget can set the opacity.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHasOpacityControl(boolean hasOpacity) {
        gtk_color_selection_set_has_opacity_control(getHandle(), hasOpacity);
    }

    /**
     * Determines whether the ColorSelection has an opacity control.
     * 
     * @return true if the widget has the opacity control.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getHasOpacityControl() {
        return gtk_color_selection_get_has_opacity_control(getHandle());
    }

    /**
     * Shows and hides the palette based upon the value of hasPalette.
     * 
     * @param hasPalette
     *            true if the palette should be visible.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHasPalette(boolean hasPalette) {
        gtk_color_selection_set_has_palette(getHandle(), hasPalette);
    }

    /**
     * Determines whether the widget's palette is visible.
     * 
     * @return true if the palette is visible.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getHasPalette() {
        return gtk_color_selection_get_has_palette(getHandle());
    }

    /**
     * Sets the current opacity to be <i>alpha</i>. The first time this is
     * called it will also set the original opacity to be <i>alpha</i> too.
     * 
     * @param alpha
     *            An integer between 0 and 65535.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCurrentAlpha(int alpha) {
        gtk_color_selection_set_current_alpha(getHandle(), alpha);
    }

    /**
     * Returns the current opacity.
     * 
     * @return An integer between 0 and 65535.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getCurrentAlpha() {
        return gtk_color_selection_get_current_alpha(getHandle());
    }

    /**
     * Sets the previous alpha value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPreviousAlpha(int alpha) {
        gtk_color_selection_set_previous_alpha(getHandle(), alpha);
    }

    /**
     * Returns the previous alpha value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPreviousAlpha() {
        return gtk_color_selection_get_previous_alpha(getHandle());
    }

    /**
     * Sets the previous color value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setPreviousColor(Color color) {
        gtk_color_selection_set_previous_color(getHandle(), color.getHandle());
    }

    /**
     * Returns the previous color.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color getPreviousColor() {
        Handle handle = gtk_color_selection_get_previous_color(getHandle());
        return Color.getColorFromHandle(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_color_selection_get_type());
    }

    /***************************************************************************
     * EVENT LISTENERS
     **************************************************************************/
    /**
     * Listeners for handling dialog events
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private Vector csListeners = null;

    /**
     * Register an object to handle dialog events.
     * 
     * @see ColorSelectionListener
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(ColorSelectionListener listener) {
        // Don't add the listener a second time if it is in the Vector.
        int i = findListener(csListeners, listener);
        if (i == -1) {
            if (null == csListeners) {
                evtMap.initialize(this, ColorSelectionEvent.Type.COLOR_CHANGED);
                csListeners = new Vector();
            }
            csListeners.addElement(listener);
        }
    }

    /**
     * Removes a listener
     * 
     * @see #addListener(ColorSelectionListener)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(ColorSelectionListener listener) {
        int i = findListener(csListeners, listener);
        if (i > -1) {
            csListeners.remove(i);
        }
        if (0 == csListeners.size()) {
            evtMap.uninitialize(this, ColorSelectionEvent.Type.COLOR_CHANGED);
            csListeners = null;
        }
    }

    protected void fireColorSelectionEvent(ColorSelectionEvent event) {
        if (null == csListeners) {
            return;
        }
        int size = csListeners.size();
        int i = 0;
        while (i < size) {
            ColorSelectionListener csl = (ColorSelectionListener) csListeners
                    .elementAt(i);
            csl.colorSelectionEvent(event);
            i++;
        }
    }

    private void handleColorChanged() {
        fireColorSelectionEvent(new ColorSelectionEvent(this,
                ColorSelectionEvent.Type.COLOR_CHANGED));
    }

    public Class getEventListenerClass(String signal) {
        Class cls = evtMap.getEventListenerClass(signal);
        if (cls == null)
            cls = super.getEventListenerClass(signal);
        return cls;
    }

    public EventType getEventType(String signal) {
        EventType et = evtMap.getEventType(signal);
        if (et == null)
            et = super.getEventType(signal);
        return et;
    }

    private static EventMap evtMap = new EventMap();
    static {
        addEvents(evtMap);
    }

    /**
     * Implementation method to build an EventMap for this widget class. Not
     * useful (or supported) for application use.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static void addEvents(EventMap anEvtMap) {
        anEvtMap.addEvent("color_changed", "handleColorChanged",
                ColorSelectionEvent.Type.COLOR_CHANGED,
                ColorSelectionListener.class);
    }

    native static final protected int gtk_color_selection_get_type();

    native static final protected Handle gtk_color_selection_new();

    native static final protected boolean gtk_color_selection_get_has_opacity_control(
            Handle colorsel);

    native static final protected void gtk_color_selection_set_has_opacity_control(
            Handle colorsel, boolean hasOpacity);

    native static final protected boolean gtk_color_selection_get_has_palette(
            Handle colorsel);

    native static final protected void gtk_color_selection_set_has_palette(
            Handle colorsel, boolean hasPalette);

    native static final protected void gtk_color_selection_set_current_color(
            Handle colorsel, Handle color);

    native static final protected void gtk_color_selection_set_current_alpha(
            Handle colorsel, int alpha);

    native static final protected Handle gtk_color_selection_get_current_color(
            Handle colorsel);

    native static final protected int gtk_color_selection_get_current_alpha(
            Handle colorsel);

    native static final protected void gtk_color_selection_set_previous_color(
            Handle colorsel, Handle color);

    native static final protected void gtk_color_selection_set_previous_alpha(
            Handle colorsel, int alpha);

    native static final protected Handle gtk_color_selection_get_previous_color(
            Handle colorsel);

    native static final protected int gtk_color_selection_get_previous_alpha(
            Handle colorsel);

    native static final protected boolean gtk_color_selection_is_adjusting(
            Handle colorsel);

    native static final protected boolean gtk_color_selection_palette_from_string(
            String str, Handle[] color);

    native static final protected String gtk_color_selection_palette_to_string(
            Handle color, int nColors);

    /*
     * Deprecated functions. native static final private void
     * gtk_color_selection_set_color(Handle colorsel, gdouble* color); native
     * static final private void gtk_color_selection_get_color(Handle colorsel,
     * gdouble* color); native static final private void
     * gtk_color_selection_set_update_policy(Handle colorsel, int policy);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
