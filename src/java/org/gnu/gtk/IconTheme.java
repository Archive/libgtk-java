/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.gdk.Screen;
import org.gnu.glib.Boxed;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconTheme</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconTheme extends GObject {

    public IconTheme() {
        super(gtk_icon_theme_new());
    }

    private IconTheme(Handle handle) {
        super(handle);
    }

    private static IconTheme getIconTheme(Handle handle) {
        if (handle == null)
            return null;

        IconTheme obj = (IconTheme) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new IconTheme(handle);

        return obj;
    }

    static public IconTheme getDefault() {
        return getIconTheme(gtk_icon_theme_get_default());
    }

    static public IconTheme getForSceen(Screen screen) {
        return getIconTheme(gtk_icon_theme_get_for_screen(screen.getHandle()));
    }

    public void setScreen(Screen screen) {
        gtk_icon_theme_set_screen(getHandle(), screen.getHandle());
    }

    public void setSearchPath(String[] path) {
        gtk_icon_theme_set_search_path(getHandle(), path, path.length);
    }

    public String[] getSearchPath() {
        return gtk_icon_theme_get_search_path(getHandle());
    }

    public void appendSearchPath(String path) {
        gtk_icon_theme_append_search_path(getHandle(), path);
    }

    public void prependSearchPath(String path) {
        gtk_icon_theme_prepend_search_path(getHandle(), path);
    }

    public void setCustomtTheme(String name) {
        gtk_icon_theme_set_custom_theme(getHandle(), name);
    }

    public boolean hasIcon(String iconName) {
        return gtk_icon_theme_has_icon(getHandle(), iconName);
    }

    public IconInfo lookupIcon(String name, int size, IconLookupFlags flags) {
        Handle handle = gtk_icon_theme_lookup_icon(getHandle(), name, size,
                flags.getValue());
        if (handle == null) {
            return null;
        }
        IconInfo iconInfo = (IconInfo) Boxed.getBoxedFromHandle(handle);
        if (iconInfo == null) {
            iconInfo = new IconInfo(handle);
        }
        return iconInfo;
    }

    /**
     * Looks up an icon in an icon theme, scales it to the given size and
     * renders it into a pixbuf.
     * 
     * @param name
     * @param size
     * @param flags
     * @return The rendered pixbuf, or null if the icon is not found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf loadIcon(String name, int size, IconLookupFlags flags) {
        Handle hndl = gtk_icon_theme_load_icon(getHandle(), name, size, flags
                .getValue());
        if (hndl == null)
            return null;
        return new Pixbuf(hndl);
    }

    public String[] listIcons(String context) {
        return gtk_icon_theme_list_icons(getHandle(), context);
    }

    public boolean rescanIfNeeded() {
        return gtk_icon_theme_rescan_if_needed(getHandle());
    }

    public static void addBuiltinIcon(String name, int size, Pixbuf pixbuf) {
        gtk_icon_theme_add_builtin_icon(name, size, pixbuf.getHandle());
    }

    /**
     * Returns an array of {@link org.gnu.gtk.IconSize} describing the sizes at
     * which the icon is available without scaling. A size of -1 means that the
     * icon is available in a scalable format.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSize[] getIconSizes(String iconName) {
        int sizes[] = gtk_icon_theme_get_icon_sizes(getHandle(), iconName);
        IconSize ret[] = new IconSize[sizes.length];
        for (int i = 0; i < sizes.length; i++) {
            ret[i] = IconSize.intern(sizes[i]);
        }
        return ret;
    }

    native static final protected int gtk_icon_theme_get_type();

    native static final protected Handle gtk_icon_theme_new();

    native static final protected Handle gtk_icon_theme_get_default();

    native static final protected Handle gtk_icon_theme_get_for_screen(
            Handle screen);

    native static final protected void gtk_icon_theme_set_screen(Handle itheme,
            Handle screen);

    native static final protected void gtk_icon_theme_set_search_path(
            Handle itheme, String[] path, int pathlen);

    native static final protected String[] gtk_icon_theme_get_search_path(
            Handle itheme);

    native static final protected void gtk_icon_theme_append_search_path(
            Handle itheme, String path);

    native static final protected void gtk_icon_theme_prepend_search_path(
            Handle itheme, String path);

    native static final protected void gtk_icon_theme_set_custom_theme(
            Handle itheme, String name);

    native static final protected boolean gtk_icon_theme_has_icon(
            Handle itheme, String iconName);

    native static final protected Handle gtk_icon_theme_lookup_icon(
            Handle itheme, String iconName, int size, int iconLookupFlags);

    native static final protected Handle gtk_icon_theme_load_icon(
            Handle itheme, String iconName, int size, int iconLookupflags);

    native static final protected String[] gtk_icon_theme_list_icons(
            Handle itheme, String context);

    native static final protected String gtk_icon_theme_get_example_icon_name(
            Handle itheme);

    native static final protected boolean gtk_icon_theme_rescan_if_needed(
            Handle itheme);

    native static final protected void gtk_icon_theme_add_builtin_icon(
            String iconName, int size, Handle pixbuf);

    native static final private int[] gtk_icon_theme_get_icon_sizes(
            Handle icon_theme, String icon_name);

}
