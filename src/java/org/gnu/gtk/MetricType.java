/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

public class MetricType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _PIXELS = 0;

    static final public org.gnu.gtk.MetricType PIXELS = new org.gnu.gtk.MetricType(
            _PIXELS);

    static final private int _INCHES = 1;

    static final public org.gnu.gtk.MetricType INCHES = new org.gnu.gtk.MetricType(
            _INCHES);

    static final private int _CENTIMETERS = 2;

    static final public org.gnu.gtk.MetricType CENTIMETERS = new org.gnu.gtk.MetricType(
            _CENTIMETERS);

    static final private org.gnu.gtk.MetricType[] theInterned = new org.gnu.gtk.MetricType[] {
            PIXELS, INCHES, CENTIMETERS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.MetricType theSacrificialOne = new org.gnu.gtk.MetricType(
            0);

    static public org.gnu.gtk.MetricType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.MetricType already = (org.gnu.gtk.MetricType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.MetricType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private MetricType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.MetricType or(org.gnu.gtk.MetricType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.MetricType and(org.gnu.gtk.MetricType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.MetricType xor(org.gnu.gtk.MetricType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.MetricType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
