/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Handle;

/**
 * ToolItem is the base class of widgets that can be added to ToolBar objects.
 * 
 * <p>
 * ToolItems are widgets that can appear on a toolbar. To create a toolbar item
 * that contain something else than a button, use new
 * {@link ToolItem#ToolItem()}. Use {@link org.gnu.gtk.Container#add(Widget)}
 * to add a child widget to the tool item.
 * 
 * <p>
 * For toolbar items that contain buttons, see the {@link ToolButton},
 * {@link ToggleToolButton} and {@link RadioToolButton} classes. see
 * {@link org.gnu.gtk.ToolBar} class for a description of the toolbar widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.ToolItem</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ToolItem extends Bin {

    /**
     * Construct a new ToolItem object from a handle to a native widget.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    ToolItem(Handle hndl) {
        super(hndl);
    }

    /**
     * Construct a new ToolItem object from a handle to a native widget.
     * <p>
     * This is an internal static factory method that should be used by
     * Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ToolItem getToolItem(Handle handle) {
        if (handle == null)
            return null;

        ToolItem obj = (ToolItem) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new ToolItem(handle);

        return obj;
    }

    /**
     * Create an empty ToolItem.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolItem() {
        super(gtk_tool_item_new());
    }

    /**
     * Sets whether the ToolItem is to be allocated the same size as other
     * homogeneous items. The effect is that all homogeneous items will have the
     * same width as the widest of the items.
     * 
     * @param homogeneous
     *            <code>TRUE</code> if the item is the same size as other
     *            homogeneous items
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHomogeneous(boolean homogeneous) {
        gtk_tool_item_set_homogeneous(getHandle(), homogeneous);
    }

    /**
     * Returns whether the item is the same size as other homogeneous items.
     * 
     * @return <code>TRUE</code> if the item is the same size as other
     *         homogeneous items
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getHomogeneous() {
        return gtk_tool_item_get_homogeneous(getHandle());
    }

    /**
     * Sets whether the ToolItem is allocated extra space when there is more
     * room on the ToolBar than needed by the ToolItems. The effect is that the
     * item gets bigger when the ToolBar gets bigger and smaller when the
     * ToolBar gets smaller.
     * 
     * @param expand
     *            whether ToolItem is allocated extra space
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setExpand(boolean expand) {
        gtk_tool_item_set_expand(getHandle(), expand);
    }

    /**
     * Returns whether ToolItem is allocated extra space.
     * 
     * @return <code>TRUE</code> if ToolItem is allocated extra space
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getExpand() {
        return gtk_tool_item_get_expand(getHandle());
    }

    /**
     * Sets the {@link ToolTips} object to be used for ToolItem, the text to be
     * displayed as tooltip on the item and the private text to be used.
     * 
     * @see ToolTips#setTip(Widget, String, String)
     * @param toolTips
     *            the {@link ToolTips} object to be used
     * @param tipText
     *            text to be used as tooltip text for ToolItem
     * @param tipPrivate
     *            text to be used as private tooltip text
     * @deprecated
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTooltip(ToolTips toolTips, String tipText, String tipPrivate) {
        gtk_tool_item_set_tooltip(getHandle(), toolTips.getHandle(), tipText,
                tipPrivate);
    }

    /**
     * Sets the {@link ToolTips} object to be used for ToolItem, the text to be
     * displayed as tooltip on the item and the private text to be used.
     * 
     * @see ToolTips#setTip(Widget, String, String)
     * @param toolTips
     *            the {@link ToolTips} object to be used
     * @param tipText
     *            text to be used as tooltip text for ToolItem
     * @param tipPrivate
     *            text to be used as private tooltip text
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setToolTip(ToolTips toolTips, String tipText, String tipPrivate) {
        gtk_tool_item_set_tooltip(getHandle(), toolTips.getHandle(), tipText,
                tipPrivate);
    }

    /**
     * Sets whether ToolItem has a drag window. When <code>TRUE</code> the
     * ToolItem can be used as a drag source through DragSource. When ToolItem
     * has a drag window it will intercept all events, even those that would
     * otherwise be sent to a child of ToolItem.
     * 
     * @param useDragWindow
     *            whether ToolItem has a drag window
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setUseDragWindow(boolean useDragWindow) {
        gtk_tool_item_set_use_drag_window(getHandle(), useDragWindow);
    }

    /**
     * Returns whether ToolItem has a drag window.
     * 
     * @see ToolItem#setUseDragWindow(boolean)
     * @return <code>TRUE</code> if toolitem uses a drag window
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getUseDragWindow() {
        return gtk_tool_item_get_use_drag_window(getHandle());
    }

    /**
     * Sets whether ToolItem is visible when the toolbar is docked horizontally.
     * 
     * @param visibleHorizontal
     *            whether ToolItem is visible when the toolbar is in horizontal
     *            mode
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibleHorizontal(boolean visibleHorizontal) {
        gtk_tool_item_set_visible_horizontal(getHandle(), visibleHorizontal);
    }

    /**
     * Returns whether the ToolItem is visible on toolbars that are docked
     * horizontally.
     * 
     * @see ToolItem#setVisibleHorizontal(boolean)
     * @return <code>TRUE</code> if ToolItem is visible on toolbars that are
     *         docked horizontally
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getVisibleHorizontal() {
        return gtk_tool_item_get_visible_horizontal(getHandle());
    }

    /**
     * Sets whether ToolItem is visible when the toolbar is docked vertically.
     * Some tool items, such as text entries, are too wide to be useful on a
     * vertically docked toolbar. If <code>visibleVertical</code> is
     * <code>FALSE</code> ToolItem will not appear on toolbars that are docked
     * vertically.
     * 
     * @param visibleVertical
     *            whether toolitem is visible when the toolbar is in vertical
     *            mode
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVisibleVertical(boolean visibleVertical) {
        gtk_tool_item_set_visible_vertical(getHandle(), visibleVertical);
    }

    /**
     * Returns whether ToolItem is visible on toolbars that are docked
     * vertically.
     * 
     * @see ToolItem#setVisibleVertical(boolean)
     * @return <code>TRUE</code> if ToolItem is visible on toolbars that are
     *         docked vertically
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getVisibleVertical() {
        return gtk_tool_item_get_visible_vertical(getHandle());
    }

    /**
     * Sets whether ToolItem should be considered important. The
     * {@link ToolButton} class uses this property to determine whether to show
     * or hide its label when the toolbar style is
     * {@link ToolBarStyle#BOTH_HORIZ}. The result is that only tool buttons
     * with the IS_IMPORTANT property set have labels, an effect known as
     * "priority text".
     * 
     * @param isImportant
     *            whether the tool item should be considered important
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIsImportant(boolean isImportant) {
        gtk_tool_item_set_is_important(getHandle(), isImportant);
    }

    /**
     * Returns whether ToolItem is considered important.
     * 
     * @see ToolItem#setIsImportant(boolean)
     * @return <code>TRUE</code> if ToolItem is considered important
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getIsImportant() {
        return gtk_tool_item_get_is_important(getHandle());
    }

    /**
     * Returns the icon size used for ToolItem. Custom subclasses of ToolItem
     * should call this function to find out what size icons they should use.
     * 
     * @return a {@link IconSize} indicating the icon size used for ToolItem
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSize getIconSize() {
        return IconSize.intern(gtk_tool_item_get_icon_size(getHandle()));
    }

    /**
     * Returns the orientation used for ToolItem. Custom subclasses of ToolItem
     * should call this function to find out what size icons they should use.
     * 
     * @return a {@link Orientation} indicating the orientation used for
     *         ToolItem
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Orientation getOrientation() {
        return Orientation.intern(gtk_tool_item_get_orientation(getHandle()));
    }

    /**
     * Returns the toolbar style used for ToolItem. Custom subclasses of
     * ToolItem should call this function in the handler of the
     * GtkToolItem::toolbar_reconfigured signal to find out in what style the
     * toolbar is displayed and change themselves accordingly.
     * 
     * <p>
     * Possibilities are:
     * <ul>
     * <li>GTK_TOOLBAR_BOTH, meaning the tool item should show both an icon and
     * a label, stacked vertically.</li>
     * <li>GTK_TOOLBAR_ICONS, meaning the toolbar shows only icons.</li>
     * <li>GTK_TOOLBAR_TEXT, meaning the tool item should only show text.</li>
     * <li>GTK_TOOLBAR_BOTH_HORIZ, meaning the tool item should show both an
     * icon and a label, arranged horizontally (however, note the
     * GtkToolButton::has_text_horizontally that makes tool buttons not show
     * labels when the toolbar style is GTK_TOOLBAR_BOTH_HORIZ.</li>
     * </ul>
     * 
     * @return a {@link ToolBarStyle} indicating the toolbar style used for
     *         ToolItem.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ToolBarStyle getToolbarStyle() {
        return ToolBarStyle
                .intern(gtk_tool_item_get_toolbar_style(getHandle()));
    }

    /**
     * Returns the relief style of ToolItem. See
     * {@link Button#setRelief(ReliefStyle)}. Custom subclasses of ToolItem
     * should call this function in the handler of the
     * GtkToolItem::toolbar_reconfigured signal to find out the relief style of
     * buttons.
     * 
     * @return a {@link ReliefStyle} indicating the relief style used for
     *         ToolItem
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public ReliefStyle getReliefStyle() {
        return ReliefStyle.intern(gtk_tool_item_get_relief_style(getHandle()));
    }

    /**
     * Returns the {@link MenuItem} that was last set by {@link
     * ToolItem#setProxyMenuItem(String, MenuItem)} (ie the {@link MenuItem}
     * that is going to appear in the overflow menu).
     * 
     * @return the {@link MenuItem} that is going to appear in the overflow menu
     *         for tool_item.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuItem retrieveProxyMenuItem() {
        Handle hndl = gtk_tool_item_retrieve_proxy_menu_item(getHandle());
        return MenuItem.getMenuItem(hndl);
    }

    /**
     * If
     * <code>menuItemId<code> matches the string passed to {@link ToolItem#setProxyMenuItem(String, MenuItem)}
     * returns the corresponding {@link MenuItem}.
     * Custom subclasses of ToolItem should use this function to update their menu 
     * item when the ToolItem changes. That the <code>menuItemId</code> must match ensures 
     * that a ToolItem will not inadvertently change a menu item that they did not 
     * create.
     * @param menuItemId a string used to identify the menu item
     * @return the {@link MenuItem} passed to {@link ToolItem#setProxyMenuItem(String, MenuItem)} 
     * if the menuItemId matches
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MenuItem getProxyMenuItem(String menuItemId) {
        Handle hndl = gtk_tool_item_get_proxy_menu_item(getHandle(), menuItemId);
        return MenuItem.getMenuItem(hndl);
    }

    /**
     * Sets the {@link MenuItem} used in the toolbar overflow menu. The
     * <code>menuItemId</code> is used to identify the caller of this function
     * and should also be used with {@link ToolItem#getProxyMenuItem(String)}.
     * 
     * @param menuItemId
     *            a string used to identify <code>menuItem</code>
     * @param menuItem
     *            a {@link MenuItem} to be used in the overflow menu
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setProxyMenuItem(String menuItemId, MenuItem menuItem) {
        gtk_tool_item_set_proxy_menu_item(getHandle(), menuItemId, menuItem
                .getHandle());
    }

    /**
     * Calling this function signals to the toolbar that the overflow menu item
     * for this ToolItem has changed. If the overflow menu is visible when this
     * function is called, the menu will be rebuilt.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void rebuildMenu() {
        gtk_tool_item_rebuild_menu(getHandle());
    }

    native static final protected int gtk_tool_item_get_type();

    native static final protected Handle gtk_tool_item_new();

    native static final protected void gtk_tool_item_set_homogeneous(
            Handle toolitem, boolean homogeneous);

    native static final protected boolean gtk_tool_item_get_homogeneous(
            Handle toolitem);

    native static final protected void gtk_tool_item_set_expand(
            Handle toolitem, boolean expand);

    native static final protected boolean gtk_tool_item_get_expand(
            Handle toolitem);

    native static final protected void gtk_tool_item_set_tooltip(
            Handle toolitem, Handle tooltips, String tipText, String tipPrivate);

    native static final protected void gtk_tool_item_set_use_drag_window(
            Handle toolitem, boolean useDragWindow);

    native static final protected boolean gtk_tool_item_get_use_drag_window(
            Handle toolitem);

    native static final protected void gtk_tool_item_set_visible_horizontal(
            Handle toolitem, boolean visibleHorizontal);

    native static final protected boolean gtk_tool_item_get_visible_horizontal(
            Handle toolitem);

    native static final protected void gtk_tool_item_set_visible_vertical(
            Handle toolitem, boolean visibleVertical);

    native static final protected boolean gtk_tool_item_get_visible_vertical(
            Handle toolitem);

    native static final protected boolean gtk_tool_item_get_is_important(
            Handle toolitem);

    native static final protected void gtk_tool_item_set_is_important(
            Handle toolitem, boolean isImportant);

    native static final protected int gtk_tool_item_get_icon_size(
            Handle toolitem);

    native static final protected int gtk_tool_item_get_orientation(
            Handle toolitem);

    native static final protected int gtk_tool_item_get_toolbar_style(
            Handle toolitem);

    native static final protected int gtk_tool_item_get_relief_style(
            Handle toolitem);

    native static final protected Handle gtk_tool_item_retrieve_proxy_menu_item(
            Handle toolitem);

    native static final protected Handle gtk_tool_item_get_proxy_menu_item(
            Handle toolitem, String menuItemId);

    native static final protected void gtk_tool_item_set_proxy_menu_item(
            Handle toolitem, String menuItemId, Handle menuItem);

    // GTK 2.6 additions.
    native static final private void gtk_tool_item_rebuild_menu(Handle tool_item);

}
