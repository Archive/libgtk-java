/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;

/**
 * The DrawingArea widget is used for creating custom user interface elements.
 * It's essentially a blank widget you can draw on. Use
 * {@link Widget#getWindow()} to get the [Gdk] Window to draw on.
 * <p>
 * General practice is to use a {@link org.gnu.gtk.event.ExposeListener} and
 * then implement its exposeEvent(ExposeEvent) method and do your drawing when
 * ExposeEvent is fired.
 * <p>
 * The upstream API documentation also suggests that for some use cases it may
 * be easier to create a [Gtk] {@link org.gnu.gtk.Image} and then call it's
 * getPixbuf() method to get at the underlying [Gdk] {@link org.gnu.gdk.Pixbuf}
 * which you can make changes to and then referesh from.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.DrawingArea</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class DrawingArea extends Widget {
    public DrawingArea() {
        super(gtk_drawing_area_new());
    }

    /**
     * Construct a DrawingArea using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DrawingArea(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static DrawingArea getDrawingArea(Handle handle) {
        if (handle == null) {
            return null;
        }

        DrawingArea obj = (DrawingArea) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new DrawingArea(handle);
        }

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_drawing_area_get_type());
    }

    native static final protected int gtk_drawing_area_get_type();

    native static final protected Handle gtk_drawing_area_new();

    /*
     * Deprecated functions. native static final private void
     * gtk_drawing_area_size(Handle darea, int width, int height);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
