/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * The Fixed container enables your program to place widgets at a fixed location
 * with fixed sizes in pixels and to move widgets from one location to another.
 * Most applications should not use this container. This container could result
 * in truncated text, overlapping widgets and other display bugs as the user
 * changes their themes, fonts, or preferences.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Fixed</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Fixed extends Container {

    /**
     * Construct a new Fixed object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Fixed() {
        super(gtk_fixed_new());
    }

    /**
     * Construct a new Fixed from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Fixed(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Fixed getFixed(Handle handle) {
        if (handle == null)
            return null;

        Fixed obj = (Fixed) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new Fixed(handle);

        return obj;
    }

    /**
     * Add a Widget to the Fixed at the specified location.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void putWidget(Widget widget, int x, int y) {
        Fixed.gtk_fixed_put(getHandle(), widget.getHandle(), x, y);
    }

    /**
     * Move a widget to a new location.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveWidget(Widget widget, int x, int y) {
        Fixed.gtk_fixed_move(getHandle(), widget.getHandle(), x, y);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_fixed_get_type());
    }

    native static final protected int gtk_fixed_get_type();

    native static final protected Handle gtk_fixed_new();

    native static final protected void gtk_fixed_put(Handle fixed,
            Handle widget, int x, int y);

    native static final protected void gtk_fixed_move(Handle fixed,
            Handle widget, int x, int y);

    native static final protected void gtk_fixed_set_has_window(Handle fixed,
            boolean hasWindow);

    native static final protected boolean gtk_fixed_get_has_window(Handle fixed);

}
