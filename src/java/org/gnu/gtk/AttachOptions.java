/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Flags;

/**
 * Denotes the expansion properties that a widget will have when it (or it's
 * parent) is resized.
 * <dl>
 * <dt>EXPAND
 * <dd>The widget should expand to take up any extra space in its container
 * that has been allocated.
 * <dt>SHRINK
 * <dd>The widget should shrink as and when possible.
 * <dt>FILL
 * <dd>The widget should fill the space allocated to it. </dd>
 * 
 * @see Table
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.AttachOptions</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class AttachOptions extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _EXPAND = 1 << 0;

    static final public org.gnu.gtk.AttachOptions EXPAND = new org.gnu.gtk.AttachOptions(
            _EXPAND);

    static final private int _SHRINK = 1 << 1;

    static final public org.gnu.gtk.AttachOptions SHRINK = new org.gnu.gtk.AttachOptions(
            _SHRINK);

    static final private int _FILL = 1 << 2;

    static final public org.gnu.gtk.AttachOptions FILL = new org.gnu.gtk.AttachOptions(
            _FILL);

    static final private org.gnu.gtk.AttachOptions[] theInterned = new org.gnu.gtk.AttachOptions[] {
            new org.gnu.gtk.AttachOptions(0), EXPAND, SHRINK,
            new org.gnu.gtk.AttachOptions(3), FILL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.AttachOptions theSacrificialOne = new org.gnu.gtk.AttachOptions(
            0);

    static public org.gnu.gtk.AttachOptions intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.AttachOptions already = (org.gnu.gtk.AttachOptions) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.AttachOptions(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private AttachOptions(int value) {
        value_ = value;
    }

    public org.gnu.gtk.AttachOptions or(org.gnu.gtk.AttachOptions other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.AttachOptions and(org.gnu.gtk.AttachOptions other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.AttachOptions xor(org.gnu.gtk.AttachOptions other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.AttachOptions other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
