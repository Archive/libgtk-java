/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Enum;

/**
 * Represents the packing location of Box children.
 * 
 * <pre>
 *  START
 *  The child is packed into the start of the box.
 *  END
 *  The child is packed into the end of the box.
 * </pre>
 * 
 * @see Box
 * @see VBox
 * @see HBox
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.PackType</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class PackType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _START = 0;

    static final public org.gnu.gtk.PackType START = new org.gnu.gtk.PackType(
            _START);

    static final private int _END = 1;

    static final public org.gnu.gtk.PackType END = new org.gnu.gtk.PackType(
            _END);

    static final private org.gnu.gtk.PackType[] theInterned = new org.gnu.gtk.PackType[] {
            START, END }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gtk.PackType theSacrificialOne = new org.gnu.gtk.PackType(
            0);

    static public org.gnu.gtk.PackType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gtk.PackType already = (org.gnu.gtk.PackType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gtk.PackType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PackType(int value) {
        value_ = value;
    }

    public org.gnu.gtk.PackType or(org.gnu.gtk.PackType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gtk.PackType and(org.gnu.gtk.PackType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gtk.PackType xor(org.gnu.gtk.PackType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gtk.PackType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
