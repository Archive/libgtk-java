/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A single RadioButton performs the same basic function as a
 * {@link CheckButton}, as it's position in the object hierarchy reflects. It
 * is only when multiple RadioButtons are grouped together that they become a
 * different user interface component.
 * <p>
 * Every RadioButton is a member of some group of RadioButtons. When one is
 * selected, all of the other RadioButtons in the same group are deselected.
 * <p>
 * A RadioButton is created by the constructor passing a <code>null</code>
 * value for the RadioButton objection for the first object, and the First
 * object as a parameter for the remaining objects.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RadioButton</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RadioButton extends CheckButton {

    public RadioButton(RadioMenuItem[] group) {
        super(init1(group));
    }

    private static Handle init1(RadioMenuItem[] group) {
        Handle[] groupHandle;
        if (null != group) {
            groupHandle = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                groupHandle[i] = group[i].getHandle();
            }
        } else
            groupHandle = null;
        return gtk_radio_button_new(groupHandle);
    }

    public RadioButton(RadioButton[] group, String label, boolean hasMnemonic) {
        super(init2(group, label, hasMnemonic));
    }

    private static Handle init2(RadioButton[] group, String label,
            boolean hasMnemonic) {
        Handle[] groupHandle;
        if (null != group) {
            groupHandle = new Handle[group.length];
            for (int i = 0; i < group.length; i++) {
                groupHandle[i] = group[i].getHandle();
            }
        } else
            groupHandle = null;
        if (hasMnemonic) {
            return gtk_radio_button_new_with_mnemonic(groupHandle, label);
        } else {
            return gtk_radio_button_new_with_label(groupHandle, label);
        }
    }

    /**
     * Create a new RadioButton object adding it to the same <i>group</i> as
     * the provided RadioButton. If this is the first RadioButton pass
     * <code>null</code> for this parameter.
     * 
     * @param group
     *            A RadioButton that belongs to the group that we wish to add
     *            this newly constructed RadioButton to. If this is the first
     *            RadioButton in the group just pass <code>null</code>.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RadioButton(RadioButton group) {
        super(init3(group));
    }

    private static Handle init3(RadioButton group) {
        Handle hndl;
        if (null == group)
            hndl = null;
        else
            hndl = group.getHandle();
        return RadioButton.gtk_radio_button_new_from_widget(hndl);
    }

    /**
     * Create a new RadioButton object adding it to the same <i>group</i> as
     * the provided RadioButton. If this is the first RadioButton pass
     * <code>null</code> for this parameter.
     * 
     * @param group
     *            A RadioButton that belongs to the group that we wish to add
     *            this newly constructed RadioButton to. If this is the first
     *            RadioButton in the group just pass <code>null</code>.
     * @param label
     *            The text label to assign to this RadioButton.
     * @param hasMnemonic
     *            An indicator to inform the widget if the label contains a
     *            mnemonic.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RadioButton(RadioButton group, String label, boolean hasMnemonic) {
        super(init4(group, label, hasMnemonic));
    }

    private static Handle init4(RadioButton group, String label,
            boolean hasMnemonic) {
        Handle hndl;
        if (null == group)
            hndl = null;
        else
            hndl = group.getHandle();
        if (hasMnemonic) {
            return gtk_radio_button_new_with_mnemonic_from_widget(hndl, label);
        } else {
            return gtk_radio_button_new_with_label_from_widget(hndl, label);
        }
    }

    /**
     * Construct a radio button using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RadioButton(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static RadioButton getRadioButton(Handle handle) {
        if (handle == null) {
            return null;
        }

        RadioButton obj = (RadioButton) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new RadioButton(handle);
        }

        return obj;
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_radio_button_get_type());
    }

    native static final protected int gtk_radio_button_get_type();

    native static final protected Handle gtk_radio_button_new(Handle[] group);

    native static final protected Handle gtk_radio_button_new_from_widget(
            Handle group);

    native static final protected Handle gtk_radio_button_new_with_label(
            Handle[] group, String label);

    native static final protected Handle gtk_radio_button_new_with_label_from_widget(
            Handle group, String label);

    native static final protected Handle gtk_radio_button_new_with_mnemonic(
            Handle[] group, String label);

    native static final protected Handle gtk_radio_button_new_with_mnemonic_from_widget(
            Handle group, String label);

    native static final protected Handle[] gtk_radio_button_get_group(
            Handle button);

    native static final protected void gtk_radio_button_set_group(
            Handle button, Handle[] group);

}
