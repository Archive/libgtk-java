/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import java.util.Hashtable;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Internal helper class for classes that implement the {@link TreeSortable}
 * interface. Applications normally shouldn't use this class.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.TreeSortableHelper</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class TreeSortableHelper {

    private static Hashtable sortMethods = null;

    /** Private constructor so we don't instantiate this class. */
    private TreeSortableHelper() {
    }

    /**
     * Set the column in the list to sort on.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void setSortColumn(TreeSortable sortable, DataColumn column,
            SortType order) {
        gtk_tree_sortable_set_sort_column_id(sortable.getHandle(), column
                .getColumn(), order.getValue());
    }

    /**
     * Get a DataColumn object representing the currently sorted column. This is
     * not the same DataColumn used to create the store. It is only of type
     * DataColumn (not DataColumnString, etc). It can be compared with another
     * DataColumn object using the <tt>{@link DataColumn#equals}</tt> method.
     * 
     * @return A DataColumn object representing the currently sorted column or
     *         <tt>null</tt> if there is no column currently sorted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static DataColumn getSortColumn(TreeSortable sortable) {
        int colid = gtk_tree_sortable_get_sort_column_id(sortable.getHandle());
        if (colid < 0) {
            return null;
        } else {
            DataColumn ret = new DataColumnInt();
            ret.setColumn(colid);
            return ret;
        }
    }

    /**
     * Get the current sorting order of the store.
     * 
     * @return A SortType object defining the current sorting order of the store
     *         or <tt>null</tt> if there is no current sort order.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static SortType getSortOrder(TreeSortable sortable) {
        int stype = gtk_tree_sortable_get_sort_column_order(sortable
                .getHandle());
        if (stype == -1) {
            return null;
        } else {
            return SortType.intern(stype);
        }
    }

    /**
     * Set the class used to sort the list according to the values stored in the
     * given DataColumn.
     * <p>
     * "handleCompareFunc" is part of the TreeSortable interface.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void setSortMethod(TreeSortable sortable,
            TreeIterComparison method, DataColumn column) {
        // sortMethods is a hashtable of hashtables. that is, each key
        // (TreeSortable objects) refers to hashtable that contains
        // TreeIterComparison objects keyed to their column number.
        if (sortMethods == null) {
            sortMethods = new Hashtable();
        }
        Hashtable sortablehash = (Hashtable) sortMethods.get(sortable);
        if (sortablehash == null) {
            sortablehash = new Hashtable();
            sortMethods.put(sortable, sortablehash);
        }
        sortablehash.put(new Integer(column.getColumn()), method);
        gtk_tree_sortable_set_sort_func(sortable.getHandle(), sortable,
                "handleCompareFunc", column.getColumn());
    }

    /**
     * Get the TreeIterComparison object stored for this column. For internal
     * use only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static TreeIterComparison getMethod(TreeSortable sortable, int column) {
        Hashtable sortablehash = (Hashtable) sortMethods.get(sortable);
        if (sortablehash == null) {
            return null;
        }
        return (TreeIterComparison) sortablehash.get(new Integer(column));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_tree_sortable_get_type());
    }

    native static final private void gtk_tree_sortable_set_sort_column_id(
            Handle treeSortable, int column_id, int order);

    native static final private int gtk_tree_sortable_get_sort_column_id(
            Handle treeSortable);

    native static final private int gtk_tree_sortable_get_sort_column_order(
            Handle treeSortable);

    native static final private void gtk_tree_sortable_set_sort_func(
            Handle treeSortable, TreeSortable model, String compareFunc, int col);

    native static final private int gtk_tree_sortable_get_type();

    // Not implemented.
    // native static final public void gtk_tree_sortable_sort_column_changed(
    // Handle treeSortable );
    // native static final public void gtk_tree_sortable_set_default_sort_func(
    // Handle treeSortable, TreeSortable model, String compareFunc );
    // native static final public boolean
    // gtk_tree_sortable_has_default_sort_func( Handle treeSortable );

}
