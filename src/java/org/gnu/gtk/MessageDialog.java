/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * MessageDialog presents a dialog with an image representing the type of
 * message (Error, Question, etc.) alongside some message text. It's simply a
 * convenience widget; you could construct the equivalent of MessageDialog from
 * {@link Dialog} without too much effort, but MessageDialog saves typing.
 * 
 * <p>
 * The easiest way to do a modal message dialog is to use {@link #run()}.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.MessageDialog</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class MessageDialog extends Dialog {

    /**
     * Construct a new MessageDialog.
     * 
     * @param parent
     *            The parent Window of this MessageDialog.
     * @param flags
     *            Provides initialization information for the Dialog.
     * @param type
     *            The type of message to display.
     * @see MessageType
     * @param buttons
     *            What type of buttons should be on this dialog.
     * @param message
     *            The message to display.
     * @param hasMarkup
     *            Does the message String contain markup?
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MessageDialog(Window parent, DialogFlags flags, MessageType type,
            ButtonsType buttons, String message, boolean hasMarkup) {
        super(init(parent, flags, type, buttons, message, hasMarkup));
    }

    private static Handle init(Window parent, DialogFlags flags,
            MessageType type, ButtonsType buttons, String message,
            boolean hasMarkup) {
        Handle parentHandle = null;
        if (parent != null) {
            parentHandle = parent.getHandle();
        }
        if (hasMarkup) {
            return gtk_message_dialog_new_with_markup(parentHandle, flags
                    .getValue(), type.getValue(), buttons.getValue(), message);
        }
        return gtk_message_dialog_new(parentHandle, flags.getValue(), type
                .getValue(), buttons.getValue(), message);
    }

    /**
     * Construct a MessageDialog using a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MessageDialog(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static MessageDialog getMessageDialog(Handle handle) {
        if (handle == null)
            return null;

        MessageDialog obj = (MessageDialog) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new MessageDialog(handle);

        return obj;
    }

    /**
     * Sets the text of the message dialog to be <tt>markup</tt>, which is
     * marked up with the Pango text markup language.
     * 
     * @param markup
     *            The markup string.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMarkup(String markup) {
        gtk_message_dialog_set_markup(getHandle(), markup);
    }

    /**
     * Sets the secondary text of the message dialog to be <tt>text</tt>.
     * <p>
     * Note that setting a secondary text makes the primary text become bold,
     * unless you have provided explicit markup.
     * 
     * @since 2.6
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSecondaryText(String text) {
        gtk_message_dialog_format_secondary_text(getHandle(), text);
    }

    /**
     * Sets the secondary text of the message dialog to be <tt>markup</tt>,
     * which is marked up with the Pango text markup language.
     * <p>
     * Note that setting a secondary text makes the primary text become bold,
     * unless you have provided explicit markup.
     * 
     * @since 2.6
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSecondaryMarkup(String markup) {
        gtk_message_dialog_format_secondary_markup(getHandle(), markup);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gtk_message_dialog_get_type());
    }

    native static final protected int gtk_message_dialog_get_type();

    native static final protected Handle gtk_message_dialog_new(Handle parent,
            int flags, int type, int buttons, String message);

    native static final protected Handle gtk_message_dialog_new_with_markup(
            Handle parent, int flags, int type, int buttons, String message);

    native static final protected void gtk_message_dialog_set_markup(
            Handle mdialog, String str);

    // GTK 2.6 additions.
    native static final private void gtk_message_dialog_format_secondary_text(
            Handle message_dialog, String message_format);

    native static final private void gtk_message_dialog_format_secondary_markup(
            Handle message_dialog, String message_format);

}
