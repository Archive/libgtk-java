/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

public class Requisition extends Boxed {

    /**
     * Construct a new Requisition.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Requisition(int width, int height) {
        super(Requisition.gtk_requisition_new());
        Requisition.setWidth(getHandle(), width);
        Requisition.setHeight(getHandle(), height);
    }

    public Requisition(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Requisition getRequisition(Handle handle) {
        if (handle == null)
            return null;

        Requisition obj = (Requisition) Boxed.getBoxedFromHandle(handle);
        if (obj == null)
            obj = new Requisition(handle);

        return obj;
    }

    public int getWidth() {
        return getWidth(getHandle());
    }

    public void setWidth(int width) {
        setWidth(getHandle(), width);
    }

    public int getHeight() {
        return getHeight(getHandle());
    }

    public void setHeight(int height) {
        setHeight(getHandle(), height);
    }

    native static final protected Handle gtk_requisition_new();

    native static final protected int getWidth(Handle cptr);

    native static final protected void setWidth(Handle cptr, int width);

    native static final protected int getHeight(Handle cptr);

    native static final protected void setHeight(Handle cptr, int height);

    native static final protected void gtk_requisition_free(Handle requisition);

}
