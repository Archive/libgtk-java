/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.RadioAction</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class RadioAction extends ToggleAction {

    public RadioAction(String name, String label, String tooltip,
            String stockId, int value) {
        super(gtk_radio_action_new(name, label, tooltip, stockId, value));
    }

    RadioAction(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static RadioAction getRadioAction(Handle handle) {
        if (handle == null)
            return null;

        RadioAction obj = (RadioAction) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new RadioAction(handle);

        return obj;
    }

    public List getGroup() {
        Handle[] group = gtk_radio_action_get_group(getHandle());
        List ret = new ArrayList();
        for (int i = 0; i < group.length; i++) {
            GObject obj = getGObjectFromHandle(group[i]);
            RadioAction action;
            if (null != obj)
                action = (RadioAction) obj;
            else
                action = new RadioAction(group[i]);
            ret.add(action);
        }
        return ret;
    }

    public void setGroup(List group) {
        if (null == group)
            return;
        Handle[] values = new Handle[group.size()];
        for (int i = 0; i < group.size(); i++) {
            values[i] = ((RadioAction) group.get(i)).getHandle();
        }
        gtk_radio_action_set_group(getHandle(), values);
    }

    public int getCurrentValue() {
        return gtk_radio_action_get_current_value(getHandle());
    }

    native static final protected int gtk_radio_action_get_type();

    native static final protected Handle gtk_radio_action_new(String name,
            String label, String tooltip, String stockId, int value);

    native static final protected Handle[] gtk_radio_action_get_group(
            Handle action);

    native static final protected void gtk_radio_action_set_group(
            Handle action, Handle[] group);

    native static final protected int gtk_radio_action_get_current_value(
            Handle action);

}
