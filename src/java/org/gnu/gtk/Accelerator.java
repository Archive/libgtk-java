/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Accelerator</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
package org.gnu.gtk;

import org.gnu.gdk.KeyValue;
import org.gnu.gdk.ModifierType;

/**
 * @author ajocksch
 * @since 2.8.1
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.Accelerator</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Accelerator {
    /**
     * Determines whether a given keyv and modifier mask constitute a valid
     * keyboard accelerator. For example, the KeyChar.a key plus
     * ModifierType.CONTROL_MASK is valid - this is a "Ctrl+a" accelerator. But,
     * you can't, for instance, use the KeySymbol.Control_L keyval as an
     * accelerator.
     * 
     * @param key
     *            The key for the accelerator4
     * @param mods
     *            The modifiers for the accelerator4
     * @return true if the accelerator is valid, false otherwise
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static boolean isValid(int key, ModifierType mods) {
        return gtk_accelerator_valid(key, mods.getValue());
    }

    /**
     * Parses a string representing an accelerator. The format looks like "<Control>a"
     * or "<Shift><Alt>F1" or "<Release>z" (the last one is for key release).
     * The parser is fairly liberal and allows lower or upper case, and also
     * abbreviations such as "<Ctl>" and "<Ctrl>". If the parse fails, it will
     * return a KeySymbol with value 0.
     * 
     * @param accelerator
     *            The string to parse
     * @return The int (from {@link KeyValue}) corresponding to the key part of
     *         the accelerator
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static int parseKey(String accelerator) {
        int[] key = new int[1];
        gtk_accelerator_parse(accelerator, key, new int[1]);
        return key[0];
    }

    /**
     * Parses a string representing an accelerator. The format looks like "<Control>a"
     * or "<Shift><Alt>F1" or "<Release>z" (the last one is for key release).
     * The parser is fairly liberal and allows lower or upper case, and also
     * abbreviations such as "<Ctl>" and "<Ctrl>". If the parse fails, it will
     * return a ModifierType with value 0.
     * 
     * @param accelerator
     *            The string to parse
     * @return The ModifierType corresponding to the modifier mask of the
     *         accelerator
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ModifierType parseModifier(String accelerator) {
        int[] mods = new int[1];
        gtk_accelerator_parse(accelerator, new int[] { 0 }, mods);
        return ModifierType.intern(mods[0]);
    }

    /**
     * Converts an accelerator keyval and modifier mask into a string parseable
     * by {@link #parseKey(String)} or {@link #parseModifier(String)}. For
     * example, if you pass in KeySymbol.q and ModifierType.CONTROL_MASK, this
     * function returns "<Control>q". If you need to display accelerators in
     * the user interface, see {@link #getLabel(int, ModifierType)}.
     * 
     * @param key
     *            The key for the accelerator
     * @param mods
     *            The modifier mask for the accelerator
     * @return The string representation of the accelerator
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static String getName(int key, ModifierType mods) {
        return gtk_accelerator_name(key, mods.getValue());
    }

    /**
     * Converts an accelerator keyval and modifier mask into a string which can
     * be used to represent the accelerator to the user.
     * 
     * @param key
     *            The key for teh accelerator
     * @param mods
     *            The modifier mask for the accelerator
     * @return The user-readable string representation of the accelerator
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static String getLabel(int key, ModifierType mods) {
        return gtk_accelerator_get_label(key, mods.getValue());
    }

    /**
     * Sets the modifiers that will be considered significant for keyboard
     * accelerators. The default mod mask is CONTROL_MASK | SHIFT_MASK |
     * MOD1_MASK | SUPER_MASK | HYPER_MASK | META_MASK, that is, Control, Shift,
     * Alt, Super, Hyper and Meta. Other modifiers will by default be ignored by
     * AccelGroup. You must include at least the three modifiers Control, Shift
     * and Alt in any value you pass to this function. The default mod mask
     * should be changed on application startup, before using any accelerator
     * groups.
     * 
     * @param mask
     *            The new default modifier mask
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void setDefaultModifierMask(ModifierType mask) {
        gtk_accelerator_set_default_mod_mask(mask.getValue());
    }

    /**
     * Gets the value set by {@link #setDefaultModifierMask(ModifierType)}.
     * 
     * @return The default modifier mask
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static ModifierType getDefaultModifierMask() {
        return ModifierType.intern(gtk_accelerator_get_default_mod_mask());
    }

    native static protected boolean gtk_accelerator_valid(int key, int mods);

    native static protected void gtk_accelerator_parse(String accelerator,
            int[] key, int[] mods);

    native static protected String gtk_accelerator_name(int key, int mods);

    native static protected String gtk_accelerator_get_label(int key, int mods);

    native static protected void gtk_accelerator_set_default_mod_mask(
            int modMask);

    native static protected int gtk_accelerator_get_default_mod_mask();
}
