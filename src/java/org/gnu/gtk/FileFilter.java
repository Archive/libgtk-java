/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.FileFilter</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class FileFilter extends GObject {

    /**
     * Creates a new FileFilter with no rules added to it. Such a filter doesn't
     * accept any files, so is not particularly useful until you add rules with
     * addMimeType() or addPattern().
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FileFilter() {
        super(gtk_file_filter_new());
    }

    /**
     * Create a new FileFilter from a point to a native object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    FileFilter(Handle hndl) {
        super(hndl);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    static FileFilter getFileFilter(Handle handle) {
        if (handle == null)
            return null;

        FileFilter obj = (FileFilter) getGObjectFromHandle(handle);
        if (obj == null)
            obj = new FileFilter(handle);

        return obj;
    }

    /**
     * Sets the human readable name for the filter. This is the string that will
     * be displayed in the file selector user interface if there is a selectable
     * list of filters.
     * 
     * @param name
     *            The name of the filter.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setName(String name) {
        gtk_file_filter_set_name(getHandle(), name);
    }

    /**
     * Returns the human readable name of the file filter.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return gtk_file_filter_get_name(getHandle());
    }

    /**
     * Adds a rule allowing a given mime type to filter.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addMimeType(String mimeType) {
        gtk_file_filter_add_mime_type(getHandle(), mimeType);
    }

    /**
     * Adds a rule allowing a shell style glob to a filter.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addPattern(String pattern) {
        gtk_file_filter_add_pattern(getHandle(), pattern);
    }

    native static final protected int gtk_file_filter_get_type();

    native static final protected Handle gtk_file_filter_new();

    native static final protected void gtk_file_filter_set_name(Handle filter,
            String name);

    native static final protected String gtk_file_filter_get_name(Handle filter);

    native static final protected void gtk_file_filter_add_mime_type(
            Handle filter, String mimeType);

    native static final protected void gtk_file_filter_add_pattern(
            Handle filter, String pattern);

    native static final protected int gtk_file_filter_get_needed(Handle filter);

    native static final private void gtk_file_filter_add_pixbuf_formats(
            Handle filter);

    /*
     * We currently do not have a wrapper for FileFilterInfo. native static
     * final protected boolean gtk_file_filter_filter(Handle filter, Handle
     * filterInfo);
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
}
