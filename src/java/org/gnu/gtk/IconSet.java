/*
 * Java-Gnome Bindings Library
 * 
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 * 
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 */

package org.gnu.gtk;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * This object represents a set of Icons that are a variant of a particular
 * icon.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may in the future have an equivalent in
 *             java-gnome 4.0, try looking for
 *             <code>org.gnome.gtk.IconSet</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class IconSet extends Boxed {
    /**
     * Initialize an IconSet with a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSet(Handle handle) {
        super(handle);
    }

    /**
     * Internal static factory method to be used by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static IconSet getIconSet(Handle handle) {
        if (handle == null)
            return null;

        IconSet obj = (IconSet) Boxed.getBoxedFromHandle(handle);
        if (obj == null)
            obj = new IconSet(handle);

        return obj;
    }

    /**
     * Construct a new empty IconSet.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSet() {
        super(gtk_icon_set_new());
    }

    /**
     * Construct a new IconSet from a Pixbuf.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public IconSet(Pixbuf pixbuf) {
        super(gtk_icon_set_new_from_pixbuf(pixbuf.getHandle()));
    }

    /**
     * Icon sets have a list of {@link IconSource} which they use as base icons
     * for rendering icons in different states and sizes. The base images and
     * when to use them are described by a IconSource. This method adds an
     * IconSource to the IconSet.
     * 
     * @param iconSource
     *            The IconSource to add to this IconSet.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addSource(IconSource iconSource) {
        gtk_icon_set_add_source(getHandle(), iconSource.getHandle());
    }

    public int getType() {
        return gtk_icon_set_get_type();
    }

    native static final protected Handle gtk_icon_set_new();

    native static final protected Handle gtk_icon_set_new_from_pixbuf(
            Handle pixbuf);

    native static final protected void gtk_icon_set_ref(Handle iconSet);

    native static final protected void gtk_icon_set_unref(Handle iconSet);

    native static final protected Handle gtk_icon_set_render_icon(
            Handle iconSet, Handle style, int direction, int state, int size,
            Handle widget, String detail);

    native static final protected void gtk_icon_set_add_source(Handle iconSet,
            Handle source);

    native static final protected void gtk_icon_set_get_sizes(Handle iconSet,
            int[] sizes, int[] numSizes);

    native static final private int gtk_icon_set_get_type();

}
