/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

/**
 * The KeyChar class contains integer representations of the keystrokes
 * supported by GTK
 * 
 * @author ajocksch
 * 
 * @since 2.8.1
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for a
 *             similar class in <code>org.gnome.gdk</code>.
 */
public class KeyValue {

    static final public int a = 0x061;

    static final public int A = 0x041;

    static final public int aacute = 0x0e1;

    static final public int Aacute = 0x0c1;

    static final public int abovedot = 0x1ff;

    static final public int abreve = 0x1e3;

    static final public int Abreve = 0x1c3;

    static final public int AccessXEnable = 0xFE70;

    static final public int AccessXFeedbackEnable = 0xFE71;

    static final public int acircumflex = 0x0e2;

    static final public int Acircumflex = 0x0c2;

    static final public int acute = 0x0b4;

    static final public int adiaeresis = 0x0e4;

    static final public int Adiaeresis = 0x0c4;

    static final public int ae = 0x0e6;

    static final public int AE = 0x0c6;

    static final public int agrave = 0x0e0;

    static final public int Agrave = 0x0c0;

    static final public int AltL = 0xFFE9;

    static final public int AltR = 0xFFEA;

    static final public int AltCursor3270 = 0xFD10;

    static final public int amacron = 0x3e0;

    static final public int Amacron = 0x3c0;

    static final public int ampersand = 0x026;

    static final public int aogonek = 0x1b1;

    static final public int Aogonek = 0x1a1;

    static final public int apostrophe = 0x027;

    static final public int approximate = 0x8c8;

    static final public int Arabicain = 0x5d9;

    static final public int Arabicalef = 0x5c7;

    static final public int Arabicalefmaksura = 0x5e9;

    static final public int Arabicbeh = 0x5c8;

    static final public int Arabiccomma = 0x5ac;

    static final public int Arabicdad = 0x5d6;

    static final public int Arabicdal = 0x5cf;

    static final public int Arabicdamma = 0x5ef;

    static final public int Arabicdammatan = 0x5ec;

    static final public int Arabicfatha = 0x5ee;

    static final public int Arabicfathatan = 0x5eb;

    static final public int Arabicfeh = 0x5e1;

    static final public int Arabicghain = 0x5da;

    static final public int Arabicha = 0x5e7;

    static final public int Arabichah = 0x5cd;

    static final public int Arabichamza = 0x5c1;

    static final public int Arabichamzaonalef = 0x5c3;

    static final public int Arabichamzaonwaw = 0x5c4;

    static final public int Arabichamzaonyeh = 0x5c6;

    static final public int Arabichamzaunderalef = 0x5c5;

    static final public int Arabicheh = 0x5e7;

    static final public int Arabicjeem = 0x5cc;

    static final public int Arabickaf = 0x5e3;

    static final public int Arabickasra = 0x5f0;

    static final public int Arabickasratan = 0x5ed;

    static final public int Arabickhah = 0x5ce;

    static final public int Arabiclam = 0x5e4;

    static final public int Arabicmaddaonalef = 0x5c2;

    static final public int Arabicmeem = 0x5e5;

    static final public int Arabicnoon = 0x5e6;

    static final public int Arabicqaf = 0x5e2;

    static final public int Arabicquestionmark = 0x5bf;

    static final public int Arabicra = 0x5d1;

    static final public int Arabicsad = 0x5d5;

    static final public int Arabicseen = 0x5d3;

    static final public int Arabicsemicolon = 0x5bb;

    static final public int Arabicshadda = 0x5f1;

    static final public int Arabicsheen = 0x5d4;

    static final public int Arabicsukun = 0x5f2;

    static final public int Arabicswitch = 0xFF7E;

    static final public int Arabictah = 0x5d7;

    static final public int Arabictatweel = 0x5e0;

    static final public int Arabicteh = 0x5ca;

    static final public int Arabictehmarbuta = 0x5c9;

    static final public int Arabicthal = 0x5d0;

    static final public int Arabictheh = 0x5cb;

    static final public int Arabicwaw = 0x5e8;

    static final public int Arabicyeh = 0x5ea;

    static final public int Arabiczah = 0x5d8;

    static final public int Arabiczain = 0x5d2;

    static final public int aring = 0x0e5;

    static final public int Aring = 0x0c5;

    static final public int asciicircum = 0x05e;

    static final public int asciitilde = 0x07e;

    static final public int asterisk = 0x02a;

    static final public int at = 0x040;

    static final public int atilde = 0x0e3;

    static final public int Atilde = 0x0c3;

    static final public int Attn3270 = 0xFD0E;

    static final public int AudibleBellEnable = 0xFE7A;

    static final public int b = 0x062;

    static final public int B = 0x042;

    static final public int backslash = 0x05c;

    static final public int BackSpace = 0xFF08;

    static final public int BackTab3270 = 0xFD05;

    static final public int ballotcross = 0xaf4;

    static final public int bar = 0x07c;

    static final public int Begin = 0xFF58;

    static final public int blank = 0x9df;

    static final public int botintegral = 0x8a5;

    static final public int botleftparens = 0x8ac;

    static final public int botleftsqbracket = 0x8a8;

    static final public int botleftsummation = 0x8b2;

    static final public int botrightparens = 0x8ae;

    static final public int botrightsqbracket = 0x8aa;

    static final public int botrightsummation = 0x8b6;

    static final public int bott = 0x9f6;

    static final public int botvertsummationconnector = 0x8b4;

    static final public int BounceKeysEnable = 0xFE74;

    static final public int braceleft = 0x07b;

    static final public int braceright = 0x07d;

    static final public int bracketleft = 0x05b;

    static final public int bracketright = 0x05d;

    static final public int Break = 0xFF6B;

    static final public int breve = 0x1a2;

    static final public int brokenbar = 0x0a6;

    static final public int Byelorussianshortu = 0x6ae;

    static final public int ByelorussianSHORTU = 0x6be;

    static final public int c = 0x063;

    static final public int C = 0x043;

    static final public int cabovedot = 0x2e5;

    static final public int Cabovedot = 0x2c5;

    static final public int cacute = 0x1e6;

    static final public int Cacute = 0x1c6;

    static final public int Cancel = 0xFF69;

    static final public int CapsLock = 0xFFE5;

    static final public int careof = 0xab8;

    static final public int caret = 0xafc;

    static final public int caron = 0x1b7;

    static final public int ccaron = 0x1e8;

    static final public int Ccaron = 0x1c8;

    static final public int ccedilla = 0x0e7;

    static final public int Ccedilla = 0x0c7;

    static final public int ccircumflex = 0x2e6;

    static final public int Ccircumflex = 0x2c6;

    static final public int cedilla = 0x0b8;

    static final public int cent = 0x0a2;

    static final public int ChangeScreen3270 = 0xFD19;

    static final public int checkerboard = 0x9e1;

    static final public int checkmark = 0xaf3;

    static final public int circle = 0xbcf;

    static final public int Clear = 0xFF0B;

    static final public int club = 0xaec;

    static final public int Codeinput = 0xFF37;

    static final public int colon = 0x03a;

    static final public int ColonSign = 0x20a1;

    static final public int comma = 0x02c;

    static final public int ControlL = 0xFFE3;

    static final public int ControlR = 0xFFE4;

    static final public int Copy3270 = 0xFD15;

    static final public int copyright = 0x0a9;

    static final public int cr = 0x9e4;

    static final public int crossinglines = 0x9ee;

    static final public int CruzeiroSign = 0x20a2;

    static final public int currency = 0x0a4;

    static final public int cursor = 0xaff;

    static final public int CursorBlink3270 = 0xFD0F;

    static final public int CursorSelect3270 = 0xFD1C;

    static final public int Cyrillica = 0x6c1;

    static final public int CyrillicA = 0x6e1;

    static final public int Cyrillicbe = 0x6c2;

    static final public int CyrillicBE = 0x6e2;

    static final public int Cyrillicche = 0x6de;

    static final public int CyrillicCHE = 0x6fe;

    static final public int Cyrillicde = 0x6c4;

    static final public int CyrillicDE = 0x6e4;

    static final public int Cyrillicdzhe = 0x6af;

    static final public int CyrillicDZHE = 0x6bf;

    static final public int Cyrillice = 0x6dc;

    static final public int CyrillicE = 0x6fc;

    static final public int Cyrillicef = 0x6c6;

    static final public int CyrillicEF = 0x6e6;

    static final public int Cyrillicel = 0x6cc;

    static final public int CyrillicEL = 0x6ec;

    static final public int Cyrillicem = 0x6cd;

    static final public int CyrillicEM = 0x6ed;

    static final public int Cyrillicen = 0x6ce;

    static final public int CyrillicEN = 0x6ee;

    static final public int Cyrillicer = 0x6d2;

    static final public int CyrillicER = 0x6f2;

    static final public int Cyrillices = 0x6d3;

    static final public int CyrillicES = 0x6f3;

    static final public int Cyrillicghe = 0x6c7;

    static final public int CyrillicGHE = 0x6e7;

    static final public int Cyrillicha = 0x6c8;

    static final public int CyrillicHA = 0x6e8;

    static final public int Cyrillichardsign = 0x6df;

    static final public int CyrillicHARDSIGN = 0x6ff;

    static final public int Cyrillici = 0x6c9;

    static final public int CyrillicI = 0x6e9;

    static final public int Cyrillicie = 0x6c5;

    static final public int CyrillicIE = 0x6e5;

    static final public int Cyrillicio = 0x6a3;

    static final public int CyrillicIO = 0x6b3;

    static final public int Cyrillicje = 0x6a8;

    static final public int CyrillicJE = 0x6b8;

    static final public int Cyrillicka = 0x6cb;

    static final public int CyrillicKA = 0x6eb;

    static final public int Cyrilliclje = 0x6a9;

    static final public int CyrillicLJE = 0x6b9;

    static final public int Cyrillicnje = 0x6aa;

    static final public int CyrillicNJE = 0x6ba;

    static final public int Cyrillico = 0x6cf;

    static final public int CyrillicO = 0x6ef;

    static final public int Cyrillicpe = 0x6d0;

    static final public int CyrillicPE = 0x6f0;

    static final public int Cyrillicsha = 0x6db;

    static final public int CyrillicSHA = 0x6fb;

    static final public int Cyrillicshcha = 0x6dd;

    static final public int CyrillicSHCHA = 0x6fd;

    static final public int Cyrillicshorti = 0x6ca;

    static final public int CyrillicSHORTI = 0x6ea;

    static final public int Cyrillicsoftsign = 0x6d8;

    static final public int CyrillicSOFTSIGN = 0x6f8;

    static final public int Cyrillicte = 0x6d4;

    static final public int CyrillicTE = 0x6f4;

    static final public int Cyrillictse = 0x6c3;

    static final public int CyrillicTSE = 0x6e3;

    static final public int Cyrillicu = 0x6d5;

    static final public int CyrillicU = 0x6f5;

    static final public int Cyrillicve = 0x6d7;

    static final public int CyrillicVE = 0x6f7;

    static final public int Cyrillicya = 0x6d1;

    static final public int CyrillicYA = 0x6f1;

    static final public int Cyrillicyeru = 0x6d9;

    static final public int CyrillicYERU = 0x6f9;

    static final public int Cyrillicyu = 0x6c0;

    static final public int CyrillicYU = 0x6e0;

    static final public int Cyrillicze = 0x6da;

    static final public int CyrillicZE = 0x6fa;

    static final public int Cyrilliczhe = 0x6d6;

    static final public int CyrillicZHE = 0x6f6;

    static final public int d = 0x064;

    static final public int D = 0x044;

    static final public int dagger = 0xaf1;

    static final public int dcaron = 0x1ef;

    static final public int Dcaron = 0x1cf;

    static final public int deadabovedot = 0xFE56;

    static final public int deadabovering = 0xFE58;

    static final public int deadacute = 0xFE51;

    static final public int deadbelowdot = 0xFE60;

    static final public int deadbreve = 0xFE55;

    static final public int deadcaron = 0xFE5A;

    static final public int deadcedilla = 0xFE5B;

    static final public int deadcircumflex = 0xFE52;

    static final public int deaddiaeresis = 0xFE57;

    static final public int deaddoubleacute = 0xFE59;

    static final public int deadgrave = 0xFE50;

    static final public int deadhook = 0xFE61;

    static final public int deadhorn = 0xFE62;

    static final public int deadiota = 0xFE5D;

    static final public int deadmacron = 0xFE54;

    static final public int deadogonek = 0xFE5C;

    static final public int deadsemivoicedsound = 0xFE5F;

    static final public int deadtilde = 0xFE53;

    static final public int deadvoicedsound = 0xFE5E;

    static final public int decimalpoint = 0xabd;

    static final public int degree = 0x0b0;

    static final public int Delete = 0xFFFF;

    static final public int DeleteWord3270 = 0xFD1A;

    static final public int diaeresis = 0x0a8;

    static final public int diamond = 0xaed;

    static final public int digitspace = 0xaa5;

    static final public int division = 0x0f7;

    static final public int dollar = 0x024;

    static final public int DongSign = 0x20ab;

    static final public int doubbaselinedot = 0xaaf;

    static final public int doubleacute = 0x1bd;

    static final public int doubledagger = 0xaf2;

    static final public int doublelowquotemark = 0xafe;

    static final public int Down = 0xFF54;

    static final public int downarrow = 0x8fe;

    static final public int downcaret = 0xba8;

    static final public int downshoe = 0xbd6;

    static final public int downstile = 0xbc4;

    static final public int downtack = 0xbc2;

    static final public int dstroke = 0x1f0;

    static final public int Dstroke = 0x1d0;

    static final public int Duplicate3270 = 0xFD01;

    static final public int e = 0x065;

    static final public int E = 0x045;

    static final public int eabovedot = 0x3ec;

    static final public int Eabovedot = 0x3cc;

    static final public int eacute = 0x0e9;

    static final public int Eacute = 0x0c9;

    static final public int ecaron = 0x1ec;

    static final public int Ecaron = 0x1cc;

    static final public int ecircumflex = 0x0ea;

    static final public int Ecircumflex = 0x0ca;

    static final public int EcuSign = 0x20a0;

    static final public int ediaeresis = 0x0eb;

    static final public int Ediaeresis = 0x0cb;

    static final public int egrave = 0x0e8;

    static final public int Egrave = 0x0c8;

    static final public int EisuShift = 0xFF2F;

    static final public int Eisutoggle = 0xFF30;

    static final public int ellipsis = 0xaae;

    static final public int em3space = 0xaa3;

    static final public int em4space = 0xaa4;

    static final public int emacron = 0x3ba;

    static final public int Emacron = 0x3aa;

    static final public int emdash = 0xaa9;

    static final public int emfilledcircle = 0xade;

    static final public int emfilledrect = 0xadf;

    static final public int emopencircle = 0xace;

    static final public int emopenrectangle = 0xacf;

    static final public int emspace = 0xaa1;

    static final public int End = 0xFF57;

    static final public int endash = 0xaaa;

    static final public int enfilledcircbullet = 0xae6;

    static final public int enfilledsqbullet = 0xae7;

    static final public int eng = 0x3bf;

    static final public int ENG = 0x3bd;

    static final public int enopencircbullet = 0xae0;

    static final public int enopensquarebullet = 0xae1;

    static final public int enspace = 0xaa2;

    static final public int Enter3270 = 0xFD1E;

    static final public int eogonek = 0x1ea;

    static final public int Eogonek = 0x1ca;

    static final public int equal = 0x03d;

    static final public int EraseEOF3270 = 0xFD06;

    static final public int EraseInput3270 = 0xFD07;

    static final public int Escape = 0xFF1B;

    static final public int eth = 0x0f0;

    static final public int Eth = 0x0d0;

    static final public int ETH = 0x0d0;

    static final public int EuroSign = 0x20ac;

    static final public int exclam = 0x021;

    static final public int exclamdown = 0x0a1;

    static final public int Execute = 0xFF62;

    static final public int ExSelect3270 = 0xFD1B;

    static final public int f = 0x066;

    static final public int F = 0x046;

    static final public int F1 = 0xFFBE;

    static final public int F10 = 0xFFC7;

    static final public int F11 = 0xFFC8;

    static final public int F12 = 0xFFC9;

    static final public int F13 = 0xFFCA;

    static final public int F14 = 0xFFCB;

    static final public int F15 = 0xFFCC;

    static final public int F16 = 0xFFCD;

    static final public int F17 = 0xFFCE;

    static final public int F18 = 0xFFCF;

    static final public int F19 = 0xFFD0;

    static final public int F2 = 0xFFBF;

    static final public int F20 = 0xFFD1;

    static final public int F21 = 0xFFD2;

    static final public int F22 = 0xFFD3;

    static final public int F23 = 0xFFD4;

    static final public int F24 = 0xFFD5;

    static final public int F25 = 0xFFD6;

    static final public int F26 = 0xFFD7;

    static final public int F27 = 0xFFD8;

    static final public int F28 = 0xFFD9;

    static final public int F29 = 0xFFDA;

    static final public int F3 = 0xFFC0;

    static final public int F30 = 0xFFDB;

    static final public int F31 = 0xFFDC;

    static final public int F32 = 0xFFDD;

    static final public int F33 = 0xFFDE;

    static final public int F34 = 0xFFDF;

    static final public int F35 = 0xFFE0;

    static final public int F4 = 0xFFC1;

    static final public int F5 = 0xFFC2;

    static final public int F6 = 0xFFC3;

    static final public int F7 = 0xFFC4;

    static final public int F8 = 0xFFC5;

    static final public int F9 = 0xFFC6;

    static final public int femalesymbol = 0xaf8;

    static final public int ff = 0x9e3;

    static final public int FFrancSign = 0x20a3;

    static final public int FieldMark3270 = 0xFD02;

    static final public int figdash = 0xabb;

    static final public int filledlefttribullet = 0xadc;

    static final public int filledrectbullet = 0xadb;

    static final public int filledrighttribullet = 0xadd;

    static final public int filledtribulletdown = 0xae9;

    static final public int filledtribulletup = 0xae8;

    static final public int Find = 0xFF68;

    static final public int FirstVirtualScreen = 0xFED0;

    static final public int fiveeighths = 0xac5;

    static final public int fivesixths = 0xab7;

    static final public int fourfifths = 0xab5;

    static final public int function = 0x8f6;

    static final public int g = 0x067;

    static final public int G = 0x047;

    static final public int gabovedot = 0x2f5;

    static final public int Gabovedot = 0x2d5;

    static final public int gbreve = 0x2bb;

    static final public int Gbreve = 0x2ab;

    static final public int gcedilla = 0x3bb;

    static final public int Gcedilla = 0x3ab;

    static final public int gcircumflex = 0x2f8;

    static final public int Gcircumflex = 0x2d8;

    static final public int grave = 0x060;

    static final public int greater = 0x03e;

    static final public int greaterthanequal = 0x8be;

    static final public int Greekaccentdieresis = 0x7ae;

    static final public int Greekalpha = 0x7e1;

    static final public int GreekALPHA = 0x7c1;

    static final public int Greekalphaaccent = 0x7b1;

    static final public int GreekALPHAaccent = 0x7a1;

    static final public int Greekbeta = 0x7e2;

    static final public int GreekBETA = 0x7c2;

    static final public int Greekchi = 0x7f7;

    static final public int GreekCHI = 0x7d7;

    static final public int Greekdelta = 0x7e4;

    static final public int GreekDELTA = 0x7c4;

    static final public int Greekepsilon = 0x7e5;

    static final public int GreekEPSILON = 0x7c5;

    static final public int Greekepsilonaccent = 0x7b2;

    static final public int GreekEPSILONaccent = 0x7a2;

    static final public int Greeketa = 0x7e7;

    static final public int GreekETA = 0x7c7;

    static final public int Greeketaaccent = 0x7b3;

    static final public int GreekETAaccent = 0x7a3;

    static final public int Greekfinalsmallsigma = 0x7f3;

    static final public int Greekgamma = 0x7e3;

    static final public int GreekGAMMA = 0x7c3;

    static final public int Greekhorizbar = 0x7af;

    static final public int Greekiota = 0x7e9;

    static final public int GreekIOTA = 0x7c9;

    static final public int Greekiotaaccent = 0x7b4;

    static final public int GreekIOTAaccent = 0x7a4;

    static final public int Greekiotaaccentdieresis = 0x7b6;

    static final public int GreekIOTAdiaeresis = 0x7a5;

    static final public int Greekiotadieresis = 0x7b5;

    static final public int Greekkappa = 0x7ea;

    static final public int GreekKAPPA = 0x7ca;

    static final public int Greeklambda = 0x7eb;

    static final public int GreekLAMBDA = 0x7cb;

    static final public int Greeklamda = 0x7eb;

    static final public int GreekLAMDA = 0x7cb;

    static final public int Greekmu = 0x7ec;

    static final public int GreekMU = 0x7cc;

    static final public int Greeknu = 0x7ed;

    static final public int GreekNU = 0x7cd;

    static final public int Greekomega = 0x7f9;

    static final public int GreekOMEGA = 0x7d9;

    static final public int Greekomegaaccent = 0x7bb;

    static final public int GreekOMEGAaccent = 0x7ab;

    static final public int Greekomicron = 0x7ef;

    static final public int GreekOMICRON = 0x7cf;

    static final public int Greekomicronaccent = 0x7b7;

    static final public int GreekOMICRONaccent = 0x7a7;

    static final public int Greekphi = 0x7f6;

    static final public int GreekPHI = 0x7d6;

    static final public int Greekpi = 0x7f0;

    static final public int GreekPI = 0x7d0;

    static final public int Greekpsi = 0x7f8;

    static final public int GreekPSI = 0x7d8;

    static final public int Greekrho = 0x7f1;

    static final public int GreekRHO = 0x7d1;

    static final public int Greeksigma = 0x7f2;

    static final public int GreekSIGMA = 0x7d2;

    static final public int Greekswitch = 0xFF7E;

    static final public int Greektau = 0x7f4;

    static final public int GreekTAU = 0x7d4;

    static final public int Greektheta = 0x7e8;

    static final public int GreekTHETA = 0x7c8;

    static final public int Greekupsilon = 0x7f5;

    static final public int GreekUPSILON = 0x7d5;

    static final public int Greekupsilonaccent = 0x7b8;

    static final public int GreekUPSILONaccent = 0x7a8;

    static final public int Greekupsilonaccentdieresis = 0x7ba;

    static final public int Greekupsilondieresis = 0x7b9;

    static final public int GreekUPSILONdieresis = 0x7a9;

    static final public int Greekxi = 0x7ee;

    static final public int GreekXI = 0x7ce;

    static final public int Greekzeta = 0x7e6;

    static final public int GreekZETA = 0x7c6;

    static final public int guillemotleft = 0x0ab;

    static final public int guillemotright = 0x0bb;

    static final public int h = 0x068;

    static final public int H = 0x048;

    static final public int hairspace = 0xaa8;

    static final public int Hangul = 0xff31;

    static final public int HangulA = 0xebf;

    static final public int HangulAE = 0xec0;

    static final public int HangulAraeA = 0xef6;

    static final public int HangulAraeAE = 0xef7;

    static final public int HangulBanja = 0xff39;

    static final public int HangulCieuc = 0xeba;

    static final public int HangulCodeinput = 0xff37;

    static final public int HangulDikeud = 0xea7;

    static final public int HangulE = 0xec4;

    static final public int HangulEnd = 0xff33;

    static final public int HangulEO = 0xec3;

    static final public int HangulEU = 0xed1;

    static final public int HangulHanja = 0xff34;

    static final public int HangulHieuh = 0xebe;

    static final public int HangulI = 0xed3;

    static final public int HangulIeung = 0xeb7;

    static final public int HangulJCieuc = 0xeea;

    static final public int HangulJDikeud = 0xeda;

    static final public int HangulJHieuh = 0xeee;

    static final public int HangulJIeung = 0xee8;

    static final public int HangulJJieuj = 0xee9;

    static final public int HangulJKhieuq = 0xeeb;

    static final public int HangulJKiyeog = 0xed4;

    static final public int HangulJKiyeogSios = 0xed6;

    static final public int HangulJKkogjiDalrinIeung = 0xef9;

    static final public int HangulJMieum = 0xee3;

    static final public int HangulJNieun = 0xed7;

    static final public int HangulJNieunHieuh = 0xed9;

    static final public int HangulJNieunJieuj = 0xed8;

    static final public int HangulJPanSios = 0xef8;

    static final public int HangulJPhieuf = 0xeed;

    static final public int HangulJPieub = 0xee4;

    static final public int HangulJPieubSios = 0xee5;

    static final public int HangulJRieul = 0xedb;

    static final public int HangulJRieulHieuh = 0xee2;

    static final public int HangulJRieulKiyeog = 0xedc;

    static final public int HangulJRieulMieum = 0xedd;

    static final public int HangulJRieulPhieuf = 0xee1;

    static final public int HangulJRieulPieub = 0xede;

    static final public int HangulJRieulSios = 0xedf;

    static final public int HangulJRieulTieut = 0xee0;

    static final public int HangulJSios = 0xee6;

    static final public int HangulJSsangKiyeog = 0xed5;

    static final public int HangulJSsangSios = 0xee7;

    static final public int HangulJTieut = 0xeec;

    static final public int HangulJYeorinHieuh = 0xefa;

    static final public int HangulJamo = 0xff35;

    static final public int HangulJeonja = 0xff38;

    static final public int HangulJieuj = 0xeb8;

    static final public int HangulKhieuq = 0xebb;

    static final public int HangulKiyeog = 0xea1;

    static final public int HangulKiyeogSios = 0xea3;

    static final public int HangulKkogjiDalrinIeung = 0xef3;

    static final public int HangulMieum = 0xeb1;

    static final public int HangulMultipleCandidate = 0xff3d;

    static final public int HangulNieun = 0xea4;

    static final public int HangulNieunHieuh = 0xea6;

    static final public int HangulNieunJieuj = 0xea5;

    static final public int HangulO = 0xec7;

    static final public int HangulOE = 0xeca;

    static final public int HangulPanSios = 0xef2;

    static final public int HangulPhieuf = 0xebd;

    static final public int HangulPieub = 0xeb2;

    static final public int HangulPieubSios = 0xeb4;

    static final public int HangulPostHanja = 0xff3b;

    static final public int HangulPreHanja = 0xff3a;

    static final public int HangulPreviousCandidate = 0xff3e;

    static final public int HangulRieul = 0xea9;

    static final public int HangulRieulHieuh = 0xeb0;

    static final public int HangulRieulKiyeog = 0xeaa;

    static final public int HangulRieulMieum = 0xeab;

    static final public int HangulRieulPhieuf = 0xeaf;

    static final public int HangulRieulPieub = 0xeac;

    static final public int HangulRieulSios = 0xead;

    static final public int HangulRieulTieut = 0xeae;

    static final public int HangulRieulYeorinHieuh = 0xeef;

    static final public int HangulRomaja = 0xff36;

    static final public int HangulSingleCandidate = 0xff3c;

    static final public int HangulSios = 0xeb5;

    static final public int HangulSpecial = 0xff3f;

    static final public int HangulSsangDikeud = 0xea8;

    static final public int HangulSsangJieuj = 0xeb9;

    static final public int HangulSsangKiyeog = 0xea2;

    static final public int HangulSsangPieub = 0xeb3;

    static final public int HangulSsangSios = 0xeb6;

    static final public int HangulStart = 0xff32;

    static final public int HangulSunkyeongeumMieum = 0xef0;

    static final public int HangulSunkyeongeumPhieuf = 0xef4;

    static final public int HangulSunkyeongeumPieub = 0xef1;

    static final public int Hangulswitch = 0xFF7E;

    static final public int HangulTieut = 0xebc;

    static final public int HangulU = 0xecc;

    static final public int HangulWA = 0xec8;

    static final public int HangulWAE = 0xec9;

    static final public int HangulWE = 0xece;

    static final public int HangulWEO = 0xecd;

    static final public int HangulWI = 0xecf;

    static final public int HangulYA = 0xec1;

    static final public int HangulYAE = 0xec2;

    static final public int HangulYE = 0xec6;

    static final public int HangulYEO = 0xec5;

    static final public int HangulYeorinHieuh = 0xef5;

    static final public int HangulYI = 0xed2;

    static final public int HangulYO = 0xecb;

    static final public int HangulYU = 0xed0;

    static final public int Hankaku = 0xFF29;

    static final public int hcircumflex = 0x2b6;

    static final public int Hcircumflex = 0x2a6;

    static final public int heart = 0xaee;

    static final public int hebrewaleph = 0xce0;

    static final public int hebrewayin = 0xcf2;

    static final public int hebrewbet = 0xce1;

    static final public int hebrewbeth = 0xce1;

    static final public int hebrewchet = 0xce7;

    static final public int hebrewdalet = 0xce3;

    static final public int hebrewdaleth = 0xce3;

    static final public int hebrewdoublelowline = 0xcdf;

    static final public int hebrewfinalkaph = 0xcea;

    static final public int hebrewfinalmem = 0xced;

    static final public int hebrewfinalnun = 0xcef;

    static final public int hebrewfinalpe = 0xcf3;

    static final public int hebrewfinalzade = 0xcf5;

    static final public int hebrewfinalzadi = 0xcf5;

    static final public int hebrewgimel = 0xce2;

    static final public int hebrewgimmel = 0xce2;

    static final public int hebrewhe = 0xce4;

    static final public int hebrewhet = 0xce7;

    static final public int hebrewkaph = 0xceb;

    static final public int hebrewkuf = 0xcf7;

    static final public int hebrewlamed = 0xcec;

    static final public int hebrewmem = 0xcee;

    static final public int hebrewnun = 0xcf0;

    static final public int hebrewpe = 0xcf4;

    static final public int hebrewqoph = 0xcf7;

    static final public int hebrewresh = 0xcf8;

    static final public int hebrewsamech = 0xcf1;

    static final public int hebrewsamekh = 0xcf1;

    static final public int hebrewshin = 0xcf9;

    static final public int Hebrewswitch = 0xFF7E;

    static final public int hebrewtaf = 0xcfa;

    static final public int hebrewtaw = 0xcfa;

    static final public int hebrewtet = 0xce8;

    static final public int hebrewteth = 0xce8;

    static final public int hebrewwaw = 0xce5;

    static final public int hebrewyod = 0xce9;

    static final public int hebrewzade = 0xcf6;

    static final public int hebrewzadi = 0xcf6;

    static final public int hebrewzain = 0xce6;

    static final public int hebrewzayin = 0xce6;

    static final public int Help = 0xFF6A;

    static final public int Henkan = 0xFF23;

    static final public int HenkanMode = 0xFF23;

    static final public int hexagram = 0xada;

    static final public int Hiragana = 0xFF25;

    static final public int HiraganaKatakana = 0xFF27;

    static final public int Home = 0xFF50;

    static final public int horizconnector = 0x8a3;

    static final public int horizlinescan1 = 0x9ef;

    static final public int horizlinescan3 = 0x9f0;

    static final public int horizlinescan5 = 0x9f1;

    static final public int horizlinescan7 = 0x9f2;

    static final public int horizlinescan9 = 0x9f3;

    static final public int hstroke = 0x2b1;

    static final public int Hstroke = 0x2a1;

    static final public int ht = 0x9e2;

    static final public int HyperL = 0xFFED;

    static final public int HyperR = 0xFFEE;

    static final public int hyphen = 0x0ad;

    static final public int i = 0x069;

    static final public int I = 0x049;

    static final public int Iabovedot = 0x2a9;

    static final public int iacute = 0x0ed;

    static final public int Iacute = 0x0cd;

    static final public int icircumflex = 0x0ee;

    static final public int Icircumflex = 0x0ce;

    static final public int Ident3270 = 0xFD13;

    static final public int identical = 0x8cf;

    static final public int idiaeresis = 0x0ef;

    static final public int Idiaeresis = 0x0cf;

    static final public int idotless = 0x2b9;

    static final public int ifonlyif = 0x8cd;

    static final public int igrave = 0x0ec;

    static final public int Igrave = 0x0cc;

    static final public int imacron = 0x3ef;

    static final public int Imacron = 0x3cf;

    static final public int implies = 0x8ce;

    static final public int includedin = 0x8da;

    static final public int includes = 0x8db;

    static final public int infinity = 0x8c2;

    static final public int Insert = 0xFF63;

    static final public int integral = 0x8bf;

    static final public int intersection = 0x8dc;

    static final public int iogonek = 0x3e7;

    static final public int Iogonek = 0x3c7;

    static final public int ISOCenterObject = 0xFE33;

    static final public int ISOContinuousUnderline = 0xFE30;

    static final public int ISODiscontinuousUnderline = 0xFE31;

    static final public int ISOEmphasize = 0xFE32;

    static final public int ISOEnter = 0xFE34;

    static final public int ISOFastCursorDown = 0xFE2F;

    static final public int ISOFastCursorLeft = 0xFE2C;

    static final public int ISOFastCursorRight = 0xFE2D;

    static final public int ISOFastCursorUp = 0xFE2E;

    static final public int ISOFirstGroup = 0xFE0C;

    static final public int ISOFirstGroupLock = 0xFE0D;

    static final public int ISOGroupLatch = 0xFE06;

    static final public int ISOGroupLock = 0xFE07;

    static final public int ISOGroupShift = 0xFF7E;

    static final public int ISOLastGroup = 0xFE0E;

    static final public int ISOLastGroupLock = 0xFE0F;

    static final public int ISOLeftTab = 0xFE20;

    static final public int ISOLevel2Latch = 0xFE02;

    static final public int ISOLevel3Latch = 0xFE04;

    static final public int ISOLevel3Lock = 0xFE05;

    static final public int ISOLevel3Shift = 0xFE03;

    static final public int ISOLock = 0xFE01;

    static final public int ISOMoveLineDown = 0xFE22;

    static final public int ISOMoveLineUp = 0xFE21;

    static final public int ISONextGroup = 0xFE08;

    static final public int ISONextGroupLock = 0xFE09;

    static final public int ISOPartialLineDown = 0xFE24;

    static final public int ISOPartialLineUp = 0xFE23;

    static final public int ISOPartialSpaceLeft = 0xFE25;

    static final public int ISOPartialSpaceRight = 0xFE26;

    static final public int ISOPrevGroup = 0xFE0A;

    static final public int ISOPrevGroupLock = 0xFE0B;

    static final public int ISOReleaseBothMargins = 0xFE2B;

    static final public int ISOReleaseMarginLeft = 0xFE29;

    static final public int ISOReleaseMarginRight = 0xFE2A;

    static final public int ISOSetMarginLeft = 0xFE27;

    static final public int ISOSetMarginRight = 0xFE28;

    static final public int itilde = 0x3b5;

    static final public int Itilde = 0x3a5;

    static final public int j = 0x06a;

    static final public int J = 0x04a;

    static final public int jcircumflex = 0x2bc;

    static final public int Jcircumflex = 0x2ac;

    static final public int jot = 0xbca;

    static final public int Jump3270 = 0xFD12;

    static final public int k = 0x06b;

    static final public int K = 0x04b;

    static final public int kanaa = 0x4a7;

    static final public int kanaA = 0x4b1;

    static final public int kanaCHI = 0x4c1;

    static final public int kanaclosingbracket = 0x4a3;

    static final public int kanacomma = 0x4a4;

    static final public int kanaconjunctive = 0x4a5;

    static final public int kanae = 0x4aa;

    static final public int kanaE = 0x4b4;

    static final public int kanaFU = 0x4cc;

    static final public int kanafullstop = 0x4a1;

    static final public int kanaHA = 0x4ca;

    static final public int kanaHE = 0x4cd;

    static final public int kanaHI = 0x4cb;

    static final public int kanaHO = 0x4ce;

    static final public int kanaHU = 0x4cc;

    static final public int kanai = 0x4a8;

    static final public int kanaI = 0x4b2;

    static final public int kanaKA = 0x4b6;

    static final public int kanaKE = 0x4b9;

    static final public int kanaKI = 0x4b7;

    static final public int kanaKO = 0x4ba;

    static final public int kanaKU = 0x4b8;

    static final public int KanaLock = 0xFF2D;

    static final public int kanaMA = 0x4cf;

    static final public int kanaME = 0x4d2;

    static final public int kanaMI = 0x4d0;

    static final public int kanamiddledot = 0x4a5;

    static final public int kanaMO = 0x4d3;

    static final public int kanaMU = 0x4d1;

    static final public int kanaN = 0x4dd;

    static final public int kanaNA = 0x4c5;

    static final public int kanaNE = 0x4c8;

    static final public int kanaNI = 0x4c6;

    static final public int kanaNO = 0x4c9;

    static final public int kanaNU = 0x4c7;

    static final public int kanao = 0x4ab;

    static final public int kanaO = 0x4b5;

    static final public int kanaopeningbracket = 0x4a2;

    static final public int kanaRA = 0x4d7;

    static final public int kanaRE = 0x4da;

    static final public int kanaRI = 0x4d8;

    static final public int kanaRO = 0x4db;

    static final public int kanaRU = 0x4d9;

    static final public int kanaSA = 0x4bb;

    static final public int kanaSE = 0x4be;

    static final public int kanaSHI = 0x4bc;

    static final public int KanaShift = 0xFF2E;

    static final public int kanaSO = 0x4bf;

    static final public int kanaSU = 0x4bd;

    static final public int kanaswitch = 0xFF7E;

    static final public int kanaTA = 0x4c0;

    static final public int kanaTE = 0x4c3;

    static final public int kanaTI = 0x4c1;

    static final public int kanaTO = 0x4c4;

    static final public int kanatsu = 0x4af;

    static final public int kanaTSU = 0x4c2;

    static final public int kanatu = 0x4af;

    static final public int kanaTU = 0x4c2;

    static final public int kanau = 0x4a9;

    static final public int kanaU = 0x4b3;

    static final public int kanaWA = 0x4dc;

    static final public int kanaWO = 0x4a6;

    static final public int kanaya = 0x4ac;

    static final public int kanaYA = 0x4d4;

    static final public int kanayo = 0x4ae;

    static final public int kanaYO = 0x4d6;

    static final public int kanayu = 0x4ad;

    static final public int kanaYU = 0x4d5;

    static final public int Kanji = 0xFF21;

    static final public int KanjiBangou = 0xFF37;

    static final public int kappa = 0x3a2;

    static final public int Katakana = 0xFF26;

    static final public int kcedilla = 0x3f3;

    static final public int Kcedilla = 0x3d3;

    static final public int KeyClick3270 = 0xFD11;

    static final public int KoreanWon = 0xeff;

    static final public int KP0 = 0xFFB0;

    static final public int KP1 = 0xFFB1;

    static final public int KP2 = 0xFFB2;

    static final public int KP3 = 0xFFB3;

    static final public int KP4 = 0xFFB4;

    static final public int KP5 = 0xFFB5;

    static final public int KP6 = 0xFFB6;

    static final public int KP7 = 0xFFB7;

    static final public int KP8 = 0xFFB8;

    static final public int KP9 = 0xFFB9;

    static final public int KPAdd = 0xFFAB;

    static final public int KPBegin = 0xFF9D;

    static final public int KPDecimal = 0xFFAE;

    static final public int KPDelete = 0xFF9F;

    static final public int KPDivide = 0xFFAF;

    static final public int KPDown = 0xFF99;

    static final public int KPEnd = 0xFF9C;

    static final public int KPEnter = 0xFF8D;

    static final public int KPEqual = 0xFFBD;

    static final public int KPF1 = 0xFF91;

    static final public int KPF2 = 0xFF92;

    static final public int KPF3 = 0xFF93;

    static final public int KPF4 = 0xFF94;

    static final public int KPHome = 0xFF95;

    static final public int KPInsert = 0xFF9E;

    static final public int KPLeft = 0xFF96;

    static final public int KPMultiply = 0xFFAA;

    static final public int KPNext = 0xFF9B;

    static final public int KPPageDown = 0xFF9B;

    static final public int KPPageUp = 0xFF9A;

    static final public int KPPrior = 0xFF9A;

    static final public int KPRight = 0xFF98;

    static final public int KPSeparator = 0xFFAC;

    static final public int KPSpace = 0xFF80;

    static final public int KPSubtract = 0xFFAD;

    static final public int KPTab = 0xFF89;

    static final public int KPUp = 0xFF97;

    static final public int kra = 0x3a2;

    static final public int l = 0x06c;

    static final public int L = 0x04c;

    static final public int L1 = 0xFFC8;

    static final public int L10 = 0xFFD1;

    static final public int L2 = 0xFFC9;

    static final public int L3 = 0xFFCA;

    static final public int L4 = 0xFFCB;

    static final public int L5 = 0xFFCC;

    static final public int L6 = 0xFFCD;

    static final public int L7 = 0xFFCE;

    static final public int L8 = 0xFFCF;

    static final public int L9 = 0xFFD0;

    static final public int lacute = 0x1e5;

    static final public int Lacute = 0x1c5;

    static final public int LastVirtualScreen = 0xFED4;

    static final public int latincross = 0xad9;

    static final public int lcaron = 0x1b5;

    static final public int Lcaron = 0x1a5;

    static final public int lcedilla = 0x3b6;

    static final public int Lcedilla = 0x3a6;

    static final public int Left = 0xFF51;

    static final public int Left23270 = 0xFD04;

    static final public int leftanglebracket = 0xabc;

    static final public int leftarrow = 0x8fb;

    static final public int leftcaret = 0xba3;

    static final public int leftdoublequotemark = 0xad2;

    static final public int leftmiddlecurlybrace = 0x8af;

    static final public int leftopentriangle = 0xacc;

    static final public int leftpointer = 0xaea;

    static final public int leftradical = 0x8a1;

    static final public int leftshoe = 0xbda;

    static final public int leftsinglequotemark = 0xad0;

    static final public int leftt = 0x9f4;

    static final public int lefttack = 0xbdc;

    static final public int less = 0x03c;

    static final public int lessthanequal = 0x8bc;

    static final public int lf = 0x9e5;

    static final public int Linefeed = 0xFF0A;

    static final public int LiraSign = 0x20a4;

    static final public int logicaland = 0x8de;

    static final public int logicalor = 0x8df;

    static final public int lowleftcorner = 0x9ed;

    static final public int lowrightcorner = 0x9ea;

    static final public int lstroke = 0x1b3;

    static final public int Lstroke = 0x1a3;

    static final public int m = 0x06d;

    static final public int M = 0x04d;

    static final public int Macedoniadse = 0x6a5;

    static final public int MacedoniaDSE = 0x6b5;

    static final public int Macedoniagje = 0x6a2;

    static final public int MacedoniaGJE = 0x6b2;

    static final public int Macedoniakje = 0x6ac;

    static final public int MacedoniaKJE = 0x6bc;

    static final public int macron = 0x0af;

    static final public int MaeKoho = 0xFF3E;

    static final public int malesymbol = 0xaf7;

    static final public int maltesecross = 0xaf0;

    static final public int marker = 0xabf;

    static final public int masculine = 0x0ba;

    static final public int Massyo = 0xFF2C;

    static final public int Menu = 0xFF67;

    static final public int MetaL = 0xFFE7;

    static final public int MetaR = 0xFFE8;

    static final public int MillSign = 0x20a5;

    static final public int minus = 0x02d;

    static final public int minutes = 0xad6;

    static final public int Modeswitch = 0xFF7E;

    static final public int MouseKeysAccelEnable = 0xFE77;

    static final public int MouseKeysEnable = 0xFE76;

    static final public int mu = 0x0b5;

    static final public int Muhenkan = 0xFF22;

    static final public int Multikey = 0xFF20;

    static final public int MultipleCandidate = 0xFF3D;

    static final public int multiply = 0x0d7;

    static final public int musicalflat = 0xaf6;

    static final public int musicalsharp = 0xaf5;

    static final public int n = 0x06e;

    static final public int N = 0x04e;

    static final public int nabla = 0x8c5;

    static final public int nacute = 0x1f1;

    static final public int Nacute = 0x1d1;

    static final public int NairaSign = 0x20a6;

    static final public int ncaron = 0x1f2;

    static final public int Ncaron = 0x1d2;

    static final public int ncedilla = 0x3f1;

    static final public int Ncedilla = 0x3d1;

    static final public int NewSheqelSign = 0x20aa;

    static final public int Next = 0xFF56;

    static final public int NextVirtualScreen = 0xFED2;

    static final public int nl = 0x9e8;

    static final public int nobreakspace = 0x0a0;

    static final public int notequal = 0x8bd;

    static final public int notsign = 0x0ac;

    static final public int ntilde = 0x0f1;

    static final public int Ntilde = 0x0d1;

    static final public int num0 = 0x030;

    static final public int num1 = 0x031;

    static final public int num2 = 0x032;

    static final public int num3 = 0x033;

    static final public int num4 = 0x034;

    static final public int num5 = 0x035;

    static final public int num6 = 0x036;

    static final public int num7 = 0x037;

    static final public int num8 = 0x038;

    static final public int num9 = 0x039;

    static final public int NumLock = 0xFF7F;

    static final public int numbersign = 0x023;

    static final public int numerosign = 0x6b0;

    static final public int o = 0x06f;

    static final public int O = 0x04f;

    static final public int oacute = 0x0f3;

    static final public int Oacute = 0x0d3;

    static final public int ocircumflex = 0x0f4;

    static final public int Ocircumflex = 0x0d4;

    static final public int odiaeresis = 0x0f6;

    static final public int Odiaeresis = 0x0d6;

    static final public int odoubleacute = 0x1f5;

    static final public int Odoubleacute = 0x1d5;

    static final public int oe = 0x13bd;

    static final public int OE = 0x13bc;

    static final public int ogonek = 0x1b2;

    static final public int ograve = 0x0f2;

    static final public int Ograve = 0x0d2;

    static final public int omacron = 0x3f2;

    static final public int Omacron = 0x3d2;

    static final public int oneeighth = 0xac3;

    static final public int onefifth = 0xab2;

    static final public int onehalf = 0x0bd;

    static final public int onequarter = 0x0bc;

    static final public int onesixth = 0xab6;

    static final public int onesuperior = 0x0b9;

    static final public int onethird = 0xab0;

    static final public int Ooblique = 0x0d8;

    static final public int openrectbullet = 0xae2;

    static final public int openstar = 0xae5;

    static final public int opentribulletdown = 0xae4;

    static final public int opentribulletup = 0xae3;

    static final public int ordfeminine = 0x0aa;

    static final public int oslash = 0x0f8;

    static final public int otilde = 0x0f5;

    static final public int Otilde = 0x0d5;

    static final public int overbar = 0xbc0;

    static final public int Overlay1Enable = 0xFE78;

    static final public int Overlay2Enable = 0xFE79;

    static final public int overline = 0x47e;

    static final public int p = 0x070;

    static final public int P = 0x050;

    static final public int PA13270 = 0xFD0A;

    static final public int PA23270 = 0xFD0B;

    static final public int PA33270 = 0xFD0C;

    static final public int PageDown = 0xFF56;

    static final public int PageUp = 0xFF55;

    static final public int paragraph = 0x0b6;

    static final public int parenleft = 0x028;

    static final public int parenright = 0x029;

    static final public int partialderivative = 0x8ef;

    static final public int Pause = 0xFF13;

    static final public int percent = 0x025;

    static final public int period = 0x02e;

    static final public int periodcentered = 0x0b7;

    static final public int PesetaSign = 0x20a7;

    static final public int phonographcopyright = 0xafb;

    static final public int Play3270 = 0xFD16;

    static final public int plus = 0x02b;

    static final public int plusminus = 0x0b1;

    static final public int PointerAccelerate = 0xFEFA;

    static final public int PointerButtonDflt = 0xFEE8;

    static final public int PointerButton1 = 0xFEE9;

    static final public int PointerButton2 = 0xFEEA;

    static final public int PointerButton3 = 0xFEEB;

    static final public int PointerButton4 = 0xFEEC;

    static final public int PointerButton5 = 0xFEED;

    static final public int PointerDblClickDflt = 0xFEEE;

    static final public int PointerDblClick1 = 0xFEEF;

    static final public int PointerDblClick2 = 0xFEF0;

    static final public int PointerDblClick3 = 0xFEF1;

    static final public int PointerDblClick4 = 0xFEF2;

    static final public int PointerDblClick5 = 0xFEF3;

    static final public int PointerDfltBtnNext = 0xFEFB;

    static final public int PointerDfltBtnPrev = 0xFEFC;

    static final public int PointerDown = 0xFEE3;

    static final public int PointerDownLeft = 0xFEE6;

    static final public int PointerDownRight = 0xFEE7;

    static final public int PointerDragDflt = 0xFEF4;

    static final public int PointerDrag1 = 0xFEF5;

    static final public int PointerDrag2 = 0xFEF6;

    static final public int PointerDrag3 = 0xFEF7;

    static final public int PointerDrag4 = 0xFEF8;

    static final public int PointerDrag5 = 0xFEFD;

    static final public int PointerEnableKeys = 0xFEF9;

    static final public int PointerLeft = 0xFEE0;

    static final public int PointerRight = 0xFEE1;

    static final public int PointerUp = 0xFEE2;

    static final public int PointerUpLeft = 0xFEE4;

    static final public int PointerUpRight = 0xFEE5;

    static final public int prescription = 0xad4;

    static final public int PrevVirtualScreen = 0xFED1;

    static final public int PreviousCandidate = 0xFF3E;

    static final public int Print = 0xFF61;

    static final public int PrintScreen3270 = 0xFD1D;

    static final public int Prior = 0xFF55;

    static final public int prolongedsound = 0x4b0;

    static final public int punctspace = 0xaa6;

    static final public int q = 0x071;

    static final public int Q = 0x051;

    static final public int quad = 0xbcc;

    static final public int question = 0x03f;

    static final public int questiondown = 0x0bf;

    static final public int Quit3270 = 0xFD09;

    static final public int quotedbl = 0x022;

    static final public int quoteleft = 0x060;

    static final public int quoteright = 0x027;

    static final public int r = 0x072;

    static final public int R = 0x052;

    static final public int R1 = 0xFFD2;

    static final public int R10 = 0xFFDB;

    static final public int R11 = 0xFFDC;

    static final public int R12 = 0xFFDD;

    static final public int R13 = 0xFFDE;

    static final public int R14 = 0xFFDF;

    static final public int R15 = 0xFFE0;

    static final public int R2 = 0xFFD3;

    static final public int R3 = 0xFFD4;

    static final public int R4 = 0xFFD5;

    static final public int R5 = 0xFFD6;

    static final public int R6 = 0xFFD7;

    static final public int R7 = 0xFFD8;

    static final public int R8 = 0xFFD9;

    static final public int R9 = 0xFFDA;

    static final public int racute = 0x1e0;

    static final public int Racute = 0x1c0;

    static final public int radical = 0x8d6;

    static final public int rcaron = 0x1f8;

    static final public int Rcaron = 0x1d8;

    static final public int rcedilla = 0x3b3;

    static final public int Rcedilla = 0x3a3;

    static final public int Record3270 = 0xFD18;

    static final public int Redo = 0xFF66;

    static final public int registered = 0x0ae;

    static final public int RepeatKeysEnable = 0xFE72;

    static final public int Reset3270 = 0xFD08;

    static final public int Return = 0xFF0D;

    static final public int Right = 0xFF53;

    static final public int Right23270 = 0xFD03;

    static final public int rightanglebracket = 0xabe;

    static final public int rightarrow = 0x8fd;

    static final public int rightcaret = 0xba6;

    static final public int rightdoublequotemark = 0xad3;

    static final public int rightmiddlecurlybrace = 0x8b0;

    static final public int rightmiddlesummation = 0x8b7;

    static final public int rightopentriangle = 0xacd;

    static final public int rightpointer = 0xaeb;

    static final public int rightshoe = 0xbd8;

    static final public int rightsinglequotemark = 0xad1;

    static final public int rightt = 0x9f5;

    static final public int righttack = 0xbfc;

    static final public int Romaji = 0xFF24;

    static final public int Rule3270 = 0xFD14;

    static final public int RupeeSign = 0x20a8;

    static final public int s = 0x073;

    static final public int S = 0x053;

    static final public int sacute = 0x1b6;

    static final public int Sacute = 0x1a6;

    static final public int scaron = 0x1b9;

    static final public int Scaron = 0x1a9;

    static final public int scedilla = 0x1ba;

    static final public int Scedilla = 0x1aa;

    static final public int scircumflex = 0x2fe;

    static final public int Scircumflex = 0x2de;

    static final public int scriptswitch = 0xFF7E;

    static final public int ScrollLock = 0xFF14;

    static final public int seconds = 0xad7;

    static final public int section = 0x0a7;

    static final public int Select = 0xFF60;

    static final public int semicolon = 0x03b;

    static final public int semivoicedsound = 0x4df;

    static final public int Serbiandje = 0x6a1;

    static final public int SerbianDJE = 0x6b1;

    static final public int Serbiandze = 0x6af;

    static final public int SerbianDZE = 0x6bf;

    static final public int Serbianje = 0x6a8;

    static final public int SerbianJE = 0x6b8;

    static final public int Serbianlje = 0x6a9;

    static final public int SerbianLJE = 0x6b9;

    static final public int Serbiannje = 0x6aa;

    static final public int SerbianNJE = 0x6ba;

    static final public int Serbiantshe = 0x6ab;

    static final public int SerbianTSHE = 0x6bb;

    static final public int Setup3270 = 0xFD17;

    static final public int seveneighths = 0xac6;

    static final public int ShiftL = 0xFFE1;

    static final public int ShiftLock = 0xFFE6;

    static final public int ShiftR = 0xFFE2;

    static final public int signaturemark = 0xaca;

    static final public int signifblank = 0xaac;

    static final public int similarequal = 0x8c9;

    static final public int SingleCandidate = 0xFF3C;

    static final public int singlelowquotemark = 0xafd;

    static final public int slash = 0x02f;

    static final public int SlowKeysEnable = 0xFE73;

    static final public int soliddiamond = 0x9e0;

    static final public int space = 0x020;

    static final public int ssharp = 0x0df;

    static final public int sterling = 0x0a3;

    static final public int StickyKeysEnable = 0xFE75;

    static final public int SuperL = 0xFFEB;

    static final public int SuperR = 0xFFEC;

    static final public int SysReq = 0xFF15;

    static final public int t = 0x074;

    static final public int T = 0x054;

    static final public int Tab = 0xFF09;

    static final public int tcaron = 0x1bb;

    static final public int Tcaron = 0x1ab;

    static final public int tcedilla = 0x1fe;

    static final public int Tcedilla = 0x1de;

    static final public int telephone = 0xaf9;

    static final public int telephonerecorder = 0xafa;

    static final public int TerminateServer = 0xFED5;

    static final public int Test3270 = 0xFD0D;

    static final public int Thaibaht = 0xddf;

    static final public int Thaibobaimai = 0xdba;

    static final public int Thaichochan = 0xda8;

    static final public int Thaichochang = 0xdaa;

    static final public int Thaichoching = 0xda9;

    static final public int Thaichochoe = 0xdac;

    static final public int Thaidochada = 0xdae;

    static final public int Thaidodek = 0xdb4;

    static final public int Thaifofa = 0xdbd;

    static final public int Thaifofan = 0xdbf;

    static final public int Thaihohip = 0xdcb;

    static final public int Thaihonokhuk = 0xdce;

    static final public int Thaikhokhai = 0xda2;

    static final public int Thaikhokhon = 0xda5;

    static final public int Thaikhokhuat = 0xda3;

    static final public int Thaikhokhwai = 0xda4;

    static final public int Thaikhorakhang = 0xda6;

    static final public int Thaikokai = 0xda1;

    static final public int Thailakkhangyao = 0xde5;

    static final public int Thailekchet = 0xdf7;

    static final public int Thailekha = 0xdf5;

    static final public int Thailekhok = 0xdf6;

    static final public int Thailekkao = 0xdf9;

    static final public int Thaileknung = 0xdf1;

    static final public int Thailekpaet = 0xdf8;

    static final public int Thaileksam = 0xdf3;

    static final public int Thaileksi = 0xdf4;

    static final public int Thaileksong = 0xdf2;

    static final public int Thaileksun = 0xdf0;

    static final public int Thailochula = 0xdcc;

    static final public int Thailoling = 0xdc5;

    static final public int Thailu = 0xdc6;

    static final public int Thaimaichattawa = 0xdeb;

    static final public int Thaimaiek = 0xde8;

    static final public int Thaimaihanakat = 0xdd1;

    static final public int Thaimaihanakatmaitho = 0xdde;

    static final public int Thaimaitaikhu = 0xde7;

    static final public int Thaimaitho = 0xde9;

    static final public int Thaimaitri = 0xdea;

    static final public int Thaimaiyamok = 0xde6;

    static final public int Thaimoma = 0xdc1;

    static final public int Thaingongu = 0xda7;

    static final public int Thainikhahit = 0xded;

    static final public int Thainonen = 0xdb3;

    static final public int Thainonu = 0xdb9;

    static final public int Thaioang = 0xdcd;

    static final public int Thaipaiyannoi = 0xdcf;

    static final public int Thaiphinthu = 0xdda;

    static final public int Thaiphophan = 0xdbe;

    static final public int Thaiphophung = 0xdbc;

    static final public int Thaiphosamphao = 0xdc0;

    static final public int Thaipopla = 0xdbb;

    static final public int Thairorua = 0xdc3;

    static final public int Thairu = 0xdc4;

    static final public int Thaisaraa = 0xdd0;

    static final public int Thaisaraaa = 0xdd2;

    static final public int Thaisaraae = 0xde1;

    static final public int Thaisaraaimaimalai = 0xde4;

    static final public int Thaisaraaimaimuan = 0xde3;

    static final public int Thaisaraam = 0xdd3;

    static final public int Thaisarae = 0xde0;

    static final public int Thaisarai = 0xdd4;

    static final public int Thaisaraii = 0xdd5;

    static final public int Thaisarao = 0xde2;

    static final public int Thaisarau = 0xdd8;

    static final public int Thaisaraue = 0xdd6;

    static final public int Thaisarauee = 0xdd7;

    static final public int Thaisarauu = 0xdd9;

    static final public int Thaisorusi = 0xdc9;

    static final public int Thaisosala = 0xdc8;

    static final public int Thaisoso = 0xdab;

    static final public int Thaisosua = 0xdca;

    static final public int Thaithanthakhat = 0xdec;

    static final public int Thaithonangmontho = 0xdb1;

    static final public int Thaithophuthao = 0xdb2;

    static final public int Thaithothahan = 0xdb7;

    static final public int Thaithothan = 0xdb0;

    static final public int Thaithothong = 0xdb8;

    static final public int Thaithothung = 0xdb6;

    static final public int Thaitopatak = 0xdaf;

    static final public int Thaitotao = 0xdb5;

    static final public int Thaiwowaen = 0xdc7;

    static final public int Thaiyoyak = 0xdc2;

    static final public int Thaiyoying = 0xdad;

    static final public int therefore = 0x8c0;

    static final public int thinspace = 0xaa7;

    static final public int thorn = 0x0fe;

    static final public int Thorn = 0x0de;

    static final public int THORN = 0x0de;

    static final public int threeeighths = 0xac4;

    static final public int threefifths = 0xab4;

    static final public int threequarters = 0x0be;

    static final public int threesuperior = 0x0b3;

    static final public int topintegral = 0x8a4;

    static final public int topleftparens = 0x8ab;

    static final public int topleftradical = 0x8a2;

    static final public int topleftsqbracket = 0x8a7;

    static final public int topleftsummation = 0x8b1;

    static final public int toprightparens = 0x8ad;

    static final public int toprightsqbracket = 0x8a9;

    static final public int toprightsummation = 0x8b5;

    static final public int topt = 0x9f7;

    static final public int topvertsummationconnector = 0x8b3;

    static final public int Touroku = 0xFF2B;

    static final public int trademark = 0xac9;

    static final public int trademarkincircle = 0xacb;

    static final public int tslash = 0x3bc;

    static final public int Tslash = 0x3ac;

    static final public int twofifths = 0xab3;

    static final public int twosuperior = 0x0b2;

    static final public int twothirds = 0xab1;

    static final public int u = 0x075;

    static final public int U = 0x055;

    static final public int uacute = 0x0fa;

    static final public int Uacute = 0x0da;

    static final public int ubreve = 0x2fd;

    static final public int Ubreve = 0x2dd;

    static final public int ucircumflex = 0x0fb;

    static final public int Ucircumflex = 0x0db;

    static final public int udiaeresis = 0x0fc;

    static final public int Udiaeresis = 0x0dc;

    static final public int udoubleacute = 0x1fb;

    static final public int Udoubleacute = 0x1db;

    static final public int ugrave = 0x0f9;

    static final public int Ugrave = 0x0d9;

    static final public int Ukrainiani = 0x6a6;

    static final public int UkrainianI = 0x6b6;

    static final public int Ukrainianie = 0x6a4;

    static final public int UkrainianIE = 0x6b4;

    static final public int Ukrainianyi = 0x6a7;

    static final public int UkrainianYI = 0x6b7;

    static final public int Ukraniani = 0x6a6;

    static final public int UkranianI = 0x6b6;

    static final public int Ukranianje = 0x6a4;

    static final public int UkranianJE = 0x6b4;

    static final public int Ukranianyi = 0x6a7;

    static final public int UkranianYI = 0x6b7;

    static final public int umacron = 0x3fe;

    static final public int Umacron = 0x3de;

    static final public int underbar = 0xbc6;

    static final public int underscore = 0x05f;

    static final public int Undo = 0xFF65;

    static final public int union = 0x8dd;

    static final public int uogonek = 0x3f9;

    static final public int Uogonek = 0x3d9;

    static final public int Up = 0xFF52;

    static final public int uparrow = 0x8fc;

    static final public int upcaret = 0xba9;

    static final public int upleftcorner = 0x9ec;

    static final public int uprightcorner = 0x9eb;

    static final public int upshoe = 0xbc3;

    static final public int upstile = 0xbd3;

    static final public int uptack = 0xbce;

    static final public int uring = 0x1f9;

    static final public int Uring = 0x1d9;

    static final public int utilde = 0x3fd;

    static final public int Utilde = 0x3dd;

    static final public int v = 0x076;

    static final public int V = 0x056;

    static final public int variation = 0x8c1;

    static final public int vertbar = 0x9f8;

    static final public int vertconnector = 0x8a6;

    static final public int voicedsound = 0x4de;

    static final public int vt = 0x9e9;

    static final public int w = 0x077;

    static final public int W = 0x057;

    static final public int WonSign = 0x20a9;

    static final public int x = 0x078;

    static final public int X = 0x058;

    static final public int y = 0x079;

    static final public int Y = 0x059;

    static final public int yacute = 0x0fd;

    static final public int Yacute = 0x0dd;

    static final public int ydiaeresis = 0x0ff;

    static final public int Ydiaeresis = 0x13be;

    static final public int yen = 0x0a5;

    static final public int z = 0x07a;

    static final public int Z = 0x05a;

    static final public int zabovedot = 0x1bf;

    static final public int Zabovedot = 0x1af;

    static final public int zacute = 0x1bc;

    static final public int Zacute = 0x1ac;

    static final public int zcaron = 0x1be;

    static final public int Zcaron = 0x1ae;

    static final public int ZenKoho = 0xFF3D;

    static final public int Zenkaku = 0xFF28;

    static final public int ZenkakuHankaku = 0xFF2A;
}
