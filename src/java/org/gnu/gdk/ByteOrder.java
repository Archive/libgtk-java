/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class ByteOrder extends Enum {

    static final private int _LSB_FIRST = 0;

    static final public ByteOrder LSB_FIRST = new ByteOrder(_LSB_FIRST);

    static final private int _MSB_FIRST = 1;

    static final public ByteOrder MSB_FIRST = new ByteOrder(_MSB_FIRST);

    static final private ByteOrder[] theInterned = new ByteOrder[] { LSB_FIRST,
            MSB_FIRST };

    static private java.util.Hashtable theInternedExtras;

    static final private ByteOrder theSacrificialOne = new ByteOrder(0);

    static public ByteOrder intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        ByteOrder already = (ByteOrder) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new ByteOrder(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ByteOrder(int value) {
        value_ = value;
    }

    public ByteOrder or(ByteOrder other) {
        return intern(value_ | other.value_);
    }

    public ByteOrder and(ByteOrder other) {
        return intern(value_ & other.value_);
    }

    public ByteOrder xor(ByteOrder other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(ByteOrder other) {
        return (value_ & other.value_) == other.value_;
    }

}
