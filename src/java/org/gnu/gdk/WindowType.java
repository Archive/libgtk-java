/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class WindowType extends Enum {

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ROOT = 0;

    static final public org.gnu.gdk.WindowType ROOT = new org.gnu.gdk.WindowType(
            _ROOT);

    static final private int _TOPLEVEL = 1;

    static final public org.gnu.gdk.WindowType TOPLEVEL = new org.gnu.gdk.WindowType(
            _TOPLEVEL);

    static final private int _CHILD = 2;

    static final public org.gnu.gdk.WindowType CHILD = new org.gnu.gdk.WindowType(
            _CHILD);

    static final private int _DIALOG = 3;

    static final public org.gnu.gdk.WindowType DIALOG = new org.gnu.gdk.WindowType(
            _DIALOG);

    static final private int _TEMP = 4;

    static final public org.gnu.gdk.WindowType TEMP = new org.gnu.gdk.WindowType(
            _TEMP);

    static final private int _FOREIGN = 5;

    static final public org.gnu.gdk.WindowType FOREIGN = new org.gnu.gdk.WindowType(
            _FOREIGN);

    static final private org.gnu.gdk.WindowType[] theInterned = new org.gnu.gdk.WindowType[] {
            ROOT, TOPLEVEL, CHILD, DIALOG, TEMP, FOREIGN }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowType theSacrificialOne = new org.gnu.gdk.WindowType(
            0);

    static public org.gnu.gdk.WindowType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowType already = (org.gnu.gdk.WindowType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowType or(org.gnu.gdk.WindowType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowType and(org.gnu.gdk.WindowType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowType xor(org.gnu.gdk.WindowType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
