/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

/**
 * Specifies the type of the Event.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.EventType</code>.
 */
public class EventType extends Enum {

    static final private int _NOTHING = -1;

    static final public org.gnu.gdk.EventType NOTHING = new org.gnu.gdk.EventType(
            _NOTHING);

    static final private int _DELETE = 0;

    static final public org.gnu.gdk.EventType DELETE = new org.gnu.gdk.EventType(
            _DELETE);

    static final private int _DESTROY = 1;

    static final public org.gnu.gdk.EventType DESTROY = new org.gnu.gdk.EventType(
            _DESTROY);

    static final private int _EXPOSE = 2;

    static final public org.gnu.gdk.EventType EXPOSE = new org.gnu.gdk.EventType(
            _EXPOSE);

    static final private int _MOTION_NOTIFY = 3;

    static final public org.gnu.gdk.EventType MOTION_NOTIFY = new org.gnu.gdk.EventType(
            _MOTION_NOTIFY);

    static final private int _BUTTON_PRESS = 4;

    static final public org.gnu.gdk.EventType BUTTON_PRESS = new org.gnu.gdk.EventType(
            _BUTTON_PRESS);

    static final private int _BUTTON2_PRESS = 5;

    static final public org.gnu.gdk.EventType BUTTON2_PRESS = new org.gnu.gdk.EventType(
            _BUTTON2_PRESS);

    static final private int _BUTTON3_PRESS = 6;

    static final public org.gnu.gdk.EventType BUTTON3_PRESS = new org.gnu.gdk.EventType(
            _BUTTON3_PRESS);

    static final private int _BUTTON_RELEASE = 7;

    static final public org.gnu.gdk.EventType BUTTON_RELEASE = new org.gnu.gdk.EventType(
            _BUTTON_RELEASE);

    static final private int _KEY_PRESS = 8;

    static final public org.gnu.gdk.EventType KEY_PRESS = new org.gnu.gdk.EventType(
            _KEY_PRESS);

    static final private int _KEY_RELEASE = 9;

    static final public org.gnu.gdk.EventType KEY_RELEASE = new org.gnu.gdk.EventType(
            _KEY_RELEASE);

    static final private int _ENTER_NOTIFY = 10;

    static final public org.gnu.gdk.EventType ENTER_NOTIFY = new org.gnu.gdk.EventType(
            _ENTER_NOTIFY);

    static final private int _LEAVE_NOTIFY = 11;

    static final public org.gnu.gdk.EventType LEAVE_NOTIFY = new org.gnu.gdk.EventType(
            _LEAVE_NOTIFY);

    static final private int _FOCUS_CHANGE = 12;

    static final public org.gnu.gdk.EventType FOCUS_CHANGE = new org.gnu.gdk.EventType(
            _FOCUS_CHANGE);

    static final private int _CONFIGURE = 13;

    static final public org.gnu.gdk.EventType CONFIGURE = new org.gnu.gdk.EventType(
            _CONFIGURE);

    static final private int _MAP = 14;

    static final public org.gnu.gdk.EventType MAP = new org.gnu.gdk.EventType(
            _MAP);

    static final private int _UNMAP = 15;

    static final public org.gnu.gdk.EventType UNMAP = new org.gnu.gdk.EventType(
            _UNMAP);

    static final private int _PROPERTY_NOTIFY = 16;

    static final public org.gnu.gdk.EventType PROPERTY_NOTIFY = new org.gnu.gdk.EventType(
            _PROPERTY_NOTIFY);

    static final private int _SELECTION_CLEAR = 17;

    static final public org.gnu.gdk.EventType SELECTION_CLEAR = new org.gnu.gdk.EventType(
            _SELECTION_CLEAR);

    static final private int _SELECTION_REQUEST = 18;

    static final public org.gnu.gdk.EventType SELECTION_REQUEST = new org.gnu.gdk.EventType(
            _SELECTION_REQUEST);

    static final private int _SELECTION_NOTIFY = 19;

    static final public org.gnu.gdk.EventType SELECTION_NOTIFY = new org.gnu.gdk.EventType(
            _SELECTION_NOTIFY);

    static final private int _PROXIMITY_IN = 20;

    static final public org.gnu.gdk.EventType PROXIMITY_IN = new org.gnu.gdk.EventType(
            _PROXIMITY_IN);

    static final private int _PROXIMITY_OUT = 21;

    static final public org.gnu.gdk.EventType PROXIMITY_OUT = new org.gnu.gdk.EventType(
            _PROXIMITY_OUT);

    static final private int _DRAG_ENTER = 22;

    static final public org.gnu.gdk.EventType DRAG_ENTER = new org.gnu.gdk.EventType(
            _DRAG_ENTER);

    static final private int _DRAG_LEAVE = 23;

    static final public org.gnu.gdk.EventType DRAG_LEAVE = new org.gnu.gdk.EventType(
            _DRAG_LEAVE);

    static final private int _DRAG_MOTION = 24;

    static final public org.gnu.gdk.EventType DRAG_MOTION = new org.gnu.gdk.EventType(
            _DRAG_MOTION);

    static final private int _DRAG_STATUS = 25;

    static final public org.gnu.gdk.EventType DRAG_STATUS = new org.gnu.gdk.EventType(
            _DRAG_STATUS);

    static final private int _DROP_START = 26;

    static final public org.gnu.gdk.EventType DROP_START = new org.gnu.gdk.EventType(
            _DROP_START);

    static final private int _DROP_FINISHED = 27;

    static final public org.gnu.gdk.EventType DROP_FINISHED = new org.gnu.gdk.EventType(
            _DROP_FINISHED);

    static final private int _CLIENT_EVENT = 28;

    static final public org.gnu.gdk.EventType CLIENT_EVENT = new org.gnu.gdk.EventType(
            _CLIENT_EVENT);

    static final private int _VISIBILITY_NOTIFY = 29;

    static final public org.gnu.gdk.EventType VISIBILITY_NOTIFY = new org.gnu.gdk.EventType(
            _VISIBILITY_NOTIFY);

    static final private int _NO_EXPOSE = 30;

    static final public org.gnu.gdk.EventType NO_EXPOSE = new org.gnu.gdk.EventType(
            _NO_EXPOSE);

    static final private int _SCROLL = 31;

    static final public org.gnu.gdk.EventType SCROLL = new org.gnu.gdk.EventType(
            _SCROLL);

    static final private int _STATE = 32;

    static final public org.gnu.gdk.EventType STATE = new org.gnu.gdk.EventType(
            _STATE);

    static final private int _SETTING = 33;

    static final public org.gnu.gdk.EventType SETTING = new org.gnu.gdk.EventType(
            _SETTING);

    static final private int _OWNER_CHANGE = 34;

    static final public org.gnu.gdk.EventType OWNER_CHANGE = new org.gnu.gdk.EventType(
            _OWNER_CHANGE);

    static final private org.gnu.gdk.EventType[] theInterned = new org.gnu.gdk.EventType[] {
            DELETE, DESTROY, EXPOSE, MOTION_NOTIFY, BUTTON_PRESS,
            BUTTON2_PRESS, BUTTON3_PRESS, BUTTON_RELEASE, KEY_PRESS,
            KEY_RELEASE, ENTER_NOTIFY, LEAVE_NOTIFY, FOCUS_CHANGE, CONFIGURE,
            MAP, UNMAP, PROPERTY_NOTIFY, SELECTION_CLEAR, SELECTION_REQUEST,
            SELECTION_NOTIFY, PROXIMITY_IN, PROXIMITY_OUT, DRAG_ENTER,
            DRAG_LEAVE, DRAG_MOTION, DRAG_STATUS, DROP_START, DROP_FINISHED,
            CLIENT_EVENT, VISIBILITY_NOTIFY, NO_EXPOSE, SCROLL, STATE, SETTING,
            OWNER_CHANGE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.EventType theSacrificialOne = new org.gnu.gdk.EventType(
            0);

    static public org.gnu.gdk.EventType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.EventType already = (org.gnu.gdk.EventType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.EventType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private EventType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.EventType or(org.gnu.gdk.EventType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.EventType and(org.gnu.gdk.EventType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.EventType xor(org.gnu.gdk.EventType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.EventType other) {
        return (value_ & other.value_) == other.value_;
    }

}
