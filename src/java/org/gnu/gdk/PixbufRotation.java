/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gdk;

import org.gnu.glib.Enum;

/**
 * The possible rotations that can be passed to the rotate method of Pixbuf.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufRotation</code>.
 */
public class PixbufRotation extends Enum {
    static final private int _NONE = 0;

    /** No rotation */
    static final public PixbufRotation NONE = new PixbufRotation(_NONE);

    static final private int _COUNTERCLOCKWISE = 1;

    /** Rotate by 90 degrees */
    static final public PixbufRotation COUNTERCLOCKWISE = new PixbufRotation(
            _COUNTERCLOCKWISE);

    static final private int _UPSIDEDOWN = 2;

    /** Rotate by 180 degrees */
    static final public PixbufRotation UPSIDEDOWN = new PixbufRotation(
            _UPSIDEDOWN);

    static final private int _CLOCKWISE = 3;

    /** Rotate by 270 degrees */
    static final public PixbufRotation CLOCKWISE = new PixbufRotation(
            _CLOCKWISE);

    static final private PixbufRotation[] theInterned = new PixbufRotation[] {
            NONE, COUNTERCLOCKWISE, UPSIDEDOWN, CLOCKWISE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private PixbufRotation theSacrificialOne = new PixbufRotation(
            0);

    static public PixbufRotation intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        PixbufRotation already = (PixbufRotation) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new PixbufRotation(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PixbufRotation(int value) {
        value_ = value;
    }

    public PixbufRotation or(PixbufRotation other) {
        return intern(value_ | other.value_);
    }

    public PixbufRotation and(PixbufRotation other) {
        return intern(value_ & other.value_);
    }

    public PixbufRotation xor(PixbufRotation other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(PixbufRotation other) {
        return (value_ & other.value_) == other.value_;
    }

}
