/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

/**
 * Determines how the bit values for the source pixels are combined with the bit
 * values for destination pixels to produce the final result.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Function</code>.
 */
public class Function extends Enum {

    static final private int _COPY = 0;

    static final public org.gnu.gdk.Function COPY = new org.gnu.gdk.Function(
            _COPY);

    static final private int _INVERT = 1;

    static final public org.gnu.gdk.Function INVERT = new org.gnu.gdk.Function(
            _INVERT);

    static final private int _XOR = 2;

    static final public org.gnu.gdk.Function XOR = new org.gnu.gdk.Function(
            _XOR);

    static final private int _CLEAR = 3;

    static final public org.gnu.gdk.Function CLEAR = new org.gnu.gdk.Function(
            _CLEAR);

    static final private int _AND = 4;

    static final public org.gnu.gdk.Function AND = new org.gnu.gdk.Function(
            _AND);

    static final private int _AND_REVERSE = 5;

    static final public org.gnu.gdk.Function AND_REVERSE = new org.gnu.gdk.Function(
            _AND_REVERSE);

    static final private int _AND_INVERT = 6;

    static final public org.gnu.gdk.Function AND_INVERT = new org.gnu.gdk.Function(
            _AND_INVERT);

    static final private int _NOOP = 7;

    static final public org.gnu.gdk.Function NOOP = new org.gnu.gdk.Function(
            _NOOP);

    static final private int _OR = 8;

    static final public org.gnu.gdk.Function OR = new org.gnu.gdk.Function(_OR);

    static final private int _EQUIV = 9;

    static final public org.gnu.gdk.Function EQUIV = new org.gnu.gdk.Function(
            _EQUIV);

    static final private int _OR_REVERSE = 10;

    static final public org.gnu.gdk.Function OR_REVERSE = new org.gnu.gdk.Function(
            _OR_REVERSE);

    static final private int _COPY_INVERT = 11;

    static final public org.gnu.gdk.Function COPY_INVERT = new org.gnu.gdk.Function(
            _COPY_INVERT);

    static final private int _OR_INVERT = 12;

    static final public org.gnu.gdk.Function OR_INVERT = new org.gnu.gdk.Function(
            _OR_INVERT);

    static final private int _NAND = 13;

    static final public org.gnu.gdk.Function NAND = new org.gnu.gdk.Function(
            _NAND);

    static final private int _NOR = 14;

    static final public org.gnu.gdk.Function NOR = new org.gnu.gdk.Function(
            _NOR);

    static final private int _SET = 15;

    static final public org.gnu.gdk.Function SET = new org.gnu.gdk.Function(
            _SET);

    static final private org.gnu.gdk.Function[] theInterned = new org.gnu.gdk.Function[] {
            COPY, INVERT, XOR, CLEAR, AND, AND_REVERSE, AND_INVERT, NOOP, OR,
            EQUIV, OR_REVERSE, COPY_INVERT, OR_INVERT, NAND, NOR, SET }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.Function theSacrificialOne = new org.gnu.gdk.Function(
            0);

    static public org.gnu.gdk.Function intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.Function already = (org.gnu.gdk.Function) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.Function(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Function(int value) {
        value_ = value;
    }

    public org.gnu.gdk.Function or(org.gnu.gdk.Function other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.Function and(org.gnu.gdk.Function other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.Function xor(org.gnu.gdk.Function other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.Function other) {
        return (value_ & other.value_) == other.value_;
    }

}
