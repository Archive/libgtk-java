/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.GObject;
import org.gnu.glib.Type;

/**
 * Provides the necessary functionality to display an animation.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufAnimationIter</code>.
 */
public class PixbufAnimationIter extends GObject {

    public PixbufAnimationIter(Handle handle) {
        super(handle);
    }

    /**
     * Possibly advances an animation to a new frame. Chooses the frame based on
     * the start time passed to the getIter method of PixbufAnimation.
     * 
     * @param currentSec
     * @param currentUsec
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean advance(long currentSec, long currentUsec) {
        return gdk_pixbuf_animation_iter_advance(getHandle(), currentSec,
                currentUsec);
    }

    /**
     * Gets the number of milliseconds the current Pixbuf should be displayed or
     * -1 if the current Pixbuf should be displayed forever.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getDelayTime() {
        return gdk_pixbuf_animation_iter_get_delay_time(getHandle());
    }

    /**
     * Returns true if the frame we're on is partially loaded, or the last frame
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean onCurrentlyLoadingFrame() {
        return gdk_pixbuf_animation_iter_on_currently_loading_frame(getHandle());
    }

    /**
     * Gets the current Pixbuf which should be displayed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getPixbuf() {
        return Pixbuf
                .getPixbufFromHandle(gdk_pixbuf_animation_iter_get_pixbuf(getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gdk_pixbuf_animation_iter_get_type());
    }

    native static final protected int gdk_pixbuf_animation_iter_get_type();

    native static final protected boolean gdk_pixbuf_animation_iter_advance(
            Handle iter, long curSec, long curUsec);

    native static final protected int gdk_pixbuf_animation_iter_get_delay_time(
            Handle iter);

    native static final protected Handle gdk_pixbuf_animation_iter_get_pixbuf(
            Handle iter);

    native static final protected boolean gdk_pixbuf_animation_iter_on_currently_loading_frame(
            Handle iter);

}
