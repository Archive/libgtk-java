/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;

public class Gdk {

    /**
     * Get the default dimensions of the screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Dimension getScreenDimension() {
        int width = Screen
                .gdk_screen_get_width(Screen.gdk_screen_get_default());
        int height = Screen.gdk_screen_get_height(Screen
                .gdk_screen_get_default());
        return new Dimension(width, height);
    }

    /**
     * Emits a short beep.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void beep() {
        Gdk.gdk_beep();
    }

    /**
     * Flushes the X output buffer and waits until all requests have been
     * processed by the server. This is rarely needed by applications.
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static void flush() {
        gdk_flush();
    }

    /**
     * Initializes GDK so that it can be used from multiple threads in
     * conjunction with threadsEnter() and threadsLeave(). g_thread_init() must
     * be called previous to this function.
     * 
     * This call must be made before any use of the main loop from GTK+; to be
     * safe, call it before Gtk.init().
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    // TODO: need g_thread_init()
    // public static void threadsInit() {
    // gdk_threads_init();
    // }
    /**
     * This method marks the beginning of a critical section in which GDK and
     * GTK+ functions can be called. Only one thread at a time can be in such a
     * critial section.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    // TODO: need g_thread
    // public static void threadsEnter() {
    // gdk_threads_enter();
    // }
    /**
     * Leaves a critical region begun with threadsEnter().
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    // TODO: need g_thread
    // public static void threadsLeave() {
    // gdk_threads_leave();
    // }

    static {
        System.loadLibrary(org.gnu.gtk.Config.LIBRARY_NAME
                + org.gnu.gtk.Config.GTK_API_VERSION);
    }

    // native static final protected void gdk_input_set_extension_events (int
    // window, int mask, int mode);
    native static final protected void gdk_flush();

    native static final protected void gdk_beep();

    // native static final protected void gdk_set_show_events (boolean
    // showEvents);
    // native static final protected boolean gdk_get_show_events ();
    // native static final protected String gdk_keyval_name (int keyval);
    // native static final protected int gdk_keyval_from_name (String
    // keyvalName);
    // native static final protected int gdk_keyval_to_upper (int keyval);
    // native static final protected int gdk_keyval_to_lower (int keyval);
    // native static final protected boolean gdk_keyval_is_upper (int keyval);
    // native static final protected boolean gdk_keyval_is_lower (int keyval);
    //
    // native static final private void gdk_init(int[] argc, String[] argv);
    // native static final private boolean gdk_init_check(int[] argc, String[]
    // argv);
    // native static final private void gdk_parse_args(int[] argc, String[]
    // argv);
    // native static final private String gdk_get_program_class();
    // native static final private void gdk_set_program_class(String
    // program_class);
    // native static final private void gdk_error_trap_push();
    // native static final private int gdk_error_trap_pop();
    // native static final private String gdk_get_display_arg_name();
    // native static final private boolean gdk_event_send_client_message(Handle
    // event, int winid);
    // native static final private void
    // gdk_event_send_clientmessage_toall(Handle event);
    // native static final private boolean
    // gdk_event_send_client_message_for_display(Handle display, Handle event,
    // int winid);
    // native static final private void gdk_notify_startup_complete();
    native static final private void gdk_threads_enter();

    native static final private void gdk_threads_leave();

    native static final private void gdk_threads_init();

    native static final protected Handle gdk_pango_context_get();
    
    /**
     * // Not implemented. native static final private void
     * gdk_threads_set_lock_functions(GCallback enter_fn, GCallback leave_fn);
     *  // Deprecated or undocumented functions. native static final private
     * void gdk_add_option_entries_libgtk_only(GOptionGroup* group); native
     * static final private void gdk_pre_parse_libgtk_only(); native static
     * final private boolean gdk_pointer_grab_info_libgtk_only(Handle display,
     * Handle grab_window, gboolean* owner_events); native static final private
     * boolean gdk_keyboard_grab_info_libgtk_only(Handle display, Handle
     * grab_window, gboolean* owner_events); native static final private void
     * gdk_add_option_entries_libgtk_only(GOptionGroup* group); native static
     * final private void gdk_pre_parse_libgtk_only(); native static final
     * private void gdk_exit(int error_code); native static final private void
     * gdk_set_use_xshm(boolean use_xshm); native static final private boolean
     * gdk_get_use_xshm(); native static final private boolean
     * gdk_pointer_grab_info_libgtk_only(Handle display, Handle grab_window,
     * gboolean* owner_events); native static final private boolean
     * gdk_keyboard_grab_info_libgtk_only(Handle display, Handle grab_window,
     * gboolean* owner_events); native static final private String
     * gdk_wcstombs(constGdkWChar* src); native static final private int
     * gdk_mbstowcs(Handle dest, String src, int dest_max); native static final
     * private int gdk_input_add_full(int source, int condition, int function,
     * gpointer data, int destroy); native static final private int
     * gdk_input_add(int source, int condition, int function, gpointer data);
     * native static final private void gdk_input_remove(int tag);
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
}
