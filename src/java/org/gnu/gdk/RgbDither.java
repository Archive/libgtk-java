/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class RgbDither extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NONE = 0;

    static final public org.gnu.gdk.RgbDither NONE = new org.gnu.gdk.RgbDither(
            _NONE);

    static final private int _NORMAL = 1;

    static final public org.gnu.gdk.RgbDither NORMAL = new org.gnu.gdk.RgbDither(
            _NORMAL);

    static final private int _MAX = 2;

    static final public org.gnu.gdk.RgbDither MAX = new org.gnu.gdk.RgbDither(
            _MAX);

    static final private org.gnu.gdk.RgbDither[] theInterned = new org.gnu.gdk.RgbDither[] {
            NONE, NORMAL, MAX }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.RgbDither theSacrificialOne = new org.gnu.gdk.RgbDither(
            0);

    static public org.gnu.gdk.RgbDither intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.RgbDither already = (org.gnu.gdk.RgbDither) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.RgbDither(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private RgbDither(int value) {
        value_ = value;
    }

    public org.gnu.gdk.RgbDither or(org.gnu.gdk.RgbDither other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.RgbDither and(org.gnu.gdk.RgbDither other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.RgbDither xor(org.gnu.gdk.RgbDither other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.RgbDither other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
