/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A Pixmap is an offscreen drawable. It can be drawn upon with the standard
 * drawing primitives, then copied to another Drawable.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Pixmap</code>.
 */
public class Pixmap extends Drawable {
    /**
     * Create a new pixmap with a given size and depth.
     * 
     * @param drawable
     *            A Drawable, used to determine default values for the new
     *            pixmap. Can be <code>null</code> if depth is specified.
     * @param width
     *            The width of the new pixmap in pixels.
     * @param height
     *            The height of the new pixmap in pixels.
     * @param depth
     *            The depth (number of bits per pixel) of the new pixmap. If
     *            <code>-1</code>, and drawable is not <code>null</code>,
     *            the depth of the new pixmap will be equal to that of drawable.
     * @throws IllegalArgumentException
     *             If drawable is null and depth is -1
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, int width, int height, int depth) {
        super(init(drawable, width, height, depth));
    }

    private static Handle init(Drawable drawable, int width, int height,
            int depth) {
        if (drawable == null && depth == -1)
            throw new IllegalArgumentException("A depth of -1 is not allowed "
                    + "when drawable is null");
        return gdk_pixmap_new(drawable == null ? null : drawable.getHandle(),
                width, height, depth);
    }

    /**
     * Create a two-color pixmap from data in XBM data.
     * 
     * @param drawable
     *            a Drawable, used to determine default values for the new
     *            pixmap. Can be <code>null</code>, in which case the root
     *            window is used.
     * @param data
     *            the pixmap data.
     * @param width
     *            the width of the new pixmap in pixels.
     * @param height
     *            the height of the new pixmap in pixels.
     * @param depth
     *            the depth (number of bits per pixel) of the new pixmap.
     * @param fg
     *            the foreground color.
     * @param bg
     *            the background color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, byte[] data, int width, int height,
            int depth, Color fg, Color bg) {
        super(gdk_pixmap_create_from_data(drawable == null ? null : drawable
                .getHandle(), data, width, height, depth, fg.getHandle(), bg
                .getHandle()));
    }

    /**
     * Create a pixmap from a XPM file.
     * 
     * @param drawable
     *            a Drawable, used to determine default values for the new
     *            pixmap.
     * @param mask
     *            object where to store a bitmap representing the transparency
     *            mask of the XPM file. Can be <code>null</code>, in which
     *            case transparency will be ignored.
     * @param transparent
     *            the color to be used for the pixels that are transparent in
     *            the input file. Can be <code>null</code>, in which case a
     *            default color will be used.
     * @param filename
     *            the filename of a file containing XPM data.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, Bitmap mask, Color transparent,
            String filename) {
        super(gdk_pixmap_create_from_xpm(drawable.getHandle(),
                mask == null ? null : mask.getHandle(),
                transparent == null ? null : transparent.getHandle(), filename));
    }

    /**
     * Create a pixmap from a XPM file using a particular colormap.
     * 
     * @param drawable
     *            a Drawable, used to determine default values for the new
     *            pixmap. Can be <code>null</code> if colormap is given.
     * @param colormap
     *            the GdkColormap that the new pixmap will be use. If omitted,
     *            the colormap for window will be used.
     * @param mask
     *            object where to store a bitmap representing the transparency
     *            mask of the XPM file. Can be <code>null</code>, in which
     *            case transparency will be ignored.
     * @param transparent
     *            the color to be used for the pixels that are transparent in
     *            the input file. Can be <code>null</code>, in which case a
     *            default color will be used.
     * @param filename
     *            the filename of a file containing XPM data.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, Colormap colormap, Bitmap mask,
            Color transparent, String filename) {
        super(gdk_pixmap_colormap_create_from_xpm(drawable == null ? null
                : drawable.getHandle(), colormap == null ? null : colormap
                .getHandle(), mask == null ? null : mask.getHandle(),
                transparent == null ? null : transparent.getHandle(), filename));
    }

    /**
     * Create a pixmap from data in XPM format.
     * 
     * @param drawable
     *            a Drawable, used to determine default values for the new
     *            pixmap.
     * @param mask
     *            object where to store a bitmap representing the transparency
     *            mask of the XPM file. Can be <code>null</code>, in which
     *            case transparency will be ignored.
     * @param transparent
     *            the color to be used for the pixels that are transparent in
     *            the input file. Can be <code>null</code>, in which case a
     *            default color will be used.
     * @param data
     *            array containing the the XPM data.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, Bitmap mask, Color transparent, byte[] data) {
        super(gdk_pixmap_create_from_xpm_d(drawable.getHandle(),
                mask == null ? null : mask.getHandle(),
                transparent == null ? null : transparent.getHandle(), data));
    }

    /**
     * Create a pixmap from data in XPM format using a particular colormap.
     * 
     * @param drawable
     *            a Drawable, used to determine default values for the new
     *            pixmap. Can be <code>null</code> if colormap is given.
     * @param colormap
     *            the GdkColormap that the new pixmap will be use. If omitted,
     *            the colormap for window will be used.
     * @param mask
     *            object where to store a bitmap representing the transparency
     *            mask of the XPM file. Can be <code>null</code>, in which
     *            case transparency will be ignored.
     * @param transparent
     *            the color to be used for the pixels that are transparent in
     *            the input file. Can be <code>null</code>, in which case a
     *            default color will be used.
     * @param data
     *            array containing the the XPM data.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixmap(Drawable drawable, Colormap colormap, Bitmap mask,
            Color transparent, byte[] data) {
        super(gdk_pixmap_colormap_create_from_xpm_d(drawable == null ? null
                : drawable.getHandle(), colormap == null ? null : colormap
                .getHandle(), mask == null ? null : mask.getHandle(),
                transparent == null ? null : transparent.getHandle(), data));
    }

    /**
     * Construct a new Pixmap from a handle to a native resource.
     * 
     * @param handle
     *            The handle to the native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    Pixmap(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gdk_pixmap_get_type());
    }

    native static final protected int gdk_pixmap_get_type();

    native static final protected Handle gdk_pixmap_new(Handle window,
            int width, int height, int depth);

    native static final protected Handle gdk_pixmap_create_from_data(
            Handle window, byte[] data, int width, int height, int depth,
            Handle fg, Handle bg);

    native static final protected Handle gdk_pixmap_create_from_xpm(
            Handle window, Handle mask, Handle transparentColor, String filename);

    native static final protected Handle gdk_pixmap_colormap_create_from_xpm(
            Handle window, Handle colormap, Handle mask,
            Handle transparentColor, String filename);

    native static final protected Handle gdk_pixmap_create_from_xpm_d(
            Handle window, Handle mask, Handle transparentColor, byte[] data);

    native static final protected Handle gdk_pixmap_colormap_create_from_xpm_d(
            Handle window, Handle colormap, Handle mask,
            Handle transparentColor, byte[] data);
    // native static final protected int gdk_pixmap_foreign_new (Handle anid);

}
