/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/**
 * Represents a rectangle with x, y, width and height members.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Rectangle</code>.
 */
package org.gnu.gdk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;

public class Rectangle extends Boxed {
    /** Construct an empty Rectangle */
    public Rectangle() {
        super(gdk_rectangle_new());
    }

    /**
     * Construct a Rectangle providing the x and y left-top coordinates, width
     * and height.
     * 
     * @param x
     * @param y
     * @param width
     * @param height
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle(int x, int y, int width, int height) {
        super(gdk_rectangle_new());
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
    }

    public Rectangle(Handle handle) {
        super(handle);
    }

    /**
     * For internal use of Java-Gnome only;
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Rectangle getRectangle(Handle handle) {
        if (handle == null) {
            return null;
        }

        Rectangle obj = (Rectangle) Boxed.getBoxedFromHandle(handle);

        if (obj == null) {
            obj = new Rectangle(handle);
        }

        return obj;
    }

    /**
     * Retrieve the x coordinate for the Rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getX() {
        return Rectangle.getX(getHandle());
    }

    /**
     * Set the x coordinate for the Rectangle.
     * 
     * @param x
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setX(int x) {
        setX(getHandle(), x);
    }

    /**
     * Retrieve the y coordinate for the Rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getY() {
        return Rectangle.getY(getHandle());
    }

    /**
     * Set the y coordinate for the Rectangle.
     * 
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setY(int y) {
        setY(getHandle(), y);
    }

    /**
     * Retrieve the width of the Rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        return Rectangle.getWidth(getHandle());
    }

    /**
     * Set the width of the Rectangle.
     * 
     * @param width
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWidth(int width) {
        setWidth(getHandle(), width);
    }

    /**
     * Retrieve the height of the Rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeight() {
        return Rectangle.getHeight(getHandle());
    }

    /**
     * Set the height of the Rectangle.
     * 
     * @param heigth
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setHeight(int heigth) {
        setHeight(getHandle(), heigth);
    }

    /**
     * Returns the intersection of this Rectangle and the provided Rectangle or
     * null if there is no intersection.
     * 
     * @param rect
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle intersect(Rectangle rect) {
        Handle handle = gdk_rectangle_intersect(getHandle(), rect.getHandle());
        return Rectangle.getRectangle(handle);
    }

    /**
     * Returns the union of this Rectangle and the provided Rectangle or null if
     * there is no union.
     * 
     * @param rect
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle union(Rectangle rect) {
        Handle handle = gdk_rectangle_union(getHandle(), rect.getHandle());
        return Rectangle.getRectangle(handle);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gdk_rectangle_get_type());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Rectangle getRectangleFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed obj = Boxed.getBoxedFromHandle(hndl);
            return (obj != null) ? (Rectangle) obj : new Rectangle(hndl);
        }
        return null;
    }

    native static final protected int getX(Handle obj);

    native final protected void setX(Handle obj, int x);

    native static final protected int getY(Handle obj);

    native final protected void setY(Handle obj, int y);

    native static final protected int getWidth(Handle obj);

    native final protected void setWidth(Handle obj, int width);

    native static final protected int getHeight(Handle obj);

    native final protected void setHeight(Handle obj, int height);

    native static final protected Handle gdk_rectangle_intersect(Handle src1,
            Handle src2);

    native static final protected Handle gdk_rectangle_union(Handle src1,
            Handle src2);

    native static final protected Handle gdk_rectangle_new();

    native static final protected void gdk_rectangle_free(Handle handle);

    native static final protected int gdk_rectangle_get_type();
}
