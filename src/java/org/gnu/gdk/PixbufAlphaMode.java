/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class PixbufAlphaMode extends Enum {

    static final private int _BILEVEL = 0;

    static final public org.gnu.gdk.PixbufAlphaMode BILEVEL = new org.gnu.gdk.PixbufAlphaMode(
            _BILEVEL);

    static final private int _FULL = 1;

    static final public org.gnu.gdk.PixbufAlphaMode FULL = new org.gnu.gdk.PixbufAlphaMode(
            _FULL);

    static final private org.gnu.gdk.PixbufAlphaMode[] theInterned = new org.gnu.gdk.PixbufAlphaMode[] {
            BILEVEL, FULL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.PixbufAlphaMode theSacrificialOne = new org.gnu.gdk.PixbufAlphaMode(
            0);

    static public org.gnu.gdk.PixbufAlphaMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.PixbufAlphaMode already = (org.gnu.gdk.PixbufAlphaMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.PixbufAlphaMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PixbufAlphaMode(int value) {
        value_ = value;
    }

    public org.gnu.gdk.PixbufAlphaMode or(org.gnu.gdk.PixbufAlphaMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.PixbufAlphaMode and(org.gnu.gdk.PixbufAlphaMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.PixbufAlphaMode xor(org.gnu.gdk.PixbufAlphaMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.PixbufAlphaMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
