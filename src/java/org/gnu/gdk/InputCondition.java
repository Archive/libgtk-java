/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class InputCondition extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _READ = 1 << 0;

    static final public org.gnu.gdk.InputCondition READ = new org.gnu.gdk.InputCondition(
            _READ);

    static final private int _WRITE = 1 << 1;

    static final public org.gnu.gdk.InputCondition WRITE = new org.gnu.gdk.InputCondition(
            _WRITE);

    static final private int _EXCEPTION = 1 << 2;

    static final public org.gnu.gdk.InputCondition EXCEPTION = new org.gnu.gdk.InputCondition(
            _EXCEPTION);

    static final private org.gnu.gdk.InputCondition[] theInterned = new org.gnu.gdk.InputCondition[] {
            new org.gnu.gdk.InputCondition(0), READ, WRITE,
            new org.gnu.gdk.InputCondition(3), EXCEPTION }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.InputCondition theSacrificialOne = new org.gnu.gdk.InputCondition(
            0);

    static public org.gnu.gdk.InputCondition intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.InputCondition already = (org.gnu.gdk.InputCondition) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.InputCondition(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private InputCondition(int value) {
        value_ = value;
    }

    public org.gnu.gdk.InputCondition or(org.gnu.gdk.InputCondition other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.InputCondition and(org.gnu.gdk.InputCondition other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.InputCondition xor(org.gnu.gdk.InputCondition other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.InputCondition other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
