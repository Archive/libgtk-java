/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Represents a horizontal line of pixels starting with the pixel at coordinates
 * x,y and ending before x+width,y.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Span</code>.
 */
public class Span extends MemStruct {

    protected Span(Handle handle) {
        super(handle);
    }

    public Span(int x, int y, int width) {
        super(gdk_span_new(x, y, width));
    }

    /**
     * Retrieve the x coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getX() {
        return getX(getHandle());
    }

    /**
     * Set the x coordinate.
     * 
     * @param x
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setX(int x) {
        setX(getHandle(), x);
    }

    /**
     * Retrieve the y coordinate.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getY() {
        return getY(getHandle());
    }

    /**
     * Set the y coordinate.
     * 
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setY(int y) {
        setY(getHandle(), y);
    }

    /**
     * Retrieve the width of the Span.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        return getWidth(getHandle());
    }

    /**
     * Set the width of the Span.
     * 
     * @param width
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWidth(int width) {
        setWidth(getHandle(), width);
    }

    native static final protected Handle gdk_span_new(int x, int y, int width);

    native static final protected int getX(Handle obj);

    native static final protected void setX(Handle obj, int x);

    native static final protected int getY(Handle obj);

    native static final protected void setY(Handle obj, int y);

    native static final protected int getWidth(Handle obj);

    native final protected void setWidth(Handle obj, int width);

}
