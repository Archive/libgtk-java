/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.EventKey</code>.
 */
public class EventKey extends Event {
    /**
     * Promotion constructor. Construct the specific event object by promoting
     * (sharing the <tt>Handle</tt> object of) the base <tt>Event</tt>.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventKey(Event evt) {
        // We share the handle with both instances. This avoids
        // copying the event (via the constructor ( super( evt ) ).
        // It is more of a "promotion" constructor.
        super(evt.getHandle());
    }

    public EventKey(Handle handle) {
        super(handle);
    }

    public static EventKey getEventKey(Handle handle) {
        if (handle == null)
            return null;

        EventKey obj = (EventKey) Boxed.getBoxedFromHandle(handle);

        if (obj == null)
            obj = new EventKey(handle);

        return obj;
    }

    public int getKeyVal() {
        return EventKey.getKeyval(getHandle());
    }

    public String getString() {
        return EventKey.getString(getHandle());
    }

    public int getGroup() {
        return EventKey.getGroup(getHandle());
    }

    public int getLength() {
        return EventKey.getLength(getHandle());
    }

    public int getState() {
        return EventKey.getState(getHandle());
    }

    public ModifierType getModifierKey() {
        return ModifierType.intern(getState());
    }

    public Window getWindow() {
        return Window.getWindowFromHandle(getWindow(getHandle()));
    }

    native static final protected Handle getWindow(Handle obj);

    native static final protected boolean getSendEvent(Handle obj);

    native static final protected int getTime(Handle obj);

    native static final protected int getState(Handle obj);

    native static final protected int getKeyval(Handle obj);

    native static final protected int getLength(Handle obj);

    native static final protected String getString(Handle obj);

    native static final protected int getHardwareKeycode(Handle obj);

    native static final protected int getGroup(Handle obj);
}
