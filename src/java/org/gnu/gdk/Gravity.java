/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class Gravity extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORTH_WEST = 1;

    static final public org.gnu.gdk.Gravity NORTH_WEST = new org.gnu.gdk.Gravity(
            _NORTH_WEST);

    static final private int _NORTH = 2;

    static final public org.gnu.gdk.Gravity NORTH = new org.gnu.gdk.Gravity(
            _NORTH);

    static final private int _NORTH_EAST = 3;

    static final public org.gnu.gdk.Gravity NORTH_EAST = new org.gnu.gdk.Gravity(
            _NORTH_EAST);

    static final private int _WEST = 4;

    static final public org.gnu.gdk.Gravity WEST = new org.gnu.gdk.Gravity(
            _WEST);

    static final private int _CENTER = 5;

    static final public org.gnu.gdk.Gravity CENTER = new org.gnu.gdk.Gravity(
            _CENTER);

    static final private int _EAST = 6;

    static final public org.gnu.gdk.Gravity EAST = new org.gnu.gdk.Gravity(
            _EAST);

    static final private int _SOUTH_WEST = 7;

    static final public org.gnu.gdk.Gravity SOUTH_WEST = new org.gnu.gdk.Gravity(
            _SOUTH_WEST);

    static final private int _SOUTH = 8;

    static final public org.gnu.gdk.Gravity SOUTH = new org.gnu.gdk.Gravity(
            _SOUTH);

    static final private int _SOUTH_EAST = 9;

    static final public org.gnu.gdk.Gravity SOUTH_EAST = new org.gnu.gdk.Gravity(
            _SOUTH_EAST);

    static final private int _STATIC = 10;

    static final public org.gnu.gdk.Gravity STATIC = new org.gnu.gdk.Gravity(
            _STATIC);

    static final private org.gnu.gdk.Gravity[] theInterned = new org.gnu.gdk.Gravity[] {
            new org.gnu.gdk.Gravity(0), NORTH_WEST, NORTH, NORTH_EAST, WEST,
            CENTER, EAST, SOUTH_WEST, SOUTH, SOUTH_EAST, STATIC }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.Gravity theSacrificialOne = new org.gnu.gdk.Gravity(
            0);

    static public org.gnu.gdk.Gravity intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.Gravity already = (org.gnu.gdk.Gravity) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.Gravity(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Gravity(int value) {
        value_ = value;
    }

    public org.gnu.gdk.Gravity or(org.gnu.gdk.Gravity other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.Gravity and(org.gnu.gdk.Gravity other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.Gravity xor(org.gnu.gdk.Gravity other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.Gravity other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
