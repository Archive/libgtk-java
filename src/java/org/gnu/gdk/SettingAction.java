/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class SettingAction extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NEW = 0;

    static final public org.gnu.gdk.SettingAction NEW = new org.gnu.gdk.SettingAction(
            _NEW);

    static final private int _CHANGED = 1;

    static final public org.gnu.gdk.SettingAction CHANGED = new org.gnu.gdk.SettingAction(
            _CHANGED);

    static final private int _DELETED = 2;

    static final public org.gnu.gdk.SettingAction DELETED = new org.gnu.gdk.SettingAction(
            _DELETED);

    static final private org.gnu.gdk.SettingAction[] theInterned = new org.gnu.gdk.SettingAction[] {
            NEW, CHANGED, DELETED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.SettingAction theSacrificialOne = new org.gnu.gdk.SettingAction(
            0);

    static public org.gnu.gdk.SettingAction intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.SettingAction already = (org.gnu.gdk.SettingAction) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.SettingAction(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SettingAction(int value) {
        value_ = value;
    }

    public org.gnu.gdk.SettingAction or(org.gnu.gdk.SettingAction other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.SettingAction and(org.gnu.gdk.SettingAction other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.SettingAction xor(org.gnu.gdk.SettingAction other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.SettingAction other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
