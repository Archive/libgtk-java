/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/*
 * TODO:
 * Documentation.
 * add functionallity like in awt?
 */

package org.gnu.gdk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Represents a point with x and y coordinates.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Point</code>.
 */
public class Point extends MemStruct {
    /** Construct an empty point */
    public Point() {
        super(gdk_point_new());
    }

    /**
     * Construct a Point providing the x and y coordinates.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Point(int x, int y) {
        super(gdk_point_new());
        setX(x);
        setY(y);
    }

    Point(Handle handle) {
        super(handle);
    }

    /**
     * Retrieve the x coordinate for the Point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getX() {
        return Point.getX(getHandle());
    }

    /**
     * Set the x coordinate for the Point.
     * 
     * @param x
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setX(int x) {
        setX(getHandle(), x);
    }

    /**
     * Retrieve the y coordinate for the Point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getY() {
        return Point.getY(getHandle());
    }

    /**
     * Set the y coordinate for the Point.
     * 
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setY(int y) {
        setY(getHandle(), y);
    }

    native static final protected int getX(Handle obj);

    native final protected void setX(Handle obj, int x);

    native static final protected int getY(Handle obj);

    native final protected void setY(Handle obj, int y);

    native static final protected Handle gdk_point_new();

    native static final protected void gdk_point_free(Handle handle);
}
