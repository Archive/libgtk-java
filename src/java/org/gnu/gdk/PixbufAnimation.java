/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Error;
import org.gnu.glib.GObject;
import org.gnu.glib.JGException;
import org.gnu.glib.Handle;

/**
 * Provides a simple mechanism to load and represent animations. An animation is
 * conceptually a series of frames to be displayed over time. Each frame is the
 * same size. The animation may not be represented as a series of frames
 * internally; for example, it may be stored as a sprite and instructions for
 * moving the sprite around a background. To display an animation you don't need
 * to understand its representation, however; you just ask this class what
 * should be displayed at a given point in time.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufAnimation</code>.
 */
public class PixbufAnimation extends GObject {
    /**
     * Construct a new Pixbuf animation by loading it from a file. The file
     * format is detected automatically. If the files format does not support
     * multi-frame images then an animation with a single frame will be created.
     * 
     * @param filename
     *            The name of the image file.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufAnimation(String filename) throws JGException {
        super(init(filename));
    }

    /**
     * Create a new pixbuf from a handle to a native resource. For internal use.
     * 
     * @param handle
     *            The handle to the native resoufce
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufAnimation(Handle handle) {
        super(handle);
    }

    private static Handle init(String filename) throws JGException {
        Handle error = getNullHandle();
        Handle hndl = gdk_pixbuf_animation_new_from_file(filename, error);
        if (!error.isNull())
            throw new JGException(new Error(error));
        return hndl;
    }

    /**
     * Return the width of the bounding box of a pixbuf animation.
     * 
     * @return The width
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int width() {
        return gdk_pixbuf_animation_get_width(getHandle());
    }

    /**
     * Return the height of the bounding box of a pixbuf animation.
     * 
     * @return The height
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int height() {
        return gdk_pixbuf_animation_get_height(getHandle());
    }

    /**
     * Get an iterator for displaying an animation. The iterator provides the
     * frames that should be displayed at a given time.
     * <p>
     * The start time marks the beginning of animation playback. After creating
     * an iterator, you should immediately display the pixbuf returned by the
     * getPixbuf() method of PixbufAnimationIter. Then, you should install a
     * timeout by some mechanism to ensure that you'll update the image after
     * the delay time. Each time the image is updated, you should reinstall the
     * timeout with the new, possibly-changed delay time.
     * 
     * @param startSec
     * @param startUsec
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufAnimationIter getIter(long startSec, long startUsec) {
        Handle hndl = gdk_pixbuf_animation_get_iter(getHandle(), startSec,
                startUsec);
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (PixbufAnimationIter) obj
                    : new PixbufAnimationIter(hndl);
        }
        return null;
    }

    /**
     * If this animation was loaded from a that turns out to be a plain
     * unaminated image then this method will return true.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isStaticImage() {
        return gdk_pixbuf_animation_is_static_image(getHandle());
    }

    /**
     * If an animation is really just a plain image (has only one frame), this
     * method returns that image. If the animation is an animation, this method
     * returns a reasonable thing to display as a static unanimated image, which
     * might be the first frame, or something more sophisticated. If an
     * animation hasn't loaded any frames yet, this method will return NULL.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getStaticImage() {
        return Pixbuf
                .getPixbufFromHandle(gdk_pixbuf_animation_get_static_image(getHandle()));
    }

    native static final protected int gdk_pixbuf_animation_get_type();

    native static final protected Handle gdk_pixbuf_animation_new_from_file(
            String filename, Handle error);

    native static final protected int gdk_pixbuf_animation_get_width(
            Handle animation);

    native static final protected int gdk_pixbuf_animation_get_height(
            Handle animation);

    native static final protected Handle gdk_pixbuf_animation_get_iter(
            Handle animation, long sec, long usec);

    native static final protected boolean gdk_pixbuf_animation_is_static_image(
            Handle animation);

    native static final protected Handle gdk_pixbuf_animation_get_static_image(
            Handle animation);
}
