/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class WMFunction extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ALL = 1 << 0;

    static final public org.gnu.gdk.WMFunction ALL = new org.gnu.gdk.WMFunction(
            _ALL);

    static final private int _RESIZE = 1 << 1;

    static final public org.gnu.gdk.WMFunction RESIZE = new org.gnu.gdk.WMFunction(
            _RESIZE);

    static final private int _MOVE = 1 << 2;

    static final public org.gnu.gdk.WMFunction MOVE = new org.gnu.gdk.WMFunction(
            _MOVE);

    static final private int _MINIMIZE = 1 << 3;

    static final public org.gnu.gdk.WMFunction MINIMIZE = new org.gnu.gdk.WMFunction(
            _MINIMIZE);

    static final private int _MAXIMIZE = 1 << 4;

    static final public org.gnu.gdk.WMFunction MAXIMIZE = new org.gnu.gdk.WMFunction(
            _MAXIMIZE);

    static final private int _CLOSE = 1 << 5;

    static final public org.gnu.gdk.WMFunction CLOSE = new org.gnu.gdk.WMFunction(
            _CLOSE);

    static final private org.gnu.gdk.WMFunction[] theInterned = new org.gnu.gdk.WMFunction[] {
            new org.gnu.gdk.WMFunction(0), ALL, RESIZE,
            new org.gnu.gdk.WMFunction(3), MOVE, new org.gnu.gdk.WMFunction(5),
            new org.gnu.gdk.WMFunction(6), new org.gnu.gdk.WMFunction(7),
            MINIMIZE, new org.gnu.gdk.WMFunction(9),
            new org.gnu.gdk.WMFunction(10), new org.gnu.gdk.WMFunction(11),
            new org.gnu.gdk.WMFunction(12), new org.gnu.gdk.WMFunction(13),
            new org.gnu.gdk.WMFunction(14), new org.gnu.gdk.WMFunction(15),
            MAXIMIZE, new org.gnu.gdk.WMFunction(17),
            new org.gnu.gdk.WMFunction(18), new org.gnu.gdk.WMFunction(19),
            new org.gnu.gdk.WMFunction(20), new org.gnu.gdk.WMFunction(21),
            new org.gnu.gdk.WMFunction(22), new org.gnu.gdk.WMFunction(23),
            new org.gnu.gdk.WMFunction(24), new org.gnu.gdk.WMFunction(25),
            new org.gnu.gdk.WMFunction(26), new org.gnu.gdk.WMFunction(27),
            new org.gnu.gdk.WMFunction(28), new org.gnu.gdk.WMFunction(29),
            new org.gnu.gdk.WMFunction(30), new org.gnu.gdk.WMFunction(31),
            CLOSE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WMFunction theSacrificialOne = new org.gnu.gdk.WMFunction(
            0);

    static public org.gnu.gdk.WMFunction intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WMFunction already = (org.gnu.gdk.WMFunction) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WMFunction(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WMFunction(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WMFunction or(org.gnu.gdk.WMFunction other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WMFunction and(org.gnu.gdk.WMFunction other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WMFunction xor(org.gnu.gdk.WMFunction other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WMFunction other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
