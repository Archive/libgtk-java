/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class CrossingMode extends Enum {
    static final private int _NORMAL = 0;

    static final public CrossingMode NORMAL = new CrossingMode(_NORMAL);

    static final private int _GRAB = 1;

    static final public CrossingMode GRAB = new CrossingMode(_GRAB);

    static final private int _UNGRAB = 2;

    static final public CrossingMode UNGRAB = new CrossingMode(_UNGRAB);

    static final private CrossingMode[] theInterned = new CrossingMode[] {
            NORMAL, GRAB, UNGRAB };

    static private java.util.Hashtable theInternedExtras;

    static final private CrossingMode theSacrificialOne = new CrossingMode(0);

    static public CrossingMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        CrossingMode already = (CrossingMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new CrossingMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CrossingMode(int value) {
        value_ = value;
    }

    public CrossingMode or(CrossingMode other) {
        return intern(value_ | other.value_);
    }

    public CrossingMode and(CrossingMode other) {
        return intern(value_ & other.value_);
    }

    public CrossingMode xor(CrossingMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(CrossingMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
