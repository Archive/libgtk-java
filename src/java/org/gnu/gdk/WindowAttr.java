/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Attributes to use for a newly-created window.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.WindowAttr</code>.
 */
public class WindowAttr extends MemStruct {

    public WindowAttr(Handle handle) {
        super(handle);
    }

    public WindowAttr() {
        super(gdk_window_attr_new());
    }

    public String getTitle() {
        return WindowAttr.getTitle(getHandle());
    }

    public void setTitle(String title) {
        if (title != null)
            setTitle(getHandle(), title);
    }

    // it will change in the future.
    public int getEventMask() {
        return WindowAttr.getEventMask(getHandle());
    }

    public void setEventMask(int evtMask) {
        setEventMask(getHandle(), evtMask);
    }

    public org.gnu.gdk.Point getPosition() {
        int x = WindowAttr.getX(getHandle());
        int y = WindowAttr.getY(getHandle());
        return new org.gnu.gdk.Point(x, y);
    }

    public void setPosition(int x, int y) {
        setX(getHandle(), x);
        setY(getHandle(), y);
    }

    public void setDimension(int width, int height) {
        if (width >= 0 && height >= 0) {
            setWidth(getHandle(), width);
            setHeight(getHandle(), height);
        }
    }

    public Dimension getDimension() {
        int width = WindowAttr.getWidth(getHandle());
        int height = WindowAttr.getHeight(getHandle());
        return new Dimension(width, height);
    }

    public Visual getVisual() {
        return Visual.getVisualFromHandle(getVisual(getHandle()));
    }

    public void setVisual(Visual visual) {
        setVisual(getHandle(), visual.getHandle());
    }

    public Colormap getColormap() {
        return Colormap.getColormapFromHandle(getColormap(getHandle()));
    }

    public void setColormap(Colormap map) {
        setColormap(getHandle(), map.getHandle());
    }

    /**
     * Get the cursor used by this WindowAttr.
     * 
     * <p>
     * Use {@link Window#setCursor} to set the <tt>Window</tt>'s cursor.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Cursor getCursor() {
        return Cursor.getCursorFromHandle(getCursor(getHandle()));
    }

    /**
     * Set the cursor used by this WindowAttr.
     * 
     * <p>
     * Use {@link Window#setCursor} to set the <tt>Window</tt>'s cursor.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCursor(Cursor cursor) {
        setCursor(getHandle(), cursor.getHandle());
    }

    native static final protected Handle gdk_window_attr_new();

    native static final protected String getTitle(Handle obj);

    native static final protected void setTitle(Handle obj, String title);

    native static final protected int getEventMask(Handle obj);

    native static final protected void setEventMask(Handle obj, int event_mask);

    native static final protected int getX(Handle obj);

    native static final protected void setX(Handle obj, int x);

    native static final protected int getY(Handle obj);

    native static final protected void setY(Handle obj, int y);

    native static final protected int getWidth(Handle obj);

    native static final protected void setWidth(Handle obj, int width);

    native static final protected int getHeight(Handle obj);

    native static final protected void setHeight(Handle obj, int height);

    native static final protected int getWclass(Handle obj);

    native static final protected void setWclass(Handle obj, int wclass);

    native static final protected Handle getVisual(Handle obj);

    native static final protected void setVisual(Handle obj, Handle visual);

    native static final protected Handle getColormap(Handle obj);

    native static final protected void setColormap(Handle obj, Handle colormap);

    native static final protected int getWindowType(Handle obj);

    native static final protected void setWindowType(Handle obj, int window_type);

    native static final protected Handle getCursor(Handle obj);

    native static final protected void setCursor(Handle obj, Handle cursor);

    native static final protected String getWmclassName(Handle obj);

    native static final protected void setWmclassName(Handle obj, String wmclass_name);

    native static final protected String getWmclassClass(Handle obj);

    native static final protected void setWmclassClass(Handle obj, String wmclass_class);

    native static final protected boolean getOverrideRedirect(Handle obj);

    native static final protected void setOverrideRedirect(Handle obj,
            boolean override_redirect);
}
