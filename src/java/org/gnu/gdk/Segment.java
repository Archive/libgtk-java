/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Specifies the start and end point for a line.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Segment</code>.
 */
public class Segment extends MemStruct {

    protected Segment(Handle handle) {
        super(handle);
    }

    public Segment(int x1, int y1, int x2, int y2) {
        super(gdk_segment_new(x1, y1, x2, y2));
    }

    public Segment() {
        super(gdk_segment_new(0, 0, 0, 0));
    }

    public int getX1() {
        return getX1(getHandle());
    }

    public void setX1(int x1) {
        setX1(getHandle(), x1);
    }

    public int getY1() {
        return getY1(getHandle());
    }

    public void setY1(int y1) {
        setY1(getHandle(), y1);
    }

    public int getX2() {
        return getX2(getHandle());
    }

    public void setX2(int x1) {
        setX2(getHandle(), x1);
    }

    public int getY2() {
        return getY2(getHandle());
    }

    public void setY2(int y1) {
        setY2(getHandle(), y1);
    }

    native static final protected Handle gdk_segment_new(int x1, int y1,
            int x2, int y2);

    native static final protected int getX1(Handle obj);

    native static final protected void setX1(Handle obj, int x1);

    native static final protected int getY1(Handle obj);

    native static final protected void setY1(Handle obj, int y1);

    native static final protected int getX2(Handle obj);

    native static final protected void setX2(Handle obj, int x2);

    native static final protected int getY2(Handle obj);

    native static final protected void setY2(Handle obj, int y2);
}
