/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class VisibilityState extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _UNOBSCURED = 0;

    static final public org.gnu.gdk.VisibilityState UNOBSCURED = new org.gnu.gdk.VisibilityState(
            _UNOBSCURED);

    static final private int _PARTIAL = 1;

    static final public org.gnu.gdk.VisibilityState PARTIAL = new org.gnu.gdk.VisibilityState(
            _PARTIAL);

    static final private int _FULLY_OBSCURED = 2;

    static final public org.gnu.gdk.VisibilityState FULLY_OBSCURED = new org.gnu.gdk.VisibilityState(
            _FULLY_OBSCURED);

    static final private org.gnu.gdk.VisibilityState[] theInterned = new org.gnu.gdk.VisibilityState[] {
            UNOBSCURED, PARTIAL, FULLY_OBSCURED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.VisibilityState theSacrificialOne = new org.gnu.gdk.VisibilityState(
            0);

    static public org.gnu.gdk.VisibilityState intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.VisibilityState already = (org.gnu.gdk.VisibilityState) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.VisibilityState(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private VisibilityState(int value) {
        value_ = value;
    }

    public org.gnu.gdk.VisibilityState or(org.gnu.gdk.VisibilityState other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.VisibilityState and(org.gnu.gdk.VisibilityState other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.VisibilityState xor(org.gnu.gdk.VisibilityState other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.VisibilityState other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
