/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class WindowHints extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _POS = 1 << 0;

    static final public org.gnu.gdk.WindowHints POS = new org.gnu.gdk.WindowHints(
            _POS);

    static final private int _MIN_SIZE = 1 << 1;

    static final public org.gnu.gdk.WindowHints MIN_SIZE = new org.gnu.gdk.WindowHints(
            _MIN_SIZE);

    static final private int _MAX_SIZE = 1 << 2;

    static final public org.gnu.gdk.WindowHints MAX_SIZE = new org.gnu.gdk.WindowHints(
            _MAX_SIZE);

    static final private int _BASE_SIZE = 1 << 3;

    static final public org.gnu.gdk.WindowHints BASE_SIZE = new org.gnu.gdk.WindowHints(
            _BASE_SIZE);

    static final private int _ASPECG = 1 << 4;

    static final public org.gnu.gdk.WindowHints ASPECG = new org.gnu.gdk.WindowHints(
            _ASPECG);

    static final private int _RESIZE_INC = 1 << 5;

    static final public org.gnu.gdk.WindowHints RESIZE_INC = new org.gnu.gdk.WindowHints(
            _RESIZE_INC);

    static final private int _WIN_GRAVITY = 1 << 6;

    static final public org.gnu.gdk.WindowHints WIN_GRAVITY = new org.gnu.gdk.WindowHints(
            _WIN_GRAVITY);

    static final private int _USER_POS = 1 << 7;

    static final public org.gnu.gdk.WindowHints USER_POS = new org.gnu.gdk.WindowHints(
            _USER_POS);

    static final private int _USER_SIZE = 1 << 8;

    static final public org.gnu.gdk.WindowHints USER_SIZE = new org.gnu.gdk.WindowHints(
            _USER_SIZE);

    static final private org.gnu.gdk.WindowHints[] theInterned = new org.gnu.gdk.WindowHints[] {
            new org.gnu.gdk.WindowHints(0), POS, MIN_SIZE,
            new org.gnu.gdk.WindowHints(3), MAX_SIZE,
            new org.gnu.gdk.WindowHints(5), new org.gnu.gdk.WindowHints(6),
            new org.gnu.gdk.WindowHints(7), BASE_SIZE,
            new org.gnu.gdk.WindowHints(9), new org.gnu.gdk.WindowHints(10),
            new org.gnu.gdk.WindowHints(11), new org.gnu.gdk.WindowHints(12),
            new org.gnu.gdk.WindowHints(13), new org.gnu.gdk.WindowHints(14),
            new org.gnu.gdk.WindowHints(15), ASPECG,
            new org.gnu.gdk.WindowHints(17), new org.gnu.gdk.WindowHints(18),
            new org.gnu.gdk.WindowHints(19), new org.gnu.gdk.WindowHints(20),
            new org.gnu.gdk.WindowHints(21), new org.gnu.gdk.WindowHints(22),
            new org.gnu.gdk.WindowHints(23), new org.gnu.gdk.WindowHints(24),
            new org.gnu.gdk.WindowHints(25), new org.gnu.gdk.WindowHints(26),
            new org.gnu.gdk.WindowHints(27), new org.gnu.gdk.WindowHints(28),
            new org.gnu.gdk.WindowHints(29), new org.gnu.gdk.WindowHints(30),
            new org.gnu.gdk.WindowHints(31), RESIZE_INC,
            new org.gnu.gdk.WindowHints(33), new org.gnu.gdk.WindowHints(34),
            new org.gnu.gdk.WindowHints(35), new org.gnu.gdk.WindowHints(36),
            new org.gnu.gdk.WindowHints(37), new org.gnu.gdk.WindowHints(38),
            new org.gnu.gdk.WindowHints(39), new org.gnu.gdk.WindowHints(40),
            new org.gnu.gdk.WindowHints(41), new org.gnu.gdk.WindowHints(42),
            new org.gnu.gdk.WindowHints(43), new org.gnu.gdk.WindowHints(44),
            new org.gnu.gdk.WindowHints(45), new org.gnu.gdk.WindowHints(46),
            new org.gnu.gdk.WindowHints(47), new org.gnu.gdk.WindowHints(48),
            new org.gnu.gdk.WindowHints(49), new org.gnu.gdk.WindowHints(50),
            new org.gnu.gdk.WindowHints(51), new org.gnu.gdk.WindowHints(52),
            new org.gnu.gdk.WindowHints(53), new org.gnu.gdk.WindowHints(54),
            new org.gnu.gdk.WindowHints(55), new org.gnu.gdk.WindowHints(56),
            new org.gnu.gdk.WindowHints(57), new org.gnu.gdk.WindowHints(58),
            new org.gnu.gdk.WindowHints(59), new org.gnu.gdk.WindowHints(60),
            new org.gnu.gdk.WindowHints(61), new org.gnu.gdk.WindowHints(62),
            new org.gnu.gdk.WindowHints(63), WIN_GRAVITY,
            new org.gnu.gdk.WindowHints(65), new org.gnu.gdk.WindowHints(66),
            new org.gnu.gdk.WindowHints(67), new org.gnu.gdk.WindowHints(68),
            new org.gnu.gdk.WindowHints(69), new org.gnu.gdk.WindowHints(70),
            new org.gnu.gdk.WindowHints(71), new org.gnu.gdk.WindowHints(72),
            new org.gnu.gdk.WindowHints(73), new org.gnu.gdk.WindowHints(74),
            new org.gnu.gdk.WindowHints(75), new org.gnu.gdk.WindowHints(76),
            new org.gnu.gdk.WindowHints(77), new org.gnu.gdk.WindowHints(78),
            new org.gnu.gdk.WindowHints(79), new org.gnu.gdk.WindowHints(80),
            new org.gnu.gdk.WindowHints(81), new org.gnu.gdk.WindowHints(82),
            new org.gnu.gdk.WindowHints(83), new org.gnu.gdk.WindowHints(84),
            new org.gnu.gdk.WindowHints(85), new org.gnu.gdk.WindowHints(86),
            new org.gnu.gdk.WindowHints(87), new org.gnu.gdk.WindowHints(88),
            new org.gnu.gdk.WindowHints(89), new org.gnu.gdk.WindowHints(90),
            new org.gnu.gdk.WindowHints(91), new org.gnu.gdk.WindowHints(92),
            new org.gnu.gdk.WindowHints(93), new org.gnu.gdk.WindowHints(94),
            new org.gnu.gdk.WindowHints(95), new org.gnu.gdk.WindowHints(96),
            new org.gnu.gdk.WindowHints(97), new org.gnu.gdk.WindowHints(98),
            new org.gnu.gdk.WindowHints(99), new org.gnu.gdk.WindowHints(100),
            new org.gnu.gdk.WindowHints(101), new org.gnu.gdk.WindowHints(102),
            new org.gnu.gdk.WindowHints(103), new org.gnu.gdk.WindowHints(104),
            new org.gnu.gdk.WindowHints(105), new org.gnu.gdk.WindowHints(106),
            new org.gnu.gdk.WindowHints(107), new org.gnu.gdk.WindowHints(108),
            new org.gnu.gdk.WindowHints(109), new org.gnu.gdk.WindowHints(110),
            new org.gnu.gdk.WindowHints(111), new org.gnu.gdk.WindowHints(112),
            new org.gnu.gdk.WindowHints(113), new org.gnu.gdk.WindowHints(114),
            new org.gnu.gdk.WindowHints(115), new org.gnu.gdk.WindowHints(116),
            new org.gnu.gdk.WindowHints(117), new org.gnu.gdk.WindowHints(118),
            new org.gnu.gdk.WindowHints(119), new org.gnu.gdk.WindowHints(120),
            new org.gnu.gdk.WindowHints(121), new org.gnu.gdk.WindowHints(122),
            new org.gnu.gdk.WindowHints(123), new org.gnu.gdk.WindowHints(124),
            new org.gnu.gdk.WindowHints(125), new org.gnu.gdk.WindowHints(126),
            new org.gnu.gdk.WindowHints(127), USER_POS,
            new org.gnu.gdk.WindowHints(129), new org.gnu.gdk.WindowHints(130),
            new org.gnu.gdk.WindowHints(131), new org.gnu.gdk.WindowHints(132),
            new org.gnu.gdk.WindowHints(133), new org.gnu.gdk.WindowHints(134),
            new org.gnu.gdk.WindowHints(135), new org.gnu.gdk.WindowHints(136),
            new org.gnu.gdk.WindowHints(137), new org.gnu.gdk.WindowHints(138),
            new org.gnu.gdk.WindowHints(139), new org.gnu.gdk.WindowHints(140),
            new org.gnu.gdk.WindowHints(141), new org.gnu.gdk.WindowHints(142),
            new org.gnu.gdk.WindowHints(143), new org.gnu.gdk.WindowHints(144),
            new org.gnu.gdk.WindowHints(145), new org.gnu.gdk.WindowHints(146),
            new org.gnu.gdk.WindowHints(147), new org.gnu.gdk.WindowHints(148),
            new org.gnu.gdk.WindowHints(149), new org.gnu.gdk.WindowHints(150),
            new org.gnu.gdk.WindowHints(151), new org.gnu.gdk.WindowHints(152),
            new org.gnu.gdk.WindowHints(153), new org.gnu.gdk.WindowHints(154),
            new org.gnu.gdk.WindowHints(155), new org.gnu.gdk.WindowHints(156),
            new org.gnu.gdk.WindowHints(157), new org.gnu.gdk.WindowHints(158),
            new org.gnu.gdk.WindowHints(159), new org.gnu.gdk.WindowHints(160),
            new org.gnu.gdk.WindowHints(161), new org.gnu.gdk.WindowHints(162),
            new org.gnu.gdk.WindowHints(163), new org.gnu.gdk.WindowHints(164),
            new org.gnu.gdk.WindowHints(165), new org.gnu.gdk.WindowHints(166),
            new org.gnu.gdk.WindowHints(167), new org.gnu.gdk.WindowHints(168),
            new org.gnu.gdk.WindowHints(169), new org.gnu.gdk.WindowHints(170),
            new org.gnu.gdk.WindowHints(171), new org.gnu.gdk.WindowHints(172),
            new org.gnu.gdk.WindowHints(173), new org.gnu.gdk.WindowHints(174),
            new org.gnu.gdk.WindowHints(175), new org.gnu.gdk.WindowHints(176),
            new org.gnu.gdk.WindowHints(177), new org.gnu.gdk.WindowHints(178),
            new org.gnu.gdk.WindowHints(179), new org.gnu.gdk.WindowHints(180),
            new org.gnu.gdk.WindowHints(181), new org.gnu.gdk.WindowHints(182),
            new org.gnu.gdk.WindowHints(183), new org.gnu.gdk.WindowHints(184),
            new org.gnu.gdk.WindowHints(185), new org.gnu.gdk.WindowHints(186),
            new org.gnu.gdk.WindowHints(187), new org.gnu.gdk.WindowHints(188),
            new org.gnu.gdk.WindowHints(189), new org.gnu.gdk.WindowHints(190),
            new org.gnu.gdk.WindowHints(191), new org.gnu.gdk.WindowHints(192),
            new org.gnu.gdk.WindowHints(193), new org.gnu.gdk.WindowHints(194),
            new org.gnu.gdk.WindowHints(195), new org.gnu.gdk.WindowHints(196),
            new org.gnu.gdk.WindowHints(197), new org.gnu.gdk.WindowHints(198),
            new org.gnu.gdk.WindowHints(199), new org.gnu.gdk.WindowHints(200),
            new org.gnu.gdk.WindowHints(201), new org.gnu.gdk.WindowHints(202),
            new org.gnu.gdk.WindowHints(203), new org.gnu.gdk.WindowHints(204),
            new org.gnu.gdk.WindowHints(205), new org.gnu.gdk.WindowHints(206),
            new org.gnu.gdk.WindowHints(207), new org.gnu.gdk.WindowHints(208),
            new org.gnu.gdk.WindowHints(209), new org.gnu.gdk.WindowHints(210),
            new org.gnu.gdk.WindowHints(211), new org.gnu.gdk.WindowHints(212),
            new org.gnu.gdk.WindowHints(213), new org.gnu.gdk.WindowHints(214),
            new org.gnu.gdk.WindowHints(215), new org.gnu.gdk.WindowHints(216),
            new org.gnu.gdk.WindowHints(217), new org.gnu.gdk.WindowHints(218),
            new org.gnu.gdk.WindowHints(219), new org.gnu.gdk.WindowHints(220),
            new org.gnu.gdk.WindowHints(221), new org.gnu.gdk.WindowHints(222),
            new org.gnu.gdk.WindowHints(223), new org.gnu.gdk.WindowHints(224),
            new org.gnu.gdk.WindowHints(225), new org.gnu.gdk.WindowHints(226),
            new org.gnu.gdk.WindowHints(227), new org.gnu.gdk.WindowHints(228),
            new org.gnu.gdk.WindowHints(229), new org.gnu.gdk.WindowHints(230),
            new org.gnu.gdk.WindowHints(231), new org.gnu.gdk.WindowHints(232),
            new org.gnu.gdk.WindowHints(233), new org.gnu.gdk.WindowHints(234),
            new org.gnu.gdk.WindowHints(235), new org.gnu.gdk.WindowHints(236),
            new org.gnu.gdk.WindowHints(237), new org.gnu.gdk.WindowHints(238),
            new org.gnu.gdk.WindowHints(239), new org.gnu.gdk.WindowHints(240),
            new org.gnu.gdk.WindowHints(241), new org.gnu.gdk.WindowHints(242),
            new org.gnu.gdk.WindowHints(243), new org.gnu.gdk.WindowHints(244),
            new org.gnu.gdk.WindowHints(245), new org.gnu.gdk.WindowHints(246),
            new org.gnu.gdk.WindowHints(247), new org.gnu.gdk.WindowHints(248),
            new org.gnu.gdk.WindowHints(249), new org.gnu.gdk.WindowHints(250),
            new org.gnu.gdk.WindowHints(251), new org.gnu.gdk.WindowHints(252),
            new org.gnu.gdk.WindowHints(253), new org.gnu.gdk.WindowHints(254),
            new org.gnu.gdk.WindowHints(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowHints theSacrificialOne = new org.gnu.gdk.WindowHints(
            0);

    static public org.gnu.gdk.WindowHints intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowHints already = (org.gnu.gdk.WindowHints) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowHints(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowHints(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowHints or(org.gnu.gdk.WindowHints other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowHints and(org.gnu.gdk.WindowHints other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowHints xor(org.gnu.gdk.WindowHints other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowHints other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
