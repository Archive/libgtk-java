/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class FillRule extends Enum {

    /*
     * public FillRule(int value){ this.value_=value; }
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */

    static final private int _EVEN_ODD = 0;

    /**
     * areas which are overlapped an odd number of times are included in the
     * region, while areas overlapped an even number of times are not.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.FillRule EVEN_ODD = new org.gnu.gdk.FillRule(
            _EVEN_ODD);

    static final private int _WINDING = 1;

    /**
     * overlapping areas are always included.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.FillRule WINDING = new org.gnu.gdk.FillRule(
            _WINDING);

    static final private org.gnu.gdk.FillRule[] theInterned = new org.gnu.gdk.FillRule[] {
            EVEN_ODD, WINDING }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.FillRule theSacrificialOne = new org.gnu.gdk.FillRule(
            0);

    static public org.gnu.gdk.FillRule intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.FillRule already = (org.gnu.gdk.FillRule) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.FillRule(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FillRule(int value) {
        value_ = value;
    }

    public org.gnu.gdk.FillRule or(org.gnu.gdk.FillRule other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.FillRule and(org.gnu.gdk.FillRule other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.FillRule xor(org.gnu.gdk.FillRule other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.FillRule other) {
        return (value_ & other.value_) == other.value_;
    }
}
