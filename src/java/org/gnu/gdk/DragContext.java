/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * A DragContext holds information about a drag in process. It is used on both
 * source and destination sides.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.DragContext</code>.
 */
public class DragContext extends GObject {
    /**
     * Construct a DragContext.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DragContext() {
        super(gdk_drag_context_new());
    }

    /**
     * Construct a DragContext using native resources. Used internally by
     * java-gnome
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DragContext(Handle handle) {
        super(handle);
    }

    /**
     * Return the source window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getSource() {
        return Window.getWindowFromHandle(getSourceWindow(getHandle()));
    }

    /**
     * Return the destination window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getDestination() {
        return Window.getWindowFromHandle(getDestWindow(getHandle()));
    }

    /**
     * Return the Drag Protocol
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DragProtocol getProtocol() {
        return DragProtocol.intern(getProtocol(getHandle()));
    }

    /**
     * Returns true if the context is used on the source side.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isSource() {
        return getIsSource(getHandle());
    }

    /**
     * Return the action suggested by the source.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DragAction getSuggestedAction() {
        return DragAction.intern(getSuggestedAction(getHandle()));
    }

    /**
     * Return the action chosen by the destination.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public DragAction getAction() {
        return DragAction.intern(getAction(getHandle()));
    }

    public Atom[] getTargets() {
        Handle[] atomHandles = getTargets(getHandle());
        if (atomHandles == null) {
            return new Atom[0];
        }
        Atom[] atoms = new Atom[atomHandles.length];
        for (int i = 0, c = atoms.length; i < c; ++i) {
            atoms[i] = new Atom(atomHandles[i]);
        }
        return atoms;
    }

    public void finish(boolean success, boolean delete, int time) {
        gtk_drag_finish(getHandle(), success, delete, time);
    }

    native static final private Handle[] getTargets(Handle obj);

    native static final protected int getProtocol(Handle obj);

    native static final protected boolean getIsSource(Handle obj);

    native static final protected Handle getSourceWindow(Handle obj);

    native static final protected Handle getDestWindow(Handle obj);

    native static final protected int getActions(Handle obj);

    native static final protected int getSuggestedAction(Handle obj);

    native static final protected int getAction(Handle obj);

    native static final protected int gdk_drag_context_get_type();

    native static final protected Handle gdk_drag_context_new();

    native static final private void gtk_drag_finish(Handle context,
            boolean success, boolean delete, int time);
}
