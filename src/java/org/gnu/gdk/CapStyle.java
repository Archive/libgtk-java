/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class CapStyle extends Enum {

    static final private int _NOT_LAST = 0;

    /**
     * The same as BUTT for lines of non-zero width. For zero width lines, the
     * final point on the line will not be drawn.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CapStyle NOT_LAST = new CapStyle(_NOT_LAST);

    static final private int _BUTT = 1;

    /**
     * The ends of the lines are drawn squared off and extending to the
     * coordinates of the end point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CapStyle BUTT = new CapStyle(_BUTT);

    static final private int _ROUND = 2;

    /**
     * The ends of the lines are drawn as semicircles with the diameter equal to
     * the line width and centered at the end point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CapStyle ROUND = new CapStyle(_ROUND);

    static final private int _PROJECTING = 3;

    /**
     * The ends of the lines are drawn squared off and extending half the width
     * of the line beyond the end point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CapStyle PROJECTING = new CapStyle(_PROJECTING);

    static final private CapStyle[] theInterned = new CapStyle[] { NOT_LAST,
            BUTT, ROUND, PROJECTING };

    static private java.util.Hashtable theInternedExtras;

    static final private CapStyle theSacrificialOne = new CapStyle(0);

    static public CapStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        CapStyle already = (CapStyle) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new CapStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CapStyle(int value) {
        value_ = value;
    }

    public CapStyle or(CapStyle other) {
        return intern(value_ | other.value_);
    }

    public CapStyle and(CapStyle other) {
        return intern(value_ & other.value_);
    }

    public CapStyle xor(CapStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(CapStyle other) {
        return (value_ & other.value_) == other.value_;
    }
}
