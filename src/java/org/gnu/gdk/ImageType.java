/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class ImageType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORMAL = 0;

    static final public org.gnu.gdk.ImageType NORMAL = new org.gnu.gdk.ImageType(
            _NORMAL);

    static final private int _SHARED = 1;

    static final public org.gnu.gdk.ImageType SHARED = new org.gnu.gdk.ImageType(
            _SHARED);

    static final private int _FASTEST = 2;

    static final public org.gnu.gdk.ImageType FASTEST = new org.gnu.gdk.ImageType(
            _FASTEST);

    static final private org.gnu.gdk.ImageType[] theInterned = new org.gnu.gdk.ImageType[] {
            NORMAL, SHARED, FASTEST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.ImageType theSacrificialOne = new org.gnu.gdk.ImageType(
            0);

    static public org.gnu.gdk.ImageType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.ImageType already = (org.gnu.gdk.ImageType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.ImageType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ImageType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.ImageType or(org.gnu.gdk.ImageType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.ImageType and(org.gnu.gdk.ImageType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.ImageType xor(org.gnu.gdk.ImageType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.ImageType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
