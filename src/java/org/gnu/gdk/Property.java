/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;

public class Property {
    /*
     * This class should probably be iliminated or fully implemented. The native
     * methods could be attached to gdk.Window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected boolean gdk_property_get(Handle window,
            Handle property, Handle type, long offset, long length, int pdelete,
            int[] actualPropertyType, int[] actualFormat, int[] actualLength,
            byte[] data);

    native static final protected void gdk_property_change(Handle window,
    		Handle property, Handle type, int format, int mode, byte[] data,
            int numElements);

    native static final protected void gdk_property_delete(Handle window,
            Handle property);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
