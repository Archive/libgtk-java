/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

public class Cursor extends Boxed {

    protected Cursor(Handle hndl) {
        super(hndl);
    }

    /**
     * Creates a new cursor from the set of builtin cursors.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Cursor(CursorType ctype) {
        super(gdk_cursor_new(ctype.getValue()));
    }

    /**
     * Create a new cursor from a pixmap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Cursor(Pixmap source, Pixmap mask, Color fg, Color bg, int x, int y) {
        super(gdk_cursor_new_from_pixmap(source.getHandle(), mask.getHandle(),
                fg.getHandle(), bg.getHandle(), x, y));
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Cursor getCursorFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            return (box != null) ? (Cursor) box : new Cursor(hndl);
        }
        return null;
    }

    native static final protected int getType(Handle obj);

    native static final protected Handle gdk_cursor_new(int cursorType);

    native static final protected Handle gdk_cursor_new_from_pixmap(
            Handle source, Handle mask, Handle fg, Handle bg, int x, int y);
}
