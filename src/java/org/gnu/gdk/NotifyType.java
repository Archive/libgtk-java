/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class NotifyType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ANCESTOR = 0;

    static final public org.gnu.gdk.NotifyType ANCESTOR = new org.gnu.gdk.NotifyType(
            _ANCESTOR);

    static final private int _VIRTUAL = 1;

    static final public org.gnu.gdk.NotifyType VIRTUAL = new org.gnu.gdk.NotifyType(
            _VIRTUAL);

    static final private int _INFERIOR = 2;

    static final public org.gnu.gdk.NotifyType INFERIOR = new org.gnu.gdk.NotifyType(
            _INFERIOR);

    static final private int _NONLINEAR = 3;

    static final public org.gnu.gdk.NotifyType NONLINEAR = new org.gnu.gdk.NotifyType(
            _NONLINEAR);

    static final private int _NONLINEAR_VIRTUAL = 4;

    static final public org.gnu.gdk.NotifyType NONLINEAR_VIRTUAL = new org.gnu.gdk.NotifyType(
            _NONLINEAR_VIRTUAL);

    static final private int _UNKNOWN = 5;

    static final public org.gnu.gdk.NotifyType UNKNOWN = new org.gnu.gdk.NotifyType(
            _UNKNOWN);

    static final private org.gnu.gdk.NotifyType[] theInterned = new org.gnu.gdk.NotifyType[] {
            ANCESTOR, VIRTUAL, INFERIOR, NONLINEAR, NONLINEAR_VIRTUAL, UNKNOWN }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.NotifyType theSacrificialOne = new org.gnu.gdk.NotifyType(
            0);

    static public org.gnu.gdk.NotifyType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.NotifyType already = (org.gnu.gdk.NotifyType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.NotifyType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private NotifyType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.NotifyType or(org.gnu.gdk.NotifyType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.NotifyType and(org.gnu.gdk.NotifyType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.NotifyType xor(org.gnu.gdk.NotifyType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.NotifyType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
