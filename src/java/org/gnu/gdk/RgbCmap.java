/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

public class RgbCmap extends MemStruct {

    native static final protected Handle gdk_rgb_cmap_new(int[] colors,
            int numColors);

}
