/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class OverlapType extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _IN = 0;

    static final public org.gnu.gdk.OverlapType IN = new org.gnu.gdk.OverlapType(
            _IN);

    static final private int _OUT = 1;

    static final public org.gnu.gdk.OverlapType OUT = new org.gnu.gdk.OverlapType(
            _OUT);

    static final private int _PART = 2;

    static final public org.gnu.gdk.OverlapType PART = new org.gnu.gdk.OverlapType(
            _PART);

    static final private org.gnu.gdk.OverlapType[] theInterned = new org.gnu.gdk.OverlapType[] {
            IN, OUT, PART }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.OverlapType theSacrificialOne = new org.gnu.gdk.OverlapType(
            0);

    static public org.gnu.gdk.OverlapType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.OverlapType already = (org.gnu.gdk.OverlapType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.OverlapType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private OverlapType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.OverlapType or(org.gnu.gdk.OverlapType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.OverlapType and(org.gnu.gdk.OverlapType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.OverlapType xor(org.gnu.gdk.OverlapType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.OverlapType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
