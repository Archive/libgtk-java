/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class WindowEdge extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORTH_WEST = 0;

    static final public org.gnu.gdk.WindowEdge NORTH_WEST = new org.gnu.gdk.WindowEdge(
            _NORTH_WEST);

    static final private int _NORTH = 1;

    static final public org.gnu.gdk.WindowEdge NORTH = new org.gnu.gdk.WindowEdge(
            _NORTH);

    static final private int _NORTH_EAST = 2;

    static final public org.gnu.gdk.WindowEdge NORTH_EAST = new org.gnu.gdk.WindowEdge(
            _NORTH_EAST);

    static final private int _WEST = 3;

    static final public org.gnu.gdk.WindowEdge WEST = new org.gnu.gdk.WindowEdge(
            _WEST);

    static final private int _EAST = 4;

    static final public org.gnu.gdk.WindowEdge EAST = new org.gnu.gdk.WindowEdge(
            _EAST);

    static final private int _SOUTH_WEST = 5;

    static final public org.gnu.gdk.WindowEdge SOUTH_WEST = new org.gnu.gdk.WindowEdge(
            _SOUTH_WEST);

    static final private int _SOUTH = 6;

    static final public org.gnu.gdk.WindowEdge SOUTH = new org.gnu.gdk.WindowEdge(
            _SOUTH);

    static final private int _SOUTH_EAST = 7;

    static final public org.gnu.gdk.WindowEdge SOUTH_EAST = new org.gnu.gdk.WindowEdge(
            _SOUTH_EAST);

    static final private org.gnu.gdk.WindowEdge[] theInterned = new org.gnu.gdk.WindowEdge[] {
            NORTH_WEST, NORTH, NORTH_EAST, WEST, EAST, SOUTH_WEST, SOUTH,
            SOUTH_EAST }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowEdge theSacrificialOne = new org.gnu.gdk.WindowEdge(
            0);

    static public org.gnu.gdk.WindowEdge intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowEdge already = (org.gnu.gdk.WindowEdge) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowEdge(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowEdge(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowEdge or(org.gnu.gdk.WindowEdge other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowEdge and(org.gnu.gdk.WindowEdge other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowEdge xor(org.gnu.gdk.WindowEdge other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowEdge other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
