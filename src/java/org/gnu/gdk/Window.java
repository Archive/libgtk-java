/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * A rectangular region on the screen. It's a low-level object, used to
 * implement high-level objects such as {@link org.gnu.gtk.Widget} and
 * {@link org.gnu.gtk.Window} on the GTK+ level. A {@link org.gnu.gtk.Window} is
 * a toplevel window, the thing a user might think of as a "window" with a
 * titlebar and so on; a {@link org.gnu.gtk.Window} may contain many Windows.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Window</code>.
 */
public class Window extends Drawable {

    public Window(Handle handle) {
        super(handle);
    }

    public Window(Window parent, WindowAttr attributes, int attributesMask) {
        super(gdk_window_new(parent.getHandle(), attributes.getHandle(),
                attributesMask));
    }

    public void destroy() {
        Window.gdk_window_destroy(getHandle());
    }

    // if the window returned by the native is null,
    // how do we know it?

    /**
     * Obtains the window underneath the mouse pointer. Returs NULL if the
     * window under the mouse pointer is not known to GDK (for example, belongs
     * to another application).
     * 
     * @param x
     *            X position of the window.
     * @param y
     *            Y position of the window.
     * @return The window underneath the mouse pointer.
     * 
     * @deprecated This method is incorrect and is deprecated in favor of
     *             {@link #getWindowAtPointer()}.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getWindowAt(int x, int y) {
        return getWindowFromHandle(Window.gdk_window_at_pointer());
    }

    /**
     * Obtains the window underneath the mouse pointer. Returs NULL if the
     * window under the mouse pointer is not known to GDK (for example, belongs
     * to another application).
     * 
     * @return The window underneath the mouse pointer.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getWindowAtPointer() {
        return getWindowFromHandle(Window.gdk_window_at_pointer());
    }

    /**
     * Raises the <code>window</code> to the top of the window stack (moves
     * the window to the front of the Z-order).
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void show() {
        Window.gdk_window_show(getHandle());
    }

    /**
     * For toplevel windows, withdraws them, so they will no longer be known to
     * the window manager; for all windows, unmaps them, so they won't be
     * displayed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void hide() {
        Window.gdk_window_hide(getHandle());
    }

    /**
     * Withdraws a window (unmaps it and asks the window manager to forget about
     * it).
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void withdraw() {
        Window.gdk_window_withdraw(getHandle());
    }

    /**
     * Shows a <code>GdkWindow</code> onscreen, but does not modify its
     * stacking order. In contrast, <code>show()</code> will raise the window
     * to the top of the window stack.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void showUnraised() {
        Window.gdk_window_show_unraised(getHandle());
    }

    /**
     * Repositions a window relative to its parent window. For toplevel windows,
     * window managers may ignore or modify the move; For child windows, the
     * move will reliably succeed.
     * 
     * @param x
     *            new x position.
     * @param y
     *            new y position.
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void move(int x, int y) {
        Window.gdk_window_move(getHandle(), x, y);
    }

    /**
     * Resizes window; for toplevel windows, asks the window manager to resize
     * the window. The window manager may not allow the resize. Windows may not
     * be resized below 1x1.
     * 
     * @param width
     *            the new window width.
     * @param height
     *            the new window height.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void resize(int width, int height) {
        Window.gdk_window_resize(getHandle(), width, height);
    }

    /**
     * Equivalent to calling <code>move()</code> and <code>resize()</code>,
     * except that both operations are performed at once, avoiding strange
     * visual effects (ie the user may be able to see the window first move,
     * then resize, if you don't use <code>moveAndResize()</code>).
     * 
     * @param x
     *            the new x position.
     * @param y
     *            the new y position.
     * @param width
     *            the new width.
     * @param height
     *            the new height.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void moveAndResize(int x, int y, int width, int height) {
        Window.gdk_window_move_resize(getHandle(), x, y, width, height);
    }

    /**
     * Reparents window into the given <code>parent</code>. The window being
     * reparented will be unmapped as a side effect.
     * 
     * @param parent
     *            the new parent to move window into.
     * @param x
     *            X location inside the new parent.
     * @param y
     *            Y location inside the new parent.
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void reparent(org.gnu.gdk.Window parent, int x, int y) {
        Window.gdk_window_reparent(getHandle(), parent.getHandle(), x, y);
    }

    /**
     * Clears an entire <code>window</code> to the background color or
     * background pixmap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void clear() {
        Window.gdk_window_clear(getHandle());
    }

    // it will generate an event in the future.
    // gdk_window_clear_area will not be used.
    public void clearArea(int x, int y, int width, int height,
            boolean exposeEvent) {
        if (exposeEvent)
            gdk_window_clear_area_e(getHandle(), x, y, width, height);
        else
            gdk_window_clear_area(getHandle(), x, y, width, height);
    }

    public void raise() {
        Window.gdk_window_raise(getHandle());
    }

    public void lower() {
        Window.gdk_window_lower(getHandle());
    }

    public void getKeyboardFocus() {
        gdk_window_focus(getHandle());
    }

    public void setUnmanaged(boolean unmanaged) {
        Window.gdk_window_set_override_redirect(getHandle(), unmanaged);
    }

    public void scrollContent(int x, int y) {
        Window.gdk_window_scroll(getHandle(), x, y);
    }

    // to unset the mask mask should be null;
    // public void unsetBitmapMask(null, ...);
    public void setBitmapMask(org.gnu.gdk.Bitmap mask, int x, int y) {
        Window.gdk_window_shape_combine_mask(getHandle(), mask.getHandle(), x,
                y);
    }

    // the same as above.
    public void setRegionMask(org.gnu.gdk.Region region, int x, int y) {
        Window.gdk_window_shape_combine_region(getHandle(), region.getHandle(),
                x, y);
    }

    public void setChildMask(boolean masked) {
        if (masked)
            Window.gdk_window_set_child_shapes(getHandle());
    }

    public void mergeChildShapes() {
        gdk_window_merge_child_shapes(getHandle());
    }

    public boolean isVisible() {
        return Window.gdk_window_is_visible(getHandle());
    }

    public boolean isViewable() {
        return Window.gdk_window_is_viewable(getHandle());
    }

    public WindowState getWindowState() {
        return WindowState.intern(Window.gdk_window_get_state(getHandle()));
    }

    // According to the api, not very usefull, so leave it unimplemented.
    // public void setStaticGravity(boolean static){
    // }

    // It seems to be not usefull. NativeWindow is not implemented.
    // public static org.gnu.gdk.Window
    // wrapNativeWindow(org.gnu.gdk.NativeWindow nw){}

    public void setHint(WindowTypeHint hint) {
        // if the window is already mapped it has no effect.
        if (hint != null && !isVisible())
            Window.gdk_window_set_type_hint(getHandle(), hint.getValue());
    }

    // ModalWindow will extend this class and implement this method.
    // public void setModal(boolean modal)
    // Window.gdk_window_set_modal_hint(....);
    // }

    // not sure if it is going to be implemented.
    // public void setGeometryHints(){}

    public static Window[] getTopLevelWindows() {
        Handle[] l = Window.gdk_window_get_toplevels();
        Window wlist[] = new Window[l.length];
        for (int i = 0; i < l.length; i++) {
            wlist[i] = getWindowFromHandle(l[i]);
        }
        return wlist;
    }

    /**
     * Retrieve the current width of the Window.
     * 
     * @return The width of the Window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        int[] x = new int[1];
        int[] y = new int[1];
        int[] width = new int[1];
        int[] height = new int[1];
        int[] depth = new int[1];
        Window.gdk_window_get_geometry(getHandle(), x, y, width, height, depth);
        return width[0];
    }

    /**
     * Retrieve the current height of the Window.
     * 
     * @return The height of the Window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeight() {
        int[] x = new int[1];
        int[] y = new int[1];
        int[] width = new int[1];
        int[] height = new int[1];
        int[] depth = new int[1];
        Window.gdk_window_get_geometry(getHandle(), x, y, width, height, depth);
        return height[0];
    }

    /**
     * The event mask for a window determines which events will be reported for
     * that window. For example, an event mask including BUTTON_PRESS_MASK means
     * the window should report button press events. The event mask is the
     * bitwise OR of values from the EventMask enumeration.
     * 
     * @param mask
     *            the new event mask.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setEvents(EventMask mask) {
        gdk_window_set_events(getHandle(), mask.getValue());
    }

    /**
     * Gets the event mask for window.
     * 
     * @return the event mask of the window.
     * @see #setEvents(EventMask)
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventMask getEvents() {
        return EventMask.intern(gdk_window_get_events(getHandle()));
    }

    /**
     * Set the bit gravity of the given window to static, and flag it so all
     * children get static subwindow gravity. This is used if you are
     * implementing scary features that involve deep knowledge of the windowing
     * system. Don't worry about it unless you have to.
     * 
     * @param useStatic
     * @return true if the server supports static gravity.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setStaticGravities(boolean useStatic) {
        return gdk_window_set_static_gravities(getHandle(), useStatic);
    }

    /**
     * The application can use this hint to tell the window manager that a
     * certain window has modal behavior. The window manager can use this
     * information to handle modal windows in certain ways.
     * 
     * @param modal
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setModalHint(boolean modal) {
        gdk_window_set_modal_hint(getHandle(), modal);
    }

    /**
     * Sets the geometry hints for a window.
     * 
     * @param geometry
     * @param hints
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setGeometryHints(Geometry geometry, WindowHints hints) {
        gdk_window_set_geometry_hints(getHandle(), geometry.getHandle(), hints
                .getValue());
    }

    /**
     * Indicates that you are beginning the process of redrawing rec. A backing
     * store (offscreen buffer) large enough to contain rectangle will be
     * created. The backing store will be initialized with the background color
     * or background pixmap for window. Then, all drawing operations performed
     * on window will be diverted to the backing store. When you call
     * endPaint(), the backing store will be copied to window, making it visible
     * onscreen. Only the part of window contained in region will be modified;
     * that is, drawing operations are clipped to rectangle.
     * 
     * @param rec
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void beginPaintRectangle(Rectangle rec) {
        gdk_window_begin_paint_rect(getHandle(), rec.getHandle());
    }

    /**
     * Indicates that you are beginning the process of redrawing region. A
     * backing store (offscreen buffer) large enough to contain region will be
     * created. The backing store will be initialized with the background color
     * or background pixmap for window. Then, all drawing operations performed
     * on window will be diverted to the backing store. When you call
     * endPaint(), the backing store will be copied to window, making it visible
     * onscreen. Only the part of window contained in region will be modified;
     * that is, drawing operations are clipped to region.
     * 
     * @param region
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void beginPaintRegion(Region region) {
        gdk_window_begin_paint_region(getHandle(), region.getHandle());
    }

    /**
     * Indicates that the backing store created by the most recent call to
     * beginPaintRegion() should be copied onscreen and deleted, leaving the
     * next-most-recent backing store or no backing store at all as the active
     * paint region. See beginPaintRegion() for full details. It is an error to
     * call this function without a matching beginPaintRegion() first.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void endPaint() {
        gdk_window_end_paint(getHandle());
    }

    /**
     * Invalidates a rectangular region of the window's content.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void invalidateRect(Rectangle rect, boolean invalidateChildren) {
        gdk_window_invalidate_rect(getHandle(), rect.getHandle(),
                invalidateChildren);
    }

    /**
     * Invalidates a region of the window's content.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void invalidateRegion(Region region, boolean invalidateChildren) {
        gdk_window_invalidate_region(getHandle(), region.getHandle(),
                invalidateChildren);
    }

    /**
     * Sets the title of a toplevel window, to be displayed in the titlebar. If
     * you haven't explicitly set the icon name for the window (using
     * setIconName()), the icon name will be set to title as well. Title may not
     * be null.
     * 
     * @param title
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTitle(String title) {
        gdk_window_set_title(getHandle(), title);
    }

    /**
     * The window manager and session manager use a window's role to distinguish
     * it from other kinds of window in the same application. When an
     * application is restarted after being saved in a previous session, all
     * windows with the same title and role are treated as interchangeable. So
     * if you have two windows with the same title that should be distinguished
     * for session management purposes, you should set the role on those
     * windows. It doesn't matter what string you use for the role, as long as
     * you have a different role for each non-interchangeable kind of window
     * 
     * @param role
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRole(String role) {
        gdk_window_set_role(getHandle(), role);
    }

    /**
     * Indicates to the window manager that the window is a transient dialog
     * associated with the application window parent. This allows the window
     * manager to do things like center window on parent and keep window above
     * parent.
     * 
     * @param parent
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTransientOf(Window parent) {
        gdk_window_set_transient_for(getHandle(), parent.getHandle());
    }

    /**
     * Sets the background color of a window.
     * 
     * @param color
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackground(Color color) {
        gdk_window_set_background(getHandle(), color.getHandle());
    }

    /**
     * Sets the background pixmap of window. May also be used to set a
     * background of "None" on window, by setting a background pixmap of null. A
     * background pixmap will be tiled, positioning the first tile at the origin
     * of window, or if parent_relative is true, the tiling will be done based
     * on the origin of the parent window (useful to align tiles in a parent
     * with tiles in a child).
     * 
     * @param pixmap
     * @param parentRelative
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackground(Pixmap pixmap, boolean parentRelative) {
        gdk_window_set_back_pixmap(getHandle(), pixmap.getHandle(),
                parentRelative);
    }

    /**
     * Sets the mouse pointer for a Window.
     * 
     * @param cursor
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setCursor(Cursor cursor) {
        gdk_window_set_cursor(getHandle(), cursor.getHandle());
    }

    /**
     * Obtains the position of the window as reported in the
     * most-recently-processed EventConfigure. Contrast with getGeometry() which
     * queries the X server for the current window position, regardless of which
     * events have been received or processed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Point getPosition() {
        int[] x = new int[1];
        int[] y = new int[1];
        gdk_window_get_position(getHandle(), x, y);
        return new Point(x[0], y[0]);
    }

    /**
     * Obtains the position of a window in root window coordinates. (Compare
     * with getPosition() and getGeometry() which return the position of a
     * window relative to its parent window.)
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Point getOrigin() {
        int[] x = new int[1];
        int[] y = new int[1];
        gdk_window_get_origin(getHandle(), x, y);
        return new Point(x[0], y[0]);
    }

    /**
     * Obtains the top-left corner of the window manager frame in root window
     * coordinates.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Point getRootOrigin() {
        int[] x = new int[1];
        int[] y = new int[1];
        gdk_window_get_root_origin(getHandle(), x, y);
        return new Point(x[0], y[0]);
    }

    /**
     * Obtains the bounding box of the window, including window manager
     * titlebar/borders if any. The frame position is given in root window
     * coordinates. To get the position of the window itself (rather than the
     * frame) in root window coordinates, use getOrigin().
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle getFrameExtents() {
        Handle handle = gdk_window_get_frame_extents(getHandle());
        return Rectangle.getRectangleFromHandle(handle);
    }

    /**
     * Obtains the parent of window, as known to GDK. Does not query the X
     * server; thus this returns the parent as passed to the constructor, not
     * the actual parent.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getParent() {
        return getWindowFromHandle(gdk_window_get_parent(getHandle()));
    }

    /**
     * Gets the toplevel window that's an ancestor of this window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getToplevel() {
        return getWindowFromHandle(gdk_window_get_toplevel(getHandle()));
    }

    /**
     * Gets the list of children of window known to GDK. This function only
     * returns children created via GDK, so for example it's useless when used
     * with the root window; it only returns windows an application created
     * itself.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window[] getChildren() {
        Handle[] hndls = gdk_window_get_children(getHandle());
        if (null == hndls)
            return null;
        Window[] wins = new Window[hndls.length];
        for (int i = 0; i < hndls.length; i++)
            wins[i] = getWindowFromHandle(hndls[i]);
        return wins;
    }

    /**
     * Sets up the icon representing a Window. The icon is used when the window
     * is minimized (also known as iconified). Some window managers or desktop
     * environments may also place it in the window frame, or display it in
     * other contexts.
     * 
     * <p>
     * This method allows you to pass in the same icon in several hand-drawn
     * sizes. The list should contain the natural sizes your icon is available
     * in; that is, don't scale the image before passing it. Scaling is
     * postponed until the last minute, when the desired final size is known, to
     * allow best quality.
     * 
     * <p>
     * By passing several sizes, you may improve the final image quality of the
     * icon, by reducing or eliminating automatic image scaling.
     * 
     * <p>
     * Recommended sizes to provide: 16x16, 32x32, 48x48 at minimum, and larger
     * images (64x64, 128x128) if you have them.
     * 
     * @param icons
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconList(Pixbuf[] icons) {
        if (null == icons)
            return;
        Handle[] hndls = new Handle[icons.length];
        for (int i = 0; i < icons.length; i++)
            hndls[i] = icons[i].getHandle();
        gdk_window_set_icon_list(getHandle(), hndls);
    }

    /**
     * Sets up the icon representing a Window. This icon is used when the window
     * is minimized (also known as iconified). Some window managers or desktop
     * environments may also place it in the window frame, or display it in
     * other contexts.
     * 
     * <p>
     * The icon should be provided in whatever size it was naturally drawn; that
     * is, don't scale the image before passing it. Scaling is postponed until
     * the last minute, when the desired final size is known, to allow best
     * quality.
     * 
     * <p>
     * If you have your icon hand-drawn in multiple sizes, use setIconList().
     * Then the best size will be used.
     * 
     * @param icon
     * @param pixmap
     * @param mask
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIcon(Window icon, Pixmap pixmap, Bitmap mask) {
        gdk_window_set_icon(getHandle(), icon.getHandle(), pixmap.getHandle(),
                mask.getHandle());
    }

    /**
     * Sets the icon for the window from a named themed icon. See the docs for
     * {@link org.gnu.gtk.IconTheme} for more details.
     * 
     * @param name
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIconName(String name) {
        gdk_window_set_icon_name(getHandle(), name);
    }

    /**
     * Asks to iconify (ie minimize) the specified window. Note that you
     * shouldn't assume the window is definitely iconified afterward, because
     * other entities (e.g. the user or window manager) could deiconify it
     * again, or there may not be a window manager in which case iconification
     * isn't possible, etc. But normally the window will end up iconified. Just
     * don't write code that crashes if not.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void iconify() {
        gdk_window_iconify(getHandle());
    }

    /**
     * Asks to deiconify (ie unminimize) the specified window. Note that you
     * shouldn't assume the window is definitely deiconified afterward, because
     * other entities (e.g. the user or window manager) could iconify it again
     * before your code which assumes deiconification gets to run.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void deiconify() {
        gdk_window_deiconify(getHandle());
    }

    /**
     * Asks to stick window, which means that it will appear on all user
     * desktops. Note that you shouldn't assume the window is definitely stuck
     * afterward, because other entities (e.g. the user or window manager) could
     * unstick it again, and some window managers do not support sticking
     * windows. But normally the window will end up stuck. Just don't write code
     * that crashes if not.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void stick() {
        gdk_window_stick(getHandle());
    }

    /**
     * Asks to unstick window, which means that it will appear on only one of
     * the user's desktops. Note that you shouldn't assume the window is
     * definitely unstuck afterward, because other entities (e.g. the user or
     * window manager) could stick it again. But normally the window will end up
     * unstuck. Just don't write code that crashes if not.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unstick() {
        gdk_window_unstick(getHandle());
    }

    /**
     * Asks to maximize window, so that it becomes full-screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void maximize() {
        gdk_window_maximize(getHandle());
    }

    /**
     * Asks to unmaximize window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unmaximize() {
        gdk_window_unmaximize(getHandle());
    }

    /**
     * Grabs the pointer (usually a mouse) so that all events are passed to this
     * application until the pointer is ungrabbed with ungrabPointer, or the
     * grab window becomes unviewable. This overrides any previous pointer grab
     * by this client.
     * 
     * <p>
     * Pointer grabs are used for operations which need complete control over
     * mouse events, even if the mouse leaves the application. For example in
     * GTK+ it is used for Drag and Drop, for dragging the handle in the HPaned
     * and VPaned widgets, and for resizing columns in CList widgets.
     * 
     * <p>
     * Note that if the event mask of an X window has selected both button press
     * and button release events, then a button press event will cause an
     * automatic pointer grab until the button is released. X does this
     * automatically since most applications expect to receive button press and
     * release events in pairs. It is equivalent to a pointer grab on the window
     * with ownerEvents set to true.
     * 
     * @param ownerEvents
     *            if false then all pointer events are reported with respect to
     *            window and are only reported if selected by event_mask. If
     *            true then pointer events for this application are reported as
     *            normal, but pointer events outside this application are
     *            reported with respect to window and only if selected by
     *            event_mask. In either mode, unreported events are discarded.
     * @param eventMask
     *            specifies the event mask, which is used in accordance with
     *            ownerEvents. Note that only pointer events (i.e. button and
     *            motion events) may be selected.
     * @param confineTo
     *            If non-null, the pointer will be confined to this window
     *            during the grab. If the pointer is outside confineTo, it will
     *            automatically be moved to the closest edge of confineTo and
     *            enter and leave events will be generated as necessary.
     * @param cursor
     *            the cursor to display while the grab is active. If this is
     *            null then the normal cursors are used for window and its
     *            descendants, and the cursor for window is used for all other
     *            windows.
     * @param time
     *            the timestamp of the event which led to this pointer grab.
     *            This usually comes from a EventButton, though CURRENT_TIME can
     *            be used if the time isn't known.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GrabStatus grabPointer(boolean ownerEvents, EventMask eventMask,
            Window confineTo, Cursor cursor, int time) {
        return GrabStatus.intern(gdk_pointer_grab(getHandle(), ownerEvents,
                eventMask.getValue(), confineTo.getHandle(),
                cursor.getHandle(), time));
    }

    /**
     * Ungrabs the pointer, if it is grabbed by this application.
     * 
     * @param time
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void ungrabPointer(int time) {
        gdk_pointer_ungrab(time);
    }

    /**
     * Returns true if the pointer is currently grabbed by this application.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean pointerIsGrabbed() {
        return gdk_pointer_is_grabbed();
    }

    /**
     * Grabs the keyboard so that all events are passed to this application
     * until the keyboard is ungrabbed with ungrabKeyboard. This overrides any
     * previous keyboard grab by this client.
     * 
     * @param ownerEvents
     *            if false then all keyboard events are reported with respect to
     *            window. If true then keyboard events for this application are
     *            reported as normal, but keyboard events outside this
     *            application are reported with respect to window. Both key
     *            press and key release events are always reported, independant
     *            of the event mask set by the application.
     * @param time
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GrabStatus grabKeyboard(boolean ownerEvents, int time) {
        return GrabStatus.intern(gdk_keyboard_grab(getHandle(), ownerEvents,
                time));
    }

    /**
     * Ungrabs the keyboard, if it is grabbed by this application.
     * 
     * @param time
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void ungrabKeyboard(int time) {
        gdk_keyboard_ungrab(time);
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gdk_window_object_get_type());
    }

    /**
     * Only to be used internally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Window getWindowFromHandle(Handle hndl) {
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (Window) obj : new Window(hndl);
        }
        return null;
    }

    public Window getPointerWindow() {
        int[] x = new int[1];
        int[] y = new int[1];
        int[] modType = new int[1];
        Handle hndl = gdk_window_get_pointer(getHandle(), x, y, modType);

        return Window.getWindowFromHandle(hndl);
    }

    public Point getPointerLocation() {
        int[] x = new int[1];
        int[] y = new int[1];
        int[] modType = new int[1];
        gdk_window_get_pointer(getHandle(), x, y, modType);

        return new Point(x[0], y[0]);
    }

    public ModifierType getPointerModifierType() {
        int[] x = new int[1];
        int[] y = new int[1];
        int[] modType = new int[1];
        gdk_window_get_pointer(getHandle(), x, y, modType);

        return ModifierType.intern(modType[0]);
    }

    native static final protected Handle gdk_window_new(Handle parent,
            Handle attriutes, int attributesMask);

    native static final protected int gdk_window_get_window_type(Handle window);

    native static final protected void gdk_window_destroy(Handle window);

    native static final protected Handle gdk_window_at_pointer();

    native static final protected void gdk_window_show(Handle window);

    native static final protected void gdk_window_hide(Handle window);

    native static final protected void gdk_window_withdraw(Handle window);

    native static final protected void gdk_window_show_unraised(Handle window);

    native static final protected void gdk_window_move(Handle window, int x,
            int y);

    native static final protected void gdk_window_resize(Handle window,
            int width, int height);

    native static final protected void gdk_window_move_resize(Handle window,
            int x, int y, int width, int height);

    native static final protected void gdk_window_reparent(Handle window,
            Handle newParent, int x, int y);

    native static final protected void gdk_window_clear(Handle window);

    native static final protected void gdk_window_clear_area(Handle window,
            int x, int y, int width, int height);

    native static final protected void gdk_window_clear_area_e(Handle window,
            int x, int y, int width, int height);

    native static final protected void gdk_window_raise(Handle window);

    native static final protected void gdk_window_lower(Handle window);

    native static final protected void gdk_window_focus(Handle window);

    native static final protected void gdk_window_set_user_data(Handle window,
            Object userData);

    native static final protected void gdk_window_set_override_redirect(
            Handle window, boolean overrideRedirect);

    native static final protected void gdk_window_scroll(Handle window, int dx,
            int dy);

    native static final protected void gdk_window_shape_combine_mask(
            Handle window, Handle shape_mask, int offsetX, int offsetY);

    native static final protected void gdk_window_shape_combine_region(
            Handle window, Handle shape_region, int offsetX, int offsetY);

    native static final protected void gdk_window_set_child_shapes(Handle window);

    native static final protected void gdk_window_merge_child_shapes(
            Handle window);

    native static final protected boolean gdk_window_is_visible(Handle window);

    native static final protected boolean gdk_window_is_viewable(Handle window);

    native static final protected int gdk_window_get_state(Handle window);

    native static final protected boolean gdk_window_set_static_gravities(
            Handle window, boolean useStatic);

    native static final protected Handle gdk_window_foreign_new(int anid);

    native static final protected void gdk_window_set_type_hint(Handle window,
            int hint);

    native static final protected void gdk_window_set_modal_hint(Handle window,
            boolean modal);

    native static final protected void gdk_window_set_geometry_hints(
            Handle window, Handle geometry, int flags);

    native static final protected void gdk_window_begin_paint_rect(
            Handle window, Handle rectangle);

    native static final protected void gdk_window_begin_paint_region(
            Handle window, Handle region);

    native static final protected void gdk_window_end_paint(Handle window);

    native static final protected void gdk_window_set_title(Handle window,
            String title);

    native static final protected void gdk_window_set_role(Handle window,
            String role);

    native static final protected void gdk_window_set_transient_for(
            Handle window, Handle leader);

    native static final protected void gdk_window_set_background(Handle window,
            Handle color);

    native static final protected void gdk_window_set_back_pixmap(
            Handle window, Handle pixmap, boolean parentRelative);

    native static final protected void gdk_window_set_cursor(Handle window,
            Handle cursor);

    native static final protected void gdk_window_get_geometry(Handle window,
            int[] x, int[] y, int[] width, int[] height, int[] depth);

    native static final protected void gdk_window_get_position(Handle window,
            int[] x, int[] y);

    native static final protected int gdk_window_get_origin(Handle window,
            int[] x, int[] y);

    native static final protected void gdk_window_get_root_origin(
            Handle window, int[] x, int[] y);

    native static final protected Handle gdk_window_get_frame_extents(
            Handle window);

    native static final protected Handle gdk_window_get_parent(Handle window);

    native static final protected Handle gdk_window_get_toplevel(Handle window);

    native static final protected Handle[] gdk_window_get_children(Handle window);

    native static final protected Handle[] gdk_window_peek_children(
            Handle window);

    native static final protected int gdk_window_get_events(Handle window);

    native static final protected void gdk_window_set_events(Handle window,
            int eventMask);

    native static final protected void gdk_window_set_icon_list(Handle window,
            Handle[] pixbufs);

    native static final protected void gdk_window_set_icon(Handle window,
            Handle iconWindow, Handle pixmap, Handle mask);

    native static final protected void gdk_window_set_icon_name(Handle window,
            String name);

    native static final protected void gdk_window_set_group(Handle window,
            Handle leader);

    native static final protected void gdk_window_set_decorations(
            Handle window, int decorations);

    native static final protected void gdk_window_set_functions(Handle window,
            int functions);

    native static final protected Handle[] gdk_window_get_toplevels();

    native static final protected void gdk_window_iconify(Handle window);

    native static final protected void gdk_window_deiconify(Handle window);

    native static final protected void gdk_window_stick(Handle window);

    native static final protected void gdk_window_unstick(Handle window);

    native static final protected void gdk_window_maximize(Handle window);

    native static final protected void gdk_window_unmaximize(Handle window);

    native static final protected void gdk_window_register_dnd(Handle window);

    native static final protected void gdk_window_begin_resize_drag(
            Handle window, int edge, int button, int rootX, int rootY,
            int timestamp);

    native static final protected void gdk_window_begin_move_drag(
            Handle window, int button, int rootX, int rootY, int timestamp);

    native static final protected void gdk_window_invalidate_rect(
            Handle window, Handle rect, boolean invalidateChildren);

    native static final protected void gdk_window_invalidate_region(
            Handle window, Handle region, boolean invalidateChildren);

    native static final protected Handle gdk_window_get_update_area(Handle window);
    
    native static final protected void gdk_window_freeze_updates(Handle window);

    native static final protected void gdk_window_thaw_updates(Handle window);

    native static final protected void gdk_window_process_all_updates();

    native static final protected void gdk_window_process_updates(
            Handle window, boolean updateChildren);

    native static final protected void gdk_window_set_debug_updates(
            boolean setting);

    native static final protected void gdk_window_constrain_size(Handle geometry,
            int flags, int width, int height, int[] newWidth, int[] newHeight);

    native static final protected void gdk_window_get_internal_paint_info(
            Handle window, Handle realDrawable, int[] xOffset, int[] yOffset);

    native static final private int gdk_window_object_get_type();

    native static final private int gdk_pointer_grab(Handle window,
            boolean owner_events, int event_mask, Handle confine_to,
            Handle cursor, int time_);

    native static final private int gdk_keyboard_grab(Handle window,
            boolean owner_events, int time_);

    native static final private void gdk_pointer_ungrab(int time_);

    native static final private void gdk_keyboard_ungrab(int time_);

    native static final private boolean gdk_pointer_is_grabbed();

    native static final private Handle gdk_window_get_pointer(Handle window,
            int[] x, int[] y, int[] modType);

}
