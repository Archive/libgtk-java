/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class ModifierType extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _SHIFT_MASK = 1 << 0;

    static final public org.gnu.gdk.ModifierType SHIFT_MASK = new org.gnu.gdk.ModifierType(
            _SHIFT_MASK);

    static final private int _LOCK_MASK = 1 << 1;

    static final public org.gnu.gdk.ModifierType LOCK_MASK = new org.gnu.gdk.ModifierType(
            _LOCK_MASK);

    static final private int _CONTROL_MASK = 1 << 2;

    static final public org.gnu.gdk.ModifierType CONTROL_MASK = new org.gnu.gdk.ModifierType(
            _CONTROL_MASK);

    static final private int _MOD1_MASK = 1 << 3;

    static final public org.gnu.gdk.ModifierType MOD1_MASK = new org.gnu.gdk.ModifierType(
            _MOD1_MASK);

    static final private int _MOD2_MASK = 1 << 4;

    static final public org.gnu.gdk.ModifierType MOD2_MASK = new org.gnu.gdk.ModifierType(
            _MOD2_MASK);

    static final private int _MOD3_MASK = 1 << 5;

    static final public org.gnu.gdk.ModifierType MOD3_MASK = new org.gnu.gdk.ModifierType(
            _MOD3_MASK);

    static final private int _MOD4_MASK = 1 << 6;

    static final public org.gnu.gdk.ModifierType MOD4_MASK = new org.gnu.gdk.ModifierType(
            _MOD4_MASK);

    static final private int _MOD5_MASK = 1 << 7;

    static final public org.gnu.gdk.ModifierType MOD5_MASK = new org.gnu.gdk.ModifierType(
            _MOD5_MASK);

    static final private int _BUTTON1_MASK = 1 << 8;

    static final public org.gnu.gdk.ModifierType BUTTON1_MASK = new org.gnu.gdk.ModifierType(
            _BUTTON1_MASK);

    static final private int _BUTTON2_MASK = 1 << 9;

    static final public org.gnu.gdk.ModifierType BUTTON2_MASK = new org.gnu.gdk.ModifierType(
            _BUTTON2_MASK);

    static final private int _BUTTON3_MASK = 1 << 10;

    static final public org.gnu.gdk.ModifierType BUTTON3_MASK = new org.gnu.gdk.ModifierType(
            _BUTTON3_MASK);

    static final private int _BUTTON4_MASK = 1 << 11;

    static final public org.gnu.gdk.ModifierType BUTTON4_MASK = new org.gnu.gdk.ModifierType(
            _BUTTON4_MASK);

    static final private int _BUTTON5_MASK = 1 << 12;

    static final public org.gnu.gdk.ModifierType BUTTON5_MASK = new org.gnu.gdk.ModifierType(
            _BUTTON5_MASK);

    static final private int _RELEASE_MASK = 1 << 30;

    static final public org.gnu.gdk.ModifierType RELEASE_MASK = new org.gnu.gdk.ModifierType(
            _RELEASE_MASK);

    static final private org.gnu.gdk.ModifierType[] theInterned = new org.gnu.gdk.ModifierType[] {
            new org.gnu.gdk.ModifierType(0), SHIFT_MASK, LOCK_MASK,
            new org.gnu.gdk.ModifierType(3), CONTROL_MASK,
            new org.gnu.gdk.ModifierType(5), new org.gnu.gdk.ModifierType(6),
            new org.gnu.gdk.ModifierType(7), MOD1_MASK,
            new org.gnu.gdk.ModifierType(9), new org.gnu.gdk.ModifierType(10),
            new org.gnu.gdk.ModifierType(11), new org.gnu.gdk.ModifierType(12),
            new org.gnu.gdk.ModifierType(13), new org.gnu.gdk.ModifierType(14),
            new org.gnu.gdk.ModifierType(15), MOD2_MASK,
            new org.gnu.gdk.ModifierType(17), new org.gnu.gdk.ModifierType(18),
            new org.gnu.gdk.ModifierType(19), new org.gnu.gdk.ModifierType(20),
            new org.gnu.gdk.ModifierType(21), new org.gnu.gdk.ModifierType(22),
            new org.gnu.gdk.ModifierType(23), new org.gnu.gdk.ModifierType(24),
            new org.gnu.gdk.ModifierType(25), new org.gnu.gdk.ModifierType(26),
            new org.gnu.gdk.ModifierType(27), new org.gnu.gdk.ModifierType(28),
            new org.gnu.gdk.ModifierType(29), new org.gnu.gdk.ModifierType(30),
            new org.gnu.gdk.ModifierType(31), MOD3_MASK,
            new org.gnu.gdk.ModifierType(33), new org.gnu.gdk.ModifierType(34),
            new org.gnu.gdk.ModifierType(35), new org.gnu.gdk.ModifierType(36),
            new org.gnu.gdk.ModifierType(37), new org.gnu.gdk.ModifierType(38),
            new org.gnu.gdk.ModifierType(39), new org.gnu.gdk.ModifierType(40),
            new org.gnu.gdk.ModifierType(41), new org.gnu.gdk.ModifierType(42),
            new org.gnu.gdk.ModifierType(43), new org.gnu.gdk.ModifierType(44),
            new org.gnu.gdk.ModifierType(45), new org.gnu.gdk.ModifierType(46),
            new org.gnu.gdk.ModifierType(47), new org.gnu.gdk.ModifierType(48),
            new org.gnu.gdk.ModifierType(49), new org.gnu.gdk.ModifierType(50),
            new org.gnu.gdk.ModifierType(51), new org.gnu.gdk.ModifierType(52),
            new org.gnu.gdk.ModifierType(53), new org.gnu.gdk.ModifierType(54),
            new org.gnu.gdk.ModifierType(55), new org.gnu.gdk.ModifierType(56),
            new org.gnu.gdk.ModifierType(57), new org.gnu.gdk.ModifierType(58),
            new org.gnu.gdk.ModifierType(59), new org.gnu.gdk.ModifierType(60),
            new org.gnu.gdk.ModifierType(61), new org.gnu.gdk.ModifierType(62),
            new org.gnu.gdk.ModifierType(63), MOD4_MASK,
            new org.gnu.gdk.ModifierType(65), new org.gnu.gdk.ModifierType(66),
            new org.gnu.gdk.ModifierType(67), new org.gnu.gdk.ModifierType(68),
            new org.gnu.gdk.ModifierType(69), new org.gnu.gdk.ModifierType(70),
            new org.gnu.gdk.ModifierType(71), new org.gnu.gdk.ModifierType(72),
            new org.gnu.gdk.ModifierType(73), new org.gnu.gdk.ModifierType(74),
            new org.gnu.gdk.ModifierType(75), new org.gnu.gdk.ModifierType(76),
            new org.gnu.gdk.ModifierType(77), new org.gnu.gdk.ModifierType(78),
            new org.gnu.gdk.ModifierType(79), new org.gnu.gdk.ModifierType(80),
            new org.gnu.gdk.ModifierType(81), new org.gnu.gdk.ModifierType(82),
            new org.gnu.gdk.ModifierType(83), new org.gnu.gdk.ModifierType(84),
            new org.gnu.gdk.ModifierType(85), new org.gnu.gdk.ModifierType(86),
            new org.gnu.gdk.ModifierType(87), new org.gnu.gdk.ModifierType(88),
            new org.gnu.gdk.ModifierType(89), new org.gnu.gdk.ModifierType(90),
            new org.gnu.gdk.ModifierType(91), new org.gnu.gdk.ModifierType(92),
            new org.gnu.gdk.ModifierType(93), new org.gnu.gdk.ModifierType(94),
            new org.gnu.gdk.ModifierType(95), new org.gnu.gdk.ModifierType(96),
            new org.gnu.gdk.ModifierType(97), new org.gnu.gdk.ModifierType(98),
            new org.gnu.gdk.ModifierType(99),
            new org.gnu.gdk.ModifierType(100),
            new org.gnu.gdk.ModifierType(101),
            new org.gnu.gdk.ModifierType(102),
            new org.gnu.gdk.ModifierType(103),
            new org.gnu.gdk.ModifierType(104),
            new org.gnu.gdk.ModifierType(105),
            new org.gnu.gdk.ModifierType(106),
            new org.gnu.gdk.ModifierType(107),
            new org.gnu.gdk.ModifierType(108),
            new org.gnu.gdk.ModifierType(109),
            new org.gnu.gdk.ModifierType(110),
            new org.gnu.gdk.ModifierType(111),
            new org.gnu.gdk.ModifierType(112),
            new org.gnu.gdk.ModifierType(113),
            new org.gnu.gdk.ModifierType(114),
            new org.gnu.gdk.ModifierType(115),
            new org.gnu.gdk.ModifierType(116),
            new org.gnu.gdk.ModifierType(117),
            new org.gnu.gdk.ModifierType(118),
            new org.gnu.gdk.ModifierType(119),
            new org.gnu.gdk.ModifierType(120),
            new org.gnu.gdk.ModifierType(121),
            new org.gnu.gdk.ModifierType(122),
            new org.gnu.gdk.ModifierType(123),
            new org.gnu.gdk.ModifierType(124),
            new org.gnu.gdk.ModifierType(125),
            new org.gnu.gdk.ModifierType(126),
            new org.gnu.gdk.ModifierType(127), MOD5_MASK,
            new org.gnu.gdk.ModifierType(129),
            new org.gnu.gdk.ModifierType(130),
            new org.gnu.gdk.ModifierType(131),
            new org.gnu.gdk.ModifierType(132),
            new org.gnu.gdk.ModifierType(133),
            new org.gnu.gdk.ModifierType(134),
            new org.gnu.gdk.ModifierType(135),
            new org.gnu.gdk.ModifierType(136),
            new org.gnu.gdk.ModifierType(137),
            new org.gnu.gdk.ModifierType(138),
            new org.gnu.gdk.ModifierType(139),
            new org.gnu.gdk.ModifierType(140),
            new org.gnu.gdk.ModifierType(141),
            new org.gnu.gdk.ModifierType(142),
            new org.gnu.gdk.ModifierType(143),
            new org.gnu.gdk.ModifierType(144),
            new org.gnu.gdk.ModifierType(145),
            new org.gnu.gdk.ModifierType(146),
            new org.gnu.gdk.ModifierType(147),
            new org.gnu.gdk.ModifierType(148),
            new org.gnu.gdk.ModifierType(149),
            new org.gnu.gdk.ModifierType(150),
            new org.gnu.gdk.ModifierType(151),
            new org.gnu.gdk.ModifierType(152),
            new org.gnu.gdk.ModifierType(153),
            new org.gnu.gdk.ModifierType(154),
            new org.gnu.gdk.ModifierType(155),
            new org.gnu.gdk.ModifierType(156),
            new org.gnu.gdk.ModifierType(157),
            new org.gnu.gdk.ModifierType(158),
            new org.gnu.gdk.ModifierType(159),
            new org.gnu.gdk.ModifierType(160),
            new org.gnu.gdk.ModifierType(161),
            new org.gnu.gdk.ModifierType(162),
            new org.gnu.gdk.ModifierType(163),
            new org.gnu.gdk.ModifierType(164),
            new org.gnu.gdk.ModifierType(165),
            new org.gnu.gdk.ModifierType(166),
            new org.gnu.gdk.ModifierType(167),
            new org.gnu.gdk.ModifierType(168),
            new org.gnu.gdk.ModifierType(169),
            new org.gnu.gdk.ModifierType(170),
            new org.gnu.gdk.ModifierType(171),
            new org.gnu.gdk.ModifierType(172),
            new org.gnu.gdk.ModifierType(173),
            new org.gnu.gdk.ModifierType(174),
            new org.gnu.gdk.ModifierType(175),
            new org.gnu.gdk.ModifierType(176),
            new org.gnu.gdk.ModifierType(177),
            new org.gnu.gdk.ModifierType(178),
            new org.gnu.gdk.ModifierType(179),
            new org.gnu.gdk.ModifierType(180),
            new org.gnu.gdk.ModifierType(181),
            new org.gnu.gdk.ModifierType(182),
            new org.gnu.gdk.ModifierType(183),
            new org.gnu.gdk.ModifierType(184),
            new org.gnu.gdk.ModifierType(185),
            new org.gnu.gdk.ModifierType(186),
            new org.gnu.gdk.ModifierType(187),
            new org.gnu.gdk.ModifierType(188),
            new org.gnu.gdk.ModifierType(189),
            new org.gnu.gdk.ModifierType(190),
            new org.gnu.gdk.ModifierType(191),
            new org.gnu.gdk.ModifierType(192),
            new org.gnu.gdk.ModifierType(193),
            new org.gnu.gdk.ModifierType(194),
            new org.gnu.gdk.ModifierType(195),
            new org.gnu.gdk.ModifierType(196),
            new org.gnu.gdk.ModifierType(197),
            new org.gnu.gdk.ModifierType(198),
            new org.gnu.gdk.ModifierType(199),
            new org.gnu.gdk.ModifierType(200),
            new org.gnu.gdk.ModifierType(201),
            new org.gnu.gdk.ModifierType(202),
            new org.gnu.gdk.ModifierType(203),
            new org.gnu.gdk.ModifierType(204),
            new org.gnu.gdk.ModifierType(205),
            new org.gnu.gdk.ModifierType(206),
            new org.gnu.gdk.ModifierType(207),
            new org.gnu.gdk.ModifierType(208),
            new org.gnu.gdk.ModifierType(209),
            new org.gnu.gdk.ModifierType(210),
            new org.gnu.gdk.ModifierType(211),
            new org.gnu.gdk.ModifierType(212),
            new org.gnu.gdk.ModifierType(213),
            new org.gnu.gdk.ModifierType(214),
            new org.gnu.gdk.ModifierType(215),
            new org.gnu.gdk.ModifierType(216),
            new org.gnu.gdk.ModifierType(217),
            new org.gnu.gdk.ModifierType(218),
            new org.gnu.gdk.ModifierType(219),
            new org.gnu.gdk.ModifierType(220),
            new org.gnu.gdk.ModifierType(221),
            new org.gnu.gdk.ModifierType(222),
            new org.gnu.gdk.ModifierType(223),
            new org.gnu.gdk.ModifierType(224),
            new org.gnu.gdk.ModifierType(225),
            new org.gnu.gdk.ModifierType(226),
            new org.gnu.gdk.ModifierType(227),
            new org.gnu.gdk.ModifierType(228),
            new org.gnu.gdk.ModifierType(229),
            new org.gnu.gdk.ModifierType(230),
            new org.gnu.gdk.ModifierType(231),
            new org.gnu.gdk.ModifierType(232),
            new org.gnu.gdk.ModifierType(233),
            new org.gnu.gdk.ModifierType(234),
            new org.gnu.gdk.ModifierType(235),
            new org.gnu.gdk.ModifierType(236),
            new org.gnu.gdk.ModifierType(237),
            new org.gnu.gdk.ModifierType(238),
            new org.gnu.gdk.ModifierType(239),
            new org.gnu.gdk.ModifierType(240),
            new org.gnu.gdk.ModifierType(241),
            new org.gnu.gdk.ModifierType(242),
            new org.gnu.gdk.ModifierType(243),
            new org.gnu.gdk.ModifierType(244),
            new org.gnu.gdk.ModifierType(245),
            new org.gnu.gdk.ModifierType(246),
            new org.gnu.gdk.ModifierType(247),
            new org.gnu.gdk.ModifierType(248),
            new org.gnu.gdk.ModifierType(249),
            new org.gnu.gdk.ModifierType(250),
            new org.gnu.gdk.ModifierType(251),
            new org.gnu.gdk.ModifierType(252),
            new org.gnu.gdk.ModifierType(253),
            new org.gnu.gdk.ModifierType(254),
            new org.gnu.gdk.ModifierType(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.ModifierType theSacrificialOne = new org.gnu.gdk.ModifierType(
            0);

    static public org.gnu.gdk.ModifierType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.ModifierType already = (org.gnu.gdk.ModifierType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.ModifierType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ModifierType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.ModifierType or(org.gnu.gdk.ModifierType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.ModifierType and(org.gnu.gdk.ModifierType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.ModifierType xor(org.gnu.gdk.ModifierType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.ModifierType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
