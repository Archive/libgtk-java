/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.pango.Attribute;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PangoAttrStipple</code>.
 */
public class PangoAttrStipple extends Attribute {
    public PangoAttrStipple(Bitmap stipple) {
        super(gdk_pango_attr_stipple_new(stipple.getHandle()));
    }

    public Bitmap getStipple() {
        Handle hndl = getStipple(getHandle());
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (Bitmap) obj : new Bitmap(hndl);
        }
        return null;
    }

    native static final protected Handle getStipple(Handle obj);

    native static final protected Handle gdk_pango_attr_stipple_new(
            Handle stipple);
}
