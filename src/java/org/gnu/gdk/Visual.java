/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/*
 * TODO:
 * Some methods might not be used, decide it.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * Describes a particular video hardware display format. It includes information
 * about the number of bits used for each color, the way the bits are translated
 * into an RGB value for display, and the way the bits are stored in memory.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Visual</code>.
 */
public class Visual extends GObject {

    protected Visual(Handle handle) {
        super(handle);
    }

    /**
     * Get the best available depth for the default GDK display. "Best" means
     * "largest," i.e. 32 preferred over 24 preferred over 8 bits per pixel.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static int getBestDepth() {
        return Visual.gdk_visual_get_best_depth();
    }

    /**
     * Return the best available visual type (the one with the most colors) for
     * the default GDK display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static VisualType getBestVisualType() {
        return VisualType.getVisualType(Visual.gdk_visual_get_best_type());
    }

    /**
     * Get the default or system visual for the default GDK display. This is the
     * visual for the root window of the display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Visual getSystemVisual() {
        return getVisualFromHandle(Visual.gdk_visual_get_system());
    }

    /**
     * Get the visual with the most available colors for the default GDK
     * display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Visual getVisual() {
        return getVisualFromHandle(Visual.gdk_visual_get_best());
    }

    /**
     * Get the best visual with depth depth for the default GDK display. Color
     * visuals and visuals with mutable colormaps are preferred over grayscale
     * or fixed-colormap visuals. NULL may be returned if no visual supports
     * depth.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Visual getVisual(int depth) {
        return getVisualFromHandle(Visual.gdk_visual_get_best_with_depth(depth));
    }

    /**
     * Get the best visual of the given visual_type for the default GDK display.
     * Visuals with higher color depths are considered better. NULL may be
     * returned if no visual has type visual_type.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Visual getVisual(VisualType vt) {
        return getVisualFromHandle(Visual.gdk_visual_get_best_with_type(vt
                .getValue()));
    }

    /**
     * Combines getVisual(int depth) and getVisual(VisualType vt).
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Visual getVisual(int depth, VisualType vt) {
        return getVisualFromHandle(Visual.gdk_visual_get_best_with_both(depth,
                vt.getValue()));
    }

    public int getDepth() {
        return getDepth(getHandle());
    }

    public ByteOrder getByteOrder() {
        return ByteOrder.intern(getByteOrder(getHandle()));
    }

    public int getColormapSize() {
        return getColormapSize(getHandle());
    }

    public int getBitsPerRGB() {
        return getBitsPerRgb(getHandle());
    }

    public int getRedMask() {
        return getRedMask(getHandle());
    }

    public int getRedShift() {
        return getRedShift(getHandle());
    }

    public int getRedPrec() {
        return getRedPrec(getHandle());
    }

    public int getGreenMask() {
        return getGreenMask(getHandle());
    }

    public int getGreenShift() {
        return getGreenShift(getHandle());
    }

    public int getGreenPrec() {
        return getGreenPrec(getHandle());
    }

    public int getBlueMask() {
        return getBlueMask(getHandle());
    }

    public int getBlueShift() {
        return getBlueShift(getHandle());
    }

    public int getBluePrec() {
        return getBluePrec(getHandle());
    }

    public Screen getScreen() {
        return Screen.getScreenFromHandle(gdk_visual_get_screen(getHandle()));
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Visual getVisualFromHandle(Handle hndl) {
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (Visual) obj : new Visual(hndl);
        }
        return null;
    }

    native static final protected int getDepth(Handle obj);

    native static final protected int getByteOrder(Handle obj);

    native static final protected int getColormapSize(Handle obj);

    native static final protected int getBitsPerRgb(Handle obj);

    native static final protected int getRedMask(Handle obj);

    native static final protected int getRedShift(Handle obj);

    native static final protected int getRedPrec(Handle obj);

    native static final protected int getGreenMask(Handle obj);

    native static final protected int getGreenShift(Handle obj);

    native static final protected int getGreenPrec(Handle obj);

    native static final protected int getBlueMask(Handle obj);

    native static final protected int getBlueShift(Handle obj);

    native static final protected int getBluePrec(Handle obj);

    native static final protected int gdk_visual_get_type();

    native static final protected int gdk_visual_get_best_depth();

    native static final protected int gdk_visual_get_best_type();

    native static final protected Handle gdk_visual_get_system();

    native static final protected Handle gdk_visual_get_best();

    native static final protected Handle gdk_visual_get_best_with_depth(
            int depth);

    native static final protected Handle gdk_visual_get_best_with_type(
            int visualType);

    native static final protected Handle gdk_visual_get_best_with_both(
            int depth, int visualType);

    native static final protected Handle gdk_visual_get_screen(Handle visual);

}
