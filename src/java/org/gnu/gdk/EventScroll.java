/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.EventScroll</code>.
 */
public class EventScroll extends Event {
    /**
     * Promotion constructor. Construct the specific event object by promoting
     * (sharing the <tt>Handle</tt> object of) the base <tt>Event</tt>.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventScroll(Event evt) {
        // We share the handle with both instances. This avoids
        // copying the event (via the constructor ( super( evt ) ).
        // It is more of a "promotion" constructor.
        super(evt.getHandle());
    }

    public EventScroll(Handle handle) {
        super(handle);
    }

    public ScrollDirection getScrollDirection() {
        return ScrollDirection.intern(getDirection(getHandle()));
    }

    public Window getWindow() {
        return Window.getWindowFromHandle(EventScroll.getWindow(getHandle()));
    }

    native static final protected Handle getWindow(Handle obj);

    native static final protected boolean getSendEvent(Handle obj);

    native static final protected int getTime(Handle obj);

    native static final protected double getX(Handle obj);

    native static final protected double getY(Handle obj);

    native static final protected int getState(Handle obj);

    native static final protected int getDirection(Handle obj);

    native static final protected Handle getDevice(Handle obj);

    native static final protected double getXRoot(Handle obj);

    native static final protected double getYRoot(Handle obj);
}
