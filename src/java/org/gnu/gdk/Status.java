/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class Status extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _OK = 0;

    static final public org.gnu.gdk.Status OK = new org.gnu.gdk.Status(_OK);

    static final private int _ERROR = -1;

    static final public org.gnu.gdk.Status ERROR = new org.gnu.gdk.Status(
            _ERROR);

    static final private int _ERROR_PARAM = -2;

    static final public org.gnu.gdk.Status ERROR_PARAM = new org.gnu.gdk.Status(
            _ERROR_PARAM);

    static final private int _ERROR_FILE = -3;

    static final public org.gnu.gdk.Status ERROR_FILE = new org.gnu.gdk.Status(
            _ERROR_FILE);

    static final private int _ERROR_MEM = -4;

    static final public org.gnu.gdk.Status ERROR_MEM = new org.gnu.gdk.Status(
            _ERROR_MEM);

    static final private org.gnu.gdk.Status[] theInterned = new org.gnu.gdk.Status[] { OK }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.Status theSacrificialOne = new org.gnu.gdk.Status(
            0);

    static public org.gnu.gdk.Status intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.Status already = (org.gnu.gdk.Status) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.Status(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Status(int value) {
        value_ = value;
    }

    public org.gnu.gdk.Status or(org.gnu.gdk.Status other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.Status and(org.gnu.gdk.Status other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.Status xor(org.gnu.gdk.Status other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.Status other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
