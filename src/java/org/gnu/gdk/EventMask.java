/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

/**
 * A set of bit flags to indicate which events a window is to receive.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.EventMask</code>.
 */
public class EventMask extends Flags {

    static final private int _EXPOSURE_MASK = 1 << 1;

    static final public org.gnu.gdk.EventMask EXPOSURE_MASK = new org.gnu.gdk.EventMask(
            _EXPOSURE_MASK);

    static final private int _POINTER_MOTION_MASK = 1 << 2;

    static final public org.gnu.gdk.EventMask POINTER_MOTION_MASK = new org.gnu.gdk.EventMask(
            _POINTER_MOTION_MASK);

    static final private int _POINTER_MOTION_HINT_MASK = 1 << 3;

    static final public org.gnu.gdk.EventMask POINTER_MOTION_HINT_MASK = new org.gnu.gdk.EventMask(
            _POINTER_MOTION_HINT_MASK);

    static final private int _BUTTON_MOTION_MASK = 1 << 4;

    static final public org.gnu.gdk.EventMask BUTTON_MOTION_MASK = new org.gnu.gdk.EventMask(
            _BUTTON_MOTION_MASK);

    static final private int _BUTTON1_MOTION_MASK = 1 << 5;

    static final public org.gnu.gdk.EventMask BUTTON1_MOTION_MASK = new org.gnu.gdk.EventMask(
            _BUTTON1_MOTION_MASK);

    static final private int _BUTTON2_MOTION_MASK = 1 << 6;

    static final public org.gnu.gdk.EventMask BUTTON2_MOTION_MASK = new org.gnu.gdk.EventMask(
            _BUTTON2_MOTION_MASK);

    static final private int _BUTTON3_MOTION_MASK = 1 << 7;

    static final public org.gnu.gdk.EventMask BUTTON3_MOTION_MASK = new org.gnu.gdk.EventMask(
            _BUTTON3_MOTION_MASK);

    static final private int _BUTTON_PRESS_MASK = 1 << 8;

    static final public org.gnu.gdk.EventMask BUTTON_PRESS_MASK = new org.gnu.gdk.EventMask(
            _BUTTON_PRESS_MASK);

    static final private int _BUTTON_RELEASE_MASK = 1 << 9;

    static final public org.gnu.gdk.EventMask BUTTON_RELEASE_MASK = new org.gnu.gdk.EventMask(
            _BUTTON_RELEASE_MASK);

    static final private int _KEY_PRESS_MASK = 1 << 10;

    static final public org.gnu.gdk.EventMask KEY_PRESS_MASK = new org.gnu.gdk.EventMask(
            _KEY_PRESS_MASK);

    static final private int _KEY_RELEASE_MASK = 1 << 11;

    static final public org.gnu.gdk.EventMask KEY_RELEASE_MASK = new org.gnu.gdk.EventMask(
            _KEY_RELEASE_MASK);

    static final private int _ENTER_NOTIFY_MASK = 1 << 12;

    static final public org.gnu.gdk.EventMask ENTER_NOTIFY_MASK = new org.gnu.gdk.EventMask(
            _ENTER_NOTIFY_MASK);

    static final private int _LEAVE_NOTIFY_MASK = 1 << 13;

    static final public org.gnu.gdk.EventMask LEAVE_NOTIFY_MASK = new org.gnu.gdk.EventMask(
            _LEAVE_NOTIFY_MASK);

    static final private int _FOCUS_CHANGE_MASK = 1 << 14;

    static final public org.gnu.gdk.EventMask FOCUS_CHANGE_MASK = new org.gnu.gdk.EventMask(
            _FOCUS_CHANGE_MASK);

    static final private int _STRUCTURE_MASK = 1 << 15;

    static final public org.gnu.gdk.EventMask STRUCTURE_MASK = new org.gnu.gdk.EventMask(
            _STRUCTURE_MASK);

    static final private int _PROPERTY_CHANGE_MASK = 1 << 16;

    static final public org.gnu.gdk.EventMask PROPERTY_CHANGE_MASK = new org.gnu.gdk.EventMask(
            _PROPERTY_CHANGE_MASK);

    static final private int _VISIBILITY_NOTIFY_MASK = 1 << 17;

    static final public org.gnu.gdk.EventMask VISIBILITY_NOTIFY_MASK = new org.gnu.gdk.EventMask(
            _VISIBILITY_NOTIFY_MASK);

    static final private int _PROXIMITY_IN_MASK = 1 << 18;

    static final public org.gnu.gdk.EventMask PROXIMITY_IN_MASK = new org.gnu.gdk.EventMask(
            _PROXIMITY_IN_MASK);

    static final private int _PROXIMITY_OUT_MASK = 1 << 19;

    static final public org.gnu.gdk.EventMask PROXIMITY_OUT_MASK = new org.gnu.gdk.EventMask(
            _PROXIMITY_OUT_MASK);

    static final private int _SUBSTRUCTURE_MASK = 1 << 20;

    static final public org.gnu.gdk.EventMask SUBSTRUCTURE_MASK = new org.gnu.gdk.EventMask(
            _SUBSTRUCTURE_MASK);

    static final private int _SCROLL_MASK = 1 << 21;

    static final public org.gnu.gdk.EventMask SCROLL_MASK = new org.gnu.gdk.EventMask(
            _SCROLL_MASK);

    static final private int _ALL_EVENTS_MASK = 0x3FFFFE;

    static final public org.gnu.gdk.EventMask ALL_EVENTS_MASK = new org.gnu.gdk.EventMask(
            _ALL_EVENTS_MASK);

    static final private org.gnu.gdk.EventMask[] theInterned = new org.gnu.gdk.EventMask[] {
            new org.gnu.gdk.EventMask(0), new org.gnu.gdk.EventMask(1),
            EXPOSURE_MASK, new org.gnu.gdk.EventMask(3), POINTER_MOTION_MASK,
            new org.gnu.gdk.EventMask(5), new org.gnu.gdk.EventMask(6),
            new org.gnu.gdk.EventMask(7), POINTER_MOTION_HINT_MASK,
            new org.gnu.gdk.EventMask(9), new org.gnu.gdk.EventMask(10),
            new org.gnu.gdk.EventMask(11), new org.gnu.gdk.EventMask(12),
            new org.gnu.gdk.EventMask(13), new org.gnu.gdk.EventMask(14),
            new org.gnu.gdk.EventMask(15), BUTTON_MOTION_MASK,
            new org.gnu.gdk.EventMask(17), new org.gnu.gdk.EventMask(18),
            new org.gnu.gdk.EventMask(19), new org.gnu.gdk.EventMask(20),
            new org.gnu.gdk.EventMask(21), new org.gnu.gdk.EventMask(22),
            new org.gnu.gdk.EventMask(23), new org.gnu.gdk.EventMask(24),
            new org.gnu.gdk.EventMask(25), new org.gnu.gdk.EventMask(26),
            new org.gnu.gdk.EventMask(27), new org.gnu.gdk.EventMask(28),
            new org.gnu.gdk.EventMask(29), new org.gnu.gdk.EventMask(30),
            new org.gnu.gdk.EventMask(31), BUTTON1_MOTION_MASK,
            new org.gnu.gdk.EventMask(33), new org.gnu.gdk.EventMask(34),
            new org.gnu.gdk.EventMask(35), new org.gnu.gdk.EventMask(36),
            new org.gnu.gdk.EventMask(37), new org.gnu.gdk.EventMask(38),
            new org.gnu.gdk.EventMask(39), new org.gnu.gdk.EventMask(40),
            new org.gnu.gdk.EventMask(41), new org.gnu.gdk.EventMask(42),
            new org.gnu.gdk.EventMask(43), new org.gnu.gdk.EventMask(44),
            new org.gnu.gdk.EventMask(45), new org.gnu.gdk.EventMask(46),
            new org.gnu.gdk.EventMask(47), new org.gnu.gdk.EventMask(48),
            new org.gnu.gdk.EventMask(49), new org.gnu.gdk.EventMask(50),
            new org.gnu.gdk.EventMask(51), new org.gnu.gdk.EventMask(52),
            new org.gnu.gdk.EventMask(53), new org.gnu.gdk.EventMask(54),
            new org.gnu.gdk.EventMask(55), new org.gnu.gdk.EventMask(56),
            new org.gnu.gdk.EventMask(57), new org.gnu.gdk.EventMask(58),
            new org.gnu.gdk.EventMask(59), new org.gnu.gdk.EventMask(60),
            new org.gnu.gdk.EventMask(61), new org.gnu.gdk.EventMask(62),
            new org.gnu.gdk.EventMask(63), BUTTON2_MOTION_MASK,
            new org.gnu.gdk.EventMask(65), new org.gnu.gdk.EventMask(66),
            new org.gnu.gdk.EventMask(67), new org.gnu.gdk.EventMask(68),
            new org.gnu.gdk.EventMask(69), new org.gnu.gdk.EventMask(70),
            new org.gnu.gdk.EventMask(71), new org.gnu.gdk.EventMask(72),
            new org.gnu.gdk.EventMask(73), new org.gnu.gdk.EventMask(74),
            new org.gnu.gdk.EventMask(75), new org.gnu.gdk.EventMask(76),
            new org.gnu.gdk.EventMask(77), new org.gnu.gdk.EventMask(78),
            new org.gnu.gdk.EventMask(79), new org.gnu.gdk.EventMask(80),
            new org.gnu.gdk.EventMask(81), new org.gnu.gdk.EventMask(82),
            new org.gnu.gdk.EventMask(83), new org.gnu.gdk.EventMask(84),
            new org.gnu.gdk.EventMask(85), new org.gnu.gdk.EventMask(86),
            new org.gnu.gdk.EventMask(87), new org.gnu.gdk.EventMask(88),
            new org.gnu.gdk.EventMask(89), new org.gnu.gdk.EventMask(90),
            new org.gnu.gdk.EventMask(91), new org.gnu.gdk.EventMask(92),
            new org.gnu.gdk.EventMask(93), new org.gnu.gdk.EventMask(94),
            new org.gnu.gdk.EventMask(95), new org.gnu.gdk.EventMask(96),
            new org.gnu.gdk.EventMask(97), new org.gnu.gdk.EventMask(98),
            new org.gnu.gdk.EventMask(99), new org.gnu.gdk.EventMask(100),
            new org.gnu.gdk.EventMask(101), new org.gnu.gdk.EventMask(102),
            new org.gnu.gdk.EventMask(103), new org.gnu.gdk.EventMask(104),
            new org.gnu.gdk.EventMask(105), new org.gnu.gdk.EventMask(106),
            new org.gnu.gdk.EventMask(107), new org.gnu.gdk.EventMask(108),
            new org.gnu.gdk.EventMask(109), new org.gnu.gdk.EventMask(110),
            new org.gnu.gdk.EventMask(111), new org.gnu.gdk.EventMask(112),
            new org.gnu.gdk.EventMask(113), new org.gnu.gdk.EventMask(114),
            new org.gnu.gdk.EventMask(115), new org.gnu.gdk.EventMask(116),
            new org.gnu.gdk.EventMask(117), new org.gnu.gdk.EventMask(118),
            new org.gnu.gdk.EventMask(119), new org.gnu.gdk.EventMask(120),
            new org.gnu.gdk.EventMask(121), new org.gnu.gdk.EventMask(122),
            new org.gnu.gdk.EventMask(123), new org.gnu.gdk.EventMask(124),
            new org.gnu.gdk.EventMask(125), new org.gnu.gdk.EventMask(126),
            new org.gnu.gdk.EventMask(127), BUTTON3_MOTION_MASK,
            new org.gnu.gdk.EventMask(129), new org.gnu.gdk.EventMask(130),
            new org.gnu.gdk.EventMask(131), new org.gnu.gdk.EventMask(132),
            new org.gnu.gdk.EventMask(133), new org.gnu.gdk.EventMask(134),
            new org.gnu.gdk.EventMask(135), new org.gnu.gdk.EventMask(136),
            new org.gnu.gdk.EventMask(137), new org.gnu.gdk.EventMask(138),
            new org.gnu.gdk.EventMask(139), new org.gnu.gdk.EventMask(140),
            new org.gnu.gdk.EventMask(141), new org.gnu.gdk.EventMask(142),
            new org.gnu.gdk.EventMask(143), new org.gnu.gdk.EventMask(144),
            new org.gnu.gdk.EventMask(145), new org.gnu.gdk.EventMask(146),
            new org.gnu.gdk.EventMask(147), new org.gnu.gdk.EventMask(148),
            new org.gnu.gdk.EventMask(149), new org.gnu.gdk.EventMask(150),
            new org.gnu.gdk.EventMask(151), new org.gnu.gdk.EventMask(152),
            new org.gnu.gdk.EventMask(153), new org.gnu.gdk.EventMask(154),
            new org.gnu.gdk.EventMask(155), new org.gnu.gdk.EventMask(156),
            new org.gnu.gdk.EventMask(157), new org.gnu.gdk.EventMask(158),
            new org.gnu.gdk.EventMask(159), new org.gnu.gdk.EventMask(160),
            new org.gnu.gdk.EventMask(161), new org.gnu.gdk.EventMask(162),
            new org.gnu.gdk.EventMask(163), new org.gnu.gdk.EventMask(164),
            new org.gnu.gdk.EventMask(165), new org.gnu.gdk.EventMask(166),
            new org.gnu.gdk.EventMask(167), new org.gnu.gdk.EventMask(168),
            new org.gnu.gdk.EventMask(169), new org.gnu.gdk.EventMask(170),
            new org.gnu.gdk.EventMask(171), new org.gnu.gdk.EventMask(172),
            new org.gnu.gdk.EventMask(173), new org.gnu.gdk.EventMask(174),
            new org.gnu.gdk.EventMask(175), new org.gnu.gdk.EventMask(176),
            new org.gnu.gdk.EventMask(177), new org.gnu.gdk.EventMask(178),
            new org.gnu.gdk.EventMask(179), new org.gnu.gdk.EventMask(180),
            new org.gnu.gdk.EventMask(181), new org.gnu.gdk.EventMask(182),
            new org.gnu.gdk.EventMask(183), new org.gnu.gdk.EventMask(184),
            new org.gnu.gdk.EventMask(185), new org.gnu.gdk.EventMask(186),
            new org.gnu.gdk.EventMask(187), new org.gnu.gdk.EventMask(188),
            new org.gnu.gdk.EventMask(189), new org.gnu.gdk.EventMask(190),
            new org.gnu.gdk.EventMask(191), new org.gnu.gdk.EventMask(192),
            new org.gnu.gdk.EventMask(193), new org.gnu.gdk.EventMask(194),
            new org.gnu.gdk.EventMask(195), new org.gnu.gdk.EventMask(196),
            new org.gnu.gdk.EventMask(197), new org.gnu.gdk.EventMask(198),
            new org.gnu.gdk.EventMask(199), new org.gnu.gdk.EventMask(200),
            new org.gnu.gdk.EventMask(201), new org.gnu.gdk.EventMask(202),
            new org.gnu.gdk.EventMask(203), new org.gnu.gdk.EventMask(204),
            new org.gnu.gdk.EventMask(205), new org.gnu.gdk.EventMask(206),
            new org.gnu.gdk.EventMask(207), new org.gnu.gdk.EventMask(208),
            new org.gnu.gdk.EventMask(209), new org.gnu.gdk.EventMask(210),
            new org.gnu.gdk.EventMask(211), new org.gnu.gdk.EventMask(212),
            new org.gnu.gdk.EventMask(213), new org.gnu.gdk.EventMask(214),
            new org.gnu.gdk.EventMask(215), new org.gnu.gdk.EventMask(216),
            new org.gnu.gdk.EventMask(217), new org.gnu.gdk.EventMask(218),
            new org.gnu.gdk.EventMask(219), new org.gnu.gdk.EventMask(220),
            new org.gnu.gdk.EventMask(221), new org.gnu.gdk.EventMask(222),
            new org.gnu.gdk.EventMask(223), new org.gnu.gdk.EventMask(224),
            new org.gnu.gdk.EventMask(225), new org.gnu.gdk.EventMask(226),
            new org.gnu.gdk.EventMask(227), new org.gnu.gdk.EventMask(228),
            new org.gnu.gdk.EventMask(229), new org.gnu.gdk.EventMask(230),
            new org.gnu.gdk.EventMask(231), new org.gnu.gdk.EventMask(232),
            new org.gnu.gdk.EventMask(233), new org.gnu.gdk.EventMask(234),
            new org.gnu.gdk.EventMask(235), new org.gnu.gdk.EventMask(236),
            new org.gnu.gdk.EventMask(237), new org.gnu.gdk.EventMask(238),
            new org.gnu.gdk.EventMask(239), new org.gnu.gdk.EventMask(240),
            new org.gnu.gdk.EventMask(241), new org.gnu.gdk.EventMask(242),
            new org.gnu.gdk.EventMask(243), new org.gnu.gdk.EventMask(244),
            new org.gnu.gdk.EventMask(245), new org.gnu.gdk.EventMask(246),
            new org.gnu.gdk.EventMask(247), new org.gnu.gdk.EventMask(248),
            new org.gnu.gdk.EventMask(249), new org.gnu.gdk.EventMask(250),
            new org.gnu.gdk.EventMask(251), new org.gnu.gdk.EventMask(252),
            new org.gnu.gdk.EventMask(253), new org.gnu.gdk.EventMask(254),
            new org.gnu.gdk.EventMask(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.EventMask theSacrificialOne = new org.gnu.gdk.EventMask(
            0);

    static public org.gnu.gdk.EventMask intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.EventMask already = (org.gnu.gdk.EventMask) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.EventMask(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private EventMask(int value) {
        value_ = value;
    }

    public org.gnu.gdk.EventMask or(org.gnu.gdk.EventMask other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.EventMask and(org.gnu.gdk.EventMask other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.EventMask xor(org.gnu.gdk.EventMask other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.EventMask other) {
        return (value_ & other.value_) == other.value_;
    }

}
