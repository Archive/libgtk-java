/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class WindowState extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _WITHDRAWN = 1 << 0;

    static final public org.gnu.gdk.WindowState WITHDRAWN = new org.gnu.gdk.WindowState(
            _WITHDRAWN);

    static final private int _ICONIFIED = 1 << 1;

    static final public org.gnu.gdk.WindowState ICONIFIED = new org.gnu.gdk.WindowState(
            _ICONIFIED);

    static final private int _MAXIMIZED = 1 << 2;

    static final public org.gnu.gdk.WindowState MAXIMIZED = new org.gnu.gdk.WindowState(
            _MAXIMIZED);

    static final private int _STICKY = 1 << 3;

    static final public org.gnu.gdk.WindowState STICKY = new org.gnu.gdk.WindowState(
            _STICKY);

    static final private org.gnu.gdk.WindowState[] theInterned = new org.gnu.gdk.WindowState[] {
            new org.gnu.gdk.WindowState(0), WITHDRAWN, ICONIFIED,
            new org.gnu.gdk.WindowState(3), MAXIMIZED,
            new org.gnu.gdk.WindowState(5), new org.gnu.gdk.WindowState(6),
            new org.gnu.gdk.WindowState(7), STICKY }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowState theSacrificialOne = new org.gnu.gdk.WindowState(
            0);

    static public org.gnu.gdk.WindowState intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowState already = (org.gnu.gdk.WindowState) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowState(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowState(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowState or(org.gnu.gdk.WindowState other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowState and(org.gnu.gdk.WindowState other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowState xor(org.gnu.gdk.WindowState other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowState other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
