/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * In addition to the normal keyboard and mouse input devices, GTK+ also
 * contains support for extended input devices. In particular, this support is
 * targeted at graphics tablets. Graphics tablets typically return sub-pixel
 * positioning information and possibly information about the pressure and tilt
 * of the stylus. Under X, the support for extended devices is done through the
 * XInput extension. This class represents a generic device, allowing the
 * programmer to configure various aspects of each device.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Device</code>.
 */
public class Device extends GObject {
    public Device(Handle handle) {
        super(handle);
    }

    /**
     * Returns the name of this device.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return Device.getName(getHandle());
    }

    /**
     * Returns the type of this device.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public InputSource getSource() {
        int value = Device.getSource(getHandle());
        return org.gnu.gdk.InputSource.intern(value);
    }

    /**
     * Returns the mode of an input device.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public InputMode getMode() {
        int value = Device.getMode(getHandle());
        return org.gnu.gdk.InputMode.intern(value);
    }

    /**
     * TRUE if the X pointer follows device motion.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean hasCursor() {
        return Device.getHasCursor(getHandle());
    }

    /**
     * Returns the list of available input devices for the default display.
     * 
     * @return A java.util.List containing the available input <tt>Device</tt>
     *         instances.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static List getDevices() {
        Handle[] l = Device.gdk_devices_list();
        ArrayList list = new ArrayList();
        for (int i = 0; i < l.length; i++) {
            list.add(Device.getDeviceFromHandle(l[i]));
        }
        return list;
    }

    /**
     * @deprecated Use {@link #getDevices} instead.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static org.gnu.glib.List getDevicesList() {
        return null;
    }

    /**
     * Sets the source type for an input device.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSource(InputSource source) {
        Device.gdk_device_set_source(getHandle(), source.getValue());
    }

    /**
     * Sets a the mode of an input device. The mode controls if the device is
     * active and whether the device's range is mapped to the entire screen or
     * to a single window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMode(InputMode mode) {
        Device.gdk_device_set_mode(getHandle(), mode.getValue());
    }

    /**
     * Specifies the X key event to generate when a macro button of a device is
     * pressed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setKeyValue(int btnIndex, int keyVal, ModifierType modifier) {
        Device.gdk_device_set_key(getHandle(), btnIndex, keyVal, modifier
                .getValue());
    }

    /**
     * Specifies how an axis of a device is used.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setAxisUse(int axisIndex, AxisUse use) {
        Device.gdk_device_set_axis_use(getHandle(), axisIndex, use.getValue());
    }

    /**
     * Returns the device for the core pointer.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Device getCorePointer() {
        return Device.getDeviceFromHandle(Device.gdk_device_get_core_pointer());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Device getDeviceFromHandle(Handle hndl) {
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (Device) obj : new Device(hndl);
        }
        return null;
    }

    native static final protected String getName(Handle obj);

    native static final protected int getSource(Handle obj);

    native static final protected int getMode(Handle obj);

    native static final protected boolean getHasCursor(Handle obj);

    native static final protected int gdk_device_get_type();

    native static final protected Handle[] gdk_devices_list();

    native static final protected void gdk_device_set_source(Handle device,
            int source);

    native static final protected void gdk_device_set_mode(Handle device,
            int mode);

    native static final protected void gdk_device_set_key(Handle device,
            int index, int keyval, int modifiers);

    native static final protected void gdk_device_set_axis_use(Handle device,
            int index, int use);

    native static final protected boolean gdk_device_get_axis(Handle device,
            double[] axes, int use, double[] value);

    native static final protected Handle gdk_device_get_core_pointer();
}
