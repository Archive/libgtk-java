/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Describes an allocated or unallocated color.
 * 
 * A color consists of red, green and blue values in the range 0-65535 and a
 * pixel value. The pixel value is highly dependent on the depth and colormap
 * which this color will be used to draw into. Therefore, sharing colors between
 * colormaps is a bad idea.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Color</code>.
 */
public class Color extends Boxed {

    public static final Color RED = new Color(65535, 0, 0);

    public static final Color BLUE = new Color(0, 0, 65535);

    public static final Color GREEN = new Color(0, 65535, 0);

    public static final Color BLACK = new Color(0, 0, 0);

    public static final Color YELLOW = new Color(65535, 65535, 0);

    public static final Color WHITE = new Color(65535, 65535, 65535);

    public static final Color ORANGE = new Color(65535, 51400, 0);

    /**
     * Construct a Color object from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color(Handle handle) {
        super(handle);
    }

    /**
     * Creates an sRGB color with the specified red, green, and blue values in
     * the range (0 - 65535).
     * 
     * @see Color#parse(String) if a Color object from a textual representation
     *      is required.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color(int red, int green, int blue) {
        super(allocHandle());
        setRed(getHandle(), red);
        setBlue(getHandle(), blue);
        setGreen(getHandle(), green);
    }

    /**
     * Makes a copy of this color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object clone() {
        return Color.getColorFromHandle(Color.gdk_color_copy(getHandle()));
    }

    /**
     * Determines whether another object is equal to this Color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equals(Object color) {
        if (color == null) {
            return false;
        }
        return Color.gdk_color_equal(getHandle(), ((Color) color).getHandle());
    }

    public int hashCode() {
        return Color.gdk_color_hash(getHandle());
    }

    /**
     * Returns the red component in the range 0-65535 in the default sRGB space.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRed() {
        return Color.getRed(getHandle());
    }

    /**
     * Sets the red component in the range of 0-65535.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRed(short value) {
        Color.setRed(getHandle(), value);
    }

    /**
     * Returns the green component in the range 0-65535 in the default sRGB
     * space.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getGreen() {
        return Color.getGreen(getHandle());
    }

    /**
     * Sets the green component in the range of 0-65535.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setGreen(short value) {
        Color.setGreen(getHandle(), value);
    }

    /**
     * Returns the blue component in the range 0-65535 in the default sRGB
     * space.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getBlue() {
        return Color.getBlue(getHandle());
    }

    /**
     * Sets the blue component in the range of 0-65535.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBlue(short value) {
        Color.setBlue(getHandle(), value);
    }

    /**
     * 
     * Returns a string representation of this Color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return "Red: " + getRed() + "\n" + "Green: " + getGreen() + "\n"
                + "Blue: " + getBlue() + "\n";
    }

    /**
     * Parses a textual specification of a color. The text string can be in any
     * of the forms accepted by <i>XParseColor</i>; these include name for a
     * color from <i>rgb.txt</i>, such as <i>DarkSlateGray</i> or a hex
     * specification such as 305050.
     * 
     * @param spec
     *            The textual description of the color.
     * @return The new Color object or <tt>null</tt> if the spec could not be
     *         parsed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Color parse(String spec) {
        return getColorFromHandle(gdk_color_parse(spec));
    }

    public int getPixel() {
        return getPixel(getHandle());
    }

    public Type getType() {
        return new Type(gdk_color_get_type());
    }

    /**
     * Construct a Color object from a handle to a native resource. It should
     * only be used internally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Color getColorFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            return (box != null) ? (Color) box : new Color(hndl);
        }
        return null;
    }

    private static native Handle allocHandle();

    native static final protected int getPixel(Handle obj);

    native static final protected int getRed(Handle obj);

    native static final protected void setRed(Handle obj, int red);

    native static final protected int getGreen(Handle obj);

    native static final protected void setGreen(Handle obj, int green);

    native static final protected int getBlue(Handle obj);

    native static final protected void setBlue(Handle obj, int blue);

    native static final protected int gdk_color_get_type();

    native static final protected Handle gdk_color_copy(Handle color);

    native static final protected Handle gdk_color_parse(String spec);

    native static final protected int gdk_color_hash(Handle colora);

    native static final protected boolean gdk_color_equal(Handle colora,
            Handle colorb);
}
