/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import java.io.IOException;
import java.io.InputStream;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * The <tt>PixbufLoader</tt> provides a way for applications to drive the
 * process of loading an image, by letting them send the image data directly to
 * the loader instead of having the loader read the data from a file.
 * Applications can use this functionality instead of
 * {@link Pixbuf#Pixbuf(String)} or {@link
 * PixbufAnimation#PixbufAnimation(String)} when they need to parse image data
 * in small chunks. For example, it should be used when reading an image from a
 * (potentially) slow network connection, when loading an extremely large file
 * or even reading an image from a database.
 * <p>
 * 
 * To use <tt>PixbufLoader</tt> to load an image, just create a new one, and
 * call {@link #write(byte[])}, {@link #write(byte[],int)}, or
 * {@link #write(InputStream)} to send the data to it. When done, {@link #close}
 * should be called to end the stream and finalize everything. The created
 * <tt>{@link Pixbuf}</tt> can be retireved using the {@link #getPixbuf}
 * method.
 * 
 * @see org.gnu.gdk.Pixbuf
 * @see org.gnu.gdk.PixbufAnimation
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufLoader</code>.
 */
public class PixbufLoader extends GObject {
    /**
     * Construct a new <tt>PixbufLoader</tt> that automatically detects the
     * type of image based on the data.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufLoader() {
        super(gdk_pixbuf_loader_new());
    }

    /**
     * Construct a new <tt>PixbufLoader</tt> that parses the image data as if
     * it were an image of <tt>imageType</tt>.
     * 
     * @param imageType
     *            Name of the image format to be loaded with the image.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufLoader(String imageType) {
        super(gdk_pixbuf_loader_new_with_type(imageType, 0));
    }

    /**
     * Parse the images bytes from the given <tt>buffer</tt>.
     * 
     * @return <tt>true</tt> if the data was loaded successfully,
     *         <tt>false</tt> if an error occured.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean write(byte[] buffer) {
        int error = 0;
        return gdk_pixbuf_loader_write(getHandle(), buffer, buffer.length,
                error);
    }

    /**
     * Parse <tt>len</tt> image bytes from <tt>buffer</tt>.
     * 
     * @return <tt>true</tt> if the data was loaded successfully,
     *         <tt>false</tt> if an error occured.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean write(byte[] buffer, int len) {
        int error = 0;
        return gdk_pixbuf_loader_write(getHandle(), buffer, len, error);
    }

    /**
     * Parse the data for the image from the given <tt>stream</tt>. The
     * contents of the stream are read and loaded into the Pixbuf using
     * succesive calls to the {@link #write(byte[],int)} method. If all of the
     * data for the image is in the given <tt>stream</tt>, the {@link #close}
     * method should be called after this method finishes. You can call
     * {@link #getPixbuf} to get the resulting <tt>Pixbuf</tt> object.
     * <p>
     * This method does not make any attempt to efficiently read the data from
     * the given stream. Calling applications should wrap their
     * <tt>InputStreams</tt> in efficient implementations (such as
     * <tt>Buffered*</tt> implementations) if necessary before calling this
     * method.
     * 
     * @param stream
     *            A stream containing the data to load.
     * @return <tt>true</tt> if the data was loaded successfully,
     *         <tt>false</tt> if an error occured.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean write(InputStream stream) {
        boolean ret = true;
        int bt = 0;
        byte buf[] = new byte[1024];
        try {
            while ((bt = stream.read(buf)) != -1) {
                ret = ret && write(buf, bt);
            }
        } catch (IOException ie) {
            ret = false;
        }
        return ret;
    }

    /**
     * Get the <tt>{@link Pixbuf}</tt> object that is currently being created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Pixbuf getPixbuf() {
        return Pixbuf
                .getPixbufFromHandle(gdk_pixbuf_loader_get_pixbuf(getHandle()));
    }

    /**
     * Get the <tt>{@link PixbufAnimation}</tt> object that is currently being
     * created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public PixbufAnimation getPixbufAnimation() {
        Handle hndl = gdk_pixbuf_loader_get_animation(getHandle());
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (PixbufAnimation) obj : new PixbufAnimation(
                    hndl);
        }
        return null;

    }

    /**
     * Informs a loader that no further writes will occur, so that it can free
     * its internal loading structures.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean close() {
        int error = 0;
        return gdk_pixbuf_loader_close(getHandle(), error);
    }

    native static final protected int gdk_pixbuf_loader_get_type();

    native static final protected Handle gdk_pixbuf_loader_new();

    native static final protected Handle gdk_pixbuf_loader_new_with_type(
            String imageType, int error);

    native static final protected boolean gdk_pixbuf_loader_write(
            Handle loader, byte[] buf, int count, int error);

    native static final protected Handle gdk_pixbuf_loader_get_pixbuf(
            Handle loader);

    native static final protected Handle gdk_pixbuf_loader_get_animation(
            Handle loader);

    native static final protected boolean gdk_pixbuf_loader_close(
            Handle loader, int error);
}
