/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Contains information about an image format of a Pixbuf.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufFormat</code>.
 */
public class PixbufFormat extends MemStruct {

    PixbufFormat(Handle handle) {
        super(handle);
    }

    static PixbufFormat getPixbufFormat(Handle handle) {
        if (handle == null) {
            return null;
        }

        PixbufFormat obj = (PixbufFormat) MemStruct
                .getMemStructFromHandle(handle);

        if (obj == null) {
            obj = new PixbufFormat(handle);
        }

        return obj;
    }

    public String getName() {
        return gdk_pixbuf_format_get_name(getHandle());
    }

    public String getDescription() {
        return gdk_pixbuf_format_get_description(getHandle());
    }

    public String[] getMimeTypes() {
        return gdk_pixbuf_format_get_mime_types(getHandle());
    }

    public String[] getExtensions() {
        return gdk_pixbuf_format_get_extensions(getHandle());
    }

    public boolean isWritable() {
        return gdk_pixbuf_format_is_writable(getHandle());
    }

    public boolean isScalable() {
        return gdk_pixbuf_format_is_scalable(getHandle());
    }

    public boolean isDisabled() {
        return gdk_pixbuf_format_is_disabled(getHandle());
    }

    public void setDisabled(boolean disabled) {
        gdk_pixbuf_format_set_disabled(getHandle(), disabled);
    }

    public String getLicense() {
        return gdk_pixbuf_format_get_license(getHandle());
    }

    native static final protected String gdk_pixbuf_format_get_name(
            Handle format);

    native static final protected String gdk_pixbuf_format_get_description(
            Handle format);

    native static final protected String[] gdk_pixbuf_format_get_mime_types(
            Handle format);

    native static final protected String[] gdk_pixbuf_format_get_extensions(
            Handle format);

    native static final protected boolean gdk_pixbuf_format_is_writable(
            Handle format);

    native static final protected boolean gdk_pixbuf_format_is_scalable(
            Handle format);

    native static final protected boolean gdk_pixbuf_format_is_disabled(
            Handle format);

    native static final protected void gdk_pixbuf_format_set_disabled(
            Handle format, boolean disabled);

    native static final protected String gdk_pixbuf_format_get_license(
            Handle format);

}
