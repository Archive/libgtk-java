/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Gives the window manager information about a window's geometry constraints.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Geometry</code>.
 */
public class Geometry extends MemStruct {
    public int getMinWidth() {
        return getMinWidth(getHandle());
    }

    public int getMaxWidth() {
        return getMaxWidth(getHandle());
    }

    public int getMinHeight() {
        return getMinHeight(getHandle());
    }

    public int getMaxHeight() {
        return getMaxHeight(getHandle());
    }

    public int getBaseWidth() {
        return getBaseWidth(getHandle());
    }

    public int getBaseHeight() {
        return getBaseHeight(getHandle());
    }

    public int getWidthIncrement() {
        return getWidthInc(getHandle());
    }

    public int getHeightIncrement() {
        return getHeightInc(getHandle());
    }

    public double getMinAspect() {
        return getMinAspect(getHandle());
    }

    public double getMaxAspect() {
        return getMaxAspect(getHandle());
    }

    public Gravity getWindowGravity() {
        return Gravity.intern(getWinGravity(getHandle()));
    }

    native static final protected int getMinWidth(Handle obj);

    native static final protected int getMinHeight(Handle obj);

    native static final protected int getMaxWidth(Handle obj);

    native static final protected int getMaxHeight(Handle obj);

    native static final protected int getBaseWidth(Handle obj);

    native static final protected int getBaseHeight(Handle obj);

    native static final protected int getWidthInc(Handle obj);

    native static final protected int getHeightInc(Handle obj);

    native static final protected double getMinAspect(Handle obj);

    native static final protected double getMaxAspect(Handle obj);

    native static final protected int getWinGravity(Handle obj);

}
