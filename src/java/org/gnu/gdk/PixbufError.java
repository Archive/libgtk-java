/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gdk;

import org.gnu.glib.Enum;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PixbufError</code>.
 */
public class PixbufError extends Enum {

    static final private int _CORRUPT_IMAGE = 0;

    static final public PixbufError CORRUPT_IMAGE = new PixbufError(
            _CORRUPT_IMAGE);

    static final private int _INSUFFICIENT_MEMORY = 1;

    static final public PixbufError INSUFFICIENT_MEMORY = new PixbufError(
            _INSUFFICIENT_MEMORY);

    static final private int _BAD_OPTION = 2;

    static final public PixbufError BAD_OPTION = new PixbufError(_BAD_OPTION);

    static final private int _UNKNOWN_TYPE = 3;

    static final public PixbufError UNKNOWN_TYPE = new PixbufError(
            _UNKNOWN_TYPE);

    static final private int _UNSUPPORTED_OPERATION = 4;

    static final public PixbufError UNSUPPORTED_OPERATION = new PixbufError(
            _UNSUPPORTED_OPERATION);

    static final private int _FAILED = 5;

    static final public PixbufError FAILED = new PixbufError(_FAILED);

    static final private PixbufError[] theInterned = new PixbufError[] {
            CORRUPT_IMAGE, INSUFFICIENT_MEMORY, BAD_OPTION, UNKNOWN_TYPE,
            UNSUPPORTED_OPERATION, FAILED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private PixbufError theSacrificialOne = new PixbufError(0);

    static public PixbufError intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        PixbufError already = (PixbufError) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new PixbufError(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PixbufError(int value) {
        value_ = value;
    }

    public PixbufError or(PixbufError other) {
        return intern(value_ | other.value_);
    }

    public PixbufError and(PixbufError other) {
        return intern(value_ & other.value_);
    }

    public PixbufError xor(PixbufError other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(PixbufError other) {
        return (value_ & other.value_) == other.value_;
    }

}
