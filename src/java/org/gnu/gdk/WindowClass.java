/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class WindowClass extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _INPUT_OUTPUT = 0;

    static final public org.gnu.gdk.WindowClass INPUT_OUTPUT = new org.gnu.gdk.WindowClass(
            _INPUT_OUTPUT);

    static final private int _INPUT_ONLY = 1;

    static final public org.gnu.gdk.WindowClass INPUT_ONLY = new org.gnu.gdk.WindowClass(
            _INPUT_ONLY);

    static final private org.gnu.gdk.WindowClass[] theInterned = new org.gnu.gdk.WindowClass[] {
            INPUT_OUTPUT, INPUT_ONLY }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowClass theSacrificialOne = new org.gnu.gdk.WindowClass(
            0);

    static public org.gnu.gdk.WindowClass intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowClass already = (org.gnu.gdk.WindowClass) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowClass(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowClass(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowClass or(org.gnu.gdk.WindowClass other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowClass and(org.gnu.gdk.WindowClass other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowClass xor(org.gnu.gdk.WindowClass other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowClass other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
