/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class AxisUse extends Enum {

    static final private int _IGNORE = 0;

    static final public AxisUse IGNORE = new AxisUse(_IGNORE);

    static final private int _X = 1;

    static final public AxisUse X = new AxisUse(_X);

    static final private int _Y = 2;

    static final public AxisUse Y = new AxisUse(_Y);

    static final private int _PRESSURE = 3;

    static final public AxisUse PRESSURE = new AxisUse(_PRESSURE);

    static final private int _XTILT = 4;

    static final public AxisUse XTILT = new AxisUse(_XTILT);

    static final private int _YTILT = 5;

    static final public AxisUse YTILT = new AxisUse(_YTILT);

    static final private int _WHEEL = 6;

    static final public AxisUse WHEEL = new AxisUse(_WHEEL);

    static final private int _LAST = 7;

    static final public AxisUse LAST = new AxisUse(_LAST);

    static final private AxisUse[] theInterned = new AxisUse[] { IGNORE, X, Y,
            PRESSURE, XTILT, YTILT, WHEEL, LAST };

    static private java.util.Hashtable theInternedExtras;

    static final private AxisUse theSacrificialOne = new AxisUse(0);

    static public AxisUse intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        AxisUse already = (AxisUse) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new AxisUse(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private AxisUse(int value) {
        value_ = value;
    }

    public AxisUse or(AxisUse other) {
        return intern(value_ | other.value_);
    }

    public AxisUse and(AxisUse other) {
        return intern(value_ & other.value_);
    }

    public AxisUse xor(AxisUse other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(AxisUse other) {
        return (value_ & other.value_) == other.value_;
    }

}
