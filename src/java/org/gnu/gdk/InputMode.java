/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class InputMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _DISABLED = 0;

    static final public org.gnu.gdk.InputMode DISABLED = new org.gnu.gdk.InputMode(
            _DISABLED);

    static final private int _SCREEN = 1;

    static final public org.gnu.gdk.InputMode SCREEN = new org.gnu.gdk.InputMode(
            _SCREEN);

    static final private int _WINDOW = 2;

    static final public org.gnu.gdk.InputMode WINDOW = new org.gnu.gdk.InputMode(
            _WINDOW);

    static final private org.gnu.gdk.InputMode[] theInterned = new org.gnu.gdk.InputMode[] {
            DISABLED, SCREEN, WINDOW }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.InputMode theSacrificialOne = new org.gnu.gdk.InputMode(
            0);

    static public org.gnu.gdk.InputMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.InputMode already = (org.gnu.gdk.InputMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.InputMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private InputMode(int value) {
        value_ = value;
    }

    public org.gnu.gdk.InputMode or(org.gnu.gdk.InputMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.InputMode and(org.gnu.gdk.InputMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.InputMode xor(org.gnu.gdk.InputMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.InputMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
