/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * This object stores the mapping between the color values stored in memory and
 * the RGB values that are used to display color values.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Colormap</code>.
 */
public class Colormap extends GObject {
    /**
     * Create a Colormap initialized to the system's default colormap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap() {
        super(Colormap.gdk_colormap_get_system());
    }

    /**
     * Create a new Colormap from a handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap(Handle handle) {
        super(handle);
    }

    /**
     * Creates a new colormap for the given visual. If allocate is true the
     * newly created colormap will be a private colormap, and all colors in it
     * will be allocated for the application use.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap(Visual v, boolean allocate) {
        super(gdk_colormap_new(v.getHandle(), allocate));
    }

    /**
     * Returns the system's default colormap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Colormap getSystem() {
        return Colormap.getColormapFromHandle(gdk_colormap_get_system());
    }

    /**
     * Returns the visual for which the colormap was created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Visual getVisual() {
        return Visual.getVisualFromHandle(gdk_colormap_get_visual(getHandle()));
    }

    /**
     * Returns the screen for which the colormap was created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Screen getScreen() {
        return Screen.getScreenFromHandle(gdk_colormap_get_screen(getHandle()));
    }

    /**
     * Allocates a single color from a colormap.
     * 
     * @param color
     *            The color to allocate
     * @param writable
     *            If true the color is allocated writable.
     * @param bestMatch
     *            If true GDK will attempt to do matching against existing
     *            colors if the color cannot be allocated as requested.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean allocateColor(Color color, boolean writable,
            boolean bestMatch) {
        return Colormap.gdk_colormap_alloc_color(getHandle(),
                color.getHandle(), writable, bestMatch);
    }

    /**
     * Allocates colors from a colormap.
     * 
     * @param colors
     * @param writable
     * @param bestMatch
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean[] allocateColors(Color[] colors, boolean writable,
            boolean bestMatch) {
        Handle[] hndls = new Handle[colors.length];
        for (int i = 0; i < colors.length; i++)
            hndls[i] = colors[i].getHandle();
        boolean[] success = new boolean[colors.length];
        gdk_colormap_alloc_colors(getHandle(), hndls, writable, bestMatch,
                success);
        return success;
    }

    /**
     * Frees previously allocated Colors.
     * 
     * @param colors
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void freeColors(Color[] colors) {
        Handle[] hndls = new Handle[colors.length];
        for (int i = 0; i < colors.length; i++)
            hndls[i] = colors[i].getHandle();
        gdk_colormap_free_colors(getHandle(), hndls);
    }

    /**
     * Locates the RGB color corresponding to the given hardware pixel. The
     * pixel must be a valid pixel in the colormap.
     * 
     * @param pixel
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color queryColor(long pixel) {
        Handle handle = gdk_colormap_query_color(getHandle(), pixel);
        return Color.getColorFromHandle(handle);
    }

    /**
     * Retrieve all of the Colors associated with the Colormap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color[] getColors() {
        Handle[] hndls = get_colors(getHandle());
        if (null == hndls)
            return null;
        Color[] colors = new Color[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            colors[i] = Color.getColorFromHandle(hndls[i]);
        }
        return colors;
    }

    public Type getType() {
        return new Type(gdk_colormap_get_type());
    }

    /**
     * Internal static factory method to be used only by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Colormap getColormapFromHandle(Handle hndl) {
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            return (obj != null) ? (Colormap) obj : new Colormap(hndl);
        }
        return null;
    }

    native static final protected int gdk_colormap_get_type();

    native static final protected Handle gdk_colormap_new(Handle visual,
            boolean allocate);

    native static final protected Handle gdk_colormap_get_system();

    native static final protected Handle gdk_colormap_get_screen(Handle colormap);

    native static final protected int gdk_colormap_alloc_colors(
            Handle colormap, Handle[] colors, boolean writable,
            boolean bestMatch, boolean[] success);

    native static final protected boolean gdk_colormap_alloc_color(
            Handle colormap, Handle color, boolean writable, boolean best_match);

    native static final protected void gdk_colormap_free_colors(
            Handle colormap, Handle[] colors);

    native static final protected Handle gdk_colormap_query_color(
            Handle colormap, long pixel);

    native static final protected Handle gdk_colormap_get_visual(Handle colormap);

    native static final protected Handle[] get_colors(Handle colormap);
}
