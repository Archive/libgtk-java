/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Bitmap</code>.
 */
public class Bitmap extends Drawable {

    public Bitmap(Handle handle) {
        super(handle);
    }

    /**
     * Creates a new <code>Bitmap</code> from data in XBM format.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Bitmap(Window window, byte data[], int width, int height) {
        super(Bitmap.gdk_bitmap_create_from_data(window.getHandle(), data,
                width, height));
    }

    /**
     * Creates a new <code>Bitmap</code> from data in XBM format.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Bitmap(byte data[], int width, int height) {
        super(Bitmap.gdk_bitmap_create_from_data(null, data, width, height));
    }

    native static final protected Handle gdk_bitmap_create_from_data(
            Handle window, byte[] data, int width, int height);
}
