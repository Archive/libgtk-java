/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class WMDecoration extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _ALL = 1 << 0;

    static final public org.gnu.gdk.WMDecoration ALL = new org.gnu.gdk.WMDecoration(
            _ALL);

    static final private int _BORDER = 1 << 1;

    static final public org.gnu.gdk.WMDecoration BORDER = new org.gnu.gdk.WMDecoration(
            _BORDER);

    static final private int _RESIZEH = 1 << 2;

    static final public org.gnu.gdk.WMDecoration RESIZEH = new org.gnu.gdk.WMDecoration(
            _RESIZEH);

    static final private int _TITLE = 1 << 3;

    static final public org.gnu.gdk.WMDecoration TITLE = new org.gnu.gdk.WMDecoration(
            _TITLE);

    static final private int _MENU = 1 << 4;

    static final public org.gnu.gdk.WMDecoration MENU = new org.gnu.gdk.WMDecoration(
            _MENU);

    static final private int _MINIMIZE = 1 << 5;

    static final public org.gnu.gdk.WMDecoration MINIMIZE = new org.gnu.gdk.WMDecoration(
            _MINIMIZE);

    static final private int _MAXIMIZE = 1 << 6;

    static final public org.gnu.gdk.WMDecoration MAXIMIZE = new org.gnu.gdk.WMDecoration(
            _MAXIMIZE);

    static final private org.gnu.gdk.WMDecoration[] theInterned = new org.gnu.gdk.WMDecoration[] {
            new org.gnu.gdk.WMDecoration(0), ALL, BORDER,
            new org.gnu.gdk.WMDecoration(3), RESIZEH,
            new org.gnu.gdk.WMDecoration(5), new org.gnu.gdk.WMDecoration(6),
            new org.gnu.gdk.WMDecoration(7), TITLE,
            new org.gnu.gdk.WMDecoration(9), new org.gnu.gdk.WMDecoration(10),
            new org.gnu.gdk.WMDecoration(11), new org.gnu.gdk.WMDecoration(12),
            new org.gnu.gdk.WMDecoration(13), new org.gnu.gdk.WMDecoration(14),
            new org.gnu.gdk.WMDecoration(15), MENU,
            new org.gnu.gdk.WMDecoration(17), new org.gnu.gdk.WMDecoration(18),
            new org.gnu.gdk.WMDecoration(19), new org.gnu.gdk.WMDecoration(20),
            new org.gnu.gdk.WMDecoration(21), new org.gnu.gdk.WMDecoration(22),
            new org.gnu.gdk.WMDecoration(23), new org.gnu.gdk.WMDecoration(24),
            new org.gnu.gdk.WMDecoration(25), new org.gnu.gdk.WMDecoration(26),
            new org.gnu.gdk.WMDecoration(27), new org.gnu.gdk.WMDecoration(28),
            new org.gnu.gdk.WMDecoration(29), new org.gnu.gdk.WMDecoration(30),
            new org.gnu.gdk.WMDecoration(31), MINIMIZE,
            new org.gnu.gdk.WMDecoration(33), new org.gnu.gdk.WMDecoration(34),
            new org.gnu.gdk.WMDecoration(35), new org.gnu.gdk.WMDecoration(36),
            new org.gnu.gdk.WMDecoration(37), new org.gnu.gdk.WMDecoration(38),
            new org.gnu.gdk.WMDecoration(39), new org.gnu.gdk.WMDecoration(40),
            new org.gnu.gdk.WMDecoration(41), new org.gnu.gdk.WMDecoration(42),
            new org.gnu.gdk.WMDecoration(43), new org.gnu.gdk.WMDecoration(44),
            new org.gnu.gdk.WMDecoration(45), new org.gnu.gdk.WMDecoration(46),
            new org.gnu.gdk.WMDecoration(47), new org.gnu.gdk.WMDecoration(48),
            new org.gnu.gdk.WMDecoration(49), new org.gnu.gdk.WMDecoration(50),
            new org.gnu.gdk.WMDecoration(51), new org.gnu.gdk.WMDecoration(52),
            new org.gnu.gdk.WMDecoration(53), new org.gnu.gdk.WMDecoration(54),
            new org.gnu.gdk.WMDecoration(55), new org.gnu.gdk.WMDecoration(56),
            new org.gnu.gdk.WMDecoration(57), new org.gnu.gdk.WMDecoration(58),
            new org.gnu.gdk.WMDecoration(59), new org.gnu.gdk.WMDecoration(60),
            new org.gnu.gdk.WMDecoration(61), new org.gnu.gdk.WMDecoration(62),
            new org.gnu.gdk.WMDecoration(63), MAXIMIZE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WMDecoration theSacrificialOne = new org.gnu.gdk.WMDecoration(
            0);

    static public org.gnu.gdk.WMDecoration intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WMDecoration already = (org.gnu.gdk.WMDecoration) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WMDecoration(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WMDecoration(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WMDecoration or(org.gnu.gdk.WMDecoration other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WMDecoration and(org.gnu.gdk.WMDecoration other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WMDecoration xor(org.gnu.gdk.WMDecoration other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WMDecoration other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
