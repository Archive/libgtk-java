/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class PropMode extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _REPLACE = 0;

    static final public org.gnu.gdk.PropMode REPLACE = new org.gnu.gdk.PropMode(
            _REPLACE);

    static final private int _PREPEND = 1;

    static final public org.gnu.gdk.PropMode PREPEND = new org.gnu.gdk.PropMode(
            _PREPEND);

    static final private int _APPEND = 2;

    static final public org.gnu.gdk.PropMode APPEND = new org.gnu.gdk.PropMode(
            _APPEND);

    static final private org.gnu.gdk.PropMode[] theInterned = new org.gnu.gdk.PropMode[] {
            REPLACE, PREPEND, APPEND }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.PropMode theSacrificialOne = new org.gnu.gdk.PropMode(
            0);

    static public org.gnu.gdk.PropMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.PropMode already = (org.gnu.gdk.PropMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.PropMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PropMode(int value) {
        value_ = value;
    }

    public org.gnu.gdk.PropMode or(org.gnu.gdk.PropMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.PropMode and(org.gnu.gdk.PropMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.PropMode xor(org.gnu.gdk.PropMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.PropMode other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
