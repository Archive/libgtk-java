/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.pango.Attribute;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.PangoAttrEmbossed</code>.
 */
public class PangoAttrEmbossed extends Attribute {
    public PangoAttrEmbossed(boolean embossed) {
        super(gdk_pango_attr_embossed_new(embossed));
    }

    public boolean getEmbossed() {
        return getEmbossed(getHandle());
    }

    native static final protected boolean getEmbossed(Handle obj);

    native static final protected Handle gdk_pango_attr_embossed_new(
            boolean embossed);
}
