/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class InputSource extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _MOUSE = 0;

    static final public org.gnu.gdk.InputSource MOUSE = new org.gnu.gdk.InputSource(
            _MOUSE);

    static final private int _PEN = 1;

    static final public org.gnu.gdk.InputSource PEN = new org.gnu.gdk.InputSource(
            _PEN);

    static final private int _ERASER = 2;

    static final public org.gnu.gdk.InputSource ERASER = new org.gnu.gdk.InputSource(
            _ERASER);

    static final private int _CURSOR = 3;

    static final public org.gnu.gdk.InputSource CURSOR = new org.gnu.gdk.InputSource(
            _CURSOR);

    static final private org.gnu.gdk.InputSource[] theInterned = new org.gnu.gdk.InputSource[] {
            MOUSE, PEN, ERASER, CURSOR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.InputSource theSacrificialOne = new org.gnu.gdk.InputSource(
            0);

    static public org.gnu.gdk.InputSource intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.InputSource already = (org.gnu.gdk.InputSource) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.InputSource(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private InputSource(int value) {
        value_ = value;
    }

    public org.gnu.gdk.InputSource or(org.gnu.gdk.InputSource other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.InputSource and(org.gnu.gdk.InputSource other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.InputSource xor(org.gnu.gdk.InputSource other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.InputSource other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
