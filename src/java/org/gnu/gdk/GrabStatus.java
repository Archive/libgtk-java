/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class GrabStatus extends Enum {
    static final private int _SUCCESS = 0;

    /**
     * The resource was successfully grabbed.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.GrabStatus SUCCESS = new org.gnu.gdk.GrabStatus(
            _SUCCESS);

    static final private int _ALREADY_GRABBED = 1;

    /**
     * The resource is actively grabbed by another client.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.GrabStatus ALREADY_GRABBED = new org.gnu.gdk.GrabStatus(
            _ALREADY_GRABBED);

    static final private int _INVALID_TIME = 2;

    /**
     * The resource was grabbed more recently than the specified time.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.GrabStatus INVALID_TIME = new org.gnu.gdk.GrabStatus(
            _INVALID_TIME);

    static final private int _NOT_VIEWABLE = 3;

    /**
     * The grab window or the confineTo window are not viewable.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.GrabStatus NOT_VIEWABLE = new org.gnu.gdk.GrabStatus(
            _NOT_VIEWABLE);

    static final private int _FROZEN = 4;

    /**
     * The resource is frozen by an active grab of another client.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.GrabStatus FROZEN = new org.gnu.gdk.GrabStatus(
            _FROZEN);

    static final private org.gnu.gdk.GrabStatus[] theInterned = new org.gnu.gdk.GrabStatus[] {
            SUCCESS, ALREADY_GRABBED, INVALID_TIME, NOT_VIEWABLE, FROZEN }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.GrabStatus theSacrificialOne = new org.gnu.gdk.GrabStatus(
            0);

    static public org.gnu.gdk.GrabStatus intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.GrabStatus already = (org.gnu.gdk.GrabStatus) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.GrabStatus(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private GrabStatus(int value) {
        value_ = value;
    }

    public org.gnu.gdk.GrabStatus or(org.gnu.gdk.GrabStatus other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.GrabStatus and(org.gnu.gdk.GrabStatus other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.GrabStatus xor(org.gnu.gdk.GrabStatus other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.GrabStatus other) {
        return (value_ & other.value_) == other.value_;
    }
}
