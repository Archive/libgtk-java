/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Keymap</code>.
 */
public class Keymap extends GObject {
    public Keymap() {
        super(gdk_keymap_get_default());
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(gdk_keymap_get_type());
    }

    native static final protected int gdk_keymap_get_type();

    native static final protected Handle gdk_keymap_get_default();

    native static final protected int gdk_keymap_lookup_key(Handle keymap,
            Handle key);

    native static final protected boolean gdk_keymap_get_entries_for_keyval(
            Handle keymap, int keyval, Handle[] keys);

    native static final protected boolean gdk_keymap_get_entries_for_keycode(
            Handle keymap, int hardwareKeycode, Handle[] keys, int[] keyvals);

    native static final protected int gdk_keymap_get_direction(Handle keymap);
}
