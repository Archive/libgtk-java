/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Event</code>.
 */
public class Event extends Boxed {

    /**
     * Create a new event of a given type.
     * 
     * @param type
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Event(EventType type) {
        super(gdk_event_new(type.getValue()));
    }

    /**
     * Creates a copy of an event.
     * 
     * @param event
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Event(Event event) {
        super(gdk_event_copy(event.getHandle()));
    }

    Event(Handle handle) {
        super(handle);
    }

    /**
     * Checks if any events are ready to be processed for any display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public boolean eventsPending() {
        return gdk_events_pending();
    }

    /**
     * If there is an event waiting in the event queue of some open display,
     * returns a copy of it.
     * 
     * @return A copy of the first Event on some event queue or null if no
     *         events are in any queue.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public Event peek() {
        return getEventFromHandle(gdk_event_peek());
    }

    /**
     * Checks all open displays for an Event to process, to be processed on,
     * fetching events from the windowing system if necessary.
     * 
     * @return The next Event to be processed or null if no events are pending.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public Event get() {
        return getEventFromHandle(gdk_event_get());
    }

    /**
     * Waits for a GraphicsExpose or NoExpose event from the X server.
     * 
     * @param window
     *            Window to wait for the events for.
     * @return An EventExpose event if a GraphicsExpose was received or null if
     *         a NoExpose event was received.
     * @deprecated This method is intented for use with deprecated GTK widgets
     *             and as such has been deprecated.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public EventExpose getGraphicsExpose(Window window) {
        Handle hndl = gdk_event_get_graphics_expose(window.getHandle());
        if (null == hndl)
            return null;
        return new EventExpose(hndl);
    }

    /**
     * Appends a copy of a given event onto the front of the event queue the
     * event window's display or the default event queue if the event's window
     * is null.
     * 
     * @param anEvent
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static public void put(Event anEvent) {
        gdk_event_put(anEvent.getHandle());
    }

    /**
     * Returns the timestamp of the event.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTime() {
        return gdk_event_get_time(getHandle());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Event getEventFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            return (box != null) ? (Event) box : new Event(hndl);
        }
        return null;
    }

    native static final protected int getType(Handle obj);

    native static final protected Handle gdk_event_get();

    native static final protected Handle gdk_event_peek();

    native static final protected Handle gdk_event_get_graphics_expose(
            Handle window);

    native static final protected void gdk_event_put(Handle event);

    native static final protected Handle gdk_event_new(int type);

    native static final protected Handle gdk_event_copy(Handle event);

    native static final protected int gdk_event_get_time(Handle event);

    native static final protected boolean gdk_event_get_coords(Handle event,
            double[] xWin, double[] yWin);

    native static final protected boolean gdk_event_get_root_coords(
            Handle event, double[] xRoot, double[] yRoot);

    native static final protected boolean gdk_event_get_axis(Handle event,
            int axisUse, double[] value);

    native static final protected boolean gdk_events_pending();
}
