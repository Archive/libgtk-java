package org.gnu.gdk;

import org.gnu.glib.Flags;

/**
 * 
 * @author ajocksch
 * @deprecated
 * @see org.gnu.gdk.KeyValue
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.KeySymbol</code>.
 */
public class KeySymbol extends Flags {

    static final private int _a = 0x061;

    static final private int _A = 0x041;

    static final private int _aacute = 0x0e1;

    static final private int _Aacute = 0x0c1;

    static final private int _abovedot = 0x1ff;

    static final private int _abreve = 0x1e3;

    static final private int _Abreve = 0x1c3;

    static final private int _AccessX_Enable = 0xFE70;

    static final private int _AccessX_Feedback_Enable = 0xFE71;

    static final private int _acircumflex = 0x0e2;

    static final private int _Acircumflex = 0x0c2;

    static final private int _acute = 0x0b4;

    static final private int _adiaeresis = 0x0e4;

    static final private int _Adiaeresis = 0x0c4;

    static final private int _ae = 0x0e6;

    static final private int _AE = 0x0c6;

    static final private int _agrave = 0x0e0;

    static final private int _Agrave = 0x0c0;

    static final private int _Alt_L = 0xFFE9;

    static final private int _Alt_R = 0xFFEA;

    static final private int _AltCursor_3270 = 0xFD10;

    static final private int _amacron = 0x3e0;

    static final private int _Amacron = 0x3c0;

    static final private int _ampersand = 0x026;

    static final private int _aogonek = 0x1b1;

    static final private int _Aogonek = 0x1a1;

    static final private int _apostrophe = 0x027;

    static final private int _approximate = 0x8c8;

    static final private int _Arabic_ain = 0x5d9;

    static final private int _Arabic_alef = 0x5c7;

    static final private int _Arabic_alefmaksura = 0x5e9;

    static final private int _Arabic_beh = 0x5c8;

    static final private int _Arabic_comma = 0x5ac;

    static final private int _Arabic_dad = 0x5d6;

    static final private int _Arabic_dal = 0x5cf;

    static final private int _Arabic_damma = 0x5ef;

    static final private int _Arabic_dammatan = 0x5ec;

    static final private int _Arabic_fatha = 0x5ee;

    static final private int _Arabic_fathatan = 0x5eb;

    static final private int _Arabic_feh = 0x5e1;

    static final private int _Arabic_ghain = 0x5da;

    static final private int _Arabic_ha = 0x5e7;

    static final private int _Arabic_hah = 0x5cd;

    static final private int _Arabic_hamza = 0x5c1;

    static final private int _Arabic_hamzaonalef = 0x5c3;

    static final private int _Arabic_hamzaonwaw = 0x5c4;

    static final private int _Arabic_hamzaonyeh = 0x5c6;

    static final private int _Arabic_hamzaunderalef = 0x5c5;

    static final private int _Arabic_heh = 0x5e7;

    static final private int _Arabic_jeem = 0x5cc;

    static final private int _Arabic_kaf = 0x5e3;

    static final private int _Arabic_kasra = 0x5f0;

    static final private int _Arabic_kasratan = 0x5ed;

    static final private int _Arabic_khah = 0x5ce;

    static final private int _Arabic_lam = 0x5e4;

    static final private int _Arabic_maddaonalef = 0x5c2;

    static final private int _Arabic_meem = 0x5e5;

    static final private int _Arabic_noon = 0x5e6;

    static final private int _Arabic_qaf = 0x5e2;

    static final private int _Arabic_question_mark = 0x5bf;

    static final private int _Arabic_ra = 0x5d1;

    static final private int _Arabic_sad = 0x5d5;

    static final private int _Arabic_seen = 0x5d3;

    static final private int _Arabic_semicolon = 0x5bb;

    static final private int _Arabic_shadda = 0x5f1;

    static final private int _Arabic_sheen = 0x5d4;

    static final private int _Arabic_sukun = 0x5f2;

    static final private int _Arabic_switch = 0xFF7E;

    static final private int _Arabic_tah = 0x5d7;

    static final private int _Arabic_tatweel = 0x5e0;

    static final private int _Arabic_teh = 0x5ca;

    static final private int _Arabic_tehmarbuta = 0x5c9;

    static final private int _Arabic_thal = 0x5d0;

    static final private int _Arabic_theh = 0x5cb;

    static final private int _Arabic_waw = 0x5e8;

    static final private int _Arabic_yeh = 0x5ea;

    static final private int _Arabic_zah = 0x5d8;

    static final private int _Arabic_zain = 0x5d2;

    static final private int _aring = 0x0e5;

    static final private int _Aring = 0x0c5;

    static final private int _asciicircum = 0x05e;

    static final private int _asciitilde = 0x07e;

    static final private int _asterisk = 0x02a;

    static final private int _at = 0x040;

    static final private int _atilde = 0x0e3;

    static final private int _Atilde = 0x0c3;

    static final private int _Attn_3270 = 0xFD0E;

    static final private int _AudibleBell_Enable = 0xFE7A;

    static final private int _b = 0x062;

    static final private int _B = 0x042;

    static final private int _backslash = 0x05c;

    static final private int _BackSpace = 0xFF08;

    static final private int _BackTab_3270 = 0xFD05;

    static final private int _ballotcross = 0xaf4;

    static final private int _bar = 0x07c;

    static final private int _Begin = 0xFF58;

    static final private int _blank = 0x9df;

    static final private int _botintegral = 0x8a5;

    static final private int _botleftparens = 0x8ac;

    static final private int _botleftsqbracket = 0x8a8;

    static final private int _botleftsummation = 0x8b2;

    static final private int _botrightparens = 0x8ae;

    static final private int _botrightsqbracket = 0x8aa;

    static final private int _botrightsummation = 0x8b6;

    static final private int _bott = 0x9f6;

    static final private int _botvertsummationconnector = 0x8b4;

    static final private int _BounceKeys_Enable = 0xFE74;

    static final private int _braceleft = 0x07b;

    static final private int _braceright = 0x07d;

    static final private int _bracketleft = 0x05b;

    static final private int _bracketright = 0x05d;

    static final private int _Break = 0xFF6B;

    static final private int _breve = 0x1a2;

    static final private int _brokenbar = 0x0a6;

    static final private int _Byelorussian_shortu = 0x6ae;

    static final private int _Byelorussian_SHORTU = 0x6be;

    static final private int _c = 0x063;

    static final private int _C = 0x043;

    static final private int _cabovedot = 0x2e5;

    static final private int _Cabovedot = 0x2c5;

    static final private int _cacute = 0x1e6;

    static final private int _Cacute = 0x1c6;

    static final private int _Cancel = 0xFF69;

    static final private int _Caps_Lock = 0xFFE5;

    static final private int _careof = 0xab8;

    static final private int _caret = 0xafc;

    static final private int _caron = 0x1b7;

    static final private int _ccaron = 0x1e8;

    static final private int _Ccaron = 0x1c8;

    static final private int _ccedilla = 0x0e7;

    static final private int _Ccedilla = 0x0c7;

    static final private int _ccircumflex = 0x2e6;

    static final private int _Ccircumflex = 0x2c6;

    static final private int _cedilla = 0x0b8;

    static final private int _cent = 0x0a2;

    static final private int _ChangeScreen_3270 = 0xFD19;

    static final private int _checkerboard = 0x9e1;

    static final private int _checkmark = 0xaf3;

    static final private int _circle = 0xbcf;

    static final private int _Clear = 0xFF0B;

    static final private int _club = 0xaec;

    static final private int _Codeinput = 0xFF37;

    static final private int _colon = 0x03a;

    static final private int _ColonSign = 0x20a1;

    static final private int _comma = 0x02c;

    static final private int _Control_L = 0xFFE3;

    static final private int _Control_R = 0xFFE4;

    static final private int _Copy_3270 = 0xFD15;

    static final private int _copyright = 0x0a9;

    static final private int _cr = 0x9e4;

    static final private int _crossinglines = 0x9ee;

    static final private int _CruzeiroSign = 0x20a2;

    static final private int _currency = 0x0a4;

    static final private int _cursor = 0xaff;

    static final private int _CursorBlink_3270 = 0xFD0F;

    static final private int _CursorSelect_3270 = 0xFD1C;

    static final private int _Cyrillic_a = 0x6c1;

    static final private int _Cyrillic_A = 0x6e1;

    static final private int _Cyrillic_be = 0x6c2;

    static final private int _Cyrillic_BE = 0x6e2;

    static final private int _Cyrillic_che = 0x6de;

    static final private int _Cyrillic_CHE = 0x6fe;

    static final private int _Cyrillic_de = 0x6c4;

    static final private int _Cyrillic_DE = 0x6e4;

    static final private int _Cyrillic_dzhe = 0x6af;

    static final private int _Cyrillic_DZHE = 0x6bf;

    static final private int _Cyrillic_e = 0x6dc;

    static final private int _Cyrillic_E = 0x6fc;

    static final private int _Cyrillic_ef = 0x6c6;

    static final private int _Cyrillic_EF = 0x6e6;

    static final private int _Cyrillic_el = 0x6cc;

    static final private int _Cyrillic_EL = 0x6ec;

    static final private int _Cyrillic_em = 0x6cd;

    static final private int _Cyrillic_EM = 0x6ed;

    static final private int _Cyrillic_en = 0x6ce;

    static final private int _Cyrillic_EN = 0x6ee;

    static final private int _Cyrillic_er = 0x6d2;

    static final private int _Cyrillic_ER = 0x6f2;

    static final private int _Cyrillic_es = 0x6d3;

    static final private int _Cyrillic_ES = 0x6f3;

    static final private int _Cyrillic_ghe = 0x6c7;

    static final private int _Cyrillic_GHE = 0x6e7;

    static final private int _Cyrillic_ha = 0x6c8;

    static final private int _Cyrillic_HA = 0x6e8;

    static final private int _Cyrillic_hardsign = 0x6df;

    static final private int _Cyrillic_HARDSIGN = 0x6ff;

    static final private int _Cyrillic_i = 0x6c9;

    static final private int _Cyrillic_I = 0x6e9;

    static final private int _Cyrillic_ie = 0x6c5;

    static final private int _Cyrillic_IE = 0x6e5;

    static final private int _Cyrillic_io = 0x6a3;

    static final private int _Cyrillic_IO = 0x6b3;

    static final private int _Cyrillic_je = 0x6a8;

    static final private int _Cyrillic_JE = 0x6b8;

    static final private int _Cyrillic_ka = 0x6cb;

    static final private int _Cyrillic_KA = 0x6eb;

    static final private int _Cyrillic_lje = 0x6a9;

    static final private int _Cyrillic_LJE = 0x6b9;

    static final private int _Cyrillic_nje = 0x6aa;

    static final private int _Cyrillic_NJE = 0x6ba;

    static final private int _Cyrillic_o = 0x6cf;

    static final private int _Cyrillic_O = 0x6ef;

    static final private int _Cyrillic_pe = 0x6d0;

    static final private int _Cyrillic_PE = 0x6f0;

    static final private int _Cyrillic_sha = 0x6db;

    static final private int _Cyrillic_SHA = 0x6fb;

    static final private int _Cyrillic_shcha = 0x6dd;

    static final private int _Cyrillic_SHCHA = 0x6fd;

    static final private int _Cyrillic_shorti = 0x6ca;

    static final private int _Cyrillic_SHORTI = 0x6ea;

    static final private int _Cyrillic_softsign = 0x6d8;

    static final private int _Cyrillic_SOFTSIGN = 0x6f8;

    static final private int _Cyrillic_te = 0x6d4;

    static final private int _Cyrillic_TE = 0x6f4;

    static final private int _Cyrillic_tse = 0x6c3;

    static final private int _Cyrillic_TSE = 0x6e3;

    static final private int _Cyrillic_u = 0x6d5;

    static final private int _Cyrillic_U = 0x6f5;

    static final private int _Cyrillic_ve = 0x6d7;

    static final private int _Cyrillic_VE = 0x6f7;

    static final private int _Cyrillic_ya = 0x6d1;

    static final private int _Cyrillic_YA = 0x6f1;

    static final private int _Cyrillic_yeru = 0x6d9;

    static final private int _Cyrillic_YERU = 0x6f9;

    static final private int _Cyrillic_yu = 0x6c0;

    static final private int _Cyrillic_YU = 0x6e0;

    static final private int _Cyrillic_ze = 0x6da;

    static final private int _Cyrillic_ZE = 0x6fa;

    static final private int _Cyrillic_zhe = 0x6d6;

    static final private int _Cyrillic_ZHE = 0x6f6;

    static final private int _d = 0x064;

    static final private int _D = 0x044;

    static final private int _dagger = 0xaf1;

    static final private int _dcaron = 0x1ef;

    static final private int _Dcaron = 0x1cf;

    static final private int _dead_abovedot = 0xFE56;

    static final private int _dead_abovering = 0xFE58;

    static final private int _dead_acute = 0xFE51;

    static final private int _dead_belowdot = 0xFE60;

    static final private int _dead_breve = 0xFE55;

    static final private int _dead_caron = 0xFE5A;

    static final private int _dead_cedilla = 0xFE5B;

    static final private int _dead_circumflex = 0xFE52;

    static final private int _dead_diaeresis = 0xFE57;

    static final private int _dead_doubleacute = 0xFE59;

    static final private int _dead_grave = 0xFE50;

    static final private int _dead_hook = 0xFE61;

    static final private int _dead_horn = 0xFE62;

    static final private int _dead_iota = 0xFE5D;

    static final private int _dead_macron = 0xFE54;

    static final private int _dead_ogonek = 0xFE5C;

    static final private int _dead_semivoiced_sound = 0xFE5F;

    static final private int _dead_tilde = 0xFE53;

    static final private int _dead_voiced_sound = 0xFE5E;

    static final private int _decimalpoint = 0xabd;

    static final private int _degree = 0x0b0;

    static final private int _Delete = 0xFFFF;

    static final private int _DeleteWord_3270 = 0xFD1A;

    static final private int _diaeresis = 0x0a8;

    static final private int _diamond = 0xaed;

    static final private int _digitspace = 0xaa5;

    static final private int _division = 0x0f7;

    static final private int _dollar = 0x024;

    static final private int _DongSign = 0x20ab;

    static final private int _doubbaselinedot = 0xaaf;

    static final private int _doubleacute = 0x1bd;

    static final private int _doubledagger = 0xaf2;

    static final private int _doublelowquotemark = 0xafe;

    static final private int _Down = 0xFF54;

    static final private int _downarrow = 0x8fe;

    static final private int _downcaret = 0xba8;

    static final private int _downshoe = 0xbd6;

    static final private int _downstile = 0xbc4;

    static final private int _downtack = 0xbc2;

    static final private int _dstroke = 0x1f0;

    static final private int _Dstroke = 0x1d0;

    static final private int _Duplicate_3270 = 0xFD01;

    static final private int _e = 0x065;

    static final private int _E = 0x045;

    static final private int _eabovedot = 0x3ec;

    static final private int _Eabovedot = 0x3cc;

    static final private int _eacute = 0x0e9;

    static final private int _Eacute = 0x0c9;

    static final private int _ecaron = 0x1ec;

    static final private int _Ecaron = 0x1cc;

    static final private int _ecircumflex = 0x0ea;

    static final private int _Ecircumflex = 0x0ca;

    static final private int _EcuSign = 0x20a0;

    static final private int _ediaeresis = 0x0eb;

    static final private int _Ediaeresis = 0x0cb;

    static final private int _egrave = 0x0e8;

    static final private int _Egrave = 0x0c8;

    static final private int _Eisu_Shift = 0xFF2F;

    static final private int _Eisu_toggle = 0xFF30;

    static final private int _ellipsis = 0xaae;

    static final private int _em3space = 0xaa3;

    static final private int _em4space = 0xaa4;

    static final private int _emacron = 0x3ba;

    static final private int _Emacron = 0x3aa;

    static final private int _emdash = 0xaa9;

    static final private int _emfilledcircle = 0xade;

    static final private int _emfilledrect = 0xadf;

    static final private int _emopencircle = 0xace;

    static final private int _emopenrectangle = 0xacf;

    static final private int _emspace = 0xaa1;

    static final private int _End = 0xFF57;

    static final private int _endash = 0xaaa;

    static final private int _enfilledcircbullet = 0xae6;

    static final private int _enfilledsqbullet = 0xae7;

    static final private int _eng = 0x3bf;

    static final private int _ENG = 0x3bd;

    static final private int _enopencircbullet = 0xae0;

    static final private int _enopensquarebullet = 0xae1;

    static final private int _enspace = 0xaa2;

    static final private int _Enter_3270 = 0xFD1E;

    static final private int _eogonek = 0x1ea;

    static final private int _Eogonek = 0x1ca;

    static final private int _equal = 0x03d;

    static final private int _EraseEOF_3270 = 0xFD06;

    static final private int _EraseInput_3270 = 0xFD07;

    static final private int _Escape = 0xFF1B;

    static final private int _eth = 0x0f0;

    static final private int _Eth = 0x0d0;

    static final private int _ETH = 0x0d0;

    static final private int _EuroSign = 0x20ac;

    static final private int _exclam = 0x021;

    static final private int _exclamdown = 0x0a1;

    static final private int _Execute = 0xFF62;

    static final private int _ExSelect_3270 = 0xFD1B;

    static final private int _f = 0x066;

    static final private int _F = 0x046;

    static final private int _F1 = 0xFFBE;

    static final private int _F10 = 0xFFC7;

    static final private int _F11 = 0xFFC8;

    static final private int _F12 = 0xFFC9;

    static final private int _F13 = 0xFFCA;

    static final private int _F14 = 0xFFCB;

    static final private int _F15 = 0xFFCC;

    static final private int _F16 = 0xFFCD;

    static final private int _F17 = 0xFFCE;

    static final private int _F18 = 0xFFCF;

    static final private int _F19 = 0xFFD0;

    static final private int _F2 = 0xFFBF;

    static final private int _F20 = 0xFFD1;

    static final private int _F21 = 0xFFD2;

    static final private int _F22 = 0xFFD3;

    static final private int _F23 = 0xFFD4;

    static final private int _F24 = 0xFFD5;

    static final private int _F25 = 0xFFD6;

    static final private int _F26 = 0xFFD7;

    static final private int _F27 = 0xFFD8;

    static final private int _F28 = 0xFFD9;

    static final private int _F29 = 0xFFDA;

    static final private int _F3 = 0xFFC0;

    static final private int _F30 = 0xFFDB;

    static final private int _F31 = 0xFFDC;

    static final private int _F32 = 0xFFDD;

    static final private int _F33 = 0xFFDE;

    static final private int _F34 = 0xFFDF;

    static final private int _F35 = 0xFFE0;

    static final private int _F4 = 0xFFC1;

    static final private int _F5 = 0xFFC2;

    static final private int _F6 = 0xFFC3;

    static final private int _F7 = 0xFFC4;

    static final private int _F8 = 0xFFC5;

    static final private int _F9 = 0xFFC6;

    static final private int _femalesymbol = 0xaf8;

    static final private int _ff = 0x9e3;

    static final private int _FFrancSign = 0x20a3;

    static final private int _FieldMark_3270 = 0xFD02;

    static final private int _figdash = 0xabb;

    static final private int _filledlefttribullet = 0xadc;

    static final private int _filledrectbullet = 0xadb;

    static final private int _filledrighttribullet = 0xadd;

    static final private int _filledtribulletdown = 0xae9;

    static final private int _filledtribulletup = 0xae8;

    static final private int _Find = 0xFF68;

    static final private int _First_Virtual_Screen = 0xFED0;

    static final private int _fiveeighths = 0xac5;

    static final private int _fivesixths = 0xab7;

    static final private int _fourfifths = 0xab5;

    static final private int _function = 0x8f6;

    static final private int _g = 0x067;

    static final private int _G = 0x047;

    static final private int _gabovedot = 0x2f5;

    static final private int _Gabovedot = 0x2d5;

    static final private int _gbreve = 0x2bb;

    static final private int _Gbreve = 0x2ab;

    static final private int _gcedilla = 0x3bb;

    static final private int _Gcedilla = 0x3ab;

    static final private int _gcircumflex = 0x2f8;

    static final private int _Gcircumflex = 0x2d8;

    static final private int _grave = 0x060;

    static final private int _greater = 0x03e;

    static final private int _greaterthanequal = 0x8be;

    static final private int _Greek_accentdieresis = 0x7ae;

    static final private int _Greek_alpha = 0x7e1;

    static final private int _Greek_ALPHA = 0x7c1;

    static final private int _Greek_alphaaccent = 0x7b1;

    static final private int _Greek_ALPHAaccent = 0x7a1;

    static final private int _Greek_beta = 0x7e2;

    static final private int _Greek_BETA = 0x7c2;

    static final private int _Greek_chi = 0x7f7;

    static final private int _Greek_CHI = 0x7d7;

    static final private int _Greek_delta = 0x7e4;

    static final private int _Greek_DELTA = 0x7c4;

    static final private int _Greek_epsilon = 0x7e5;

    static final private int _Greek_EPSILON = 0x7c5;

    static final private int _Greek_epsilonaccent = 0x7b2;

    static final private int _Greek_EPSILONaccent = 0x7a2;

    static final private int _Greek_eta = 0x7e7;

    static final private int _Greek_ETA = 0x7c7;

    static final private int _Greek_etaaccent = 0x7b3;

    static final private int _Greek_ETAaccent = 0x7a3;

    static final private int _Greek_finalsmallsigma = 0x7f3;

    static final private int _Greek_gamma = 0x7e3;

    static final private int _Greek_GAMMA = 0x7c3;

    static final private int _Greek_horizbar = 0x7af;

    static final private int _Greek_iota = 0x7e9;

    static final private int _Greek_IOTA = 0x7c9;

    static final private int _Greek_iotaaccent = 0x7b4;

    static final private int _Greek_IOTAaccent = 0x7a4;

    static final private int _Greek_iotaaccentdieresis = 0x7b6;

    static final private int _Greek_IOTAdiaeresis = 0x7a5;

    static final private int _Greek_iotadieresis = 0x7b5;

    static final private int _Greek_kappa = 0x7ea;

    static final private int _Greek_KAPPA = 0x7ca;

    static final private int _Greek_lambda = 0x7eb;

    static final private int _Greek_LAMBDA = 0x7cb;

    static final private int _Greek_lamda = 0x7eb;

    static final private int _Greek_LAMDA = 0x7cb;

    static final private int _Greek_mu = 0x7ec;

    static final private int _Greek_MU = 0x7cc;

    static final private int _Greek_nu = 0x7ed;

    static final private int _Greek_NU = 0x7cd;

    static final private int _Greek_omega = 0x7f9;

    static final private int _Greek_OMEGA = 0x7d9;

    static final private int _Greek_omegaaccent = 0x7bb;

    static final private int _Greek_OMEGAaccent = 0x7ab;

    static final private int _Greek_omicron = 0x7ef;

    static final private int _Greek_OMICRON = 0x7cf;

    static final private int _Greek_omicronaccent = 0x7b7;

    static final private int _Greek_OMICRONaccent = 0x7a7;

    static final private int _Greek_phi = 0x7f6;

    static final private int _Greek_PHI = 0x7d6;

    static final private int _Greek_pi = 0x7f0;

    static final private int _Greek_PI = 0x7d0;

    static final private int _Greek_psi = 0x7f8;

    static final private int _Greek_PSI = 0x7d8;

    static final private int _Greek_rho = 0x7f1;

    static final private int _Greek_RHO = 0x7d1;

    static final private int _Greek_sigma = 0x7f2;

    static final private int _Greek_SIGMA = 0x7d2;

    static final private int _Greek_switch = 0xFF7E;

    static final private int _Greek_tau = 0x7f4;

    static final private int _Greek_TAU = 0x7d4;

    static final private int _Greek_theta = 0x7e8;

    static final private int _Greek_THETA = 0x7c8;

    static final private int _Greek_upsilon = 0x7f5;

    static final private int _Greek_UPSILON = 0x7d5;

    static final private int _Greek_upsilonaccent = 0x7b8;

    static final private int _Greek_UPSILONaccent = 0x7a8;

    static final private int _Greek_upsilonaccentdieresis = 0x7ba;

    static final private int _Greek_upsilondieresis = 0x7b9;

    static final private int _Greek_UPSILONdieresis = 0x7a9;

    static final private int _Greek_xi = 0x7ee;

    static final private int _Greek_XI = 0x7ce;

    static final private int _Greek_zeta = 0x7e6;

    static final private int _Greek_ZETA = 0x7c6;

    static final private int _guillemotleft = 0x0ab;

    static final private int _guillemotright = 0x0bb;

    static final private int _h = 0x068;

    static final private int _H = 0x048;

    static final private int _hairspace = 0xaa8;

    static final private int _Hangul = 0xff31;

    static final private int _Hangul_A = 0xebf;

    static final private int _Hangul_AE = 0xec0;

    static final private int _Hangul_AraeA = 0xef6;

    static final private int _Hangul_AraeAE = 0xef7;

    static final private int _Hangul_Banja = 0xff39;

    static final private int _Hangul_Cieuc = 0xeba;

    static final private int _Hangul_Codeinput = 0xff37;

    static final private int _Hangul_Dikeud = 0xea7;

    static final private int _Hangul_E = 0xec4;

    static final private int _Hangul_End = 0xff33;

    static final private int _Hangul_EO = 0xec3;

    static final private int _Hangul_EU = 0xed1;

    static final private int _Hangul_Hanja = 0xff34;

    static final private int _Hangul_Hieuh = 0xebe;

    static final private int _Hangul_I = 0xed3;

    static final private int _Hangul_Ieung = 0xeb7;

    static final private int _Hangul_J_Cieuc = 0xeea;

    static final private int _Hangul_J_Dikeud = 0xeda;

    static final private int _Hangul_J_Hieuh = 0xeee;

    static final private int _Hangul_J_Ieung = 0xee8;

    static final private int _Hangul_J_Jieuj = 0xee9;

    static final private int _Hangul_J_Khieuq = 0xeeb;

    static final private int _Hangul_J_Kiyeog = 0xed4;

    static final private int _Hangul_J_KiyeogSios = 0xed6;

    static final private int _Hangul_J_KkogjiDalrinIeung = 0xef9;

    static final private int _Hangul_J_Mieum = 0xee3;

    static final private int _Hangul_J_Nieun = 0xed7;

    static final private int _Hangul_J_NieunHieuh = 0xed9;

    static final private int _Hangul_J_NieunJieuj = 0xed8;

    static final private int _Hangul_J_PanSios = 0xef8;

    static final private int _Hangul_J_Phieuf = 0xeed;

    static final private int _Hangul_J_Pieub = 0xee4;

    static final private int _Hangul_J_PieubSios = 0xee5;

    static final private int _Hangul_J_Rieul = 0xedb;

    static final private int _Hangul_J_RieulHieuh = 0xee2;

    static final private int _Hangul_J_RieulKiyeog = 0xedc;

    static final private int _Hangul_J_RieulMieum = 0xedd;

    static final private int _Hangul_J_RieulPhieuf = 0xee1;

    static final private int _Hangul_J_RieulPieub = 0xede;

    static final private int _Hangul_J_RieulSios = 0xedf;

    static final private int _Hangul_J_RieulTieut = 0xee0;

    static final private int _Hangul_J_Sios = 0xee6;

    static final private int _Hangul_J_SsangKiyeog = 0xed5;

    static final private int _Hangul_J_SsangSios = 0xee7;

    static final private int _Hangul_J_Tieut = 0xeec;

    static final private int _Hangul_J_YeorinHieuh = 0xefa;

    static final private int _Hangul_Jamo = 0xff35;

    static final private int _Hangul_Jeonja = 0xff38;

    static final private int _Hangul_Jieuj = 0xeb8;

    static final private int _Hangul_Khieuq = 0xebb;

    static final private int _Hangul_Kiyeog = 0xea1;

    static final private int _Hangul_KiyeogSios = 0xea3;

    static final private int _Hangul_KkogjiDalrinIeung = 0xef3;

    static final private int _Hangul_Mieum = 0xeb1;

    static final private int _Hangul_MultipleCandidate = 0xff3d;

    static final private int _Hangul_Nieun = 0xea4;

    static final private int _Hangul_NieunHieuh = 0xea6;

    static final private int _Hangul_NieunJieuj = 0xea5;

    static final private int _Hangul_O = 0xec7;

    static final private int _Hangul_OE = 0xeca;

    static final private int _Hangul_PanSios = 0xef2;

    static final private int _Hangul_Phieuf = 0xebd;

    static final private int _Hangul_Pieub = 0xeb2;

    static final private int _Hangul_PieubSios = 0xeb4;

    static final private int _Hangul_PostHanja = 0xff3b;

    static final private int _Hangul_PreHanja = 0xff3a;

    static final private int _Hangul_PreviousCandidate = 0xff3e;

    static final private int _Hangul_Rieul = 0xea9;

    static final private int _Hangul_RieulHieuh = 0xeb0;

    static final private int _Hangul_RieulKiyeog = 0xeaa;

    static final private int _Hangul_RieulMieum = 0xeab;

    static final private int _Hangul_RieulPhieuf = 0xeaf;

    static final private int _Hangul_RieulPieub = 0xeac;

    static final private int _Hangul_RieulSios = 0xead;

    static final private int _Hangul_RieulTieut = 0xeae;

    static final private int _Hangul_RieulYeorinHieuh = 0xeef;

    static final private int _Hangul_Romaja = 0xff36;

    static final private int _Hangul_SingleCandidate = 0xff3c;

    static final private int _Hangul_Sios = 0xeb5;

    static final private int _Hangul_Special = 0xff3f;

    static final private int _Hangul_SsangDikeud = 0xea8;

    static final private int _Hangul_SsangJieuj = 0xeb9;

    static final private int _Hangul_SsangKiyeog = 0xea2;

    static final private int _Hangul_SsangPieub = 0xeb3;

    static final private int _Hangul_SsangSios = 0xeb6;

    static final private int _Hangul_Start = 0xff32;

    static final private int _Hangul_SunkyeongeumMieum = 0xef0;

    static final private int _Hangul_SunkyeongeumPhieuf = 0xef4;

    static final private int _Hangul_SunkyeongeumPieub = 0xef1;

    static final private int _Hangul_switch = 0xFF7E;

    static final private int _Hangul_Tieut = 0xebc;

    static final private int _Hangul_U = 0xecc;

    static final private int _Hangul_WA = 0xec8;

    static final private int _Hangul_WAE = 0xec9;

    static final private int _Hangul_WE = 0xece;

    static final private int _Hangul_WEO = 0xecd;

    static final private int _Hangul_WI = 0xecf;

    static final private int _Hangul_YA = 0xec1;

    static final private int _Hangul_YAE = 0xec2;

    static final private int _Hangul_YE = 0xec6;

    static final private int _Hangul_YEO = 0xec5;

    static final private int _Hangul_YeorinHieuh = 0xef5;

    static final private int _Hangul_YI = 0xed2;

    static final private int _Hangul_YO = 0xecb;

    static final private int _Hangul_YU = 0xed0;

    static final private int _Hankaku = 0xFF29;

    static final private int _hcircumflex = 0x2b6;

    static final private int _Hcircumflex = 0x2a6;

    static final private int _heart = 0xaee;

    static final private int _hebrew_aleph = 0xce0;

    static final private int _hebrew_ayin = 0xcf2;

    static final private int _hebrew_bet = 0xce1;

    static final private int _hebrew_beth = 0xce1;

    static final private int _hebrew_chet = 0xce7;

    static final private int _hebrew_dalet = 0xce3;

    static final private int _hebrew_daleth = 0xce3;

    static final private int _hebrew_doublelowline = 0xcdf;

    static final private int _hebrew_finalkaph = 0xcea;

    static final private int _hebrew_finalmem = 0xced;

    static final private int _hebrew_finalnun = 0xcef;

    static final private int _hebrew_finalpe = 0xcf3;

    static final private int _hebrew_finalzade = 0xcf5;

    static final private int _hebrew_finalzadi = 0xcf5;

    static final private int _hebrew_gimel = 0xce2;

    static final private int _hebrew_gimmel = 0xce2;

    static final private int _hebrew_he = 0xce4;

    static final private int _hebrew_het = 0xce7;

    static final private int _hebrew_kaph = 0xceb;

    static final private int _hebrew_kuf = 0xcf7;

    static final private int _hebrew_lamed = 0xcec;

    static final private int _hebrew_mem = 0xcee;

    static final private int _hebrew_nun = 0xcf0;

    static final private int _hebrew_pe = 0xcf4;

    static final private int _hebrew_qoph = 0xcf7;

    static final private int _hebrew_resh = 0xcf8;

    static final private int _hebrew_samech = 0xcf1;

    static final private int _hebrew_samekh = 0xcf1;

    static final private int _hebrew_shin = 0xcf9;

    static final private int _Hebrew_switch = 0xFF7E;

    static final private int _hebrew_taf = 0xcfa;

    static final private int _hebrew_taw = 0xcfa;

    static final private int _hebrew_tet = 0xce8;

    static final private int _hebrew_teth = 0xce8;

    static final private int _hebrew_waw = 0xce5;

    static final private int _hebrew_yod = 0xce9;

    static final private int _hebrew_zade = 0xcf6;

    static final private int _hebrew_zadi = 0xcf6;

    static final private int _hebrew_zain = 0xce6;

    static final private int _hebrew_zayin = 0xce6;

    static final private int _Help = 0xFF6A;

    static final private int _Henkan = 0xFF23;

    static final private int _Henkan_Mode = 0xFF23;

    static final private int _hexagram = 0xada;

    static final private int _Hiragana = 0xFF25;

    static final private int _Hiragana_Katakana = 0xFF27;

    static final private int _Home = 0xFF50;

    static final private int _horizconnector = 0x8a3;

    static final private int _horizlinescan1 = 0x9ef;

    static final private int _horizlinescan3 = 0x9f0;

    static final private int _horizlinescan5 = 0x9f1;

    static final private int _horizlinescan7 = 0x9f2;

    static final private int _horizlinescan9 = 0x9f3;

    static final private int _hstroke = 0x2b1;

    static final private int _Hstroke = 0x2a1;

    static final private int _ht = 0x9e2;

    static final private int _Hyper_L = 0xFFED;

    static final private int _Hyper_R = 0xFFEE;

    static final private int _hyphen = 0x0ad;

    static final private int _i = 0x069;

    static final private int _I = 0x049;

    static final private int _Iabovedot = 0x2a9;

    static final private int _iacute = 0x0ed;

    static final private int _Iacute = 0x0cd;

    static final private int _icircumflex = 0x0ee;

    static final private int _Icircumflex = 0x0ce;

    static final private int _Ident_3270 = 0xFD13;

    static final private int _identical = 0x8cf;

    static final private int _idiaeresis = 0x0ef;

    static final private int _Idiaeresis = 0x0cf;

    static final private int _idotless = 0x2b9;

    static final private int _ifonlyif = 0x8cd;

    static final private int _igrave = 0x0ec;

    static final private int _Igrave = 0x0cc;

    static final private int _imacron = 0x3ef;

    static final private int _Imacron = 0x3cf;

    static final private int _implies = 0x8ce;

    static final private int _includedin = 0x8da;

    static final private int _includes = 0x8db;

    static final private int _infinity = 0x8c2;

    static final private int _Insert = 0xFF63;

    static final private int _integral = 0x8bf;

    static final private int _intersection = 0x8dc;

    static final private int _iogonek = 0x3e7;

    static final private int _Iogonek = 0x3c7;

    static final private int _ISO_Center_Object = 0xFE33;

    static final private int _ISO_Continuous_Underline = 0xFE30;

    static final private int _ISO_Discontinuous_Underline = 0xFE31;

    static final private int _ISO_Emphasize = 0xFE32;

    static final private int _ISO_Enter = 0xFE34;

    static final private int _ISO_Fast_Cursor_Down = 0xFE2F;

    static final private int _ISO_Fast_Cursor_Left = 0xFE2C;

    static final private int _ISO_Fast_Cursor_Right = 0xFE2D;

    static final private int _ISO_Fast_Cursor_Up = 0xFE2E;

    static final private int _ISO_First_Group = 0xFE0C;

    static final private int _ISO_First_Group_Lock = 0xFE0D;

    static final private int _ISO_Group_Latch = 0xFE06;

    static final private int _ISO_Group_Lock = 0xFE07;

    static final private int _ISO_Group_Shift = 0xFF7E;

    static final private int _ISO_Last_Group = 0xFE0E;

    static final private int _ISO_Last_Group_Lock = 0xFE0F;

    static final private int _ISO_Left_Tab = 0xFE20;

    static final private int _ISO_Level2_Latch = 0xFE02;

    static final private int _ISO_Level3_Latch = 0xFE04;

    static final private int _ISO_Level3_Lock = 0xFE05;

    static final private int _ISO_Level3_Shift = 0xFE03;

    static final private int _ISO_Lock = 0xFE01;

    static final private int _ISO_Move_Line_Down = 0xFE22;

    static final private int _ISO_Move_Line_Up = 0xFE21;

    static final private int _ISO_Next_Group = 0xFE08;

    static final private int _ISO_Next_Group_Lock = 0xFE09;

    static final private int _ISO_Partial_Line_Down = 0xFE24;

    static final private int _ISO_Partial_Line_Up = 0xFE23;

    static final private int _ISO_Partial_Space_Left = 0xFE25;

    static final private int _ISO_Partial_Space_Right = 0xFE26;

    static final private int _ISO_Prev_Group = 0xFE0A;

    static final private int _ISO_Prev_Group_Lock = 0xFE0B;

    static final private int _ISO_Release_Both_Margins = 0xFE2B;

    static final private int _ISO_Release_Margin_Left = 0xFE29;

    static final private int _ISO_Release_Margin_Right = 0xFE2A;

    static final private int _ISO_Set_Margin_Left = 0xFE27;

    static final private int _ISO_Set_Margin_Right = 0xFE28;

    static final private int _itilde = 0x3b5;

    static final private int _Itilde = 0x3a5;

    static final private int _j = 0x06a;

    static final private int _J = 0x04a;

    static final private int _jcircumflex = 0x2bc;

    static final private int _Jcircumflex = 0x2ac;

    static final private int _jot = 0xbca;

    static final private int _Jump_3270 = 0xFD12;

    static final private int _k = 0x06b;

    static final private int _K = 0x04b;

    static final private int _kana_a = 0x4a7;

    static final private int _kana_A = 0x4b1;

    static final private int _kana_CHI = 0x4c1;

    static final private int _kana_closingbracket = 0x4a3;

    static final private int _kana_comma = 0x4a4;

    static final private int _kana_conjunctive = 0x4a5;

    static final private int _kana_e = 0x4aa;

    static final private int _kana_E = 0x4b4;

    static final private int _kana_FU = 0x4cc;

    static final private int _kana_fullstop = 0x4a1;

    static final private int _kana_HA = 0x4ca;

    static final private int _kana_HE = 0x4cd;

    static final private int _kana_HI = 0x4cb;

    static final private int _kana_HO = 0x4ce;

    static final private int _kana_HU = 0x4cc;

    static final private int _kana_i = 0x4a8;

    static final private int _kana_I = 0x4b2;

    static final private int _kana_KA = 0x4b6;

    static final private int _kana_KE = 0x4b9;

    static final private int _kana_KI = 0x4b7;

    static final private int _kana_KO = 0x4ba;

    static final private int _kana_KU = 0x4b8;

    static final private int _Kana_Lock = 0xFF2D;

    static final private int _kana_MA = 0x4cf;

    static final private int _kana_ME = 0x4d2;

    static final private int _kana_MI = 0x4d0;

    static final private int _kana_middledot = 0x4a5;

    static final private int _kana_MO = 0x4d3;

    static final private int _kana_MU = 0x4d1;

    static final private int _kana_N = 0x4dd;

    static final private int _kana_NA = 0x4c5;

    static final private int _kana_NE = 0x4c8;

    static final private int _kana_NI = 0x4c6;

    static final private int _kana_NO = 0x4c9;

    static final private int _kana_NU = 0x4c7;

    static final private int _kana_o = 0x4ab;

    static final private int _kana_O = 0x4b5;

    static final private int _kana_openingbracket = 0x4a2;

    static final private int _kana_RA = 0x4d7;

    static final private int _kana_RE = 0x4da;

    static final private int _kana_RI = 0x4d8;

    static final private int _kana_RO = 0x4db;

    static final private int _kana_RU = 0x4d9;

    static final private int _kana_SA = 0x4bb;

    static final private int _kana_SE = 0x4be;

    static final private int _kana_SHI = 0x4bc;

    static final private int _Kana_Shift = 0xFF2E;

    static final private int _kana_SO = 0x4bf;

    static final private int _kana_SU = 0x4bd;

    static final private int _kana_switch = 0xFF7E;

    static final private int _kana_TA = 0x4c0;

    static final private int _kana_TE = 0x4c3;

    static final private int _kana_TI = 0x4c1;

    static final private int _kana_TO = 0x4c4;

    static final private int _kana_tsu = 0x4af;

    static final private int _kana_TSU = 0x4c2;

    static final private int _kana_tu = 0x4af;

    static final private int _kana_TU = 0x4c2;

    static final private int _kana_u = 0x4a9;

    static final private int _kana_U = 0x4b3;

    static final private int _kana_WA = 0x4dc;

    static final private int _kana_WO = 0x4a6;

    static final private int _kana_ya = 0x4ac;

    static final private int _kana_YA = 0x4d4;

    static final private int _kana_yo = 0x4ae;

    static final private int _kana_YO = 0x4d6;

    static final private int _kana_yu = 0x4ad;

    static final private int _kana_YU = 0x4d5;

    static final private int _Kanji = 0xFF21;

    static final private int _Kanji_Bangou = 0xFF37;

    static final private int _kappa = 0x3a2;

    static final private int _Katakana = 0xFF26;

    static final private int _kcedilla = 0x3f3;

    static final private int _Kcedilla = 0x3d3;

    static final private int _KeyClick_3270 = 0xFD11;

    static final private int _Korean_Won = 0xeff;

    static final private int _KP_0 = 0xFFB0;

    static final private int _KP_1 = 0xFFB1;

    static final private int _KP_2 = 0xFFB2;

    static final private int _KP_3 = 0xFFB3;

    static final private int _KP_4 = 0xFFB4;

    static final private int _KP_5 = 0xFFB5;

    static final private int _KP_6 = 0xFFB6;

    static final private int _KP_7 = 0xFFB7;

    static final private int _KP_8 = 0xFFB8;

    static final private int _KP_9 = 0xFFB9;

    static final private int _KP_Add = 0xFFAB;

    static final private int _KP_Begin = 0xFF9D;

    static final private int _KP_Decimal = 0xFFAE;

    static final private int _KP_Delete = 0xFF9F;

    static final private int _KP_Divide = 0xFFAF;

    static final private int _KP_Down = 0xFF99;

    static final private int _KP_End = 0xFF9C;

    static final private int _KP_Enter = 0xFF8D;

    static final private int _KP_Equal = 0xFFBD;

    static final private int _KP_F1 = 0xFF91;

    static final private int _KP_F2 = 0xFF92;

    static final private int _KP_F3 = 0xFF93;

    static final private int _KP_F4 = 0xFF94;

    static final private int _KP_Home = 0xFF95;

    static final private int _KP_Insert = 0xFF9E;

    static final private int _KP_Left = 0xFF96;

    static final private int _KP_Multiply = 0xFFAA;

    static final private int _KP_Next = 0xFF9B;

    static final private int _KP_Page_Down = 0xFF9B;

    static final private int _KP_Page_Up = 0xFF9A;

    static final private int _KP_Prior = 0xFF9A;

    static final private int _KP_Right = 0xFF98;

    static final private int _KP_Separator = 0xFFAC;

    static final private int _KP_Space = 0xFF80;

    static final private int _KP_Subtract = 0xFFAD;

    static final private int _KP_Tab = 0xFF89;

    static final private int _KP_Up = 0xFF97;

    static final private int _kra = 0x3a2;

    static final private int _l = 0x06c;

    static final private int _L = 0x04c;

    static final private int _L1 = 0xFFC8;

    static final private int _L10 = 0xFFD1;

    static final private int _L2 = 0xFFC9;

    static final private int _L3 = 0xFFCA;

    static final private int _L4 = 0xFFCB;

    static final private int _L5 = 0xFFCC;

    static final private int _L6 = 0xFFCD;

    static final private int _L7 = 0xFFCE;

    static final private int _L8 = 0xFFCF;

    static final private int _L9 = 0xFFD0;

    static final private int _lacute = 0x1e5;

    static final private int _Lacute = 0x1c5;

    static final private int _Last_Virtual_Screen = 0xFED4;

    static final private int _latincross = 0xad9;

    static final private int _lcaron = 0x1b5;

    static final private int _Lcaron = 0x1a5;

    static final private int _lcedilla = 0x3b6;

    static final private int _Lcedilla = 0x3a6;

    static final private int _Left = 0xFF51;

    static final private int _Left2_3270 = 0xFD04;

    static final private int _leftanglebracket = 0xabc;

    static final private int _leftarrow = 0x8fb;

    static final private int _leftcaret = 0xba3;

    static final private int _leftdoublequotemark = 0xad2;

    static final private int _leftmiddlecurlybrace = 0x8af;

    static final private int _leftopentriangle = 0xacc;

    static final private int _leftpointer = 0xaea;

    static final private int _leftradical = 0x8a1;

    static final private int _leftshoe = 0xbda;

    static final private int _leftsinglequotemark = 0xad0;

    static final private int _leftt = 0x9f4;

    static final private int _lefttack = 0xbdc;

    static final private int _less = 0x03c;

    static final private int _lessthanequal = 0x8bc;

    static final private int _lf = 0x9e5;

    static final private int _Linefeed = 0xFF0A;

    static final private int _LiraSign = 0x20a4;

    static final private int _logicaland = 0x8de;

    static final private int _logicalor = 0x8df;

    static final private int _lowleftcorner = 0x9ed;

    static final private int _lowrightcorner = 0x9ea;

    static final private int _lstroke = 0x1b3;

    static final private int _Lstroke = 0x1a3;

    static final private int _m = 0x06d;

    static final private int _M = 0x04d;

    static final private int _Macedonia_dse = 0x6a5;

    static final private int _Macedonia_DSE = 0x6b5;

    static final private int _Macedonia_gje = 0x6a2;

    static final private int _Macedonia_GJE = 0x6b2;

    static final private int _Macedonia_kje = 0x6ac;

    static final private int _Macedonia_KJE = 0x6bc;

    static final private int _macron = 0x0af;

    static final private int _Mae_Koho = 0xFF3E;

    static final private int _malesymbol = 0xaf7;

    static final private int _maltesecross = 0xaf0;

    static final private int _marker = 0xabf;

    static final private int _masculine = 0x0ba;

    static final private int _Massyo = 0xFF2C;

    static final private int _Menu = 0xFF67;

    static final private int _Meta_L = 0xFFE7;

    static final private int _Meta_R = 0xFFE8;

    static final private int _MillSign = 0x20a5;

    static final private int _minus = 0x02d;

    static final private int _minutes = 0xad6;

    static final private int _Mode_switch = 0xFF7E;

    static final private int _MouseKeys_Accel_Enable = 0xFE77;

    static final private int _MouseKeys_Enable = 0xFE76;

    static final private int _mu = 0x0b5;

    static final private int _Muhenkan = 0xFF22;

    static final private int _Multi_key = 0xFF20;

    static final private int _MultipleCandidate = 0xFF3D;

    static final private int _multiply = 0x0d7;

    static final private int _musicalflat = 0xaf6;

    static final private int _musicalsharp = 0xaf5;

    static final private int _n = 0x06e;

    static final private int _N = 0x04e;

    static final private int _nabla = 0x8c5;

    static final private int _nacute = 0x1f1;

    static final private int _Nacute = 0x1d1;

    static final private int _NairaSign = 0x20a6;

    static final private int _ncaron = 0x1f2;

    static final private int _Ncaron = 0x1d2;

    static final private int _ncedilla = 0x3f1;

    static final private int _Ncedilla = 0x3d1;

    static final private int _NewSheqelSign = 0x20aa;

    static final private int _Next = 0xFF56;

    static final private int _Next_Virtual_Screen = 0xFED2;

    static final private int _nl = 0x9e8;

    static final private int _nobreakspace = 0x0a0;

    static final private int _notequal = 0x8bd;

    static final private int _notsign = 0x0ac;

    static final private int _ntilde = 0x0f1;

    static final private int _Ntilde = 0x0d1;

    static final private int _num_0 = 0x030;

    static final private int _num_1 = 0x031;

    static final private int _num_2 = 0x032;

    static final private int _num_3 = 0x033;

    static final private int _num_4 = 0x034;

    static final private int _num_5 = 0x035;

    static final private int _num_6 = 0x036;

    static final private int _num_7 = 0x037;

    static final private int _num_8 = 0x038;

    static final private int _num_9 = 0x039;

    static final private int _Num_Lock = 0xFF7F;

    static final private int _numbersign = 0x023;

    static final private int _numerosign = 0x6b0;

    static final private int _o = 0x06f;

    static final private int _O = 0x04f;

    static final private int _oacute = 0x0f3;

    static final private int _Oacute = 0x0d3;

    static final private int _ocircumflex = 0x0f4;

    static final private int _Ocircumflex = 0x0d4;

    static final private int _odiaeresis = 0x0f6;

    static final private int _Odiaeresis = 0x0d6;

    static final private int _odoubleacute = 0x1f5;

    static final private int _Odoubleacute = 0x1d5;

    static final private int _oe = 0x13bd;

    static final private int _OE = 0x13bc;

    static final private int _ogonek = 0x1b2;

    static final private int _ograve = 0x0f2;

    static final private int _Ograve = 0x0d2;

    static final private int _omacron = 0x3f2;

    static final private int _Omacron = 0x3d2;

    static final private int _oneeighth = 0xac3;

    static final private int _onefifth = 0xab2;

    static final private int _onehalf = 0x0bd;

    static final private int _onequarter = 0x0bc;

    static final private int _onesixth = 0xab6;

    static final private int _onesuperior = 0x0b9;

    static final private int _onethird = 0xab0;

    static final private int _Ooblique = 0x0d8;

    static final private int _openrectbullet = 0xae2;

    static final private int _openstar = 0xae5;

    static final private int _opentribulletdown = 0xae4;

    static final private int _opentribulletup = 0xae3;

    static final private int _ordfeminine = 0x0aa;

    static final private int _oslash = 0x0f8;

    static final private int _otilde = 0x0f5;

    static final private int _Otilde = 0x0d5;

    static final private int _overbar = 0xbc0;

    static final private int _Overlay1_Enable = 0xFE78;

    static final private int _Overlay2_Enable = 0xFE79;

    static final private int _overline = 0x47e;

    static final private int _p = 0x070;

    static final private int _P = 0x050;

    static final private int _PA1_3270 = 0xFD0A;

    static final private int _PA2_3270 = 0xFD0B;

    static final private int _PA3_3270 = 0xFD0C;

    static final private int _Page_Down = 0xFF56;

    static final private int _Page_Up = 0xFF55;

    static final private int _paragraph = 0x0b6;

    static final private int _parenleft = 0x028;

    static final private int _parenright = 0x029;

    static final private int _partialderivative = 0x8ef;

    static final private int _Pause = 0xFF13;

    static final private int _percent = 0x025;

    static final private int _period = 0x02e;

    static final private int _periodcentered = 0x0b7;

    static final private int _PesetaSign = 0x20a7;

    static final private int _phonographcopyright = 0xafb;

    static final private int _Play_3270 = 0xFD16;

    static final private int _plus = 0x02b;

    static final private int _plusminus = 0x0b1;

    static final private int _Pointer_Accelerate = 0xFEFA;

    static final private int _Pointer_Button_Dflt = 0xFEE8;

    static final private int _Pointer_Button1 = 0xFEE9;

    static final private int _Pointer_Button2 = 0xFEEA;

    static final private int _Pointer_Button3 = 0xFEEB;

    static final private int _Pointer_Button4 = 0xFEEC;

    static final private int _Pointer_Button5 = 0xFEED;

    static final private int _Pointer_DblClick_Dflt = 0xFEEE;

    static final private int _Pointer_DblClick1 = 0xFEEF;

    static final private int _Pointer_DblClick2 = 0xFEF0;

    static final private int _Pointer_DblClick3 = 0xFEF1;

    static final private int _Pointer_DblClick4 = 0xFEF2;

    static final private int _Pointer_DblClick5 = 0xFEF3;

    static final private int _Pointer_DfltBtnNext = 0xFEFB;

    static final private int _Pointer_DfltBtnPrev = 0xFEFC;

    static final private int _Pointer_Down = 0xFEE3;

    static final private int _Pointer_DownLeft = 0xFEE6;

    static final private int _Pointer_DownRight = 0xFEE7;

    static final private int _Pointer_Drag_Dflt = 0xFEF4;

    static final private int _Pointer_Drag1 = 0xFEF5;

    static final private int _Pointer_Drag2 = 0xFEF6;

    static final private int _Pointer_Drag3 = 0xFEF7;

    static final private int _Pointer_Drag4 = 0xFEF8;

    static final private int _Pointer_Drag5 = 0xFEFD;

    static final private int _Pointer_EnableKeys = 0xFEF9;

    static final private int _Pointer_Left = 0xFEE0;

    static final private int _Pointer_Right = 0xFEE1;

    static final private int _Pointer_Up = 0xFEE2;

    static final private int _Pointer_UpLeft = 0xFEE4;

    static final private int _Pointer_UpRight = 0xFEE5;

    static final private int _prescription = 0xad4;

    static final private int _Prev_Virtual_Screen = 0xFED1;

    static final private int _PreviousCandidate = 0xFF3E;

    static final private int _Print = 0xFF61;

    static final private int _PrintScreen_3270 = 0xFD1D;

    static final private int _Prior = 0xFF55;

    static final private int _prolongedsound = 0x4b0;

    static final private int _punctspace = 0xaa6;

    static final private int _q = 0x071;

    static final private int _Q = 0x051;

    static final private int _quad = 0xbcc;

    static final private int _question = 0x03f;

    static final private int _questiondown = 0x0bf;

    static final private int _Quit_3270 = 0xFD09;

    static final private int _quotedbl = 0x022;

    static final private int _quoteleft = 0x060;

    static final private int _quoteright = 0x027;

    static final private int _r = 0x072;

    static final private int _R = 0x052;

    static final private int _R1 = 0xFFD2;

    static final private int _R10 = 0xFFDB;

    static final private int _R11 = 0xFFDC;

    static final private int _R12 = 0xFFDD;

    static final private int _R13 = 0xFFDE;

    static final private int _R14 = 0xFFDF;

    static final private int _R15 = 0xFFE0;

    static final private int _R2 = 0xFFD3;

    static final private int _R3 = 0xFFD4;

    static final private int _R4 = 0xFFD5;

    static final private int _R5 = 0xFFD6;

    static final private int _R6 = 0xFFD7;

    static final private int _R7 = 0xFFD8;

    static final private int _R8 = 0xFFD9;

    static final private int _R9 = 0xFFDA;

    static final private int _racute = 0x1e0;

    static final private int _Racute = 0x1c0;

    static final private int _radical = 0x8d6;

    static final private int _rcaron = 0x1f8;

    static final private int _Rcaron = 0x1d8;

    static final private int _rcedilla = 0x3b3;

    static final private int _Rcedilla = 0x3a3;

    static final private int _Record_3270 = 0xFD18;

    static final private int _Redo = 0xFF66;

    static final private int _registered = 0x0ae;

    static final private int _RepeatKeys_Enable = 0xFE72;

    static final private int _Reset_3270 = 0xFD08;

    static final private int _Return = 0xFF0D;

    static final private int _Right = 0xFF53;

    static final private int _Right2_3270 = 0xFD03;

    static final private int _rightanglebracket = 0xabe;

    static final private int _rightarrow = 0x8fd;

    static final private int _rightcaret = 0xba6;

    static final private int _rightdoublequotemark = 0xad3;

    static final private int _rightmiddlecurlybrace = 0x8b0;

    static final private int _rightmiddlesummation = 0x8b7;

    static final private int _rightopentriangle = 0xacd;

    static final private int _rightpointer = 0xaeb;

    static final private int _rightshoe = 0xbd8;

    static final private int _rightsinglequotemark = 0xad1;

    static final private int _rightt = 0x9f5;

    static final private int _righttack = 0xbfc;

    static final private int _Romaji = 0xFF24;

    static final private int _Rule_3270 = 0xFD14;

    static final private int _RupeeSign = 0x20a8;

    static final private int _s = 0x073;

    static final private int _S = 0x053;

    static final private int _sacute = 0x1b6;

    static final private int _Sacute = 0x1a6;

    static final private int _scaron = 0x1b9;

    static final private int _Scaron = 0x1a9;

    static final private int _scedilla = 0x1ba;

    static final private int _Scedilla = 0x1aa;

    static final private int _scircumflex = 0x2fe;

    static final private int _Scircumflex = 0x2de;

    static final private int _script_switch = 0xFF7E;

    static final private int _Scroll_Lock = 0xFF14;

    static final private int _seconds = 0xad7;

    static final private int _section = 0x0a7;

    static final private int _Select = 0xFF60;

    static final private int _semicolon = 0x03b;

    static final private int _semivoicedsound = 0x4df;

    static final private int _Serbian_dje = 0x6a1;

    static final private int _Serbian_DJE = 0x6b1;

    static final private int _Serbian_dze = 0x6af;

    static final private int _Serbian_DZE = 0x6bf;

    static final private int _Serbian_je = 0x6a8;

    static final private int _Serbian_JE = 0x6b8;

    static final private int _Serbian_lje = 0x6a9;

    static final private int _Serbian_LJE = 0x6b9;

    static final private int _Serbian_nje = 0x6aa;

    static final private int _Serbian_NJE = 0x6ba;

    static final private int _Serbian_tshe = 0x6ab;

    static final private int _Serbian_TSHE = 0x6bb;

    static final private int _Setup_3270 = 0xFD17;

    static final private int _seveneighths = 0xac6;

    static final private int _Shift_L = 0xFFE1;

    static final private int _Shift_Lock = 0xFFE6;

    static final private int _Shift_R = 0xFFE2;

    static final private int _signaturemark = 0xaca;

    static final private int _signifblank = 0xaac;

    static final private int _similarequal = 0x8c9;

    static final private int _SingleCandidate = 0xFF3C;

    static final private int _singlelowquotemark = 0xafd;

    static final private int _slash = 0x02f;

    static final private int _SlowKeys_Enable = 0xFE73;

    static final private int _soliddiamond = 0x9e0;

    static final private int _space = 0x020;

    static final private int _ssharp = 0x0df;

    static final private int _sterling = 0x0a3;

    static final private int _StickyKeys_Enable = 0xFE75;

    static final private int _Super_L = 0xFFEB;

    static final private int _Super_R = 0xFFEC;

    static final private int _Sys_Req = 0xFF15;

    static final private int _t = 0x074;

    static final private int _T = 0x054;

    static final private int _Tab = 0xFF09;

    static final private int _tcaron = 0x1bb;

    static final private int _Tcaron = 0x1ab;

    static final private int _tcedilla = 0x1fe;

    static final private int _Tcedilla = 0x1de;

    static final private int _telephone = 0xaf9;

    static final private int _telephonerecorder = 0xafa;

    static final private int _Terminate_Server = 0xFED5;

    static final private int _Test_3270 = 0xFD0D;

    static final private int _Thai_baht = 0xddf;

    static final private int _Thai_bobaimai = 0xdba;

    static final private int _Thai_chochan = 0xda8;

    static final private int _Thai_chochang = 0xdaa;

    static final private int _Thai_choching = 0xda9;

    static final private int _Thai_chochoe = 0xdac;

    static final private int _Thai_dochada = 0xdae;

    static final private int _Thai_dodek = 0xdb4;

    static final private int _Thai_fofa = 0xdbd;

    static final private int _Thai_fofan = 0xdbf;

    static final private int _Thai_hohip = 0xdcb;

    static final private int _Thai_honokhuk = 0xdce;

    static final private int _Thai_khokhai = 0xda2;

    static final private int _Thai_khokhon = 0xda5;

    static final private int _Thai_khokhuat = 0xda3;

    static final private int _Thai_khokhwai = 0xda4;

    static final private int _Thai_khorakhang = 0xda6;

    static final private int _Thai_kokai = 0xda1;

    static final private int _Thai_lakkhangyao = 0xde5;

    static final private int _Thai_lekchet = 0xdf7;

    static final private int _Thai_lekha = 0xdf5;

    static final private int _Thai_lekhok = 0xdf6;

    static final private int _Thai_lekkao = 0xdf9;

    static final private int _Thai_leknung = 0xdf1;

    static final private int _Thai_lekpaet = 0xdf8;

    static final private int _Thai_leksam = 0xdf3;

    static final private int _Thai_leksi = 0xdf4;

    static final private int _Thai_leksong = 0xdf2;

    static final private int _Thai_leksun = 0xdf0;

    static final private int _Thai_lochula = 0xdcc;

    static final private int _Thai_loling = 0xdc5;

    static final private int _Thai_lu = 0xdc6;

    static final private int _Thai_maichattawa = 0xdeb;

    static final private int _Thai_maiek = 0xde8;

    static final private int _Thai_maihanakat = 0xdd1;

    static final private int _Thai_maihanakat_maitho = 0xdde;

    static final private int _Thai_maitaikhu = 0xde7;

    static final private int _Thai_maitho = 0xde9;

    static final private int _Thai_maitri = 0xdea;

    static final private int _Thai_maiyamok = 0xde6;

    static final private int _Thai_moma = 0xdc1;

    static final private int _Thai_ngongu = 0xda7;

    static final private int _Thai_nikhahit = 0xded;

    static final private int _Thai_nonen = 0xdb3;

    static final private int _Thai_nonu = 0xdb9;

    static final private int _Thai_oang = 0xdcd;

    static final private int _Thai_paiyannoi = 0xdcf;

    static final private int _Thai_phinthu = 0xdda;

    static final private int _Thai_phophan = 0xdbe;

    static final private int _Thai_phophung = 0xdbc;

    static final private int _Thai_phosamphao = 0xdc0;

    static final private int _Thai_popla = 0xdbb;

    static final private int _Thai_rorua = 0xdc3;

    static final private int _Thai_ru = 0xdc4;

    static final private int _Thai_saraa = 0xdd0;

    static final private int _Thai_saraaa = 0xdd2;

    static final private int _Thai_saraae = 0xde1;

    static final private int _Thai_saraaimaimalai = 0xde4;

    static final private int _Thai_saraaimaimuan = 0xde3;

    static final private int _Thai_saraam = 0xdd3;

    static final private int _Thai_sarae = 0xde0;

    static final private int _Thai_sarai = 0xdd4;

    static final private int _Thai_saraii = 0xdd5;

    static final private int _Thai_sarao = 0xde2;

    static final private int _Thai_sarau = 0xdd8;

    static final private int _Thai_saraue = 0xdd6;

    static final private int _Thai_sarauee = 0xdd7;

    static final private int _Thai_sarauu = 0xdd9;

    static final private int _Thai_sorusi = 0xdc9;

    static final private int _Thai_sosala = 0xdc8;

    static final private int _Thai_soso = 0xdab;

    static final private int _Thai_sosua = 0xdca;

    static final private int _Thai_thanthakhat = 0xdec;

    static final private int _Thai_thonangmontho = 0xdb1;

    static final private int _Thai_thophuthao = 0xdb2;

    static final private int _Thai_thothahan = 0xdb7;

    static final private int _Thai_thothan = 0xdb0;

    static final private int _Thai_thothong = 0xdb8;

    static final private int _Thai_thothung = 0xdb6;

    static final private int _Thai_topatak = 0xdaf;

    static final private int _Thai_totao = 0xdb5;

    static final private int _Thai_wowaen = 0xdc7;

    static final private int _Thai_yoyak = 0xdc2;

    static final private int _Thai_yoying = 0xdad;

    static final private int _therefore = 0x8c0;

    static final private int _thinspace = 0xaa7;

    static final private int _thorn = 0x0fe;

    static final private int _Thorn = 0x0de;

    static final private int _THORN = 0x0de;

    static final private int _threeeighths = 0xac4;

    static final private int _threefifths = 0xab4;

    static final private int _threequarters = 0x0be;

    static final private int _threesuperior = 0x0b3;

    static final private int _topintegral = 0x8a4;

    static final private int _topleftparens = 0x8ab;

    static final private int _topleftradical = 0x8a2;

    static final private int _topleftsqbracket = 0x8a7;

    static final private int _topleftsummation = 0x8b1;

    static final private int _toprightparens = 0x8ad;

    static final private int _toprightsqbracket = 0x8a9;

    static final private int _toprightsummation = 0x8b5;

    static final private int _topt = 0x9f7;

    static final private int _topvertsummationconnector = 0x8b3;

    static final private int _Touroku = 0xFF2B;

    static final private int _trademark = 0xac9;

    static final private int _trademarkincircle = 0xacb;

    static final private int _tslash = 0x3bc;

    static final private int _Tslash = 0x3ac;

    static final private int _twofifths = 0xab3;

    static final private int _twosuperior = 0x0b2;

    static final private int _twothirds = 0xab1;

    static final private int _u = 0x075;

    static final private int _U = 0x055;

    static final private int _uacute = 0x0fa;

    static final private int _Uacute = 0x0da;

    static final private int _ubreve = 0x2fd;

    static final private int _Ubreve = 0x2dd;

    static final private int _ucircumflex = 0x0fb;

    static final private int _Ucircumflex = 0x0db;

    static final private int _udiaeresis = 0x0fc;

    static final private int _Udiaeresis = 0x0dc;

    static final private int _udoubleacute = 0x1fb;

    static final private int _Udoubleacute = 0x1db;

    static final private int _ugrave = 0x0f9;

    static final private int _Ugrave = 0x0d9;

    static final private int _Ukrainian_i = 0x6a6;

    static final private int _Ukrainian_I = 0x6b6;

    static final private int _Ukrainian_ie = 0x6a4;

    static final private int _Ukrainian_IE = 0x6b4;

    static final private int _Ukrainian_yi = 0x6a7;

    static final private int _Ukrainian_YI = 0x6b7;

    static final private int _Ukranian_i = 0x6a6;

    static final private int _Ukranian_I = 0x6b6;

    static final private int _Ukranian_je = 0x6a4;

    static final private int _Ukranian_JE = 0x6b4;

    static final private int _Ukranian_yi = 0x6a7;

    static final private int _Ukranian_YI = 0x6b7;

    static final private int _umacron = 0x3fe;

    static final private int _Umacron = 0x3de;

    static final private int _underbar = 0xbc6;

    static final private int _underscore = 0x05f;

    static final private int _Undo = 0xFF65;

    static final private int _union = 0x8dd;

    static final private int _uogonek = 0x3f9;

    static final private int _Uogonek = 0x3d9;

    static final private int _Up = 0xFF52;

    static final private int _uparrow = 0x8fc;

    static final private int _upcaret = 0xba9;

    static final private int _upleftcorner = 0x9ec;

    static final private int _uprightcorner = 0x9eb;

    static final private int _upshoe = 0xbc3;

    static final private int _upstile = 0xbd3;

    static final private int _uptack = 0xbce;

    static final private int _uring = 0x1f9;

    static final private int _Uring = 0x1d9;

    static final private int _utilde = 0x3fd;

    static final private int _Utilde = 0x3dd;

    static final private int _v = 0x076;

    static final private int _V = 0x056;

    static final private int _variation = 0x8c1;

    static final private int _vertbar = 0x9f8;

    static final private int _vertconnector = 0x8a6;

    static final private int _voicedsound = 0x4de;

    static final private int _vt = 0x9e9;

    static final private int _w = 0x077;

    static final private int _W = 0x057;

    static final private int _WonSign = 0x20a9;

    static final private int _x = 0x078;

    static final private int _X = 0x058;

    static final private int _y = 0x079;

    static final private int _Y = 0x059;

    static final private int _yacute = 0x0fd;

    static final private int _Yacute = 0x0dd;

    static final private int _ydiaeresis = 0x0ff;

    static final private int _Ydiaeresis = 0x13be;

    static final private int _yen = 0x0a5;

    static final private int _z = 0x07a;

    static final private int _Z = 0x05a;

    static final private int _zabovedot = 0x1bf;

    static final private int _Zabovedot = 0x1af;

    static final private int _zacute = 0x1bc;

    static final private int _Zacute = 0x1ac;

    static final private int _zcaron = 0x1be;

    static final private int _Zcaron = 0x1ae;

    static final private int _Zen_Koho = 0xFF3D;

    static final private int _Zenkaku = 0xFF28;

    static final private int _Zenkaku_Hankaku = 0xFF2A;

    /**
     * Representation of a a key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol a = new KeySymbol(_a);

    /**
     * Representation of a A key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol A = new KeySymbol(_A);

    /**
     * Representation of a aacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol aacute = new KeySymbol(_aacute);

    /**
     * Representation of a Aacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Aacute = new KeySymbol(_Aacute);

    /**
     * Representation of a abovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol abovedot = new KeySymbol(_abovedot);

    /**
     * Representation of a abreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol abreve = new KeySymbol(_abreve);

    /**
     * Representation of a Abreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Abreve = new KeySymbol(_Abreve);

    /**
     * Representation of a AccessX_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol AccessX_Enable = new KeySymbol(
            _AccessX_Enable);

    /**
     * Representation of a AccessX_Feedback_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol AccessX_Feedback_Enable = new KeySymbol(
            _AccessX_Feedback_Enable);

    /**
     * Representation of a acircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol acircumflex = new KeySymbol(_acircumflex);

    /**
     * Representation of a Acircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Acircumflex = new KeySymbol(_Acircumflex);

    /**
     * Representation of a acute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol acute = new KeySymbol(_acute);

    /**
     * Representation of a adiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol adiaeresis = new KeySymbol(_adiaeresis);

    /**
     * Representation of a Adiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Adiaeresis = new KeySymbol(_Adiaeresis);

    /**
     * Representation of a ae key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ae = new KeySymbol(_ae);

    /**
     * Representation of a AE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol AE = new KeySymbol(_AE);

    /**
     * Representation of a agrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol agrave = new KeySymbol(_agrave);

    /**
     * Representation of a Agrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Agrave = new KeySymbol(_Agrave);

    /**
     * Representation of a Alt_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Alt_L = new KeySymbol(_Alt_L);

    /**
     * Representation of a Alt_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Alt_R = new KeySymbol(_Alt_R);

    /**
     * Representation of a AltCursor_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol AltCursor_3270 = new KeySymbol(
            _AltCursor_3270);

    /**
     * Representation of a amacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol amacron = new KeySymbol(_amacron);

    /**
     * Representation of a Amacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Amacron = new KeySymbol(_Amacron);

    /**
     * Representation of a ampersand key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ampersand = new KeySymbol(_ampersand);

    /**
     * Representation of a aogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol aogonek = new KeySymbol(_aogonek);

    /**
     * Representation of a Aogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Aogonek = new KeySymbol(_Aogonek);

    /**
     * Representation of a apostrophe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol apostrophe = new KeySymbol(_apostrophe);

    /**
     * Representation of a approximate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol approximate = new KeySymbol(_approximate);

    /**
     * Representation of a Arabic_ain key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_ain = new KeySymbol(_Arabic_ain);

    /**
     * Representation of a Arabic_alef key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_alef = new KeySymbol(_Arabic_alef);

    /**
     * Representation of a Arabic_alefmaksura key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_alefmaksura = new KeySymbol(
            _Arabic_alefmaksura);

    /**
     * Representation of a Arabic_beh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_beh = new KeySymbol(_Arabic_beh);

    /**
     * Representation of a Arabic_comma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_comma = new KeySymbol(_Arabic_comma);

    /**
     * Representation of a Arabic_dad key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_dad = new KeySymbol(_Arabic_dad);

    /**
     * Representation of a Arabic_dal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_dal = new KeySymbol(_Arabic_dal);

    /**
     * Representation of a Arabic_damma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_damma = new KeySymbol(_Arabic_damma);

    /**
     * Representation of a Arabic_dammatan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_dammatan = new KeySymbol(
            _Arabic_dammatan);

    /**
     * Representation of a Arabic_fatha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_fatha = new KeySymbol(_Arabic_fatha);

    /**
     * Representation of a Arabic_fathatan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_fathatan = new KeySymbol(
            _Arabic_fathatan);

    /**
     * Representation of a Arabic_feh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_feh = new KeySymbol(_Arabic_feh);

    /**
     * Representation of a Arabic_ghain key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_ghain = new KeySymbol(_Arabic_ghain);

    /**
     * Representation of a Arabic_ha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_ha = new KeySymbol(_Arabic_ha);

    /**
     * Representation of a Arabic_hah key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hah = new KeySymbol(_Arabic_hah);

    /**
     * Representation of a Arabic_hamza key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hamza = new KeySymbol(_Arabic_hamza);

    /**
     * Representation of a Arabic_hamzaonalef key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hamzaonalef = new KeySymbol(
            _Arabic_hamzaonalef);

    /**
     * Representation of a Arabic_hamzaonwaw key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hamzaonwaw = new KeySymbol(
            _Arabic_hamzaonwaw);

    /**
     * Representation of a Arabic_hamzaonyeh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hamzaonyeh = new KeySymbol(
            _Arabic_hamzaonyeh);

    /**
     * Representation of a Arabic_hamzaunderalef key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_hamzaunderalef = new KeySymbol(
            _Arabic_hamzaunderalef);

    /**
     * Representation of a Arabic_heh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_heh = new KeySymbol(_Arabic_heh);

    /**
     * Representation of a Arabic_jeem key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_jeem = new KeySymbol(_Arabic_jeem);

    /**
     * Representation of a Arabic_kaf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_kaf = new KeySymbol(_Arabic_kaf);

    /**
     * Representation of a Arabic_kasra key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_kasra = new KeySymbol(_Arabic_kasra);

    /**
     * Representation of a Arabic_kasratan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_kasratan = new KeySymbol(
            _Arabic_kasratan);

    /**
     * Representation of a Arabic_khah key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_khah = new KeySymbol(_Arabic_khah);

    /**
     * Representation of a Arabic_lam key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_lam = new KeySymbol(_Arabic_lam);

    /**
     * Representation of a Arabic_maddaonalef key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_maddaonalef = new KeySymbol(
            _Arabic_maddaonalef);

    /**
     * Representation of a Arabic_meem key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_meem = new KeySymbol(_Arabic_meem);

    /**
     * Representation of a Arabic_noon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_noon = new KeySymbol(_Arabic_noon);

    /**
     * Representation of a Arabic_qaf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_qaf = new KeySymbol(_Arabic_qaf);

    /**
     * Representation of a Arabic_question_mark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_question_mark = new KeySymbol(
            _Arabic_question_mark);

    /**
     * Representation of a Arabic_ra key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_ra = new KeySymbol(_Arabic_ra);

    /**
     * Representation of a Arabic_sad key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_sad = new KeySymbol(_Arabic_sad);

    /**
     * Representation of a Arabic_seen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_seen = new KeySymbol(_Arabic_seen);

    /**
     * Representation of a Arabic_semicolon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_semicolon = new KeySymbol(
            _Arabic_semicolon);

    /**
     * Representation of a Arabic_shadda key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_shadda = new KeySymbol(_Arabic_shadda);

    /**
     * Representation of a Arabic_sheen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_sheen = new KeySymbol(_Arabic_sheen);

    /**
     * Representation of a Arabic_sukun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_sukun = new KeySymbol(_Arabic_sukun);

    /**
     * Representation of a Arabic_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_switch = new KeySymbol(_Arabic_switch);

    /**
     * Representation of a Arabic_tah key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_tah = new KeySymbol(_Arabic_tah);

    /**
     * Representation of a Arabic_tatweel key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_tatweel = new KeySymbol(
            _Arabic_tatweel);

    /**
     * Representation of a Arabic_teh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_teh = new KeySymbol(_Arabic_teh);

    /**
     * Representation of a Arabic_tehmarbuta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_tehmarbuta = new KeySymbol(
            _Arabic_tehmarbuta);

    /**
     * Representation of a Arabic_thal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_thal = new KeySymbol(_Arabic_thal);

    /**
     * Representation of a Arabic_theh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_theh = new KeySymbol(_Arabic_theh);

    /**
     * Representation of a Arabic_waw key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_waw = new KeySymbol(_Arabic_waw);

    /**
     * Representation of a Arabic_yeh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_yeh = new KeySymbol(_Arabic_yeh);

    /**
     * Representation of a Arabic_zah key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_zah = new KeySymbol(_Arabic_zah);

    /**
     * Representation of a Arabic_zain key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Arabic_zain = new KeySymbol(_Arabic_zain);

    /**
     * Representation of a aring key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol aring = new KeySymbol(_aring);

    /**
     * Representation of a Aring key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Aring = new KeySymbol(_Aring);

    /**
     * Representation of a asciicircum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol asciicircum = new KeySymbol(_asciicircum);

    /**
     * Representation of a asciitilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol asciitilde = new KeySymbol(_asciitilde);

    /**
     * Representation of a asterisk key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol asterisk = new KeySymbol(_asterisk);

    /**
     * Representation of a at key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol at = new KeySymbol(_at);

    /**
     * Representation of a atilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol atilde = new KeySymbol(_atilde);

    /**
     * Representation of a Atilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Atilde = new KeySymbol(_Atilde);

    /**
     * Representation of a Attn_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Attn_3270 = new KeySymbol(_Attn_3270);

    /**
     * Representation of a AudibleBell_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol AudibleBell_Enable = new KeySymbol(
            _AudibleBell_Enable);

    /**
     * Representation of a b key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol b = new KeySymbol(_b);

    /**
     * Representation of a B key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol B = new KeySymbol(_B);

    /**
     * Representation of a backslash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol backslash = new KeySymbol(_backslash);

    /**
     * Representation of a BackSpace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol BackSpace = new KeySymbol(_BackSpace);

    /**
     * Representation of a BackTab_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol BackTab_3270 = new KeySymbol(_BackTab_3270);

    /**
     * Representation of a ballotcross key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ballotcross = new KeySymbol(_ballotcross);

    /**
     * Representation of a bar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol bar = new KeySymbol(_bar);

    /**
     * Representation of a Begin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Begin = new KeySymbol(_Begin);

    /**
     * Representation of a blank key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol blank = new KeySymbol(_blank);

    /**
     * Representation of a botintegral key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botintegral = new KeySymbol(_botintegral);

    /**
     * Representation of a botleftparens key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botleftparens = new KeySymbol(_botleftparens);

    /**
     * Representation of a botleftsqbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botleftsqbracket = new KeySymbol(
            _botleftsqbracket);

    /**
     * Representation of a botleftsummation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botleftsummation = new KeySymbol(
            _botleftsummation);

    /**
     * Representation of a botrightparens key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botrightparens = new KeySymbol(
            _botrightparens);

    /**
     * Representation of a botrightsqbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botrightsqbracket = new KeySymbol(
            _botrightsqbracket);

    /**
     * Representation of a botrightsummation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botrightsummation = new KeySymbol(
            _botrightsummation);

    /**
     * Representation of a bott key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol bott = new KeySymbol(_bott);

    /**
     * Representation of a botvertsummationconnector key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol botvertsummationconnector = new KeySymbol(
            _botvertsummationconnector);

    /**
     * Representation of a BounceKeys_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol BounceKeys_Enable = new KeySymbol(
            _BounceKeys_Enable);

    /**
     * Representation of a braceleft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol braceleft = new KeySymbol(_braceleft);

    /**
     * Representation of a braceright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol braceright = new KeySymbol(_braceright);

    /**
     * Representation of a bracketleft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol bracketleft = new KeySymbol(_bracketleft);

    /**
     * Representation of a bracketright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol bracketright = new KeySymbol(_bracketright);

    /**
     * Representation of a Break key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Break = new KeySymbol(_Break);

    /**
     * Representation of a breve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol breve = new KeySymbol(_breve);

    /**
     * Representation of a brokenbar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol brokenbar = new KeySymbol(_brokenbar);

    /**
     * Representation of a Byelorussian_shortu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Byelorussian_shortu = new KeySymbol(
            _Byelorussian_shortu);

    /**
     * Representation of a Byelorussian_SHORTU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Byelorussian_SHORTU = new KeySymbol(
            _Byelorussian_SHORTU);

    /**
     * Representation of a c key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol c = new KeySymbol(_c);

    /**
     * Representation of a C key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol C = new KeySymbol(_C);

    /**
     * Representation of a cabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cabovedot = new KeySymbol(_cabovedot);

    /**
     * Representation of a Cabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cabovedot = new KeySymbol(_Cabovedot);

    /**
     * Representation of a cacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cacute = new KeySymbol(_cacute);

    /**
     * Representation of a Cacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cacute = new KeySymbol(_Cacute);

    /**
     * Representation of a Cancel key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cancel = new KeySymbol(_Cancel);

    /**
     * Representation of a Caps_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Caps_Lock = new KeySymbol(_Caps_Lock);

    /**
     * Representation of a careof key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol careof = new KeySymbol(_careof);

    /**
     * Representation of a caret key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol caret = new KeySymbol(_caret);

    /**
     * Representation of a caron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol caron = new KeySymbol(_caron);

    /**
     * Representation of a ccaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ccaron = new KeySymbol(_ccaron);

    /**
     * Representation of a Ccaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ccaron = new KeySymbol(_Ccaron);

    /**
     * Representation of a ccedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ccedilla = new KeySymbol(_ccedilla);

    /**
     * Representation of a Ccedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ccedilla = new KeySymbol(_Ccedilla);

    /**
     * Representation of a ccircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ccircumflex = new KeySymbol(_ccircumflex);

    /**
     * Representation of a Ccircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ccircumflex = new KeySymbol(_Ccircumflex);

    /**
     * Representation of a cedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cedilla = new KeySymbol(_cedilla);

    /**
     * Representation of a cent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cent = new KeySymbol(_cent);

    /**
     * Representation of a ChangeScreen_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ChangeScreen_3270 = new KeySymbol(
            _ChangeScreen_3270);

    /**
     * Representation of a checkerboard key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol checkerboard = new KeySymbol(_checkerboard);

    /**
     * Representation of a checkmark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol checkmark = new KeySymbol(_checkmark);

    /**
     * Representation of a circle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol circle = new KeySymbol(_circle);

    /**
     * Representation of a Clear key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Clear = new KeySymbol(_Clear);

    /**
     * Representation of a club key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol club = new KeySymbol(_club);

    /**
     * Representation of a Codeinput key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Codeinput = new KeySymbol(_Codeinput);

    /**
     * Representation of a colon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol colon = new KeySymbol(_colon);

    /**
     * Representation of a ColonSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ColonSign = new KeySymbol(_ColonSign);

    /**
     * Representation of a comma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol comma = new KeySymbol(_comma);

    /**
     * Representation of a Control_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Control_L = new KeySymbol(_Control_L);

    /**
     * Representation of a Control_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Control_R = new KeySymbol(_Control_R);

    /**
     * Representation of a Copy_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Copy_3270 = new KeySymbol(_Copy_3270);

    /**
     * Representation of a copyright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol copyright = new KeySymbol(_copyright);

    /**
     * Representation of a cr key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cr = new KeySymbol(_cr);

    /**
     * Representation of a crossinglines key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol crossinglines = new KeySymbol(_crossinglines);

    /**
     * Representation of a CruzeiroSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol CruzeiroSign = new KeySymbol(_CruzeiroSign);

    /**
     * Representation of a currency key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol currency = new KeySymbol(_currency);

    /**
     * Representation of a cursor key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol cursor = new KeySymbol(_cursor);

    /**
     * Representation of a CursorBlink_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol CursorBlink_3270 = new KeySymbol(
            _CursorBlink_3270);

    /**
     * Representation of a CursorSelect_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol CursorSelect_3270 = new KeySymbol(
            _CursorSelect_3270);

    /**
     * Representation of a Cyrillic_a key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_a = new KeySymbol(_Cyrillic_a);

    /**
     * Representation of a Cyrillic_A key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_A = new KeySymbol(_Cyrillic_A);

    /**
     * Representation of a Cyrillic_be key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_be = new KeySymbol(_Cyrillic_be);

    /**
     * Representation of a Cyrillic_BE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_BE = new KeySymbol(_Cyrillic_BE);

    /**
     * Representation of a Cyrillic_che key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_che = new KeySymbol(_Cyrillic_che);

    /**
     * Representation of a Cyrillic_CHE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_CHE = new KeySymbol(_Cyrillic_CHE);

    /**
     * Representation of a Cyrillic_de key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_de = new KeySymbol(_Cyrillic_de);

    /**
     * Representation of a Cyrillic_DE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_DE = new KeySymbol(_Cyrillic_DE);

    /**
     * Representation of a Cyrillic_dzhe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_dzhe = new KeySymbol(_Cyrillic_dzhe);

    /**
     * Representation of a Cyrillic_DZHE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_DZHE = new KeySymbol(_Cyrillic_DZHE);

    /**
     * Representation of a Cyrillic_e key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_e = new KeySymbol(_Cyrillic_e);

    /**
     * Representation of a Cyrillic_E key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_E = new KeySymbol(_Cyrillic_E);

    /**
     * Representation of a Cyrillic_ef key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ef = new KeySymbol(_Cyrillic_ef);

    /**
     * Representation of a Cyrillic_EF key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_EF = new KeySymbol(_Cyrillic_EF);

    /**
     * Representation of a Cyrillic_el key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_el = new KeySymbol(_Cyrillic_el);

    /**
     * Representation of a Cyrillic_EL key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_EL = new KeySymbol(_Cyrillic_EL);

    /**
     * Representation of a Cyrillic_em key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_em = new KeySymbol(_Cyrillic_em);

    /**
     * Representation of a Cyrillic_EM key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_EM = new KeySymbol(_Cyrillic_EM);

    /**
     * Representation of a Cyrillic_en key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_en = new KeySymbol(_Cyrillic_en);

    /**
     * Representation of a Cyrillic_EN key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_EN = new KeySymbol(_Cyrillic_EN);

    /**
     * Representation of a Cyrillic_er key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_er = new KeySymbol(_Cyrillic_er);

    /**
     * Representation of a Cyrillic_ER key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ER = new KeySymbol(_Cyrillic_ER);

    /**
     * Representation of a Cyrillic_es key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_es = new KeySymbol(_Cyrillic_es);

    /**
     * Representation of a Cyrillic_ES key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ES = new KeySymbol(_Cyrillic_ES);

    /**
     * Representation of a Cyrillic_ghe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ghe = new KeySymbol(_Cyrillic_ghe);

    /**
     * Representation of a Cyrillic_GHE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_GHE = new KeySymbol(_Cyrillic_GHE);

    /**
     * Representation of a Cyrillic_ha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ha = new KeySymbol(_Cyrillic_ha);

    /**
     * Representation of a Cyrillic_HA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_HA = new KeySymbol(_Cyrillic_HA);

    /**
     * Representation of a Cyrillic_hardsign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_hardsign = new KeySymbol(
            _Cyrillic_hardsign);

    /**
     * Representation of a Cyrillic_HARDSIGN key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_HARDSIGN = new KeySymbol(
            _Cyrillic_HARDSIGN);

    /**
     * Representation of a Cyrillic_i key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_i = new KeySymbol(_Cyrillic_i);

    /**
     * Representation of a Cyrillic_I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_I = new KeySymbol(_Cyrillic_I);

    /**
     * Representation of a Cyrillic_ie key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ie = new KeySymbol(_Cyrillic_ie);

    /**
     * Representation of a Cyrillic_IE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_IE = new KeySymbol(_Cyrillic_IE);

    /**
     * Representation of a Cyrillic_io key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_io = new KeySymbol(_Cyrillic_io);

    /**
     * Representation of a Cyrillic_IO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_IO = new KeySymbol(_Cyrillic_IO);

    /**
     * Representation of a Cyrillic_je key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_je = new KeySymbol(_Cyrillic_je);

    /**
     * Representation of a Cyrillic_JE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_JE = new KeySymbol(_Cyrillic_JE);

    /**
     * Representation of a Cyrillic_ka key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ka = new KeySymbol(_Cyrillic_ka);

    /**
     * Representation of a Cyrillic_KA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_KA = new KeySymbol(_Cyrillic_KA);

    /**
     * Representation of a Cyrillic_lje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_lje = new KeySymbol(_Cyrillic_lje);

    /**
     * Representation of a Cyrillic_LJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_LJE = new KeySymbol(_Cyrillic_LJE);

    /**
     * Representation of a Cyrillic_nje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_nje = new KeySymbol(_Cyrillic_nje);

    /**
     * Representation of a Cyrillic_NJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_NJE = new KeySymbol(_Cyrillic_NJE);

    /**
     * Representation of a Cyrillic_o key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_o = new KeySymbol(_Cyrillic_o);

    /**
     * Representation of a Cyrillic_O key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_O = new KeySymbol(_Cyrillic_O);

    /**
     * Representation of a Cyrillic_pe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_pe = new KeySymbol(_Cyrillic_pe);

    /**
     * Representation of a Cyrillic_PE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_PE = new KeySymbol(_Cyrillic_PE);

    /**
     * Representation of a Cyrillic_sha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_sha = new KeySymbol(_Cyrillic_sha);

    /**
     * Representation of a Cyrillic_SHA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_SHA = new KeySymbol(_Cyrillic_SHA);

    /**
     * Representation of a Cyrillic_shcha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_shcha = new KeySymbol(
            _Cyrillic_shcha);

    /**
     * Representation of a Cyrillic_SHCHA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_SHCHA = new KeySymbol(
            _Cyrillic_SHCHA);

    /**
     * Representation of a Cyrillic_shorti key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_shorti = new KeySymbol(
            _Cyrillic_shorti);

    /**
     * Representation of a Cyrillic_SHORTI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_SHORTI = new KeySymbol(
            _Cyrillic_SHORTI);

    /**
     * Representation of a Cyrillic_softsign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_softsign = new KeySymbol(
            _Cyrillic_softsign);

    /**
     * Representation of a Cyrillic_SOFTSIGN key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_SOFTSIGN = new KeySymbol(
            _Cyrillic_SOFTSIGN);

    /**
     * Representation of a Cyrillic_te key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_te = new KeySymbol(_Cyrillic_te);

    /**
     * Representation of a Cyrillic_TE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_TE = new KeySymbol(_Cyrillic_TE);

    /**
     * Representation of a Cyrillic_tse key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_tse = new KeySymbol(_Cyrillic_tse);

    /**
     * Representation of a Cyrillic_TSE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_TSE = new KeySymbol(_Cyrillic_TSE);

    /**
     * Representation of a Cyrillic_u key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_u = new KeySymbol(_Cyrillic_u);

    /**
     * Representation of a Cyrillic_U key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_U = new KeySymbol(_Cyrillic_U);

    /**
     * Representation of a Cyrillic_ve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ve = new KeySymbol(_Cyrillic_ve);

    /**
     * Representation of a Cyrillic_VE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_VE = new KeySymbol(_Cyrillic_VE);

    /**
     * Representation of a Cyrillic_ya key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ya = new KeySymbol(_Cyrillic_ya);

    /**
     * Representation of a Cyrillic_YA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_YA = new KeySymbol(_Cyrillic_YA);

    /**
     * Representation of a Cyrillic_yeru key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_yeru = new KeySymbol(_Cyrillic_yeru);

    /**
     * Representation of a Cyrillic_YERU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_YERU = new KeySymbol(_Cyrillic_YERU);

    /**
     * Representation of a Cyrillic_yu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_yu = new KeySymbol(_Cyrillic_yu);

    /**
     * Representation of a Cyrillic_YU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_YU = new KeySymbol(_Cyrillic_YU);

    /**
     * Representation of a Cyrillic_ze key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ze = new KeySymbol(_Cyrillic_ze);

    /**
     * Representation of a Cyrillic_ZE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ZE = new KeySymbol(_Cyrillic_ZE);

    /**
     * Representation of a Cyrillic_zhe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_zhe = new KeySymbol(_Cyrillic_zhe);

    /**
     * Representation of a Cyrillic_ZHE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Cyrillic_ZHE = new KeySymbol(_Cyrillic_ZHE);

    /**
     * Representation of a d key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol d = new KeySymbol(_d);

    /**
     * Representation of a D key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol D = new KeySymbol(_D);

    /**
     * Representation of a dagger key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dagger = new KeySymbol(_dagger);

    /**
     * Representation of a dcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dcaron = new KeySymbol(_dcaron);

    /**
     * Representation of a Dcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Dcaron = new KeySymbol(_Dcaron);

    /**
     * Representation of a dead_abovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_abovedot = new KeySymbol(_dead_abovedot);

    /**
     * Representation of a dead_abovering key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_abovering = new KeySymbol(
            _dead_abovering);

    /**
     * Representation of a dead_acute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_acute = new KeySymbol(_dead_acute);

    /**
     * Representation of a dead_belowdot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_belowdot = new KeySymbol(_dead_belowdot);

    /**
     * Representation of a dead_breve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_breve = new KeySymbol(_dead_breve);

    /**
     * Representation of a dead_caron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_caron = new KeySymbol(_dead_caron);

    /**
     * Representation of a dead_cedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_cedilla = new KeySymbol(_dead_cedilla);

    /**
     * Representation of a dead_circumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_circumflex = new KeySymbol(
            _dead_circumflex);

    /**
     * Representation of a dead_diaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_diaeresis = new KeySymbol(
            _dead_diaeresis);

    /**
     * Representation of a dead_doubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_doubleacute = new KeySymbol(
            _dead_doubleacute);

    /**
     * Representation of a dead_grave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_grave = new KeySymbol(_dead_grave);

    /**
     * representation of a dead_hook key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_hook = new KeySymbol(_dead_hook);

    /**
     * representation of a dead_horn key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_horn = new KeySymbol(_dead_horn);

    /**
     * Representation of a dead_iota key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_iota = new KeySymbol(_dead_iota);

    /**
     * Representation of a dead_macron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_macron = new KeySymbol(_dead_macron);

    /**
     * Representation of a dead_ogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_ogonek = new KeySymbol(_dead_ogonek);

    /**
     * Representation of a dead_semivoiced_sound key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_semivoiced_sound = new KeySymbol(
            _dead_semivoiced_sound);

    /**
     * Representation of a dead_tilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_tilde = new KeySymbol(_dead_tilde);

    /**
     * Representation of a dead_voiced_sound key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dead_voiced_sound = new KeySymbol(
            _dead_voiced_sound);

    /**
     * Representation of a decimalpoint key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol decimalpoint = new KeySymbol(_decimalpoint);

    /**
     * Representation of a degree key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol degree = new KeySymbol(_degree);

    /**
     * Representation of a Delete key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Delete = new KeySymbol(_Delete);

    /**
     * Representation of a DeleteWord_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol DeleteWord_3270 = new KeySymbol(
            _DeleteWord_3270);

    /**
     * Representation of a diaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol diaeresis = new KeySymbol(_diaeresis);

    /**
     * Representation of a diamond key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol diamond = new KeySymbol(_diamond);

    /**
     * Representation of a digitspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol digitspace = new KeySymbol(_digitspace);

    /**
     * Representation of a division key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol division = new KeySymbol(_division);

    /**
     * Representation of a dollar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dollar = new KeySymbol(_dollar);

    /**
     * Representation of a DongSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol DongSign = new KeySymbol(_DongSign);

    /**
     * Representation of a doubbaselinedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol doubbaselinedot = new KeySymbol(
            _doubbaselinedot);

    /**
     * Representation of a doubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol doubleacute = new KeySymbol(_doubleacute);

    /**
     * Representation of a doubledagger key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol doubledagger = new KeySymbol(_doubledagger);

    /**
     * Representation of a doublelowquotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol doublelowquotemark = new KeySymbol(
            _doublelowquotemark);

    /**
     * Representation of a Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Down = new KeySymbol(_Down);

    /**
     * Representation of a downarrow key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol downarrow = new KeySymbol(_downarrow);

    /**
     * Representation of a downcaret key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol downcaret = new KeySymbol(_downcaret);

    /**
     * Representation of a downshoe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol downshoe = new KeySymbol(_downshoe);

    /**
     * Representation of a downstile key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol downstile = new KeySymbol(_downstile);

    /**
     * Representation of a downtack key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol downtack = new KeySymbol(_downtack);

    /**
     * Representation of a dstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol dstroke = new KeySymbol(_dstroke);

    /**
     * Representation of a Dstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Dstroke = new KeySymbol(_Dstroke);

    /**
     * Representation of a Duplicate_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Duplicate_3270 = new KeySymbol(
            _Duplicate_3270);

    /**
     * Representation of a e key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol e = new KeySymbol(_e);

    /**
     * Representation of a E key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol E = new KeySymbol(_E);

    /**
     * Representation of a eabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol eabovedot = new KeySymbol(_eabovedot);

    /**
     * Representation of a Eabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eabovedot = new KeySymbol(_Eabovedot);

    /**
     * Representation of a eacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol eacute = new KeySymbol(_eacute);

    /**
     * Representation of a Eacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eacute = new KeySymbol(_Eacute);

    /**
     * Representation of a ecaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ecaron = new KeySymbol(_ecaron);

    /**
     * Representation of a Ecaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ecaron = new KeySymbol(_Ecaron);

    /**
     * Representation of a ecircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ecircumflex = new KeySymbol(_ecircumflex);

    /**
     * Representation of a Ecircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ecircumflex = new KeySymbol(_Ecircumflex);

    /**
     * Representation of a EcuSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol EcuSign = new KeySymbol(_EcuSign);

    /**
     * Representation of a ediaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ediaeresis = new KeySymbol(_ediaeresis);

    /**
     * Representation of a Ediaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ediaeresis = new KeySymbol(_Ediaeresis);

    /**
     * Representation of a egrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol egrave = new KeySymbol(_egrave);

    /**
     * Representation of a Egrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Egrave = new KeySymbol(_Egrave);

    /**
     * Representation of a Eisu_Shift key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eisu_Shift = new KeySymbol(_Eisu_Shift);

    /**
     * Representation of a Eisu_toggle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eisu_toggle = new KeySymbol(_Eisu_toggle);

    /**
     * Representation of a ellipsis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ellipsis = new KeySymbol(_ellipsis);

    /**
     * Representation of a em3space key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol em3space = new KeySymbol(_em3space);

    /**
     * Representation of a em4space key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol em4space = new KeySymbol(_em4space);

    /**
     * Representation of a emacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emacron = new KeySymbol(_emacron);

    /**
     * Representation of a Emacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Emacron = new KeySymbol(_Emacron);

    /**
     * Representation of a emdash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emdash = new KeySymbol(_emdash);

    /**
     * Representation of a emfilledcircle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emfilledcircle = new KeySymbol(
            _emfilledcircle);

    /**
     * Representation of a emfilledrect key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emfilledrect = new KeySymbol(_emfilledrect);

    /**
     * Representation of a emopencircle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emopencircle = new KeySymbol(_emopencircle);

    /**
     * Representation of a emopenrectangle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emopenrectangle = new KeySymbol(
            _emopenrectangle);

    /**
     * Representation of a emspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol emspace = new KeySymbol(_emspace);

    /**
     * Representation of a End key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol End = new KeySymbol(_End);

    /**
     * Representation of a endash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol endash = new KeySymbol(_endash);

    /**
     * Representation of a enfilledcircbullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol enfilledcircbullet = new KeySymbol(
            _enfilledcircbullet);

    /**
     * Representation of a enfilledsqbullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol enfilledsqbullet = new KeySymbol(
            _enfilledsqbullet);

    /**
     * Representation of a eng key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol eng = new KeySymbol(_eng);

    /**
     * Representation of a ENG key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ENG = new KeySymbol(_ENG);

    /**
     * Representation of a enopencircbullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol enopencircbullet = new KeySymbol(
            _enopencircbullet);

    /**
     * Representation of a enopensquarebullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol enopensquarebullet = new KeySymbol(
            _enopensquarebullet);

    /**
     * Representation of a enspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol enspace = new KeySymbol(_enspace);

    /**
     * Representation of a Enter_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Enter_3270 = new KeySymbol(_Enter_3270);

    /**
     * Representation of a eogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol eogonek = new KeySymbol(_eogonek);

    /**
     * Representation of a Eogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eogonek = new KeySymbol(_Eogonek);

    /**
     * Representation of a equal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol equal = new KeySymbol(_equal);

    /**
     * Representation of a EraseEOF_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol EraseEOF_3270 = new KeySymbol(_EraseEOF_3270);

    /**
     * Representation of a EraseInput_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol EraseInput_3270 = new KeySymbol(
            _EraseInput_3270);

    /**
     * Representation of a Escape key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Escape = new KeySymbol(_Escape);

    /**
     * Representation of a eth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol eth = new KeySymbol(_eth);

    /**
     * Representation of a Eth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Eth = new KeySymbol(_Eth);

    /**
     * Representation of a ETH key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ETH = new KeySymbol(_ETH);

    /**
     * Representation of a EuroSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol EuroSign = new KeySymbol(_EuroSign);

    /**
     * Representation of a exclam key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol exclam = new KeySymbol(_exclam);

    /**
     * Representation of a exclamdown key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol exclamdown = new KeySymbol(_exclamdown);

    /**
     * Representation of a Execute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Execute = new KeySymbol(_Execute);

    /**
     * Representation of a ExSelect_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ExSelect_3270 = new KeySymbol(_ExSelect_3270);

    /**
     * Representation of a f key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol f = new KeySymbol(_f);

    /**
     * Representation of a F key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F = new KeySymbol(_F);

    /**
     * Representation of a F1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F1 = new KeySymbol(_F1);

    /**
     * Representation of a F10 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F10 = new KeySymbol(_F10);

    /**
     * Representation of a F11 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F11 = new KeySymbol(_F11);

    /**
     * Representation of a F12 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F12 = new KeySymbol(_F12);

    /**
     * Representation of a F13 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F13 = new KeySymbol(_F13);

    /**
     * Representation of a F14 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F14 = new KeySymbol(_F14);

    /**
     * Representation of a F15 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F15 = new KeySymbol(_F15);

    /**
     * Representation of a F16 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F16 = new KeySymbol(_F16);

    /**
     * Representation of a F17 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F17 = new KeySymbol(_F17);

    /**
     * Representation of a F18 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F18 = new KeySymbol(_F18);

    /**
     * Representation of a F19 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F19 = new KeySymbol(_F19);

    /**
     * Representation of a F2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F2 = new KeySymbol(_F2);

    /**
     * Representation of a F20 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F20 = new KeySymbol(_F20);

    /**
     * Representation of a F21 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F21 = new KeySymbol(_F21);

    /**
     * Representation of a F22 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F22 = new KeySymbol(_F22);

    /**
     * Representation of a F23 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F23 = new KeySymbol(_F23);

    /**
     * Representation of a F24 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F24 = new KeySymbol(_F24);

    /**
     * Representation of a F25 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F25 = new KeySymbol(_F25);

    /**
     * Representation of a F26 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F26 = new KeySymbol(_F26);

    /**
     * Representation of a F27 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F27 = new KeySymbol(_F27);

    /**
     * Representation of a F28 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F28 = new KeySymbol(_F28);

    /**
     * Representation of a F29 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F29 = new KeySymbol(_F29);

    /**
     * Representation of a F3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F3 = new KeySymbol(_F3);

    /**
     * Representation of a F30 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F30 = new KeySymbol(_F30);

    /**
     * Representation of a F31 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F31 = new KeySymbol(_F31);

    /**
     * Representation of a F32 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F32 = new KeySymbol(_F32);

    /**
     * Representation of a F33 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F33 = new KeySymbol(_F33);

    /**
     * Representation of a F34 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F34 = new KeySymbol(_F34);

    /**
     * Representation of a F35 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F35 = new KeySymbol(_F35);

    /**
     * Representation of a F4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F4 = new KeySymbol(_F4);

    /**
     * Representation of a F5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F5 = new KeySymbol(_F5);

    /**
     * Representation of a F6 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F6 = new KeySymbol(_F6);

    /**
     * Representation of a F7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F7 = new KeySymbol(_F7);

    /**
     * Representation of a F8 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F8 = new KeySymbol(_F8);

    /**
     * Representation of a F9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol F9 = new KeySymbol(_F9);

    /**
     * Representation of a femalesymbol key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol femalesymbol = new KeySymbol(_femalesymbol);

    /**
     * Representation of a ff key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ff = new KeySymbol(_ff);

    /**
     * Representation of a FFrancSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol FFrancSign = new KeySymbol(_FFrancSign);

    /**
     * Representation of a FieldMark_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol FieldMark_3270 = new KeySymbol(
            _FieldMark_3270);

    /**
     * Representation of a figdash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol figdash = new KeySymbol(_figdash);

    /**
     * Representation of a filledlefttribullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol filledlefttribullet = new KeySymbol(
            _filledlefttribullet);

    /**
     * Representation of a filledrectbullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol filledrectbullet = new KeySymbol(
            _filledrectbullet);

    /**
     * Representation of a filledrighttribullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol filledrighttribullet = new KeySymbol(
            _filledrighttribullet);

    /**
     * Representation of a filledtribulletdown key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol filledtribulletdown = new KeySymbol(
            _filledtribulletdown);

    /**
     * Representation of a filledtribulletup key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol filledtribulletup = new KeySymbol(
            _filledtribulletup);

    /**
     * Representation of a Find key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Find = new KeySymbol(_Find);

    /**
     * Representation of a First_Virtual_Screen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol First_Virtual_Screen = new KeySymbol(
            _First_Virtual_Screen);

    /**
     * Representation of a fiveeighths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol fiveeighths = new KeySymbol(_fiveeighths);

    /**
     * Representation of a fivesixths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol fivesixths = new KeySymbol(_fivesixths);

    /**
     * Representation of a fourfifths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol fourfifths = new KeySymbol(_fourfifths);

    /**
     * Representation of a function key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol function = new KeySymbol(_function);

    /**
     * Representation of a g key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol g = new KeySymbol(_g);

    /**
     * Representation of a G key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol G = new KeySymbol(_G);

    /**
     * Representation of a gabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol gabovedot = new KeySymbol(_gabovedot);

    /**
     * Representation of a Gabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Gabovedot = new KeySymbol(_Gabovedot);

    /**
     * Representation of a gbreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol gbreve = new KeySymbol(_gbreve);

    /**
     * Representation of a Gbreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Gbreve = new KeySymbol(_Gbreve);

    /**
     * Representation of a gcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol gcedilla = new KeySymbol(_gcedilla);

    /**
     * Representation of a Gcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Gcedilla = new KeySymbol(_Gcedilla);

    /**
     * Representation of a gcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol gcircumflex = new KeySymbol(_gcircumflex);

    /**
     * Representation of a Gcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Gcircumflex = new KeySymbol(_Gcircumflex);

    /**
     * Representation of a grave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol grave = new KeySymbol(_grave);

    /**
     * Representation of a greater key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol greater = new KeySymbol(_greater);

    /**
     * Representation of a greaterthanequal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol greaterthanequal = new KeySymbol(
            _greaterthanequal);

    /**
     * Representation of a Greek_accentdieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_accentdieresis = new KeySymbol(
            _Greek_accentdieresis);

    /**
     * Representation of a Greek_alpha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_alpha = new KeySymbol(_Greek_alpha);

    /**
     * Representation of a Greek_ALPHA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_ALPHA = new KeySymbol(_Greek_ALPHA);

    /**
     * Representation of a Greek_alphaaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_alphaaccent = new KeySymbol(
            _Greek_alphaaccent);

    /**
     * Representation of a Greek_ALPHAaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_ALPHAaccent = new KeySymbol(
            _Greek_ALPHAaccent);

    /**
     * Representation of a Greek_beta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_beta = new KeySymbol(_Greek_beta);

    /**
     * Representation of a Greek_BETA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_BETA = new KeySymbol(_Greek_BETA);

    /**
     * Representation of a Greek_chi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_chi = new KeySymbol(_Greek_chi);

    /**
     * Representation of a Greek_CHI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_CHI = new KeySymbol(_Greek_CHI);

    /**
     * Representation of a Greek_delta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_delta = new KeySymbol(_Greek_delta);

    /**
     * Representation of a Greek_DELTA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_DELTA = new KeySymbol(_Greek_DELTA);

    /**
     * Representation of a Greek_epsilon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_epsilon = new KeySymbol(_Greek_epsilon);

    /**
     * Representation of a Greek_EPSILON key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_EPSILON = new KeySymbol(_Greek_EPSILON);

    /**
     * Representation of a Greek_epsilonaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_epsilonaccent = new KeySymbol(
            _Greek_epsilonaccent);

    /**
     * Representation of a Greek_EPSILONaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_EPSILONaccent = new KeySymbol(
            _Greek_EPSILONaccent);

    /**
     * Representation of a Greek_eta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_eta = new KeySymbol(_Greek_eta);

    /**
     * Representation of a Greek_ETA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_ETA = new KeySymbol(_Greek_ETA);

    /**
     * Representation of a Greek_etaaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_etaaccent = new KeySymbol(
            _Greek_etaaccent);

    /**
     * Representation of a Greek_ETAaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_ETAaccent = new KeySymbol(
            _Greek_ETAaccent);

    /**
     * Representation of a Greek_finalsmallsigma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_finalsmallsigma = new KeySymbol(
            _Greek_finalsmallsigma);

    /**
     * Representation of a Greek_gamma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_gamma = new KeySymbol(_Greek_gamma);

    /**
     * Representation of a Greek_GAMMA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_GAMMA = new KeySymbol(_Greek_GAMMA);

    /**
     * Representation of a Greek_horizbar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_horizbar = new KeySymbol(
            _Greek_horizbar);

    /**
     * Representation of a Greek_iota key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_iota = new KeySymbol(_Greek_iota);

    /**
     * Representation of a Greek_IOTA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_IOTA = new KeySymbol(_Greek_IOTA);

    /**
     * Representation of a Greek_iotaaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_iotaaccent = new KeySymbol(
            _Greek_iotaaccent);

    /**
     * Representation of a Greek_IOTAaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_IOTAaccent = new KeySymbol(
            _Greek_IOTAaccent);

    /**
     * Representation of a Greek_iotaaccentdieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_iotaaccentdieresis = new KeySymbol(
            _Greek_iotaaccentdieresis);

    /**
     * Representation of a Greek_IOTAdiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_IOTAdiaeresis = new KeySymbol(
            _Greek_IOTAdiaeresis);

    /**
     * Representation of a Greek_iotadieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_iotadieresis = new KeySymbol(
            _Greek_iotadieresis);

    /**
     * Representation of a Greek_kappa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_kappa = new KeySymbol(_Greek_kappa);

    /**
     * Representation of a Greek_KAPPA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_KAPPA = new KeySymbol(_Greek_KAPPA);

    /**
     * Representation of a Greek_lambda key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_lambda = new KeySymbol(_Greek_lambda);

    /**
     * Representation of a Greek_LAMBDA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_LAMBDA = new KeySymbol(_Greek_LAMBDA);

    /**
     * Representation of a Greek_lamda key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_lamda = new KeySymbol(_Greek_lamda);

    /**
     * Representation of a Greek_LAMDA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_LAMDA = new KeySymbol(_Greek_LAMDA);

    /**
     * Representation of a Greek_mu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_mu = new KeySymbol(_Greek_mu);

    /**
     * Representation of a Greek_MU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_MU = new KeySymbol(_Greek_MU);

    /**
     * Representation of a Greek_nu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_nu = new KeySymbol(_Greek_nu);

    /**
     * Representation of a Greek_NU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_NU = new KeySymbol(_Greek_NU);

    /**
     * Representation of a Greek_omega key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_omega = new KeySymbol(_Greek_omega);

    /**
     * Representation of a Greek_OMEGA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_OMEGA = new KeySymbol(_Greek_OMEGA);

    /**
     * Representation of a Greek_omegaaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_omegaaccent = new KeySymbol(
            _Greek_omegaaccent);

    /**
     * Representation of a Greek_OMEGAaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_OMEGAaccent = new KeySymbol(
            _Greek_OMEGAaccent);

    /**
     * Representation of a Greek_omicron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_omicron = new KeySymbol(_Greek_omicron);

    /**
     * Representation of a Greek_OMICRON key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_OMICRON = new KeySymbol(_Greek_OMICRON);

    /**
     * Representation of a Greek_omicronaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_omicronaccent = new KeySymbol(
            _Greek_omicronaccent);

    /**
     * Representation of a Greek_OMICRONaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_OMICRONaccent = new KeySymbol(
            _Greek_OMICRONaccent);

    /**
     * Representation of a Greek_phi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_phi = new KeySymbol(_Greek_phi);

    /**
     * Representation of a Greek_PHI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_PHI = new KeySymbol(_Greek_PHI);

    /**
     * Representation of a Greek_pi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_pi = new KeySymbol(_Greek_pi);

    /**
     * Representation of a Greek_PI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_PI = new KeySymbol(_Greek_PI);

    /**
     * Representation of a Greek_psi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_psi = new KeySymbol(_Greek_psi);

    /**
     * Representation of a Greek_PSI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_PSI = new KeySymbol(_Greek_PSI);

    /**
     * Representation of a Greek_rho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_rho = new KeySymbol(_Greek_rho);

    /**
     * Representation of a Greek_RHO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_RHO = new KeySymbol(_Greek_RHO);

    /**
     * Representation of a Greek_sigma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_sigma = new KeySymbol(_Greek_sigma);

    /**
     * Representation of a Greek_SIGMA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_SIGMA = new KeySymbol(_Greek_SIGMA);

    /**
     * Representation of a Greek_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_switch = new KeySymbol(_Greek_switch);

    /**
     * Representation of a Greek_tau key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_tau = new KeySymbol(_Greek_tau);

    /**
     * Representation of a Greek_TAU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_TAU = new KeySymbol(_Greek_TAU);

    /**
     * Representation of a Greek_theta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_theta = new KeySymbol(_Greek_theta);

    /**
     * Representation of a Greek_THETA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_THETA = new KeySymbol(_Greek_THETA);

    /**
     * Representation of a Greek_upsilon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_upsilon = new KeySymbol(_Greek_upsilon);

    /**
     * Representation of a Greek_UPSILON key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_UPSILON = new KeySymbol(_Greek_UPSILON);

    /**
     * Representation of a Greek_upsilonaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_upsilonaccent = new KeySymbol(
            _Greek_upsilonaccent);

    /**
     * Representation of a Greek_UPSILONaccent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_UPSILONaccent = new KeySymbol(
            _Greek_UPSILONaccent);

    /**
     * Representation of a Greek_upsilonaccentdieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_upsilonaccentdieresis = new KeySymbol(
            _Greek_upsilonaccentdieresis);

    /**
     * Representation of a Greek_upsilondieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_upsilondieresis = new KeySymbol(
            _Greek_upsilondieresis);

    /**
     * Representation of a Greek_UPSILONdieresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_UPSILONdieresis = new KeySymbol(
            _Greek_UPSILONdieresis);

    /**
     * Representation of a Greek_xi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_xi = new KeySymbol(_Greek_xi);

    /**
     * Representation of a Greek_XI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_XI = new KeySymbol(_Greek_XI);

    /**
     * Representation of a Greek_zeta key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_zeta = new KeySymbol(_Greek_zeta);

    /**
     * Representation of a Greek_ZETA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Greek_ZETA = new KeySymbol(_Greek_ZETA);

    /**
     * Representation of a guillemotleft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol guillemotleft = new KeySymbol(_guillemotleft);

    /**
     * Representation of a guillemotright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol guillemotright = new KeySymbol(
            _guillemotright);

    /**
     * Representation of a h key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol h = new KeySymbol(_h);

    /**
     * Representation of a H key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol H = new KeySymbol(_H);

    /**
     * Representation of a hairspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hairspace = new KeySymbol(_hairspace);

    /**
     * Representation of a Hangul key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul = new KeySymbol(_Hangul);

    /**
     * Representation of a Hangul_A key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_A = new KeySymbol(_Hangul_A);

    /**
     * Representation of a Hangul_AE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_AE = new KeySymbol(_Hangul_AE);

    /**
     * Representation of a Hangul_AraeA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_AraeA = new KeySymbol(_Hangul_AraeA);

    /**
     * Representation of a Hangul_AraeAE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_AraeAE = new KeySymbol(_Hangul_AraeAE);

    /**
     * Representation of a Hangul_Banja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Banja = new KeySymbol(_Hangul_Banja);

    /**
     * Representation of a Hangul_Cieuc key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Cieuc = new KeySymbol(_Hangul_Cieuc);

    /**
     * Representation of a Hangul_Codeinput key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Codeinput = new KeySymbol(
            _Hangul_Codeinput);

    /**
     * Representation of a Hangul_Dikeud key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Dikeud = new KeySymbol(_Hangul_Dikeud);

    /**
     * Representation of a Hangul_E key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_E = new KeySymbol(_Hangul_E);

    /**
     * Representation of a Hangul_End key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_End = new KeySymbol(_Hangul_End);

    /**
     * Representation of a Hangul_EO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_EO = new KeySymbol(_Hangul_EO);

    /**
     * Representation of a Hangul_EU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_EU = new KeySymbol(_Hangul_EU);

    /**
     * Representation of a Hangul_Hanja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Hanja = new KeySymbol(_Hangul_Hanja);

    /**
     * Representation of a Hangul_Hieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Hieuh = new KeySymbol(_Hangul_Hieuh);

    /**
     * Representation of a Hangul_I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_I = new KeySymbol(_Hangul_I);

    /**
     * Representation of a Hangul_Ieung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Ieung = new KeySymbol(_Hangul_Ieung);

    /**
     * Representation of a Hangul_J_Cieuc key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Cieuc = new KeySymbol(
            _Hangul_J_Cieuc);

    /**
     * Representation of a Hangul_J_Dikeud key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Dikeud = new KeySymbol(
            _Hangul_J_Dikeud);

    /**
     * Representation of a Hangul_J_Hieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Hieuh = new KeySymbol(
            _Hangul_J_Hieuh);

    /**
     * Representation of a Hangul_J_Ieung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Ieung = new KeySymbol(
            _Hangul_J_Ieung);

    /**
     * Representation of a Hangul_J_Jieuj key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Jieuj = new KeySymbol(
            _Hangul_J_Jieuj);

    /**
     * Representation of a Hangul_J_Khieuq key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Khieuq = new KeySymbol(
            _Hangul_J_Khieuq);

    /**
     * Representation of a Hangul_J_Kiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Kiyeog = new KeySymbol(
            _Hangul_J_Kiyeog);

    /**
     * Representation of a Hangul_J_KiyeogSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_KiyeogSios = new KeySymbol(
            _Hangul_J_KiyeogSios);

    /**
     * Representation of a Hangul_J_KkogjiDalrinIeung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_KkogjiDalrinIeung = new KeySymbol(
            _Hangul_J_KkogjiDalrinIeung);

    /**
     * Representation of a Hangul_J_Mieum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Mieum = new KeySymbol(
            _Hangul_J_Mieum);

    /**
     * Representation of a Hangul_J_Nieun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Nieun = new KeySymbol(
            _Hangul_J_Nieun);

    /**
     * Representation of a Hangul_J_NieunHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_NieunHieuh = new KeySymbol(
            _Hangul_J_NieunHieuh);

    /**
     * Representation of a Hangul_J_NieunJieuj key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_NieunJieuj = new KeySymbol(
            _Hangul_J_NieunJieuj);

    /**
     * Representation of a Hangul_J_PanSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_PanSios = new KeySymbol(
            _Hangul_J_PanSios);

    /**
     * Representation of a Hangul_J_Phieuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Phieuf = new KeySymbol(
            _Hangul_J_Phieuf);

    /**
     * Representation of a Hangul_J_Pieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Pieub = new KeySymbol(
            _Hangul_J_Pieub);

    /**
     * Representation of a Hangul_J_PieubSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_PieubSios = new KeySymbol(
            _Hangul_J_PieubSios);

    /**
     * Representation of a Hangul_J_Rieul key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Rieul = new KeySymbol(
            _Hangul_J_Rieul);

    /**
     * Representation of a Hangul_J_RieulHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulHieuh = new KeySymbol(
            _Hangul_J_RieulHieuh);

    /**
     * Representation of a Hangul_J_RieulKiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulKiyeog = new KeySymbol(
            _Hangul_J_RieulKiyeog);

    /**
     * Representation of a Hangul_J_RieulMieum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulMieum = new KeySymbol(
            _Hangul_J_RieulMieum);

    /**
     * Representation of a Hangul_J_RieulPhieuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulPhieuf = new KeySymbol(
            _Hangul_J_RieulPhieuf);

    /**
     * Representation of a Hangul_J_RieulPieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulPieub = new KeySymbol(
            _Hangul_J_RieulPieub);

    /**
     * Representation of a Hangul_J_RieulSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulSios = new KeySymbol(
            _Hangul_J_RieulSios);

    /**
     * Representation of a Hangul_J_RieulTieut key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_RieulTieut = new KeySymbol(
            _Hangul_J_RieulTieut);

    /**
     * Representation of a Hangul_J_Sios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Sios = new KeySymbol(_Hangul_J_Sios);

    /**
     * Representation of a Hangul_J_SsangKiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_SsangKiyeog = new KeySymbol(
            _Hangul_J_SsangKiyeog);

    /**
     * Representation of a Hangul_J_SsangSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_SsangSios = new KeySymbol(
            _Hangul_J_SsangSios);

    /**
     * Representation of a Hangul_J_Tieut key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_Tieut = new KeySymbol(
            _Hangul_J_Tieut);

    /**
     * Representation of a Hangul_J_YeorinHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_J_YeorinHieuh = new KeySymbol(
            _Hangul_J_YeorinHieuh);

    /**
     * Representation of a Hangul_Jamo key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Jamo = new KeySymbol(_Hangul_Jamo);

    /**
     * Representation of a Hangul_Jeonja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Jeonja = new KeySymbol(_Hangul_Jeonja);

    /**
     * Representation of a Hangul_Jieuj key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Jieuj = new KeySymbol(_Hangul_Jieuj);

    /**
     * Representation of a Hangul_Khieuq key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Khieuq = new KeySymbol(_Hangul_Khieuq);

    /**
     * Representation of a Hangul_Kiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Kiyeog = new KeySymbol(_Hangul_Kiyeog);

    /**
     * Representation of a Hangul_KiyeogSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_KiyeogSios = new KeySymbol(
            _Hangul_KiyeogSios);

    /**
     * Representation of a Hangul_KkogjiDalrinIeung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_KkogjiDalrinIeung = new KeySymbol(
            _Hangul_KkogjiDalrinIeung);

    /**
     * Representation of a Hangul_Mieum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Mieum = new KeySymbol(_Hangul_Mieum);

    /**
     * Representation of a Hangul_MultipleCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_MultipleCandidate = new KeySymbol(
            _Hangul_MultipleCandidate);

    /**
     * Representation of a Hangul_Nieun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Nieun = new KeySymbol(_Hangul_Nieun);

    /**
     * Representation of a Hangul_NieunHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_NieunHieuh = new KeySymbol(
            _Hangul_NieunHieuh);

    /**
     * Representation of a Hangul_NieunJieuj key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_NieunJieuj = new KeySymbol(
            _Hangul_NieunJieuj);

    /**
     * Representation of a Hangul_O key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_O = new KeySymbol(_Hangul_O);

    /**
     * Representation of a Hangul_OE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_OE = new KeySymbol(_Hangul_OE);

    /**
     * Representation of a Hangul_PanSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_PanSios = new KeySymbol(
            _Hangul_PanSios);

    /**
     * Representation of a Hangul_Phieuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Phieuf = new KeySymbol(_Hangul_Phieuf);

    /**
     * Representation of a Hangul_Pieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Pieub = new KeySymbol(_Hangul_Pieub);

    /**
     * Representation of a Hangul_PieubSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_PieubSios = new KeySymbol(
            _Hangul_PieubSios);

    /**
     * Representation of a Hangul_PostHanja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_PostHanja = new KeySymbol(
            _Hangul_PostHanja);

    /**
     * Representation of a Hangul_PreHanja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_PreHanja = new KeySymbol(
            _Hangul_PreHanja);

    /**
     * Representation of a Hangul_PreviousCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_PreviousCandidate = new KeySymbol(
            _Hangul_PreviousCandidate);

    /**
     * Representation of a Hangul_Rieul key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Rieul = new KeySymbol(_Hangul_Rieul);

    /**
     * Representation of a Hangul_RieulHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulHieuh = new KeySymbol(
            _Hangul_RieulHieuh);

    /**
     * Representation of a Hangul_RieulKiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulKiyeog = new KeySymbol(
            _Hangul_RieulKiyeog);

    /**
     * Representation of a Hangul_RieulMieum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulMieum = new KeySymbol(
            _Hangul_RieulMieum);

    /**
     * Representation of a Hangul_RieulPhieuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulPhieuf = new KeySymbol(
            _Hangul_RieulPhieuf);

    /**
     * Representation of a Hangul_RieulPieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulPieub = new KeySymbol(
            _Hangul_RieulPieub);

    /**
     * Representation of a Hangul_RieulSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulSios = new KeySymbol(
            _Hangul_RieulSios);

    /**
     * Representation of a Hangul_RieulTieut key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulTieut = new KeySymbol(
            _Hangul_RieulTieut);

    /**
     * Representation of a Hangul_RieulYeorinHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_RieulYeorinHieuh = new KeySymbol(
            _Hangul_RieulYeorinHieuh);

    /**
     * Representation of a Hangul_Romaja key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Romaja = new KeySymbol(_Hangul_Romaja);

    /**
     * Representation of a Hangul_SingleCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SingleCandidate = new KeySymbol(
            _Hangul_SingleCandidate);

    /**
     * Representation of a Hangul_Sios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Sios = new KeySymbol(_Hangul_Sios);

    /**
     * Representation of a Hangul_Special key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Special = new KeySymbol(
            _Hangul_Special);

    /**
     * Representation of a Hangul_SsangDikeud key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SsangDikeud = new KeySymbol(
            _Hangul_SsangDikeud);

    /**
     * Representation of a Hangul_SsangJieuj key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SsangJieuj = new KeySymbol(
            _Hangul_SsangJieuj);

    /**
     * Representation of a Hangul_SsangKiyeog key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SsangKiyeog = new KeySymbol(
            _Hangul_SsangKiyeog);

    /**
     * Representation of a Hangul_SsangPieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SsangPieub = new KeySymbol(
            _Hangul_SsangPieub);

    /**
     * Representation of a Hangul_SsangSios key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SsangSios = new KeySymbol(
            _Hangul_SsangSios);

    /**
     * Representation of a Hangul_Start key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Start = new KeySymbol(_Hangul_Start);

    /**
     * Representation of a Hangul_SunkyeongeumMieum key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SunkyeongeumMieum = new KeySymbol(
            _Hangul_SunkyeongeumMieum);

    /**
     * Representation of a Hangul_SunkyeongeumPhieuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SunkyeongeumPhieuf = new KeySymbol(
            _Hangul_SunkyeongeumPhieuf);

    /**
     * Representation of a Hangul_SunkyeongeumPieub key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_SunkyeongeumPieub = new KeySymbol(
            _Hangul_SunkyeongeumPieub);

    /**
     * Representation of a Hangul_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_switch = new KeySymbol(_Hangul_switch);

    /**
     * Representation of a Hangul_Tieut key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_Tieut = new KeySymbol(_Hangul_Tieut);

    /**
     * Representation of a Hangul_U key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_U = new KeySymbol(_Hangul_U);

    /**
     * Representation of a Hangul_WA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_WA = new KeySymbol(_Hangul_WA);

    /**
     * Representation of a Hangul_WAE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_WAE = new KeySymbol(_Hangul_WAE);

    /**
     * Representation of a Hangul_WE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_WE = new KeySymbol(_Hangul_WE);

    /**
     * Representation of a Hangul_WEO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_WEO = new KeySymbol(_Hangul_WEO);

    /**
     * Representation of a Hangul_WI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_WI = new KeySymbol(_Hangul_WI);

    /**
     * Representation of a Hangul_YA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YA = new KeySymbol(_Hangul_YA);

    /**
     * Representation of a Hangul_YAE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YAE = new KeySymbol(_Hangul_YAE);

    /**
     * Representation of a Hangul_YE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YE = new KeySymbol(_Hangul_YE);

    /**
     * Representation of a Hangul_YEO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YEO = new KeySymbol(_Hangul_YEO);

    /**
     * Representation of a Hangul_YeorinHieuh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YeorinHieuh = new KeySymbol(
            _Hangul_YeorinHieuh);

    /**
     * Representation of a Hangul_YI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YI = new KeySymbol(_Hangul_YI);

    /**
     * Representation of a Hangul_YO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YO = new KeySymbol(_Hangul_YO);

    /**
     * Representation of a Hangul_YU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hangul_YU = new KeySymbol(_Hangul_YU);

    /**
     * Representation of a Hankaku key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hankaku = new KeySymbol(_Hankaku);

    /**
     * Representation of a hcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hcircumflex = new KeySymbol(_hcircumflex);

    /**
     * Representation of a Hcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hcircumflex = new KeySymbol(_Hcircumflex);

    /**
     * Representation of a heart key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol heart = new KeySymbol(_heart);

    /**
     * Representation of a hebrew_aleph key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_aleph = new KeySymbol(_hebrew_aleph);

    /**
     * Representation of a hebrew_ayin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_ayin = new KeySymbol(_hebrew_ayin);

    /**
     * Representation of a hebrew_bet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_bet = new KeySymbol(_hebrew_bet);

    /**
     * Representation of a hebrew_beth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_beth = new KeySymbol(_hebrew_beth);

    /**
     * Representation of a hebrew_chet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_chet = new KeySymbol(_hebrew_chet);

    /**
     * Representation of a hebrew_dalet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_dalet = new KeySymbol(_hebrew_dalet);

    /**
     * Representation of a hebrew_daleth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_daleth = new KeySymbol(_hebrew_daleth);

    /**
     * Representation of a hebrew_doublelowline key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_doublelowline = new KeySymbol(
            _hebrew_doublelowline);

    /**
     * Representation of a hebrew_finalkaph key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalkaph = new KeySymbol(
            _hebrew_finalkaph);

    /**
     * Representation of a hebrew_finalmem key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalmem = new KeySymbol(
            _hebrew_finalmem);

    /**
     * Representation of a hebrew_finalnun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalnun = new KeySymbol(
            _hebrew_finalnun);

    /**
     * Representation of a hebrew_finalpe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalpe = new KeySymbol(
            _hebrew_finalpe);

    /**
     * Representation of a hebrew_finalzade key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalzade = new KeySymbol(
            _hebrew_finalzade);

    /**
     * Representation of a hebrew_finalzadi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_finalzadi = new KeySymbol(
            _hebrew_finalzadi);

    /**
     * Representation of a hebrew_gimel key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_gimel = new KeySymbol(_hebrew_gimel);

    /**
     * Representation of a hebrew_gimmel key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_gimmel = new KeySymbol(_hebrew_gimmel);

    /**
     * Representation of a hebrew_he key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_he = new KeySymbol(_hebrew_he);

    /**
     * Representation of a hebrew_het key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_het = new KeySymbol(_hebrew_het);

    /**
     * Representation of a hebrew_kaph key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_kaph = new KeySymbol(_hebrew_kaph);

    /**
     * Representation of a hebrew_kuf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_kuf = new KeySymbol(_hebrew_kuf);

    /**
     * Representation of a hebrew_lamed key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_lamed = new KeySymbol(_hebrew_lamed);

    /**
     * Representation of a hebrew_mem key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_mem = new KeySymbol(_hebrew_mem);

    /**
     * Representation of a hebrew_nun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_nun = new KeySymbol(_hebrew_nun);

    /**
     * Representation of a hebrew_pe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_pe = new KeySymbol(_hebrew_pe);

    /**
     * Representation of a hebrew_qoph key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_qoph = new KeySymbol(_hebrew_qoph);

    /**
     * Representation of a hebrew_resh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_resh = new KeySymbol(_hebrew_resh);

    /**
     * Representation of a hebrew_samech key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_samech = new KeySymbol(_hebrew_samech);

    /**
     * Representation of a hebrew_samekh key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_samekh = new KeySymbol(_hebrew_samekh);

    /**
     * Representation of a hebrew_shin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_shin = new KeySymbol(_hebrew_shin);

    /**
     * Representation of a Hebrew_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hebrew_switch = new KeySymbol(_Hebrew_switch);

    /**
     * Representation of a hebrew_taf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_taf = new KeySymbol(_hebrew_taf);

    /**
     * Representation of a hebrew_taw key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_taw = new KeySymbol(_hebrew_taw);

    /**
     * Representation of a hebrew_tet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_tet = new KeySymbol(_hebrew_tet);

    /**
     * Representation of a hebrew_teth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_teth = new KeySymbol(_hebrew_teth);

    /**
     * Representation of a hebrew_waw key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_waw = new KeySymbol(_hebrew_waw);

    /**
     * Representation of a hebrew_yod key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_yod = new KeySymbol(_hebrew_yod);

    /**
     * Representation of a hebrew_zade key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_zade = new KeySymbol(_hebrew_zade);

    /**
     * Representation of a hebrew_zadi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_zadi = new KeySymbol(_hebrew_zadi);

    /**
     * Representation of a hebrew_zain key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_zain = new KeySymbol(_hebrew_zain);

    /**
     * Representation of a hebrew_zayin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hebrew_zayin = new KeySymbol(_hebrew_zayin);

    /**
     * Representation of a Help key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Help = new KeySymbol(_Help);

    /**
     * Representation of a Henkan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Henkan = new KeySymbol(_Henkan);

    /**
     * Representation of a Henkan_Mode key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Henkan_Mode = new KeySymbol(_Henkan_Mode);

    /**
     * Representation of a hexagram key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hexagram = new KeySymbol(_hexagram);

    /**
     * Representation of a Hiragana key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hiragana = new KeySymbol(_Hiragana);

    /**
     * Representation of a Hiragana_Katakana key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hiragana_Katakana = new KeySymbol(
            _Hiragana_Katakana);

    /**
     * Representation of a Home key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Home = new KeySymbol(_Home);

    /**
     * Representation of a horizconnector key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizconnector = new KeySymbol(
            _horizconnector);

    /**
     * Representation of a horizlinescan1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizlinescan1 = new KeySymbol(
            _horizlinescan1);

    /**
     * Representation of a horizlinescan3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizlinescan3 = new KeySymbol(
            _horizlinescan3);

    /**
     * Representation of a horizlinescan5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizlinescan5 = new KeySymbol(
            _horizlinescan5);

    /**
     * Representation of a horizlinescan7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizlinescan7 = new KeySymbol(
            _horizlinescan7);

    /**
     * Representation of a horizlinescan9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol horizlinescan9 = new KeySymbol(
            _horizlinescan9);

    /**
     * Representation of a hstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hstroke = new KeySymbol(_hstroke);

    /**
     * Representation of a Hstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hstroke = new KeySymbol(_Hstroke);

    /**
     * Representation of a ht key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ht = new KeySymbol(_ht);

    /**
     * Representation of a Hyper_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hyper_L = new KeySymbol(_Hyper_L);

    /**
     * Representation of a Hyper_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Hyper_R = new KeySymbol(_Hyper_R);

    /**
     * Representation of a hyphen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol hyphen = new KeySymbol(_hyphen);

    /**
     * Representation of a i key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol i = new KeySymbol(_i);

    /**
     * Representation of a I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol I = new KeySymbol(_I);

    /**
     * Representation of a Iabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Iabovedot = new KeySymbol(_Iabovedot);

    /**
     * Representation of a iacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol iacute = new KeySymbol(_iacute);

    /**
     * Representation of a Iacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Iacute = new KeySymbol(_Iacute);

    /**
     * Representation of a icircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol icircumflex = new KeySymbol(_icircumflex);

    /**
     * Representation of a Icircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Icircumflex = new KeySymbol(_Icircumflex);

    /**
     * Representation of a Ident_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ident_3270 = new KeySymbol(_Ident_3270);

    /**
     * Representation of a identical key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol identical = new KeySymbol(_identical);

    /**
     * Representation of a idiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol idiaeresis = new KeySymbol(_idiaeresis);

    /**
     * Representation of a Idiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Idiaeresis = new KeySymbol(_Idiaeresis);

    /**
     * Representation of a idotless key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol idotless = new KeySymbol(_idotless);

    /**
     * Representation of a ifonlyif key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ifonlyif = new KeySymbol(_ifonlyif);

    /**
     * Representation of a igrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol igrave = new KeySymbol(_igrave);

    /**
     * Representation of a Igrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Igrave = new KeySymbol(_Igrave);

    /**
     * Representation of a imacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol imacron = new KeySymbol(_imacron);

    /**
     * Representation of a Imacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Imacron = new KeySymbol(_Imacron);

    /**
     * Representation of a implies key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol implies = new KeySymbol(_implies);

    /**
     * Representation of a includedin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol includedin = new KeySymbol(_includedin);

    /**
     * Representation of a includes key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol includes = new KeySymbol(_includes);

    /**
     * Representation of a infinity key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol infinity = new KeySymbol(_infinity);

    /**
     * Representation of a Insert key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Insert = new KeySymbol(_Insert);

    /**
     * Representation of a integral key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol integral = new KeySymbol(_integral);

    /**
     * Representation of a intersection key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol intersection = new KeySymbol(_intersection);

    /**
     * Representation of a iogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol iogonek = new KeySymbol(_iogonek);

    /**
     * Representation of a Iogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Iogonek = new KeySymbol(_Iogonek);

    /**
     * Representation of a ISO_Center_Object key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Center_Object = new KeySymbol(
            _ISO_Center_Object);

    /**
     * Representation of a ISO_Continuous_Underline key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Continuous_Underline = new KeySymbol(
            _ISO_Continuous_Underline);

    /**
     * Representation of a ISO_Discontinuous_Underline key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Discontinuous_Underline = new KeySymbol(
            _ISO_Discontinuous_Underline);

    /**
     * Representation of a ISO_Emphasize key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Emphasize = new KeySymbol(_ISO_Emphasize);

    /**
     * Representation of a ISO_Enter key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Enter = new KeySymbol(_ISO_Enter);

    /**
     * Representation of a ISO_Fast_Cursor_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Fast_Cursor_Down = new KeySymbol(
            _ISO_Fast_Cursor_Down);

    /**
     * Representation of a ISO_Fast_Cursor_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Fast_Cursor_Left = new KeySymbol(
            _ISO_Fast_Cursor_Left);

    /**
     * Representation of a ISO_Fast_Cursor_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Fast_Cursor_Right = new KeySymbol(
            _ISO_Fast_Cursor_Right);

    /**
     * Representation of a ISO_Fast_Cursor_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Fast_Cursor_Up = new KeySymbol(
            _ISO_Fast_Cursor_Up);

    /**
     * Representation of a ISO_First_Group key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_First_Group = new KeySymbol(
            _ISO_First_Group);

    /**
     * Representation of a ISO_First_Group_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_First_Group_Lock = new KeySymbol(
            _ISO_First_Group_Lock);

    /**
     * Representation of a ISO_Group_Latch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Group_Latch = new KeySymbol(
            _ISO_Group_Latch);

    /**
     * Representation of a ISO_Group_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Group_Lock = new KeySymbol(
            _ISO_Group_Lock);

    /**
     * Representation of a ISO_Group_Shift key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Group_Shift = new KeySymbol(
            _ISO_Group_Shift);

    /**
     * Representation of a ISO_Last_Group key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Last_Group = new KeySymbol(
            _ISO_Last_Group);

    /**
     * Representation of a ISO_Last_Group_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Last_Group_Lock = new KeySymbol(
            _ISO_Last_Group_Lock);

    /**
     * Representation of a ISO_Left_Tab key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Left_Tab = new KeySymbol(_ISO_Left_Tab);

    /**
     * Representation of a ISO_Level2_Latch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Level2_Latch = new KeySymbol(
            _ISO_Level2_Latch);

    /**
     * Representation of a ISO_Level3_Latch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Level3_Latch = new KeySymbol(
            _ISO_Level3_Latch);

    /**
     * Representation of a ISO_Level3_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Level3_Lock = new KeySymbol(
            _ISO_Level3_Lock);

    /**
     * Representation of a ISO_Level3_Shift key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Level3_Shift = new KeySymbol(
            _ISO_Level3_Shift);

    /**
     * Representation of a ISO_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Lock = new KeySymbol(_ISO_Lock);

    /**
     * Representation of a ISO_Move_Line_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Move_Line_Down = new KeySymbol(
            _ISO_Move_Line_Down);

    /**
     * Representation of a ISO_Move_Line_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Move_Line_Up = new KeySymbol(
            _ISO_Move_Line_Up);

    /**
     * Representation of a ISO_Next_Group key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Next_Group = new KeySymbol(
            _ISO_Next_Group);

    /**
     * Representation of a ISO_Next_Group_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Next_Group_Lock = new KeySymbol(
            _ISO_Next_Group_Lock);

    /**
     * Representation of a ISO_Partial_Line_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Partial_Line_Down = new KeySymbol(
            _ISO_Partial_Line_Down);

    /**
     * Representation of a ISO_Partial_Line_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Partial_Line_Up = new KeySymbol(
            _ISO_Partial_Line_Up);

    /**
     * Representation of a ISO_Partial_Space_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Partial_Space_Left = new KeySymbol(
            _ISO_Partial_Space_Left);

    /**
     * Representation of a ISO_Partial_Space_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Partial_Space_Right = new KeySymbol(
            _ISO_Partial_Space_Right);

    /**
     * Representation of a ISO_Prev_Group key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Prev_Group = new KeySymbol(
            _ISO_Prev_Group);

    /**
     * Representation of a ISO_Prev_Group_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Prev_Group_Lock = new KeySymbol(
            _ISO_Prev_Group_Lock);

    /**
     * Representation of a ISO_Release_Both_Margins key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Release_Both_Margins = new KeySymbol(
            _ISO_Release_Both_Margins);

    /**
     * Representation of a ISO_Release_Margin_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Release_Margin_Left = new KeySymbol(
            _ISO_Release_Margin_Left);

    /**
     * Representation of a ISO_Release_Margin_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Release_Margin_Right = new KeySymbol(
            _ISO_Release_Margin_Right);

    /**
     * Representation of a ISO_Set_Margin_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Set_Margin_Left = new KeySymbol(
            _ISO_Set_Margin_Left);

    /**
     * Representation of a ISO_Set_Margin_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ISO_Set_Margin_Right = new KeySymbol(
            _ISO_Set_Margin_Right);

    /**
     * Representation of a itilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol itilde = new KeySymbol(_itilde);

    /**
     * Representation of a Itilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Itilde = new KeySymbol(_Itilde);

    /**
     * Representation of a j key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol j = new KeySymbol(_j);

    /**
     * Representation of a J key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol J = new KeySymbol(_J);

    /**
     * Representation of a jcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol jcircumflex = new KeySymbol(_jcircumflex);

    /**
     * Representation of a Jcircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Jcircumflex = new KeySymbol(_Jcircumflex);

    /**
     * Representation of a jot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol jot = new KeySymbol(_jot);

    /**
     * Representation of a Jump_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Jump_3270 = new KeySymbol(_Jump_3270);

    /**
     * Representation of a k key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol k = new KeySymbol(_k);

    /**
     * Representation of a K key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol K = new KeySymbol(_K);

    /**
     * Representation of a kana_a key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_a = new KeySymbol(_kana_a);

    /**
     * Representation of a kana_A key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_A = new KeySymbol(_kana_A);

    /**
     * Representation of a kana_CHI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_CHI = new KeySymbol(_kana_CHI);

    /**
     * Representation of a kana_closingbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_closingbracket = new KeySymbol(
            _kana_closingbracket);

    /**
     * Representation of a kana_comma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_comma = new KeySymbol(_kana_comma);

    /**
     * Representation of a kana_conjunctive key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_conjunctive = new KeySymbol(
            _kana_conjunctive);

    /**
     * Representation of a kana_e key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_e = new KeySymbol(_kana_e);

    /**
     * Representation of a kana_E key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_E = new KeySymbol(_kana_E);

    /**
     * Representation of a kana_FU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_FU = new KeySymbol(_kana_FU);

    /**
     * Representation of a kana_fullstop key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_fullstop = new KeySymbol(_kana_fullstop);

    /**
     * Representation of a kana_HA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_HA = new KeySymbol(_kana_HA);

    /**
     * Representation of a kana_HE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_HE = new KeySymbol(_kana_HE);

    /**
     * Representation of a kana_HI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_HI = new KeySymbol(_kana_HI);

    /**
     * Representation of a kana_HO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_HO = new KeySymbol(_kana_HO);

    /**
     * Representation of a kana_HU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_HU = new KeySymbol(_kana_HU);

    /**
     * Representation of a kana_i key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_i = new KeySymbol(_kana_i);

    /**
     * Representation of a kana_I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_I = new KeySymbol(_kana_I);

    /**
     * Representation of a kana_KA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_KA = new KeySymbol(_kana_KA);

    /**
     * Representation of a kana_KE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_KE = new KeySymbol(_kana_KE);

    /**
     * Representation of a kana_KI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_KI = new KeySymbol(_kana_KI);

    /**
     * Representation of a kana_KO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_KO = new KeySymbol(_kana_KO);

    /**
     * Representation of a kana_KU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_KU = new KeySymbol(_kana_KU);

    /**
     * Representation of a Kana_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Kana_Lock = new KeySymbol(_Kana_Lock);

    /**
     * Representation of a kana_MA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_MA = new KeySymbol(_kana_MA);

    /**
     * Representation of a kana_ME key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_ME = new KeySymbol(_kana_ME);

    /**
     * Representation of a kana_MI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_MI = new KeySymbol(_kana_MI);

    /**
     * Representation of a kana_middledot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_middledot = new KeySymbol(
            _kana_middledot);

    /**
     * Representation of a kana_MO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_MO = new KeySymbol(_kana_MO);

    /**
     * Representation of a kana_MU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_MU = new KeySymbol(_kana_MU);

    /**
     * Representation of a kana_N key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_N = new KeySymbol(_kana_N);

    /**
     * Representation of a kana_NA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_NA = new KeySymbol(_kana_NA);

    /**
     * Representation of a kana_NE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_NE = new KeySymbol(_kana_NE);

    /**
     * Representation of a kana_NI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_NI = new KeySymbol(_kana_NI);

    /**
     * Representation of a kana_NO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_NO = new KeySymbol(_kana_NO);

    /**
     * Representation of a kana_NU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_NU = new KeySymbol(_kana_NU);

    /**
     * Representation of a kana_o key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_o = new KeySymbol(_kana_o);

    /**
     * Representation of a kana_O key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_O = new KeySymbol(_kana_O);

    /**
     * Representation of a kana_openingbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_openingbracket = new KeySymbol(
            _kana_openingbracket);

    /**
     * Representation of a kana_RA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_RA = new KeySymbol(_kana_RA);

    /**
     * Representation of a kana_RE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_RE = new KeySymbol(_kana_RE);

    /**
     * Representation of a kana_RI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_RI = new KeySymbol(_kana_RI);

    /**
     * Representation of a kana_RO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_RO = new KeySymbol(_kana_RO);

    /**
     * Representation of a kana_RU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_RU = new KeySymbol(_kana_RU);

    /**
     * Representation of a kana_SA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_SA = new KeySymbol(_kana_SA);

    /**
     * Representation of a kana_SE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_SE = new KeySymbol(_kana_SE);

    /**
     * Representation of a kana_SHI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_SHI = new KeySymbol(_kana_SHI);

    /**
     * Representation of a Kana_Shift key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Kana_Shift = new KeySymbol(_Kana_Shift);

    /**
     * Representation of a kana_SO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_SO = new KeySymbol(_kana_SO);

    /**
     * Representation of a kana_SU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_SU = new KeySymbol(_kana_SU);

    /**
     * Representation of a kana_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_switch = new KeySymbol(_kana_switch);

    /**
     * Representation of a kana_TA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TA = new KeySymbol(_kana_TA);

    /**
     * Representation of a kana_TE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TE = new KeySymbol(_kana_TE);

    /**
     * Representation of a kana_TI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TI = new KeySymbol(_kana_TI);

    /**
     * Representation of a kana_TO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TO = new KeySymbol(_kana_TO);

    /**
     * Representation of a kana_tsu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_tsu = new KeySymbol(_kana_tsu);

    /**
     * Representation of a kana_TSU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TSU = new KeySymbol(_kana_TSU);

    /**
     * Representation of a kana_tu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_tu = new KeySymbol(_kana_tu);

    /**
     * Representation of a kana_TU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_TU = new KeySymbol(_kana_TU);

    /**
     * Representation of a kana_u key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_u = new KeySymbol(_kana_u);

    /**
     * Representation of a kana_U key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_U = new KeySymbol(_kana_U);

    /**
     * Representation of a kana_WA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_WA = new KeySymbol(_kana_WA);

    /**
     * Representation of a kana_WO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_WO = new KeySymbol(_kana_WO);

    /**
     * Representation of a kana_ya key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_ya = new KeySymbol(_kana_ya);

    /**
     * Representation of a kana_YA key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_YA = new KeySymbol(_kana_YA);

    /**
     * Representation of a kana_yo key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_yo = new KeySymbol(_kana_yo);

    /**
     * Representation of a kana_YO key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_YO = new KeySymbol(_kana_YO);

    /**
     * Representation of a kana_yu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_yu = new KeySymbol(_kana_yu);

    /**
     * Representation of a kana_YU key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kana_YU = new KeySymbol(_kana_YU);

    /**
     * Representation of a Kanji key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Kanji = new KeySymbol(_Kanji);

    /**
     * Representation of a Kanji_Bangou key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Kanji_Bangou = new KeySymbol(_Kanji_Bangou);

    /**
     * Representation of a kappa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kappa = new KeySymbol(_kappa);

    /**
     * Representation of a Katakana key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Katakana = new KeySymbol(_Katakana);

    /**
     * Representation of a kcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kcedilla = new KeySymbol(_kcedilla);

    /**
     * Representation of a Kcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Kcedilla = new KeySymbol(_Kcedilla);

    /**
     * Representation of a KeyClick_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KeyClick_3270 = new KeySymbol(_KeyClick_3270);

    /**
     * Representation of a Korean_Won key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Korean_Won = new KeySymbol(_Korean_Won);

    /**
     * Representation of a KP_0 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_0 = new KeySymbol(_KP_0);

    /**
     * Representation of a KP_1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_1 = new KeySymbol(_KP_1);

    /**
     * Representation of a KP_2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_2 = new KeySymbol(_KP_2);

    /**
     * Representation of a KP_3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_3 = new KeySymbol(_KP_3);

    /**
     * Representation of a KP_4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_4 = new KeySymbol(_KP_4);

    /**
     * Representation of a KP_5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_5 = new KeySymbol(_KP_5);

    /**
     * Representation of a KP_6 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_6 = new KeySymbol(_KP_6);

    /**
     * Representation of a KP_7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_7 = new KeySymbol(_KP_7);

    /**
     * Representation of a KP_8 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_8 = new KeySymbol(_KP_8);

    /**
     * Representation of a KP_9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_9 = new KeySymbol(_KP_9);

    /**
     * Representation of a KP_Add key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Add = new KeySymbol(_KP_Add);

    /**
     * Representation of a KP_Begin key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Begin = new KeySymbol(_KP_Begin);

    /**
     * Representation of a KP_Decimal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Decimal = new KeySymbol(_KP_Decimal);

    /**
     * Representation of a KP_Delete key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Delete = new KeySymbol(_KP_Delete);

    /**
     * Representation of a KP_Divide key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Divide = new KeySymbol(_KP_Divide);

    /**
     * Representation of a KP_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Down = new KeySymbol(_KP_Down);

    /**
     * Representation of a KP_End key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_End = new KeySymbol(_KP_End);

    /**
     * Representation of a KP_Enter key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Enter = new KeySymbol(_KP_Enter);

    /**
     * Representation of a KP_Equal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Equal = new KeySymbol(_KP_Equal);

    /**
     * Representation of a KP_F1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_F1 = new KeySymbol(_KP_F1);

    /**
     * Representation of a KP_F2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_F2 = new KeySymbol(_KP_F2);

    /**
     * Representation of a KP_F3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_F3 = new KeySymbol(_KP_F3);

    /**
     * Representation of a KP_F4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_F4 = new KeySymbol(_KP_F4);

    /**
     * Representation of a KP_Home key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Home = new KeySymbol(_KP_Home);

    /**
     * Representation of a KP_Insert key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Insert = new KeySymbol(_KP_Insert);

    /**
     * Representation of a KP_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Left = new KeySymbol(_KP_Left);

    /**
     * Representation of a KP_Multiply key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Multiply = new KeySymbol(_KP_Multiply);

    /**
     * Representation of a KP_Next key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Next = new KeySymbol(_KP_Next);

    /**
     * Representation of a KP_Page_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Page_Down = new KeySymbol(_KP_Page_Down);

    /**
     * Representation of a KP_Page_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Page_Up = new KeySymbol(_KP_Page_Up);

    /**
     * Representation of a KP_Prior key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Prior = new KeySymbol(_KP_Prior);

    /**
     * Representation of a KP_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Right = new KeySymbol(_KP_Right);

    /**
     * Representation of a KP_Separator key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Separator = new KeySymbol(_KP_Separator);

    /**
     * Representation of a KP_Space key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Space = new KeySymbol(_KP_Space);

    /**
     * Representation of a KP_Subtract key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Subtract = new KeySymbol(_KP_Subtract);

    /**
     * Representation of a KP_Tab key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Tab = new KeySymbol(_KP_Tab);

    /**
     * Representation of a KP_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol KP_Up = new KeySymbol(_KP_Up);

    /**
     * Representation of a kra key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol kra = new KeySymbol(_kra);

    /**
     * Representation of a l key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol l = new KeySymbol(_l);

    /**
     * Representation of a L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L = new KeySymbol(_L);

    /**
     * Representation of a L1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L1 = new KeySymbol(_L1);

    /**
     * Representation of a L10 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L10 = new KeySymbol(_L10);

    /**
     * Representation of a L2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L2 = new KeySymbol(_L2);

    /**
     * Representation of a L3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L3 = new KeySymbol(_L3);

    /**
     * Representation of a L4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L4 = new KeySymbol(_L4);

    /**
     * Representation of a L5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L5 = new KeySymbol(_L5);

    /**
     * Representation of a L6 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L6 = new KeySymbol(_L6);

    /**
     * Representation of a L7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L7 = new KeySymbol(_L7);

    /**
     * Representation of a L8 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L8 = new KeySymbol(_L8);

    /**
     * Representation of a L9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol L9 = new KeySymbol(_L9);

    /**
     * Representation of a lacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lacute = new KeySymbol(_lacute);

    /**
     * Representation of a Lacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Lacute = new KeySymbol(_Lacute);

    /**
     * Representation of a Last_Virtual_Screen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Last_Virtual_Screen = new KeySymbol(
            _Last_Virtual_Screen);

    /**
     * Representation of a latincross key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol latincross = new KeySymbol(_latincross);

    /**
     * Representation of a lcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lcaron = new KeySymbol(_lcaron);

    /**
     * Representation of a Lcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Lcaron = new KeySymbol(_Lcaron);

    /**
     * Representation of a lcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lcedilla = new KeySymbol(_lcedilla);

    /**
     * Representation of a Lcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Lcedilla = new KeySymbol(_Lcedilla);

    /**
     * Representation of a Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Left = new KeySymbol(_Left);

    /**
     * Representation of a Left2_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Left2_3270 = new KeySymbol(_Left2_3270);

    /**
     * Representation of a leftanglebracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftanglebracket = new KeySymbol(
            _leftanglebracket);

    /**
     * Representation of a leftarrow key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftarrow = new KeySymbol(_leftarrow);

    /**
     * Representation of a leftcaret key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftcaret = new KeySymbol(_leftcaret);

    /**
     * Representation of a leftdoublequotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftdoublequotemark = new KeySymbol(
            _leftdoublequotemark);

    /**
     * Representation of a leftmiddlecurlybrace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftmiddlecurlybrace = new KeySymbol(
            _leftmiddlecurlybrace);

    /**
     * Representation of a leftopentriangle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftopentriangle = new KeySymbol(
            _leftopentriangle);

    /**
     * Representation of a leftpointer key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftpointer = new KeySymbol(_leftpointer);

    /**
     * Representation of a leftradical key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftradical = new KeySymbol(_leftradical);

    /**
     * Representation of a leftshoe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftshoe = new KeySymbol(_leftshoe);

    /**
     * Representation of a leftsinglequotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftsinglequotemark = new KeySymbol(
            _leftsinglequotemark);

    /**
     * Representation of a leftt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol leftt = new KeySymbol(_leftt);

    /**
     * Representation of a lefttack key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lefttack = new KeySymbol(_lefttack);

    /**
     * Representation of a less key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol less = new KeySymbol(_less);

    /**
     * Representation of a lessthanequal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lessthanequal = new KeySymbol(_lessthanequal);

    /**
     * Representation of a lf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lf = new KeySymbol(_lf);

    /**
     * Representation of a Linefeed key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Linefeed = new KeySymbol(_Linefeed);

    /**
     * Representation of a LiraSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol LiraSign = new KeySymbol(_LiraSign);

    /**
     * Representation of a logicaland key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol logicaland = new KeySymbol(_logicaland);

    /**
     * Representation of a logicalor key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol logicalor = new KeySymbol(_logicalor);

    /**
     * Representation of a lowleftcorner key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lowleftcorner = new KeySymbol(_lowleftcorner);

    /**
     * Representation of a lowrightcorner key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lowrightcorner = new KeySymbol(
            _lowrightcorner);

    /**
     * Representation of a lstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol lstroke = new KeySymbol(_lstroke);

    /**
     * Representation of a Lstroke key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Lstroke = new KeySymbol(_Lstroke);

    /**
     * Representation of a m key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol m = new KeySymbol(_m);

    /**
     * Representation of a M key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol M = new KeySymbol(_M);

    /**
     * Representation of a Macedonia_dse key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_dse = new KeySymbol(_Macedonia_dse);

    /**
     * Representation of a Macedonia_DSE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_DSE = new KeySymbol(_Macedonia_DSE);

    /**
     * Representation of a Macedonia_gje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_gje = new KeySymbol(_Macedonia_gje);

    /**
     * Representation of a Macedonia_GJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_GJE = new KeySymbol(_Macedonia_GJE);

    /**
     * Representation of a Macedonia_kje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_kje = new KeySymbol(_Macedonia_kje);

    /**
     * Representation of a Macedonia_KJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Macedonia_KJE = new KeySymbol(_Macedonia_KJE);

    /**
     * Representation of a macron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol macron = new KeySymbol(_macron);

    /**
     * Representation of a Mae_Koho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Mae_Koho = new KeySymbol(_Mae_Koho);

    /**
     * Representation of a malesymbol key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol malesymbol = new KeySymbol(_malesymbol);

    /**
     * Representation of a maltesecross key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol maltesecross = new KeySymbol(_maltesecross);

    /**
     * Representation of a marker key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol marker = new KeySymbol(_marker);

    /**
     * Representation of a masculine key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol masculine = new KeySymbol(_masculine);

    /**
     * Representation of a Massyo key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Massyo = new KeySymbol(_Massyo);

    /**
     * Representation of a Menu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Menu = new KeySymbol(_Menu);

    /**
     * Representation of a Meta_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Meta_L = new KeySymbol(_Meta_L);

    /**
     * Representation of a Meta_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Meta_R = new KeySymbol(_Meta_R);

    /**
     * Representation of a MillSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol MillSign = new KeySymbol(_MillSign);

    /**
     * Representation of a minus key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol minus = new KeySymbol(_minus);

    /**
     * Representation of a minutes key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol minutes = new KeySymbol(_minutes);

    /**
     * Representation of a Mode_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Mode_switch = new KeySymbol(_Mode_switch);

    /**
     * Representation of a MouseKeys_Accel_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol MouseKeys_Accel_Enable = new KeySymbol(
            _MouseKeys_Accel_Enable);

    /**
     * Representation of a MouseKeys_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol MouseKeys_Enable = new KeySymbol(
            _MouseKeys_Enable);

    /**
     * Representation of a mu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol mu = new KeySymbol(_mu);

    /**
     * Representation of a Muhenkan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Muhenkan = new KeySymbol(_Muhenkan);

    /**
     * Representation of a Multi_key key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Multi_key = new KeySymbol(_Multi_key);

    /**
     * Representation of a MultipleCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol MultipleCandidate = new KeySymbol(
            _MultipleCandidate);

    /**
     * Representation of a multiply key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol multiply = new KeySymbol(_multiply);

    /**
     * Representation of a musicalflat key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol musicalflat = new KeySymbol(_musicalflat);

    /**
     * Representation of a musicalsharp key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol musicalsharp = new KeySymbol(_musicalsharp);

    /**
     * Representation of a n key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol n = new KeySymbol(_n);

    /**
     * Representation of a N key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol N = new KeySymbol(_N);

    /**
     * Representation of a nabla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol nabla = new KeySymbol(_nabla);

    /**
     * Representation of a nacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol nacute = new KeySymbol(_nacute);

    /**
     * Representation of a Nacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Nacute = new KeySymbol(_Nacute);

    /**
     * Representation of a NairaSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol NairaSign = new KeySymbol(_NairaSign);

    /**
     * Representation of a ncaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ncaron = new KeySymbol(_ncaron);

    /**
     * Representation of a Ncaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ncaron = new KeySymbol(_Ncaron);

    /**
     * Representation of a ncedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ncedilla = new KeySymbol(_ncedilla);

    /**
     * Representation of a Ncedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ncedilla = new KeySymbol(_Ncedilla);

    /**
     * Representation of a NewSheqelSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol NewSheqelSign = new KeySymbol(_NewSheqelSign);

    /**
     * Representation of a Next key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Next = new KeySymbol(_Next);

    /**
     * Representation of a Next_Virtual_Screen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Next_Virtual_Screen = new KeySymbol(
            _Next_Virtual_Screen);

    /**
     * Representation of a nl key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol nl = new KeySymbol(_nl);

    /**
     * Representation of a nobreakspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol nobreakspace = new KeySymbol(_nobreakspace);

    /**
     * Representation of a notequal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol notequal = new KeySymbol(_notequal);

    /**
     * Representation of a notsign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol notsign = new KeySymbol(_notsign);

    /**
     * Representation of a ntilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ntilde = new KeySymbol(_ntilde);

    /**
     * Representation of a Ntilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ntilde = new KeySymbol(_Ntilde);

    /**
     * Representation of a num_0 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_0 = new KeySymbol(_num_0);

    /**
     * Representation of a num_1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_1 = new KeySymbol(_num_1);

    /**
     * Representation of a num_2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_2 = new KeySymbol(_num_2);

    /**
     * Representation of a num_3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_3 = new KeySymbol(_num_3);

    /**
     * Representation of a num_4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_4 = new KeySymbol(_num_4);

    /**
     * Representation of a num_5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_5 = new KeySymbol(_num_5);

    /**
     * Representation of a num_6 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_6 = new KeySymbol(_num_6);

    /**
     * Representation of a num_7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_7 = new KeySymbol(_num_7);

    /**
     * Representation of a num_8 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_8 = new KeySymbol(_num_8);

    /**
     * Representation of a num_9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol num_9 = new KeySymbol(_num_9);

    /**
     * Representation of a Num_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Num_Lock = new KeySymbol(_Num_Lock);

    /**
     * Representation of a numbersign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol numbersign = new KeySymbol(_numbersign);

    /**
     * Representation of a numerosign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol numerosign = new KeySymbol(_numerosign);

    /**
     * Representation of a o key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol o = new KeySymbol(_o);

    /**
     * Representation of a O key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol O = new KeySymbol(_O);

    /**
     * Representation of a oacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol oacute = new KeySymbol(_oacute);

    /**
     * Representation of a Oacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Oacute = new KeySymbol(_Oacute);

    /**
     * Representation of a ocircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ocircumflex = new KeySymbol(_ocircumflex);

    /**
     * Representation of a Ocircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ocircumflex = new KeySymbol(_Ocircumflex);

    /**
     * Representation of a odiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol odiaeresis = new KeySymbol(_odiaeresis);

    /**
     * Representation of a Odiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Odiaeresis = new KeySymbol(_Odiaeresis);

    /**
     * Representation of a odoubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol odoubleacute = new KeySymbol(_odoubleacute);

    /**
     * Representation of a Odoubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Odoubleacute = new KeySymbol(_Odoubleacute);

    /**
     * Representation of a oe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol oe = new KeySymbol(_oe);

    /**
     * Representation of a OE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol OE = new KeySymbol(_OE);

    /**
     * Representation of a ogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ogonek = new KeySymbol(_ogonek);

    /**
     * Representation of a ograve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ograve = new KeySymbol(_ograve);

    /**
     * Representation of a Ograve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ograve = new KeySymbol(_Ograve);

    /**
     * Representation of a omacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol omacron = new KeySymbol(_omacron);

    /**
     * Representation of a Omacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Omacron = new KeySymbol(_Omacron);

    /**
     * Representation of a oneeighth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol oneeighth = new KeySymbol(_oneeighth);

    /**
     * Representation of a onefifth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onefifth = new KeySymbol(_onefifth);

    /**
     * Representation of a onehalf key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onehalf = new KeySymbol(_onehalf);

    /**
     * Representation of a onequarter key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onequarter = new KeySymbol(_onequarter);

    /**
     * Representation of a onesixth key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onesixth = new KeySymbol(_onesixth);

    /**
     * Representation of a onesuperior key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onesuperior = new KeySymbol(_onesuperior);

    /**
     * Representation of a onethird key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol onethird = new KeySymbol(_onethird);

    /**
     * Representation of a Ooblique key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ooblique = new KeySymbol(_Ooblique);

    /**
     * Representation of a openrectbullet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol openrectbullet = new KeySymbol(
            _openrectbullet);

    /**
     * Representation of a openstar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol openstar = new KeySymbol(_openstar);

    /**
     * Representation of a opentribulletdown key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol opentribulletdown = new KeySymbol(
            _opentribulletdown);

    /**
     * Representation of a opentribulletup key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol opentribulletup = new KeySymbol(
            _opentribulletup);

    /**
     * Representation of a ordfeminine key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ordfeminine = new KeySymbol(_ordfeminine);

    /**
     * Representation of a oslash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol oslash = new KeySymbol(_oslash);

    /**
     * Representation of a otilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol otilde = new KeySymbol(_otilde);

    /**
     * Representation of a Otilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Otilde = new KeySymbol(_Otilde);

    /**
     * Representation of a overbar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol overbar = new KeySymbol(_overbar);

    /**
     * Representation of a Overlay1_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Overlay1_Enable = new KeySymbol(
            _Overlay1_Enable);

    /**
     * Representation of a Overlay2_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Overlay2_Enable = new KeySymbol(
            _Overlay2_Enable);

    /**
     * Representation of a overline key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol overline = new KeySymbol(_overline);

    /**
     * Representation of a p key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol p = new KeySymbol(_p);

    /**
     * Representation of a P key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol P = new KeySymbol(_P);

    /**
     * Representation of a PA1_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PA1_3270 = new KeySymbol(_PA1_3270);

    /**
     * Representation of a PA2_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PA2_3270 = new KeySymbol(_PA2_3270);

    /**
     * Representation of a PA3_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PA3_3270 = new KeySymbol(_PA3_3270);

    /**
     * Representation of a Page_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Page_Down = new KeySymbol(_Page_Down);

    /**
     * Representation of a Page_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Page_Up = new KeySymbol(_Page_Up);

    /**
     * Representation of a paragraph key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol paragraph = new KeySymbol(_paragraph);

    /**
     * Representation of a parenleft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol parenleft = new KeySymbol(_parenleft);

    /**
     * Representation of a parenright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol parenright = new KeySymbol(_parenright);

    /**
     * Representation of a partialderivative key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol partialderivative = new KeySymbol(
            _partialderivative);

    /**
     * Representation of a Pause key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pause = new KeySymbol(_Pause);

    /**
     * Representation of a percent key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol percent = new KeySymbol(_percent);

    /**
     * Representation of a period key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol period = new KeySymbol(_period);

    /**
     * Representation of a periodcentered key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol periodcentered = new KeySymbol(
            _periodcentered);

    /**
     * Representation of a PesetaSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PesetaSign = new KeySymbol(_PesetaSign);

    /**
     * Representation of a phonographcopyright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol phonographcopyright = new KeySymbol(
            _phonographcopyright);

    /**
     * Representation of a Play_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Play_3270 = new KeySymbol(_Play_3270);

    /**
     * Representation of a plus key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol plus = new KeySymbol(_plus);

    /**
     * Representation of a plusminus key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol plusminus = new KeySymbol(_plusminus);

    /**
     * Representation of a Pointer_Accelerate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Accelerate = new KeySymbol(
            _Pointer_Accelerate);

    /**
     * Representation of a Pointer_Button_Dflt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button_Dflt = new KeySymbol(
            _Pointer_Button_Dflt);

    /**
     * Representation of a Pointer_Button1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button1 = new KeySymbol(
            _Pointer_Button1);

    /**
     * Representation of a Pointer_Button2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button2 = new KeySymbol(
            _Pointer_Button2);

    /**
     * Representation of a Pointer_Button3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button3 = new KeySymbol(
            _Pointer_Button3);

    /**
     * Representation of a Pointer_Button4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button4 = new KeySymbol(
            _Pointer_Button4);

    /**
     * Representation of a Pointer_Button5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Button5 = new KeySymbol(
            _Pointer_Button5);

    /**
     * Representation of a Pointer_DblClick_Dflt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick_Dflt = new KeySymbol(
            _Pointer_DblClick_Dflt);

    /**
     * Representation of a Pointer_DblClick1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick1 = new KeySymbol(
            _Pointer_DblClick1);

    /**
     * Representation of a Pointer_DblClick2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick2 = new KeySymbol(
            _Pointer_DblClick2);

    /**
     * Representation of a Pointer_DblClick3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick3 = new KeySymbol(
            _Pointer_DblClick3);

    /**
     * Representation of a Pointer_DblClick4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick4 = new KeySymbol(
            _Pointer_DblClick4);

    /**
     * Representation of a Pointer_DblClick5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DblClick5 = new KeySymbol(
            _Pointer_DblClick5);

    /**
     * Representation of a Pointer_DfltBtnNext key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DfltBtnNext = new KeySymbol(
            _Pointer_DfltBtnNext);

    /**
     * Representation of a Pointer_DfltBtnPrev key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DfltBtnPrev = new KeySymbol(
            _Pointer_DfltBtnPrev);

    /**
     * Representation of a Pointer_Down key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Down = new KeySymbol(_Pointer_Down);

    /**
     * Representation of a Pointer_DownLeft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DownLeft = new KeySymbol(
            _Pointer_DownLeft);

    /**
     * Representation of a Pointer_DownRight key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_DownRight = new KeySymbol(
            _Pointer_DownRight);

    /**
     * Representation of a Pointer_Drag_Dflt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag_Dflt = new KeySymbol(
            _Pointer_Drag_Dflt);

    /**
     * Representation of a Pointer_Drag1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag1 = new KeySymbol(_Pointer_Drag1);

    /**
     * Representation of a Pointer_Drag2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag2 = new KeySymbol(_Pointer_Drag2);

    /**
     * Representation of a Pointer_Drag3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag3 = new KeySymbol(_Pointer_Drag3);

    /**
     * Representation of a Pointer_Drag4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag4 = new KeySymbol(_Pointer_Drag4);

    /**
     * Representation of a Pointer_Drag5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Drag5 = new KeySymbol(_Pointer_Drag5);

    /**
     * Representation of a Pointer_EnableKeys key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_EnableKeys = new KeySymbol(
            _Pointer_EnableKeys);

    /**
     * Representation of a Pointer_Left key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Left = new KeySymbol(_Pointer_Left);

    /**
     * Representation of a Pointer_Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Right = new KeySymbol(_Pointer_Right);

    /**
     * Representation of a Pointer_Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_Up = new KeySymbol(_Pointer_Up);

    /**
     * Representation of a Pointer_UpLeft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_UpLeft = new KeySymbol(
            _Pointer_UpLeft);

    /**
     * Representation of a Pointer_UpRight key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Pointer_UpRight = new KeySymbol(
            _Pointer_UpRight);

    /**
     * Representation of a prescription key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol prescription = new KeySymbol(_prescription);

    /**
     * Representation of a Prev_Virtual_Screen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Prev_Virtual_Screen = new KeySymbol(
            _Prev_Virtual_Screen);

    /**
     * Representation of a PreviousCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PreviousCandidate = new KeySymbol(
            _PreviousCandidate);

    /**
     * Representation of a Print key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Print = new KeySymbol(_Print);

    /**
     * Representation of a PrintScreen_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol PrintScreen_3270 = new KeySymbol(
            _PrintScreen_3270);

    /**
     * Representation of a Prior key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Prior = new KeySymbol(_Prior);

    /**
     * Representation of a prolongedsound key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol prolongedsound = new KeySymbol(
            _prolongedsound);

    /**
     * Representation of a punctspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol punctspace = new KeySymbol(_punctspace);

    /**
     * Representation of a q key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol q = new KeySymbol(_q);

    /**
     * Representation of a Q key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Q = new KeySymbol(_Q);

    /**
     * Representation of a quad key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol quad = new KeySymbol(_quad);

    /**
     * Representation of a question key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol question = new KeySymbol(_question);

    /**
     * Representation of a questiondown key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol questiondown = new KeySymbol(_questiondown);

    /**
     * Representation of a Quit_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Quit_3270 = new KeySymbol(_Quit_3270);

    /**
     * Representation of a quotedbl key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol quotedbl = new KeySymbol(_quotedbl);

    /**
     * Representation of a quoteleft key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol quoteleft = new KeySymbol(_quoteleft);

    /**
     * Representation of a quoteright key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol quoteright = new KeySymbol(_quoteright);

    /**
     * Representation of a r key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol r = new KeySymbol(_r);

    /**
     * Representation of a R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R = new KeySymbol(_R);

    /**
     * Representation of a R1 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R1 = new KeySymbol(_R1);

    /**
     * Representation of a R10 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R10 = new KeySymbol(_R10);

    /**
     * Representation of a R11 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R11 = new KeySymbol(_R11);

    /**
     * Representation of a R12 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R12 = new KeySymbol(_R12);

    /**
     * Representation of a R13 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R13 = new KeySymbol(_R13);

    /**
     * Representation of a R14 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R14 = new KeySymbol(_R14);

    /**
     * Representation of a R15 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R15 = new KeySymbol(_R15);

    /**
     * Representation of a R2 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R2 = new KeySymbol(_R2);

    /**
     * Representation of a R3 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R3 = new KeySymbol(_R3);

    /**
     * Representation of a R4 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R4 = new KeySymbol(_R4);

    /**
     * Representation of a R5 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R5 = new KeySymbol(_R5);

    /**
     * Representation of a R6 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R6 = new KeySymbol(_R6);

    /**
     * Representation of a R7 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R7 = new KeySymbol(_R7);

    /**
     * Representation of a R8 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R8 = new KeySymbol(_R8);

    /**
     * Representation of a R9 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol R9 = new KeySymbol(_R9);

    /**
     * Representation of a racute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol racute = new KeySymbol(_racute);

    /**
     * Representation of a Racute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Racute = new KeySymbol(_Racute);

    /**
     * Representation of a radical key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol radical = new KeySymbol(_radical);

    /**
     * Representation of a rcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rcaron = new KeySymbol(_rcaron);

    /**
     * Representation of a Rcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Rcaron = new KeySymbol(_Rcaron);

    /**
     * Representation of a rcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rcedilla = new KeySymbol(_rcedilla);

    /**
     * Representation of a Rcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Rcedilla = new KeySymbol(_Rcedilla);

    /**
     * Representation of a Record_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Record_3270 = new KeySymbol(_Record_3270);

    /**
     * Representation of a Redo key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Redo = new KeySymbol(_Redo);

    /**
     * Representation of a registered key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol registered = new KeySymbol(_registered);

    /**
     * Representation of a RepeatKeys_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol RepeatKeys_Enable = new KeySymbol(
            _RepeatKeys_Enable);

    /**
     * Representation of a Reset_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Reset_3270 = new KeySymbol(_Reset_3270);

    /**
     * Representation of a Return key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Return = new KeySymbol(_Return);

    /**
     * Representation of a Right key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Right = new KeySymbol(_Right);

    /**
     * Representation of a Right2_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Right2_3270 = new KeySymbol(_Right2_3270);

    /**
     * Representation of a rightanglebracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightanglebracket = new KeySymbol(
            _rightanglebracket);

    /**
     * Representation of a rightarrow key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightarrow = new KeySymbol(_rightarrow);

    /**
     * Representation of a rightcaret key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightcaret = new KeySymbol(_rightcaret);

    /**
     * Representation of a rightdoublequotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightdoublequotemark = new KeySymbol(
            _rightdoublequotemark);

    /**
     * Representation of a rightmiddlecurlybrace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightmiddlecurlybrace = new KeySymbol(
            _rightmiddlecurlybrace);

    /**
     * Representation of a rightmiddlesummation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightmiddlesummation = new KeySymbol(
            _rightmiddlesummation);

    /**
     * Representation of a rightopentriangle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightopentriangle = new KeySymbol(
            _rightopentriangle);

    /**
     * Representation of a rightpointer key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightpointer = new KeySymbol(_rightpointer);

    /**
     * Representation of a rightshoe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightshoe = new KeySymbol(_rightshoe);

    /**
     * Representation of a rightsinglequotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightsinglequotemark = new KeySymbol(
            _rightsinglequotemark);

    /**
     * Representation of a rightt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol rightt = new KeySymbol(_rightt);

    /**
     * Representation of a righttack key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol righttack = new KeySymbol(_righttack);

    /**
     * Representation of a Romaji key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Romaji = new KeySymbol(_Romaji);

    /**
     * Representation of a Rule_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Rule_3270 = new KeySymbol(_Rule_3270);

    /**
     * Representation of a RupeeSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol RupeeSign = new KeySymbol(_RupeeSign);

    /**
     * Representation of a s key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol s = new KeySymbol(_s);

    /**
     * Representation of a S key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol S = new KeySymbol(_S);

    /**
     * Representation of a sacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol sacute = new KeySymbol(_sacute);

    /**
     * Representation of a Sacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Sacute = new KeySymbol(_Sacute);

    /**
     * Representation of a scaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol scaron = new KeySymbol(_scaron);

    /**
     * Representation of a Scaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Scaron = new KeySymbol(_Scaron);

    /**
     * Representation of a scedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol scedilla = new KeySymbol(_scedilla);

    /**
     * Representation of a Scedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Scedilla = new KeySymbol(_Scedilla);

    /**
     * Representation of a scircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol scircumflex = new KeySymbol(_scircumflex);

    /**
     * Representation of a Scircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Scircumflex = new KeySymbol(_Scircumflex);

    /**
     * Representation of a script_switch key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol script_switch = new KeySymbol(_script_switch);

    /**
     * Representation of a Scroll_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Scroll_Lock = new KeySymbol(_Scroll_Lock);

    /**
     * Representation of a seconds key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol seconds = new KeySymbol(_seconds);

    /**
     * Representation of a section key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol section = new KeySymbol(_section);

    /**
     * Representation of a Select key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Select = new KeySymbol(_Select);

    /**
     * Representation of a semicolon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol semicolon = new KeySymbol(_semicolon);

    /**
     * Representation of a semivoicedsound key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol semivoicedsound = new KeySymbol(
            _semivoicedsound);

    /**
     * Representation of a Serbian_dje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_dje = new KeySymbol(_Serbian_dje);

    /**
     * Representation of a Serbian_DJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_DJE = new KeySymbol(_Serbian_DJE);

    /**
     * Representation of a Serbian_dze key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_dze = new KeySymbol(_Serbian_dze);

    /**
     * Representation of a Serbian_DZE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_DZE = new KeySymbol(_Serbian_DZE);

    /**
     * Representation of a Serbian_je key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_je = new KeySymbol(_Serbian_je);

    /**
     * Representation of a Serbian_JE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_JE = new KeySymbol(_Serbian_JE);

    /**
     * Representation of a Serbian_lje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_lje = new KeySymbol(_Serbian_lje);

    /**
     * Representation of a Serbian_LJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_LJE = new KeySymbol(_Serbian_LJE);

    /**
     * Representation of a Serbian_nje key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_nje = new KeySymbol(_Serbian_nje);

    /**
     * Representation of a Serbian_NJE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_NJE = new KeySymbol(_Serbian_NJE);

    /**
     * Representation of a Serbian_tshe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_tshe = new KeySymbol(_Serbian_tshe);

    /**
     * Representation of a Serbian_TSHE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Serbian_TSHE = new KeySymbol(_Serbian_TSHE);

    /**
     * Representation of a Setup_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Setup_3270 = new KeySymbol(_Setup_3270);

    /**
     * Representation of a seveneighths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol seveneighths = new KeySymbol(_seveneighths);

    /**
     * Representation of a Shift_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Shift_L = new KeySymbol(_Shift_L);

    /**
     * Representation of a Shift_Lock key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Shift_Lock = new KeySymbol(_Shift_Lock);

    /**
     * Representation of a Shift_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Shift_R = new KeySymbol(_Shift_R);

    /**
     * Representation of a signaturemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol signaturemark = new KeySymbol(_signaturemark);

    /**
     * Representation of a signifblank key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol signifblank = new KeySymbol(_signifblank);

    /**
     * Representation of a similarequal key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol similarequal = new KeySymbol(_similarequal);

    /**
     * Representation of a SingleCandidate key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol SingleCandidate = new KeySymbol(
            _SingleCandidate);

    /**
     * Representation of a singlelowquotemark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol singlelowquotemark = new KeySymbol(
            _singlelowquotemark);

    /**
     * Representation of a slash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol slash = new KeySymbol(_slash);

    /**
     * Representation of a SlowKeys_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol SlowKeys_Enable = new KeySymbol(
            _SlowKeys_Enable);

    /**
     * Representation of a soliddiamond key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol soliddiamond = new KeySymbol(_soliddiamond);

    /**
     * Representation of a space key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol space = new KeySymbol(_space);

    /**
     * Representation of a ssharp key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ssharp = new KeySymbol(_ssharp);

    /**
     * Representation of a sterling key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol sterling = new KeySymbol(_sterling);

    /**
     * Representation of a StickyKeys_Enable key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol StickyKeys_Enable = new KeySymbol(
            _StickyKeys_Enable);

    /**
     * Representation of a Super_L key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Super_L = new KeySymbol(_Super_L);

    /**
     * Representation of a Super_R key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Super_R = new KeySymbol(_Super_R);

    /**
     * Representation of a Sys_Req key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Sys_Req = new KeySymbol(_Sys_Req);

    /**
     * Representation of a t key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol t = new KeySymbol(_t);

    /**
     * Representation of a T key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol T = new KeySymbol(_T);

    /**
     * Representation of a Tab key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Tab = new KeySymbol(_Tab);

    /**
     * Representation of a tcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol tcaron = new KeySymbol(_tcaron);

    /**
     * Representation of a Tcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Tcaron = new KeySymbol(_Tcaron);

    /**
     * Representation of a tcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol tcedilla = new KeySymbol(_tcedilla);

    /**
     * Representation of a Tcedilla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Tcedilla = new KeySymbol(_Tcedilla);

    /**
     * Representation of a telephone key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol telephone = new KeySymbol(_telephone);

    /**
     * Representation of a telephonerecorder key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol telephonerecorder = new KeySymbol(
            _telephonerecorder);

    /**
     * Representation of a Terminate_Server key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Terminate_Server = new KeySymbol(
            _Terminate_Server);

    /**
     * Representation of a Test_3270 key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Test_3270 = new KeySymbol(_Test_3270);

    /**
     * Representation of a Thai_baht key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_baht = new KeySymbol(_Thai_baht);

    /**
     * Representation of a Thai_bobaimai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_bobaimai = new KeySymbol(_Thai_bobaimai);

    /**
     * Representation of a Thai_chochan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_chochan = new KeySymbol(_Thai_chochan);

    /**
     * Representation of a Thai_chochang key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_chochang = new KeySymbol(_Thai_chochang);

    /**
     * Representation of a Thai_choching key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_choching = new KeySymbol(_Thai_choching);

    /**
     * Representation of a Thai_chochoe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_chochoe = new KeySymbol(_Thai_chochoe);

    /**
     * Representation of a Thai_dochada key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_dochada = new KeySymbol(_Thai_dochada);

    /**
     * Representation of a Thai_dodek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_dodek = new KeySymbol(_Thai_dodek);

    /**
     * Representation of a Thai_fofa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_fofa = new KeySymbol(_Thai_fofa);

    /**
     * Representation of a Thai_fofan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_fofan = new KeySymbol(_Thai_fofan);

    /**
     * Representation of a Thai_hohip key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_hohip = new KeySymbol(_Thai_hohip);

    /**
     * Representation of a Thai_honokhuk key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_honokhuk = new KeySymbol(_Thai_honokhuk);

    /**
     * Representation of a Thai_khokhai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_khokhai = new KeySymbol(_Thai_khokhai);

    /**
     * Representation of a Thai_khokhon key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_khokhon = new KeySymbol(_Thai_khokhon);

    /**
     * Representation of a Thai_khokhuat key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_khokhuat = new KeySymbol(_Thai_khokhuat);

    /**
     * Representation of a Thai_khokhwai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_khokhwai = new KeySymbol(_Thai_khokhwai);

    /**
     * Representation of a Thai_khorakhang key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_khorakhang = new KeySymbol(
            _Thai_khorakhang);

    /**
     * Representation of a Thai_kokai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_kokai = new KeySymbol(_Thai_kokai);

    /**
     * Representation of a Thai_lakkhangyao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lakkhangyao = new KeySymbol(
            _Thai_lakkhangyao);

    /**
     * Representation of a Thai_lekchet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lekchet = new KeySymbol(_Thai_lekchet);

    /**
     * Representation of a Thai_lekha key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lekha = new KeySymbol(_Thai_lekha);

    /**
     * Representation of a Thai_lekhok key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lekhok = new KeySymbol(_Thai_lekhok);

    /**
     * Representation of a Thai_lekkao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lekkao = new KeySymbol(_Thai_lekkao);

    /**
     * Representation of a Thai_leknung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_leknung = new KeySymbol(_Thai_leknung);

    /**
     * Representation of a Thai_lekpaet key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lekpaet = new KeySymbol(_Thai_lekpaet);

    /**
     * Representation of a Thai_leksam key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_leksam = new KeySymbol(_Thai_leksam);

    /**
     * Representation of a Thai_leksi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_leksi = new KeySymbol(_Thai_leksi);

    /**
     * Representation of a Thai_leksong key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_leksong = new KeySymbol(_Thai_leksong);

    /**
     * Representation of a Thai_leksun key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_leksun = new KeySymbol(_Thai_leksun);

    /**
     * Representation of a Thai_lochula key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lochula = new KeySymbol(_Thai_lochula);

    /**
     * Representation of a Thai_loling key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_loling = new KeySymbol(_Thai_loling);

    /**
     * Representation of a Thai_lu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_lu = new KeySymbol(_Thai_lu);

    /**
     * Representation of a Thai_maichattawa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maichattawa = new KeySymbol(
            _Thai_maichattawa);

    /**
     * Representation of a Thai_maiek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maiek = new KeySymbol(_Thai_maiek);

    /**
     * Representation of a Thai_maihanakat key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maihanakat = new KeySymbol(
            _Thai_maihanakat);

    /**
     * Representation of a Thai_maihanakat_maitho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maihanakat_maitho = new KeySymbol(
            _Thai_maihanakat_maitho);

    /**
     * Representation of a Thai_maitaikhu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maitaikhu = new KeySymbol(
            _Thai_maitaikhu);

    /**
     * Representation of a Thai_maitho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maitho = new KeySymbol(_Thai_maitho);

    /**
     * Representation of a Thai_maitri key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maitri = new KeySymbol(_Thai_maitri);

    /**
     * Representation of a Thai_maiyamok key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_maiyamok = new KeySymbol(_Thai_maiyamok);

    /**
     * Representation of a Thai_moma key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_moma = new KeySymbol(_Thai_moma);

    /**
     * Representation of a Thai_ngongu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_ngongu = new KeySymbol(_Thai_ngongu);

    /**
     * Representation of a Thai_nikhahit key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_nikhahit = new KeySymbol(_Thai_nikhahit);

    /**
     * Representation of a Thai_nonen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_nonen = new KeySymbol(_Thai_nonen);

    /**
     * Representation of a Thai_nonu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_nonu = new KeySymbol(_Thai_nonu);

    /**
     * Representation of a Thai_oang key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_oang = new KeySymbol(_Thai_oang);

    /**
     * Representation of a Thai_paiyannoi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_paiyannoi = new KeySymbol(
            _Thai_paiyannoi);

    /**
     * Representation of a Thai_phinthu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_phinthu = new KeySymbol(_Thai_phinthu);

    /**
     * Representation of a Thai_phophan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_phophan = new KeySymbol(_Thai_phophan);

    /**
     * Representation of a Thai_phophung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_phophung = new KeySymbol(_Thai_phophung);

    /**
     * Representation of a Thai_phosamphao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_phosamphao = new KeySymbol(
            _Thai_phosamphao);

    /**
     * Representation of a Thai_popla key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_popla = new KeySymbol(_Thai_popla);

    /**
     * Representation of a Thai_rorua key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_rorua = new KeySymbol(_Thai_rorua);

    /**
     * Representation of a Thai_ru key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_ru = new KeySymbol(_Thai_ru);

    /**
     * Representation of a Thai_saraa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraa = new KeySymbol(_Thai_saraa);

    /**
     * Representation of a Thai_saraaa key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraaa = new KeySymbol(_Thai_saraaa);

    /**
     * Representation of a Thai_saraae key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraae = new KeySymbol(_Thai_saraae);

    /**
     * Representation of a Thai_saraaimaimalai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraaimaimalai = new KeySymbol(
            _Thai_saraaimaimalai);

    /**
     * Representation of a Thai_saraaimaimuan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraaimaimuan = new KeySymbol(
            _Thai_saraaimaimuan);

    /**
     * Representation of a Thai_saraam key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraam = new KeySymbol(_Thai_saraam);

    /**
     * Representation of a Thai_sarae key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarae = new KeySymbol(_Thai_sarae);

    /**
     * Representation of a Thai_sarai key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarai = new KeySymbol(_Thai_sarai);

    /**
     * Representation of a Thai_saraii key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraii = new KeySymbol(_Thai_saraii);

    /**
     * Representation of a Thai_sarao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarao = new KeySymbol(_Thai_sarao);

    /**
     * Representation of a Thai_sarau key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarau = new KeySymbol(_Thai_sarau);

    /**
     * Representation of a Thai_saraue key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_saraue = new KeySymbol(_Thai_saraue);

    /**
     * Representation of a Thai_sarauee key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarauee = new KeySymbol(_Thai_sarauee);

    /**
     * Representation of a Thai_sarauu key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sarauu = new KeySymbol(_Thai_sarauu);

    /**
     * Representation of a Thai_sorusi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sorusi = new KeySymbol(_Thai_sorusi);

    /**
     * Representation of a Thai_sosala key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sosala = new KeySymbol(_Thai_sosala);

    /**
     * Representation of a Thai_soso key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_soso = new KeySymbol(_Thai_soso);

    /**
     * Representation of a Thai_sosua key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_sosua = new KeySymbol(_Thai_sosua);

    /**
     * Representation of a Thai_thanthakhat key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thanthakhat = new KeySymbol(
            _Thai_thanthakhat);

    /**
     * Representation of a Thai_thonangmontho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thonangmontho = new KeySymbol(
            _Thai_thonangmontho);

    /**
     * Representation of a Thai_thophuthao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thophuthao = new KeySymbol(
            _Thai_thophuthao);

    /**
     * Representation of a Thai_thothahan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thothahan = new KeySymbol(
            _Thai_thothahan);

    /**
     * Representation of a Thai_thothan key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thothan = new KeySymbol(_Thai_thothan);

    /**
     * Representation of a Thai_thothong key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thothong = new KeySymbol(_Thai_thothong);

    /**
     * Representation of a Thai_thothung key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_thothung = new KeySymbol(_Thai_thothung);

    /**
     * Representation of a Thai_topatak key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_topatak = new KeySymbol(_Thai_topatak);

    /**
     * Representation of a Thai_totao key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_totao = new KeySymbol(_Thai_totao);

    /**
     * Representation of a Thai_wowaen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_wowaen = new KeySymbol(_Thai_wowaen);

    /**
     * Representation of a Thai_yoyak key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_yoyak = new KeySymbol(_Thai_yoyak);

    /**
     * Representation of a Thai_yoying key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thai_yoying = new KeySymbol(_Thai_yoying);

    /**
     * Representation of a therefore key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol therefore = new KeySymbol(_therefore);

    /**
     * Representation of a thinspace key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol thinspace = new KeySymbol(_thinspace);

    /**
     * Representation of a thorn key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol thorn = new KeySymbol(_thorn);

    /**
     * Representation of a Thorn key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Thorn = new KeySymbol(_Thorn);

    /**
     * Representation of a THORN key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol THORN = new KeySymbol(_THORN);

    /**
     * Representation of a threeeighths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol threeeighths = new KeySymbol(_threeeighths);

    /**
     * Representation of a threefifths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol threefifths = new KeySymbol(_threefifths);

    /**
     * Representation of a threequarters key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol threequarters = new KeySymbol(_threequarters);

    /**
     * Representation of a threesuperior key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol threesuperior = new KeySymbol(_threesuperior);

    /**
     * Representation of a topintegral key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topintegral = new KeySymbol(_topintegral);

    /**
     * Representation of a topleftparens key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topleftparens = new KeySymbol(_topleftparens);

    /**
     * Representation of a topleftradical key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topleftradical = new KeySymbol(
            _topleftradical);

    /**
     * Representation of a topleftsqbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topleftsqbracket = new KeySymbol(
            _topleftsqbracket);

    /**
     * Representation of a topleftsummation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topleftsummation = new KeySymbol(
            _topleftsummation);

    /**
     * Representation of a toprightparens key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol toprightparens = new KeySymbol(
            _toprightparens);

    /**
     * Representation of a toprightsqbracket key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol toprightsqbracket = new KeySymbol(
            _toprightsqbracket);

    /**
     * Representation of a toprightsummation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol toprightsummation = new KeySymbol(
            _toprightsummation);

    /**
     * Representation of a topt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topt = new KeySymbol(_topt);

    /**
     * Representation of a topvertsummationconnector key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol topvertsummationconnector = new KeySymbol(
            _topvertsummationconnector);

    /**
     * Representation of a Touroku key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Touroku = new KeySymbol(_Touroku);

    /**
     * Representation of a trademark key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol trademark = new KeySymbol(_trademark);

    /**
     * Representation of a trademarkincircle key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol trademarkincircle = new KeySymbol(
            _trademarkincircle);

    /**
     * Representation of a tslash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol tslash = new KeySymbol(_tslash);

    /**
     * Representation of a Tslash key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Tslash = new KeySymbol(_Tslash);

    /**
     * Representation of a twofifths key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol twofifths = new KeySymbol(_twofifths);

    /**
     * Representation of a twosuperior key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol twosuperior = new KeySymbol(_twosuperior);

    /**
     * Representation of a twothirds key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol twothirds = new KeySymbol(_twothirds);

    /**
     * Representation of a u key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol u = new KeySymbol(_u);

    /**
     * Representation of a U key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol U = new KeySymbol(_U);

    /**
     * Representation of a uacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uacute = new KeySymbol(_uacute);

    /**
     * Representation of a Uacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Uacute = new KeySymbol(_Uacute);

    /**
     * Representation of a ubreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ubreve = new KeySymbol(_ubreve);

    /**
     * Representation of a Ubreve key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ubreve = new KeySymbol(_Ubreve);

    /**
     * Representation of a ucircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ucircumflex = new KeySymbol(_ucircumflex);

    /**
     * Representation of a Ucircumflex key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ucircumflex = new KeySymbol(_Ucircumflex);

    /**
     * Representation of a udiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol udiaeresis = new KeySymbol(_udiaeresis);

    /**
     * Representation of a Udiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Udiaeresis = new KeySymbol(_Udiaeresis);

    /**
     * Representation of a udoubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol udoubleacute = new KeySymbol(_udoubleacute);

    /**
     * Representation of a Udoubleacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Udoubleacute = new KeySymbol(_Udoubleacute);

    /**
     * Representation of a ugrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ugrave = new KeySymbol(_ugrave);

    /**
     * Representation of a Ugrave key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ugrave = new KeySymbol(_Ugrave);

    /**
     * Representation of a Ukrainian_i key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_i = new KeySymbol(_Ukrainian_i);

    /**
     * Representation of a Ukrainian_I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_I = new KeySymbol(_Ukrainian_I);

    /**
     * Representation of a Ukrainian_ie key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_ie = new KeySymbol(_Ukrainian_ie);

    /**
     * Representation of a Ukrainian_IE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_IE = new KeySymbol(_Ukrainian_IE);

    /**
     * Representation of a Ukrainian_yi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_yi = new KeySymbol(_Ukrainian_yi);

    /**
     * Representation of a Ukrainian_YI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukrainian_YI = new KeySymbol(_Ukrainian_YI);

    /**
     * Representation of a Ukranian_i key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_i = new KeySymbol(_Ukranian_i);

    /**
     * Representation of a Ukranian_I key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_I = new KeySymbol(_Ukranian_I);

    /**
     * Representation of a Ukranian_je key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_je = new KeySymbol(_Ukranian_je);

    /**
     * Representation of a Ukranian_JE key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_JE = new KeySymbol(_Ukranian_JE);

    /**
     * Representation of a Ukranian_yi key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_yi = new KeySymbol(_Ukranian_yi);

    /**
     * Representation of a Ukranian_YI key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ukranian_YI = new KeySymbol(_Ukranian_YI);

    /**
     * Representation of a umacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol umacron = new KeySymbol(_umacron);

    /**
     * Representation of a Umacron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Umacron = new KeySymbol(_Umacron);

    /**
     * Representation of a underbar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol underbar = new KeySymbol(_underbar);

    /**
     * Representation of a underscore key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol underscore = new KeySymbol(_underscore);

    /**
     * Representation of a Undo key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Undo = new KeySymbol(_Undo);

    /**
     * Representation of a union key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol union = new KeySymbol(_union);

    /**
     * Representation of a uogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uogonek = new KeySymbol(_uogonek);

    /**
     * Representation of a Uogonek key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Uogonek = new KeySymbol(_Uogonek);

    /**
     * Representation of a Up key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Up = new KeySymbol(_Up);

    /**
     * Representation of a uparrow key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uparrow = new KeySymbol(_uparrow);

    /**
     * Representation of a upcaret key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol upcaret = new KeySymbol(_upcaret);

    /**
     * Representation of a upleftcorner key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol upleftcorner = new KeySymbol(_upleftcorner);

    /**
     * Representation of a uprightcorner key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uprightcorner = new KeySymbol(_uprightcorner);

    /**
     * Representation of a upshoe key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol upshoe = new KeySymbol(_upshoe);

    /**
     * Representation of a upstile key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol upstile = new KeySymbol(_upstile);

    /**
     * Representation of a uptack key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uptack = new KeySymbol(_uptack);

    /**
     * Representation of a uring key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol uring = new KeySymbol(_uring);

    /**
     * Representation of a Uring key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Uring = new KeySymbol(_Uring);

    /**
     * Representation of a utilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol utilde = new KeySymbol(_utilde);

    /**
     * Representation of a Utilde key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Utilde = new KeySymbol(_Utilde);

    /**
     * Representation of a v key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol v = new KeySymbol(_v);

    /**
     * Representation of a V key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol V = new KeySymbol(_V);

    /**
     * Representation of a variation key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol variation = new KeySymbol(_variation);

    /**
     * Representation of a vertbar key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol vertbar = new KeySymbol(_vertbar);

    /**
     * Representation of a vertconnector key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol vertconnector = new KeySymbol(_vertconnector);

    /**
     * Representation of a voicedsound key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol voicedsound = new KeySymbol(_voicedsound);

    /**
     * Representation of a vt key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol vt = new KeySymbol(_vt);

    /**
     * Representation of a w key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol w = new KeySymbol(_w);

    /**
     * Representation of a W key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol W = new KeySymbol(_W);

    /**
     * Representation of a WonSign key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol WonSign = new KeySymbol(_WonSign);

    /**
     * Representation of a x key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol x = new KeySymbol(_x);

    /**
     * Representation of a X key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol X = new KeySymbol(_X);

    /**
     * Representation of a y key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol y = new KeySymbol(_y);

    /**
     * Representation of a Y key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Y = new KeySymbol(_Y);

    /**
     * Representation of a yacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol yacute = new KeySymbol(_yacute);

    /**
     * Representation of a Yacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Yacute = new KeySymbol(_Yacute);

    /**
     * Representation of a ydiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol ydiaeresis = new KeySymbol(_ydiaeresis);

    /**
     * Representation of a Ydiaeresis key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Ydiaeresis = new KeySymbol(_Ydiaeresis);

    /**
     * Representation of a yen key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol yen = new KeySymbol(_yen);

    /**
     * Representation of a z key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol z = new KeySymbol(_z);

    /**
     * Representation of a Z key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Z = new KeySymbol(_Z);

    /**
     * Representation of a zabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol zabovedot = new KeySymbol(_zabovedot);

    /**
     * Representation of a Zabovedot key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zabovedot = new KeySymbol(_Zabovedot);

    /**
     * Representation of a zacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol zacute = new KeySymbol(_zacute);

    /**
     * Representation of a Zacute key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zacute = new KeySymbol(_Zacute);

    /**
     * Representation of a zcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol zcaron = new KeySymbol(_zcaron);

    /**
     * Representation of a Zcaron key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zcaron = new KeySymbol(_Zcaron);

    /**
     * Representation of a Zen_Koho key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zen_Koho = new KeySymbol(_Zen_Koho);

    /**
     * Representation of a Zenkaku key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zenkaku = new KeySymbol(_Zenkaku);

    /**
     * Representation of a Zenkaku_Hankaku key.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final KeySymbol Zenkaku_Hankaku = new KeySymbol(
            _Zenkaku_Hankaku);

    /**
     * Returns the internal representation of value as a KeySymbol
     * 
     * @param value
     *            The gtk integer representation of the key
     * @return The KeySymbol associated with the key
     * 
     * @since 2.8.1
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static KeySymbol intern(int value) {

        return null;
    }

    private KeySymbol(int value) {
        value_ = value;
    }

}
