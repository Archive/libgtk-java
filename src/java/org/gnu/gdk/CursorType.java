/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class CursorType extends Enum {
    static final private int _X_CURSOR = 0;

    static final public CursorType X_CURSOR = new CursorType(_X_CURSOR);

    static final private int _ARROW = 2;

    static final public CursorType ARROW = new CursorType(_ARROW);

    static final private int _BASED_ARROW_DOWN = 4;

    static final public CursorType BASED_ARROW_DOWN = new CursorType(
            _BASED_ARROW_DOWN);

    static final private int _BASED_ARROW_UP = 6;

    static final public CursorType BASED_ARROW_UP = new CursorType(
            _BASED_ARROW_UP);

    static final private int _BOAT = 8;

    static final public CursorType BOAT = new CursorType(_BOAT);

    static final private int _BOGOSITY = 10;

    static final public CursorType BOGOSITY = new CursorType(_BOGOSITY);

    static final private int _BOTTOM_LEFT_CORNER = 12;

    static final public CursorType BOTTOM_LEFT_CORNER = new CursorType(
            _BOTTOM_LEFT_CORNER);

    static final private int _BOTTOM_RIGHT_CORNER = 14;

    static final public CursorType BOTTOM_RIGHT_CORNER = new CursorType(
            _BOTTOM_RIGHT_CORNER);

    static final private int _BOTTOM_SIDE = 16;

    static final public CursorType BOTTOM_SIDE = new CursorType(_BOTTOM_SIDE);

    static final private int _BOTTOM_TEE = 18;

    static final public CursorType BOTTOM_TEE = new CursorType(_BOTTOM_TEE);

    static final private int _BOX_SPIRAL = 20;

    static final public CursorType BOX_SPIRAL = new CursorType(_BOX_SPIRAL);

    static final private int _CENTER_PTR = 22;

    static final public CursorType CENTER_PTR = new CursorType(_CENTER_PTR);

    static final private int _CIRCLE = 24;

    static final public CursorType CIRCLE = new CursorType(_CIRCLE);

    static final private int _CLOCK = 26;

    static final public CursorType CLOCK = new CursorType(_CLOCK);

    static final private int _COFFEE_MUG = 28;

    static final public CursorType COFFEE_MUG = new CursorType(_COFFEE_MUG);

    static final private int _CROSS = 30;

    static final public CursorType CROSS = new CursorType(_CROSS);

    static final private int _CROSS_REVERSE = 32;

    static final public CursorType CROSS_REVERSE = new CursorType(
            _CROSS_REVERSE);

    static final private int _CROSSHAIR = 34;

    static final public CursorType CROSSHAIR = new CursorType(_CROSSHAIR);

    static final private int _DIAMOND_CROSS = 36;

    static final public CursorType DIAMOND_CROSS = new CursorType(
            _DIAMOND_CROSS);

    static final private int _DOT = 38;

    static final public CursorType DOT = new CursorType(_DOT);

    static final private int _DOTBOX = 40;

    static final public CursorType DOTBOX = new CursorType(_DOTBOX);

    static final private int _DOUBLE_ARROW = 42;

    static final public CursorType DOUBLE_ARROW = new CursorType(_DOUBLE_ARROW);

    static final private int _DRAFT_LARGE = 44;

    static final public CursorType DRAFT_LARGE = new CursorType(_DRAFT_LARGE);

    static final private int _DRAFT_SMALL = 46;

    static final public CursorType DRAFT_SMALL = new CursorType(_DRAFT_SMALL);

    static final private int _DRAPED_BOX = 48;

    static final public CursorType DRAPED_BOX = new CursorType(_DRAPED_BOX);

    static final private int _EXCHANGE = 50;

    static final public CursorType EXCHANGE = new CursorType(_EXCHANGE);

    static final private int _FLEUR = 52;

    static final public CursorType FLEUR = new CursorType(_FLEUR);

    static final private int _GOBBLER = 54;

    static final public CursorType GOBBLER = new CursorType(_GOBBLER);

    static final private int _GUMBY = 56;

    static final public CursorType GUMBY = new CursorType(_GUMBY);

    static final private int _HAND1 = 58;

    static final public CursorType HAND1 = new CursorType(_HAND1);

    static final private int _HAND2 = 60;

    static final public CursorType HAND2 = new CursorType(_HAND2);

    static final private int _HEART = 62;

    static final public CursorType HEART = new CursorType(_HEART);

    static final private int _ICON = 64;

    static final public CursorType ICON = new CursorType(_ICON);

    static final private int _IRON_CROSS = 66;

    static final public CursorType IRON_CROSS = new CursorType(_IRON_CROSS);

    static final private int _LEFT_PTR = 68;

    static final public CursorType LEFT_PTR = new CursorType(_LEFT_PTR);

    static final private int _LEFT_SIDE = 70;

    static final public CursorType LEFT_SIDE = new CursorType(_LEFT_SIDE);

    static final private int _LEFT_TEE = 72;

    static final public CursorType LEFT_TEE = new CursorType(_LEFT_TEE);

    static final private int _LEFTBUTTON = 74;

    static final public CursorType LEFTBUTTON = new CursorType(_LEFTBUTTON);

    static final private int _LL_ANGLE = 76;

    static final public CursorType LL_ANGLE = new CursorType(_LL_ANGLE);

    static final private int _LR_ANGLE = 78;

    static final public CursorType LR_ANGLE = new CursorType(_LR_ANGLE);

    static final private int _MAN = 80;

    static final public CursorType MAN = new CursorType(_MAN);

    static final private int _MIDDLEBUTTON = 82;

    static final public CursorType MIDDLEBUTTON = new CursorType(_MIDDLEBUTTON);

    static final private int _MOUSE = 84;

    static final public CursorType MOUSE = new CursorType(_MOUSE);

    static final private int _PENCIL = 86;

    static final public CursorType PENCIL = new CursorType(_PENCIL);

    static final private int _PIRATE = 88;

    static final public CursorType PIRATE = new CursorType(_PIRATE);

    static final private int _PLUS = 90;

    static final public CursorType PLUS = new CursorType(_PLUS);

    static final private int _QUESTION_ARROW = 92;

    static final public CursorType QUESTION_ARROW = new CursorType(
            _QUESTION_ARROW);

    static final private int _RIGHT_PTR = 94;

    static final public CursorType RIGHT_PTR = new CursorType(_RIGHT_PTR);

    static final private int _RIGHT_SIDE = 96;

    static final public CursorType RIGHT_SIDE = new CursorType(_RIGHT_SIDE);

    static final private int _RIGHT_TEE = 98;

    static final public CursorType RIGHT_TEE = new CursorType(_RIGHT_TEE);

    static final private int _RIGHTBUTTON = 100;

    static final public CursorType RIGHTBUTTON = new CursorType(_RIGHTBUTTON);

    static final private int _RTL_LOGO = 102;

    static final public CursorType RTL_LOGO = new CursorType(_RTL_LOGO);

    static final private int _SAILBOAT = 104;

    static final public CursorType SAILBOAT = new CursorType(_SAILBOAT);

    static final private int _SB_DOWN_ARROW = 106;

    static final public CursorType SB_DOWN_ARROW = new CursorType(
            _SB_DOWN_ARROW);

    static final private int _SB_H_DOUBLE_ARROW = 108;

    static final public CursorType SB_H_DOUBLE_ARROW = new CursorType(
            _SB_H_DOUBLE_ARROW);

    static final private int _SB_LEFT_ARROW = 110;

    static final public CursorType SB_LEFT_ARROW = new CursorType(
            _SB_LEFT_ARROW);

    static final private int _SB_RIGHT_ARROW = 112;

    static final public CursorType SB_RIGHT_ARROW = new CursorType(
            _SB_RIGHT_ARROW);

    static final private int _SB_UP_ARROW = 114;

    static final public CursorType SB_UP_ARROW = new CursorType(_SB_UP_ARROW);

    static final private int _SB_V_DOUBLE_ARROW = 116;

    static final public CursorType SB_V_DOUBLE_ARROW = new CursorType(
            _SB_V_DOUBLE_ARROW);

    static final private int _SHUTTLE = 118;

    static final public CursorType SHUTTLE = new CursorType(_SHUTTLE);

    static final private int _SIZING = 120;

    static final public CursorType SIZING = new CursorType(_SIZING);

    static final private int _SPIDER = 122;

    static final public CursorType SPIDER = new CursorType(_SPIDER);

    static final private int _SPARYCAN = 124;

    static final public CursorType SPARYCAN = new CursorType(_SPARYCAN);

    static final private int _STAR = 126;

    static final public CursorType STAR = new CursorType(_STAR);

    static final private int _TARGET = 128;

    static final public CursorType TARGET = new CursorType(_TARGET);

    static final private int _TCROSS = 130;

    static final public CursorType TCROSS = new CursorType(_TCROSS);

    static final private int _TOP_LEFT_ARROW = 132;

    static final public CursorType TOP_LEFT_ARROW = new CursorType(
            _TOP_LEFT_ARROW);

    static final private int _TOP_LEFT_CORNER = 134;

    static final public CursorType TOP_LEFT_CORNER = new CursorType(
            _TOP_LEFT_CORNER);

    static final private int _TOP_RIGHT_CORNER = 136;

    static final public CursorType TOP_RIGHT_CORNER = new CursorType(
            _TOP_RIGHT_CORNER);

    static final private int _TOP_SIDE = 138;

    static final public CursorType TOP_SIDE = new CursorType(_TOP_SIDE);

    static final private int _TOP_TEE = 140;

    static final public CursorType TOP_TEE = new CursorType(_TOP_TEE);

    static final private int _TREK = 142;

    static final public CursorType TREK = new CursorType(_TREK);

    static final private int _UL_ANGLE = 144;

    static final public CursorType UL_ANGLE = new CursorType(_UL_ANGLE);

    static final private int _UMBRELLA = 146;

    static final public CursorType UMBRELLA = new CursorType(_UMBRELLA);

    static final private int _UR_ANGLE = 148;

    static final public CursorType UR_ANGLE = new CursorType(_UR_ANGLE);

    static final private int _WATCH = 150;

    static final public CursorType WATCH = new CursorType(_WATCH);

    static final private int _XTERM = 152;

    static final public CursorType XTERM = new CursorType(_XTERM);

    static final private int _LAST_CURSOR = 153;

    static final public CursorType LAST_CURSOR = new CursorType(_LAST_CURSOR);

    static final private int _CURSOR_IS_PIXMAP = -1;

    static final public CursorType CURSOR_IS_PIXMAP = new CursorType(
            _CURSOR_IS_PIXMAP);

    static final private CursorType[] theInterned = new CursorType[] {
            X_CURSOR, new CursorType(1), ARROW, new CursorType(3),
            BASED_ARROW_DOWN, new CursorType(5), BASED_ARROW_UP,
            new CursorType(7), BOAT, new CursorType(9), BOGOSITY,
            new CursorType(11), BOTTOM_LEFT_CORNER, new CursorType(13),
            BOTTOM_RIGHT_CORNER, new CursorType(15), BOTTOM_SIDE,
            new CursorType(17), BOTTOM_TEE, new CursorType(19), BOX_SPIRAL,
            new CursorType(21), CENTER_PTR, new CursorType(23), CIRCLE,
            new CursorType(25), CLOCK, new CursorType(27), COFFEE_MUG,
            new CursorType(29), CROSS, new CursorType(31), CROSS_REVERSE,
            new CursorType(33), CROSSHAIR, new CursorType(35), DIAMOND_CROSS,
            new CursorType(37), DOT, new CursorType(39), DOTBOX,
            new CursorType(41), DOUBLE_ARROW, new CursorType(43), DRAFT_LARGE,
            new CursorType(45), DRAFT_SMALL, new CursorType(47), DRAPED_BOX,
            new CursorType(49), EXCHANGE, new CursorType(51), FLEUR,
            new CursorType(53), GOBBLER, new CursorType(55), GUMBY,
            new CursorType(57), HAND1, new CursorType(59), HAND2,
            new CursorType(61), HEART, new CursorType(63), ICON,
            new CursorType(65), IRON_CROSS, new CursorType(67), LEFT_PTR,
            new CursorType(69), LEFT_SIDE, new CursorType(71), LEFT_TEE,
            new CursorType(73), LEFTBUTTON, new CursorType(75), LL_ANGLE,
            new CursorType(77), LR_ANGLE, new CursorType(79), MAN,
            new CursorType(81), MIDDLEBUTTON, new CursorType(83), MOUSE,
            new CursorType(85), PENCIL, new CursorType(87), PIRATE,
            new CursorType(89), PLUS, new CursorType(91), QUESTION_ARROW,
            new CursorType(93), RIGHT_PTR, new CursorType(95), RIGHT_SIDE,
            new CursorType(97), RIGHT_TEE, new CursorType(99), RIGHTBUTTON,
            new CursorType(101), RTL_LOGO, new CursorType(103), SAILBOAT,
            new CursorType(105), SB_DOWN_ARROW, new CursorType(107),
            SB_H_DOUBLE_ARROW, new CursorType(109), SB_LEFT_ARROW,
            new CursorType(111), SB_RIGHT_ARROW, new CursorType(113),
            SB_UP_ARROW, new CursorType(115), SB_V_DOUBLE_ARROW,
            new CursorType(117), SHUTTLE, new CursorType(119), SIZING,
            new CursorType(121), SPIDER, new CursorType(123), SPARYCAN,
            new CursorType(125), STAR, new CursorType(127), TARGET,
            new CursorType(129), TCROSS, new CursorType(131), TOP_LEFT_ARROW,
            new CursorType(133), TOP_LEFT_CORNER, new CursorType(135),
            TOP_RIGHT_CORNER, new CursorType(137), TOP_SIDE,
            new CursorType(139), TOP_TEE, new CursorType(141), TREK,
            new CursorType(143), UL_ANGLE, new CursorType(145), UMBRELLA,
            new CursorType(147), UR_ANGLE, new CursorType(149), WATCH,
            new CursorType(151), XTERM, LAST_CURSOR };

    static private java.util.Hashtable theInternedExtras;

    static final private CursorType theSacrificialOne = new CursorType(0);

    static public CursorType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        CursorType already = (CursorType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new CursorType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CursorType(int value) {
        value_ = value;
    }

    public CursorType or(CursorType other) {
        return intern(value_ | other.value_);
    }

    public CursorType and(CursorType other) {
        return intern(value_ & other.value_);
    }

    public CursorType xor(CursorType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(CursorType other) {
        return (value_ & other.value_) == other.value_;
    }

}
