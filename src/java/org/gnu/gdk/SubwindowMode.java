/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class SubwindowMode extends Enum {

    static final private int _CLIP_BY_CHILDREN = 0;

    /**
     * Only draw onto the Window itself.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.SubwindowMode CLIP_BY_CHILDREN = new org.gnu.gdk.SubwindowMode(
            _CLIP_BY_CHILDREN);

    static final private int _INCLUDE_INFERIORS = 1;

    /**
     * Draw onto the Window and child Windows.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.SubwindowMode INCLUDE_INFERIORS = new org.gnu.gdk.SubwindowMode(
            _INCLUDE_INFERIORS);

    static final private org.gnu.gdk.SubwindowMode[] theInterned = new org.gnu.gdk.SubwindowMode[] {
            CLIP_BY_CHILDREN, INCLUDE_INFERIORS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.SubwindowMode theSacrificialOne = new org.gnu.gdk.SubwindowMode(
            0);

    static public org.gnu.gdk.SubwindowMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.SubwindowMode already = (org.gnu.gdk.SubwindowMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.SubwindowMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SubwindowMode(int value) {
        value_ = value;
    }

    public org.gnu.gdk.SubwindowMode or(org.gnu.gdk.SubwindowMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.SubwindowMode and(org.gnu.gdk.SubwindowMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.SubwindowMode xor(org.gnu.gdk.SubwindowMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.SubwindowMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
