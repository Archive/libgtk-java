/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class Colorspace extends Enum {
    static final private int _RGB = 0;

    static final public Colorspace RGB = new Colorspace(_RGB);

    static final private Colorspace[] theInterned = new Colorspace[] { RGB };

    static private java.util.Hashtable theInternedExtras;

    static final private Colorspace theSacrificialOne = new Colorspace(0);

    static public Colorspace intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Colorspace already = (Colorspace) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new Colorspace(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Colorspace(int value) {
        value_ = value;
    }

    public Colorspace or(Colorspace other) {
        return intern(value_ | other.value_);
    }

    public Colorspace and(Colorspace other) {
        return intern(value_ & other.value_);
    }

    public Colorspace xor(Colorspace other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Colorspace other) {
        return (value_ & other.value_) == other.value_;
    }

}
