/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class WindowTypeHint extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NORMAL = 0;

    static final public org.gnu.gdk.WindowTypeHint NORMAL = new org.gnu.gdk.WindowTypeHint(
            _NORMAL);

    static final private int _DIALOG = 1;

    static final public org.gnu.gdk.WindowTypeHint DIALOG = new org.gnu.gdk.WindowTypeHint(
            _DIALOG);

    static final private int _MENU = 2;

    static final public org.gnu.gdk.WindowTypeHint MENU = new org.gnu.gdk.WindowTypeHint(
            _MENU);

    static final private int _TOOLBAR = 3;

    static final public org.gnu.gdk.WindowTypeHint TOOLBAR = new org.gnu.gdk.WindowTypeHint(
            _TOOLBAR);

    static final private org.gnu.gdk.WindowTypeHint[] theInterned = new org.gnu.gdk.WindowTypeHint[] {
            NORMAL, DIALOG, MENU, TOOLBAR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowTypeHint theSacrificialOne = new org.gnu.gdk.WindowTypeHint(
            0);

    static public org.gnu.gdk.WindowTypeHint intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowTypeHint already = (org.gnu.gdk.WindowTypeHint) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowTypeHint(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowTypeHint(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowTypeHint or(org.gnu.gdk.WindowTypeHint other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowTypeHint and(org.gnu.gdk.WindowTypeHint other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowTypeHint xor(org.gnu.gdk.WindowTypeHint other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowTypeHint other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
