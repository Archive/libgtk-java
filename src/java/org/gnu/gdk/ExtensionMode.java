/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

/**
 * An enumeration used to specify which extension events are desired for a
 * particular widget.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.ExtensionMode</code>.
 */
public class ExtensionMode extends Enum {
    static final private int _EVENTS_NONE = 0;

    /**
     * No extension events are desired.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.ExtensionMode EVENTS_NONE = new org.gnu.gdk.ExtensionMode(
            _EVENTS_NONE);

    static final private int _EVENTS_ALL = 1;

    /**
     * All extension events are desired.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.ExtensionMode EVENTS_ALL = new org.gnu.gdk.ExtensionMode(
            _EVENTS_ALL);

    static final private int _EVENTS_CURSOR = 2;

    /**
     * Extension events are desired only if a cursor will be displayed for the
     * device.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.ExtensionMode EVENTS_CURSOR = new org.gnu.gdk.ExtensionMode(
            _EVENTS_CURSOR);

    static final private org.gnu.gdk.ExtensionMode[] theInterned = new org.gnu.gdk.ExtensionMode[] {
            EVENTS_NONE, EVENTS_ALL, EVENTS_CURSOR };

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.ExtensionMode theSacrificialOne = new org.gnu.gdk.ExtensionMode(
            0);

    static public org.gnu.gdk.ExtensionMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.ExtensionMode already = (org.gnu.gdk.ExtensionMode) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.ExtensionMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ExtensionMode(int value) {
        value_ = value;
    }

    public org.gnu.gdk.ExtensionMode or(org.gnu.gdk.ExtensionMode other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.ExtensionMode and(org.gnu.gdk.ExtensionMode other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.ExtensionMode xor(org.gnu.gdk.ExtensionMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.ExtensionMode other) {
        return (value_ & other.value_) == other.value_;
    }
}
