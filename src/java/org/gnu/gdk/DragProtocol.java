/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class DragProtocol extends Enum {
    static final private int _MOTIF = 0;

    static final public DragProtocol MOTIF = new DragProtocol(_MOTIF);

    static final private int _XDND = 1;

    static final public DragProtocol XDND = new DragProtocol(_XDND);

    static final private int _ROOTWIN = 2;

    static final public DragProtocol ROOTWIN = new DragProtocol(_ROOTWIN);

    static final private int _NONE = 3;

    static final public DragProtocol NONE = new DragProtocol(_NONE);

    static final private int _WIN32_DROPFILES = 4;

    static final public DragProtocol WIN32_DROPFILES = new DragProtocol(
            _WIN32_DROPFILES);

    static final private int _OLE2 = 5;

    static final public DragProtocol OLE2 = new DragProtocol(_OLE2);

    static final private int _LOCAL = 6;

    static final public DragProtocol LOCAL = new DragProtocol(_LOCAL);

    static final private DragProtocol[] theInterned = new DragProtocol[] {
            MOTIF, XDND, ROOTWIN, NONE, WIN32_DROPFILES, OLE2, LOCAL };

    static private java.util.Hashtable theInternedExtras;

    static final private DragProtocol theSacrificialOne = new DragProtocol(0);

    static public DragProtocol intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        DragProtocol already = (DragProtocol) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new DragProtocol(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DragProtocol(int value) {
        value_ = value;
    }

    public DragProtocol or(DragProtocol other) {
        return intern(value_ | other.value_);
    }

    public DragProtocol and(DragProtocol other) {
        return intern(value_ & other.value_);
    }

    public DragProtocol xor(DragProtocol other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(DragProtocol other) {
        return (value_ & other.value_) == other.value_;
    }

}
