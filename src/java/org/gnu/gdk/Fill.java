/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class Fill extends Enum {

    static final private int _SOLID = 0;

    /**
     * Draw with the foreground color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.Fill SOLID = new org.gnu.gdk.Fill(_SOLID);

    static final private int _TILED = 1;

    /**
     * Draw with a tiled pixmap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.Fill TILED = new org.gnu.gdk.Fill(_TILED);

    static final private int _STIPPLED = 2;

    /**
     * Draw using a stipple bitmap. Pixels corresponding to bits in the stipple
     * bitmap that are set will be drawn in the foreground color; pixels
     * corresponding to bits that are not set will be left untouched.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.Fill STIPPLED = new org.gnu.gdk.Fill(
            _STIPPLED);

    static final private int _OPAQUE_STIPPLED = 3;

    /**
     * Draw using the stipple bitmap. Pixels corresponding to bits in the
     * stipple bitmap that are set will be drawn in the foreground color; pixels
     * corresponding to bits that are not set will be drawn with the background
     * color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.Fill OPAQUE_STIPPLED = new org.gnu.gdk.Fill(
            _OPAQUE_STIPPLED);

    static final private org.gnu.gdk.Fill[] theInterned = new org.gnu.gdk.Fill[] {
            SOLID, TILED, STIPPLED, OPAQUE_STIPPLED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.Fill theSacrificialOne = new org.gnu.gdk.Fill(
            0);

    static public org.gnu.gdk.Fill intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.Fill already = (org.gnu.gdk.Fill) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.Fill(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Fill(int value) {
        value_ = value;
    }

    public org.gnu.gdk.Fill or(org.gnu.gdk.Fill other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.Fill and(org.gnu.gdk.Fill other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.Fill xor(org.gnu.gdk.Fill other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.Fill other) {
        return (value_ & other.value_) == other.value_;
    }

}
