/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class InterpType extends Enum {

    static final private int _NEAREST = 0;

    static final private int _TILES = 1;

    static final private int _BILINEAR = 2;

    static final private int _HYPER = 3;

    /**
     * Nearest neighbor sampling; this is the fastest and lowest quality mode.
     * Quality is normally unacceptable when scaling down, but may be OK when
     * scaling up.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.InterpType NEAREST = new org.gnu.gdk.InterpType(
            _NEAREST);

    /**
     * This is an accurate simulation of the PostScript image operator without
     * any interpolation enabled. Each pixel is rendered as a tiny parallelogram
     * of solid color, the edges of which are implemented with antialiasing. It
     * resembles nearest neighbor for enlargement, and bilinear for reduction.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.InterpType TILES = new org.gnu.gdk.InterpType(
            _TILES);

    /**
     * Best quality/speed balance; use this mode by default. Bilinear
     * interpolation. For enlargement, it is equivalent to point-sampling the
     * ideal bilinear-interpolated image. For reduction, it is equivalent to
     * laying down small tiles and integrating over the coverage area.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.InterpType BILINEAR = new org.gnu.gdk.InterpType(
            _BILINEAR);

    /**
     * This is the slowest and highest quality reconstruction function. It is
     * derived from the hyperbolic filters in Wolberg's "Digital Image Warping",
     * and is formally defined as the hyperbolic-filter sampling the ideal
     * hyperbolic-filter interpolated image (the filter is designed to be
     * idempotent for 1:1 pixel mapping).
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.InterpType HYPER = new org.gnu.gdk.InterpType(
            _HYPER);

    static final private org.gnu.gdk.InterpType[] theInterned = new org.gnu.gdk.InterpType[] {
            NEAREST, TILES, BILINEAR, HYPER }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.InterpType theSacrificialOne = new org.gnu.gdk.InterpType(
            0);

    static public org.gnu.gdk.InterpType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.InterpType already = (org.gnu.gdk.InterpType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.InterpType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private InterpType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.InterpType or(org.gnu.gdk.InterpType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.InterpType and(org.gnu.gdk.InterpType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.InterpType xor(org.gnu.gdk.InterpType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.InterpType other) {
        return (value_ & other.value_) == other.value_;
    }

}
