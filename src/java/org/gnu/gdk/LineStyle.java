/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class LineStyle extends Enum {

    static final private int _SOLID = 0;

    /**
     * Lines are drawn solid.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.LineStyle SOLID = new org.gnu.gdk.LineStyle(
            _SOLID);

    static final private int _ON_OFF_DASH = 1;

    /**
     * Even segments are drawn; odd segments are not drawn.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.LineStyle ON_OFF_DASH = new org.gnu.gdk.LineStyle(
            _ON_OFF_DASH);

    static final private int _DOUBLE_DASH = 2;

    /**
     * Even segments are normally. Odd segments are drawn in the background
     * color if the fill style is SOLID, or in the background color masked by
     * the stipple if the fill style is STIPPLED.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.LineStyle DOUBLE_DASH = new org.gnu.gdk.LineStyle(
            _DOUBLE_DASH);

    static final private org.gnu.gdk.LineStyle[] theInterned = new org.gnu.gdk.LineStyle[] {
            SOLID, ON_OFF_DASH, DOUBLE_DASH }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.LineStyle theSacrificialOne = new org.gnu.gdk.LineStyle(
            0);

    static public org.gnu.gdk.LineStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.LineStyle already = (org.gnu.gdk.LineStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.LineStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private LineStyle(int value) {
        value_ = value;
    }

    public org.gnu.gdk.LineStyle or(org.gnu.gdk.LineStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.LineStyle and(org.gnu.gdk.LineStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.LineStyle xor(org.gnu.gdk.LineStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.LineStyle other) {
        return (value_ & other.value_) == other.value_;
    }
}
