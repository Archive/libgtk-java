/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class JoinStyle extends Enum {
    static final private int _MITER = 0;

    /**
     * The sides of each line are extended to meet at an angle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.JoinStyle MITER = new org.gnu.gdk.JoinStyle(
            _MITER);

    static final private int _ROUND = 1;

    /**
     * The sides of the two lines are joined by a circular arc.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.JoinStyle ROUND = new org.gnu.gdk.JoinStyle(
            _ROUND);

    static final private int _BEVEL = 2;

    /**
     * The sides of the two lines are joined by a straight line which makes an
     * equal angle with each line.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public org.gnu.gdk.JoinStyle BEVEL = new org.gnu.gdk.JoinStyle(
            _BEVEL);

    static final private org.gnu.gdk.JoinStyle[] theInterned = new org.gnu.gdk.JoinStyle[] {
            MITER, ROUND, BEVEL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.JoinStyle theSacrificialOne = new org.gnu.gdk.JoinStyle(
            0);

    static public org.gnu.gdk.JoinStyle intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.JoinStyle already = (org.gnu.gdk.JoinStyle) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.JoinStyle(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private JoinStyle(int value) {
        value_ = value;
    }

    public org.gnu.gdk.JoinStyle or(org.gnu.gdk.JoinStyle other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.JoinStyle and(org.gnu.gdk.JoinStyle other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.JoinStyle xor(org.gnu.gdk.JoinStyle other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.JoinStyle other) {
        return (value_ & other.value_) == other.value_;
    }
}
