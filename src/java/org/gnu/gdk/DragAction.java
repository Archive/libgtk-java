/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class DragAction extends Flags {

    static final private int _DEFAULT = 1 << 0;

    static final public DragAction DEFAULT = new DragAction(_DEFAULT);

    static final private int _COPY = 1 << 1;

    static final public DragAction COPY = new DragAction(_COPY);

    static final private int _MOVE = 1 << 2;

    static final public DragAction MOVE = new DragAction(_MOVE);

    static final private int _LINK = 1 << 3;

    static final public DragAction LINK = new DragAction(_LINK);

    static final private int _PRIVATE = 1 << 4;

    static final public DragAction PRIVATE = new DragAction(_PRIVATE);

    static final private int _ASK = 1 << 5;

    static final public DragAction ASK = new DragAction(_ASK);

    static final private DragAction[] theInterned = new DragAction[] {
            new DragAction(0), DEFAULT, COPY, new DragAction(3), MOVE,
            new DragAction(5), new DragAction(6), new DragAction(7), LINK,
            new DragAction(9), new DragAction(10), new DragAction(11),
            new DragAction(12), new DragAction(13), new DragAction(14),
            new DragAction(15), PRIVATE, new DragAction(17),
            new DragAction(18), new DragAction(19), new DragAction(20),
            new DragAction(21), new DragAction(22), new DragAction(23),
            new DragAction(24), new DragAction(25), new DragAction(26),
            new DragAction(27), new DragAction(28), new DragAction(29),
            new DragAction(30), new DragAction(31), ASK };

    static private java.util.Hashtable theInternedExtras;

    static final private DragAction theSacrificialOne = new DragAction(0);

    static public DragAction intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        DragAction already = (DragAction) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new DragAction(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private DragAction(int value) {
        value_ = value;
    }

    public DragAction or(DragAction other) {
        return intern(value_ | other.value_);
    }

    public DragAction and(DragAction other) {
        return intern(value_ & other.value_);
    }

    public DragAction xor(DragAction other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(DragAction other) {
        return (value_ & other.value_) == other.value_;
    }

}
