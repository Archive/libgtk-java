/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class PropertyState extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _NEW_VALUE = 0;

    static final public org.gnu.gdk.PropertyState NEW_VALUE = new org.gnu.gdk.PropertyState(
            _NEW_VALUE);

    static final private int _DELETE = 1;

    static final public org.gnu.gdk.PropertyState DELETE = new org.gnu.gdk.PropertyState(
            _DELETE);

    static final private org.gnu.gdk.PropertyState[] theInterned = new org.gnu.gdk.PropertyState[] {
            NEW_VALUE, DELETE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.PropertyState theSacrificialOne = new org.gnu.gdk.PropertyState(
            0);

    static public org.gnu.gdk.PropertyState intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.PropertyState already = (org.gnu.gdk.PropertyState) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.PropertyState(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private PropertyState(int value) {
        value_ = value;
    }

    public org.gnu.gdk.PropertyState or(org.gnu.gdk.PropertyState other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.PropertyState and(org.gnu.gdk.PropertyState other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.PropertyState xor(org.gnu.gdk.PropertyState other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.PropertyState other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
