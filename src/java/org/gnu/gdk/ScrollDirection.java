/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class ScrollDirection extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _UP = 0;

    static final public org.gnu.gdk.ScrollDirection UP = new org.gnu.gdk.ScrollDirection(
            _UP);

    static final private int _DOWN = 1;

    static final public org.gnu.gdk.ScrollDirection DOWN = new org.gnu.gdk.ScrollDirection(
            _DOWN);

    static final private int _LEFT = 2;

    static final public org.gnu.gdk.ScrollDirection LEFT = new org.gnu.gdk.ScrollDirection(
            _LEFT);

    static final private int _RIGHT = 3;

    static final public org.gnu.gdk.ScrollDirection RIGHT = new org.gnu.gdk.ScrollDirection(
            _RIGHT);

    static final private org.gnu.gdk.ScrollDirection[] theInterned = new org.gnu.gdk.ScrollDirection[] {
            UP, DOWN, LEFT, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.ScrollDirection theSacrificialOne = new org.gnu.gdk.ScrollDirection(
            0);

    static public org.gnu.gdk.ScrollDirection intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.ScrollDirection already = (org.gnu.gdk.ScrollDirection) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.ScrollDirection(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ScrollDirection(int value) {
        value_ = value;
    }

    public org.gnu.gdk.ScrollDirection or(org.gnu.gdk.ScrollDirection other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.ScrollDirection and(org.gnu.gdk.ScrollDirection other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.ScrollDirection xor(org.gnu.gdk.ScrollDirection other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.ScrollDirection other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
