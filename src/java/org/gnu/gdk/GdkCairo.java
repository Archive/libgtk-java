/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.freedesktop.cairo.Context;
import org.gnu.glib.Handle;

public class GdkCairo extends Context {

    /**
     * Create a cairo Context from drawing to the Drawable.
     * 
     * @param drawable
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GdkCairo(Drawable drawable) {
        super(gdk_cairo_create(drawable.getHandle()));
    }

    /**
     * Sets the specified color as the source color of the cairo Context.
     * 
     * @param color
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSourceColor(Color color) {
        gdk_cairo_set_source_color(getHandle(), color.getHandle());
    }

    /**
     * Sets the given pixbuf as the source pattern for the cairo Context. The
     * pattern has an extend mode of NONE and is aligned so that the origin of
     * pixbuf is pixbufX, pixbufY.
     * 
     * @param pixbuf
     * @param pixbufX
     * @param pixbufY
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSourcePixbuf(Pixbuf pixbuf, double pixbufX, double pixbufY) {
        gdk_cairo_set_source_pixbuf(getHandle(), pixbuf.getHandle(), pixbufX,
                pixbufY);
    }

    /**
     * Adds the given rectangle to the current path.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void rectangle(Rectangle rect) {
        gdk_cairo_rectangle(getHandle(), rect.getHandle());
    }

    /**
     * Adds the given region to the current path.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void region(Region reg) {
        gdk_cairo_region(getHandle(), reg.getHandle());
    }

    native static final private Handle gdk_cairo_create(Handle drawable);

    native static final private void gdk_cairo_set_source_color(Handle obj,
            Handle color);

    native static final private void gdk_cairo_set_source_pixbuf(Handle ojb,
            Handle pixbuf, double x, double y);

    native static final private void gdk_cairo_rectangle(Handle obj, Handle rect);

    native static final private void gdk_cairo_region(Handle obj, Handle region);

}
