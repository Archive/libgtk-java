/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;

/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.EventExpose</code>.
 */
public class EventExpose extends Event {
    /**
     * Promotion constructor. Construct the specific event object by promoting
     * (sharing the <tt>Handle</tt> object of) the base <tt>Event</tt>.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventExpose(Event evt) {
        // We share the handle with both instances. This avoids
        // copying the event (via the constructor ( super( evt ) ).
        // It is more of a "promotion" constructor.
        super(evt.getHandle());
    }

    /**
     * To be used internally by Java-Gnome only.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static EventExpose getEventExpose(Handle handle) {
        if (handle == null) {
            return null;
        }

        EventExpose obj = (EventExpose) Boxed.getBoxedFromHandle(handle);

        if (obj == null) {
            obj = new EventExpose(handle);
        }
        return obj;
    }

    public EventExpose(Handle handle) {
        super(handle);
    }

    public Window getWindow() {
        return Window.getWindowFromHandle(getWindow(getHandle()));
    }

    public boolean getSendEvent() {
        return getSendEvent(getHandle());
    }

    public Rectangle getArea() {
        return Rectangle.getRectangleFromHandle(getArea(getHandle()));
    }

    public Region getRegion() {
        return Region.getRegionFromHandle(getRegion(getHandle()));
    }

    public int getCount() {
        return getCount(getHandle());
    }

    native static final protected Handle getWindow(Handle obj);

    native static final protected boolean getSendEvent(Handle obj);

    native static final protected Handle getRegion(Handle obj);

    native static final protected int getCount(Handle obj);

    native static final protected Handle getArea(Handle obj);
}
