/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class GCValuesMask extends Enum {

    static final private int _FOREGROUND = 0;

    static final public org.gnu.gdk.GCValuesMask FOREGROUND = new org.gnu.gdk.GCValuesMask(
            _FOREGROUND);

    static final private int _BACKGROUND = 1;

    static final public org.gnu.gdk.GCValuesMask BACKGROUND = new org.gnu.gdk.GCValuesMask(
            _BACKGROUND);

    static final private int _FONT = 2;

    static final public org.gnu.gdk.GCValuesMask FONT = new org.gnu.gdk.GCValuesMask(
            _FONT);

    static final private int _FUNCTION = 3;

    static final public org.gnu.gdk.GCValuesMask FUNCTION = new org.gnu.gdk.GCValuesMask(
            _FUNCTION);

    static final private int _FILL = 4;

    static final public org.gnu.gdk.GCValuesMask FILL = new org.gnu.gdk.GCValuesMask(
            _FILL);

    static final private int _TILE = 5;

    static final public org.gnu.gdk.GCValuesMask TILE = new org.gnu.gdk.GCValuesMask(
            _TILE);

    static final private int _STIPPLE = 6;

    static final public org.gnu.gdk.GCValuesMask STIPPLE = new org.gnu.gdk.GCValuesMask(
            _STIPPLE);

    static final private int _CLIP_MASK = 7;

    static final public org.gnu.gdk.GCValuesMask CLIP_MASK = new org.gnu.gdk.GCValuesMask(
            _CLIP_MASK);

    static final private int _SUBWINDOW = 8;

    static final public org.gnu.gdk.GCValuesMask SUBWINDOW = new org.gnu.gdk.GCValuesMask(
            _SUBWINDOW);

    static final private int _TS_X_ORIGIN = 9;

    static final public org.gnu.gdk.GCValuesMask TS_X_ORIGIN = new org.gnu.gdk.GCValuesMask(
            _TS_X_ORIGIN);

    static final private int _TS_Y_ORIGIN = 10;

    static final public org.gnu.gdk.GCValuesMask TS_Y_ORIGIN = new org.gnu.gdk.GCValuesMask(
            _TS_Y_ORIGIN);

    static final private int _CLIP_X_ORIGIN = 11;

    static final public org.gnu.gdk.GCValuesMask CLIP_X_ORIGIN = new org.gnu.gdk.GCValuesMask(
            _CLIP_X_ORIGIN);

    static final private int _CLIP_Y_ORIGIN = 12;

    static final public org.gnu.gdk.GCValuesMask CLIP_Y_ORIGIN = new org.gnu.gdk.GCValuesMask(
            _CLIP_Y_ORIGIN);

    static final private int _EXPOSURES = 13;

    static final public org.gnu.gdk.GCValuesMask EXPOSURES = new org.gnu.gdk.GCValuesMask(
            _EXPOSURES);

    static final private int _LINE_WIDTH = 14;

    static final public org.gnu.gdk.GCValuesMask LINE_WIDTH = new org.gnu.gdk.GCValuesMask(
            _LINE_WIDTH);

    static final private int _LINE_STYLE = 15;

    static final public org.gnu.gdk.GCValuesMask LINE_STYLE = new org.gnu.gdk.GCValuesMask(
            _LINE_STYLE);

    static final private int _CAP_STYLE = 16;

    static final public org.gnu.gdk.GCValuesMask CAP_STYLE = new org.gnu.gdk.GCValuesMask(
            _CAP_STYLE);

    static final private int _JOIN_STYLE = 17;

    static final public org.gnu.gdk.GCValuesMask JOIN_STYLE = new org.gnu.gdk.GCValuesMask(
            _JOIN_STYLE);

    static final private org.gnu.gdk.GCValuesMask[] theInterned = new org.gnu.gdk.GCValuesMask[] {
            FOREGROUND, BACKGROUND, FONT, FUNCTION, FILL, TILE, STIPPLE,
            CLIP_MASK, SUBWINDOW, TS_X_ORIGIN, TS_Y_ORIGIN, CLIP_X_ORIGIN,
            CLIP_Y_ORIGIN, EXPOSURES, LINE_WIDTH, LINE_STYLE, CAP_STYLE,
            JOIN_STYLE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.GCValuesMask theSacrificialOne = new org.gnu.gdk.GCValuesMask(
            0);

    static public org.gnu.gdk.GCValuesMask intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.GCValuesMask already = (org.gnu.gdk.GCValuesMask) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.GCValuesMask(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private GCValuesMask(int value) {
        value_ = value;
    }

    public org.gnu.gdk.GCValuesMask or(org.gnu.gdk.GCValuesMask other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.GCValuesMask and(org.gnu.gdk.GCValuesMask other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.GCValuesMask xor(org.gnu.gdk.GCValuesMask other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.GCValuesMask other) {
        return (value_ & other.value_) == other.value_;
    }

}
