/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

public class Dimension {

    private int height;

    private int width;

    /**
     * Creates a new <code>Dimension</code> object with the specified width
     * and height.
     * 
     * @param width
     *            the new object's width.
     * @param height
     *            the new object's height.
     * @throws IllegalArgumentException
     *             If <code>width</code> or <code>height</code> is negative
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Dimension(int width, int height) {
        if (width < 0 || height < 0)
            throw new IllegalArgumentException(
                    "Negative values are not allowed");
        this.height = height;
        this.width = width;
    }

    /**
     * @return the height of the <code>dimension</code> object.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return the width of the <code>dimension</code> object.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        return width;
    }
}
