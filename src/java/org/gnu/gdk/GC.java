/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * All drawing operations in GDK take a GC argument. A graphics context
 * encapsulates information about the way things are drawn, such as the
 * foreground color or line width. By using graphics context, the number of
 * arguments to each drawing call is reduced, and communication overhead is
 * minimized.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.GC</code>.
 */
public class GC extends GObject {

    public GC(Handle handle) {
        super(handle);
    }

    /**
     * @deprecated
     * @param window
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GC(Window window) {
        super(gdk_gc_new(window.getHandle()));
    }

    /**
     * Create a new graphics context with default values.
     * 
     * @param drawable
     *            The created GC must always be used with drawables of the same
     *            depth as this one.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GC(Drawable drawable) {
        super(gdk_gc_new(drawable.getHandle()));
    }

    /**
     * Convenience method to construct a new GC with default values.
     * 
     * @param d
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static GC getDefaultGC(Drawable d) {
        return new GC(GC.gdk_gc_new(d.getHandle()));
    }

    /**
     * Getst he x origin of the clip mask.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getClipXOrigin() {
        return getClipXOrigin(getHandle());
    }

    /**
     * Gets the y origin of the clip mask.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getClipYOrigin() {
        return getClipYOrigin(getHandle());
    }

    /**
     * Gets the x origin of the tile or stipple.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTsXOrigin() {
        return getTsXOrigin(getHandle());
    }

    /**
     * Gets the y origin of the tile or stipple.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTsYOrigin() {
        return getTsYOrigin(getHandle());
    }

    /**
     * Sets the foreground color for the graphics context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setForeground(Color color) {
        gdk_gc_set_foreground(getHandle(), color.getHandle());
    }

    /**
     * Sets the background color for the graphics context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBackground(Color color) {
        gdk_gc_set_background(getHandle(), color.getHandle());
    }

    /**
     * Sets the foreground color using an unallocated color. The pixel value for
     * the color will be determined using GdkRGB. If the colormap has not
     * previously been initialized for GdkRGB, then for pseudo-color colormaps
     * (colormaps with a small modifiable number of colors), a colorcube will be
     * allocated in the colormap.
     * <p>
     * Calling this function for a GC without a colormap is an error.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRGBForeground(Color color) {
        gdk_gc_set_rgb_fg_color(getHandle(), color.getHandle());
    }

    /**
     * Sets the background color using an unallocated color. The pixel value for
     * the color will be determined using GdkRGB. If the colormap has not
     * previously been initialized for GdkRGB, then for pseudo-color colormaps
     * (colormaps with a small modifiable number of colors), a colorcube will be
     * allocated in the colormap.
     * <p>
     * Calling this function for a GC without a colormap is an error.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRGBBackground(Color color) {
        gdk_gc_set_rgb_bg_color(getHandle(), color.getHandle());
    }

    /**
     * Determines how the current pixel values and the pixel values being drawn
     * are combined to produce the final pixel values.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFunction(Function func) {
        gdk_gc_set_function(getHandle(), func.getValue());
    }

    /**
     * Sets the fill mode for the graphic context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFillMode(Fill mode) {
        gdk_gc_set_fill(getHandle(), mode.getValue());
    }

    /**
     * Sets the tile pixmap for the graphics context. This will only be used if
     * the fill mode is TILED.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTile(Pixmap tile) {
        gdk_gc_set_tile(getHandle(), tile.getHandle());
    }

    /**
     * Sets the stipple bitmap for a graphics context. The stipple will only be
     * used if the fill mode is STIPPLED or OPAQUE_STIPPLED.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStipple(Pixmap stipple) {
        gdk_gc_set_stipple(getHandle(), stipple.getHandle());
    }

    /**
     * Sets the origin when using tiles or stipples with the GC. The tile or
     * stipple will be aligned such that the upper left cornor of the tile or
     * stipple will coincide with this point.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setOrigin(int x, int y) {
        gdk_gc_set_ts_origin(getHandle(), x, y);
    }

    /**
     * Sets the origin of the clip mask. The coordinates are interpreted
     * relative to the upper-left cornor of the destination drawable of the
     * current operation.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setClipOrigin(int x, int y) {
        gdk_gc_set_clip_origin(getHandle(), x, y);
    }

    /**
     * Sets the clip mask for a graphics context from a rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setClipRectangle(Rectangle rect) {
        gdk_gc_set_clip_rectangle(getHandle(), rect.getHandle());
    }

    /**
     * Sets the clip mask for a graphics context from a region.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setClipRegion(Region reg) {
        gdk_gc_set_clip_region(getHandle(), reg.getHandle());
    }

    /**
     * Sets the clip mask for a graphics context from a bitmap.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setClipMask(Bitmap mask) {
        gdk_gc_set_clip_mask(getHandle(), mask.getHandle());
    }

    /**
     * Sets how drawing with this GC on a window will affect child windows.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSubwindow(SubwindowMode mode) {
        gdk_gc_set_subwindow(getHandle(), mode.getValue());
    }

    /**
     * Sets whether copying non-visible portions of a drawable using this
     * graphics context generate exposure events for the corresponding regions
     * of the destination drawable.
     * 
     * @see Drawable#drawDrawable
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */

    public void setExposures(boolean exposures) {
        gdk_gc_set_exposures(getHandle(), exposures);
    }

    /**
     * Sets various attributes of how lines are drawn. See the corresponding
     * members of GdkGCValues for full explanations of the arguments.
     * 
     * @param lineWidth
     *            the width of lines.
     * @param lineStyle
     *            the dash-style for lines.
     * @param capStyle
     *            the manner in which the ends of lines are drawn.
     * @param joinStyle
     *            the manner in which lines are joined together.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLineAttributes(int lineWidth, LineStyle lineStyle,
            CapStyle capStyle, JoinStyle joinStyle) {
        gdk_gc_set_line_attributes(getHandle(), lineWidth,
                lineStyle.getValue(), capStyle.getValue(), joinStyle.getValue());
    }

    /**
     * Sets the way dashed-lines are drawn. Lines will be drawn with alternating
     * on and off segments of the lengths specified in dash_list. The manner in
     * which the on and off segments are drawn is determined by the line style.
     * 
     * @param dashOffset
     *            the phase of the pattern for the dashed line-style you want to
     *            see
     * @param dashList
     *            the dash-list for the dashed line-style you want to set.
     * @see #setLineAttributes
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDashes(int dashOffset, int[] dashList) {
        gdk_gc_set_dashes(getHandle(), dashOffset, dashList, dashList.length);
    }

    /**
     * Offset attributes such as the clip and tile-stipple origins of the GC so
     * that drawing at X - x_offset, y - y_offset with the offset GC has the
     * same effect as drawing at x, y with the original GC.
     * 
     * @param xOffset
     *            amount by which to offset the GC in the X direction
     * @param yOffset
     *            amount by which to offset the GC in the Y direction
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void offset(int xOffset, int yOffset) {
        gdk_gc_offset(getHandle(), xOffset, yOffset);
    }

    /**
     * Copy the set of values (settings) from another graphics context.
     * 
     * @param source
     *            the source graphics context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void copy(GC source) {
        gdk_gc_copy(getHandle(), source.getHandle());
    }

    /**
     * Sets the colormap to the given colormap. The depth of the colormap's
     * visual must match the depth of the drawable for which the GC was created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColormap(Colormap colormap) {
        gdk_gc_set_colormap(getHandle(), colormap.getHandle());
    }

    /**
     * Gets the colormap, if it exists. A GC will have a colormap if the
     * drawable for which it was created has a colormap, or if a colormap was
     * set explicitely with setColormap().
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap getColormap() {
        return Colormap.getColormapFromHandle(gdk_gc_get_colormap(getHandle()));
    }

    /**
     * Gets the Screen for which this GC was created.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Screen getScreen() {
        return Screen.getScreenFromHandle(gdk_gc_get_screen(getHandle()));
    }

    native static final protected int getClipXOrigin(Handle obj);

    native static final protected int getClipYOrigin(Handle obj);

    native static final protected int getTsXOrigin(Handle obj);

    native static final protected int getTsYOrigin(Handle obj);

    native static final protected int gdk_gc_get_type();

    native static final protected Handle gdk_gc_new(Handle drawable);

    native static final protected Handle gdk_gc_get_screen(Handle gc);

    native static final protected void gdk_gc_set_foreground(Handle gc,
            Handle color);

    native static final protected void gdk_gc_set_background(Handle gc,
            Handle color);

    native static final protected void gdk_gc_set_rgb_fg_color(Handle gc,
            Handle color);

    native static final protected void gdk_gc_set_rgb_bg_color(Handle gc,
            Handle color);

    native static final protected void gdk_gc_set_function(Handle gc,
            int function);

    native static final protected void gdk_gc_set_fill(Handle gc, int fill);

    native static final protected void gdk_gc_set_tile(Handle gc, Handle tile);

    native static final protected void gdk_gc_set_stipple(Handle gc,
            Handle stipple);

    native static final protected void gdk_gc_set_ts_origin(Handle gc, int x,
            int y);

    native static final protected void gdk_gc_set_clip_origin(Handle gc, int x,
            int y);

    native static final protected void gdk_gc_set_clip_mask(Handle gc,
            Handle mask);

    native static final protected void gdk_gc_set_clip_rectangle(Handle gc,
            Handle rectangle);

    native static final protected void gdk_gc_set_clip_region(Handle gc,
            Handle region);

    native static final protected void gdk_gc_set_subwindow(Handle gc, int mode);

    native static final protected void gdk_gc_set_exposures(Handle gc,
            boolean exposures);

    native static final protected void gdk_gc_set_line_attributes(Handle gc,
            int lineWidth, int lineStyle, int capStyle, int joinStyle);

    native static final protected void gdk_gc_set_dashes(Handle gc,
            int dashOffset, int[] dashList, int n);

    native static final protected void gdk_gc_copy(Handle dstGC, Handle srcGC);

    native static final protected void gdk_gc_set_colormap(Handle gc,
            Handle colormap);

    native static final protected Handle gdk_gc_get_colormap(Handle gc);

    native static final protected void gdk_gc_offset(Handle gc, int xOffset,
            int yOffset);

}
