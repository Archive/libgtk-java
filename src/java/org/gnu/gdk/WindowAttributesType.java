/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Flags;

public class WindowAttributesType extends Flags {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _TITLE = 1 << 1;

    static final public org.gnu.gdk.WindowAttributesType TITLE = new org.gnu.gdk.WindowAttributesType(
            _TITLE);

    static final private int _X = 1 << 2;

    static final public org.gnu.gdk.WindowAttributesType X = new org.gnu.gdk.WindowAttributesType(
            _X);

    static final private int _Y = 1 << 3;

    static final public org.gnu.gdk.WindowAttributesType Y = new org.gnu.gdk.WindowAttributesType(
            _Y);

    static final private int _CURSOR = 1 << 4;

    static final public org.gnu.gdk.WindowAttributesType CURSOR = new org.gnu.gdk.WindowAttributesType(
            _CURSOR);

    static final private int _COLORMAP = 1 << 5;

    static final public org.gnu.gdk.WindowAttributesType COLORMAP = new org.gnu.gdk.WindowAttributesType(
            _COLORMAP);

    static final private int _VISUAL = 1 << 6;

    static final public org.gnu.gdk.WindowAttributesType VISUAL = new org.gnu.gdk.WindowAttributesType(
            _VISUAL);

    static final private int _WMCLASS = 1 << 7;

    static final public org.gnu.gdk.WindowAttributesType WMCLASS = new org.gnu.gdk.WindowAttributesType(
            _WMCLASS);

    static final private int _NOREDIR = 1 << 8;

    static final public org.gnu.gdk.WindowAttributesType NOREDIR = new org.gnu.gdk.WindowAttributesType(
            _NOREDIR);

    static final private org.gnu.gdk.WindowAttributesType[] theInterned = new org.gnu.gdk.WindowAttributesType[] {
            new org.gnu.gdk.WindowAttributesType(0),
            new org.gnu.gdk.WindowAttributesType(1), TITLE,
            new org.gnu.gdk.WindowAttributesType(3), X,
            new org.gnu.gdk.WindowAttributesType(5),
            new org.gnu.gdk.WindowAttributesType(6),
            new org.gnu.gdk.WindowAttributesType(7), Y,
            new org.gnu.gdk.WindowAttributesType(9),
            new org.gnu.gdk.WindowAttributesType(10),
            new org.gnu.gdk.WindowAttributesType(11),
            new org.gnu.gdk.WindowAttributesType(12),
            new org.gnu.gdk.WindowAttributesType(13),
            new org.gnu.gdk.WindowAttributesType(14),
            new org.gnu.gdk.WindowAttributesType(15), CURSOR,
            new org.gnu.gdk.WindowAttributesType(17),
            new org.gnu.gdk.WindowAttributesType(18),
            new org.gnu.gdk.WindowAttributesType(19),
            new org.gnu.gdk.WindowAttributesType(20),
            new org.gnu.gdk.WindowAttributesType(21),
            new org.gnu.gdk.WindowAttributesType(22),
            new org.gnu.gdk.WindowAttributesType(23),
            new org.gnu.gdk.WindowAttributesType(24),
            new org.gnu.gdk.WindowAttributesType(25),
            new org.gnu.gdk.WindowAttributesType(26),
            new org.gnu.gdk.WindowAttributesType(27),
            new org.gnu.gdk.WindowAttributesType(28),
            new org.gnu.gdk.WindowAttributesType(29),
            new org.gnu.gdk.WindowAttributesType(30),
            new org.gnu.gdk.WindowAttributesType(31), COLORMAP,
            new org.gnu.gdk.WindowAttributesType(33),
            new org.gnu.gdk.WindowAttributesType(34),
            new org.gnu.gdk.WindowAttributesType(35),
            new org.gnu.gdk.WindowAttributesType(36),
            new org.gnu.gdk.WindowAttributesType(37),
            new org.gnu.gdk.WindowAttributesType(38),
            new org.gnu.gdk.WindowAttributesType(39),
            new org.gnu.gdk.WindowAttributesType(40),
            new org.gnu.gdk.WindowAttributesType(41),
            new org.gnu.gdk.WindowAttributesType(42),
            new org.gnu.gdk.WindowAttributesType(43),
            new org.gnu.gdk.WindowAttributesType(44),
            new org.gnu.gdk.WindowAttributesType(45),
            new org.gnu.gdk.WindowAttributesType(46),
            new org.gnu.gdk.WindowAttributesType(47),
            new org.gnu.gdk.WindowAttributesType(48),
            new org.gnu.gdk.WindowAttributesType(49),
            new org.gnu.gdk.WindowAttributesType(50),
            new org.gnu.gdk.WindowAttributesType(51),
            new org.gnu.gdk.WindowAttributesType(52),
            new org.gnu.gdk.WindowAttributesType(53),
            new org.gnu.gdk.WindowAttributesType(54),
            new org.gnu.gdk.WindowAttributesType(55),
            new org.gnu.gdk.WindowAttributesType(56),
            new org.gnu.gdk.WindowAttributesType(57),
            new org.gnu.gdk.WindowAttributesType(58),
            new org.gnu.gdk.WindowAttributesType(59),
            new org.gnu.gdk.WindowAttributesType(60),
            new org.gnu.gdk.WindowAttributesType(61),
            new org.gnu.gdk.WindowAttributesType(62),
            new org.gnu.gdk.WindowAttributesType(63), VISUAL,
            new org.gnu.gdk.WindowAttributesType(65),
            new org.gnu.gdk.WindowAttributesType(66),
            new org.gnu.gdk.WindowAttributesType(67),
            new org.gnu.gdk.WindowAttributesType(68),
            new org.gnu.gdk.WindowAttributesType(69),
            new org.gnu.gdk.WindowAttributesType(70),
            new org.gnu.gdk.WindowAttributesType(71),
            new org.gnu.gdk.WindowAttributesType(72),
            new org.gnu.gdk.WindowAttributesType(73),
            new org.gnu.gdk.WindowAttributesType(74),
            new org.gnu.gdk.WindowAttributesType(75),
            new org.gnu.gdk.WindowAttributesType(76),
            new org.gnu.gdk.WindowAttributesType(77),
            new org.gnu.gdk.WindowAttributesType(78),
            new org.gnu.gdk.WindowAttributesType(79),
            new org.gnu.gdk.WindowAttributesType(80),
            new org.gnu.gdk.WindowAttributesType(81),
            new org.gnu.gdk.WindowAttributesType(82),
            new org.gnu.gdk.WindowAttributesType(83),
            new org.gnu.gdk.WindowAttributesType(84),
            new org.gnu.gdk.WindowAttributesType(85),
            new org.gnu.gdk.WindowAttributesType(86),
            new org.gnu.gdk.WindowAttributesType(87),
            new org.gnu.gdk.WindowAttributesType(88),
            new org.gnu.gdk.WindowAttributesType(89),
            new org.gnu.gdk.WindowAttributesType(90),
            new org.gnu.gdk.WindowAttributesType(91),
            new org.gnu.gdk.WindowAttributesType(92),
            new org.gnu.gdk.WindowAttributesType(93),
            new org.gnu.gdk.WindowAttributesType(94),
            new org.gnu.gdk.WindowAttributesType(95),
            new org.gnu.gdk.WindowAttributesType(96),
            new org.gnu.gdk.WindowAttributesType(97),
            new org.gnu.gdk.WindowAttributesType(98),
            new org.gnu.gdk.WindowAttributesType(99),
            new org.gnu.gdk.WindowAttributesType(100),
            new org.gnu.gdk.WindowAttributesType(101),
            new org.gnu.gdk.WindowAttributesType(102),
            new org.gnu.gdk.WindowAttributesType(103),
            new org.gnu.gdk.WindowAttributesType(104),
            new org.gnu.gdk.WindowAttributesType(105),
            new org.gnu.gdk.WindowAttributesType(106),
            new org.gnu.gdk.WindowAttributesType(107),
            new org.gnu.gdk.WindowAttributesType(108),
            new org.gnu.gdk.WindowAttributesType(109),
            new org.gnu.gdk.WindowAttributesType(110),
            new org.gnu.gdk.WindowAttributesType(111),
            new org.gnu.gdk.WindowAttributesType(112),
            new org.gnu.gdk.WindowAttributesType(113),
            new org.gnu.gdk.WindowAttributesType(114),
            new org.gnu.gdk.WindowAttributesType(115),
            new org.gnu.gdk.WindowAttributesType(116),
            new org.gnu.gdk.WindowAttributesType(117),
            new org.gnu.gdk.WindowAttributesType(118),
            new org.gnu.gdk.WindowAttributesType(119),
            new org.gnu.gdk.WindowAttributesType(120),
            new org.gnu.gdk.WindowAttributesType(121),
            new org.gnu.gdk.WindowAttributesType(122),
            new org.gnu.gdk.WindowAttributesType(123),
            new org.gnu.gdk.WindowAttributesType(124),
            new org.gnu.gdk.WindowAttributesType(125),
            new org.gnu.gdk.WindowAttributesType(126),
            new org.gnu.gdk.WindowAttributesType(127), WMCLASS,
            new org.gnu.gdk.WindowAttributesType(129),
            new org.gnu.gdk.WindowAttributesType(130),
            new org.gnu.gdk.WindowAttributesType(131),
            new org.gnu.gdk.WindowAttributesType(132),
            new org.gnu.gdk.WindowAttributesType(133),
            new org.gnu.gdk.WindowAttributesType(134),
            new org.gnu.gdk.WindowAttributesType(135),
            new org.gnu.gdk.WindowAttributesType(136),
            new org.gnu.gdk.WindowAttributesType(137),
            new org.gnu.gdk.WindowAttributesType(138),
            new org.gnu.gdk.WindowAttributesType(139),
            new org.gnu.gdk.WindowAttributesType(140),
            new org.gnu.gdk.WindowAttributesType(141),
            new org.gnu.gdk.WindowAttributesType(142),
            new org.gnu.gdk.WindowAttributesType(143),
            new org.gnu.gdk.WindowAttributesType(144),
            new org.gnu.gdk.WindowAttributesType(145),
            new org.gnu.gdk.WindowAttributesType(146),
            new org.gnu.gdk.WindowAttributesType(147),
            new org.gnu.gdk.WindowAttributesType(148),
            new org.gnu.gdk.WindowAttributesType(149),
            new org.gnu.gdk.WindowAttributesType(150),
            new org.gnu.gdk.WindowAttributesType(151),
            new org.gnu.gdk.WindowAttributesType(152),
            new org.gnu.gdk.WindowAttributesType(153),
            new org.gnu.gdk.WindowAttributesType(154),
            new org.gnu.gdk.WindowAttributesType(155),
            new org.gnu.gdk.WindowAttributesType(156),
            new org.gnu.gdk.WindowAttributesType(157),
            new org.gnu.gdk.WindowAttributesType(158),
            new org.gnu.gdk.WindowAttributesType(159),
            new org.gnu.gdk.WindowAttributesType(160),
            new org.gnu.gdk.WindowAttributesType(161),
            new org.gnu.gdk.WindowAttributesType(162),
            new org.gnu.gdk.WindowAttributesType(163),
            new org.gnu.gdk.WindowAttributesType(164),
            new org.gnu.gdk.WindowAttributesType(165),
            new org.gnu.gdk.WindowAttributesType(166),
            new org.gnu.gdk.WindowAttributesType(167),
            new org.gnu.gdk.WindowAttributesType(168),
            new org.gnu.gdk.WindowAttributesType(169),
            new org.gnu.gdk.WindowAttributesType(170),
            new org.gnu.gdk.WindowAttributesType(171),
            new org.gnu.gdk.WindowAttributesType(172),
            new org.gnu.gdk.WindowAttributesType(173),
            new org.gnu.gdk.WindowAttributesType(174),
            new org.gnu.gdk.WindowAttributesType(175),
            new org.gnu.gdk.WindowAttributesType(176),
            new org.gnu.gdk.WindowAttributesType(177),
            new org.gnu.gdk.WindowAttributesType(178),
            new org.gnu.gdk.WindowAttributesType(179),
            new org.gnu.gdk.WindowAttributesType(180),
            new org.gnu.gdk.WindowAttributesType(181),
            new org.gnu.gdk.WindowAttributesType(182),
            new org.gnu.gdk.WindowAttributesType(183),
            new org.gnu.gdk.WindowAttributesType(184),
            new org.gnu.gdk.WindowAttributesType(185),
            new org.gnu.gdk.WindowAttributesType(186),
            new org.gnu.gdk.WindowAttributesType(187),
            new org.gnu.gdk.WindowAttributesType(188),
            new org.gnu.gdk.WindowAttributesType(189),
            new org.gnu.gdk.WindowAttributesType(190),
            new org.gnu.gdk.WindowAttributesType(191),
            new org.gnu.gdk.WindowAttributesType(192),
            new org.gnu.gdk.WindowAttributesType(193),
            new org.gnu.gdk.WindowAttributesType(194),
            new org.gnu.gdk.WindowAttributesType(195),
            new org.gnu.gdk.WindowAttributesType(196),
            new org.gnu.gdk.WindowAttributesType(197),
            new org.gnu.gdk.WindowAttributesType(198),
            new org.gnu.gdk.WindowAttributesType(199),
            new org.gnu.gdk.WindowAttributesType(200),
            new org.gnu.gdk.WindowAttributesType(201),
            new org.gnu.gdk.WindowAttributesType(202),
            new org.gnu.gdk.WindowAttributesType(203),
            new org.gnu.gdk.WindowAttributesType(204),
            new org.gnu.gdk.WindowAttributesType(205),
            new org.gnu.gdk.WindowAttributesType(206),
            new org.gnu.gdk.WindowAttributesType(207),
            new org.gnu.gdk.WindowAttributesType(208),
            new org.gnu.gdk.WindowAttributesType(209),
            new org.gnu.gdk.WindowAttributesType(210),
            new org.gnu.gdk.WindowAttributesType(211),
            new org.gnu.gdk.WindowAttributesType(212),
            new org.gnu.gdk.WindowAttributesType(213),
            new org.gnu.gdk.WindowAttributesType(214),
            new org.gnu.gdk.WindowAttributesType(215),
            new org.gnu.gdk.WindowAttributesType(216),
            new org.gnu.gdk.WindowAttributesType(217),
            new org.gnu.gdk.WindowAttributesType(218),
            new org.gnu.gdk.WindowAttributesType(219),
            new org.gnu.gdk.WindowAttributesType(220),
            new org.gnu.gdk.WindowAttributesType(221),
            new org.gnu.gdk.WindowAttributesType(222),
            new org.gnu.gdk.WindowAttributesType(223),
            new org.gnu.gdk.WindowAttributesType(224),
            new org.gnu.gdk.WindowAttributesType(225),
            new org.gnu.gdk.WindowAttributesType(226),
            new org.gnu.gdk.WindowAttributesType(227),
            new org.gnu.gdk.WindowAttributesType(228),
            new org.gnu.gdk.WindowAttributesType(229),
            new org.gnu.gdk.WindowAttributesType(230),
            new org.gnu.gdk.WindowAttributesType(231),
            new org.gnu.gdk.WindowAttributesType(232),
            new org.gnu.gdk.WindowAttributesType(233),
            new org.gnu.gdk.WindowAttributesType(234),
            new org.gnu.gdk.WindowAttributesType(235),
            new org.gnu.gdk.WindowAttributesType(236),
            new org.gnu.gdk.WindowAttributesType(237),
            new org.gnu.gdk.WindowAttributesType(238),
            new org.gnu.gdk.WindowAttributesType(239),
            new org.gnu.gdk.WindowAttributesType(240),
            new org.gnu.gdk.WindowAttributesType(241),
            new org.gnu.gdk.WindowAttributesType(242),
            new org.gnu.gdk.WindowAttributesType(243),
            new org.gnu.gdk.WindowAttributesType(244),
            new org.gnu.gdk.WindowAttributesType(245),
            new org.gnu.gdk.WindowAttributesType(246),
            new org.gnu.gdk.WindowAttributesType(247),
            new org.gnu.gdk.WindowAttributesType(248),
            new org.gnu.gdk.WindowAttributesType(249),
            new org.gnu.gdk.WindowAttributesType(250),
            new org.gnu.gdk.WindowAttributesType(251),
            new org.gnu.gdk.WindowAttributesType(252),
            new org.gnu.gdk.WindowAttributesType(253),
            new org.gnu.gdk.WindowAttributesType(254),
            new org.gnu.gdk.WindowAttributesType(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.WindowAttributesType theSacrificialOne = new org.gnu.gdk.WindowAttributesType(
            0);

    static public org.gnu.gdk.WindowAttributesType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.WindowAttributesType already = (org.gnu.gdk.WindowAttributesType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.WindowAttributesType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WindowAttributesType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.WindowAttributesType or(
            org.gnu.gdk.WindowAttributesType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.WindowAttributesType and(
            org.gnu.gdk.WindowAttributesType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.WindowAttributesType xor(
            org.gnu.gdk.WindowAttributesType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.WindowAttributesType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
