/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.Enum;

public class VisualType extends Enum {

    /**
     * Builds a VisualType.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */

    public static VisualType getVisualType(int type) {
        if (type >= 0 && type <= 5) {
            return new VisualType(type);
        } else
            throw new IllegalArgumentException();
    }

    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _STATIC_GRAY = 0;

    static final public org.gnu.gdk.VisualType STATIC_GRAY = new org.gnu.gdk.VisualType(
            _STATIC_GRAY);

    static final private int _GRAYSCALE = 1;

    static final public org.gnu.gdk.VisualType GRAYSCALE = new org.gnu.gdk.VisualType(
            _GRAYSCALE);

    static final private int _STATIC_COLOR = 2;

    static final public org.gnu.gdk.VisualType STATIC_COLOR = new org.gnu.gdk.VisualType(
            _STATIC_COLOR);

    static final private int _PSEUDO_COLOR = 3;

    static final public org.gnu.gdk.VisualType PSEUDO_COLOR = new org.gnu.gdk.VisualType(
            _PSEUDO_COLOR);

    static final private int _TRUE_COLOR = 4;

    static final public org.gnu.gdk.VisualType TRUE_COLOR = new org.gnu.gdk.VisualType(
            _TRUE_COLOR);

    static final private int _DIRECT_COLOR = 5;

    static final public org.gnu.gdk.VisualType DIRECT_COLOR = new org.gnu.gdk.VisualType(
            _DIRECT_COLOR);

    static final private org.gnu.gdk.VisualType[] theInterned = new org.gnu.gdk.VisualType[] {
            STATIC_GRAY, GRAYSCALE, STATIC_COLOR, PSEUDO_COLOR, TRUE_COLOR,
            DIRECT_COLOR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.gdk.VisualType theSacrificialOne = new org.gnu.gdk.VisualType(
            0);

    static public org.gnu.gdk.VisualType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.gdk.VisualType already = (org.gnu.gdk.VisualType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.gdk.VisualType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private VisualType(int value) {
        value_ = value;
    }

    public org.gnu.gdk.VisualType or(org.gnu.gdk.VisualType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.gdk.VisualType and(org.gnu.gdk.VisualType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.gdk.VisualType xor(org.gnu.gdk.VisualType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.gdk.VisualType other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
