/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/*
 * TODO:
 * Everything but the constructor.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * Image represents an area for drawing graphics.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Image</code>.
 */
public class Image extends GObject {

    public Image(Handle handle) {
        super(handle);
    }

    /**
     * Create a new Image.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Image(ImageType type, Visual visual, int width, int height) {
        super(gdk_image_new(type.getValue(), visual.getHandle(), width, height));
    }

    /**
     * Retrieves the colormap for a given image.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap getColormap() {
        return Colormap
                .getColormapFromHandle(gdk_image_get_colormap(getHandle()));
    }

    /**
     * Sets the colormap for the image.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setColormap(Colormap cmap) {
        gdk_image_set_colormap(getHandle(), cmap.getHandle());
    }

    native static final protected Handle getVisual(Handle obj);

    native static final protected int getByteOrder(Handle obj);

    native static final protected int getWidth(Handle obj);

    native static final protected int getHeight(Handle obj);

    native static final protected int getDepth(Handle obj);

    native static final protected int getBpp(Handle obj);

    native static final protected int getBpl(Handle obj);

    native static final protected int getBitsPerPixel(Handle obj);

    native static final protected int gdk_image_get_type();

    native static final protected Handle gdk_image_new(int type, Handle visual,
            int width, int height);

    native static final protected void gdk_image_put_pixel(Handle image, int x,
            int y, int pixel);

    native static final protected int gdk_image_get_pixel(Handle image, int x,
            int y);

    native static final protected void gdk_image_set_colormap(Handle image,
            Handle colormap);

    native static final protected Handle gdk_image_get_colormap(Handle image);
}
