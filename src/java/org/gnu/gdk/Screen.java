/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 * Screen objects are the GDK representation of a physical screen. It is used
 * throughout GDK and GTK+ to specify which screen the top level windows are to
 * be displayed on. It is also used to query the screen specification and
 * default settings such as the default colormap, the screen width, etc.
 * 
 * <p>
 * Note that a Screen may consist of multiple monitors which are merged to form
 * a large screen area.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Screen</code>.
 */
public class Screen extends GObject {

    /**
     * Creates and object representing the default Screen for the default
     * Display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Screen() {
        super(gdk_screen_get_default());
    }

    public Screen(Handle handle) {
        super(handle);
    }

    /**
     * Returns the default Colormap for this screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap getDefaultColormap() {
        return Colormap
                .getColormapFromHandle(gdk_screen_get_default_colormap(getHandle()));
    }

    /**
     * Sets the default Colormap for this screen.
     * 
     * @param colormap
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDefaultColormap(Colormap colormap) {
        gdk_screen_set_default_colormap(getHandle(), colormap.getHandle());
    }

    /**
     * Returns the system's default Colormap for Screens
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap getSystemColormap() {
        return Colormap
                .getColormapFromHandle(gdk_screen_get_system_colormap(getHandle()));
    }

    /**
     * Returns the system's default Visual for Screens. This is the Visual for
     * the root window of the Display.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Visual getSystemVisual() {
        return Visual
                .getVisualFromHandle(gdk_screen_get_system_visual(getHandle()));
    }

    /**
     * Gets the preferred colormap for rendering image data on screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Colormap getRGBColormap() {
        return Colormap
                .getColormapFromHandle(gdk_screen_get_rgb_colormap(getHandle()));
    }

    public Colormap getRGBAColormap() {
        return Colormap
                .getColormapFromHandle(gdk_screen_get_rgba_colormap(getHandle()));
    }

    /**
     * Gets a "preferred visual" chosen by RGB for rendering image data on
     * screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Visual getRGBVisual() {
        return Visual
                .getVisualFromHandle(gdk_screen_get_rgb_visual(getHandle()));
    }

    public Visual getRGBAVisual() {
        return Visual
                .getVisualFromHandle(gdk_screen_get_rgba_visual(getHandle()));
    }

    /**
     * Returns the root window of the Screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window getRootWindow() {
        return Window
                .getWindowFromHandle(gdk_screen_get_root_window(getHandle()));
    }

    /**
     * Returns the display that belongs to this Screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Display getDisplay() {
        return Display
                .getDisplayFromHandle(gdk_screen_get_display(getHandle()));
    }

    /**
     * Gets the index of this Screen among the Screens in the Display to which
     * it belongs.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getNumber() {
        return gdk_screen_get_number(getHandle());
    }

    /**
     * Gets the height of the Screen in pixels.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeight() {
        return gdk_screen_get_height(getHandle());
    }

    /**
     * Gets the width of the Screen in pixels.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        return gdk_screen_get_width(getHandle());
    }

    /**
     * Gets the height of the Screen in milimeters.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeightMM() {
        return gdk_screen_get_height_mm(getHandle());
    }

    /**
     * Gets the width of the Screen in milimeters.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidthMM() {
        return gdk_screen_get_width_mm(getHandle());
    }

    /**
     * List the Visuals for this Screen. A Visual describes a hardware image
     * data format. For example, a visual might support 24-bit color, or 8-bit
     * color, and might expect pixels to be in a certain format.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Visual[] listVisuals() {
        Handle[] hndls = gdk_screen_list_visuals(getHandle());
        Visual[] vis = new Visual[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            vis[i] = Visual.getVisualFromHandle(hndls[i]);
        }
        return vis;
    }

    /**
     * Returns a list of all known toplevel Windows on the Screen. A toplevel
     * Window is a child of the root window.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Window[] getToplevelWindows() {
        Handle[] hndls = gdk_screen_get_toplevel_windows(getHandle());
        Window[] win = new Window[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            win[i] = Window.getWindowFromHandle(hndls[i]);
        }
        return win;
    }

    /**
     * Returns the name to pass to the Display constructor to create a Display
     * with this Screen as the default Screen.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String makeDisplayName() {
        return gdk_screen_make_display_name(getHandle());
    }

    /**
     * Returns the number of monitors which Screen consists of.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getNumMonitors() {
        return gdk_screen_get_n_monitors(getHandle());
    }

    /**
     * Returns a Rectangle representing the size and position of the individual
     * monitor within the entire Screen area.
     * 
     * @param monitorNum
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle getMonitorGeometry(int monitorNum) {
        Handle hndl = gdk_screen_get_monitor_geometry(getHandle(), monitorNum);
        return Rectangle.getRectangleFromHandle(hndl);
    }

    /**
     * Returns the monitor number in which the point (x,y) is located.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getMonitorAtPoint(int x, int y) {
        return gdk_screen_get_monitor_at_point(getHandle(), x, y);
    }

    /**
     * Returns the number of the monitor in which the largest area of the
     * bounding rectangle of the Window resides.
     * 
     * @param win
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getMonitorAtWindow(Window win) {
        return gdk_screen_get_monitor_at_window(getHandle(), win.getHandle());
    }

    /**
     * On X11, sends an X ClientMessage event to all toplevel windows on screen.
     * 
     * Toplevel windows are determined by checking for the WM_STATE property, as
     * described in the Inter-Client Communication Conventions Manual (ICCCM).
     * If no windows are found with the WM_STATE property set, the message is
     * sent to all children of the root window.
     * 
     * On Windows, broadcasts a message registered with the name
     * GDK_WIN32_CLIENT_MESSAGE to all top-level windows. The amount of data is
     * limited to one long, i.e. four bytes.
     * 
     * @param evt
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void broadcastClientMessage(Event evt) {
        gdk_screen_broadcast_client_message(getHandle(), evt.getHandle());
    }

    public Type getType() {
        return new Type(gdk_screen_get_type());
    }

    /**
     * Internal static factory method to be used only internally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Screen getScreenFromHandle(Handle hndl) {
        if (hndl == null)
            return null;
        
        Screen obj = (Screen) getGObjectFromHandle(hndl);
        if (obj == null) 
            obj = new Screen(hndl);
        
        return obj;
    }

    native static final protected int gdk_screen_get_type();

    native static final protected Handle gdk_screen_get_default_colormap(
            Handle screen);

    native static final protected void gdk_screen_set_default_colormap(
            Handle screen, Handle colormap);

    native static final protected Handle gdk_screen_get_system_colormap(
            Handle screen);

    native static final protected Handle gdk_screen_get_system_visual(
            Handle screen);

    native static final protected Handle gdk_screen_get_rgb_colormap(
            Handle screen);

    native static final protected Handle gdk_screen_get_rgba_colormap(
            Handle screen);

    native static final protected Handle gdk_screen_get_rgb_visual(Handle screen);

    native static final protected Handle gdk_screen_get_rgba_visual(
            Handle screen);

    native static final protected Handle gdk_screen_get_root_window(
            Handle screen);

    native static final protected Handle gdk_screen_get_display(Handle screen);

    native static final protected int gdk_screen_get_number(Handle screen);

    native static final int gdk_screen_get_height(Handle screen);

    native static final int gdk_screen_get_width(Handle screen);

    native static final protected int gdk_screen_get_height_mm(Handle screen);

    native static final protected int gdk_screen_get_width_mm(Handle screen);

    native static final protected Handle[] gdk_screen_list_visuals(Handle screen);

    native static final protected Handle[] gdk_screen_get_toplevel_windows(
            Handle screen);

    native static final protected String gdk_screen_make_display_name(
            Handle screen);

    native static final protected int gdk_screen_get_n_monitors(Handle screen);

    native static final protected Handle gdk_screen_get_monitor_geometry(
            Handle screen, int monitorNum);

    native static final protected int gdk_screen_get_monitor_at_point(
            Handle screen, int x, int y);

    native static final protected int gdk_screen_get_monitor_at_window(
            Handle screen, Handle window);

    native static final protected void gdk_screen_broadcast_client_message(
            Handle screen, Handle event);

    native static final Handle gdk_screen_get_default();
    // native static final protected boolean gdk_screen_get_setting(Handle
    // screen, String name, Handle value);
}
