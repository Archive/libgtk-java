/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/*
 
 * TODO:
 * - Wrap the intern native methode.
 * - Beautify documentation.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.Struct;

public class Atom extends Struct {
    // NOTE: An GdkAtom is just a struct or pointer. We don't need to
    // do any memory management on it since the struct does not
    // contain any data. It is only used as an index into a string
    // table on the X server.

    public Atom(String name, boolean onlyIfExists) {
        super(gdk_atom_intern(name, onlyIfExists));
    }

    /**
     * Builds an atom with the specified handle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Atom(Handle handle) {
        super(handle);
    }

    /**
     * Obtains the atom's name.
     * 
     * @return The atom's name
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */

    public String getName() {
        return Atom.gdk_atom_name(getHandle());
    }

    native static final protected Handle gdk_atom_intern(String atomName,
            boolean onlyIfExists);

    native static final protected String gdk_atom_name(Handle atom);

}
