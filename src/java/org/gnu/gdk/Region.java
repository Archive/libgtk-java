/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

/*
 * TODO:
 * - Documentation.
 * - Complete the methods.
 */

package org.gnu.gdk;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Represents a set of pixels on the Screen.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.gdk.Region</code>.
 */
public class Region extends MemStruct {

    Region(Handle handle) {
        super(handle);
    }

    /**
     * Create a new empty Region.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Region() {
        super(gdk_region_new());
    }

    /**
     * Create a new Region using the poligon defined by a number of points.
     * 
     * @param points
     * @param rule
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Region(Point points[], FillRule rule) {
        super(init(points, rule));
    }

    /**
     * Create a new Region that is a copy of the provided Region
     * 
     * @param regionToCopy
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Region(Region regionToCopy) {
        super(gdk_region_copy(regionToCopy.getHandle()));
    }

    /**
     * Create a new Region containing the area of the Rectangle.
     * 
     * @param rectangle
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Region(Rectangle rectangle) {
        super(gdk_region_rectangle(rectangle.getHandle()));
    }

    /**
     * Return the smallest rectangle which includes the entire Region.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle getClipbox() {
        Handle rect = Rectangle.gdk_rectangle_new();
        gdk_region_get_clipbox(getHandle(), rect);
        return Rectangle.getRectangleFromHandle(rect);
    }

    /**
     * Obtain the area covered by this Region as a list of Rectangles.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle[] getRectangles() {
        Handle[] hndls = gdk_region_get_rectangles(getHandle());
        if (null == hndls)
            return null;
        Rectangle[] recs = new Rectangle[hndls.length];
        for (int i = 0; i < hndls.length; i++)
            recs[i] = Rectangle.getRectangleFromHandle(hndls[i]);
        return recs;
    }

    /**
     * Returns true if the Region is empty.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isEmpty() {
        return gdk_region_empty(getHandle());
    }

    /**
     * Returns true if the provided Region is the same as this one.
     * 
     * @param other
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isEqual(Region other) {
        return gdk_region_equal(getHandle(), other.getHandle());
    }

    /**
     * Returns true if the provided point is in the Region.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean containsPoint(int x, int y) {
        return gdk_region_point_in(getHandle(), x, y);
    }

    /**
     * Tests whether a Rectangle is within the Region.
     * 
     * @param rect
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public OverlapType containsRectangle(Rectangle rect) {
        return OverlapType.intern(gdk_region_rect_in(getHandle(), rect
                .getHandle()));
    }

    /**
     * Move the specified distance.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void offset(int x, int y) {
        gdk_region_offset(getHandle(), x, y);
    }

    /**
     * Resizes this Regions by the specified amount. Positive values shrink the
     * Region. Negative numbers expand it.
     * 
     * @param x
     * @param y
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void shrink(int x, int y) {
        gdk_region_shrink(getHandle(), x, y);
    }

    /**
     * Sets the area to the union of this Region and the provided Rectangle.
     * 
     * @param rect
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unionWithRect(Rectangle rect) {
        gdk_region_union_with_rect(getHandle(), rect.getHandle());
    }

    /**
     * Sets the area to the intersection of areas for this Region and the
     * provided Region.
     * 
     * @param region
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void intersect(Region region) {
        gdk_region_intersect(getHandle(), region.getHandle());
    }

    /**
     * Sets the area to the union of areas for this Region and the provided
     * Region.
     * 
     * @param region
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void union(Region region) {
        gdk_region_union(getHandle(), region.getHandle());
    }

    /**
     * Subtracts the area of the provided Region from this Region.
     * 
     * @param region
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void subtract(Region region) {
        gdk_region_subtract(getHandle(), region.getHandle());
    }

    /**
     * Sets the area of this Region to the exclusive-OR of the areas of this
     * Region and the provided Region.
     * 
     * @param region
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void xor(Region region) {
        gdk_region_xor(getHandle(), region.getHandle());
    }

    private static Handle init(Point points[], FillRule rule) {
        Handle pointsh[] = new Handle[points.length];
        for (int i = 0; i < points.length; i++)
            pointsh[i] = points[i].getHandle();
        return gdk_region_polygon(pointsh, rule.getValue());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Region getRegionFromHandle(Handle hndl) {
        if (hndl != null) {
            MemStruct strct = MemStruct.getMemStructFromHandle(hndl);
            return (strct != null) ? (Region) strct : new Region(hndl);
        }
        return null;
    }

    native static final protected Handle gdk_region_new();

    native static final protected Handle gdk_region_polygon(Handle[] points,
            int fillRule);

    native static final protected Handle gdk_region_copy(Handle region);

    native static final protected Handle gdk_region_rectangle(Handle rectangle);

    native static final protected void gdk_region_get_clipbox(Handle region,
            Handle rectangle);

    native static final protected Handle[] gdk_region_get_rectangles(
            Handle region);

    native static final protected boolean gdk_region_empty(Handle region);

    native static final protected boolean gdk_region_equal(Handle region1,
            Handle region2);

    native static final protected boolean gdk_region_point_in(Handle region,
            int x, int y);

    native static final protected int gdk_region_rect_in(Handle region,
            Handle rect);

    native static final protected void gdk_region_offset(Handle retion, int dx,
            int dy);

    native static final protected void gdk_region_shrink(Handle region, int dx,
            int dy);

    native static final protected void gdk_region_union_with_rect(
            Handle region, Handle rect);

    native static final protected void gdk_region_intersect(Handle source1,
            Handle source2);

    native static final protected void gdk_region_union(Handle source1,
            Handle source2);

    native static final protected void gdk_region_subtract(Handle source1,
            Handle source2);

    native static final protected void gdk_region_xor(Handle source1,
            Handle source2);
}
