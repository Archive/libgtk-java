/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * An enumeration specifying the width of the font relative to other designs
 * within a family.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Stretch</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Stretch extends Enum {
    static final private int _ULTRA_CONDENSED = 0;

    static final public Stretch ULTRA_CONDENSED = new Stretch(_ULTRA_CONDENSED);

    static final private int _EXTRA_CONDENSED = 1;

    static final public Stretch EXTRA_CONDENSED = new Stretch(_EXTRA_CONDENSED);

    static final private int _CONDENSED = 2;

    static final public Stretch CONDENSED = new Stretch(_CONDENSED);

    static final private int _SEMI_CONDENSED = 3;

    static final public Stretch SEMI_CONDENSED = new Stretch(_SEMI_CONDENSED);

    static final private int _NORMAL = 4;

    static final public Stretch NORMAL = new Stretch(_NORMAL);

    static final private int _SEMI_EXPANDED = 5;

    static final public Stretch SEMI_EXPANDED = new Stretch(_SEMI_EXPANDED);

    static final private int _EXPANDED = 6;

    static final public Stretch EXPANDED = new Stretch(_EXPANDED);

    static final private int _EXTRA_EXPANDED = 7;

    static final public Stretch EXTRA_EXPANDED = new Stretch(_EXTRA_EXPANDED);

    static final private int _ULTRA_EXPANDED = 8;

    static final public Stretch ULTRA_EXPANDED = new Stretch(_ULTRA_EXPANDED);

    static final private Stretch[] theInterned = new Stretch[] {
            ULTRA_CONDENSED, EXTRA_CONDENSED, CONDENSED, SEMI_CONDENSED,
            NORMAL, SEMI_EXPANDED, EXPANDED, EXTRA_EXPANDED, ULTRA_EXPANDED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Stretch theSacrificialOne = new Stretch(0);

    static public Stretch intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Stretch already = (Stretch) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new Stretch(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Stretch(int value) {
        value_ = value;
    }

    public Stretch or(Stretch other) {
        return intern(value_ | other.value_);
    }

    public Stretch and(Stretch other) {
        return intern(value_ & other.value_);
    }

    public Stretch xor(Stretch other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Stretch other) {
        return (value_ & other.value_) == other.value_;
    }

}
