/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * It is often necessary in Pango to determine if a particular font can
 * represent a particular character, and also how well it can represent that
 * character. The PangoCoverage is a data structure that is used to represent
 * that information.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Coverage</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Coverage extends MemStruct {

    protected Coverage(Handle handle) {
        super(handle);
    }

    /**
     * Create a new Coverage
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Coverage() {
        super(pango_coverage_new());
    }

    /**
     * Create a new Coverage that is a copy of the provided Coverage.
     * 
     * @param coverage
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Coverage(Coverage coverage) {
        super(pango_coverage_copy(coverage.getHandle()));
    }

    /**
     * Determine whether a particular index is covered by coverage.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public CoverageLevel get(int index) {
        return CoverageLevel.intern(pango_coverage_get(getHandle(), index));
    }

    /**
     * Set the coverage for each index in coverage to be the max (better) value
     * of the current coverage for the index and the coverage for the
     * corresponding index in other.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setMax(Coverage other) {
        pango_coverage_max(getHandle(), other.getHandle());
    }

    /**
     * Modify a particular index within coverage.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void set(int index, CoverageLevel level) {
        pango_coverage_set(getHandle(), index, level.getValue());
    }

    public byte[] toBytes() {
        return pango_coverage_to_bytes(getHandle());
    }

    public static Coverage fromBytes(byte[] ary) {
        return new Coverage(pango_coverage_from_bytes(ary, ary.length));
    }

    native static final protected Handle pango_coverage_new();

    native static final protected Handle pango_coverage_copy(Handle coverage);

    native static final protected int pango_coverage_get(Handle coverage,
            int index);

    native static final protected void pango_coverage_set(Handle coverage,
            int index, int level);

    native static final protected void pango_coverage_max(Handle coverage,
            Handle other);

    native static final protected byte[] pango_coverage_to_bytes(Handle coverage);

    native static final protected Handle pango_coverage_from_bytes(
            byte[] bytes, int numBytes);

}
