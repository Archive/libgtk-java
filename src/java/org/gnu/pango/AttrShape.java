/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;

/**
 * The AttrShape structure is used to represent attributes which impose shape
 * restrictions.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.AttrShape</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AttrShape extends Attribute {
    /**
     * Returns the ink rectangle to restrict to.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle getInkRect() {
        return Rectangle.getRectangleFromHandle(getInkRect(getHandle()));
    }

    /**
     * Returns the logical rectangle to restrict to.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle getLogicalRect() {
        return Rectangle.getRectangleFromHandle(getLogicalRect(getHandle()));
    }

    native static final protected Handle getInkRect(Handle obj);

    native static final protected Handle getLogicalRect(Handle obj);
}
