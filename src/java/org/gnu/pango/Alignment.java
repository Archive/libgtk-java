/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * describes how to align the lines of a PangoLayout within the available space.
 * If the PangoLayout is set to justify using pango_layout_set_justify(), then
 * this only has an effect for partial lines.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Alignment</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Alignment extends Enum {
    static final private int _LEFT = 0;

    /** Put all available space on the right */
    static final public Alignment LEFT = new Alignment(_LEFT);

    static final private int _CENTER = 1;

    /** Center the line within the available space */
    static final public Alignment CENTER = new Alignment(_CENTER);

    static final private int _RIGHT = 2;

    /** Put all available space on the left */
    static final public Alignment RIGHT = new Alignment(_RIGHT);

    static final private Alignment[] theInterned = new Alignment[] { LEFT,
            CENTER, RIGHT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Alignment theSacrificialOne = new Alignment(0);

    static public Alignment intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Alignment already = (Alignment) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new Alignment(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Alignment(int value) {
        value_ = value;
    }

    public Alignment or(Alignment other) {
        return intern(value_ | other.value_);
    }

    public Alignment and(Alignment other) {
        return intern(value_ & other.value_);
    }

    public Alignment xor(Alignment other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Alignment other) {
        return (value_ & other.value_) == other.value_;
    }

}
