/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * The FontDescription represents the description of an ideal font. This is used
 * both to list what fonts are available on the system and also for specifying
 * the characteristics of a font to load.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.FontDescription</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class FontDescription extends Boxed {

    /**
     * Create a new FontDescription using a handle from native methods. This
     * should only be used internally by java-gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription(Handle handle) {
        super(handle);
    }

    /**
     * Create a new FontDescription.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription() {
        super(FontDescription.pango_font_description_new());
    }

    /**
     * Create a new FontDescription from a string in the form
     * "[FAMILY-LIST][STYLE-OPTIONS][SIZE]" where FAMILY-LIST is a comma
     * separated list of families optionally terminated by a comma,
     * STYLE-OPTIONS is a whitespace separated list of words where each WORD
     * describes one of style, varient, weight, or stretch and SIZE is a decimal
     * number (size in points). Any one of the options may be absent.
     * 
     * @param str
     *            The string representation for the font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription(String str) {
        super(FontDescription.pango_font_description_from_string(str));
    }

    /**
     * Create a new FontDescription that is a copy of the provided
     * FontDescription.
     * 
     * @param fd
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription(FontDescription fd) {
        super(pango_font_description_copy(fd.getHandle()));
    }

    /**
     * Sets the family name field of a font description. The family name
     * represents a family of related font styles, and will resolve to a
     * particular PangoFontFamily. In some uses of PangoFontDescription, it is
     * also possible to use a comma separated list of family names for this
     * field.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFamily(String family) {
        pango_font_description_set_family(getHandle(), family);
    }

    /**
     * Gets the family name field of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getFamily() {
        return pango_font_description_get_family(getHandle());
    }

    /**
     * Sets the style field of a PangoFontDescription. The PangoStyle
     * enumeration describes whether the font is slanted and the manner in which
     * it is slanted; it can be either Style.NORMAL, Style.ITALIC, or
     * Style.OBLIQUE. Most fonts will either have a italic style or an oblique
     * style, but not both, and font matching in Pango will match italic
     * specifications with oblique fonts and vice-versa if an exact match is not
     * found.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStyle(Style style) {
        pango_font_description_set_style(getHandle(), style.getValue());
    }

    /**
     * Gets the style field of a PangoFontDescription.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Style getStyle() {
        return Style.intern(pango_font_description_get_style(getHandle()));
    }

    /**
     * Sets the variant field of a font description. The Variant can either be
     * Variant.NORMAL or Variant.SMALL_CAPS.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setVariant(Variant var) {
        pango_font_description_set_variant(getHandle(), var.getValue());
    }

    /**
     * Gets the variant field of the description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Variant getVariant() {
        return Variant.intern(pango_font_description_get_variant(getHandle()));
    }

    /**
     * Sets the weight field of a font description. The weight field specifies
     * how bold or light the font should be.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setWeight(Weight weight) {
        pango_font_description_set_weight(getHandle(), weight.getValue());
    }

    /**
     * Gets the weight field of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Weight getWeight() {
        return Weight.intern(pango_font_description_get_weight(getHandle()));
    }

    /**
     * Sets the stretch field of a font description. The stretch field specifies
     * how narrow or wide the font should be.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStretch(Stretch stretch) {
        pango_font_description_set_stretch(getHandle(), stretch.getValue());
    }

    /**
     * Gets the stretch field of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Stretch getStretch() {
        return Stretch.intern(pango_font_description_get_stretch(getHandle()));
    }

    /**
     * Sets the size field of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(int size) {
        pango_font_description_set_size(getHandle(), size);
    }

    /**
     * Gets the size field of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getSize() {
        return pango_font_description_get_size(getHandle());
    }

    /**
     * Determines which fields in a font description have been set.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontMask getSetFields() {
        return FontMask
                .intern(pango_font_description_get_set_fields(getHandle()));
    }

    /**
     * Merges the fields that are set in desc_to_merge into the fields in desc.
     * If replace_existing is FALSE, only fields in desc that are not already
     * set are affected. If TRUE, then fields that are already set will be
     * replaced as well.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void replace(FontDescription descToMerge, boolean replaceExisting) {
        pango_font_description_merge(getHandle(), descToMerge.getHandle(),
                replaceExisting);
    }

    /**
     * Creates a string representation of a font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return pango_font_description_to_string(getHandle());
    }

    /**
     * Create a hash of this object.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int hash() {
        return pango_font_description_hash(getHandle());
    }

    /**
     * Compares two FontDescriptions for equality.
     * 
     * @param fd
     * @return ture if the two FontDescriptions are proveably identical.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equal(FontDescription fd) {
        return pango_font_description_equal(getHandle(), fd.getHandle());
    }

    /**
     * Unset some of the fields in this FontDescription.
     * 
     * @param mask
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void unsetFields(FontMask mask) {
        pango_font_description_unset_fields(getHandle(), mask.getValue());
    }

    /**
     * Determines if the style attributes of <i>newMatch</i> are a closer match
     * for this FontDescription than <i>oldMatch</i>, or if <i>oldMatch</i> is
     * null, determines if <i>newMatch</i> is a match at all.
     * 
     * @param oldMatch
     * @param newMatch
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean betterMatch(FontDescription oldMatch,
            FontDescription newMatch) {
        return pango_font_description_better_match(getHandle(), oldMatch
                .getHandle(), newMatch.getHandle());
    }

    /**
     * Creates a filename representation of a font description. The filename is
     * identical to the results of calling <i>toString</i>, but with
     * underscores instead of characters that are untypical in filenames, and in
     * lower case only.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toFilename() {
        return pango_font_description_to_filename(getHandle());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static FontDescription getFontDescriptionFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            if (box != null) {
                return (FontDescription) box;
            } else {
                return new FontDescription(hndl);
            }
        }
        return null;
    }

    native static final protected int pango_font_description_get_type();

    native static final protected Handle pango_font_description_new();

    native static final protected Handle pango_font_description_copy(Handle desc);

    native static final protected int pango_font_description_hash(Handle desc);

    native static final protected boolean pango_font_description_equal(
            Handle desc1, Handle desc2);

    native static final protected void pango_font_description_set_family(
            Handle desc, String family);

    native static final protected String pango_font_description_get_family(
            Handle desc);

    native static final protected void pango_font_description_set_style(
            Handle desc, int style);

    native static final protected int pango_font_description_get_style(
            Handle desc);

    native static final protected void pango_font_description_set_variant(
            Handle desc, int variant);

    native static final protected int pango_font_description_get_variant(
            Handle desc);

    native static final protected void pango_font_description_set_weight(
            Handle desc, int weight);

    native static final protected int pango_font_description_get_weight(
            Handle desc);

    native static final protected void pango_font_description_set_stretch(
            Handle desc, int stretch);

    native static final protected int pango_font_description_get_stretch(
            Handle desc);

    native static final protected void pango_font_description_set_size(
            Handle desc, int size);

    native static final protected int pango_font_description_get_size(
            Handle desc);

    native static final protected int pango_font_description_get_set_fields(
            Handle desc);

    native static final protected void pango_font_description_unset_fields(
            Handle desc, int toUnset);

    native static final protected void pango_font_description_merge(
            Handle desc, Handle descToMerge, boolean replaceExisting);

    native static final protected boolean pango_font_description_better_match(
            Handle desc, Handle oldMatch, Handle newMatch);

    native static final protected Handle pango_font_description_from_string(
            String str);

    native static final protected String pango_font_description_to_string(
            Handle desc);

    native static final protected String pango_font_description_to_filename(
            Handle desc);

}
