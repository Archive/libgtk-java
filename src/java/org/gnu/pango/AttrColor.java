/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * The PangoAttrColor structure is used to represent attributes that are colors.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.AttrColor</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AttrColor extends Attribute {
    /**
     * Returns the color.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color getColor() {
        Handle hndl = getColor(getHandle());
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            if (box != null) {
                return (Color) box;
            } else {
                return new Color(hndl);
            }
        }
        return null;
    }

    native static final protected Handle getColor(Handle handle);
}
