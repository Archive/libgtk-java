/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * The FontFamily object is used to represent a family of related font faces.
 * The faces in a family share a common design, but differ in slant, weight,
 * width and other aspects.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.FontFamily</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class FontFamily extends GObject {
    FontFamily(Handle hndl) {
        super(hndl);
    }

    /**
     * Gets the name of the family. The name is unique among all fonts for the
     * font backend and can be used in a PangoFontDescription to specify that a
     * face from this family is desired.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return pango_font_family_get_name(getHandle());
    }

    /**
     * Lists the different font faces that make up family. The faces in a family
     * share a common design, but differ in slant, weight, width and other
     * aspects.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontFace[] listFaces() {
        Handle[] hndls = pango_font_family_list_faces(getHandle());
        if (null == hndls)
            return null;
        FontFace[] faces = new FontFace[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            GObject obj = GObject.getGObjectFromHandle(hndls[i]);
            if (obj != null) {
                faces[i] = (FontFace) obj;
            } else {
                faces[i] = new FontFace(hndls[i]);
            }
        }
        return faces;
    }

    native static final protected int pango_font_family_get_type();

    native static final protected Handle[] pango_font_family_list_faces(
            Handle family);

    native static final protected String pango_font_family_get_name(
            Handle family);

}
