/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Flags;

/**
 * The bits in a PangoFontMask correspond to fields in a PangoFontDescription
 * that have been set.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.FontMask</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class FontMask extends Flags {
    static final private int _FAMILY = 1 << 0;

    static final public FontMask FAMILY = new FontMask(_FAMILY);

    static final private int _STYLE = 1 << 1;

    static final public FontMask STYLE = new FontMask(_STYLE);

    static final private int _VARIANT = 1 << 2;

    static final public FontMask VARIANT = new FontMask(_VARIANT);

    static final private int _WEIGHT = 1 << 3;

    static final public FontMask WEIGHT = new FontMask(_WEIGHT);

    static final private int _STRETCH = 1 << 4;

    static final public FontMask STRETCH = new FontMask(_STRETCH);

    static final private int _SIZE = 1 << 5;

    static final public FontMask SIZE = new FontMask(_SIZE);

    static final private FontMask[] theInterned = new FontMask[] {
            new FontMask(0), FAMILY, STYLE, new FontMask(3), VARIANT,
            new FontMask(5), new FontMask(6), new FontMask(7), WEIGHT,
            new FontMask(9), new FontMask(10), new FontMask(11),
            new FontMask(12), new FontMask(13), new FontMask(14),
            new FontMask(15), STRETCH, new FontMask(17), new FontMask(18),
            new FontMask(19), new FontMask(20), new FontMask(21),
            new FontMask(22), new FontMask(23), new FontMask(24),
            new FontMask(25), new FontMask(26), new FontMask(27),
            new FontMask(28), new FontMask(29), new FontMask(30),
            new FontMask(31), SIZE }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private FontMask theSacrificialOne = new FontMask(0);

    static public FontMask intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        FontMask already = (FontMask) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new FontMask(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private FontMask(int value) {
        value_ = value;
    }

    public FontMask or(FontMask other) {
        return intern(value_ | other.value_);
    }

    public FontMask and(FontMask other) {
        return intern(value_ & other.value_);
    }

    public FontMask xor(FontMask other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(FontMask other) {
        return (value_ & other.value_) == other.value_;
    }

}
