/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * The PangoFontFace structure is used to represent a group of fonts with the
 * same family, slant, weight, width, but varying sizes.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.FontFace</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class FontFace extends GObject {
    FontFace(Handle hndl) {
        super(hndl);
    }

    /**
     * Gets a name representing the style of this face among the different faces
     * in the PangoFontFamily for the face. This name is unique among all faces
     * in the family and is suitable for displaying to users.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return pango_font_face_get_face_name(getHandle());
    }

    /**
     * Returns the family, style, variant, weight and stretch of a
     * PangoFontFace. The size field of the resulting font description will be
     * unset.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription describe() {
        return FontDescription
                .getFontDescriptionFromHandle(pango_font_face_describe(getHandle()));
    }

    native static final protected int pango_font_face_get_type();

    native static final protected Handle pango_font_face_describe(Handle face);

    native static final protected String pango_font_face_get_face_name(
            Handle face);

}
