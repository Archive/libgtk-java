/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * An enumeration specifying the various slant styles possible for a font.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Style</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Style extends Enum {

    static final private int _NORMAL = 0;

    /** the font is upright. */
    static final public Style NORMAL = new Style(_NORMAL);

    static final private int _OBLIQUE = 1;

    /** the font is slanted, but in a roman style. */
    static final public Style OBLIQUE = new Style(_OBLIQUE);

    static final private int _ITALIC = 2;

    /** the font is slanted in an italic style. */
    static final public Style ITALIC = new Style(_ITALIC);

    static final private Style[] theInterned = new Style[] { NORMAL, OBLIQUE,
            ITALIC }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Style theSacrificialOne = new Style(0);

    static public Style intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Style already = (Style) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new Style(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Style(int value) {
        value_ = value;
    }

    public Style or(Style other) {
        return intern(value_ | other.value_);
    }

    public Style and(Style other) {
        return intern(value_ & other.value_);
    }

    public Style xor(Style other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Style other) {
        return (value_ & other.value_) == other.value_;
    }

}
