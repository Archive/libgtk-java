/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * Describes how to wrap the lines of a PangoLayout to the desired width.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.WrapMode</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class WrapMode extends Enum {
    static final private int _WORD = 0;

    /** Wrap lines at word boundaries. */
    static final public WrapMode WORD = new WrapMode(_WORD);

    static final private int _CHAR = 1;

    /** Wrap lines at character boundaries. */
    static final public WrapMode CHAR = new WrapMode(_CHAR);

    static final private int _WORD_CHAR = 2;

    /**
     * Wrap lines at word boundaries, but fall back to character boundaries if
     * there is not enough space for a full word.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public WrapMode WORD_CHAR = new WrapMode(_WORD_CHAR);

    static final private WrapMode[] theInterned = new WrapMode[] { WORD, CHAR,
            WORD_CHAR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private WrapMode theSacrificialOne = new WrapMode(0);

    static public WrapMode intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        WrapMode already = (WrapMode) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new WrapMode(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private WrapMode(int value) {
        value_ = value;
    }

    public WrapMode or(WrapMode other) {
        return intern(value_ | other.value_);
    }

    public WrapMode and(WrapMode other) {
        return intern(value_ & other.value_);
    }

    public WrapMode xor(WrapMode other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(WrapMode other) {
        return (value_ & other.value_) == other.value_;
    }

}
