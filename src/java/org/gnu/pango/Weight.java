/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * An enumeration specifying the weight (boldness) of a font. This is a
 * numerical value ranging from 100 to 900,
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Weight</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Weight extends Enum {

    static final private int _ULTRALIGHT = 200;

    static final public Weight ULTRALIGHT = new Weight(_ULTRALIGHT);

    static final private int _LIGHT = 300;

    static final public Weight LIGHT = new Weight(_LIGHT);

    static final private int _NORMAL = 400;

    static final public Weight NORMAL = new Weight(_NORMAL);

    static final private int _BOLD = 700;

    static final public Weight BOLD = new Weight(_BOLD);

    static final private int _ULTRABOLD = 800;

    static final public Weight ULTRABOLD = new Weight(_ULTRABOLD);

    static final private int _HEAVY = 900;

    static final public Weight HEAVY = new Weight(_HEAVY);

    static final private Weight[] theInterned = new Weight[] { new Weight(0),
            new Weight(1), new Weight(2), new Weight(3), new Weight(4),
            new Weight(5), new Weight(6), new Weight(7), new Weight(8),
            new Weight(9), new Weight(10), new Weight(11), new Weight(12),
            new Weight(13), new Weight(14), new Weight(15), new Weight(16),
            new Weight(17), new Weight(18), new Weight(19), new Weight(20),
            new Weight(21), new Weight(22), new Weight(23), new Weight(24),
            new Weight(25), new Weight(26), new Weight(27), new Weight(28),
            new Weight(29), new Weight(30), new Weight(31), new Weight(32),
            new Weight(33), new Weight(34), new Weight(35), new Weight(36),
            new Weight(37), new Weight(38), new Weight(39), new Weight(40),
            new Weight(41), new Weight(42), new Weight(43), new Weight(44),
            new Weight(45), new Weight(46), new Weight(47), new Weight(48),
            new Weight(49), new Weight(50), new Weight(51), new Weight(52),
            new Weight(53), new Weight(54), new Weight(55), new Weight(56),
            new Weight(57), new Weight(58), new Weight(59), new Weight(60),
            new Weight(61), new Weight(62), new Weight(63), new Weight(64),
            new Weight(65), new Weight(66), new Weight(67), new Weight(68),
            new Weight(69), new Weight(70), new Weight(71), new Weight(72),
            new Weight(73), new Weight(74), new Weight(75), new Weight(76),
            new Weight(77), new Weight(78), new Weight(79), new Weight(80),
            new Weight(81), new Weight(82), new Weight(83), new Weight(84),
            new Weight(85), new Weight(86), new Weight(87), new Weight(88),
            new Weight(89), new Weight(90), new Weight(91), new Weight(92),
            new Weight(93), new Weight(94), new Weight(95), new Weight(96),
            new Weight(97), new Weight(98), new Weight(99), new Weight(100),
            new Weight(101), new Weight(102), new Weight(103), new Weight(104),
            new Weight(105), new Weight(106), new Weight(107), new Weight(108),
            new Weight(109), new Weight(110), new Weight(111), new Weight(112),
            new Weight(113), new Weight(114), new Weight(115), new Weight(116),
            new Weight(117), new Weight(118), new Weight(119), new Weight(120),
            new Weight(121), new Weight(122), new Weight(123), new Weight(124),
            new Weight(125), new Weight(126), new Weight(127), new Weight(128),
            new Weight(129), new Weight(130), new Weight(131), new Weight(132),
            new Weight(133), new Weight(134), new Weight(135), new Weight(136),
            new Weight(137), new Weight(138), new Weight(139), new Weight(140),
            new Weight(141), new Weight(142), new Weight(143), new Weight(144),
            new Weight(145), new Weight(146), new Weight(147), new Weight(148),
            new Weight(149), new Weight(150), new Weight(151), new Weight(152),
            new Weight(153), new Weight(154), new Weight(155), new Weight(156),
            new Weight(157), new Weight(158), new Weight(159), new Weight(160),
            new Weight(161), new Weight(162), new Weight(163), new Weight(164),
            new Weight(165), new Weight(166), new Weight(167), new Weight(168),
            new Weight(169), new Weight(170), new Weight(171), new Weight(172),
            new Weight(173), new Weight(174), new Weight(175), new Weight(176),
            new Weight(177), new Weight(178), new Weight(179), new Weight(180),
            new Weight(181), new Weight(182), new Weight(183), new Weight(184),
            new Weight(185), new Weight(186), new Weight(187), new Weight(188),
            new Weight(189), new Weight(190), new Weight(191), new Weight(192),
            new Weight(193), new Weight(194), new Weight(195), new Weight(196),
            new Weight(197), new Weight(198), new Weight(199), ULTRALIGHT,
            new Weight(201), new Weight(202), new Weight(203), new Weight(204),
            new Weight(205), new Weight(206), new Weight(207), new Weight(208),
            new Weight(209), new Weight(210), new Weight(211), new Weight(212),
            new Weight(213), new Weight(214), new Weight(215), new Weight(216),
            new Weight(217), new Weight(218), new Weight(219), new Weight(220),
            new Weight(221), new Weight(222), new Weight(223), new Weight(224),
            new Weight(225), new Weight(226), new Weight(227), new Weight(228),
            new Weight(229), new Weight(230), new Weight(231), new Weight(232),
            new Weight(233), new Weight(234), new Weight(235), new Weight(236),
            new Weight(237), new Weight(238), new Weight(239), new Weight(240),
            new Weight(241), new Weight(242), new Weight(243), new Weight(244),
            new Weight(245), new Weight(246), new Weight(247), new Weight(248),
            new Weight(249), new Weight(250), new Weight(251), new Weight(252),
            new Weight(253), new Weight(254), new Weight(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Weight theSacrificialOne = new Weight(0);

    static public Weight intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Weight already = (Weight) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new Weight(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    public Weight(int value) {
        value_ = value;
    }

    public Weight or(Weight other) {
        return intern(value_ | other.value_);
    }

    public Weight and(Weight other) {
        return intern(value_ & other.value_);
    }

    public Weight xor(Weight other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Weight other) {
        return (value_ & other.value_) == other.value_;
    }

}
