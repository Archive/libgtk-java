/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * A FontMetrics structure holds the overall metric information for a font
 * (possibly restricted to a script).
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.FontMetrics</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class FontMetrics extends Boxed {
    protected FontMetrics(Handle handle) {
        super(handle);
    }

    public FontMetrics(FontMetrics orig) {
        super(pango_font_metrics_ref(orig.getHandle()));
    }

    /**
     * Gets the ascent from the font metrics. The ascent is the distance from
     * the baseline to the logical top of a line of text. (The logical top may
     * be above or below the top of the actual drawn ink. It is necessary to lay
     * out the text to figure where the ink will be.)
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getAscent() {
        return pango_font_metrics_get_ascent(getHandle());
    }

    /**
     * Gets the descent from the font metrics. The descent is the distance from
     * the baseline to the logical bottom of a line of text. (The logical bottom
     * may be above or below the bottom of the actual drawn ink. It is necessary
     * to lay out the text to figure where the ink will be.)
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getDescent() {
        return pango_font_metrics_get_descent(getHandle());
    }

    /**
     * Gets the approximate character width for the font metrics. This is merely
     * a representative value useful, for example, for determining the initial
     * size for a window. Actual characters in text will be wider and narrower
     * than this.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getApproximateCharWidth() {
        return pango_font_metrics_get_approximate_char_width(getHandle());
    }

    /**
     * Gets the approximate digit width for the font metrics. This is merely a
     * representative value useful, for example, for determining the initial
     * size for a window. Actual digits in text can be wider and narrower than
     * this.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getApproximateDigitWidth() {
        return pango_font_metrics_get_approximate_digit_width(getHandle());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static FontMetrics getFontMetricsFromHandle(Handle hndl) {
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            if (box != null) {
                return (FontMetrics) box;
            } else {
                return new FontMetrics(hndl);
            }
        }
        return null;
    }

    native static final protected int pango_font_metrics_get_type();

    native static final protected Handle pango_font_metrics_ref(Handle metrics);
    
    native static final protected void pango_font_metrics_unref(Handle metrics);

    native static final protected int pango_font_metrics_get_ascent(
            Handle metrics);

    native static final protected int pango_font_metrics_get_descent(
            Handle metrics);

    native static final protected int pango_font_metrics_get_approximate_char_width(
            Handle metrics);

    native static final protected int pango_font_metrics_get_approximate_digit_width(
            Handle metrics);

}
