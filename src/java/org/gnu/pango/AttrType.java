/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * Distinguishes between different types of attributes.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.AttrType</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AttrType extends Enum {

    static final private int _INVALID = 0;

    static final public AttrType INVALID = new AttrType(_INVALID);

    static final private int _LANGUAGE = 1;

    static final public AttrType LANGUAGE = new AttrType(_LANGUAGE);

    static final private int _FAMILY = 2;

    static final public AttrType FAMILY = new AttrType(_FAMILY);

    static final private int _STYLE = 3;

    static final public AttrType STYLE = new AttrType(_STYLE);

    static final private int _WEIGHT = 4;

    static final public AttrType WEIGHT = new AttrType(_WEIGHT);

    static final private int _VARIANT = 5;

    static final public AttrType VARIANT = new AttrType(_VARIANT);

    static final private int _STRETCH = 6;

    static final public AttrType STRETCH = new AttrType(_STRETCH);

    static final private int _SIZE = 7;

    static final public AttrType SIZE = new AttrType(_SIZE);

    static final private int _FONT_DESC = 8;

    static final public AttrType FONT_DESC = new AttrType(_FONT_DESC);

    static final private int _FOREGROUND = 9;

    static final public AttrType FOREGROUND = new AttrType(_FOREGROUND);

    static final private int _BACKGROUND = 10;

    static final public AttrType BACKGROUND = new AttrType(_BACKGROUND);

    static final private int _UNDERLINE = 11;

    static final public AttrType UNDERLINE = new AttrType(_UNDERLINE);

    static final private int _STRIKETHROUGH = 12;

    static final public AttrType STRIKETHROUGH = new AttrType(_STRIKETHROUGH);

    static final private int _RISE = 13;

    static final public AttrType RISE = new AttrType(_RISE);

    static final private int _SHAPE = 14;

    static final public AttrType SHAPE = new AttrType(_SHAPE);

    static final private int _SCALE = 15;

    static final public AttrType SCALE = new AttrType(_SCALE);

    static final private int _FALLBACK = 16;

    static final public AttrType FALLBACK = new AttrType(_FALLBACK);

    static final private int _LETTER_SPACING = 17;

    static final public AttrType LETTER_SPACING = new AttrType(_LETTER_SPACING);

    static final private int _UNDERLINE_COLOR = 18;

    static final public AttrType UNDERLINE_COLOR = new AttrType(
            _UNDERLINE_COLOR);

    static final private int _STRIKETHROUGH_COLOR = 19;

    static final public AttrType STRIKETHROUGH_COLOR = new AttrType(
            _STRIKETHROUGH_COLOR);

    static final private AttrType[] theInterned = new AttrType[] { INVALID,
            LANGUAGE, FAMILY, STYLE, WEIGHT, VARIANT, STRETCH, SIZE, FONT_DESC,
            FOREGROUND, BACKGROUND, UNDERLINE, STRIKETHROUGH, RISE, SHAPE,
            SCALE, FALLBACK, LETTER_SPACING, UNDERLINE_COLOR,
            STRIKETHROUGH_COLOR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private AttrType theSacrificialOne = new AttrType(0);

    static public AttrType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        AttrType already = (AttrType) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new AttrType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private AttrType(int value) {
        value_ = value;
    }

    public AttrType or(AttrType other) {
        return intern(value_ | other.value_);
    }

    public AttrType and(AttrType other) {
        return intern(value_ & other.value_);
    }

    public AttrType xor(AttrType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(AttrType other) {
        return (value_ & other.value_) == other.value_;
    }

}
