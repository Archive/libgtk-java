/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * Type describing what sort of (if any) ellipsization should be applied to a
 * line of text. In the ellipsization process characters are removed from the
 * text in order to make it fit to a given width and replaced with an ellipsis.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.EllipsizeMode</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class EllipsizeMode extends Enum {

    private static final int _NONE = 0;

    private static final int _START = 1;

    private static final int _MIDDLE = 2;

    private static final int _END = 3;

    private EllipsizeMode(int val) {
        value_ = val;
    }

    /**
     * No ellipsization.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final EllipsizeMode NONE = new EllipsizeMode(_NONE);

    /**
     * Omit characters at the start of the text.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final EllipsizeMode START = new EllipsizeMode(_START);

    /**
     * Omit characters in the middle of the text.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final EllipsizeMode MIDDLE = new EllipsizeMode(_MIDDLE);

    /**
     * Omit characters at the end of the text.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final EllipsizeMode END = new EllipsizeMode(_END);

    private static final EllipsizeMode[] internValues = new EllipsizeMode[] {
            NONE, START, MIDDLE, END };

    /**
     * Get an EllipsizeMode instance from the given value. For internal use
     * only!
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static EllipsizeMode intern(int value) {
        if (value < 0 || value >= internValues.length) {
            return NONE;
        }
        return internValues[value];
    }

}
