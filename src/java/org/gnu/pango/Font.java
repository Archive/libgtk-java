/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.MemStruct;
import org.gnu.glib.Handle;

/**
 * Pango supports a flexible architecture where a particular rendering
 * architecture can supply an implementation of fonts. The PangoFont structure
 * represents an abstract rendering-system-indepent font. Pango provides
 * routines to list available fonts, and to load a font of a given description.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Font</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Font extends GObject {
    public Font() {
        super(getType());
    }

    /**
     * Returns a description of the font.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription describe() {
        return FontDescription
                .getFontDescriptionFromHandle(pango_font_describe(getHandle()));
    }

    /**
     * Computes the coverage map for a given font and language tag.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Coverage getCoverage(Language lang) {
        Handle hndl = pango_font_get_coverage(getHandle(), lang.getHandle());
        if (hndl != null) {
            MemStruct strct = MemStruct.getMemStructFromHandle(hndl);
            if (strct != null) {
                return (Coverage) strct;
            } else {
                return new Coverage(hndl);
            }
        }
        return null;
    }

    /**
     * Gets overall metric information for a font. Since the metrics may be
     * substantially different for different scripts, a language tag can be
     * provided to indicate that the metrics should be retrieved that correspond
     * to the script(s) used by that language.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontMetrics getMetrics(Language lang) {
        return FontMetrics.getFontMetricsFromHandle(pango_font_get_metrics(
                getHandle(), lang.getHandle()));
    }

    /**
     * Retrieve the runtime type used by the GLib library.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Type getType() {
        return new Type(pango_font_get_type());
    }

    native static final protected int pango_font_get_type();

    native static final protected Handle pango_font_describe(Handle font);

    native static final protected Handle pango_font_get_coverage(Handle font,
            Handle language);

    native static final protected Handle pango_font_get_metrics(Handle font,
            Handle language);

}
