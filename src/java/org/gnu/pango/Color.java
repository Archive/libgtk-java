/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * The PangoColor structure is used to represent a color in an uncalibrated RGB
 * colorspace.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Color</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Color extends Boxed {
    /**
     * Create a new instance whose fields are filled from a string
     * specification.
     * 
     * @see #setColor
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color(String spec) {
        super(pango_color_new());
        setColor(spec);
    }

    /**
     * Create a new Color that is a copy of the provided color.
     * 
     * @param color
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Color(Color color) {
        super(pango_color_copy(color.getHandle()));
    }

    protected Color(Handle handle) {
        super(handle);
    }

    /**
     * Returns the red component of the color. This is a value between 0 and
     * 65535, with 65535 indicating full intensity.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRed() {
        return getRed(getHandle());
    }

    /**
     * Returns the Green component of the color. This is a value between 0 and
     * 65535, with 65535 indicating full intensity.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getGreen() {
        return getGreen(getHandle());
    }

    /**
     * Returns the Blue component of the color. This is a value between 0 and
     * 65535, with 65535 indicating full intensity.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getBlue() {
        return getBlue(getHandle());
    }

    /**
     * Fill in the fields of a color from a string specification. The string can
     * either one of a large set of standard names. (Taken from the X11 rgb.txt
     * file), or it can be a hex value in the form 'rgb' 'rrggbb' 'rrrgggbbb' or
     * 'rrrrggggbbbb' where 'r', 'g' and 'b' are hex digits of the red, green,
     * and blue components of the color, respectively. (White in the four forms
     * is 'fff' 'ffffff' 'fffffffff' and 'ffffffffffff'.)
     * 
     * @param spec
     *            A string specifying the new color.
     * @return TRUE if parsing of the specifier succeeded, otherwise false.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean setColor(String spec) {
        return pango_color_parse(getHandle(), spec);
    }

    native static final protected int getRed(Handle obj);

    native static final protected int getGreen(Handle obj);

    native static final protected int getBlue(Handle obj);

    native static final protected int pango_color_get_type();

    native static final protected Handle pango_color_copy(Handle src);

    native static final protected boolean pango_color_parse(Handle color,
            String spec);

    native static final protected Handle pango_color_new();
}
