/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Scale</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Scale {
    private double value = 1;

    static final public Scale XX_SMALL = new Scale((1 / (1.2 * 1.2 * 1.2)));

    static final public Scale X_SMALL = new Scale(1 / (1.2 * 1.2));

    static final public Scale SMALL = new Scale((1 / 1.2));

    static final public Scale MEDIUM = new Scale(1.0);

    static final public Scale LARGE = new Scale(1.2);

    static final public Scale X_LARGE = new Scale(1.2 * 1.2);

    static final public Scale XX_LARGE = new Scale(1.2 * 1.2 * 1.2);

    public Scale(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
