/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * The PangoLayoutLine structure represents one of the lines resulting from
 * laying out a paragraph via PangoLayout. PangoLayoutLine structures are
 * obtained by calling {@link Layout#getLine(int)} and are only valid until the
 * text, attributes, or settings of the parent PangoLayout are modified.
 * 
 * <p>
 * Routines for rendering PangoLayout objects are provided in code specific to
 * each rendering system.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.LayoutLine</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class LayoutLine extends MemStruct {
    protected LayoutLine(Handle handle) {
        super(handle);
    }

    native static final protected void pango_layout_line_ref(Handle line);

    native static final protected void pango_layout_line_unref(Handle line);

    native static final protected boolean pango_layout_line_x_to_index(
            Handle line, int xPos, int[] index, int[] trailing);

    native static final protected void pango_layout_line_index_to_x(
            Handle line, int index, boolean trailing, int[] xPos);

    native static final protected void pango_layout_line_get_x_ranges(
            Handle line, int startIndex, int endIndex, int[] ranges,
            int[] numRanges);

    native static final protected void pango_layout_line_get_extents(
            Handle line, Handle inkRect, Handle logicalRect);

    native static final protected void pango_layout_line_get_pixel_extents(
            Handle line, Handle inkRect, Handle logicalRect);
}
