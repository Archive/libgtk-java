/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * Used to indicate how well a font can represent a particular ISO 10646
 * character point for a particular script.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.CoverageLevel</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class CoverageLevel extends Enum {
    static final private int _NONE = 0;

    /** The character is not representable with the font. */
    static final public CoverageLevel NONE = new CoverageLevel(_NONE);

    static final private int _FALLBACK = 1;

    /**
     * The character is represented in a way that may be comprehensible but is
     * not the correct graphical form. For instance, a Hangul character
     * represented as a a sequence of Jamos, or a Latin transliteration of a
     * Cyrillic word.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CoverageLevel FALLBACK = new CoverageLevel(_FALLBACK);

    static final private int _APPROXIMATE = 2;

    /**
     * The character is represented as basically the correct graphical form, but
     * with a stylistic variant inappropriate for the current script.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public CoverageLevel APPROXIMATE = new CoverageLevel(
            _APPROXIMATE);

    static final private int _EXACT = 3;

    /** The character is represented as the correct graphical form. */
    static final public CoverageLevel EXACT = new CoverageLevel(_EXACT);

    static final private CoverageLevel[] theInterned = new CoverageLevel[] {
            NONE, FALLBACK, APPROXIMATE, EXACT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private CoverageLevel theSacrificialOne = new CoverageLevel(0);

    static public CoverageLevel intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        CoverageLevel already = (CoverageLevel) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new CoverageLevel(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private CoverageLevel(int value) {
        value_ = value;
    }

    public CoverageLevel or(CoverageLevel other) {
        return intern(value_ | other.value_);
    }

    public CoverageLevel and(CoverageLevel other) {
        return intern(value_ & other.value_);
    }

    public CoverageLevel xor(CoverageLevel other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(CoverageLevel other) {
        return (value_ & other.value_) == other.value_;
    }

}
