/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Boxed;
import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * The PangoAttrList structure represents a list of attributes that apply to a
 * section of text. The attributes are, in general, allowed to overlap in an
 * arbitrary fashion, however, if the attributes are manipulated only through
 * pango_attr_list_change(), the overlap between properties will meet stricter
 * criteria.
 * 
 * <p>
 * Since the PangoAttrList structure is stored as a linear list, it is not
 * suitable for storing attributes for large amounts of text. In general, you
 * should not use a single PangoAttrList for more than one paragraph of text.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.AttrList</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AttrList extends Boxed {
    /**
     * Create a new AttrList that is a copy of the provided list.
     * 
     * @param list
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AttrList(AttrList list) {
        super(pango_attr_list_copy(list.getHandle()));
    }

    public AttrList(Handle handle) {
        super(handle);
    }

    /**
     * Static factory method that should only be used internally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AttrList getAttrListFromHandle(Handle handle) {
        if (handle == null) {
            return null;
        }

        AttrList obj = (AttrList) Boxed.getBoxedFromHandle(handle);

        if (obj == null) {
            obj = new AttrList(handle);
        }

        return obj;
    }

    /**
     * Construct a new AttrList.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AttrList() {
        super(pango_attr_list_new());
    }

    /**
     * Insert the given attribute into the list. It will be inserted after all
     * other attributes with a matching <i>start_index</i>.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insert(Attribute attr) {
        pango_attr_list_insert(getHandle(), attr.getHandle());
    }

    /**
     * Insert the given attribute into the list. It will be inserted before all
     * other attributes with a matching <i>start_index</i>.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insertBefore(Attribute attr) {
        pango_attr_list_insert_before(getHandle(), attr.getHandle());
    }

    /**
     * Insert the given attribute into the list. It will replace any attribute
     * of the same type on that segment and be merged with any adjoining
     * attributes that are identical.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void change(Attribute attr) {
        pango_attr_list_change(getHandle(), attr.getHandle());
    }

    /**
     * Splice on list into another.
     * 
     * @param other
     *            The other AttrList to splice in the current one.
     * @param pos
     *            The position at which to insert.
     * @param len
     *            The length of the spliced segment.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void splice(AttrList other, int pos, int len) {
        pango_attr_list_splice(getHandle(), other.getHandle(), pos, len);
    }

    /**
     * Create an iterator pointing at the beginning of the list.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AttrIterator getIterator() {
        Handle hndl = pango_attr_list_get_iterator(getHandle());
        if (hndl != null) {
            MemStruct strct = MemStruct.getMemStructFromHandle(hndl);
            if (strct != null) {
                return (AttrIterator) strct;
            } else {
                return new AttrIterator(hndl);
            }
        }
        return null;
    }

    native static final protected int pango_attr_list_get_type();

    native static final protected Handle pango_attr_list_new();

    native static final protected void pango_attr_list_ref(Handle list);

    native static final protected void pango_attr_list_unref(Handle list);

    native static final protected Handle pango_attr_list_copy(Handle list);

    native static final protected void pango_attr_list_insert(Handle list,
            Handle attr);

    native static final protected void pango_attr_list_insert_before(
            Handle list, Handle attr);

    native static final protected void pango_attr_list_change(Handle list,
            Handle attr);

    native static final protected void pango_attr_list_splice(Handle list,
            Handle other, int pos, int len);

    native static final protected Handle pango_attr_list_get_iterator(
            Handle list);

}
