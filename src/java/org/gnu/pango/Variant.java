/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * An enumeration specifying capitalization variant of the font.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Variant</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Variant extends Enum {
    static final private int _NORMAL = 0;

    /** A normal font. */
    static final public Variant NORMAL = new Variant(_NORMAL);

    /**
     * A font with the lower case characters replaced by smaller variants of the
     * capital characters.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final private int _SMALL_CAPS = 1;

    static final public Variant SMALL_CAPS = new Variant(_SMALL_CAPS);

    static final private Variant[] theInterned = new Variant[] { NORMAL,
            SMALL_CAPS }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Variant theSacrificialOne = new Variant(0);

    static public Variant intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Variant already = (Variant) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new Variant(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Variant(int value) {
        value_ = value;
    }

    public Variant or(Variant other) {
        return intern(value_ | other.value_);
    }

    public Variant and(Variant other) {
        return intern(value_ & other.value_);
    }

    public Variant xor(Variant other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Variant other) {
        return (value_ & other.value_) == other.value_;
    }

}
