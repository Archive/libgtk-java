/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * The PangoRectangle structure represents a rectangle. It is frequently used to
 * represent the logical or ink extents of a single glyph or section of text.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Rectangle</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Rectangle extends MemStruct {

    protected Rectangle(Handle handle) {
        super(handle);
    }

    /**
     * Create a new rectangle with x=y=width=height=0.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle() {
        super(pango_rectangle_new(0, 0, 0, 0));
    }

    /**
     * Create a new rectangle with the given x, y, width and height parameters.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Rectangle(int x, int y, int width, int height) {
        super(pango_rectangle_new(x, y, width, height));
    }

    /**
     * Returns X coordinate of the left side of the rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getX() {
        return getX(getHandle());
    }

    /**
     * Returns Y coordinate of the the top side of the rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getY() {
        return getY(getHandle());
    }

    /**
     * Returns width of the rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getWidth() {
        return getWidth(getHandle());
    }

    /**
     * Returns height of the rectangle.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getHeight() {
        return getHeight(getHandle());
    }

    /**
     * Package private helper method.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static Rectangle getRectangleFromHandle(Handle hndl) {
        if (hndl != null) {
            MemStruct obj = MemStruct.getMemStructFromHandle(hndl);
            return (obj != null) ? (Rectangle) obj : new Rectangle(hndl);
        }
        return null;
    }

    native static final protected Handle pango_rectangle_new(int x, int y,
            int width, int height);

    native static final protected int getX(Handle obj);

    native static final protected int getY(Handle obj);

    native static final protected int getWidth(Handle obj);

    native static final protected int getHeight(Handle obj);
}
