/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * A PangoTabArray struct contains an array of tab stops. Each tab stop has an
 * alignment and a position.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.TabArray</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class TabArray extends Boxed {
    /**
     * Creates an array of <code>initialSize</code> tab stops. Tab stops are
     * specified in pixel units if
     * <code>positionsInPixels<code> is <code>true</code>, otherwise
     * in Pango units. All stops are initially at position 0.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TabArray(int initialSize, boolean positionsInPixels) {
        super(pango_tab_array_new(initialSize, positionsInPixels));
    }

    /**
     * Create a TabArray that is a copy of the provided TabArray.
     * 
     * @param tabArray
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TabArray(TabArray tabArray) {
        super(pango_tab_array_copy(tabArray.getHandle()));
    }

    /**
     * Constructs new Tab array from handle to native resources. Used internally
     * by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TabArray(Handle handle) {
        super(handle);
    }

    /**
     * Constructs new Tab array from handle to native resources. Used internally
     * by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static TabArray getTabArray(Handle handle) {
        if (handle == null)
            return null;

        TabArray obj = (TabArray) Boxed.getBoxedFromHandle(handle);

        if (obj == null)
            obj = new TabArray(handle);

        return obj;
    }

    /**
     * Get the number of tab stops in the array
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getSize() {
        return pango_tab_array_get_size(getHandle());
    }

    /**
     * Resizes a tab array. You must subsequently initialize any tabs that were
     * added as a result of growing the array.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void resize(int newSize) {
        pango_tab_array_resize(getHandle(), newSize);
    }

    /**
     * Sets the alignment and location of a tab stop. Alignment must always be
     * {@link TabAlign#LEFT} in the current implementation.
     * 
     * @param tabIndex
     *            The index of a tab stop
     * @param alignment
     *            Tab alignment
     * @param location
     *            Tab location in pango units
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setTab(int tabIndex, TabAlign alignment, int location) {
        pango_tab_array_set_tab(getHandle(), tabIndex, alignment.getValue(),
                location);
    }

    /**
     * Returns the position of the tab stops
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getPosition(int tabIndex) {
        return pango_tab_array_get_tabLocation(getHandle(), tabIndex);
    }

    /**
     * Returns the alignment ofthe tab stop
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public TabAlign getAligment(int tabIndex) {
        return TabAlign.intern(pango_tab_array_get_tabAlignment(getHandle(),
                tabIndex));
    }

    /**
     * Returns true if the tab positions are in pixels and false if they are in
     * Pango units.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean positionInPixels() {
        return pango_tab_array_get_positions_in_pixels(getHandle());
    }

    native static final protected Handle pango_tab_array_new(int initialSize,
            boolean positionInPixels);

    native static final protected int pango_tab_array_get_type();

    native static final protected Handle pango_tab_array_copy(Handle src);

    native static final protected int pango_tab_array_get_size(Handle tabArray);

    native static final protected void pango_tab_array_resize(Handle tabArray,
            int newSize);

    native static final protected void pango_tab_array_set_tab(Handle tabArray,
            int tabIndex, int alignment, int location);

    native static final protected void pango_tab_array_get_tab(Handle tabArray,
            int tabIndex, int alignment, int[] location);

    native static final protected boolean pango_tab_array_get_positions_in_pixels(
            Handle tabArray);

    native static final protected int pango_tab_array_get_tabLocation(
            Handle tabArray, int tabIndex);

    native static final protected int pango_tab_array_get_tabAlignment(
            Handle tabArray, int tabIndex);

}
