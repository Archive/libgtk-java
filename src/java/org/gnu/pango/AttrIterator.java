/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * The AttrIterator object is used to represent an iterator through a List. A
 * new iterator is created with pango_attr_list_get_iterator(). Once the
 * iterator is created, it can be advanced through the style changes in the text
 * using pango_attr_iterator_next(). At each style change, the range of the
 * current style segment and the attributes currently in effect can be queried.
 * 
 * 
 * todo: is this needed?
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.AttrIterator</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AttrIterator extends MemStruct {
    public AttrIterator(Handle handle) {
        super(handle);
    }

    /**
     * Create a copy of the provided iterator.
     * 
     * @param iter
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AttrIterator(AttrIterator iter) {
        super(pango_attr_iterator_copy(iter.getHandle()));
    }

    /**
     * Advance the iterator until the next change of style.
     * 
     * @return false if the iterator is at the end of the list.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean next() {
        return pango_attr_iterator_next(getHandle());
    }

    /**
     * Return the beginning of the current segment range.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRangeStart() {
        int[] start = new int[1];
        int[] end = new int[1];
        pango_attr_iterator_range(getHandle(), start, end);
        return start[0];
    }

    /**
     * Return the end of the current segment range.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getRangeEnd() {
        int[] start = new int[1];
        int[] end = new int[1];
        pango_attr_iterator_range(getHandle(), start, end);
        return end[0];
    }

    /**
     * Find the current attribute of a particular type at the iterator location.
     * When multiple attributes of the same type overlap, the attribute whose
     * range starts closest to the current location is used.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Attribute get(AttrType type) {
        Handle hndl = pango_attr_iterator_get(getHandle(), type.getValue());
        if (hndl != null) {
            MemStruct strct = MemStruct.getMemStructFromHandle(hndl);
            if (strct != null) {
                return (Attribute) strct;
            } else {
                return new Attribute(hndl);
            }
        }
        return null;
    }

    native static final protected void pango_attr_iterator_range(
            Handle iterator, int[] start, int[] end);

    native static final protected boolean pango_attr_iterator_next(
            Handle iterator);

    native static final protected Handle pango_attr_iterator_copy(
            Handle iterator);

    native static final protected Handle pango_attr_iterator_get(
            Handle iterator, int type);

}
