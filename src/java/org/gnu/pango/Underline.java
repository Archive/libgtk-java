/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * The PangoUnderline enumeration is used to specify whether text should be
 * underlined, and if so, the type of underlining.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Underline</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Underline extends Enum {

    static final private int _NONE = 0;

    /** no underline should be drawn. */
    static final public Underline NONE = new Underline(_NONE);

    static final private int _SINGLE = 1;

    /** a single underline should be drawn. */
    static final public Underline SINGLE = new Underline(_SINGLE);

    static final private int _DOUBLE = 2;

    /** a double underline should be drawn. */
    static final public Underline DOUBLE = new Underline(_DOUBLE);

    static final private int _LOW = 3;

    /**
     * a single underline should be drawn at a position beneath the ink extents
     * of the text being underlined. This should be used only for underlining
     * single characters, such as for keyboard accelerators. {@link #SINGLE}
     * should be used for extended portions of text.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public Underline LOW = new Underline(_LOW);

    static final private int _ERROR = 4;

    static final public Underline ERROR = new Underline(_ERROR);

    static final private Underline[] theInterned = new Underline[] { NONE,
            SINGLE, DOUBLE, LOW, ERROR }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Underline theSacrificialOne = new Underline(0);

    static public Underline intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Underline already = (Underline) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new Underline(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Underline(int value) {
        value_ = value;
    }

    public Underline or(Underline other) {
        return intern(value_ | other.value_);
    }

    public Underline and(Underline other) {
        return intern(value_ & other.value_);
    }

    public Underline xor(Underline other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Underline other) {
        return (value_ & other.value_) == other.value_;
    }

}
