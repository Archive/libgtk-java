/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * The PangoDirection type represents the direction of writing for text.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Direction</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Direction extends Enum {
    static final private int _LTR = 0;

    /** The text is written left-to-right */
    static final public Direction LTR = new Direction(_LTR);

    static final private int _RTL = 1;

    /** The text is written right-to-left */
    static final public Direction RTL = new Direction(_RTL);

    static final private int _TTB_LTR = 2;

    /**
     * The text is written vertically top-to-bottom, with the rows ordered from
     * left to right.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public Direction TTB_LTR = new Direction(_TTB_LTR);

    static final private int _TTB_RTL = 3;

    /**
     * The text is written vertically top-to-bottom, with the rows ordered from
     * right to left.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    static final public Direction TTB_RTL = new Direction(_TTB_RTL);

    static final private Direction[] theInterned = new Direction[] { LTR, RTL,
            TTB_LTR, TTB_RTL }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private Direction theSacrificialOne = new Direction(0);

    static public Direction intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        Direction already = (Direction) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new Direction(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Direction(int value) {
        value_ = value;
    }

    public Direction or(Direction other) {
        return intern(value_ | other.value_);
    }

    public Direction and(Direction other) {
        return intern(value_ & other.value_);
    }

    public Direction xor(Direction other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(Direction other) {
        return (value_ & other.value_) == other.value_;
    }

}
