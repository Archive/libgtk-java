/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Boxed;
import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * The PangoContext structure stores global information used to control the
 * itemization process. It contains the following fields:
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Context</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Context extends GObject {
    /**
     * Create a new Context passing a handle that was created in the native
     * layer. This is an internal method that should only be used by Java-Gnome.
     * 
     * @param handle
     *            The handle to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Context(Handle handle) {
        super(handle);
    }

    /**
     * Static factory method that should only be used interally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Context getContextFromHandle(Handle handle) {
        if (handle == null) {
            return null;
        }

        Context obj = (Context) GObject.getGObjectFromHandle(handle);

        if (obj == null) {
            obj = new Context(handle);
        }

        return obj;
    }

    /**
     * Retrieve the default font description for the context.
     * 
     * @return default font description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription getFontDescription() {
        return FontDescription
                .getFontDescriptionFromHandle(pango_context_get_font_description(getHandle()));
    }

    /**
     * Sets the font description
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFontDescription(FontDescription desc) {
        pango_context_set_font_description(getHandle(), desc.getHandle());
    }

    /**
     * Retrieves the global language tag for the context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Language getLanguage() {
        Handle hndl = pango_context_get_language(getHandle());
        if (hndl != null) {
            Boxed box = Boxed.getBoxedFromHandle(hndl);
            if (box != null) {
                return (Language) box;
            } else {
                return new Language(hndl);
            }
        }
        return null;
    }

    /**
     * Sets the language
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLanguage(Language lang) {
        pango_context_set_language(getHandle(), lang.getHandle());
    }

    /**
     * Retrieves the base direction for the context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Direction getBaseDir() {
        return Direction.intern(pango_context_get_base_dir(getHandle()));
    }

    /**
     * Sets the base direction for the context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBaseDir(Direction dir) {
        pango_context_set_base_dir(getHandle(), dir.getValue());
    }

    /**
     * Loads the font in one of the fontmaps in the context that is the closest
     * match for desc.
     * 
     * @param desc
     *            A FontDescription describing the font to load
     * @return The font loaded, or <code>null</code> if no font matched.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontDescription loadFont(FontDescription desc) {
        return FontDescription
                .getFontDescriptionFromHandle(pango_context_load_font(
                        getHandle(), desc.getHandle()));
    }

    /**
     * Get overall metric information for a font particular font description.
     * Since the metrics may be substantially different for different scripts, a
     * language tag can be provided to indicate that the metrics should be
     * retrieved that correspond to the script(s) used by that language.
     * 
     * <p>
     * The PangoFontDescription is interpreted in the same way as by
     * pango_itemize(), and the family name may be a comma separated list of
     * figures. If characters from multiple of these families would be used to
     * render the string, then the returned fonts would be a composite of the
     * metrics for the fonts loaded for the individual families.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontMetrics getMetrics(FontDescription desc, Language language) {
        return FontMetrics.getFontMetricsFromHandle(pango_context_get_metrics(
                getHandle(), desc.getHandle(), language.getHandle()));
    }

    /**
     * List all families for a Context.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public FontFamily[] listFamilies() {
        Handle[] hndls = pango_context_list_families(getHandle());
        if (null == hndls)
            return null;
        FontFamily[] fams = new FontFamily[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            if (getGObjectFromHandle(hndls[i]) == null) {
                fams[i] = new FontFamily(hndls[i]);
            } else {
                fams[i] = (FontFamily) getGObjectFromHandle(hndls[i]);
            }
        }
        return fams;
    }

    native static final protected int pango_context_get_type();

    native static final protected Handle[] pango_context_list_families(
            Handle context);

    native static final protected Handle pango_context_load_font(
            Handle context, Handle desc);

    native static final protected Handle pango_context_get_metrics(
            Handle context, Handle desc, Handle language);

    native static final protected void pango_context_set_font_description(
            Handle context, Handle desc);

    native static final protected Handle pango_context_get_font_description(
            Handle context);

    native static final protected Handle pango_context_get_language(
            Handle context);

    native static final protected void pango_context_set_language(
            Handle context, Handle language);

    native static final protected void pango_context_set_base_dir(
            Handle context, int direction);

    native static final protected int pango_context_get_base_dir(Handle context);

    native static final protected boolean pango_context_load_fontNULL(
            Handle handle, Handle desc);

}
