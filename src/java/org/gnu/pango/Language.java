/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * The PangoLanguage structure is used to represent a language.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Language</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Language extends Boxed {
    /**
     * Take a RFC-3066 format language tag as a string and convert it to a
     * PangoLanguage pointer.
     * 
     * <p>
     * This function first canonicalizes the string by converting it to
     * lowercase, mapping '_' to '-', and stripping all characters other than
     * letters and '-'.
     * 
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Language(String lang) {
        super(pango_language_from_string(lang));
    }

    /**
     * Constructs a new language object from handle to native resources. This
     * should only be used internally by the java-gnome packages.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Language(Handle handle) {
        super(handle);
    }

    /**
     * Returns a RFC-3066 format string representing the given language tag.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return pango_language_to_string(getHandle());
    }

    native static final protected int pango_language_get_type();

    native static final protected Handle pango_language_from_string(
            String language);

    native static final protected String pango_language_to_string(
            Handle language);

    native static final protected boolean pango_language_matches(
            Handle language, String rangeList);
}
