/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.MemStruct;

/**
 * Attributed text is used in a number of places in Pango. It is used as the
 * input to the itemization process and also when creating a {@link Layout}.
 * The data types and functions in this section are used to represent and
 * manipulate sets of attributes applied to a portion of text.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.Attribute</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Attribute extends MemStruct {
    protected Attribute() {
    }

    protected Attribute(Handle handle) {
        super(handle);
    }

    /**
     * Returns the start index of the range
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getStartIndex() {
        return getStartIndex(getHandle());
    }

    /**
     * Returns end index of the range. The character at this index is not
     * included in the range.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getEndIndex() {
        return getEndIndex(getHandle());
    }

    native static final protected int getStartIndex(Handle obj);

    native static final protected int getEndIndex(Handle obj);
}
