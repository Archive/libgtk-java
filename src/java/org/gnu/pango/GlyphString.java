/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Handle;
import org.gnu.glib.Boxed;

/**
 * Used to store strings of glyphs with geometry and visible attribute
 * information.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.GlyphString</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class GlyphString extends Boxed {
    /**
     * Construct a new GlyphString.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GlyphString() {
        super(pango_glyph_string_new());
    }

    /**
     * Construct a new GlyphString that is a copy of the provided GlyphString.
     * 
     * @param glyphString
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GlyphString(GlyphString glyphString) {
        super(pango_glyph_string_copy(glyphString.getHandle()));
    }

    /**
     * Resizes the GlyphString to the given length.
     * 
     * @param newLength
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setSize(int newLength) {
        pango_glyph_string_set_size(getHandle(), newLength);
    }

    native static final protected Handle pango_glyph_string_new();

    native static final protected void pango_glyph_string_set_size(
            Handle string, int newLen);

    native static final protected int pango_glyph_string_get_type();

    native static final protected Handle pango_glyph_string_copy(Handle string);

    native static final protected void pango_glyph_string_extents(
            Handle glyphs, Handle font, Handle inkRect, Handle logicalRect);

    native static final protected void pango_glyph_string_extents_range(
            Handle glyphs, int start, int end, Handle font, Handle inkRect,
            Handle logicalRect);

    native static final protected void pango_glyph_string_get_logical_widths(
            Handle glyphs, String text, int length, int embeddingLevel,
            int[] logicalWidths);

}
