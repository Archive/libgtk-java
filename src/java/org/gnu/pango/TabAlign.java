/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.pango;

import org.gnu.glib.Enum;

/**
 * A PangoTabAlign specifies where a tab stop appears relative to the text
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.pango.TabAlign</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class TabAlign extends Enum {

    static final private int _LEFT = 0;

    /** the tab stop appears to the left of the text. */
    static final public TabAlign LEFT = new TabAlign(_LEFT);

    static final private TabAlign[] theInterned = new TabAlign[] { LEFT }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private TabAlign theSacrificialOne = new TabAlign(0);

    static public TabAlign intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        TabAlign already = (TabAlign) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new TabAlign(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private TabAlign(int value) {
        value_ = value;
    }

    public TabAlign or(TabAlign other) {
        return intern(value_ | other.value_);
    }

    public TabAlign and(TabAlign other) {
        return intern(value_ & other.value_);
    }

    public TabAlign xor(TabAlign other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(TabAlign other) {
        return (value_ & other.value_) == other.value_;
    }

}
