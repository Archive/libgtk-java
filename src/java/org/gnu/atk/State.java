/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

public class State {
    public static String getTypeName(StateType type) {
        return atk_state_type_get_name(type.getValue());
    }

    public static StateType getTypeForName(String name) {
        return StateType.intern(atk_state_type_for_name(name));
    }

    native static final protected int atk_state_type_register(String name);

    native static final protected String atk_state_type_get_name(int type);

    native static final protected int atk_state_type_for_name(String name);
}
