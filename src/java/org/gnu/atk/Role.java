/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.Enum;

/**
 * Describes the role of an object.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.Role</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Role extends Enum {

    static final private int _INVALID = 0;

    static final public org.gnu.atk.Role INVALID = new org.gnu.atk.Role(
            _INVALID);

    static final private int _ACCEL_LABEL = 1;

    static final public org.gnu.atk.Role ACCEL_LABEL = new org.gnu.atk.Role(
            _ACCEL_LABEL);

    static final private int _ALERT = 2;

    static final public org.gnu.atk.Role ALERT = new org.gnu.atk.Role(_ALERT);

    static final private int _ANIMATION = 3;

    static final public org.gnu.atk.Role ANIMATION = new org.gnu.atk.Role(
            _ANIMATION);

    static final private int _ARROW = 4;

    static final public org.gnu.atk.Role ARROW = new org.gnu.atk.Role(_ARROW);

    static final private int _CALENDAR = 5;

    static final public org.gnu.atk.Role CALENDAR = new org.gnu.atk.Role(
            _CALENDAR);

    static final private int _CANVAS = 6;

    static final public org.gnu.atk.Role CANVAS = new org.gnu.atk.Role(_CANVAS);

    static final private int _CHECK_BOX = 7;

    static final public org.gnu.atk.Role CHECK_BOX = new org.gnu.atk.Role(
            _CHECK_BOX);

    static final private int _CHECK_MENU_ITEM = 8;

    static final public org.gnu.atk.Role CHECK_MENU_ITEM = new org.gnu.atk.Role(
            _CHECK_MENU_ITEM);

    static final private int _COLOR_CHOOSER = 9;

    static final public org.gnu.atk.Role COLOR_CHOOSER = new org.gnu.atk.Role(
            _COLOR_CHOOSER);

    static final private int _COLUMN_HEADER = 10;

    static final public org.gnu.atk.Role COLUMN_HEADER = new org.gnu.atk.Role(
            _COLUMN_HEADER);

    static final private int _COMBO_BOX = 11;

    static final public org.gnu.atk.Role COMBO_BOX = new org.gnu.atk.Role(
            _COMBO_BOX);

    static final private int _DATE_EDITOR = 12;

    static final public org.gnu.atk.Role DATE_EDITOR = new org.gnu.atk.Role(
            _DATE_EDITOR);

    static final private int _DESKTOP_ICON = 13;

    static final public org.gnu.atk.Role DESKTOP_ICON = new org.gnu.atk.Role(
            _DESKTOP_ICON);

    static final private int _DESKTOP_FRAME = 14;

    static final public org.gnu.atk.Role DESKTOP_FRAME = new org.gnu.atk.Role(
            _DESKTOP_FRAME);

    static final private int _DIAL = 15;

    static final public org.gnu.atk.Role DIAL = new org.gnu.atk.Role(_DIAL);

    static final private int _DIALOG = 16;

    static final public org.gnu.atk.Role DIALOG = new org.gnu.atk.Role(_DIALOG);

    static final private int _DIRECOTRY_PANE = 17;

    static final public org.gnu.atk.Role DIRECOTRY_PANE = new org.gnu.atk.Role(
            _DIRECOTRY_PANE);

    static final private int _DRAWING_AREA = 18;

    static final public org.gnu.atk.Role DRAWING_AREA = new org.gnu.atk.Role(
            _DRAWING_AREA);

    static final private int _FILE_CHOOSER = 19;

    static final public org.gnu.atk.Role FILE_CHOOSER = new org.gnu.atk.Role(
            _FILE_CHOOSER);

    static final private int _FILLER = 20;

    static final public org.gnu.atk.Role FILLER = new org.gnu.atk.Role(_FILLER);

    static final private int _FONT_CHOOSER = 21;

    static final public org.gnu.atk.Role FONT_CHOOSER = new org.gnu.atk.Role(
            _FONT_CHOOSER);

    static final private int _FRAME = 22;

    static final public org.gnu.atk.Role FRAME = new org.gnu.atk.Role(_FRAME);

    static final private int _GLASS_PANE = 23;

    static final public org.gnu.atk.Role GLASS_PANE = new org.gnu.atk.Role(
            _GLASS_PANE);

    static final private int _HTML_CONTAINER = 24;

    static final public org.gnu.atk.Role HTML_CONTAINER = new org.gnu.atk.Role(
            _HTML_CONTAINER);

    static final private int _ICON = 25;

    static final public org.gnu.atk.Role ICON = new org.gnu.atk.Role(_ICON);

    static final private int _IMAGE = 26;

    static final public org.gnu.atk.Role IMAGE = new org.gnu.atk.Role(_IMAGE);

    static final private int _INTERNAL_FRAME = 27;

    static final public org.gnu.atk.Role INTERNAL_FRAME = new org.gnu.atk.Role(
            _INTERNAL_FRAME);

    static final private int _LABEL = 28;

    static final public org.gnu.atk.Role LABEL = new org.gnu.atk.Role(_LABEL);

    static final private int _LAYERED_PANE = 29;

    static final public org.gnu.atk.Role LAYERED_PANE = new org.gnu.atk.Role(
            _LAYERED_PANE);

    static final private int _LIST = 30;

    static final public org.gnu.atk.Role LIST = new org.gnu.atk.Role(_LIST);

    static final private int _LIST_ITEM = 31;

    static final public org.gnu.atk.Role LIST_ITEM = new org.gnu.atk.Role(
            _LIST_ITEM);

    static final private int _MENU = 32;

    static final public org.gnu.atk.Role MENU = new org.gnu.atk.Role(_MENU);

    static final private int _MENU_BAR = 33;

    static final public org.gnu.atk.Role MENU_BAR = new org.gnu.atk.Role(
            _MENU_BAR);

    static final private int _MENU_ITEM = 34;

    static final public org.gnu.atk.Role MENU_ITEM = new org.gnu.atk.Role(
            _MENU_ITEM);

    static final private int _OPTION_PANE = 35;

    static final public org.gnu.atk.Role OPTION_PANE = new org.gnu.atk.Role(
            _OPTION_PANE);

    static final private int _PAGE_TAB = 36;

    static final public org.gnu.atk.Role PAGE_TAB = new org.gnu.atk.Role(
            _PAGE_TAB);

    static final private int _PAGE_TAB_LIST = 37;

    static final public org.gnu.atk.Role PAGE_TAB_LIST = new org.gnu.atk.Role(
            _PAGE_TAB_LIST);

    static final private int _PANEL = 38;

    static final public org.gnu.atk.Role PANEL = new org.gnu.atk.Role(_PANEL);

    static final private int _PASSWORD_TEXT = 39;

    static final public org.gnu.atk.Role PASSWORD_TEXT = new org.gnu.atk.Role(
            _PASSWORD_TEXT);

    static final private int _POPUP_MENU = 40;

    static final public org.gnu.atk.Role POPUP_MENU = new org.gnu.atk.Role(
            _POPUP_MENU);

    static final private int _PROGRESS_BAR = 41;

    static final public org.gnu.atk.Role PROGRESS_BAR = new org.gnu.atk.Role(
            _PROGRESS_BAR);

    static final private int _PUSH_BUTTON = 42;

    static final public org.gnu.atk.Role PUSH_BUTTON = new org.gnu.atk.Role(
            _PUSH_BUTTON);

    static final private int _RADIO_BUTTON = 43;

    static final public org.gnu.atk.Role RADIO_BUTTON = new org.gnu.atk.Role(
            _RADIO_BUTTON);

    static final private int _RADIO_MENU_ITEM = 44;

    static final public org.gnu.atk.Role RADIO_MENU_ITEM = new org.gnu.atk.Role(
            _RADIO_MENU_ITEM);

    static final private int _ROOT_PANE = 45;

    static final public org.gnu.atk.Role ROOT_PANE = new org.gnu.atk.Role(
            _ROOT_PANE);

    static final private int _ROW_HEADER = 46;

    static final public org.gnu.atk.Role ROW_HEADER = new org.gnu.atk.Role(
            _ROW_HEADER);

    static final private int _SCROLL_BAR = 47;

    static final public org.gnu.atk.Role SCROLL_BAR = new org.gnu.atk.Role(
            _SCROLL_BAR);

    static final private int _SCROLL_PANE = 48;

    static final public org.gnu.atk.Role SCROLL_PANE = new org.gnu.atk.Role(
            _SCROLL_PANE);

    static final private int _SEPARATOR = 49;

    static final public org.gnu.atk.Role SEPARATOR = new org.gnu.atk.Role(
            _SEPARATOR);

    static final private int _SLIDER = 50;

    static final public org.gnu.atk.Role SLIDER = new org.gnu.atk.Role(_SLIDER);

    static final private int _SPLIT_PANE = 51;

    static final public org.gnu.atk.Role SPLIT_PANE = new org.gnu.atk.Role(
            _SPLIT_PANE);

    static final private int _SPIN_BUTTON = 52;

    static final public org.gnu.atk.Role SPIN_BUTTON = new org.gnu.atk.Role(
            _SPIN_BUTTON);

    static final private int _STATUSBAR = 53;

    static final public org.gnu.atk.Role STATUSBAR = new org.gnu.atk.Role(
            _STATUSBAR);

    static final private int _TABLE = 54;

    static final public org.gnu.atk.Role TABLE = new org.gnu.atk.Role(_TABLE);

    static final private int _TABLE_CELL = 55;

    static final public org.gnu.atk.Role TABLE_CELL = new org.gnu.atk.Role(
            _TABLE_CELL);

    static final private int _TABLE_COLUMN_HEADER = 56;

    static final public org.gnu.atk.Role TABLE_COLUMN_HEADER = new org.gnu.atk.Role(
            _TABLE_COLUMN_HEADER);

    static final private int _TABLE_ROW_HEADER = 57;

    static final public org.gnu.atk.Role TABLE_ROW_HEADER = new org.gnu.atk.Role(
            _TABLE_ROW_HEADER);

    static final private int _TEAR_OFF_MENU_ITEM = 58;

    static final public org.gnu.atk.Role TEAR_OFF_MENU_ITEM = new org.gnu.atk.Role(
            _TEAR_OFF_MENU_ITEM);

    static final private int _TERMINAL = 59;

    static final public org.gnu.atk.Role TERMINAL = new org.gnu.atk.Role(
            _TERMINAL);

    static final private int _TEXT = 60;

    static final public org.gnu.atk.Role TEXT = new org.gnu.atk.Role(_TEXT);

    static final private int _TOGGLE_BUTTON = 61;

    static final public org.gnu.atk.Role TOGGLE_BUTTON = new org.gnu.atk.Role(
            _TOGGLE_BUTTON);

    static final private int _TOOL_BAR = 62;

    static final public org.gnu.atk.Role TOOL_BAR = new org.gnu.atk.Role(
            _TOOL_BAR);

    static final private int _TOOL_TIP = 63;

    static final public org.gnu.atk.Role TOOL_TIP = new org.gnu.atk.Role(
            _TOOL_TIP);

    static final private int _TREE = 64;

    static final public org.gnu.atk.Role TREE = new org.gnu.atk.Role(_TREE);

    static final private int _TREE_TABLE = 65;

    static final public org.gnu.atk.Role TREE_TABLE = new org.gnu.atk.Role(
            _TREE_TABLE);

    static final private int _UNKNOWN = 66;

    static final public org.gnu.atk.Role UNKNOWN = new org.gnu.atk.Role(
            _UNKNOWN);

    static final private int _VIEWPORT = 67;

    static final public org.gnu.atk.Role VIEWPORT = new org.gnu.atk.Role(
            _VIEWPORT);

    static final private int _WINDOW = 68;

    static final public org.gnu.atk.Role WINDOW = new org.gnu.atk.Role(_WINDOW);

    static final private int _HEADER = 69;

    static final public org.gnu.atk.Role HEADER = new org.gnu.atk.Role(_HEADER);

    static final private int _FOOTER = 70;

    static final public org.gnu.atk.Role FOOTER = new org.gnu.atk.Role(_FOOTER);

    static final private int _PARAGRAPH = 71;

    static final public org.gnu.atk.Role PARAGRAPH = new org.gnu.atk.Role(
            _PARAGRAPH);

    static final private int _RULER = 72;

    static final public org.gnu.atk.Role RULER = new org.gnu.atk.Role(_RULER);

    static final private int _APPLICATION = 73;

    static final public org.gnu.atk.Role APPLICATION = new org.gnu.atk.Role(
            _APPLICATION);

    static final private int _AUTOCOMPLETE = 74;

    static final public org.gnu.atk.Role AUTOCOMPLETE = new org.gnu.atk.Role(
            _AUTOCOMPLETE);

    static final private int _EDITBAR = 75;

    static final public org.gnu.atk.Role EDITBAR = new org.gnu.atk.Role(
            _EDITBAR);

    static final private int _EMBEDDED = 76;

    static final public org.gnu.atk.Role EMBEDDED = new org.gnu.atk.Role(
            _EMBEDDED);

    static final private int _LAST_DEFINED = 77;

    static final public org.gnu.atk.Role LAST_DEFINED = new org.gnu.atk.Role(
            _LAST_DEFINED);

    static final private org.gnu.atk.Role[] theInterned = new org.gnu.atk.Role[] {
            INVALID, ACCEL_LABEL, ALERT, ANIMATION, ARROW, CALENDAR, CANVAS,
            CHECK_BOX, CHECK_MENU_ITEM, COLOR_CHOOSER, COLUMN_HEADER,
            COMBO_BOX, DATE_EDITOR, DESKTOP_ICON, DESKTOP_FRAME, DIAL, DIALOG,
            DIRECOTRY_PANE, DRAWING_AREA, FILE_CHOOSER, FILLER, FONT_CHOOSER,
            FRAME, GLASS_PANE, HTML_CONTAINER, ICON, IMAGE, INTERNAL_FRAME,
            LABEL, LAYERED_PANE, LIST, LIST_ITEM, MENU, MENU_BAR, MENU_ITEM,
            OPTION_PANE, PAGE_TAB, PAGE_TAB_LIST, PANEL, PASSWORD_TEXT,
            POPUP_MENU, PROGRESS_BAR, PUSH_BUTTON, RADIO_BUTTON,
            RADIO_MENU_ITEM, ROOT_PANE, ROW_HEADER, SCROLL_BAR, SCROLL_PANE,
            SEPARATOR, SLIDER, SPLIT_PANE, SPIN_BUTTON, STATUSBAR, TABLE,
            TABLE_CELL, TABLE_COLUMN_HEADER, TABLE_ROW_HEADER,
            TEAR_OFF_MENU_ITEM, TERMINAL, TEXT, TOGGLE_BUTTON, TOOL_BAR,
            TOOL_TIP, TREE, TREE_TABLE, UNKNOWN, VIEWPORT, WINDOW, HEADER,
            FOOTER, PARAGRAPH, RULER, APPLICATION, AUTOCOMPLETE, EDITBAR,
            EMBEDDED, LAST_DEFINED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.atk.Role theSacrificialOne = new org.gnu.atk.Role(
            0);

    static public org.gnu.atk.Role intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.atk.Role already = (org.gnu.atk.Role) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.atk.Role(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Role(int value) {
        value_ = value;
    }

    public org.gnu.atk.Role or(org.gnu.atk.Role other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.atk.Role and(org.gnu.atk.Role other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.atk.Role xor(org.gnu.atk.Role other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.atk.Role other) {
        return (value_ & other.value_) == other.value_;
    }

}
