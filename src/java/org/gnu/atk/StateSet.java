/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.StateSet</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class StateSet extends GObject {

    public StateSet() {
        super(atk_state_set_new());
    }

    public StateSet(Handle handle) {
        super(handle);
    }

    public boolean isEmpty() {
        return atk_state_set_is_empty(getHandle());
    }

    public boolean addState(StateType type) {
        return atk_state_set_add_state(getHandle(), type.getValue());
    }

    public void addStates(StateType[] types) {
        atk_state_set_add_states(getHandle(), getStateTypeValues(types));
    }

    public void clearStates() {
        atk_state_set_clear_states(getHandle());
    }

    public boolean containsState(StateType type) {
        return atk_state_set_contains_state(getHandle(), type.getValue());
    }

    public boolean containsStates(StateType[] types) {
        return atk_state_set_contains_states(getHandle(),
                getStateTypeValues(types));
    }

    private int[] getStateTypeValues(StateType[] types) {
        int[] val = new int[types.length];
        for (int i = 0; i < types.length; i++)
            val[i] = types[i].getValue();
        return val;
    }

    native static final protected int atk_state_set_get_type();

    native static final protected Handle atk_state_set_new();

    native static final protected boolean atk_state_set_is_empty(Handle set);

    native static final protected boolean atk_state_set_add_state(Handle set,
            int type);

    native static final protected void atk_state_set_add_states(Handle set,
            int[] types);

    native static final protected void atk_state_set_clear_states(Handle set);

    native static final protected boolean atk_state_set_contains_state(
            Handle set, int type);

    native static final protected boolean atk_state_set_contains_states(
            Handle set, int[] types);

    native static final protected boolean atk_state_set_remove_state(
            Handle set, int type);

    native static final protected Handle atk_state_set_and_sets(Handle set,
            Handle compareSet);

    native static final protected Handle atk_state_set_or_sets(Handle set,
            Handle compareSet);

    native static final protected Handle atk_state_set_xor_sets(Handle set,
            Handle compareSet);

}
