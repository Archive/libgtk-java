/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class RelationSet extends GObject {
    /**
     * Create a new empty RelationSet
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public RelationSet() {
        super(atk_relation_set_new());
    }

    /**
     * Add a new relation to the current relation set if it is not already
     * present.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void add(Relation relation) {
        atk_relation_set_add(getHandle(), relation.getHandle());
    }

    public void add(RelationType type, AtkObject target) {
        atk_relation_set_add_relation_by_type(getHandle(), type.getValue(),
                target.getHandle());
    }

    /**
     * Remove a relation from the relation set.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void remove(Relation relation) {
        atk_relation_set_remove(getHandle(), relation.getHandle());
    }

    /**
     * Determines whether the relation set contains a relation that matches the
     * specified type.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean contains(RelationType relationship) {
        return atk_relation_set_contains(getHandle(), relationship.getValue());
    }

    public int getNumRelations() {
        return atk_relation_set_get_n_relations(getHandle());
    }

    public Relation getRelation(int index) {
        Handle hndl = atk_relation_set_get_relation(getHandle(), index);
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            if (obj != null) {
                return (Relation) obj;
            } else {
                return new Relation(hndl);
            }
        }
        return null;
    }

    public Relation getRelationByType(RelationType type) {
        Handle hndl = atk_relation_set_get_relation_by_type(getHandle(), type
                .getValue());
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            if (obj != null) {
                return (Relation) obj;
            } else {
                return new Relation(hndl);
            }
        }
        return null;
    }

    native static final protected int atk_relation_set_get_type();

    native static final protected Handle atk_relation_set_new();

    native static final protected boolean atk_relation_set_contains(Handle set,
            int relationship);

    native static final protected void atk_relation_set_remove(Handle set,
            Handle relation);

    native static final protected void atk_relation_set_add(Handle set,
            Handle relation);

    native static final protected int atk_relation_set_get_n_relations(
            Handle set);

    native static final protected Handle atk_relation_set_get_relation(
            Handle set, int index);

    native static final protected Handle atk_relation_set_get_relation_by_type(
            Handle set, int relationship);

    native static final protected void atk_relation_set_add_relation_by_type(
            Handle set, int relationship, Handle target);

}
