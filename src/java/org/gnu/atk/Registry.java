/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.Registry</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Registry extends GObject {

    public Registry() {
        super(atk_get_default_registry());
    }

    public Registry(Handle handle) {
        super(handle);
    }

    public void setFactoryType(Type type, Type factoryType) {
        atk_registry_set_factory_type(getHandle(), type.getTypeHandle(),
                factoryType.getTypeHandle());
    }

    public Type getFactoryType(Type type) {
        return new Type(atk_registry_get_factory_type(getHandle(), type
                .getTypeHandle()));
    }

    public ObjectFactory getFactory(Type type) {
        Handle hndl = atk_registry_get_factory(getHandle(), type
                .getTypeHandle());
        GObject obj = getGObjectFromHandle(hndl);
        if (obj != null) {
            return (ObjectFactory) obj;
        } else {
            return new ObjectFactory(hndl);
        }
    }

    native static final protected int atk_registry_get_type();

    native static final protected void atk_registry_set_factory_type(
            Handle registry, int type, int factoryType);

    native static final protected int atk_registry_get_factory_type(
            Handle registry, int type);

    native static final protected Handle atk_registry_get_factory(
            Handle registry, int type);

    native static final protected Handle atk_get_default_registry();

}
