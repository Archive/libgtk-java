/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.Enum;

public class StateType extends Enum {

    static final private int _INVALID = 0;

    static final public org.gnu.atk.StateType INVALID = new org.gnu.atk.StateType(
            _INVALID);

    static final private int _ACTIVE = 1;

    static final public org.gnu.atk.StateType ACTIVE = new org.gnu.atk.StateType(
            _ACTIVE);

    static final private int _ARMED = 2;

    static final public org.gnu.atk.StateType ARMED = new org.gnu.atk.StateType(
            _ARMED);

    static final private int _BUSY = 3;

    static final public org.gnu.atk.StateType BUSY = new org.gnu.atk.StateType(
            _BUSY);

    static final private int _CHECKED = 4;

    static final public org.gnu.atk.StateType CHECKED = new org.gnu.atk.StateType(
            _CHECKED);

    static final private int _DEFUNCT = 5;

    static final public org.gnu.atk.StateType DEFUNCT = new org.gnu.atk.StateType(
            _DEFUNCT);

    static final private int _EDITABLE = 6;

    static final public org.gnu.atk.StateType EDITABLE = new org.gnu.atk.StateType(
            _EDITABLE);

    static final private int _ENABLED = 7;

    static final public org.gnu.atk.StateType ENABLED = new org.gnu.atk.StateType(
            _ENABLED);

    static final private int _EXPANDABLE = 8;

    static final public org.gnu.atk.StateType EXPANDABLE = new org.gnu.atk.StateType(
            _EXPANDABLE);

    static final private int _EXPANDED = 9;

    static final public org.gnu.atk.StateType EXPANDED = new org.gnu.atk.StateType(
            _EXPANDED);

    static final private int _FOCUSABLE = 10;

    static final public org.gnu.atk.StateType FOCUSABLE = new org.gnu.atk.StateType(
            _FOCUSABLE);

    static final private int _FOCUSED = 11;

    static final public org.gnu.atk.StateType FOCUSED = new org.gnu.atk.StateType(
            _FOCUSED);

    static final private int _HORIZONTAL = 12;

    static final public org.gnu.atk.StateType HORIZONTAL = new org.gnu.atk.StateType(
            _HORIZONTAL);

    static final private int _ICONIFIED = 13;

    static final public org.gnu.atk.StateType ICONIFIED = new org.gnu.atk.StateType(
            _ICONIFIED);

    static final private int _MODAL = 14;

    static final public org.gnu.atk.StateType MODAL = new org.gnu.atk.StateType(
            _MODAL);

    static final private int _MULTI_LINE = 15;

    static final public org.gnu.atk.StateType MULTI_LINE = new org.gnu.atk.StateType(
            _MULTI_LINE);

    static final private int _MULTISELECTABLE = 16;

    static final public org.gnu.atk.StateType MULTISELECTABLE = new org.gnu.atk.StateType(
            _MULTISELECTABLE);

    static final private int _OPAQUE = 17;

    static final public org.gnu.atk.StateType OPAQUE = new org.gnu.atk.StateType(
            _OPAQUE);

    static final private int _PRESSED = 18;

    static final public org.gnu.atk.StateType PRESSED = new org.gnu.atk.StateType(
            _PRESSED);

    static final private int _RESIZABLE = 19;

    static final public org.gnu.atk.StateType RESIZABLE = new org.gnu.atk.StateType(
            _RESIZABLE);

    static final private int _SELECTABLE = 20;

    static final public org.gnu.atk.StateType SELECTABLE = new org.gnu.atk.StateType(
            _SELECTABLE);

    static final private int _SELECTED = 21;

    static final public org.gnu.atk.StateType SELECTED = new org.gnu.atk.StateType(
            _SELECTED);

    static final private int _SENSITIVE = 22;

    static final public org.gnu.atk.StateType SENSITIVE = new org.gnu.atk.StateType(
            _SENSITIVE);

    static final private int _SHOWING = 23;

    static final public org.gnu.atk.StateType SHOWING = new org.gnu.atk.StateType(
            _SHOWING);

    static final private int _SINGLE_LINE = 24;

    static final public org.gnu.atk.StateType SINGLE_LINE = new org.gnu.atk.StateType(
            _SINGLE_LINE);

    static final private int _STALE = 25;

    static final public org.gnu.atk.StateType STALE = new org.gnu.atk.StateType(
            _STALE);

    static final private int _TRANSIENT = 26;

    static final public org.gnu.atk.StateType TRANSIENT = new org.gnu.atk.StateType(
            _TRANSIENT);

    static final private int _VERTICAL = 27;

    static final public org.gnu.atk.StateType VERTICAL = new org.gnu.atk.StateType(
            _VERTICAL);

    static final private int _VISIBLE = 28;

    static final public org.gnu.atk.StateType VISIBLE = new org.gnu.atk.StateType(
            _VISIBLE);

    static final private int _MANAGES_DESCENDANTS = 29;

    static final public org.gnu.atk.StateType MANAGES_DESCENDANTS = new org.gnu.atk.StateType(
            _MANAGES_DESCENDANTS);

    static final private int _INDETERMINATE = 30;

    static final public org.gnu.atk.StateType INDETERMINATE = new org.gnu.atk.StateType(
            _INDETERMINATE);

    static final private int _TRUNCATED = 31;

    static final public org.gnu.atk.StateType TRUNCATED = new org.gnu.atk.StateType(
            _TRUNCATED);

    static final private int _LAST_DEFINED = 32;

    static final public org.gnu.atk.StateType LAST_DEFINED = new org.gnu.atk.StateType(
            _LAST_DEFINED);

    static final private org.gnu.atk.StateType[] theInterned = new org.gnu.atk.StateType[] {
            INVALID, ACTIVE, ARMED, BUSY, CHECKED, DEFUNCT, EDITABLE, ENABLED,
            EXPANDABLE, EXPANDED, FOCUSABLE, FOCUSED, HORIZONTAL, ICONIFIED,
            MODAL, MULTI_LINE, MULTISELECTABLE, OPAQUE, PRESSED, RESIZABLE,
            SELECTABLE, SELECTED, SENSITIVE, SHOWING, SINGLE_LINE, STALE,
            TRANSIENT, VERTICAL, VISIBLE, LAST_DEFINED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.atk.StateType theSacrificialOne = new org.gnu.atk.StateType(
            0);

    static public org.gnu.atk.StateType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.atk.StateType already = (org.gnu.atk.StateType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.atk.StateType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private StateType(int value) {
        value_ = value;
    }

    public org.gnu.atk.StateType or(org.gnu.atk.StateType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.atk.StateType and(org.gnu.atk.StateType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.atk.StateType xor(org.gnu.atk.StateType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.atk.StateType other) {
        return (value_ & other.value_) == other.value_;
    }

    native static final protected byte[] atk_state_type_get_name(int type);

    native static final protected int atk_state_type_for_name(byte[] name);

}
