/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.Enum;

/**
 * Describes the layer of a component.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.Layer</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Layer extends Enum {

    static final private int _INVALID = 0;

    static final public org.gnu.atk.Layer INVALID = new org.gnu.atk.Layer(
            _INVALID);

    static final private int _BACKGROUND = 1;

    static final public org.gnu.atk.Layer BACKGROUND = new org.gnu.atk.Layer(
            _BACKGROUND);

    static final private int _CANVAS = 2;

    static final public org.gnu.atk.Layer CANVAS = new org.gnu.atk.Layer(
            _CANVAS);

    static final private int _WIDGET = 3;

    static final public org.gnu.atk.Layer WIDGET = new org.gnu.atk.Layer(
            _WIDGET);

    static final private int _MDI = 4;

    static final public org.gnu.atk.Layer MDI = new org.gnu.atk.Layer(_MDI);

    static final private int _POPUP = 5;

    static final public org.gnu.atk.Layer POPUP = new org.gnu.atk.Layer(_POPUP);

    static final private int _OVERLAY = 6;

    static final public org.gnu.atk.Layer OVERLAY = new org.gnu.atk.Layer(
            _OVERLAY);

    static final private int _WINDOW = 7;

    static final public org.gnu.atk.Layer WINDOW = new org.gnu.atk.Layer(
            _WINDOW);

    static final private org.gnu.atk.Layer[] theInterned = new org.gnu.atk.Layer[] {
            INVALID, BACKGROUND, CANVAS, WIDGET, MDI, POPUP, OVERLAY, WINDOW }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.atk.Layer theSacrificialOne = new org.gnu.atk.Layer(
            0);

    static public org.gnu.atk.Layer intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.atk.Layer already = (org.gnu.atk.Layer) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.atk.Layer(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private Layer(int value) {
        value_ = value;
    }

    public org.gnu.atk.Layer or(org.gnu.atk.Layer other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.atk.Layer and(org.gnu.atk.Layer other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.atk.Layer xor(org.gnu.atk.Layer other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.atk.Layer other) {
        return (value_ & other.value_) == other.value_;
    }

}
