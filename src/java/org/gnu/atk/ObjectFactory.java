/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Type;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.ObjectFactory</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class ObjectFactory extends GObject {

    public ObjectFactory(Handle handle) {
        super(handle);
    }

    public AtkObject createAccessible(GObject object) {
        return AtkObject
                .getAtkObjectFromHandle(atk_object_factory_create_accessible(
                        getHandle(), object.getHandle()));
    }

    public void invalidate() {
        atk_object_factory_invalidate(getHandle());
    }

    public Type getAccessibleType() {
        return new Type(atk_object_factory_get_accessible_type(getHandle()));
    }

    native static final protected int atk_object_factory_get_type();

    native static final protected Handle atk_object_factory_create_accessible(
            Handle factory, Handle object);

    native static final protected void atk_object_factory_invalidate(
            Handle factory);

    native static final protected int atk_object_factory_get_accessible_type(
            Handle factory);

}
