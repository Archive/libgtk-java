/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.Enum;

public class RelationType extends Enum {

    static final private int _NULL = 0;

    static final public org.gnu.atk.RelationType NULL = new org.gnu.atk.RelationType(
            _NULL);

    static final private int _CONTROLLED_BY = 1;

    static final public org.gnu.atk.RelationType CONTROLLED_BY = new org.gnu.atk.RelationType(
            _CONTROLLED_BY);

    static final private int _CONTROLLER_FOR = 2;

    static final public org.gnu.atk.RelationType CONTROLLER_FOR = new org.gnu.atk.RelationType(
            _CONTROLLER_FOR);

    static final private int _LABEL_FOR = 3;

    static final public org.gnu.atk.RelationType LABEL_FOR = new org.gnu.atk.RelationType(
            _LABEL_FOR);

    static final private int _LABELLED_BY = 4;

    static final public org.gnu.atk.RelationType LABELLED_BY = new org.gnu.atk.RelationType(
            _LABELLED_BY);

    static final private int _MEMBER_OF = 5;

    static final public org.gnu.atk.RelationType MEMBER_OF = new org.gnu.atk.RelationType(
            _MEMBER_OF);

    static final private int _NODE_CHILD_OF = 6;

    static final public org.gnu.atk.RelationType NODE_CHILD_OF = new org.gnu.atk.RelationType(
            _NODE_CHILD_OF);

    static final private int _FLOWS_TO = 7;

    static final public org.gnu.atk.RelationType FLOWS_TO = new org.gnu.atk.RelationType(
            _FLOWS_TO);

    static final private int _FLOWS_FROM = 8;

    static final public org.gnu.atk.RelationType FLOWS_FROM = new org.gnu.atk.RelationType(
            _FLOWS_FROM);

    static final private int _SUBWINDOW_OF = 9;

    static final public org.gnu.atk.RelationType SUBWINDOW_OF = new org.gnu.atk.RelationType(
            _SUBWINDOW_OF);

    static final private int _EMBEDS = 10;

    static final public org.gnu.atk.RelationType EMBEDS = new org.gnu.atk.RelationType(
            _EMBEDS);

    static final private int _EMBEDDED_BY = 11;

    static final public org.gnu.atk.RelationType EMBEDDED_BY = new org.gnu.atk.RelationType(
            _EMBEDDED_BY);

    static final private int _POPUP_FOR = 12;

    static final public org.gnu.atk.RelationType POPUP_FOR = new org.gnu.atk.RelationType(
            _POPUP_FOR);

    static final private int _PARENT_WINDOW_OF = 13;

    static final public org.gnu.atk.RelationType PARENT_WINDOW_OF = new org.gnu.atk.RelationType(
            _PARENT_WINDOW_OF);

    static final private int _LAST_DEFINED = 14;

    static final public org.gnu.atk.RelationType LAST_DEFINED = new org.gnu.atk.RelationType(
            _LAST_DEFINED);

    static final private org.gnu.atk.RelationType[] theInterned = new org.gnu.atk.RelationType[] {
            NULL, CONTROLLED_BY, CONTROLLER_FOR, LABEL_FOR, LABELLED_BY,
            MEMBER_OF, NODE_CHILD_OF, FLOWS_TO, FLOWS_FROM, SUBWINDOW_OF,
            EMBEDS, EMBEDDED_BY, POPUP_FOR, PARENT_WINDOW_OF, LAST_DEFINED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.atk.RelationType theSacrificialOne = new org.gnu.atk.RelationType(
            0);

    static public org.gnu.atk.RelationType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.atk.RelationType already = (org.gnu.atk.RelationType) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.atk.RelationType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private RelationType(int value) {
        value_ = value;
    }

    public org.gnu.atk.RelationType or(org.gnu.atk.RelationType other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.atk.RelationType and(org.gnu.atk.RelationType other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.atk.RelationType xor(org.gnu.atk.RelationType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.atk.RelationType other) {
        return (value_ & other.value_) == other.value_;
    }

    native static final protected int atk_relation_type_register(byte[] name);

    native static final protected byte[] atk_relation_type_get_name(int type);

    native static final protected int atk_relation_type_for_name(byte[] name);
}
