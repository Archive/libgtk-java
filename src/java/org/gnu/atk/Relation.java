/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class Relation extends GObject {
    public Relation(AtkObject[] targets, RelationType relationship) {
        super(init(targets, relationship));
    }

    public Relation(Handle handle) {
        super(handle);
    }

    private static Handle init(AtkObject[] targets, RelationType relationship) {
        Handle[] hndls = new Handle[targets.length];
        for (int i = 0; i < targets.length; i++)
            hndls[i] = targets[i].getHandle();
        return atk_relation_new(hndls, relationship.getValue());
    }

    public static RelationType registerType(String name) {
        return RelationType.intern(atk_relation_type_register(name));
    }

    public static String getRelationTypeName(RelationType type) {
        return atk_relation_type_get_name(type.getValue());
    }

    public static RelationType getRelationTypeForName(String name) {
        return RelationType.intern(atk_relation_type_for_name(name));
    }

    public RelationType getRelationType() {
        return RelationType.intern(atk_relation_get_relation_type(getHandle()));
    }

    public AtkObject[] getTargets() {
        Handle[] hndls = atk_relation_get_target(getHandle());
        if (hndls == null)
            return null;
        AtkObject[] objs = new AtkObject[hndls.length];
        for (int i = 0; i < hndls.length; i++) {
            objs[i] = AtkObject.getAtkObjectFromHandle(hndls[i]);
        }
        return objs;
    }

    public void addTarget(AtkObject target) {
        atk_relation_add_target(getHandle(), target.getHandle());
    }

    native static final protected int atk_relation_get_type();

    native static final protected int atk_relation_type_register(String name);

    native static final protected String atk_relation_type_get_name(int type);

    native static final protected int atk_relation_type_for_name(String name);

    native static final protected Handle atk_relation_new(Handle[] targets,
            int relationship);

    native static final protected int atk_relation_get_relation_type(
            Handle relation);

    native static final protected Handle[] atk_relation_get_target(
            Handle relation);

    native static final protected void atk_relation_add_target(Handle relation,
            Handle target);

}
