/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * This class is the primary class for accessibility support via the
 * Accessibility TookKit (ATK). Objects which are instances of AtkObject are
 * queried for properties which relate basic properties of a UI component such
 * as name and description.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.AtkObject</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class AtkObject extends GObject {
    /**
     * Construct a new AtkObject using the handle returned by a call to the
     * native layer.
     * 
     * @param handle
     *            The handle to the native resource.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AtkObject(Handle handle) {
        super(handle);
    }

    /**
     * Gets the accessible name of the accessible.
     * 
     * @return A string representing the accessible name of the accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getName() {
        return atk_object_get_name(getHandle());
    }

    /**
     * Sets the accessible name of the accessible.
     * 
     * @param name
     *            The accessible name.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setName(String name) {
        atk_object_set_name(getHandle(), name);
    }

    /**
     * Gets the accessible description of the accessible.
     * 
     * @return A string representing the accessible description of the
     *         accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getDescription() {
        return atk_object_get_description(getHandle());
    }

    /**
     * Sets the accessible description of the accessible.
     * 
     * @param description
     *            The accessible description.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDescription(String description) {
        atk_object_set_description(getHandle(), description);
    }

    /**
     * Get the accessible parent of the accessible.
     * 
     * @return An AtkObject representing the accessible parent of the
     *         accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AtkObject getParent() {
        return getAtkObjectFromHandle(atk_object_get_parent(getHandle()));
    }

    /**
     * Sets the accessible parent of the accessible.
     * 
     * @param parent
     *            The accessible parent.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setParent(AtkObject parent) {
        atk_object_set_parent(getHandle(), parent.getHandle());
    }

    /**
     * Returns the number of accessible children of the accessible.
     * 
     * @return the number of accessible children.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getNumChildren() {
        return atk_object_get_n_accessible_children(getHandle());
    }

    /**
     * Returns the specified child of the object. The index is 0 based.
     * 
     * @param index
     *            The position of the child starting at 0.
     * @return An AtkObject that is the child at the specified position.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public AtkObject getChild(int index) {
        return getAtkObjectFromHandle(atk_object_ref_accessible_child(
                getHandle(), index));
    }

    /**
     * Gets the role of the accessible.
     * 
     * @return A Role which is the role of the accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Role getRole() {
        return Role.intern(atk_object_get_role(getHandle()));
    }

    /**
     * Sets the role of the accessible.
     * 
     * @param role
     *            The role to be set.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setRole(Role role) {
        atk_object_set_role(getHandle(), role.getValue());
    }

    /**
     * Get the Layer of the accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Layer getLayer() {
        return Layer.intern(atk_object_get_layer(getHandle()));
    }

    /**
     * Get the zorder of the accessible.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getMDIZorder() {
        return atk_object_get_mdi_zorder(getHandle());
    }

    /**
     * Get the index of the accessible in its parent. Returns -1 if the
     * accessible does not have an accessible parent.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getIndexInParent() {
        return atk_object_get_index_in_parent(getHandle());
    }

    /**
     * Get the description describing the Role.
     * 
     * @param role
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getRoleName(Role role) {
        return atk_role_get_name(role.getValue());
    }

    /**
     * Get the role corresponding to the name.
     * 
     * @param name
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Role getRoleForName(String name) {
        return Role.intern(atk_role_for_name(name));
    }

    /**
     * Adds a relationship of the specified type with the specified target.
     * 
     * @param type
     * @param target
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean addRelationship(RelationType type, AtkObject target) {
        return atk_object_add_relationship(getHandle(), type.getValue(), target
                .getHandle());
    }

    /**
     * Removes a relationship of the specified type with the specified target.
     * 
     * @param type
     * @param target
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean removeRelationship(RelationType type, AtkObject target) {
        return atk_object_remove_relationship(getHandle(), type.getValue(),
                target.getHandle());
    }

    /**
     * Get the localized description describing the Role
     * 
     * @param role
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getLocalizedRoleName(Role role) {
        return atk_role_get_localized_name(role.getValue());
    }

    /**
     * Static factory method that should only be used interally by Java-Gnome.
     * @deprecated Superceeded by java-gnome 4.0; this method or constant
     *             will no doubt exist conceptually, but it may have a different
     *             name or signature in order that the presented API is an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static AtkObject getAtkObjectFromHandle(Handle hndl) {
        if (hndl != null) {
            GObject obj = GObject.getGObjectFromHandle(hndl);
            if (obj != null) {
                return (AtkObject) obj;
            } else {
                return new AtkObject(hndl);
            }
        }
        return null;
    }

    native static final protected int atk_object_get_type();

    native static final protected String atk_object_get_name(Handle accessible);

    native static final protected String atk_object_get_description(
            Handle accessible);

    native static final protected Handle atk_object_get_parent(Handle accessible);

    native static final protected int atk_object_get_n_accessible_children(
            Handle accessible);

    native static final protected Handle atk_object_ref_accessible_child(
            Handle accessible, int i);

    native static final protected Handle atk_object_ref_relation_set(
            Handle accessible);

    native static final protected int atk_object_get_role(Handle accessible);

    native static final protected int atk_object_get_layer(Handle accessible);

    native static final protected int atk_object_get_mdi_zorder(
            Handle accessible);

    native static final protected int atk_object_get_index_in_parent(
            Handle accessible);

    native static final protected void atk_object_set_name(Handle accessible,
            String name);

    native static final protected void atk_object_set_description(
            Handle accessible, String description);

    native static final protected void atk_object_set_parent(Handle accessible,
            Handle parent);

    native static final protected void atk_object_set_role(Handle accessible,
            int role);

    native static final protected void atk_object_remove_property_change_handler(
            Handle accessible, int handlerId);

    native static final protected void atk_object_notify_state_change(
            Handle accessible, int state, boolean value);

    native static final protected void atk_object_initialize(Handle accessible,
            long data);

    native static final protected String atk_role_get_name(int role);

    native static final protected int atk_role_for_name(String name);

    native static final protected boolean atk_object_add_relationship(
            Handle object, int relationship, Handle target);

    native static final protected boolean atk_object_remove_relationship(
            Handle object, int relationship, Handle target);

    native static final protected String atk_role_get_localized_name(int role);

    native static final protected Handle atk_object_ref_state_set(
            Handle accessible);

}
