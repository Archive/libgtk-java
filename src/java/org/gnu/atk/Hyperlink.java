/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.atk;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may exist in java-gnome 4.0; look out for
 *             <code>org.gnome.atk.Hyperlink</code>.
 *             As this package was never fully implemented in java-gnome 2.x,
 *             however, any new code written will have a considerably different
 *             public API.
 */
public class Hyperlink extends GObject {

    public Hyperlink(Handle handle) {
        super(handle);
    }

    public String getURI(int index) {
        return atk_hyperlink_get_uri(getHandle(), index);
    }

    public AtkObject getObject(int index) {
        return AtkObject.getAtkObjectFromHandle(atk_hyperlink_get_object(
                getHandle(), index));
    }

    public int getEndIndex() {
        return atk_hyperlink_get_end_index(getHandle());
    }

    public int getStartIndex() {
        return atk_hyperlink_get_start_index(getHandle());
    }

    public boolean isValid() {
        return atk_hyperlink_is_valid(getHandle());
    }

    public boolean isInline() {
        return atk_hyperlink_is_inline(getHandle());
    }

    public int getNumAnchors() {
        return atk_hyperlink_get_n_anchors(getHandle());
    }

    public boolean isSelectedLink() {
        return atk_hyperlink_is_selected_link(getHandle());
    }

    native static final protected int atk_hyperlink_get_type();

    native static final protected String atk_hyperlink_get_uri(Handle link,
            int i);

    native static final protected Handle atk_hyperlink_get_object(Handle link,
            int i);

    native static final protected int atk_hyperlink_get_end_index(Handle link);

    native static final protected int atk_hyperlink_get_start_index(Handle link);

    native static final protected boolean atk_hyperlink_is_valid(Handle link);

    native static final protected boolean atk_hyperlink_is_inline(Handle link);

    native static final protected int atk_hyperlink_get_n_anchors(Handle link);

    native static final protected boolean atk_hyperlink_is_selected_link(
            Handle link);

}
