import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * A Java class for validating a GTK language binding implementation.  The
 * validation is a simple check of the functions defined in the GTK .h files
 * against the language bindings native methods (i.e. methods that map to the
 * GTK public functions).  It assumes that they have the same name in both the
 * GTK .h files and in the language bindings files.
 * <p>
 * This class can be used in the following manner:
 * 
 * <pre>
 *   % java -DbindingsValidator.gtk.dir=&lt;directory&gt; -DbindingsValidator.gtk.regex=&lt;regex&gt; -DbindingsValidator.language.dir=&lt;directory&gt; BindingsValidator
 * </pre>
 * 
 * The general execution flow is the following (mostly in the <tt>main</tt> 
 * method):
 * <ol>
 * <li>Instantiate the <tt>BindingsValidator</tt> class.</li>
 * <li>Call the <tt>{@link #getBindingsInfo}</tt> method on this instance. 
 * This method returns an <tt>ArrayList</tt> of <tt>{@link GTKBinding}</tt> 
 * objects, one for each file parsed.  Internally, for each GTK .h file, 
 * this method:
 *   <ol>
 *   <li>First calls <tt>{@link #grepGTKHeaderFile}</tt> to parse the GTK 
 *   header files and store function names in an instance of the 
 *   <tt>GTKBinding</tt> object.</li>
 *   <li>Then the <tt>{@link #grepJavaFile}</tt> method is called to parse the 
 *   language bindings file and store the native method definitions in the 
 *   <tt>GTKBindings</tt> object.
 *   </li>
 *   <li>The <tt>GTKBinding</tt> object is added to the returned 
 *   <tt>ArrayList</tt>.
 *   </ol>
 * </li>
 * <li>For each <tt>GTKBinding</tt> object returned by the 
 * <tt>getBindingsInfo</tt> method, output (in the type specified by the 
 * <tt>bindingsValidator.output.type</tt> Java property) the info for this 
 * binding. One of the <tt>to*String</tt> methods on the <tt>GTKBinding</tt> 
 * object is called.</li>
 * </ol>
 *
 * <p>
 * The following Java properties are available:
 * <ul>
 *
 * <li><tt>bindingsValidator.gtk.dir</tt> - The directory containing the 
 * GTK .h files against which the language bindings are validated. Defaults to
 * <tt>/usr/local/gnome2/include/gtk-2.0/gtk</tt>.</li>
 *
 * <li><tt>bindingsValidator.gtk.regex</tt> - A regular expression used to 
 * filter the GTK files that will be parsed.  Example: use ".*\.h" to select 
 * all .h files. Or use ".*label.*" to select the .h files whose name 
 * contains "label" (will select "gtkaccellabel.h" and "gtklabel.h").  
 * Defaults to ".*\.h".</li>
 *
 * <li><tt>bindingsValidator.language.dir</tt> - The directory of the 
 * language binding tree that contains the language files to parse.  For 
 * Java-Gnome, this is 
 * <tt>&lt;path-to-JG-source&gt;/libgtk-java/src/java/org/gnu/gtk</tt>.  
 * Defaults to <tt>/usr/local/gnome2/libgtk-java/src/java/org/gnu/gtk</tt>.
 * </li>
 *
 * <li><tt>bindingsValidator.output.type</tt> - The type of output generated.
 * Defaults to <tt>validate</tt>. Available output types are:
 * <ul>
 * <li><tt>validate</tt> - For each GTK header file, show the public functions
 * and whether they are implemented in the language bindings file. This is
 * the default.</li>
 * <li><tt>missingfuncs</tt> - Show only the functions that are 
 * missing from the language binding's implementation.</li>
 * <li><tt>missingfiles</tt> - Generate a list of files that have not been
 * implemented in the language binding. Useful when parsing all GTK .h 
 * files.</li>
 * <li><tt>native</tt> - Generate native method declarations for the language
 * bindings.  These are simple, generic method declarations and must be 
 * modified by hand for proper return type and parameter values.</li>
 * <li><tt>list</tt> - List the functions and native methods for all selected
 * GTK header files and the corresponding language binding file.</li>
 * <li><tt>jni</tt> - Generate JNI functions for the missing language
 * binding methods. This requires that the Java property,
 * <tt>bindingsValidator.output.package</tt>, be set to the name of the
 * package within which these methods are located.</li> 
 * </ul>
 *
 * </ul>
 *
 * <p>
 * <em>NOTE</em>:
 * Certain GTK header files are not parsed.  These files are marked 
 * in the {@link #GTK_LANG} static array as "IGNORE".  Other files in this
 * array are marked as "DEPRECATED" and are also discarded.
 * <p>
 * <em>NOTE</em>:
 * GTK private functions are ignored.  A GTK private function is on that 
 * starts with an underscore character ('_').
 * <p>
 * <em>NOTE</em>:
 * This currently only works for Java language bindings, but theoretically 
 * could be upgraded to work with another language binding.  Not really sure 
 * if that's useful since the other languages probably already have their own 
 * implementation of something similar to this. NickRahn 2004-12-22.
 */
public class BindingsValidator {

    public static void main( String[] argv ) {

        // Get the properties.
        String gtkheadersdir = 
            System.getProperty( "bindingsValidator.gtk.dir", 
                                DEFAULT_GTK_HEADERS_DIR );
        String langdir = 
            System.getProperty( "bindingsValidator.language.dir", 
                                DEFAULT_LANG_DIR );
        String gtkheaderregex = 
            System.getProperty( "bindingsValidator.gtk.regex", 
                                DEFAULT_GTK_HEADER_REGEX );
        String bvOutputType = 
            System.getProperty( "bindingsValidator.output.type",
                                DEFAULT_OUTPUT_TYPE );
        String bvOutputPackage = 
            System.getProperty( "bindingsValidator.output.package",
                                DEFAULT_OUTPUT_PACKAGE );

        // Create our validator.
        BindingsValidator validator = new BindingsValidator( gtkheadersdir,
                                                             langdir );
        // Get the GTKBinding objects for the GTK header files that end with
        // the given string.
        
        ArrayList bindings = null;
        try {
            bindings = validator.getBindingsInfo( validator.getGTKHeadersDir(),
                                                  gtkheaderregex );
        } catch( FileNotFoundException fe ) {
            System.err.println( fe );
            return;
        } catch( IOException ie ) {
            System.err.println( ie );
            return;
        }

        if ( bindings.size() < 1 ) {
            System.out.println( "could not find any GTK header files!" );
            return;
        }

        // Loop through the GTKBinding objects, sending the output to stdout.
        ListIterator it = bindings.listIterator();
        String res = null;
        while( it.hasNext() ) {
            GTKBinding bind = (GTKBinding)it.next();
            if ( bvOutputType.equals( "validate" ) ) {
                res = bind.toValidatedString();
            } else if ( bvOutputType.equals( "missingfuncs" ) ) {
                res = bind.toOnlyMissingString();
            } else if ( bvOutputType.equals( "missingfiles" ) ) {
                res = bind.toMissingFilesString();
            } else if ( bvOutputType.equals( "native" ) ) {
                res = bind.toMissingAsNativeString();
            } else if ( bvOutputType.equals( "nativecall" ) ) {
                res = bind.toMissingAsNativeCallString();
            } else if ( bvOutputType.equals( "list" ) ) {
                res = bind.toString();
            } else if ( bvOutputType.equals( "jni" ) ) {
                res = bind.toMissingAsJNIString( bvOutputPackage );
            } else if ( bvOutputType.equals( "signallist" ) ) {
                res = bind.toSignalAsJava();
            } else {
                res = bind.toValidatedString();
            }
            if ( res != null && !res.trim().equals( "" ) ) {
                System.out.println( res );
            }
        }
    }

    private static final String DEFAULT_GTK_HEADERS_DIR = 
        "/usr/local/gnome2/include/gtk-2.0/gtk";
    private static final String DEFAULT_LANG_DIR = 
        "/usr/local/gnome2/java-gnome/libgtk-java/src/java/org/gnu/gtk";
    private static final String DEFAULT_GTK_HEADER_REGEX = ".*\\.h";
    private static final String DEFAULT_OUTPUT_TYPE = "validate";
    private static final String DEFAULT_OUTPUT_PACKAGE = "org.gnu.gtk";

    private static final Pattern GTK_H_IGNORE = 
        Pattern.compile( "(?:^\\s+$)|(?:^#.*$)|(?:^\\s*/\\*.*$)|(?:^[\\s\\w]*\\*/$)|(?:^\\s*\\*.*$)|(?:^extern.*$)" );
    private static final Pattern GTK_H_FUNCTION =
        Pattern.compile( "([\\w\\s\\*]+?)[\\s]*((?:_gtk_|g_|gtk_|gdk_|atk_|pango_|gnome_|gconf_|html_)[\\w]+)[\\s]*\\((.*)\\).*", Pattern.UNIX_LINES | Pattern.MULTILINE | Pattern.DOTALL );
    private static final Pattern JAVA_NATIVE_FUNCTION =
        Pattern.compile( "^[\\s]*[\\w\\s]+native[\\s]+[\\w\\s\\[\\]]+((?:gtk_|gdk_|atk_|pango_|gnome_|gconf_|html_)[\\w]+).*$" );
    private static final Pattern GTK_H_SIGNAL = 
        Pattern.compile( "^(?:[\\s]|/\\*.*\\*/)+^[\\s]*([\\w]+)[\\s]*\\(\\* *([\\w]+)\\)[\\s]*\\((.*)\\).*$", Pattern.UNIX_LINES | Pattern.MULTILINE | Pattern.DOTALL );

    private String gtkHDir;
    private String langDir;

    /**
     * Create a new BindingsValidator instance.
     *
     * @param gtkHDir The directory containing the GTK ".h" files.
     * @param langDir The directory containing the language bindings files.
     */
    public BindingsValidator( String gtkHDir, String langDir ) {
        this.gtkHDir = gtkHDir;
        this.langDir = langDir;
    }

    public String getGTKHeadersDir() { return gtkHDir; }
    public String getLanguageDir() { return langDir; }

    /**
     * Get <tt>{@link GTKBinding}</tt> objects for the GTK .h files that end
     * with the given string.
     *
     * @param basedir A path to the base directory containing the GTK .h files.
     * @param regex A String containing a regular expression used to filter
     * the GTK .h file names.
     * @return An ArrayList containing the GTKBindings objects.
     */
    public ArrayList getBindingsInfo( String basedir, String regex )
        throws FileNotFoundException, IOException {

        File gtkdir = new File( basedir );
        File gtkheaders[] = gtkdir.listFiles( new GTKHeaderFilter( regex ) );
        TreeSet sortedheaders = new TreeSet();
        ArrayList retarray = new ArrayList();
        if ( gtkheaders == null ) {
            return retarray;
        }
        // Put the files into a TreeSet so we can get them in 
        // alphabetical order.
        for( int i = 0; i < gtkheaders.length; i++ ) {
            sortedheaders.add( gtkheaders[ i ] );
        }
        String ghfilename;
        GTKBinding gtkbind;
        File jfile = null;
        File tmpfile = null;
        for( Iterator it = sortedheaders.iterator();
             it.hasNext(); ) {
            tmpfile = (File)it.next();
            ghfilename = tmpfile.getName();
            //System.out.println( "in: " + basedir + 
            //                  "(" + gtkheaders[ i ].isDirectory() + 
            //                  ") - found: " + ghfilename );
            if ( tmpfile.isDirectory() ) {
                retarray.addAll( getBindingsInfo( basedir + "/" + ghfilename,
                                                  regex ) );
            } else {
                gtkbind = new GTKBinding( ghfilename );
                grepGTKHeaderFile( tmpfile, gtkbind );
                String[] jfiles = gtkbind.getLanguageFilenames();
                if ( jfiles.length > 0 && 
                     ( jfiles[0].equals( "IGNORE" ) ||
                       jfiles[0].equals( "DEPRECATED" ) ) ) {
                    // Ignore special files.
                } else {
                    for( int i = 0; i < jfiles.length; i++ ) {
                        grepJavaFile( jfiles[ i ], gtkbind );
                    }
                    retarray.add( gtkbind );
                }
            }
        }
        return retarray;
    }

    protected void grepGTKHeaderFile( File hfile, GTKBinding gtkbind ) 
        throws FileNotFoundException, IOException {
        // Read the file into a string buffer.
        FileReader fr = new FileReader( hfile );
        BufferedReader br = new BufferedReader( fr );
        String line = "";
        StringBuffer fullfile = new StringBuffer();
        Matcher mat = null;
        while( (line = br.readLine()) != null ) {
            // Throw away any of the defined ignorable lines.
            mat = GTK_H_IGNORE.matcher( line );
            if ( !mat.matches() ) {
                fullfile.append( line );
                fullfile.append( "\n" );
            }
        }
        // Split the buffer into statements (split on ';').
        GTKFunction gtkfunc = null;
        Pattern semi = Pattern.compile( ";" );
        String[] statements = semi.split( fullfile );
        String gtkfuncname = null;
        // For each statement, create a GTKFunction.
        for( int i = 0; i < statements.length; i++ ) {
            mat = GTK_H_FUNCTION.matcher( statements[ i ] );
            if ( mat.matches() ) {
                //System.out.println( "MATCH![" + i + "]" + statements[ i ] );
                gtkfuncname = mat.group( 2 ).trim();
                if ( !gtkfuncname.startsWith( "_" ) ) {
                    gtkfunc = 
                        new GTKFunction( removeSpaces( mat.group( 1 ).
                                                       trim() ), 
                                         gtkfuncname,
                                         reduceSpaces( mat.group( 3 ).
                                                       trim() ) );
                    gtkbind.addGTKFunction( gtkfunc );
                } else {
                    // Ignore any private functions.
                }
            } else {
                mat = GTK_H_SIGNAL.matcher( statements[ i ] );
                if ( mat.matches() ) {
                    gtkfunc = 
                        new GTKFunction( removeSpaces( mat.group( 1 ).trim() ),
                                         mat.group( 2 ).trim(),
                                         reduceSpaces( mat.group( 3 ).
                                                       trim() ) );
                    gtkbind.addGTKSignal( gtkfunc );
                    //System.out.println( "[" + i + "]SIGNAL = '" + mat.group( 1 ) + "','" + mat.group( 2 ) + "','" + reduceSpaces( mat.group( 3 ).trim() ) + /*"','" + statements[ i ] +*/ "'" );
                }
            }
        }
    }

    protected void grepJavaFile( String jfilename, GTKBinding gtkbind ) 
        throws IOException {
        File jfile = new File( gtkbind.
                               getFullPathLanguageFilename( jfilename ) );
        FileReader fr;
        try {
            fr = new FileReader( jfile );
        } catch( FileNotFoundException fe ) {
            gtkbind.setLanguageImplemented( jfilename, false );
            return;
        }
        gtkbind.setLanguageImplemented( jfilename, true );
        BufferedReader br = new BufferedReader( fr );
        String line = "";
        Matcher mat = JAVA_NATIVE_FUNCTION.matcher( line );
        while( (line = br.readLine()) != null ) {
            mat.reset( line );
            if ( mat.matches() ) {
                gtkbind.addLanguageFunction( jfilename, 
                                             mat.group( 1 ).trim() );
            }
        }
    }

    /**
     * Map the given GTK filename to a language binding file name.
     */
    protected String getGTK2Lang( String gtkfile ) {
        return (String)gtk2lang.get( gtkfile );
    }

    /**
     * Map the given GTK type to a language binding type.
     */
    protected String getGTKType2Lang( String gtktype ) {
        String gtktypeg = removeSpaces( gtktype );
        String ret = (String)gtktype2lang.get( gtktypeg );
        if ( ret == null ) {
            if ( gtktypeg.startsWith( "G" ) ||
                 gtktypeg.startsWith( "Gtk" ) ||
                 gtktypeg.startsWith( "Gdk" ) ||
                 gtktypeg.startsWith( "Pango" ) ||
                 gtktypeg.startsWith( "Atk" ) ||
                 gtktypeg.startsWith( "Gnome" ) ||
                 gtktypeg.startsWith( "GConf" )  ) {
                if ( gtktypeg.endsWith( "*" ) ) {
                    // Assume a handle for Gtk/Gdk pointer types.
                    return "Handle";
                } else {
                    // Assume an int for Gtk/Gdk non-pointers (enums often).
                    return "int";
                }
            } else {
                // If nothing else matches, just return Gtk type.
                return gtktypeg;
            }
        } else {
            return ret;
        }
    }

    /**
     * Map the given GTK type to a Java JNI type.
     */
    protected String getGTKType2JNI( String gtktype ) {
        // First, get the Java type for this GTK type.
        String java = getGTKType2Lang( removeSpaces( gtktype ) );
        // Map to JNI type.
        String ret = (String)java2jni.get( java );
        if ( ret == null ) {
            return java;
        } else {
            return ret;
        }
    }

    /**
     * Class representing a GTK language binding function definition.
     */
    public class GTKFunction {
        private Pattern comma = Pattern.compile( "," );
        private Pattern spacer = 
            Pattern.compile( "^([\\w\\s\\*]+?)([\\w]+)$" );
        private String retType;
        private String funcName;
        private ArrayList parameters;
        public GTKFunction( String retType, String funcName, 
                            String pars ) {
            this.retType = retType;
            this.funcName = funcName;
            this.parameters = new ArrayList();
            String[] params = parseParams( pars );
            //System.out.print( "GTKFunction: '" + retType + "' " + funcName );
            //System.out.print( "(" );
            for( int i = 0; i < params.length; i++ ) {
                String param = params[i].trim();
                if ( !param.equals( "void" ) ) {
                    Matcher mat = spacer.matcher( param );
                    if ( mat.matches() ) {
                        parameters.add( mat.group( 1 ).trim() );
                        //                        parameters.add( removeSpaces( mat.group( 1 ).trim() ) );
                        parameters.add( mat.group( 2 ).trim() );
                        //System.out.print( mat.group( 1 ) + ":"+ mat.group( 2 ) );
                    }
                }
                //System.out.print( "," );
            }
            //System.out.println( ")" );
        }
        public String getReturnType() { return retType; }
        public String getFunctionName() { return funcName; }
        public ArrayList getParameters() { return parameters; }
        private String[] parseParams( String params ) {
            return comma.split( params );
        }
    }
        
    /**
     * Class representing a GTK language binding.  Contains an
     * ArrayList of the function names defined in the GTK .h file and
     * a Hashtable of ArrayLists of the native functions defined in
     * the corresponding language binding file.
     */
    public class GTKBinding {
        String hfilename;
        ArrayList hsignals;
        Hashtable hsignalshash;
        ArrayList hfunctions;
        Hashtable hfunctionshash;
        // Array of Java filenames to which bind the GTK functions.
        String[] jfilenames;
        // Contains an ArrayList keyed to Java filenames.
        Hashtable jfunctions;
        // Contains a Boolean keyed to Java filenames.
        Hashtable jimplemented;
        public GTKBinding( String h_name ) {
            hfilename = h_name;
            hsignals = new ArrayList();
            hsignalshash = new Hashtable();
            hfunctions = new ArrayList();
            hfunctionshash = new Hashtable();
            if ( getGTK2Lang( hfilename ) != null ) {
                jfilenames = getGTK2Lang( hfilename ).split( ";" );
            } else {
                //System.err.println( "ERROR: " + hfilename );
                jfilenames = new String[] { "" };
            }
            //System.out.println( "GTKBinding: " + jfilenames.length + ";" + jfilenames[0] );
            jfunctions = new Hashtable();
            jimplemented = new Hashtable();
        }

        public String getGTKFilename() { return hfilename; }
        public void addGTKFunction( GTKFunction func ) {
            hfunctionshash.put( func.getFunctionName(), func );
            hfunctions.add( func.getFunctionName() );
        }
        public boolean hasGTKFunction( String func ) {
            return hfunctions.contains( func );
        }
        public void addGTKSignal( GTKFunction func ) {
            hsignalshash.put( func.getFunctionName(), func );
            hsignals.add( func.getFunctionName() );
        }
        public boolean hasGTKSignal( String sig ) {
            return (hsignalshash.get( sig ) != null);
        }

        public String getFullPathLanguageFilename( String jfilename ) {
            return getLanguageDir() + "/" + jfilename;
        }
        public String[] getLanguageFilenames() { return jfilenames; }
        public void addLanguageFunction( String jfilename, String func ) {
            ArrayList jfuncts = (ArrayList)jfunctions.get( jfilename );
            if ( jfuncts == null ) {
                jfuncts = new ArrayList();
                jfunctions.put( jfilename, jfuncts );
            }
            jfuncts.add( func );
        }
        public boolean hasLanguageFunction( String func ) {
            for ( int i = 0; i < jfilenames.length; i++ ) {
                ArrayList jfuncts = (ArrayList)jfunctions.get( jfilenames[i] );
                if ( jfuncts != null && jfuncts.contains( func ) ) {
                    return true;
                }
            }
            return false;
        }
        public void setLanguageImplemented( String jfilename, boolean val ) {
            jimplemented.put( jfilename, new Boolean( val ) );
            //jimplemented = val;
        }
        public boolean getLanguageImplemented( String jfilename ) {
            if ( jimplemented.get( jfilename ) == null ) {
                return false;
            } else {
                return ((Boolean)jimplemented.get( jfilename )).booleanValue();
            }
        }
        public String getLanguageClass( String jfilename ) {
            return jfilename.substring( 0, jfilename.indexOf( "." ) );
        }
        private void addJFileImplemented( StringBuffer buf ) {
            for( int i = 0; i < jfilenames.length; i++ ) {
                buf.append( " [" );
                buf.append( jfilenames[ i ] );
                if ( !getLanguageImplemented( jfilenames[ i ] ) ) {
                    buf.append( " -> NOT IMPLEMENTED" );
                }
                buf.append( "]" );
            }
        }
        /**
         * Generate Java native method declarations for missing GTK functions.
         */
        public String toMissingAsNativeString() {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            addJFileImplemented( buf );
            String func;
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                if ( !hasLanguageFunction( func ) ) {
                    GTKFunction gtkfunc = 
                        (GTKFunction)hfunctionshash.get( func );
                    buf.append( "\n" );
                    buf.append( "    native static final private " );
                    buf.append( getGTKType2Lang( gtkfunc.getReturnType() ) );
                    buf.append( " " );
                    buf.append( func );
                    buf.append( "(" );
                    ListIterator it2 = gtkfunc.getParameters().listIterator();
                    while( it2.hasNext() ) {
                        buf.append( getGTKType2Lang( (String)it2.next() ) );
                        buf.append( " " );
                        buf.append( (String)it2.next() );
                        if ( it2.hasNext() ) {
                            buf.append( ", " );
                        }
                    }
                    buf.append( ");" );
                }
            }
            return buf.toString();
        }
        /**
         * Generate possible Java native method call declarations for missing
         * GTK functions.
         */
        public String toMissingAsNativeCallString() {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            addJFileImplemented( buf );
            String func;
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                if ( !hasLanguageFunction( func ) ) {
                    GTKFunction gtkfunc = 
                        (GTKFunction)hfunctionshash.get( func );
                    buf.append("\n    /**");
                    buf.append("\n     * ");
                    ListIterator it3 = gtkfunc.getParameters().listIterator();
                    while( it3.hasNext() ) {
                        String next =  getGTKType2Lang((String)it3.next());
                        if (!(next.equals("handle")
                              || next.equals("Handle"))){
                            buf.append("\n     * @param ");
                            buf.append(toJavaVarNameStyle((String)it3.next()));
                        }
                        else
                            it3.next(); // throw away
                    }
                    if (!getGTKType2Lang( gtkfunc.getReturnType() ).equals("void")){
                        buf.append("\n     * @return ");
                    }
                    buf.append("\n     */");
                    buf.append("\n    ");
                    buf.append(toJavaVarNameStyle(func));
                    buf.append("(");
                    ListIterator it2 = gtkfunc.getParameters().listIterator();
                    while( it2.hasNext() ) {
                        String next = getGTKType2Lang((String)it2.next());
                        if (!(next.equals("handle")
                              || next.equals("Handle"))){
                            buf.append( getGTKType2Lang( next ) );
                            buf.append( " " );
                            buf.append( (String)it2.next() );
                            if ( it2.hasNext() ) {
                                buf.append( ", " );
                            }
                        }
                        else
                            it2.next(); // throw away
                    }
                    buf.append(") {");
										
                    buf.append("\n      ");
                    if (!getGTKType2Lang( gtkfunc.getReturnType() ).equals("void"))
                        buf.append("return ");
                    buf.append( func );
                    buf.append( "(" );
                    it2 = gtkfunc.getParameters().listIterator();
                    while( it2.hasNext() ) {
                        String next = getGTKType2Lang((String)it2.next());
                        if (next.equals("handle")
                            || next.equals("Handle")){
                            buf.append("GetHandle()");
                            it2.next(); // throw away
                        }
                        else 
                            buf.append( toJavaVarNameStyle((String)it2.next() ));
                        if ( it2.hasNext() ) {
                            buf.append( ", " );
                        }
                    }
                    buf.append( ");" );
                    buf.append("\n    }\n");
                }
            }
            return buf.toString();
        }
        private String toJavaVarNameStyle(String src){
            StringBuffer s = new StringBuffer(src);
            int i = s.indexOf("_");
            while (i >= 0){
                s.deleteCharAt(i);
                s.setCharAt(i, Character.toUpperCase(s.charAt(i)));
                i = s.indexOf("_");
            }
            return s.toString();			
        }
        /**
         * Generate a default C JNI function for each missing GTK function.
         */
        public String toMissingAsJNIString( String outputpackage ) {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            addJFileImplemented( buf );
            String func;
            // For each function.
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                // If it is not yet implemented.
                if ( !hasLanguageFunction( func ) ) {
                    // Add the standard JNI function stuff.
                    GTKFunction gtkfunc = 
                        (GTKFunction)hfunctionshash.get( func );
                    buf.append( "\n" );
                    buf.append( "JNIEXPORT " );
                    buf.append( getGTKType2JNI( gtkfunc.getReturnType() ) );
                    buf.append( " JNICALL Java_" );
                    buf.append( outputpackage.replace( '.', '_' ) );
                    buf.append( "_" );
                    buf.append( getLanguageClass( jfilenames[ 0 ] ) );
                    buf.append( "_" );
                    buf.append( func.replaceAll( "_", "_1" ) );
                    buf.append( "\n(JNIEnv *env, jclass cls" );
                    // Add the function parameters.
                    ListIterator it2 = gtkfunc.getParameters().listIterator();
                    while( it2.hasNext() ) {
                        buf.append( ", " );
                        buf.append( getGTKType2JNI( (String)it2.next() ) );
                        buf.append( " " );
                        buf.append( (String)it2.next() );
                    }
                    buf.append( ")" );
                    buf.append( "\n{\n" );
                    // Add the standard casting of each JNI function parameter
                    // into the GTK equivalent.
                    it2 = gtkfunc.getParameters().listIterator();
                    StringBuffer post = new StringBuffer();
                    while( it2.hasNext() ) {
                        String gtype = (String)it2.next();
                        String gname = (String)it2.next();
                        buf.append( "    " );
                        buf.append( gtype );
                        buf.append( " " );
                        buf.append( gname ).append( "_g" );
                        buf.append( " = " );
                        buf.append( "(" ).append( gtype ).append( ")" );
                        if ( getGTKType2Lang( gtype ).equals( "Handle" ) ) {
                            buf.append( "getPointerFromHandle(env, " );
                            buf.append( gname );
                            buf.append( ")" );
                        } else if ( getGTKType2Lang( gtype ).
                                    equals( "String" ) ) {
                            buf.append( "(*env)->GetStringUTFChars(env, " );
                            buf.append( gname );
                            buf.append( ", 0)" );
                            // Remeber to release the string later.
                            post.append( "    " );
                            post.append( "(*env)->ReleaseStringUTFChars(env, " );
                            post.append( gname );
                            post.append( ", " );
                            post.append( gname ).append( "_g" );
                            post.append( ");\n" );
                        } else if ( getGTKType2Lang( gtype ).
                                    equals( "int[]" ) ) {
                            buf.append( "(*env)->GetIntArrayElements(env, " );
                            buf.append( gname );
                            buf.append( ", NULL)" );
                            // Remember to release the array later.
                            post.append( "    " );
                            post.append( "(*env)->ReleaseIntArrayElements(env, " );
                            post.append( gname );
                            post.append( ", " );
                            post.append( gname ).append( "_g" );
                            post.append( ", 0);\n" );
                        } else {
                            buf.append( gname );
                        }
                        buf.append( ";\n" );
                    }
                    // Add the GTK function call.
                    buf.append( "    " );
                    boolean retspecial = false;
                    // If there is a return value add the return statement.
                    if ( !gtkfunc.getReturnType().equals( "void" ) ) {
                        // If we have stuff to do after the GTK function call,
                        // Save the return value and add the return statement
                        // with the other post-GTK-call elements.
                        if ( post.length() > 0 ) {
                            buf.append( getGTKType2JNI( gtkfunc.
                                                        getReturnType() ) );
                            buf.append( " ret_g = " );
                            post.append( "    " );
                            post.append( "return ret_g;\n" );
                        } else {
                            buf.append( "return " );
                        }
                        if ( getGTKType2Lang( gtkfunc.getReturnType() ).
                             equals( "Handle" ) ) {
                            // Must convert pointers to Handles.
                            buf.append( "getHandleFromPointer(env, " );
                            retspecial = true;
                        } else if ( getGTKType2Lang( gtkfunc.getReturnType() ).
                                    equals( "String" ) ) {
                            // Must convert char *s to Strings.
                            buf.append( "(*env)->NewStringUTF(env, " );
                            retspecial = true;
                        } else {
                            // Otherwise just cast.
                            buf.append( "(" );
                            buf.append( getGTKType2JNI( gtkfunc.
                                                        getReturnType() ) );
                            buf.append( ")" );
                        }
                    }
                    buf.append( gtkfunc.getFunctionName() );
                    buf.append( "(" );
                    it2 = gtkfunc.getParameters().listIterator();
                    while( it2.hasNext() ) {
                        String gtype = (String)it2.next();
                        String gname = (String)it2.next();
                        buf.append( gname ).append( "_g" );
                        if ( it2.hasNext() ) {
                            buf.append( ", " );
                        }
                    }                    
                    buf.append( ")" );
                    // If GTK function was called within another (for Handles
                    // or Strings), close the function call.
                    if ( retspecial ) {
                        buf.append( ")" );
                    }
                    buf.append( ";\n" );
                    buf.append( post );
                    buf.append( "}" );
                    buf.append( "\n" );
                }
            }
            return buf.toString();
        }
        public String toMissingFilesString() {
            StringBuffer buf = new StringBuffer();
            for ( int i = 0; i < jfilenames.length; i++ ) {
                if ( !getLanguageImplemented( jfilenames[ i ] ) ) {
                    buf.append( hfilename );
                    addJFileImplemented( buf );
                }
            }
            return buf.toString();
        }
        public String toOnlyMissingString() {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            addJFileImplemented( buf );
            //            if ( !jimplemented ) {
            //                return buf.toString();
            //            }
            String func;
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                if ( !hasLanguageFunction( func ) ) {
                    buf.append( "\n" );
                    buf.append( "  " );
                    buf.append( func );
                    buf.append( " [" );
                    buf.append( "NOT FOUND" );
                    buf.append( "]" );
                }
            }
            return buf.toString();
        }
        public String toValidatedString() {
            StringBuffer buf = new StringBuffer();
            String func;
            buf.append( hfilename );
            addJFileImplemented( buf );
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                buf.append( "\n" );
                buf.append( "  " );
                buf.append( func );
                buf.append( " [" );
                if ( hasLanguageFunction( func ) ) {
                    buf.append( "ok" );
                } else {
                    buf.append( "NOT FOUND" );
                }
                buf.append( "]" );
            }
            return buf.toString();
        }
        public String toString() {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            String func;
            for( ListIterator it = hfunctions.listIterator();
                 it.hasNext(); ) {
                func = (String)it.next();
                buf.append( "\n" );
                buf.append( "  " );
                buf.append( func );
            }
            buf.append( "\n" );
            for( int i = 0; i < jfilenames.length; i ++ ) {
                buf.append( "  " ).append( jfilenames[ i ] );
                ArrayList filenames = 
                    (ArrayList)jfunctions.get( jfilenames[ i ] );
                if ( filenames != null ) {
                    for( ListIterator it = filenames.listIterator();
                         it.hasNext(); ) {
                        func = (String)it.next();
                        buf.append( "\n" );
                        buf.append( "    " );
                        buf.append( func );
                    }
                }
            }
            return buf.toString();
        }
        protected String signalToJava( String sig ) {
            //sig.replace
            return null;
        }
        public String toSignalAsJava() {
            StringBuffer buf = new StringBuffer();
            buf.append( hfilename );
            buf.append( "\n" );
            for( ListIterator it = hsignals.listIterator();
                 it.hasNext(); ) {
                String func = (String)it.next();
                if ( func.startsWith( "_" ) )
                    continue;
                GTKFunction gtkfunc = (GTKFunction)hsignalshash.get( func );
                /*
                // SIGNAL_NAME
                buf.append( "    " );
                buf.append( "protected final static String " );
                buf.append( func.toUpperCase() );
                buf.append( " = " );
                buf.append( "\"" ).append( func ).append( "\";" );
                buf.append( "\n" );
                // addListener
                buf.append( "    " );
                buf.append( "void addListener();" );
                buf.append( "\n" );
                // SignalHandler.
                buf.append( "    " );
                buf.append( getGTKType2Lang( gtkfunc.getReturnType() ) );
                buf.append( " " );
                buf.append( gtkfunc.getFunctionName() );
                buf.append( "_SignalHandler" );
                buf.append( "(" );
                ListIterator it2 = gtkfunc.getParameters().listIterator();
                while( it2.hasNext() ) {
                    buf.append( getGTKType2Lang( (String)it2.next() ) );
                    buf.append( " " );
                    buf.append( (String)it2.next() );
                    if ( it2.hasNext() ) {
                        buf.append( ", " );
                    }
                }
                buf.append( ") {}\n" );
                */
                buf.append( "    " );
                buf.append( getGTKType2Lang( gtkfunc.getReturnType() ) );
                buf.append( " " );
                buf.append( gtkfunc.getFunctionName() );
                buf.append( "(" );
                ListIterator it2 = gtkfunc.getParameters().listIterator();
                while( it2.hasNext() ) {
                    buf.append( getGTKType2Lang( (String)it2.next() ) );
                    buf.append( " " );
                    buf.append( (String)it2.next() );
                    if ( it2.hasNext() ) {
                        buf.append( ", " );
                    }
                }
                buf.append( ");\n" );
            }
            return buf.toString();
        }
    }

    /**
     * Used to filter the .h files from the GTK include directory.
     */
    private class GTKHeaderFilter implements FilenameFilter {
        private Pattern regpattern;
        public GTKHeaderFilter( String regex ) {
            regpattern = Pattern.compile( regex );
        }
        public boolean accept( File dir, String name ) {
            return regpattern.matcher( name ).matches();
        }
    }

    private String reduceSpaces( String val ) {
	StringBuffer buf = new StringBuffer();
	StringCharacterIterator sci = new StringCharacterIterator( val );
	while ( sci.current() != CharacterIterator.DONE ) {
            char cur = sci.current();
	    if ( Character.isWhitespace( cur ) ) {
                char next = sci.next();
                if ( !Character.isWhitespace( next ) ) {
                    if ( cur == '\t' ) {
                        buf.append( ' ' );
                    } else {
                        buf.append( cur );
                    }
                }
	    } else {
		buf.append( cur );
                sci.next();
	    }
        }
        return buf.toString();
    }

    private String removeSpaces( String val ) {
	StringBuffer buf = new StringBuffer();
	StringCharacterIterator sci = new StringCharacterIterator( val );
	while ( sci.current() != CharacterIterator.DONE ) {
            char cur = sci.current();
            if ( !Character.isWhitespace( cur ) ) {
		buf.append( cur );
	    }
            sci.next();
        }
        return buf.toString();
    }

    //
    // Private data-structure to map Java types to the corresponding JNI type.
    //
    private static final Hashtable java2jni;
    private static final String[] JAVA_JNI = {
        "Handle", "jobject",
        "int", "jint",
        "int[]", "jintArray",
        "double", "jdouble",
        "float", "jfloat",
        "String[]", "jobjectArray",
        "Handle[]", "jobjectArray",
        "void", "void",
        "boolean", "jboolean",
        "String", "jstring",
        "double", "jdouble",
        "float", "jfloat"
    };
    static {
        java2jni = new Hashtable();
        for( int i = 0; i < JAVA_JNI.length; i += 2 ) {
            java2jni.put( JAVA_JNI[i], JAVA_JNI[i+1] );
        }
    }

    //
    // Private data-structure to map GTK types to the corresponding language
    // binding type.
    //
    // NOTE: This needs to be flushed out a lot!
    //
    private static final Hashtable gtktype2lang;
    private static final String[] GTKTYPE_LANG = {
        "GType", "int",
        "GdkEventKey*", "Handle",
        "GdkPixbuf*", "Handle",
        "GdkPixmap*", "Handle",
        "GdkScreen*", "Handle",
        "GdkGravity", "int",
        "GtkComboBox*", "Handle",
        "GtkCornerType", "int",
        "GtkShadowType", "int",
        "GtkAdjustment*", "Handle",
        "GtkToolItem*", "Handle",
        "GtkWidget*", "Handle",
        "GtkWindow*", "Handle",
        "GtkWindowGroup*", "Handle",
        "GSList*", "Handle[]",
        "GList*", "Handle[]",
        "void", "void",
        "gboolean", "boolean",
        "gint", "int",
        "gint32", "int",
        "gint*", "int[]",
        "guint", "int",
        "guint*", "int[]",
        "guint16", "int",
        "guint32", "int",
        "gchar*", "String",
        "gchar**", "String[]",
        "constgchar*", "String",
        "constchar*", "String",
        "G_CONST_RETURNgchar*", "String",
        "G_CONST_RETURNgchar*G_CONST_RETURN*", "String",
        "G_CONST_RETURNchar*", "String",
        "gdouble", "double",
        "gfloat", "float",
        "constGValue*", "Handle",
        "char*", "String"
    };
    static {
        gtktype2lang = new Hashtable();
        for( int i = 0; i < GTKTYPE_LANG.length; i += 2 ) {
            gtktype2lang.put( GTKTYPE_LANG[i], GTKTYPE_LANG[i+1] );
        }
    }

    //
    // Private data-structure to map a GTK .h filename to the corresponding
    // language binding file.
    //

    private static final Hashtable gtk2lang;
    private static final String[] GTK_LANG = {
        "gtk.h", "Gtk.java",
        "gtkaboutdialog.h", "AboutDialog.java",
        "gtkaccelgroup.h", "AccelGroup.java",
        "gtkaccellabel.h", "AccelLabel.java",
        "gtkaccelmap.h", "AccelMap.java",
        "gtkaccessible.h", "Accessible.java",
        "gtkaction.h", "Action.java",
        "gtkactiongroup.h", "ActionGroup.java",
        "gtkadjustment.h", "Adjustment.java",
        "gtkalias.h", "Alias.java",
        "gtkalignment.h", "Alignment.java",
        "gtkarrow.h", "Arrow.java",
        "gtkaspectframe.h", "AspectFrame.java",
        "gtkbbox.h", "ButtonBox.java",
        "gtkbin.h", "Bin.java",
        "gtkbindings.h", "BindingSet.java",
        "gtkbox.h", "Box.java",
        "gtkbutton.h", "Button.java",
        "gtkcalendar.h", "Calendar.java",
        "gtkcelleditable.h", "CellEditable.java",
        "gtkcelllayout.h", "CellLayoutHelper.java",
        "gtkcellrenderer.h", "CellRenderer.java",
        "gtkcellrenderercombo.h", "CellRendererCombo.java",
        "gtkcellrendererpixbuf.h", "CellRendererPixbuf.java",
        "gtkcellrendererprogress.h", "CellRendererProgress.java",
        "gtkcellrenderertext.h", "CellRendererText.java",
        "gtkcellrenderertoggle.h", "CellRendererToggle.java",
        "gtkcellview.h", "CellView.java",
        "gtkcheckbutton.h", "CheckButton.java",
        "gtkcheckmenuitem.h", "CheckMenuItem.java",
        "gtkclipboard.h", "Clipboard.java",
        "gtkclist.h", "DEPRECATED",
        "gtkcolorbutton.h", "ColorButton.java",
        "gtkcolorsel.h", "ColorSelection.java",
        "gtkcolorseldialog.h", "ColorSelectionDialog.java",
        "gtkcombo.h", "DEPRECATED",
        "gtkcombobox.h", "ComboBox.java",
        "gtkcomboboxentry.h", "ComboBoxEntry.java",
        "gtkcontainer.h", "Container.java",
        "gtkctree.h", "DEPRECATED",
        "gtkcurve.h", "Curve.java",
        "gtkdebug.h", "Debug.java",
        "gtkdialog.h", "Dialog.java",
        "gtkdnd.h", "Widget.java",
        "gtkdrawingarea.h", "DrawingArea.java",
        "gtkeditable.h", "EditableHelper.java",
        "gtkentry.h", "Entry.java",
        "gtkentrycompletion.h", "EntryCompletion.java",
        "gtkentryprivate.h", "IGNORE",
        "gtkenums.h", "Enums.java",
        "gtkeventbox.h", "EventBox.java",
        "gtkexpander.h", "Expander.java",
        "gtkfilechooser.h", "FileChooserHelper.java",
        "gtkfilechooserbutton.h", "FileChooserButton.java",
        "gtkfilechooserdefault.h", "IGNORE",
        "gtkfilechooserdialog.h", "FileChooserDialog.java",
        "gtkfilechooserembed.h", "IGNORE",
        "gtkfilechooserentry.h", "IGNORE",
        "gtkfilechooserprivate.h", "IGNORE",
        "gtkfilechooserutils.h", "IGNORE",
        "gtkfilechooserwidget.h", "FileChooserWidget.java",
        "gtkfilefilter.h", "FileFilter.java",
        "gtkfilesel.h", "FileSelection.java",
        "gtkfilesystem.h", "FileSystem.java",
        "gtkfilesystemmodel.h", "IGNORE",
        "gtkfilesystemunix.h", "FileSystemUnix.java",
        "gtkfilesystemwin32.h", "FileSystemWin32.java",
        "gtkfixed.h", "Fixed.java",
        "gtkfontbutton.h", "FontButton.java",
        "gtkfontsel.h", "FontSelection.java;FontSelectionDialog.java",
        "gtkframe.h", "Frame.java",
        "gtkgamma.h", "GammaCurve.java",
        "gtkgc.h", "GC.java",
        "gtkhandlebox.h", "HandleBox.java",
        "gtkhbbox.h", "HButtonBox.java",
        "gtkhbox.h", "HBox.java",
        "gtkhpaned.h", "HPaned.java",
        "gtkhruler.h", "HRuler.java",
        "gtkhscale.h", "HScale.java",
        "gtkhscrollbar.h", "HScrollBar.java",
        "gtkhseparator.h", "HSeparator.java",
        "gtkhsv.h", "Hsv.java",
        "gtkiconcache.h", "IGNORE",
        "gtkiconfactory.h", "IconFactory.java;IconSize.java;IconSet.java;IconSource.java",
        "gtkicontheme.h", "IconTheme.java;IconInfo.java",
        "gtkiconview.h", "IconView.java",
        "gtkimage.h", "Image.java",
        "gtkimagemenuitem.h", "ImageMenuItem.java",
        "gtkimcontext.h", "IMContext.java",
        "gtkimcontextsimple.h", "IMContextSimple.java",
        "gtkimmodule.h", "IGNORE",
        "gtkimmulticontext.h", "IMMulticontext.java",
        "gtkinputdialog.h", "InputDialog.java",
        "gtkintl.h", "Intl.java",
        "gtkinvisible.h", "Invisible.java",
        "gtkitem.h", "Item.java",
        "gtkitemfactory.h", "DEPRECATED",
        "gtkkeyhash.h", "IGNORE",
        "gtklabel.h", "Label.java",
        "gtklayout.h", "Layout.java",
        "gtklist.h", "DEPRECATED",
        "gtklistitem.h", "DEPRECATED",
        "gtkliststore.h", "ListStore.java",
        "gtkmain.h", "Gtk.java",
        "gtkmarshal.h", "Marshal.java",
        "gtkmarshalers.h", "Marshalers.java",
        "gtkmenu.h", "Menu.java",
        "gtkmenubar.h", "MenuBar.java",
        "gtkmenuitem.h", "MenuItem.java",
        "gtkmenushell.h", "MenuShell.java",
        "gtkmenutoolbutton.h", "MenuToolButton.java",
        "gtkmessagedialog.h", "MessageDialog.java",
        "gtkmisc.h", "Misc.java",
        "gtkmnemonichash.h", "IGNORE",
        "gtkmodules.h", "IGNORE",
        "gtknotebook.h", "Notebook.java",
        "gtkobject.h", "GtkObject.java",
        "gtkoldeditable.h", "DEPRECATED",
        "gtkoptionmenu.h", "DEPRECATED",
        "gtkpaned.h", "Paned.java",
        "gtkpathbar.h", "PathBar.java",
        "gtkpixmap.h", "DEPRECATED",
        "gtkplug.h", "Plug.java",
        "gtkpreview.h", "DEPRECATED",
        "gtkprivate.h", "IGNORE",
        "gtkprogress.h", "DEPRECATED",
        "gtkprogressbar.h", "ProgressBar.java",
        "gtkradioaction.h", "RadioAction.java",
        "gtkradiobutton.h", "RadioButton.java",
        "gtkradiomenuitem.h", "RadioMenuItem.java",
        "gtkradiotoolbutton.h", "RadioToolButton.java",
        "gtkrange.h", "Range.java",
        "gtkrbtree.h", "IGNORE",
        "gtkrc.h", "Rc.java;RcStyle.java",
        "gtkruler.h", "Ruler.java",
        "gtkscale.h", "Scale.java",
        "gtkscrollbar.h", "ScrollBar.java",
        "gtkscrolledwindow.h", "ScrolledWindow.java",
        "gtkselection.h", "SelectionData.java",
        "gtkseparator.h", "Separator.java",
        "gtkseparatormenuitem.h", "SeparatorMenuItem.java",
        "gtkseparatortoolitem.h", "SeparatorToolItem.java",
        "gtksequence.h", "IGNORE",
        "gtksettings.h", "Settings.java",
        "gtksignal.h", "Signal.java",
        "gtksizegroup.h", "SizeGroup.java",
        "gtksocket.h", "Socket.java",
        "gtkspinbutton.h", "SpinButton.java",
        "gtkstatusbar.h", "StatusBar.java",
        "gtkstock.h", "Stock.java",
        "gtkstyle.h", "Style.java",
        "gtktable.h", "Table.java",
        "gtktearoffmenuitem.h", "TearoffMenuItem.java",
        "gtktext.h", "DEPRECATED",
        "gtktextbtree.h", "IGNORE",
        "gtktextbuffer.h", "TextBuffer.java",
        "gtktextchild.h", "TextChildAnchor.java",
        "gtktextchildprivate.h", "IGNORE",
        "gtktextdisplay.h", "TextDisplay.java",
        "gtktextiter.h", "TextIter.java",
        "gtktextiterprivate.h", "IGNORE",
        "gtktextlayout.h", "TextLayout.java",
        "gtktextmark.h", "TextMark.java",
        "gtktextmarkprivate.h", "IGNORE",
        "gtktextsegment.h", "TextSegment.java",
        "gtktexttag.h", "TextTag.java;TextAttributes.java",
        "gtktexttagprivate.h", "IGNORE",
        "gtktexttagtable.h", "TextTagTable.java",
        "gtktexttypes.h", "TextTypes.java",
        "gtktextutil.h", "IGNORE",
        "gtktextview.h", "TextView.java",
        "gtkthemes.h", "Themes.java",
        "gtktipsquery.h", "DEPRECATED",
        "gtktoggleaction.h", "ToggleAction.java",
        "gtktoggleactionprivate.h", "IGNORE",
        "gtktogglebutton.h", "ToggleButton.java",
        "gtktoggletoolbutton.h", "ToggleToolButton.java",
        "gtktoolbar.h", "ToolBar.java",
        "gtktoolbutton.h", "ToolButton.java",
        "gtktoolitem.h", "ToolItem.java",
        "gtktooltips.h", "ToolTips.java",
        "gtktree.h", "DEPRECATED",
        "gtktreedatalist.h", "IGNORE",
        "gtktreednd.h", "TreeDND.java",
        "gtktreeitem.h", "DEPRECATED",
        "gtktreemodel.h", "TreeModel.java;TreePath.java;TreeIter.java",
        "gtktreemodelfilter.h", "TreeModelFilter.java",
        "gtktreemodelsort.h", "TreeModelSort.java",
        "gtktreeprivate.h", "IGNORE",
        "gtktreeselection.h", "TreeSelection.java",
        "gtktreesortable.h", "TreeSortableHelper.java",
        "gtktreestore.h", "TreeStore.java",
        "gtktreeview.h", "TreeView.java",
        "gtktreeviewcolumn.h", "TreeViewColumn.java",
        "gtktypebuiltins.h", "TypeBuiltins.java",
        "gtktypeutils.h", "TypeUtils.java",
        "gtkuimanager.h", "UIManager.java",
        "gtkvbbox.h", "VButtonBox.java",
        "gtkvbox.h", "VBox.java",
        "gtkversion.h", "Version.java",
        "gtkviewport.h", "Viewport.java",
        "gtkvpaned.h", "VPaned.java",
        "gtkvruler.h", "VRuler.java",
        "gtkvscale.h", "VScale.java",
        "gtkvscrollbar.h", "VScrollBar.java",
        "gtkvseparator.h", "VSeparator.java",
        "gtkwidget.h", "Widget.java",
        "gtkwindow-decorate.h", "DecoratedWindow.java",
        "gtkwindow.h", "Window.java",
        "gtkxembed.h", "IGNORE",

        // GDK files.
        "gdk.h", "Gdk.java",
        "gdkalias.h", "IGNORE",
        "gdkcolor.h", "Colormap.java;Color.java",
        "gdkconfig.h", "IGNORE",
        "gdkcursor.h", "Cursor.java",
        "gdkdisplay.h", "Display.java",
        "gdkdisplaymanager.h", "DisplayManager.java",
        "gdkdnd.h", "DragContext.java",
        "gdkdrawable.h", "Drawable.java;Window.java",
        "gdkenumtypes.h", "Enumtypes.java",
        "gdkevents.h", "Event.java",
        "gdkfont.h", "Font.java",
        "gdkgc.h", "GC.java",
        "gdki18n.h", "IGNORE",
        "gdkimage.h", "Image.java",
        "gdkinput.h", "Device.java",
        "gdkinternals.h", "IGNORE",
        "gdkintl.h", "IGNORE",
        "gdkkeys.h", "Keymap.java",
        "gdkkeysyms.h", "KeySymbol.java",
        "gdkmarshalers.h", "Marshalers.java",
        "gdkpango.h", "Pango.java",
        "gdkpixbuf.h", "Pixbuf.java",
        "gdkpixmap.h", "Pixmap.java",
        "gdkpoly-generic.h", "PolyGeneric.java",
        "gdkprivate.h", "IGNORE",
        "gdkproperty.h", "Property.java",
        "gdkregion-generic.h", "IGNORE",
        "gdkregion.h", "Region.java",
        "gdkrgb.h", "RgbCmap.java;Drawable.java",
        "gdkscreen.h", "Screen.java",
        "gdkselection.h", "Selection.java",
        "gdkspawn.h", "Spawn.java",
        "gdktypes.h", "IGNORE",
        "gdkvisual.h", "Visual.java",
        "gdkwindow.h", "Window.java",

        // Pango files.
        "pango-glyph.h", "GlyphString.java",
        "pango-layout.h", "Layout.java",

        // ATK files.
        "atkobject.h", "AtkObject.java",

        // Gnome files.
        "gnome-about.h", "About.java",

        // GConf files.
        "gconf-client.h", "ConfClient.java",

        // Glib.
        "gobject.h", "GObject.java.in",
        "gvalue.h", "Value.java",
        "gboxed.h", "Boxed.java",

        // gtkhtml files.
        "gtkhtml.h", "HTML.java",
        "gtkhtmlcontext.h", "HTMLContext.java",
        "htmldocument.h", "HTMLDocument.java",
        "htmlparser.h", "HTMLParser.java",
        "htmlstream.h", "HtmlStream.java",
        "htmlview.h", "HTMLView.java",
        "dom-document.h", "dom/DomDocument.java",
        "dom-documenttype.h", "dom/DomDocumentType.java",
        "dom-namednodemap.h", "dom/DomNamedNodeMap.java",
        "dom-node.h", "dom/DomNode.java",
        "dom-nodelist.h", "dom/DomNodeList.java",

        // gtkmozembed
        "gtkmozembed.h", "MozEmbed.java"
    };
    static {
        gtk2lang = new Hashtable();
        for( int i = 0; i < GTK_LANG.length; i += 2 ) {
            gtk2lang.put( GTK_LANG[i], GTK_LANG[i+1] );
        }
    }
}
