#!/bin/sh

function usage {
    echo "Usage:  bindingsValidator.sh [-modulename] <regex> <output-type>"
    echo ""
    echo "  [-modulename] - One of {-gtkmozembed,-gconf,-gnome,-pango,-atk,-gdk,-gtk}."
    echo "                  Defaults to -gtk."
    echo "  <regex>       - Regular expression to choose GTK .h file.  "
    echo "                  Example: \".*window.h\""
    echo "  <output-type> - One of {validate,missingfuncs,missingfiles,"
    echo "                          native,nativecall,jni,list,signallist}."
    echo ""
    echo "Example Usages:"
    echo "%> ./bindingsValidator.sh \".*menutoolbutton.h\" native"
    echo ""
    echo "%> ./bindingsValidator.sh -gconf gconf-client.h missingfuncs"
    echo ""
    echo "See the BindingsValidator JavaDoc for more details."
    echo ""
    exit 1;
}

if [ $# -lt 2 ]; then
    usage;
fi

classpath=.

case $1 in 
    -gtkmozembed)
        echo "gtkmozembed"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=/usr/include/mozilla/gtkembedmoz
        langbindings=../../libgtkmozembed-java/src/java/org/gnu/gtkmozembed
        javapackage=org.gnu.gtkmozembed
        hfile=$2
        otype=$3
        ;;
    -gconf)
        echo "gconf"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../gconf/gconf
        langbindings=../../libgconf-java/src/java/org/gnu/gconf
        javapackage=org.gnu.gconf
        hfile=$2
        otype=$3
        ;;
    -gnome)
        echo "gnome"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../libgnomeui/libgnomeui
        langbindings=../../libgnome-java/src/java/org/gnu/gnome
        javapackage=org.gnu.gnome
        hfile=$2
        otype=$3
        ;;
    -pango)
        echo "pango"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../pango/pango
        langbindings=../../libgtk-java/src/java/org/gnu/pango
        javapackage=org.gnu.pango
        hfile=$2
        otype=$3
        ;;
    -atk)
        echo "atk"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../atk/atk
        langbindings=../../libgtk-java/src/java/org/gnu/atk
        javapackage=org.gnu.atk
        hfile=$2
        otype=$3
        ;;
    -gdk)
        echo "gdk"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../gtk+/gdk
        langbindings=../../libgtk-java/src/java/org/gnu/gdk
        javapackage=org.gnu.gdk
        hfile=$2
        otype=$3
        ;;
    -gtk)
        echo "gtk"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../gtk+/gtk
        langbindings=../../libgtk-java/src/java/org/gnu/gtk
        javapackage=org.gnu.gtk
        hfile=$2
        otype=$3
        ;;
    -glib)
        echo "glib"
        if [ $# -ne 3 ]; then usage; fi
        gtkbindings=../../../glib/gobject
        langbindings=../../libgtk-java/src/java/org/gnu/glib
        javapackage=org.gnu.glib
        hfile=$2
        otype=$3
        ;;
    *)
        echo "default";
        if [ $# -gt 2 ]; then usage; fi
        gtkbindings=../../../gtk+/gtk
        langbindings=../../libgtk-java/src/java/org/gnu/gtk
        javapackage=org.gnu.gtk
        hfile=$1
        otype=$2
        ;;
esac

java -classpath $classpath -DbindingsValidator.gtk.dir=$gtkbindings -DbindingsValidator.language.dir=$langbindings -DbindingsValidator.output.package=$javapackage -DbindingsValidator.output.type=$otype -DbindingsValidator.gtk.regex=$hfile BindingsValidator
