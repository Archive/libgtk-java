#!/bin/sh
# creates a src/ symlink dir for convenience

mkdir -pv src/java
cd src/java

ln -s ../../gtk/src/java/org/gnu/gtk/ gtk
ln -s ../../gdk/src/java/org/gnu/gdk/ gdk
ln -s ../../atk/src/java/org/gnu/atk/ atk
ln -s ../../pango/src/java/org/gnu/pango/ pango
ln -s ../../gnome/src/java/org/gnu/gnome/ gnome
ln -s ../../glib/src/java/org/gnu/glib/ glib
ln -s ../../gtkhtml/src/java/org/gnu/gtkhtml/ gtkhtml
ln -s ../../gconf/src/java/org/gnu/gconf/ gconf
ln -s ../../vte/src/java/org/gnu/gnomevte/ vte
ln -s ../../glade/src/java/org/gnu/glade/ glade

cd ../
mkdir -pv c-jni
cd c-jni

ln -s ../../gtk/src/jni/ gtk
ln -s ../../gdk/src/jni/ gdk
ln -s ../../atk/src/jni/ atk
ln -s ../../pango/src/jni/ pango
ln -s ../../gnome/src/jni/ gnome
ln -s ../../glib/src/jni/ glib
ln -s ../../gtkhtml/src/jni/ gtkhtml
ln -s ../../gconf/src/jni/ gconf
ln -s ../../vte/src/jni/ vte
ln -s ../../glade/src/jni/ glade

