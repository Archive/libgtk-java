package frame;

import org.gnu.gtk.Gtk;
import org.gnu.gtk.ShadowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Frame {

    public Frame() {

        org.gnu.gtk.Window window = new org.gnu.gtk.Window(
                org.gnu.gtk.WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.setTitle("Frame Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.setDefaultSize(300, 300);

        org.gnu.gtk.Frame frame = new org.gnu.gtk.Frame("Frame Widget");
        frame.setLabelAlign(1.0);
        frame.setShadow(ShadowType.ETCHED_OUT);
        window.add(frame);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        Frame frame = new Frame();

        Gtk.main();
    }
}
