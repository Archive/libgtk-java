package drawingAreaTest;

import org.gnu.gdk.EventMask;
import org.gnu.gtk.DrawingArea;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ExposeEvent;
import org.gnu.gtk.event.ExposeListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MouseEvent;
import org.gnu.gtk.event.MouseListener;

public class Test {

    DrawingArea area = null;

    Window window;

    Test() {
        window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Drawing Area Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return true;
            }
        });
        area = new DrawingArea();
        area.addEvents(EventMask.BUTTON_PRESS_MASK);
        area.addEvents(EventMask.BUTTON_RELEASE_MASK);
        area.addListener(new MouseListener() {
            public boolean mouseEvent(MouseEvent event) {
                if (event.isOfType(MouseEvent.Type.BUTTON_PRESS))
                    System.out.println("Button Pressed");
                else if (event.isOfType(MouseEvent.Type.BUTTON_RELEASE))
                    System.out.println("Button Released");
                return false;
            }
        });
        window.add(area);
        area.addListener(new ExposeListener() {
            public boolean exposeEvent(ExposeEvent event) {
                System.out.println("Expose event: " + window.getWindow());
                window.getWindow().drawPoint(20, 20);
                return false;
            }
        });
        window.showAll();
        System.out.println("Should be printed before expose event");
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new Test();
        Gtk.main();
    }

}
