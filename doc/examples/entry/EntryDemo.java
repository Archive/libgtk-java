package entry;

import org.gnu.gtk.Entry;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class EntryDemo {

    public static Widget createEntryWidget() {
        HBox b = new HBox(true, 0);
        Entry entry = new Entry();
        entry.setText("Entry widget demo.");
        entry.setVisible(true);
        b.add(entry);
        return b;
    }

    public static void main(String[] args) {
        Gtk.init(args);
        Window w = new Window(WindowType.TOPLEVEL);
        w.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                Gtk.mainQuit();
                return false;
            }
        });
        w.setDefaultSize(200, 30);
        w.setBorderWidth(5);
        w.setTitle("The Java-Gnome team");
        w.add(createEntryWidget());
        w.showAll();
        Gtk.main();
    }
}
