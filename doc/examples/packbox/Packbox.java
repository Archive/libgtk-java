package packbox;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.Label;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * This is an example that demonstrates the Box layout control.
 */
public class Packbox {

    private VBox box1;

    public Packbox(int which) {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.setTitle("Packbox");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        // We create a vertical box (vbox) to pack the horizontal boxes into.
        // This allows us to stack the horizontal boxes filled with buttons one
        // on top of the other in this vbox.
        box1 = new VBox(false, 0);
        window.add(box1);

        // which example to show.
        switch (which) {
        case 1:
            createView1();
            break;

        case 2:
            createView2();
            break;

        case 3:
            createView3();
        }

        /* Create another new hbox.. remember we can use as many as we need! */
        HBox quitbox = new HBox(false, 0);

        /* Our quit button. */
        Button button = new Button("Quit", true);

        /* Setup the signal to terminate the program when the button is clicked */
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });

        /*
         * Pack the button into the quitbox. The last 3 arguments to
         * gtk_box_pack_start are: expand, fill, padding.
         */
        quitbox.packStart(button, true, false, 0);
        /* pack the quitbox into the vbox (box1) */
        box1.packStart(quitbox, false, false, 0);

        window.showAll();
    }

    protected HBox makeBox(boolean homogeneous, int spacing, boolean expand,
            boolean fill, int padding) {

        /*
         * Create a new hbox with the appropriate homogeneous and spacing
         * settings
         */
        HBox box = new HBox(homogeneous, spacing);

        /* Create a series of buttons with the appropriate settings */
        Button button = new Button("gtk_box_pack", true);
        box.packStart(button, expand, fill, padding);

        button = new Button("(box,", true);
        box.packStart(button, expand, fill, padding);

        button = new Button("button,", true);
        box.packStart(button, expand, fill, padding);

        /*
         * Create a button with the label depending on the value of expand.
         */
        if (expand == true) {
            button = new Button("true,", true);
        } else {
            button = new Button("false,", true);
        }
        box.packStart(button, expand, fill, padding);

        /*
         * This is the same as the button creation for "expand" above, but uses
         * the shorthand form.
         */
        button = new Button(fill ? "true," : "false,", true);
        box.packStart(button, expand, fill, padding);

        button = new Button("" + padding, true);
        box.packStart(button, expand, fill, padding);

        return box;
    }

    protected void createView1() {

        /* create a new label. */
        Label label = new Label("gtk_hbox_new (FALSE, 0);");

        /*
         * Align the label to the left side. We'll discuss this function and
         * others in the section on Widget Attributes.
         */
        label.setAlignment(0, 0);

        /*
         * Pack the label into the vertical box (vbox box1). Remember that
         * widgets added to a vbox will be packed one on top of the other in
         * order.
         */
        box1.packStart(label, false, false, 0);

        /*
         * Call our make box function - homogeneous = FALSE, spacing = 0, expand =
         * FALSE, fill = FALSE, padding = 0
         */
        HBox box2 = makeBox(false, 0, false, false, 0);
        box1.packStart(box2, false, false, 0);

        /*
         * Call our make box function - homogeneous = FALSE, spacing = 0, expand =
         * TRUE, fill = FALSE, padding = 0
         */
        box2 = makeBox(false, 0, true, false, 0);
        box1.packStart(box2, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(false, 0, true, true, 0);
        box1.packStart(box2, false, false, 0);

        /* Creates a separator */
        HSeparator separator = new HSeparator();

        /*
         * Pack the separator into the vbox. Remember each of these widgets is
         * being packed into a vbox, so they'll be stacked vertically.
         */
        box1.packStart(separator, false, true, 5);

        /* Create another new label, and show it. */
        label = new Label("gtk_hbox_new (TRUE, 0);");
        label.setAlignment(0, 0);
        box1.packStart(label, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(true, 0, true, false, 0);
        box1.packStart(box2, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(true, 0, true, true, 0);
        box1.packStart(box2, false, false, 0);

        /* Another new separator. */
        separator = new HSeparator();
        /*
         * The last 3 arguments to gtk_box_pack_start are: expand, fill,
         * padding.
         */
        box1.packStart(separator, false, true, 5);
    }

    protected void createView2() {
        /*
         * Create a new label, remember box1 is a vbox as created near the
         * beginning of main()
         */
        Label label = new Label("gtk_hbox_new (FALSE, 10);");
        label.setAlignment(0, 0);
        box1.packStart(label, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        HBox box2 = makeBox(false, 10, true, false, 0);
        box1.packStart(box2, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(false, 10, true, true, 0);
        box1.packStart(box2, false, false, 0);

        HSeparator separator = new HSeparator();
        /*
         * The last 3 arguments to gtk_box_pack_start are: expand, fill,
         * padding.
         */
        box1.packStart(separator, false, true, 5);

        label = new Label("gtk_hbox_new (FALSE, 0);");
        label.setAlignment(0, 0);
        box1.packStart(label, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(false, 0, true, false, 10);
        box1.packStart(box2, false, false, 0);

        /* Args are: homogeneous, spacing, expand, fill, padding */
        box2 = makeBox(false, 0, true, true, 10);
        box1.packStart(box2, false, false, 0);

        separator = new HSeparator();
        /*
         * The last 3 arguments to gtk_box_pack_start are: expand, fill,
         * padding.
         */
        box1.packStart(separator, false, true, 5);
    }

    protected void createView3() {
        /*
         * This demonstrates the ability to use gtk_box_pack_end() to right
         * justify widgets. First, we create a new box as before.
         */
        HBox box2 = makeBox(false, 0, false, false, 0);

        /* Create the label that will be put at the end. */
        Label label = new Label("end");
        /*
         * Pack it using gtk_box_pack_end(), so it is put on the right side of
         * the hbox created in the make_box() call.
         */
        box2.packEnd(label, false, false, 0);

        /* Pack box2 into box1 (the vbox remember ? :) */
        box1.packStart(box2, false, false, 0);

        /* A separator for the bottom. */
        HSeparator separator = new HSeparator();
        /*
         * This explicitly sets the separator to 400 pixels wide by 5 pixels
         * high. This is so the hbox we created will also be 400 pixels wide,
         * and the "end" label will be separated from the other labels in the
         * hbox. Otherwise, all the widgets in the hbox would be packed as close
         * together as possible.
         */
        separator.setMinimumSize(400, 5);
        /*
         * pack the separator into the vbox (box1) created near the start of
         * main()
         */
        box1.packStart(separator, false, true, 5);
    }

    public static void main(String[] args) {

        int which = 0;

        // Initialize GTK
        Gtk.init(args);

        if (args.length != 1) {
            System.err
                    .println("usage: java Packbox num, where num is 1, 2, or 3");
            System.exit(1);
        } else {
            which = new Integer(args[0]).intValue();
        }

        Packbox pb = new Packbox(which);

        Gtk.main();
    }
}
