package uimanager;

import org.gnu.gtk.Action;
import org.gnu.gtk.ActionEntry;
import org.gnu.gtk.ActionGroup;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.Label;
import org.gnu.gtk.RadioAction;
import org.gnu.gtk.RadioActionEntry;
import org.gnu.gtk.ToggleAction;
import org.gnu.gtk.ToggleActionEntry;
import org.gnu.gtk.UIManager;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ActionEntryListener;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.RadioActionEntryListener;
import org.gnu.gtk.event.ToggleActionEntryListener;

/**
 * This is an example that demonstrates various ways to use *Action* classes and
 * the UIManager class.
 */
public class UIManagerExample implements ActionEntryListener,
        ToggleActionEntryListener, RadioActionEntryListener {

    // colors used for colorEntries
    private static final int RED = 0;

    private static final int GREEN = 1;

    private static final int BLUE = 2;

    // shapes used for shapeEntries
    private static final int SQUARE = 0;

    private static final int RECTANGLE = 1;

    private static final int OVAL = 2;

    /*
     * The next four arrays define the actions that can be applied to Menus and
     * ToolBars.
     */

    private ActionEntry[] entries = {
            new ActionEntry("FileMenu", null, "_File"),
            new ActionEntry("PreferencesMenu", null, "_Preferences"),
            new ActionEntry("ColorMenu", null, "_Color"),
            new ActionEntry("ShapeMenu", null, "_Shape"),
            new ActionEntry("HelpMenu", null, "_Help"),
            new ActionEntry("New", GtkStockItem.NEW.getString(), "_New",
                    "<control>N", "Create a new file", this),
            new ActionEntry("Open", GtkStockItem.OPEN.getString(), "_Open",
                    "<control>O", "Open a file", this),
            new ActionEntry("Save", GtkStockItem.SAVE.getString(), "_Save",
                    "<control>S", "Save current file", this),
            new ActionEntry("SaveAs", GtkStockItem.SAVE_AS.getString(),
                    "Save _As", null, "Save to a file", this),
            new ActionEntry("Quit", GtkStockItem.QUIT.getString(), "_Quit",
                    "<control>Q", "Quit", this),
            new ActionEntry("About", null, "_About", "<control>A", "About",
                    this) };

    private ToggleActionEntry[] toggleEntries = { new ToggleActionEntry("Bold",
            GtkStockItem.BOLD.getString(), "_Bold", "<control>B", "Bold", true,
            this), };

    private RadioActionEntry[] colorEntries = {
            new RadioActionEntry("Red", null, "_Red", "<control>R", "Blood",
                    RED),
            new RadioActionEntry("Green", null, "_Green", "<control>G",
                    "Grass", GREEN),
            new RadioActionEntry("Blue", null, "_Blue", "<control>B", "Sky",
                    BLUE) };

    private RadioActionEntry[] shapeEntries = {
            new RadioActionEntry("Square", null, "_Square", "<control>S",
                    "Square", SQUARE),
            new RadioActionEntry("Rectangle", null, "_Rectangle", "<control>R",
                    "Rectangle", RECTANGLE),
            new RadioActionEntry("Oval", null, "_Oval", "<control>O", "Egg",
                    OVAL) };

    /*
     * This string defines the structure of the MenuBar and the ToolBar. This
     * information could also be stored in an external file.
     */
    private String uiInfo = "<ui>" + "  <menubar name='MenuBar'>"
            + "    <menu action='FileMenu'>" + "      <menuitem action='New'/>"
            + "      <menuitem action='Open'/>"
            + "      <menuitem action='Save'/>"
            + "      <menuitem action='SaveAs'/>" + "      <separator/>"
            + "      <menuitem action='Quit'/>" + "    </menu>"
            + "    <menu action='PreferencesMenu'>"
            + "      <menu action='ColorMenu'>"
            + "        <menuitem action='Red'/>"
            + "        <menuitem action='Green'/>"
            + "        <menuitem action='Blue'/>" + "      </menu>"
            + "      <menu action='ShapeMenu'>"
            + "        <menuitem action='Square'/>"
            + "        <menuitem action='Rectangle'/>"
            + "        <menuitem action='Oval'/>" + "      </menu>"
            + "      <menuitem action='Bold'/>" + "    </menu>"
            + "    <menu action='HelpMenu'>"
            + "      <menuitem action='About'/>" + "    </menu>"
            + "  </menubar>" + "  <toolbar name='ToolBar'>"
            + "    <toolitem action='Open'/>" + "    <toolitem action='Quit'/>"
            + "    <toolitem action='Sepq'/>" + "  </toolbar>" + "</ui>";

    UIManagerExample() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setDefaultSize(600, 500);
        window.setTitle("UIManager Example");
        window.setBorderWidth(0);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        // create the action group (The collection of possible actions and
        // related
        // event handling information.
        ActionGroup actions = new ActionGroup("Actions");
        actions.addActions(entries);
        actions.addToggleActions(toggleEntries);
        actions.addRadioActions(colorEntries, RED, this);
        actions.addRadioActions(shapeEntries, OVAL, this);

        // create the ui manager to will be responsible for rendering the menus
        // and
        // toolbars.
        UIManager ui = new UIManager();
        ui.insertActionGroup(actions, 0);
        window.addAccelGroup(ui.getAccelGroup());
        ui.addUIFromString(uiInfo);

        VBox box1 = new VBox(false, 0);
        window.add(box1);

        // add the menu bar from the ui by specifying the path
        box1.packStart(ui.getWidget("/MenuBar"), false, false, 0);

        Label label = new Label("UIManager\nExample\nApplication");
        label.setMinimumSize(200, 200);
        label.setAlignment(0.5, 0.5);
        box1.packStart(label, true, true, 0);

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, false, true, 0);

        Button button = new Button("Close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        box2.packStart(button, true, true, 0);

        window.showAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.event.ActionEntryListener#actionEvent(org.gnu.gtk.Action)
     */
    public void actionEvent(Action action) {
        System.out.println("Action " + action.getName() + " activated");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.event.ToggleActionEntryListener#actionEvent(org.gnu.gtk.ToggleAction)
     */
    public void actionEvent(ToggleAction action) {
        System.out.println("Action " + action.getName() + " activated");
        System.out.println("Toggle is " + action.getActive());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gnu.gtk.event.RadioActionEntryListener#actionEvent(org.gnu.gtk.RadioAction)
     */
    public void actionEvent(RadioAction action) {
        System.out.println("Action " + action.getName() + " activated");
        System.out.println("Current value is " + action.getCurrentValue());
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        UIManagerExample example = new UIManagerExample();

        Gtk.main();
    }
}
