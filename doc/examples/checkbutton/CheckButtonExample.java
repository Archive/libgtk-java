package checkbutton;

import org.gnu.gtk.CheckButton;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.ToggleEvent;
import org.gnu.gtk.event.ToggleListener;

public class CheckButtonExample implements LifeCycleListener {
    CheckButton checkButton1;

    CheckButton checkButton2;

    CheckButton checkButton3;

    public CheckButtonExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("CheckButton Example");
        window.addListener(this);
        window.setBorderWidth(20);

        VBox vbox = new VBox(false, 10);
        window.add(vbox);

        checkButton1 = new CheckButton("Disable Button 3", false);
        checkButton1.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                if (checkButton1.getState())
                    checkButton3.setSensitive(false);
                else
                    checkButton3.setSensitive(true);
            }
        });
        vbox.packStart(checkButton1, true, true, 0);

        checkButton2 = new CheckButton("Disable Button 1", false);
        checkButton2.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                if (checkButton2.getState())
                    checkButton1.setSensitive(false);
                else
                    checkButton1.setSensitive(true);
            }
        });
        vbox.packStart(checkButton2, true, true, 0);

        checkButton3 = new CheckButton("Say Hello", false);
        checkButton3.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                if (checkButton3.getState())
                    System.out.println("Hello");
            }
        });
        vbox.packStart(checkButton3, true, true, 0);

        window.showAll();
    }

    public void lifeCycleEvent(LifeCycleEvent event) {
    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                || event.isOfType(LifeCycleEvent.Type.DELETE)) {
            Gtk.mainQuit();
        }
        return false;
    }

    public static void main(String[] args) {
        Gtk.init(args);
        CheckButtonExample cb = new CheckButtonExample();
        Gtk.main();
    }
}
