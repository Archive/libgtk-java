package scale;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HScale;
import org.gnu.gtk.PositionType;
import org.gnu.gtk.Scale;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.UpdateType;
import org.gnu.gtk.VBox;
import org.gnu.gtk.VScale;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.RangeEvent;
import org.gnu.gtk.event.RangeListener;
import org.gnu.gtk.event.ScaleEvent;
import org.gnu.gtk.event.ScaleListener;

public class ScalesExample implements LifeCycleListener {

    protected HScale hscale = null;

    protected VScale vscale = null;

    public ScalesExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Java-GNOME Scales Example");
        window.setBorderWidth(10);

        VBox mainvbox = new VBox(false, 5);

        // create a horizontal scale
        hscale = new HScale(0.0, 10.0, 0.25);
        // Don't display a value with this one.
        hscale.setDrawValue(false);
        // Do something when the value changes
        hscale.addRangeListener(new RangeListener() {
            public void rangeEvent(RangeEvent event) {
                System.out.println(event);
                System.out.println("Value = "
                        + ((Scale) event.getSource()).getValue());
            }
        });
        // Make this update at less fequent times
        hscale.setUpdatePolicy(UpdateType.DELAYED);
        // Have the lowest value at the right.
        hscale.setInverted(true);

        // Show the scale
        hscale.show();
        mainvbox.packStart(hscale);

        // Create a vertical scale
        vscale = new VScale(0.0, 10.0, 0.25);
        // Show 5 decimal places
        vscale.setDigits(5);
        // move the placement of the value label
        // Note that bottom means the place where the value is at it's lowest.
        vscale.setValuePosition(PositionType.LEFT.and(PositionType.BOTTOM));
        // Add a Format listener, so that we can have a custom format for the
        // label
        vscale.setFormatListener(new ScaleListener() {
            public String formatScaleValue(ScaleEvent event, double value) {
                return ">>>" + value + "<<<";
            }

        });
        vscale.show();
        mainvbox.packStart(vscale);

        // Quit this application.
        Button button = new Button(GtkStockItem.CLOSE);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        mainvbox.packEnd(button);
        button.show();

        mainvbox.show();
        window.add(mainvbox);
        window.show();
    }

    // Method to cause the application to exit
    public void lifeCycleEvent(LifeCycleEvent event) {
    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY))
            Gtk.mainQuit();
        return false;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        ScalesExample example = new ScalesExample();

        Gtk.main();
    }
}
