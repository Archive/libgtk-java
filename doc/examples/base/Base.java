package base;

import org.gnu.gdk.EventKey;
import org.gnu.glib.GObject;
import org.gnu.glib.PropertyNotificationListener;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.KeySnoopMethod;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Base {

    public Base() {
        Gtk.setKeySnoopMethod(new KeySnoop());

        Window window = new Window(WindowType.TOPLEVEL);
        window.addListener(new Life());
        window.show();

        Button butt = new Button("Test");
        PropListen listen = new PropListen();
        butt.addListener(listen);
        butt.setStringProperty("label", "Hello World!");

        window.add(butt);
        window.showAll();
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new Base();
        Gtk.main();
    }

    protected class PropListen implements PropertyNotificationListener {
        public void notify(GObject obj, String property) {
            System.out.println("notified of change to: " + property);
        }
    }

    protected class KeySnoop implements KeySnoopMethod {
        public boolean keyEvent(Widget widget, EventKey event) {
            // if ( event.getModifierKey().and( ModifierType.RELEASE_MASK
            // ).equals( ModifierType.RELEASE_MASK ) ) {
            // } else {
            // System.out.println( "keyEvent: #" + event.getState() );
            // }
            System.out.println("keyEvent: " + event.getString());
            if (event.getString().equals("q")) {
                Gtk.removeKeySnoopMethod();
            }
            return false;
        }
    }

    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {
        }

        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                    || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                Gtk.mainQuit();
            }
            return false;
        }
    }
}
