package draganddrop;

import org.gnu.gtk.*;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.DragTargetAdapter;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.ReceiveDragDataEvent;

/**
 * An example of using drag and drop in applications
 */
public class DnDExample3 extends DragTargetAdapter {

    final private Window win;

    private Label info;

    /**
     * Create the main window
     */
    private DnDExample3() {

        // Gtk.init();
        win = new Window(WindowType.TOPLEVEL);
        win.setBorderWidth(25);
        win.setTitle("Java-Gnome Drag and Drop Example");
        win.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        VBox vb = new VBox(false, 25);
        win.add(vb);

        info = new Label(
                "Drag things (e.g. files from nautilus) into the frame below");
        info.setLineWrap(true);
        vb.packStart(info, false, false, 0);

        // Frame ddFrame = new Frame("Drag and Drop frame");
        // ddFrame.setShadow( ShadowType.OUT );
        Entry ddFrame = new Entry();
        vb.packStart(ddFrame, true, true, 0);

        /*
         * Set the frame up so that it may receive drag and drop events of the
         * specified types from any application. The final integers of
         * TargetEntry are used only in this application - They exist as it is
         * faster to compare integers than strings when handling drop events.
         */
        ddFrame.addListener((DragTargetAdapter) this);
        ddFrame
                .setDragDestination(DestDefaults.ALL,
                        new TargetEntry[] {
                                new TargetEntry("text/uri-list",
                                        TargetFlags.NO_RESTRICTION, 1),
                                new TargetEntry("text/x-moz-url",
                                        TargetFlags.NO_RESTRICTION, 2),
                                new TargetEntry("UTF8_STRING",
                                        TargetFlags.NO_RESTRICTION, 3),
                                new TargetEntry("STRING",
                                        TargetFlags.NO_RESTRICTION, 4),
                                new TargetEntry("application/x-color",
                                        TargetFlags.NO_RESTRICTION, 5),
                                new TargetEntry("property/bgimage",
                                        TargetFlags.NO_RESTRICTION, 6),

                        }, org.gnu.gdk.DragAction.COPY);

        Button close = new Button(GtkStockItem.CLOSE);
        close.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.hide();
                    Gtk.mainQuit();
                }
            }
        });
        ButtonBox bb = new HButtonBox();
        bb.setLayout(ButtonBoxStyle.END);
        bb.add(close);
        vb.packStart(bb, false, false, 0);

        win.showAll();
    }

    /**
     * Called when items are dragged into the frame implements the
     * DragDestinationListener interface.
     */
    public void dataReceived(ReceiveDragDataEvent event) {
        System.out.println("Drag Data Received");
        System.out.println("Target info: " + event.getTargetInfo());
        System.out.println("length: " + event.getSelectionData().getLength());
        System.out.println(event.getSelectionData().getText());
        System.out.println(event.getSelectionData().getUris()[0]);
    }

    /**
     * Initialise gtk, create the application, start the main graphics loop
     */
    public static void main(String[] args) {
        Gtk.init(args);
        DnDExample3 instance = new DnDExample3();
        Gtk.main();
    }

}
