package memorymgt;

import java.util.Date;

import org.gnu.gdk.Point;
import org.gnu.gdk.Rectangle;
import org.gnu.glib.GObject;
import org.gnu.glib.Value;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Label;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class MemoryMgt {
    VBox dynamicbox;

    public MemoryMgt() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.addListener(new Life());
        window.show();

        VBox box = new VBox(true, 5);
        HBox fixedbtns = new HBox(true, 5);
        Button btn = new Button("Add");
        btn.addListener(new AddListener());
        fixedbtns.packStart(btn);
        btn = new Button("Remove");
        btn.addListener(new RemoveListener());
        fixedbtns.packStart(btn);
        btn = new Button("Collect");
        btn.addListener(new CollectListener());
        fixedbtns.packStart(btn);

        dynamicbox = new VBox(true, 5);

        box.packStart(dynamicbox);
        box.packStart(fixedbtns);

        window.add(box);
        window.showAll();
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new MemoryMgt();
        Gtk.main();
    }

    protected class AddListener implements ButtonListener {
        public void buttonEvent(ButtonEvent event) {
            if (event.isOfType(ButtonEvent.Type.CLICK)) {
                System.out.println("MemoryMgt:Add - Started.");
                System.out.println("MemoryMgt:  creating label...");
                String dt = new Date().toString();
                Label lbl = new Label(dt);
                /*
                 * Error err = new Error(new Quark( dt ), 12, "My error message
                 * now: " + dt ); java.util.List devs =
                 * org.gnu.gdk.Device.getDevices(); for( java.util.ListIterator
                 * it = devs.listIterator(); it.hasNext(); ) {
                 * System.out.println( "MemoryMgt: device: " +
                 * ((org.gnu.gdk.Device)it.next()).getName() ); } Combo cmb =
                 * new Combo(); String[] pops = new String[] { "one", "two",
                 * "three" }; cmb.setPopupdownStrings( pops );
                 * System.out.println( "MemoryMgt: created label. packing..." );
                 * dynamicbox.packStart( cmb );
                 */
                Point pt = new Point(0, 0);
                Rectangle rect = new Rectangle(0, 0, 1, 1);
                org.gnu.pango.Rectangle rect2 = new org.gnu.pango.Rectangle();
                Value val = new Value(Rectangle.getType());
                val.setBoxed(rect);
                dynamicbox.packStart(lbl);
                System.out.println("MemoryMgt:  packed.");
                dynamicbox.showAll();
                System.out.println("MemoryMgt:Add - Done.");
            }
        }
    }

    protected class RemoveListener implements ButtonListener {
        public void buttonEvent(ButtonEvent event) {
            if (event.isOfType(ButtonEvent.Type.CLICK)) {
                System.out.println("MemoryMgt:Remove - Started.");
                Widget[] children = dynamicbox.getChildren();
                if (children.length > 0) {
                    System.out.println("MemoryMgt:  Removing...");
                    dynamicbox.remove(children[children.length - 1]);
                    System.out.println("MemoryMgt:  Removed.");
                }
                System.out.println("MemoryMgt:Remove - Done.");
            }
        }
    }

    protected class CollectListener implements ButtonListener {
        public void buttonEvent(ButtonEvent event) {
            if (event.isOfType(ButtonEvent.Type.CLICK)) {
                System.out.println("MemoryMgt:Collect - Started.");
                System.out.println("MemoryMgt:  GC - Start.");
                System.gc();
                System.out.println("MemoryMgt:  GC - Done. Collecting...");
                GObject.collect();
                System.out.println("MemoryMgt:Collect - Done.");
            }
        }
    }

    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {
        }

        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                    || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                Gtk.mainQuit();
            }
            return false;
        }
    }
}
