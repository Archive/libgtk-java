package application;

import org.gnu.gtk.AccelGroup;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Table;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Application {

    public Application() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(5);
        window.setTitle("Application Window");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.setMinimumSize(200, 200);

        Table table = new Table(1, 4, false);
        window.add(table);

        AccelGroup accelGroup = new AccelGroup();
        window.addAccelGroup(accelGroup);
        // ItemFactory itemFactory = new ItemFactory(MenuBar.getType(),
        // "<main>", accelGroup);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        Application app = new Application();

        Gtk.main();
    }
}
