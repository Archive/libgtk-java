package aspectframe;

import org.gnu.gtk.AspectFrame;
import org.gnu.gtk.DrawingArea;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class AspectFrameExample {

    public static void main(String[] args) {
        Gtk.init(args);

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Aspect Frame");
        window.setBorderWidth(10);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        // Create an AspectFrame and add it to our toplevel window
        AspectFrame aspectFrame = new AspectFrame("2x1", // label
                0.5, // center x
                0.5, // center y
                2, // xsize/yxsize = 2
                false); // ignore childs aspect

        window.add(aspectFrame);
        aspectFrame.show();

        // Now add a child widget to the AspectFrame
        DrawingArea drawing_area = new DrawingArea();

        // Ask for a 200x200 window, but the AspectFrame will give us a 200x100
        // window since we are forcing a 2x1 aspect ratio
        drawing_area.setMinimumSize(200, 200);
        aspectFrame.add(drawing_area);
        drawing_area.show();

        window.show();
        Gtk.main();
    }
}
