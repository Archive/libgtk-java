package text;

import java.io.File;
import java.io.FileReader;

import org.gnu.gtk.AttachOptions;
import org.gnu.gtk.Button;
import org.gnu.gtk.CheckButton;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HButtonBox;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.Table;
import org.gnu.gtk.TextBuffer;
import org.gnu.gtk.TextView;
import org.gnu.gtk.VBox;
import org.gnu.gtk.VScrollBar;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.WrapMode;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.ToggleEvent;
import org.gnu.gtk.event.ToggleListener;

public class Text {
    TextView text = null;

    TextBuffer textBuffer = null;

    CheckButton editable = null;

    CheckButton wordWrap = null;

    VScrollBar vscroll = null;

    public Text() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setMinimumSize(600, 500);
        window.setTitle("Text Widget Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.setBorderWidth(0);

        VBox box1 = new VBox(false, 0);
        window.add(box1);
        box1.show();

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, true, true, 0);
        box2.show();

        Table table = new Table(2, 2, false);
        table.setRowSpacing(0, 2);
        table.setColumnSpacing(0, 2);
        box2.packStart(table, true, true, 0);
        table.show();

        textBuffer = new TextBuffer();
        text = new TextView(textBuffer);
        text.setEditable(true);
        table.attach(text, 0, 1, 0, 1, AttachOptions.EXPAND
                .or(AttachOptions.SHRINK.or(AttachOptions.FILL)),
                AttachOptions.EXPAND.or(AttachOptions.SHRINK
                        .or(AttachOptions.FILL)), 0, 0);
        text.show();
        window.show();
        vscroll = new VScrollBar(text.getVAdjustment());
        table.attach(vscroll, 1, 2, 0, 1, AttachOptions.FILL,
                AttachOptions.EXPAND.or(AttachOptions.SHRINK
                        .or(AttachOptions.FILL)), 0, 0);
        vscroll.show();

        // Load the file Text.java into the text window
        try {
            File infile = new File(".", "text/Text.java"); // Create a file
            // object
            FileReader in = new FileReader(infile); // Create a char stream
            int size = (int) infile.length(); // check file size
            char[] data = new char[size];
            int charsRead = 0;
            while (charsRead < size)
                charsRead += in.read(data, charsRead, size - charsRead);

            // insert the data into the Text widget
            textBuffer.insertText(new String(data));
        } catch (Exception e) {
            System.err.println(e);
        }

        HButtonBox hbbox = new HButtonBox();
        box2.packStart(hbbox, false, false, 0);
        hbbox.show();

        editable = new CheckButton("Editable", false);
        hbbox.packStart(editable, false, false, 0);
        editable.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                toggleEditable();
            }
        });
        editable.setState(true);
        editable.show();

        wordWrap = new CheckButton("Wrap Words", false);
        hbbox.packStart(wordWrap, false, true, 0);
        wordWrap.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                toggleWordWrap();
            }
        });
        wordWrap.setState(false);
        wordWrap.show();

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);
        separator.show();

        VBox box3 = new VBox(false, 10);
        box3.setBorderWidth(10);
        box1.packStart(box3, false, true, 0);
        box3.show();

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        box3.packStart(button, true, true, 0);
        button.show();
    }

    public void toggleEditable() {
        text.setEditable(editable.getState());
    }

    public void toggleWordWrap() {
        if (wordWrap.getState())
            text.setWrapMode(WrapMode.WORD);
        else
            text.setWrapMode(WrapMode.NONE);
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        Text text = new Text();

        Gtk.main();
    }
}
