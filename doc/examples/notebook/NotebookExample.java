package notebook;

import org.gnu.gtk.Button;
import org.gnu.gtk.CheckButton;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Label;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.PositionType;
import org.gnu.gtk.Table;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.NotebookEvent;
import org.gnu.gtk.event.NotebookListener;

public class NotebookExample implements LifeCycleListener {

    protected Notebook notebook = null;

    private int tabPosition = 1;

    private boolean displayTabs = true;

    private boolean displayBorder = false;

    public NotebookExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return true;
            }
        });

        Table table = new Table(3, 6, false);
        window.add(table);

        // Create a new notebook, place the position of the tabs
        notebook = new Notebook();
        notebook.setTabPosition(PositionType.TOP);
        table.attach(notebook, 0, 6, 0, 1);
        notebook.show();

        // Let's append a bunch of pages to the notebook
        for (int i = 0; i < 5; i++) {
            String bufferf = "Append Frame " + (i + 1);
            String bufferl = "Page " + (i + 1);

            Frame frame = new Frame(bufferf);
            frame.setBorderWidth(10);
            frame.setMinimumSize(100, 75);
            frame.show();

            Label lbl = new Label(bufferf);
            frame.add(lbl);
            lbl.show();

            lbl = new Label(bufferl);
            notebook.appendPage(frame, lbl);
        }

        // Now let's add a page to a specific spot
        CheckButton checkButton = new CheckButton("Click me please!", false);
        checkButton.setMinimumSize(100, 75);
        checkButton.show();

        Label label = new Label("Add page");
        notebook.insertPage(checkButton, label, 2);

        // Now finally let's perpend pages to the notebook
        for (int i = 0; i < 5; i++) {
            String bufferf = "Prepend Frame " + (i + 1);
            String bufferl = "PPage " + (i + 1);

            Frame frame = new Frame(bufferf);
            frame.setBorderWidth(10);
            frame.setMinimumSize(100, 75);
            frame.show();

            Label lbl = new Label(bufferf);
            frame.add(lbl);
            lbl.show();

            lbl = new Label(bufferl);
            notebook.prependPage(frame, lbl);
        }

        // Set what page to start at (page 4)
        notebook.setCurrentPage(3);
        notebook.addListener(new NotebookListener() {
            public void notebookEvent(NotebookEvent event) {
                if (event.isOfType(NotebookEvent.Type.SWITCH_PAGE)) {
                    System.out.println("User just selected page "
                            + event.getPageNumber());
                }
            }
        });

        // Quit this application.
        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        table.attach(button, 0, 1, 1, 2);
        button.show();

        // Move the Notebook to the next page.
        button = new Button("next page", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    notebook.nextPage();
                }
            }
        });
        table.attach(button, 1, 2, 1, 2);
        button.show();

        // Move the Notebook to the previous page.
        button = new Button("prev page", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    notebook.prevPage();
                }
            }
        });
        table.attach(button, 2, 3, 1, 2);
        button.show();

        // Change the position of the tabs on this Notebook
        button = new Button("tab position", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    if (tabPosition == 4)
                        tabPosition = 0;
                    notebook.setTabPosition(PositionType.intern(++tabPosition));
                }
            }
        });
        table.attach(button, 3, 4, 1, 2);
        button.show();

        // Display/hode the tabs and border.
        button = new Button("tabs/border on/off", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    displayTabs = !displayTabs;
                    displayBorder = !displayBorder;
                    notebook.setShowTabs(displayTabs);
                    notebook.setShowBorder(displayBorder);
                }
            }
        });
        table.attach(button, 4, 5, 1, 2);
        button.show();

        // Remove the currently selected page.
        button = new Button("remove page", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    int page = notebook.getCurrentPage();
                    notebook.removePage(page);
                }
            }
        });
        table.attach(button, 5, 6, 1, 2);
        button.show();

        table.show();

        window.show();
    }

    // Method to cause the application to exit
    public void lifeCycleEvent(LifeCycleEvent event) {
    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                || event.isOfType(LifeCycleEvent.Type.DELETE)) {
            Gtk.mainQuit();
        }
        return false;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        new NotebookExample();

        Gtk.main();
    }
}
