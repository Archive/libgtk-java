/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package stockitem;

import org.gnu.gtk.Button;
import org.gnu.gtk.Entry;
import org.gnu.gtk.Fixed;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Label;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.PositionType;
import org.gnu.gtk.Table;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * This example uses the Notebook class and the Table class to display the array
 * of GtkStockItems that are available for use. Each GtkStockItem has a
 * corresponding static ID that can be used when you are constructing a button
 * by new Button(GtkStockItem).
 * 
 * Clicking on any of the buttons will display their corresponding ID in the
 * entry widget.
 */
public class StockItemExample {
    protected Notebook notebook = null;

    private int tabPosition = 1;

    private boolean displayTabs = true;

    private boolean displayBorder = false;

    private Entry stockID = new Entry();

    private final short MAX_COLUMNS = 6;

    private final short ICONS_PER_ROW = 5;

    // browsing buttons
    private Button goBack = new Button(GtkStockItem.GO_BACK);

    private Button goForward = new Button(GtkStockItem.GO_FORWARD);

    private Button home = new Button(GtkStockItem.HOME);

    private Button missingImage = new Button(GtkStockItem.MISSING_IMAGE);

    private Button refresh = new Button(GtkStockItem.REFRESH);

    // computer buttons
    private Button cdrom = new Button(GtkStockItem.CDROM);

    private Button connect = new Button(GtkStockItem.CONNECT);

    private Button disconnect = new Button(GtkStockItem.DISCONNECT);

    private Button floppy = new Button(GtkStockItem.FLOPPY);

    private Button harddisk = new Button(GtkStockItem.HARDDISK);

    private Button network = new Button(GtkStockItem.NETWORK);

    // editing buttons
    private Button bold = new Button(GtkStockItem.BOLD);

    private Button clear = new Button(GtkStockItem.CLEAR);

    private Button convert = new Button(GtkStockItem.CONVERT);

    private Button copy = new Button(GtkStockItem.COPY);

    private Button cut = new Button(GtkStockItem.CUT);

    private Button edit = new Button(GtkStockItem.EDIT);

    private Button find = new Button(GtkStockItem.FIND);

    private Button findAndReplace = new Button(GtkStockItem.FIND_AND_REPLACE);

    private Button indent = new Button(GtkStockItem.INDENT);

    private Button italic = new Button(GtkStockItem.ITALIC);

    private Button justifyCenter = new Button(GtkStockItem.JUSTIFY_CENTER);

    private Button justifyFill = new Button(GtkStockItem.JUSTIFY_FILL);

    private Button justifyLeft = new Button(GtkStockItem.JUSTIFY_LEFT);

    private Button justifyRight = new Button(GtkStockItem.JUSTIFY_RIGHT);

    private Button paste = new Button(GtkStockItem.PASTE);

    private Button redo = new Button(GtkStockItem.REDO);

    private Button revertToSaved = new Button(GtkStockItem.REVERT_TO_SAVED);

    private Button save = new Button(GtkStockItem.SAVE);

    private Button saveAs = new Button(GtkStockItem.SAVE_AS);

    private Button selectFont = new Button(GtkStockItem.SELECT_FONT);

    private Button spellCheck = new Button(GtkStockItem.SPELL_CHECK);

    private Button sortAscending = new Button(GtkStockItem.SORT_ASCENDING);

    private Button sortDescending = new Button(GtkStockItem.SORT_DESCENDING);

    private Button strikethrough = new Button(GtkStockItem.STRIKETHROUGH);

    private Button undelete = new Button(GtkStockItem.UNDELETE);

    private Button underline = new Button(GtkStockItem.UNDERLINE);

    private Button unindent = new Button(GtkStockItem.UNINDENT);

    private Button undo = new Button(GtkStockItem.UNDO);

    // files buttons
    private Button add = new Button(GtkStockItem.ADD);

    private Button directory = new Button(GtkStockItem.DIRECTORY);

    private Button execute = new Button(GtkStockItem.EXECUTE);

    private Button help = new Button(GtkStockItem.HELP);

    private Button newButton = new Button(GtkStockItem.NEW);

    private Button open = new Button(GtkStockItem.OPEN);

    private Button preferences = new Button(GtkStockItem.PREFERENCES);

    private Button print = new Button(GtkStockItem.PRINT);

    private Button printPreview = new Button(GtkStockItem.PRINT_PREVIEW);

    private Button properties = new Button(GtkStockItem.PROPERTIES);

    private Button remove = new Button(GtkStockItem.REMOVE);

    // media buttons
    private Button mediaForward = new Button(GtkStockItem.MEDIA_FORWARD);

    private Button mediaNext = new Button(GtkStockItem.MEDIA_NEXT);

    private Button mediaPause = new Button(GtkStockItem.MEDIA_PAUSE);

    private Button mediaPlay = new Button(GtkStockItem.MEDIA_PLAY);

    private Button mediaPrevious = new Button(GtkStockItem.MEDIA_PREVIOUS);

    private Button mediaRecord = new Button(GtkStockItem.MEDIA_RECORD);

    private Button mediaRewind = new Button(GtkStockItem.MEDIA_REWIND);

    private Button mediaStop = new Button(GtkStockItem.MEDIA_STOP);

    // miscellaneous buttons
    private Button about = new Button(GtkStockItem.ABOUT);

    private Button colorPicker = new Button(GtkStockItem.COLOR_PICKER);

    private Button goDown = new Button(GtkStockItem.GO_DOWN);

    private Button goUp = new Button(GtkStockItem.GO_UP);

    private Button goToBottom = new Button(GtkStockItem.GOTO_BOTTOM);

    private Button goToFirst = new Button(GtkStockItem.GOTO_FIRST);

    private Button goToLast = new Button(GtkStockItem.GOTO_LAST);

    private Button goToTop = new Button(GtkStockItem.GOTO_TOP);

    private Button index = new Button(GtkStockItem.INDEX);

    private Button jumpTo = new Button(GtkStockItem.JUMP_TO);

    private Button selectColor = new Button(GtkStockItem.SELECT_COLOR);

    private Button zoom100 = new Button(GtkStockItem.ZOOM_100);

    private Button zoomFit = new Button(GtkStockItem.ZOOM_FIT);

    private Button zoomIn = new Button(GtkStockItem.ZOOM_IN);

    private Button zoomOut = new Button(GtkStockItem.ZOOM_OUT);

    // pop-up buttons
    private Button apply = new Button(GtkStockItem.APPLY);

    private Button cancel = new Button(GtkStockItem.CANCEL);

    private Button close = new Button(GtkStockItem.CLOSE);

    private Button delete = new Button(GtkStockItem.DELETE);

    private Button dialogAuthentication = new Button(
            GtkStockItem.DIALOG_AUTHENTICATION);

    private Button dialogError = new Button(GtkStockItem.DIALOG_ERROR);

    private Button dialogInfo = new Button(GtkStockItem.DIALOG_INFO);

    private Button dialogQuestion = new Button(GtkStockItem.DIALOG_QUESTION);

    private Button dialogWarning = new Button(GtkStockItem.DIALOG_WARNING);

    private Button dnd = new Button(GtkStockItem.DND);

    private Button dndMultiple = new Button(GtkStockItem.DND_MULTIPLE);

    private Button no = new Button(GtkStockItem.NO);

    private Button ok = new Button(GtkStockItem.OK);

    private Button quit = new Button(GtkStockItem.QUIT);

    private Button stop = new Button(GtkStockItem.STOP);

    private Button yes = new Button(GtkStockItem.YES);

    public StockItemExample() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(3);

        Table table = new Table(4, 3, false);
        window.add(table);

        notebook = new Notebook();
        notebook.setTabPosition(PositionType.TOP);
        table.attach(notebook, 0, 3, 0, 1);
        notebook.show();

        Frame frame = new Frame("Browsing");
        frame.setBorderWidth(3);

        VBox vert = new VBox(true, 0);

        HBox contents = new HBox(true, 0);
        contents.packStart(goBack);
        contents.packStart(goForward);
        contents.packStart(home);
        contents.packStart(missingImage);
        contents.packStart(refresh);
        vert.packStart(contents);

        int columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Browsing"));

        frame = new Frame("Computer");
        frame.setBorderWidth(3);
        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(cdrom);
        contents.packStart(connect);
        contents.packStart(disconnect);
        contents.packStart(floppy);
        contents.packStart(harddisk);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(network);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());

        columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = 0; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Computer"));

        frame = new Frame("Editing");
        frame.setBorderWidth(3);

        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(bold);
        contents.packStart(clear);
        contents.packStart(convert);
        contents.packStart(copy);
        contents.packStart(cut);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(edit);
        contents.packStart(find);
        contents.packStart(findAndReplace);
        contents.packStart(indent);
        contents.packStart(italic);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(justifyCenter);
        contents.packStart(justifyFill);
        contents.packStart(justifyLeft);
        contents.packStart(justifyRight);
        contents.packStart(paste);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(redo);
        contents.packStart(revertToSaved);
        contents.packStart(save);
        contents.packStart(saveAs);
        contents.packStart(selectFont);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(spellCheck);
        contents.packStart(sortAscending);
        contents.packStart(sortDescending);
        contents.packStart(strikethrough);
        contents.packStart(undelete);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(underline);
        contents.packStart(unindent);
        contents.packStart(undo);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());
        vert.packStart(contents);

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Editing"));

        frame = new Frame("Files");
        frame.setBorderWidth(3);

        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(add);
        contents.packStart(directory);
        contents.packStart(execute);
        contents.packStart(help);
        contents.packStart(newButton);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(open);
        contents.packStart(preferences);
        contents.packStart(print);
        contents.packStart(printPreview);
        contents.packStart(properties);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(remove);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());

        columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = 0; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Files"));

        frame = new Frame("Media");
        frame.setBorderWidth(3);

        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(mediaForward);
        contents.packStart(mediaNext);
        contents.packStart(mediaPause);
        contents.packStart(mediaPlay);
        contents.packStart(mediaPrevious);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(mediaRecord);
        contents.packStart(mediaRewind);
        contents.packStart(mediaStop);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());

        columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = 0; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Media"));

        frame = new Frame("Miscellaneous");
        frame.setBorderWidth(3);

        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(about);
        contents.packStart(colorPicker);
        contents.packStart(goDown);
        contents.packStart(goUp);
        contents.packStart(goToBottom);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(goToFirst);
        contents.packStart(goToLast);
        contents.packStart(goToTop);
        contents.packStart(index);
        contents.packStart(jumpTo);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(selectColor);
        contents.packStart(zoom100);
        contents.packStart(zoomFit);
        contents.packStart(zoomIn);
        contents.packStart(zoomOut);
        vert.packStart(contents);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());

        columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = 0; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Miscellaneous"));

        frame = new Frame("Pop-Ups");
        frame.setBorderWidth(3);

        vert = new VBox(true, 0);

        contents = new HBox(true, 0);
        contents.packStart(apply);
        contents.packStart(cancel);
        contents.packStart(close);
        contents.packStart(delete);
        contents.packStart(dialogAuthentication);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(dialogError);
        contents.packStart(dialogInfo);
        contents.packStart(dialogQuestion);
        contents.packStart(dialogWarning);
        contents.packStart(dnd);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(dndMultiple);
        contents.packStart(no);
        contents.packStart(ok);
        contents.packStart(quit);
        contents.packStart(stop);
        vert.packStart(contents);

        contents = new HBox(true, 0);
        contents.packStart(yes);

        for (int j = contents.getChildren().length; j < ICONS_PER_ROW; j++)
            contents.packStart(new Fixed());

        columnsLeft = MAX_COLUMNS - vert.getChildren().length;
        for (int i = 0; i < columnsLeft; i++) {
            contents = new HBox(true, 0);
            for (int j = 0; j < ICONS_PER_ROW; j++)
                contents.packStart(new Fixed());
            vert.packStart(contents);
        }

        frame.add(vert);
        frame.showAll();

        notebook.appendPage(frame, new Label("Pop-Ups"));

        addButtonListeners();

        stockID.setAlignment((float) 0.5);
        stockID.setEditable(false);
        stockID.show();
        table.attach(stockID, 0, 3, 1, 2);

        // Move the Notebook to the previous page
        Button button = new Button("Prev page", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    notebook.prevPage();
                }
            }
        });

        // Quit this application.
        button = new Button("Close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        table.attach(button, 0, 1, 2, 3);
        button.show();

        // Move the Notebook to the next page.
        button = new Button("Next Page", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    notebook.nextPage();
                }
            }
        });
        table.attach(button, 1, 2, 2, 3);
        button.show();

        table.show();

        window.show();
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
    }

    private void addButtonListeners() {
        goBack.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GO_BACK");
                }
            }
        });

        goForward.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GO_FORWARD");
                }
            }
        });

        home.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("HOME");
                }
            }
        });

        missingImage.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MISSING_IMAGE");
                }
            }
        });

        refresh.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("REFRESH");
                }
            }
        });

        cdrom.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CDROM");
                }
            }
        });

        connect.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CONNECT");
                }
            }
        });

        disconnect.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DISCONNECT");
                }
            }
        });

        floppy.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("FLOPPY");
                }
            }
        });

        harddisk.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("HARDDISK");
                }
            }
        });

        network.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("NETWORK");
                }
            }
        });

        bold.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("BOLD");
                }
            }
        });

        clear.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CLEAR");
                }
            }
        });

        convert.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CONVERT");
                }
            }
        });

        copy.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("COPY");
                }
            }
        });

        cut.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CUT");
                }
            }
        });

        edit.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("EDIT");
                }
            }
        });

        find.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("FIND");
                }
            }
        });

        findAndReplace.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("FIND_AND_REPLACE");
                }
            }
        });

        indent.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("INDENT");
                }
            }
        });

        italic.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ITALIC");
                }
            }
        });

        justifyCenter.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("JUSTIFY_CENTER");
                }
            }
        });

        justifyFill.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("JUSTIFY_FILL");
                }
            }
        });

        justifyLeft.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("JUSTIFY_LEFT");
                }
            }
        });

        justifyRight.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("JUSTIFY_RIGHT");
                }
            }
        });

        paste.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("PASTE");
                }
            }
        });

        redo.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("REDO");
                }
            }
        });

        revertToSaved.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("REVERT_TO_SAVED");
                }
            }
        });

        save.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SAVE");
                }
            }
        });

        saveAs.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SAVE_AS");
                }
            }
        });

        selectFont.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SELECT_FONT");
                }
            }
        });

        spellCheck.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SPELL_CHECK");
                }
            }
        });

        sortAscending.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SORT_ASCENDING");
                }
            }
        });

        sortDescending.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SORT_DESCENDING");
                }
            }
        });

        strikethrough.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("STRIKETHROUGH");
                }
            }
        });

        undelete.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("UNDELETE");
                }
            }
        });

        underline.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("UNDERLINE");
                }
            }
        });

        unindent.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("UNINDENT");
                }
            }
        });

        undo.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("UNDO");
                }
            }
        });

        add.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ADD");
                }
            }
        });

        directory.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIRECTORY");
                }
            }
        });

        execute.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("EXECUTE");
                }
            }
        });

        help.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("HELP");
                }
            }
        });

        newButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("NEW");
                }
            }
        });

        open.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("OPEN");
                }
            }
        });

        preferences.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("PREFERENCES");
                }
            }
        });

        print.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("PRINT");
                }
            }
        });

        printPreview.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("PRINT_PREVIEW");
                }
            }
        });

        properties.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("PROPERTIES");
                }
            }
        });

        remove.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("REMOVE");
                }
            }
        });

        mediaForward.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_FORWARD");
                }
            }
        });

        mediaNext.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_NEXT");
                }
            }
        });

        mediaPause.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_PAUSE");
                }
            }
        });

        mediaPlay.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_PLAY");
                }
            }
        });

        mediaPrevious.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_PREVIOUS");
                }
            }
        });

        mediaRecord.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_RECORD");
                }
            }
        });

        mediaRewind.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_REWIND");
                }
            }
        });

        mediaStop.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("MEDIA_STOP");
                }
            }
        });

        about.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ABOUT");
                }
            }
        });

        colorPicker.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("COLOR_PICKER");
                }
            }
        });

        goDown.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GO_DOWN");
                }
            }
        });

        goUp.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GO_UP");
                }
            }
        });

        goToBottom.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GOTO_BOTTOM");
                }
            }
        });

        goToFirst.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GOTO_FIRST");
                }
            }
        });

        goToLast.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GOTO_LAST");
                }
            }
        });

        goToTop.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("GOTO_TOP");
                }
            }
        });

        index.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("INDEX");
                }
            }
        });

        jumpTo.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("JUMP_TO");
                }
            }
        });

        selectColor.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("SELECT_COLOR");
                }
            }
        });

        zoom100.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ZOOM_100");
                }
            }
        });

        zoomFit.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ZOOM_FIT");
                }
            }
        });

        zoomIn.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ZOOM_IN");
                }
            }
        });

        zoomOut.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("ZOOM_OUT");
                }
            }
        });

        apply.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("APPLY");
                }
            }
        });

        cancel.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CANCEL");
                }
            }
        });

        close.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("CLOSE");
                }
            }
        });

        delete.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DELETE");
                }
            }
        });

        dialogAuthentication.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIALOG_AUTHENTICATION");
                }
            }
        });

        dialogError.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIALOG_ERROR");
                }
            }
        });

        dialogInfo.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIALOG_INFO");
                }
            }
        });

        dialogQuestion.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIALOG_QUESTION");
                }
            }
        });

        dialogWarning.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DIALOG_WARNING");
                }
            }
        });

        dnd.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DND");
                }
            }
        });

        dndMultiple.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("DND_MULTIPLE");
                }
            }
        });

        no.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("NO");
                }
            }
        });

        ok.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("OK");
                }
            }
        });

        quit.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("QUIT");
                }
            }
        });

        stop.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("STOP");
                }
            }
        });

        yes.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent e) {
                if (e.isOfType(ButtonEvent.Type.CLICK)) {
                    stockID.setText("YES");
                }
            }
        });
    }

    public static void main(String[] args) {
        Gtk.init(args);

        StockItemExample notebook = new StockItemExample();

        Gtk.main();
    }
}
