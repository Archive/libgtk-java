package editor;

import org.gnu.gtk.Gtk;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.TextBuffer;
import org.gnu.gtk.TextTagTable;
import org.gnu.gtk.TextView;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Editor {

    public Editor() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Editor");
        window.setDefaultSize(500, 380);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        String text = "This is a test";
        TextBuffer textBuffer = new TextBuffer(new TextTagTable());
        textBuffer.insertText(text);
        TextView textView = new TextView(textBuffer);

        ScrolledWindow scrolledWindow = new ScrolledWindow(null, null);
        scrolledWindow.add(textView);
        window.add(scrolledWindow);
        window.showAll();
    }

    public static void main(String[] args) {
        Gtk.init(args);
        Editor editor = new Editor();
        Gtk.main();
    }
}
