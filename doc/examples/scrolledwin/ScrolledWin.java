package scrolledwin;

import org.gnu.gtk.AttachOptions;
import org.gnu.gtk.Box;
import org.gnu.gtk.Button;
import org.gnu.gtk.Dialog;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.PolicyType;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.Table;
import org.gnu.gtk.ToggleButton;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class ScrolledWin {

    public ScrolledWin() {

        Dialog window = new Dialog();
        window.setTitle("ScrolledWindow Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.setDefaultSize(300, 300);

        ScrolledWindow scrollWin = new ScrolledWindow(null, null);
        scrollWin.setBorderWidth(10);
        scrollWin.setPolicy(PolicyType.AUTOMATIC, PolicyType.ALWAYS);
        ((Box) (window.getDialogLayout())).packStart(scrollWin, true, true, 0);

        Table table = new Table(10, 10, false);

        table.setRowSpacing(10);
        table.setColumnSpacing(10);
        scrollWin.addWithViewport(table);
        table.show();

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                ToggleButton button = new ToggleButton("button (" + i + "," + j
                        + ")", false);
                table.attach(button, i, i + 1, j, j + 1, AttachOptions.EXPAND
                        .or(AttachOptions.FILL), AttachOptions.EXPAND
                        .or(AttachOptions.FILL), 0, 0);
                button.show();
            }

        Button button = new Button("close");
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        ((Box) window.getActionArea()).packStart(button, false, false, 10);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        ScrolledWin sw = new ScrolledWin();

        Gtk.main();
    }
}
