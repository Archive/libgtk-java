package testgtk;

import org.gnu.gtk.Adjustment;
import org.gnu.gtk.AttachOptions;
import org.gnu.gtk.Button;
import org.gnu.gtk.ButtonBox;
import org.gnu.gtk.ButtonBoxStyle;
import org.gnu.gtk.ButtonsType;
import org.gnu.gtk.CellRenderer;
import org.gnu.gtk.CellRendererText;
import org.gnu.gtk.CellRendererToggle;
import org.gnu.gtk.CheckButton;
import org.gnu.gtk.CheckMenuItem;
import org.gnu.gtk.ColorSelection;
import org.gnu.gtk.ColorSelectionDialog;
import org.gnu.gtk.ComboBox;
import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnBoolean;
import org.gnu.gtk.DataColumnInt;
import org.gnu.gtk.DataColumnString;
import org.gnu.gtk.Dialog;
import org.gnu.gtk.DialogFlags;
import org.gnu.gtk.Entry;
import org.gnu.gtk.FileChooserAction;
import org.gnu.gtk.FileChooserDialog;
import org.gnu.gtk.FileSelection;
import org.gnu.gtk.FontSelectionDialog;
import org.gnu.gtk.Frame;
import org.gnu.gtk.GammaCurve;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HButtonBox;
import org.gnu.gtk.HPaned;
import org.gnu.gtk.HScale;
import org.gnu.gtk.HScrollBar;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.HandleBox;
import org.gnu.gtk.Image;
import org.gnu.gtk.Justification;
import org.gnu.gtk.Label;
import org.gnu.gtk.ListStore;
import org.gnu.gtk.Menu;
import org.gnu.gtk.MenuBar;
import org.gnu.gtk.MenuItem;
import org.gnu.gtk.MessageDialog;
import org.gnu.gtk.MessageType;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.Orientation;
import org.gnu.gtk.PolicyType;
import org.gnu.gtk.RadioButton;
import org.gnu.gtk.ResponseType;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.SelectionMode;
import org.gnu.gtk.SeparatorToolItem;
import org.gnu.gtk.ShadowType;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.Table;
import org.gnu.gtk.ToggleButton;
import org.gnu.gtk.ToolBar;
import org.gnu.gtk.ToolBarStyle;
import org.gnu.gtk.ToolButton;
import org.gnu.gtk.ToolTips;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreePath;
import org.gnu.gtk.TreeSelection;
import org.gnu.gtk.TreeView;
import org.gnu.gtk.TreeViewColumn;
import org.gnu.gtk.UpdateType;
import org.gnu.gtk.VBox;
import org.gnu.gtk.VButtonBox;
import org.gnu.gtk.VPaned;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.ToggleEvent;
import org.gnu.gtk.event.ToggleListener;
import org.gnu.gtk.event.ToolButtonEvent;
import org.gnu.gtk.event.ToolButtonListener;
import org.gnu.gtk.event.TreeSelectionEvent;
import org.gnu.gtk.event.TreeSelectionListener;

/**
 * A transcription of the testgtk.c example in Java.
 * 
 * Not the very best example of good object-oriented Java programming, but well,
 * that's not the aim. :-)
 * 
 * @author Ol. Gutknecht, Aug 1999
 * 
 */

public class TestGTK implements ButtonListener {
    // members
    private Window mainWindow = null;

    private Label label = null;

    private ToolBar toolbar = null;

    private Entry entry = null;

    private CheckButton editable = null;

    private CheckButton visible = null;

    private CheckButton sensitive = null;

    private StatusBar statusBar = null;

    private FileSelection filesel = null;

    private FontSelectionDialog fontsel = null;

    private ColorSelectionDialog colorsel = null;

    private ListStore list = null;

    // For list store example
    private DataColumnBoolean b;

    private DataColumnInt i1;

    private DataColumnString s1;

    private DataColumnString s2;

    // Buttons for the main selection
    private Button buttonBox, buttons, checkButton, colorSelection, dialog,
            entryButton, fileChooser, fileSelection, fontSelection, gammaCurve,
            handleBox, labelButton, listStore, menu, messageDialog, notebook,
            pane, radioButton, rangeControls, reparent, scrolledWindow,
            statusBarButton, toggleButton, toolBar, toolTips;

    // //////////////////////////////////////////////////////
    // Button Box
    // //////////////////////////////////////////////////////
    private Frame createButtonBox(boolean horizontal, String title,
            int spacing, ButtonBoxStyle layout) {
        ButtonBox box;

        Frame frame = new Frame(title);
        if (horizontal) {
            box = new HButtonBox();
        } else {
            box = new VButtonBox();
        }

        box.setBorderWidth(5);
        frame.add(box);

        box.setLayout(layout);
        box.setSpacing(spacing);

        Button button;
        button = new Button("OK", false);
        box.add(button);
        button = new Button("Cancel", false);
        box.add(button);
        button = new Button("Help", false);
        box.add(button);

        return frame;
    }

    public void createButtonBox() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Button Boxes");
        win.setBorderWidth(10);

        VBox mainvbox = new VBox(false, 0);
        win.add(mainvbox);

        Frame frameHoriz = new Frame("Horizontal Button Boxes");
        mainvbox.packStart(frameHoriz, true, true, 10);

        VBox vbox = new VBox(false, 0);
        vbox.setBorderWidth(10);
        frameHoriz.add(vbox);

        vbox.packStart(createButtonBox(true, "Spread", 40,
                ButtonBoxStyle.SPREAD), true, true, 0);
        vbox.packStart(createButtonBox(true, "Edge", 40, ButtonBoxStyle.EDGE),
                true, true, 5);
        vbox.packStart(
                createButtonBox(true, "Start", 40, ButtonBoxStyle.START), true,
                true, 5);
        vbox.packStart(createButtonBox(true, "End", 40, ButtonBoxStyle.END),
                true, true, 5);

        Frame vertFrame = new Frame("Vertical Button Boxes");
        mainvbox.packStart(vertFrame, true, true, 10);

        HBox hbox = new HBox(false, 0);
        hbox.setBorderWidth(10);
        vertFrame.add(hbox);

        hbox.packStart(createButtonBox(false, "Spread", 30,
                ButtonBoxStyle.SPREAD), true, true, 0);
        hbox.packStart(createButtonBox(false, "Edge", 30, ButtonBoxStyle.EDGE),
                true, true, 5);
        hbox.packStart(
                createButtonBox(false, "Start", 30, ButtonBoxStyle.START),
                true, true, 5);
        hbox.packStart(createButtonBox(false, "End", 30, ButtonBoxStyle.END),
                true, true, 5);

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        mainvbox.packStart(button);
        button.show();
        win.showAll();
    }

    // //////////////////////////////////////////////////////
    // Buttons
    // //////////////////////////////////////////////////////
    public void createButtons() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Button");
        win.setBorderWidth(0);

        VBox box1 = new VBox(false, 0);
        win.add(box1);

        Table table = new Table(3, 3, false);
        table.setRowSpacing(5);
        table.setColumnSpacing(5);
        table.setBorderWidth(10);
        box1.packStart(table, true, true, 0);

        Button myButtons[] = new Button[9];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int n = i * 3 + j;
                myButtons[n] = new Button("button" + n, false);
                table.attach(myButtons[n], i, i + 1, j, j + 1);
                // We don't have access to widget flags yet
            }
        }
        for (int n = 0; n < 9; n++) {
            myButtons[n].addListener(new ButtonListener() {
                public void buttonEvent(ButtonEvent event) {
                    if (event.isOfType(ButtonEvent.Type.CLICK)) {
                        ((Button) event.getSource()).hide();
                    }
                }
            });
        }

        HSeparator separator = new HSeparator();
        box1.packStart(separator);

        VBox box2 = new VBox(true, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box2.packStart(button);
        win.showAll();
    }

    // //////////////////////////////////////////////////////
    // Check Buttons
    // //////////////////////////////////////////////////////
    public void createCheckButtons() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("CheckButton");

        VBox box1 = new VBox(false, 0);
        win.add(box1);

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);

        for (int i = 1; i < 5; i++) {
            CheckButton button = new CheckButton("button" + i, false);
            box2.packStart(button);
        }

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);

        VBox box3 = new VBox(false, 10);
        box3.setBorderWidth(10);
        box1.packStart(box3, false, true, 0);

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box3.packStart(button);

        win.showAll();
    }

    // //////////////////////////////////////////////////////
    // Color Selection
    // //////////////////////////////////////////////////////
    public void createColorSelection() {
        colorsel = new ColorSelectionDialog("color selection dialog");

        Button okButton = colorsel.getOKButton();
        okButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    ColorSelection cs = colorsel.getColorSelectionWidget();
                    System.out.println("The color selected has a value of "
                            + cs.getCurrentColor());
                }
            }
        });
        Button cancelButton = colorsel.getCancelButton();
        cancelButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    colorsel.destroy();
                }
            }
        });
        colorsel.show();
    }

    // //////////////////////////////////////////////////////
    // Dialog
    // //////////////////////////////////////////////////////
    public void createDialog() {
        final Dialog win = new Dialog();
        win.setTitle("dialog");
        VBox vbox = win.getDialogLayout();

        label = new Label("This is a label!");
        label.setPadding(10, 10);
        vbox.packStart(label);
        label.show();

        win.addButton("OK", 0);
        win.addButton("Cancel", 1);
        int result = win.run();
        System.out.println("User selected " + (result == 0 ? "OK" : "Cancel"));
        win.destroy();
    }

    // //////////////////////////////////////////////////////
    // Entry
    // //////////////////////////////////////////////////////
    public void createEntry() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("entry");

        VBox box1 = new VBox(false, 0);
        win.add(box1);

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);

        entry = new Entry();
        entry.setText("hello world");
        box2.packStart(entry);

        ComboBox combo = new ComboBox();
        combo.appendText("item0");
        combo.appendText("item1, item1");
        combo.appendText("item2, item2, item2");
        combo.appendText("item3, item3, item3, item3");
        combo.appendText("item4, item4, item4, item4, item4");
        combo.appendText("item5, item5, item5, item5, item5, item5");
        combo.appendText("item6, item6, item6, item6, item6");
        combo.appendText("item7, item7, item7, item7");
        combo.appendText("item8, item8, item8");
        combo.appendText("item9, item9");
        combo.setActive(0);
        box2.packStart(combo);

        editable = new CheckButton("Editable", false);
        box2.packStart(editable, false, true, 0);
        editable.setState(true);
        editable.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                editableToggled();
            }
        });

        visible = new CheckButton("Visible", false);
        box2.packStart(visible, false, true, 0);
        visible.setState(true);
        visible.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                visibleToggled();
            }
        });

        sensitive = new CheckButton("Sensitive", false);
        box2.packStart(sensitive, false, true, 0);
        sensitive.setState(true);
        sensitive.addListener(new ToggleListener() {
            public void toggleEvent(ToggleEvent event) {
                sensitiveToggled();
            }
        });

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);

        VBox box3 = new VBox(false, 10);
        box3.setBorderWidth(10);
        box1.packStart(box3, false, true, 0);

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box3.packStart(button);

        win.showAll();
    }

    //
    // Entry Callbacks
    //
    public void editableToggled() {
        entry.setEditable(editable.getState());
    }

    public void visibleToggled() {
        entry.setVisible(visible.getState());
    }

    public void sensitiveToggled() {
        entry.setSensitive(sensitive.getState());
    }

    // //////////////////////////////////////////////////////
    // File Chooser
    // //////////////////////////////////////////////////////
    public void createFileChooser() {
        final Dialog dia = new Dialog();
        dia.addButton("Close", 0);
        VBox vbox = dia.getDialogLayout();
        ButtonBox box = new VButtonBox();
        box.setLayout(ButtonBoxStyle.SPREAD);
        Button open = new Button("Open", false);
        open.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    FileChooserDialog chooser = new FileChooserDialog("Open",
                            dia, FileChooserAction.ACTION_OPEN);
                    chooser.run();
                    chooser.destroy();
                }
            }
        });
        Button save = new Button("Save", false);
        save.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    FileChooserDialog chooser = new FileChooserDialog("Save",
                            dia, FileChooserAction.ACTION_SAVE);
                    chooser.run();
                    chooser.destroy();
                }
            }
        });
        Button openFolder = new Button("Select Folder", false);
        openFolder.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    FileChooserDialog chooser = new FileChooserDialog(
                            "Select Folder", dia,
                            FileChooserAction.ACTION_SELECT_FOLDER);
                    chooser.run();
                    chooser.destroy();
                }
            }
        });
        Button createFolder = new Button("Create Folder", false);
        createFolder.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    FileChooserDialog chooser = new FileChooserDialog(
                            "Create Folder", dia,
                            FileChooserAction.ACTION_CREATE_FOLDER);
                    chooser.run();
                    chooser.destroy();
                }
            }
        });
        box.packStart(open);
        box.packStart(save);
        box.packStart(openFolder);
        box.packStart(createFolder);
        vbox.packStart(box);

        dia.showAll();
        dia.run();
        dia.destroy();
    }

    // //////////////////////////////////////////////////////
    // File Selection
    // //////////////////////////////////////////////////////
    public void createFileSelection() {
        filesel = new FileSelection("file selection");
        filesel.setFilename("TestGTK.java");

        Button okButton = filesel.getOKButton();
        okButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    fileOkSel();
                }
            }
        });

        Button cancelButton = filesel.getCancelButton();
        cancelButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    filesel.destroy();
                }
            }
        });

        filesel.show();
    }

    public void fileOkSel() {
        System.out.println(filesel.getFilename());
    }

    // //////////////////////////////////////////////////////
    // Font Selection
    // //////////////////////////////////////////////////////
    public void createFontSelection() {
        fontsel = new FontSelectionDialog("font selection");

        Button okButton = fontsel.getOKButton();
        okButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    fontSelectionOk();
                }
            }
        });

        Button cancelButton = fontsel.getCancelButton();
        cancelButton.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    fontsel.destroy();
                }
            }
        });

        fontsel.show();
    }

    public void fontSelectionOk() {
        System.out.println(fontsel.getFontName());
    }

    // //////////////////////////////////////////////////////
    // Gamma Curve
    // //////////////////////////////////////////////////////
    public void createGammaCurve() {
        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("gamma curve");
        win.setDefaultSize(200, 200);

        GammaCurve curve = new GammaCurve();
        win.add(curve);
        curve.show();

        win.show();
    }

    // //////////////////////////////////////////////////////
    // Handle Box
    // //////////////////////////////////////////////////////
    public void createHandleBox() {
        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("handle box");
        win.setBorderWidth(20);

        VBox vbox = new VBox(false, 0);
        win.add(vbox);

        Label aLabel = new Label("Above");
        vbox.add(aLabel);

        HSeparator separator = new HSeparator();
        vbox.add(separator);

        HBox hbox = new HBox(false, 10);
        vbox.add(hbox);

        separator = new HSeparator();
        vbox.add(separator);
        separator.show();

        aLabel = new Label("Below");
        vbox.add(aLabel);

        HandleBox aHandleBox = new HandleBox();
        hbox.packStart(aHandleBox, false, false, 0);
        aHandleBox.show();

        toolbar = makeToolbar();
        aHandleBox.add(toolbar);

        win.showAll();
    }

    private ToolBar makeToolbar() {
        ToolBar tb = new ToolBar();
        tb.setOrientation(Orientation.HORIZONTAL);
        tb.setStyle(ToolBarStyle.TEXT);

        ToolButton toolButton1 = new ToolButton(null, "Horizontal");
        tb.insert(toolButton1, 0);
        toolButton1.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setToolbarHorizontal();
                return true;
            }
        });

        ToolButton toolButton2 = new ToolButton(null, "Vertical");
        tb.insert(toolButton2, 1);
        toolButton2.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setToolbarVertical();
                return true;
            }
        });
        tb.insert(new SeparatorToolItem(), 2);

        ToolButton toolButton3 = new ToolButton(null, "Enable");
        tb.insert(toolButton3, 3);
        toolButton3.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setToolbarEnableTooltips();
                return true;
            }
        });

        ToolButton toolButton4 = new ToolButton(null, "Disable");
        tb.insert(toolButton4, 4);
        toolButton4.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setToolbarDisableTooltips();
                return true;
            }
        });
        tb.insert(new SeparatorToolItem(), 5);

        return tb;
    }

    //
    // Handle Box Events
    //
    public void setToolbarHorizontal() {
        toolbar.setOrientation(Orientation.HORIZONTAL);
    }

    public void setToolbarVertical() {
        toolbar.setOrientation(Orientation.VERTICAL);
    }

    public void setToolbarEnableTooltips() {
        toolbar.setToolTips(true);
    }

    public void setToolbarDisableTooltips() {
        toolbar.setToolTips(false);
    }

    // //////////////////////////////////////////////////////
    // Labels
    // //////////////////////////////////////////////////////
    public void createLabels() {
        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("labels");
        win.setBorderWidth(5);

        VBox vbox1 = new VBox(false, 5);
        HBox hbox = new HBox(false, 5);
        win.add(hbox);
        hbox.packStart(vbox1, false, false, 0);

        Frame frame = new Frame("Normal Label");
        Label aLabel = new Label("This is a Normal Label");
        frame.add(aLabel);
        vbox1.packStart(frame, false, false, 0);

        frame = new Frame("Multi-Line Label");
        aLabel = new Label(
                "This is a Multi-Line Label. \nSecond line\nThird line");
        frame.add(aLabel);
        vbox1.packStart(frame, false, false, 0);

        frame = new Frame("Left Justiied Label");
        aLabel = new Label(
                "This is a Left-Justified\nMulti-line label.\nThird line");
        aLabel.setJustification(Justification.LEFT);
        frame.add(aLabel);
        vbox1.packStart(frame, false, false, 0);

        frame = new Frame("Right Justified Label");
        aLabel = new Label(
                "This is a Right-Justified\nMulti-line label\n\nFourth line");
        aLabel.setJustification(Justification.RIGHT);
        frame.add(aLabel);
        vbox1.packStart(frame, false, false, 0);

        VBox vbox2 = new VBox(false, 5);
        hbox.packStart(vbox2, false, false, 0);

        frame = new Frame("Line wrapped label");
        aLabel = new Label(
                "This is an example of a line-wrapped label.  It should not be taking up the entire             "
                        +
                        // big space to test spacing
                        "width allocated to it, but automatically wraps the words to fit.  "
                        + "The time has come, for all good men, to come to the aid of their party.  "
                        + "The sixth sheik's six sheep's sick.\n"
                        + "     It supports multiple paragraphs correctly, and  correctly  adds "
                        + "many           extra  spaces.  ");
        aLabel.setLineWrap(true);
        frame.add(aLabel);
        vbox2.packStart(frame, false, false, 0);

        frame = new Frame("Filled, wrapped label");
        aLabel = new Label(
                "This is an example of a line-wrapped, filled label.  It should be taking "
                        + "up the entire              width allocated to it.  Here is a sentence to prove "
                        + "my point.  Here is another sentence.  "
                        + "Here comes the sun, do de do de do \n"
                        + "    This is a new paragraph \n"
                        + "    This is another newer, longer, better paragraph.  It is coming to an end.  "
                        + "unfortunately.");
        aLabel.setJustification(Justification.FILL);
        aLabel.setLineWrap(true);
        frame.add(aLabel);
        vbox2.packStart(frame, false, false, 0);

        frame = new Frame("Underlined label");
        aLabel = new Label("This label is underlined!\n"
                + "This one is underlined in quit a funky fashion");
        aLabel.setJustification(Justification.LEFT);
        aLabel
                .setUnderlinePattern("_________________________ _ _________ _ _____ _ __ __  ___ ____ _____");
        frame.add(aLabel);
        vbox2.packStart(frame, false, false, 0);

        win.showAll();
    }

    // //////////////////////////////////////////////////////
    // List Store
    // /////////////////////////////////////////////////////
    public void createListStore() {
        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("list store example");
        win.setBorderWidth(8);
        win.setDefaultSize(280, 250);

        VBox vbox = new VBox(false, 8);
        win.add(vbox);

        Label aLabel = new Label(
                "This is a bug list (note: not based on read data).");
        vbox.packStart(aLabel, false, false, 0);

        ScrolledWindow sw = new ScrolledWindow(null, null);
        sw.setShadowType(ShadowType.ETCHED_IN);
        sw.setPolicy(PolicyType.NEVER, PolicyType.AUTOMATIC);
        vbox.packStart(sw, true, true, 0);

        // Initialize column data blocks
        b = new DataColumnBoolean();
        i1 = new DataColumnInt();
        s1 = new DataColumnString();
        s2 = new DataColumnString();

        list = new ListStore(new DataColumn[] { b, i1, s1, s2 });

        addListStoreRow(list, false, 60482, "Normal",
                "scrollable notebookds are hidden tabs");
        addListStoreRow(list, false, 60620, "Critical",
                "gdk_window_clear_area (gdkwindow-win32.c) is not thread-safe");
        addListStoreRow(list, false, 50214, "Major",
                "Xft support does not clean up correctly");
        addListStoreRow(list, true, 52877, "Major",
                "GtkFileSelection needs a refresh method");
        addListStoreRow(list, false, 56070, "Normal",
                "Can't click button after setting in sensitive");

        TreeView treeView = new TreeView(list);
        TreeSelection selection = treeView.getSelection();
        selection.setMode(SelectionMode.SINGLE);
        selection.addListener(new TreeSelectionListener() {
            public void selectionChangedEvent(TreeSelectionEvent event) {
                TreeSelection mySelection = (TreeSelection) event.getSource();
                TreePath[] selected = mySelection.getSelectedRows();
                for (int index = 0; index < selected.length; index++) {
                    TreeIter anIter = list.getIter(selected[index]);
                    boolean fixed = list.getValue(anIter, b);
                    int bugNum = list.getValue(anIter, i1);
                    String severity = list.getValue(anIter, s1);
                    String description = list.getValue(anIter, s2);
                    System.out.println("Row selected:");
                    System.out.println("    Fixed: " + fixed + " Num: "
                            + bugNum + " Sev: " + severity + " Desc: "
                            + description);
                }
            }
        });

        // add the columns
        TreeViewColumn column = new TreeViewColumn();
        CellRenderer renderer = new CellRendererToggle();
        column.packStart(renderer, true);
        column.addAttributeMapping(renderer,
                CellRendererToggle.Attribute.ACTIVE, b);
        column.setTitle("Fixed?");
        treeView.appendColumn(column);

        column = new TreeViewColumn();
        renderer = new CellRendererText();
        column.packStart(renderer, true);
        column.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                i1);
        column.setTitle("Bug number");
        treeView.appendColumn(column);

        column = new TreeViewColumn();
        renderer = new CellRendererText();
        column.packStart(renderer, true);
        column.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                s1);
        column.setTitle("Severity");
        treeView.appendColumn(column);

        column = new TreeViewColumn();
        renderer = new CellRendererText();
        column.packStart(renderer, true);
        column.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                s2);
        column.setTitle("Description");
        treeView.appendColumn(column);

        sw.add(treeView);

        win.showAll();
    }

    private void addListStoreRow(ListStore store, boolean fixed, int number,
            String severity, String description) {
        TreeIter iter = store.appendRow();
        store.setValue(iter, b, fixed);
        store.setValue(iter, i1, number);
        store.setValue(iter, s1, severity);
        store.setValue(iter, s2, description);
    }

    // //////////////////////////////////////////////////////
    // Menus
    // //////////////////////////////////////////////////////
    public Menu createMenu(int items) {

        Menu menu1, menu2;
        MenuItem menuItem;
        CheckMenuItem checkItem;
        int i, j, k;

        menu1 = new Menu();

        for (i = 1; i <= items; i++) {

            menuItem = new MenuItem("Item" + i, false);

            for (j = 1; j <= items; j++) {

                menu2 = new Menu();
                menuItem.setSubmenu(menu2);

                for (k = 1; k <= items; k++) {

                    checkItem = new CheckMenuItem("CheckItem" + k, false);
                    menu2.append(checkItem);
                    checkItem.show();
                }
            }
            menu1.append(menuItem);
            menuItem.show();
        }

        return menu1;
    }

    public void createMenus() {

        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Menus");
        win.setDefaultSize(200, 100);

        VBox box1 = new VBox(false, 0);
        win.add(box1);

        MenuBar menuBar = new MenuBar();

        MenuItem menuItem1 = new MenuItem("Menu 1", false);
        MenuItem menuItem2 = new MenuItem("Menu 2", false);

        box1.add(menuBar);
        menuBar.append(menuItem1);
        menuBar.append(menuItem2);

        menuItem1.setSubmenu(createMenu(5));
        menuItem2.setSubmenu(createMenu(8));

        HSeparator separator = new HSeparator();
        box1.packStart(separator);

        VBox box2 = new VBox(true, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box2.packStart(button);

        win.showAll();

    }

    // //////////////////////////////////////////////////////
    // Message Dialog
    // //////////////////////////////////////////////////////
    public void createMessageDialog() {
        MessageDialog dia = new MessageDialog(mainWindow, DialogFlags.MODAL,
                MessageType.QUESTION, ButtonsType.YES_NO,
                "Do you like this MessageDialog", false);
        int answer = dia.run();

        System.out.println("User selected "
                + ((answer == ResponseType.YES.getValue()) ? "yes" : "no"));
        dia.destroy();
    }

    // //////////////////////////////////////////////////////
    // Notebook
    // //////////////////////////////////////////////////////
    public void createNotebook() {

        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Notebook");
        win.setBorderWidth(10);
        win.setDefaultSize(200, 200);

        HBox box1 = new HBox(false, 0);
        box1.show();

        Notebook aNotebook = new Notebook();
        aNotebook.show();
        box1.add(aNotebook);

        HBox page1 = new HBox(false, 0);
        page1.show();
        Label label1 = new Label("Page 1");
        label1.show();
        page1.packStart(label1);
        aNotebook.insertPage(page1, new Label("Page 1"), 0);

        HBox page2 = new HBox(false, 0);
        page2.show();
        Label label2 = new Label("Page 2");
        label2.show();
        page2.packStart(label2);
        aNotebook.insertPage(page2, new Label("Page 2"), 1);

        HBox page3 = new HBox(false, 0);
        page3.show();
        Label label3 = new Label("Page 3");
        label3.show();
        page3.packStart(label3);
        aNotebook.insertPage(page3, new Label("Page 3"), 2);

        win.add(box1);
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Panes
    // //////////////////////////////////////////////////////
    public void createPanes() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("panes");

        VPaned vpaned = new VPaned();
        win.add(vpaned);
        vpaned.setBorderWidth(5);
        vpaned.show();

        HPaned hpaned = new HPaned();
        vpaned.add(hpaned);

        Frame frame = new Frame("frame");
        frame.setShadow(ShadowType.IN);
        frame.setMinimumSize(60, 60);
        hpaned.add1(frame);
        frame.show();

        frame = new Frame("frame");
        frame.setShadow(ShadowType.IN);
        frame.setMinimumSize(80, 60);
        hpaned.add2(frame);
        frame.show();

        hpaned.show();

        VBox box = new VBox(false, 10);
        box.setBorderWidth(10);
        vpaned.add2(box);
        box.show();

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box.packStart(button);
        button.show();

        win.show();
    }

    // //////////////////////////////////////////////////////
    // Radio Buttons
    // //////////////////////////////////////////////////////
    public void createRadioButtons() {

        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Radio Buttons");
        win.setBorderWidth(10);

        VBox box1 = new VBox(false, 0);
        box1.show();

        RadioButton button1 = new RadioButton((RadioButton) null, "Button1",
                false);
        box1.packStart(button1);
        button1.show();

        RadioButton button2 = new RadioButton(button1, "Button2", false);
        box1.packStart(button2);
        button2.show();

        RadioButton button3 = new RadioButton(button1, "Button3", false);
        box1.packStart(button3);
        button3.show();

        VBox box2 = new VBox(true, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);
        box2.show();

        HSeparator separator = new HSeparator();
        box2.packStart(separator);
        separator.show();

        Button button4 = new Button("close", false);
        button4.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box2.packStart(button4);
        button4.show();

        win.add(box1);
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Range Control
    // //////////////////////////////////////////////////////
    public void createRangeControls() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Range Controls");
        win.setBorderWidth(0);

        VBox box1 = new VBox(false, 0);
        win.add(box1);
        box1.show();

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, true, true, 0);
        box2.show();

        Adjustment adjustment = new Adjustment(0, 0, 101, (float) 0.1, 1, 1);
        HScale scale = new HScale(adjustment);
        scale.setMinimumSize(150, 30);
        scale.setUpdatePolicy(UpdateType.DELAYED);
        scale.setDigits(1);
        scale.setDrawValue(true);
        box2.packStart(scale, true, true, 0);
        scale.show();

        HScrollBar scrollbar = new HScrollBar(adjustment);
        scrollbar.setUpdatePolicy(UpdateType.CONTINUOUS);
        box2.packStart(scrollbar, true, true, 0);
        scrollbar.show();

        box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, false, true, 0);
        box2.show();

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box2.packStart(button);
        button.show();
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Reparent
    // //////////////////////////////////////////////////////
    public void createReparent() {

        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Reparent");
        win.setBorderWidth(10);
        win.setDefaultSize(200, 125);

        VBox box1 = new VBox(false, 0);
        box1.show();

        HBox box2 = new HBox(false, 0);
        box2.setBorderWidth(10);
        box1.packStart(box2);
        box2.show();

        Frame frame1 = new Frame("Frame 1");
        box2.packStart(frame1);
        frame1.show();

        Frame frame2 = new Frame("Frame 2");
        box2.packStart(frame2);
        frame2.show();

        final VBox box3 = new VBox(false, 0);
        frame1.add(box3);
        box3.show();

        final VBox box4 = new VBox(false, 0);
        frame2.add(box4);
        box4.show();

        Button button1 = new Button("switch", false);
        button1.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    label.reparent(box3);
                }
            }
        });
        button1.show();
        box3.packStart(button1);

        Button button2 = new Button("switch", false);
        button2.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    label.reparent(box4);
                }
            }
        });
        button2.show();
        box4.packStart(button2);

        label = new Label("Hello World");
        label.show();
        box3.packStart(label);

        HSeparator separator = new HSeparator();
        box1.packStart(separator);
        separator.show();

        Button button3 = new Button("close", false);
        button3.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box1.packStart(button3);
        button3.show();

        win.add(box1);
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Scrolled Window
    // //////////////////////////////////////////////////////
    public void createScrolledWindows() {
        final Dialog win = new Dialog();
        win.setTitle("Scrolled Windows");
        win.setDefaultSize(200, 200);

        ScrolledWindow scrolled_window = new ScrolledWindow(null, null);
        scrolled_window.setBorderWidth(10);
        scrolled_window.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        win.getDialogLayout().packStart(scrolled_window);
        scrolled_window.show();

        Table table = new Table(20, 20, true);
        table.setRowSpacing(10);
        table.setColumnSpacing(10);
        scrolled_window.addWithViewport(table);
        table.show();

        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 20; j++) {
                ToggleButton button = new ToggleButton("button (" + i + "," + j
                        + ")", false);
                table.attach(button, i, i + 1, j, j + 1, AttachOptions.EXPAND
                        .or(AttachOptions.FILL), AttachOptions.EXPAND
                        .or(AttachOptions.FILL), 0, 0);
                button.show();
            }
        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        win.getActionArea().packStart(button, false, false, 10);
        button.show();
        win.show();
        win.run();
    }

    // //////////////////////////////////////////////////////
    // Status Bar
    // //////////////////////////////////////////////////////
    public void createStatusBar() {

        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Status Bar");
        win.setBorderWidth(10);
        win.setDefaultSize(200, 100);

        VBox box1 = new VBox(false, 0);
        box1.show();

        HBox box2 = new HBox(false, 0);
        box2.setBorderWidth(10);
        box1.packStart(box2);
        box2.show();

        statusBar = new StatusBar();
        box1.packStart(statusBar);
        statusBar.show();

        Button button1 = new Button("Push", false);
        box2.packStart(button1);
        button1.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    java.text.DateFormat format = java.text.DateFormat
                            .getTimeInstance(java.text.DateFormat.LONG);
                    statusBar.push(1, "status updated at: "
                            + format.format(new java.util.Date()));
                }
            }
        });
        button1.show();

        Button button2 = new Button("Pop", false);
        box2.packStart(button2);
        button2.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    statusBar.pop(1);
                }
            }
        });
        button2.show();

        HSeparator separator = new HSeparator();
        box1.packStart(separator);
        separator.show();

        Button button3 = new Button("close", false);
        button3.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box1.packStart(button3);
        button3.show();

        win.add(box1);
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Toggle Buttons
    // //////////////////////////////////////////////////////
    public void createToggleButtons() {
        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("toggle butttons");
        win.setBorderWidth(0);
        VBox box1 = new VBox(false, 0);

        win.add(box1);
        box1.show();

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);
        box2.show();
        for (int i = 0; i < 4; i++) {
            ToggleButton button = new ToggleButton("button" + i, false);
            box2.packStart(button);
            button.show();
        }

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, false, 0);
        separator.show();

        VBox box3 = new VBox(false, 10);
        box3.setBorderWidth(10);
        box1.packStart(box3, false, false, 0);
        box3.show();

        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        box3.packStart(button);
        button.grabDefault();
        button.show();
        win.show();

    }

    // //////////////////////////////////////////////////////
    // Toolbar
    // //////////////////////////////////////////////////////
    public void createToolbar() {
        Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Toolbar test");
        win.setBorderWidth(0);
        win.realize();

        toolbar = new ToolBar();
        toolbar.setOrientation(Orientation.HORIZONTAL);
        toolbar.setStyle(ToolBarStyle.TEXT);

        Image image = new Image("testgtk/Test.xpm");
        ToolButton toolButton = new ToolButton(image, "Horizontal");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBHorizontal();
                return true;
            }
        });
        toolbar.insert(toolButton, 0);

        toolButton = new ToolButton(image, "Vertical");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBVertical();
                return true;
            }
        });
        toolbar.insert(toolButton, 1);

        SeparatorToolItem sti = new SeparatorToolItem();
        sti.setDrawLine(false);
        toolbar.insert(sti, 2);

        toolButton = new ToolButton(image, "foo");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBIcons();
                return true;
            }
        });
        toolbar.insert(toolButton, 3);

        toolButton = new ToolButton(image, "bar");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBText();
                return true;
            }
        });
        toolbar.insert(toolButton, 4);

        toolButton = new ToolButton(image, "foobar");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBBoth();
                return true;
            }
        });
        toolbar.insert(toolButton, 5);
        sti = new SeparatorToolItem();
        toolbar.insert(sti, 6);

        toolButton = new ToolButton(image, "Enable");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBEnable();
                return true;
            }
        });
        toolbar.insert(toolButton, 7);

        toolButton = new ToolButton(image, "Disable");
        toolButton.addListener(new ToolButtonListener() {
            public boolean toolButtonEvent(ToolButtonEvent evt) {
                setTBDisable();
                return true;
            }
        });
        toolbar.insert(toolButton, 8);
        toolbar.setMinimumSize(550, 50);

        win.add(toolbar);
        win.showAll();
    }

    public Button createToolbarButton(String buttonText, String xpmFilename) {
        VBox box1 = new VBox(false, 0);
        box1.setBorderWidth(2);

        Image image = new Image(xpmFilename);

        Label aLabel = new Label(buttonText);

        box1.packStart(image, false, false, 3);
        box1.packStart(aLabel, false, false, 3);

        Button button = new Button();
        button.add(box1);

        return button;
    }

    public void setTBHorizontal() {
        toolbar.setOrientation(Orientation.HORIZONTAL);
    }

    public void setTBVertical() {
        toolbar.setOrientation(Orientation.VERTICAL);
    }

    public void setTBIcons() {
        System.out.println("foo!");
    }

    public void setTBText() {
        System.out.println("bar!");
    }

    public void setTBBoth() {
        System.out.println("foobar!");
    }

    public void setTBEnable() {
        toolbar.setToolTips(true);
    }

    public void setTBDisable() {
        toolbar.setToolTips(false);
    }

    // //////////////////////////////////////////////////////
    // Tool Tips
    // //////////////////////////////////////////////////////
    public void createToolTips() {

        final Window win = new Window(WindowType.TOPLEVEL);
        win.setTitle("Tool Tips");
        win.setBorderWidth(10);
        win.setDefaultSize(200, 100);

        VBox box1 = new VBox(false, 0);
        box1.show();

        MenuBar menuBar = new MenuBar();
        menuBar.show();

        MenuItem menuItem = new MenuItem("Menu 1", false);
        menuItem.show();

        ToolTips aToolTips = new ToolTips();

        box1.add(menuBar);
        menuBar.append(menuItem);
        aToolTips.setTip(menuItem, "This is a menu tooltip", "Menu");

        HBox box3 = new HBox(false, 0);
        box1.packStart(box3);
        box3.show();

        Button button = new Button("Button", false);
        button.setBorderWidth(2);
        box3.packStart(button);
        aToolTips.setTip(button, "This is a button with a tooltip.", "Button");
        button.show();

        Button button2 = new Button("Button", false);
        button2.setBorderWidth(2);
        box3.packStart(button2);
        aToolTips
                .setTip(
                        button2,
                        "This is a button with an extra long tooltip text. This way we can be sure that wrapping works inside of tooltip text. I should ramble a bit more just to make sure though.",
                        "Button");
        button2.show();

        HSeparator separator = new HSeparator();
        box1.packStart(separator);
        separator.show();

        VBox box2 = new VBox(true, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2);
        box2.show();

        Button button3 = new Button("close", false);
        button3.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    win.destroy();
                }
            }
        });
        aToolTips.setTip(button3, "Use this button to close this window",
                "Button");
        box2.packStart(button3);
        button3.show();

        win.add(box1);
        win.show();

    }

    public void addButtons(VBox box) {
        buttonBox = new Button("Button Box", false);
        buttonBox.addListener(this);
        box.packStart(buttonBox, true, true, 0);
        buttons = new Button("Buttons", false);
        buttons.addListener(this);
        box.packStart(buttons, true, true, 0);
        checkButton = new Button("Check Button", false);
        checkButton.addListener(this);
        box.packStart(checkButton, true, true, 0);
        colorSelection = new Button("Color Selection", false);
        colorSelection.addListener(this);
        box.packStart(colorSelection, true, true, 0);
        dialog = new Button("Dialog", false);
        dialog.addListener(this);
        box.packStart(dialog, true, true, 0);
        entryButton = new Button("Entry", false);
        entryButton.addListener(this);
        box.packStart(entryButton, true, true, 0);
        fileChooser = new Button("File Chooser (new)", false);
        fileChooser.addListener(this);
        box.packStart(fileChooser, true, true, 0);
        fileSelection = new Button("File Selection (old)", false);
        fileSelection.addListener(this);
        box.packStart(fileSelection, true, true, 0);
        fontSelection = new Button("Font Selection", false);
        fontSelection.addListener(this);
        box.packStart(fontSelection, true, true, 0);
        gammaCurve = new Button("Gamma Curve", false);
        gammaCurve.addListener(this);
        box.packStart(gammaCurve, true, true, 0);
        handleBox = new Button("Handle Box", false);
        handleBox.addListener(this);
        box.packStart(handleBox, true, true, 0);
        labelButton = new Button("Label", false);
        labelButton.addListener(this);
        box.packStart(labelButton, true, true, 0);
        listStore = new Button("List Store", false);
        listStore.addListener(this);
        box.packStart(listStore, true, true, 0);
        menu = new Button("Menu", false);
        menu.addListener(this);
        box.packStart(menu, true, true, 0);
        messageDialog = new Button("Message Dialog", false);
        messageDialog.addListener(this);
        box.packStart(messageDialog, true, true, 0);
        notebook = new Button("Notebook", false);
        notebook.addListener(this);
        box.packStart(notebook, true, true, 0);
        pane = new Button("Pane", false);
        pane.addListener(this);
        box.packStart(pane, true, true, 0);
        radioButton = new Button("Radio Button", false);
        radioButton.addListener(this);
        box.packStart(radioButton, true, true, 0);
        rangeControls = new Button("Range Controls", false);
        rangeControls.addListener(this);
        box.packStart(rangeControls, true, true, 0);
        reparent = new Button("Reparent", false);
        reparent.addListener(this);
        box.packStart(reparent, true, true, 0);
        scrolledWindow = new Button("Scrolled Window", false);
        scrolledWindow.addListener(this);
        box.packStart(scrolledWindow, true, true, 0);
        statusBarButton = new Button("Status Bar", false);
        statusBarButton.addListener(this);
        box.packStart(statusBarButton, true, true, 0);
        toggleButton = new Button("Toggle Button", false);
        toggleButton.addListener(this);
        box.packStart(toggleButton, true, true, 0);
        toolBar = new Button("ToolBar", false);
        toolBar.addListener(this);
        box.packStart(toolBar, true, true, 0);
        toolTips = new Button("ToolTips", false);
        toolTips.addListener(this);
        box.packStart(toolTips, true, true, 0);
    }

    // //////////////////////////////////////////////////////
    // Main Window
    // //////////////////////////////////////////////////////
    public void createMainWindow() {

        mainWindow = new Window(WindowType.TOPLEVEL);
        mainWindow.setName("main window");
        mainWindow.setDefaultSize(200, 400);
        mainWindow.setTitle("gtktest");
        mainWindow.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return true;
            }
        });
        VBox box1 = new VBox(false, 0);
        mainWindow.add(box1);
        ScrolledWindow aScrolledWindow = new ScrolledWindow(null, null);
        aScrolledWindow.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        aScrolledWindow.setBorderWidth(10);
        box1.packStart(aScrolledWindow, true, true, 0);
        VBox box2 = new VBox(false, 0); // Constructors
        // without args fail
        box2.setBorderWidth(10);
        aScrolledWindow.addWithViewport(box2);
        addButtons(box2);
        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);
        VBox box3 = new VBox(false, 10);
        box3.setBorderWidth(10);
        box1.packStart(box3, false, true, 0);
        Button button = new Button("close", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        box3.packStart(button, true, true, 0);
        mainWindow.showAll();
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            Button source = (Button) event.getSource();

            if (source == buttonBox)
                createButtonBox();
            else if (source == buttons)
                createButtons();
            else if (source == checkButton)
                createCheckButtons();
            else if (source == colorSelection)
                createColorSelection();
            else if (source == dialog)
                createDialog();
            else if (source == entryButton)
                createEntry();
            else if (source == fileChooser)
                createFileChooser();
            else if (source == fileSelection)
                createFileSelection();
            else if (source == fontSelection)
                createFontSelection();
            else if (source == gammaCurve)
                createGammaCurve();
            else if (source == handleBox)
                createHandleBox();
            else if (source == labelButton)
                createLabels();
            else if (source == listStore)
                createListStore();
            else if (source == menu)
                createMenus();
            else if (source == messageDialog)
                createMessageDialog();
            else if (source == notebook)
                createNotebook();
            else if (source == pane)
                createPanes();
            else if (source == radioButton)
                createRadioButtons();
            else if (source == rangeControls)
                createRangeControls();
            else if (source == reparent)
                createReparent();
            else if (source == scrolledWindow)
                createScrolledWindows();
            else if (source == statusBarButton)
                createStatusBar();
            else if (source == toggleButton)
                createToggleButtons();
            else if (source == toolBar)
                createToolbar();
            else if (source == toolTips)
                createToolTips();
        }
    }

    public TestGTK() {
        createMainWindow();
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new TestGTK();
        Gtk.main();
    }
}
