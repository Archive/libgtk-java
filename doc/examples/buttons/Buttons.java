package buttons;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Image;
import org.gnu.gtk.Label;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Buttons implements ButtonListener {

    public Buttons() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        Button button = new Button();
        button.addListener(this);

        HBox box1 = xpmLabelBox("buttons/info.xpm", "cool button");
        button.add(box1);
        window.add(button);

        window.showAll();
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            System.out.println("Hello again - the button was pressed");
        }
    }

    private HBox xpmLabelBox(String xpmFilename, String labelText) {
        HBox box1 = new HBox(false, 0);
        box1.setBorderWidth(2);

        Image image = new Image(xpmFilename);
        Label label = new Label(labelText);

        box1.packStart(image, false, false, 3);
        box1.packStart(label, false, false, 3);

        return box1;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);
        new Buttons();
        Gtk.main();
    }
}
