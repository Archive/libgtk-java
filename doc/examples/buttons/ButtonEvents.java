package buttons;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.MouseEvent;
import org.gnu.gtk.event.MouseListener;

/**
 * An example class for handing events from the button class.
 */
public class ButtonEvents {

    /**
     * In the constructor for the class, we set up a simple window containing a
     * single button
     */
    public ButtonEvents() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Java-Gnome Button Events Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.show();

        // Create the button
        Button button = new Button("_Click me & watch the terminal window",
                true);

        // Create an instantiaion of a listener abjoect and associate it
        // with the button
        button.addListener(new MyListener());

        button.addListener(new MouseListener() {
            public boolean mouseEvent(MouseEvent event) {
                System.out.println("Event: " + event);
                return true;
            }
        });
        // Add the button to the window
        window.add(button);
        window.showAll();
    }

    /**
     * In the main part of this application, we set up Gtk and call the
     * constructor.
     */
    public static void main(String[] args) {
        Gtk.init(args);
        new ButtonEvents();
        Gtk.main();
    }

    /**
     * This class is for handling all events from the button. It implements
     * ButtonListener.
     */
    public class MyListener implements ButtonListener {
        /**
         * This method is called whenever a button event occurs
         */
        public void buttonEvent(ButtonEvent event) {
            // Print a message
            System.out.println(event);
            // Look for the specific event of click
            if (event.isOfType(ButtonEvent.Type.CLICK)) {
                // call another method to handle this
                click(event);
            }
        }

        // This is not part of the Listener interface.
        public void click(ButtonEvent event) {
            System.out.println("The button was pressed.");
        }
    }
}
