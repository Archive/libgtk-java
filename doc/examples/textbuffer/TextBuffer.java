package textbuffer;

import java.io.FileNotFoundException;

import org.gnu.glib.JGException;
import org.gnu.gtk.*;
import org.gnu.pango.*;
import org.gnu.gdk.*;
import org.gnu.gtk.event.*;

public class TextBuffer extends org.gnu.gtk.Window implements ButtonListener {

    VPaned vpaned;

    TextView view1;

    TextView view2;

    org.gnu.gtk.TextBuffer buffer;

    public TextBuffer() {
        super(org.gnu.gtk.WindowType.TOPLEVEL);

        setDefaultSize(450, 450);
        addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        setTitle("TextView Demo");

        vpaned = new VPaned();
        vpaned.setBorderWidth(5);
        add(vpaned);

        view1 = new TextView();
        buffer = view1.getBuffer();
        view2 = new TextView(buffer);

        ScrolledWindow sw;

        sw = new ScrolledWindow();
        sw.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        vpaned.add1(sw);
        sw.add(view1);

        sw = new ScrolledWindow();
        sw.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        vpaned.add2(sw);
        sw.add(view2);

        createTags();
        insertText();

        attachWidgets(view1);
        attachWidgets(view2);

        showAll();
    }

    private void createTags() {
        /*
         * Create a bunch of tags. Note that it's also possible to create tags
         * with gtk_text_tag_new() then add them to the tag table for the
         * buffer, gtk_text_buffer_create_tag() is just a convenience function.
         * Also note that you don't have to give tags a name; pass NULL for the
         * name to create an anonymous tag.
         * 
         * In any real app, another useful optimization would be to create a
         * GtkTextTagTable in advance, and reuse the same tag table for all the
         * buffers with the same tag set, instead of creating new copies of the
         * same tags for every buffer.
         * 
         * Tags are assigned default priorities in order of addition to the tag
         * table. That is, tags created later that affect the same text property
         * affected by an earlier tag will override the earlier tag. You can
         * modify tag priorities with gtk_text_tag_set_priority().
         */
        TextTag tag;

        tag = buffer.createTag("heading");
        tag.setWeight(Weight.BOLD);
        tag.setSize(15);

        tag = buffer.createTag("italic");
        tag.setStyle(org.gnu.pango.Style.ITALIC);

        tag = buffer.createTag("bold");
        tag.setWeight(Weight.BOLD);

        tag = buffer.createTag("big");
        tag.setSize(20);

        tag = buffer.createTag("xx-small");
        tag.setScale(org.gnu.pango.Scale.XX_SMALL);

        tag = buffer.createTag("x-large");
        tag.setScale(org.gnu.pango.Scale.X_LARGE);

        tag = buffer.createTag("monospace");
        tag.setFamily("monospace");

        tag = buffer.createTag("blue_foreground");
        tag.setForeground("blue");

        tag = buffer.createTag("red_background");
        tag.setBackground("red");

        // #define gray50_width 2
        // #define gray50_height 2
        // static char gray50_bits[] = {
        // 0x02, 0x01
        // };
        //	
        // stipple = gdk_bitmap_create_from_data (NULL,
        // gray50_bits, gray50_width,
        // gray50_height);

        tag = buffer.createTag("background_stipple");
        // "background_stipple", stipple, NULL);

        tag = buffer.createTag("foreground_stipple");
        // "foreground_stipple", stipple, NULL);

        // g_object_unref (stipple);

        tag = buffer.createTag("big_gap_before_line");
        tag.setPixelsAboveLines(30);

        tag = buffer.createTag("big_gap_after_line");
        tag.setPixelsBelowLines(30);

        tag = buffer.createTag("double_spaced_line");
        tag.setPixelsInsideWrap(10);

        tag = buffer.createTag("not_editable");
        tag.setEditable(false);

        tag = buffer.createTag("word_wrap");
        tag.setWrapMode(org.gnu.gtk.WrapMode.WORD);

        tag = buffer.createTag("char_wrap");
        tag.setWrapMode(org.gnu.gtk.WrapMode.CHAR);

        tag = buffer.createTag("no_wrap");
        tag.setWrapMode(org.gnu.gtk.WrapMode.NONE);

        tag = buffer.createTag("center");
        tag.setJustification(Justification.CENTER);

        tag = buffer.createTag("right_justify");
        tag.setJustification(Justification.RIGHT);

        tag = buffer.createTag("wide_margins");
        tag.setLeftMargin(50);
        tag.setRightMargin(50);

        tag = buffer.createTag("strikethrough");
        tag.setStrikethrough(true);

        tag = buffer.createTag("underline");
        tag.setUnderline(Underline.SINGLE);

        tag = buffer.createTag("double_underline");
        tag.setUnderline(Underline.DOUBLE);

        tag = buffer.createTag("superscript");
        tag.setRise(10);
        tag.setSize(8);

        tag = buffer.createTag("subscript");
        tag.setRise(-10);
        tag.setSize(8);

        tag = buffer.createTag("rtl_quote");
        tag.setWrapMode(org.gnu.gtk.WrapMode.WORD);
        tag.setDirection(TextDirection.RTL);
        tag.setIndent(30);
        tag.setLeftMargin(20);
        tag.setRightMargin(20);
    }

    private void insertText() {
        TextIter iter;
        Pixbuf pixbuf = null;
        Pixbuf scaled;
        TextChildAnchor anchor;
        String filename;

        filename = "/usr/share/gtk-2.0/demo/gtk-logo-rgb.gif";
        try {
            pixbuf = new Pixbuf(filename);
            scaled = pixbuf.scale(32, 32, InterpType.BILINEAR);
            pixbuf = scaled;
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (JGException jge) {
            System.out.println(jge);
        }

        /*
         * get start of buffer; each insertion will revalidate the iterator to
         * point to just after the inserted text.
         */
        iter = buffer.getStartIter();

        buffer
                .insertText(
                        iter,
                        "The text widget can display text with all kinds of nifty attributes. It also supports multiple views of the same buffer; this demo is showing the same buffer in two places.\n\n");

        buffer.insertText(iter, "Font styles. ", "heading");

        buffer.insertText(iter, "For example, you can have ");
        buffer.insertText(iter, "italic", "italic");

        buffer.insertText(iter, ", ");
        buffer.insertText(iter, "bold", "bold");
        buffer.insertText(iter, ", or ");
        buffer.insertText(iter, "monospace (typewriter)", "monospace");
        buffer.insertText(iter, ", or ");
        buffer.insertText(iter, "big", "big");
        buffer.insertText(iter, " text. ");
        buffer
                .insertText(
                        iter,
                        "It's best not to hardcode specific text sizes; you can use relative sizes as with CSS, such as ");
        buffer.insertText(iter, "xx-small", "xx-small");
        buffer.insertText(iter, " or ");
        buffer.insertText(iter, "x-large", "x-large");
        buffer
                .insertText(
                        iter,
                        " to ensure that your program properly adapts if the user changes the default font size.\n\n");

        buffer.insertText(iter, "Colors. ", "heading");

        buffer.insertText(iter, "Colors such as ");
        buffer.insertText(iter, "a blue foreground", "blue_foreground");
        buffer.insertText(iter, " or ");
        buffer.insertText(iter, "a red background", "red_background");
        buffer.insertText(iter, " or even ");
        buffer.insertText(iter, "a stippled red background", new String[] {
                "red_background", "background_stipple" });

        buffer.insertText(iter, " or ");
        buffer.insertText(iter,
                "a stippled blue foreground on solid red background",
                new String[] { "blue_foreground", "red_background",
                        "foreground_stipple" });
        buffer.insertText(iter, " (select that to read it) can be used.\n\n");

        buffer.insertText(iter, "Underline, strikethrough, and rise. ",
                "heading");

        buffer.insertText(iter, "Strikethrough", "strikethrough");
        buffer.insertText(iter, ", ");
        buffer.insertText(iter, "underline", "underline");
        buffer.insertText(iter, ", ");
        buffer.insertText(iter, "double underline", "double_underline");
        buffer.insertText(iter, ", ");
        buffer.insertText(iter, "superscript", "superscript");
        buffer.insertText(iter, ", and ");
        buffer.insertText(iter, "subscript", "subscript");
        buffer.insertText(iter, " are all supported.\n\n");

        buffer.insertText(iter, "Images. ", "heading");

        buffer.insertText(iter, "The buffer can have images in it: ");
        if (null != pixbuf) {
            buffer.insertPixbuf(iter, pixbuf);
            buffer.insertPixbuf(iter, pixbuf);
            buffer.insertPixbuf(iter, pixbuf);
        }
        buffer.insertText(iter, " for example.\n\n");

        buffer.insertText(iter, "Spacing. ", "heading");

        buffer.insertText(iter,
                "You can adjust the amount of space before each line.\n");

        buffer.insertText(iter,
                "This line has a whole lot of space before it.\n",
                new String[] { "big_gap_before_line", "wide_margins" });
        buffer
                .insertText(
                        iter,
                        "You can also adjust the amount of space after each line; this line has a whole lot of space after it.\n",
                        new String[] { "big_gap_after_line", "wide_margins" });

        buffer
                .insertText(
                        iter,
                        "You can also adjust the amount of space between wrapped lines; this line has extra space between each wrapped line in the same paragraph. To show off wrapping, some filler text: the quick brown fox jumped over the lazy dog. Blah blah blah blah blah blah blah blah blah.\n",
                        new String[] { "double_spaced_line", "wide_margins" });

        buffer.insertText(iter,
                "Also note that those lines have extra-wide margins.\n\n");

        buffer.insertText(iter, "Editability. ", "heading");

        buffer
                .insertText(
                        iter,
                        "This line is 'locked down' and can't be edited by the user - just try it! You can't delete this line.\n\n",
                        "not_editable");

        buffer.insertText(iter, "Wrapping. ", "heading");

        buffer
                .insertText(
                        iter,
                        "This line (and most of the others in this buffer) is word-wrapped, using the proper Unicode algorithm. Word wrap should work in all scripts and languages that GTK+ supports. Let's make this a long paragraph to demonstrate: blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah\n\n");

        buffer
                .insertText(
                        iter,
                        "This line has character-based wrapping, and can wrap between any two character glyphs. Let's make this a long paragraph to demonstrate: blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah\n\n",
                        "char_wrap");

        buffer
                .insertText(
                        iter,
                        "This line has all wrapping turned off, so it makes the horizontal scrollbar appear.\n\n\n",
                        "no_wrap");

        buffer.insertText(iter, "Justification. ", "heading");

        buffer.insertText(iter, "\nThis line has center justification.\n",
                "center");

        buffer.insertText(iter, "This line has right justification.\n",
                "right_justify");

        buffer
                .insertText(
                        iter,
                        "\nThis line has big wide margins. Text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text.\n",
                        "wide_margins");

        buffer.insertText(iter, "Internationalization. ", "heading");

        buffer
                .insertText(
                        iter,
                        "You can put all sorts of Unicode text in the buffer.\n\nGerman (Deutsch S\303\274d) Gr\303\274\303\237 Gott\nGreek (\316\225\316\273\316\273\316\267\316\275\316\271\316\272\316\254) \316\223\316\265\316\271\316\254 \317\203\316\261\317\202\nHebrew	\327\251\327\234\327\225\327\235\nJapanese (\346\227\245\346\234\254\350\252\236)\n\nThe widget properly handles bidirectional text, word wrapping, DOS/UNIX/Unicode paragraph separators, grapheme boundaries, and so on using the Pango internationalization framework.\n");

        buffer.insertText(iter,
                "Here's a word-wrapped quote in a right-to-left language:\n");
        buffer
                .insertText(
                        iter,
                        "\331\210\331\202\330\257 \330\250\330\257\330\243 \330\253\331\204\330\247\330\253 \331\205\331\206 \330\243\331\203\330\253\330\261 \330\247\331\204\331\205\330\244\330\263\330\263\330\247\330\252 \330\252\331\202\330\257\331\205\330\247 \331\201\331\212 \330\264\330\250\331\203\330\251 \330\247\331\203\330\263\331\212\331\210\331\206 \330\250\330\261\330\247\331\205\330\254\331\207\330\247 \331\203\331\205\331\206\330\270\331\205\330\247\330\252 \331\204\330\247 \330\252\330\263\330\271\331\211 \331\204\331\204\330\261\330\250\330\255\330\214 \330\253\331\205 \330\252\330\255\331\210\331\204\330\252 \331\201\331\212 \330\247\331\204\330\263\331\206\331\210\330\247\330\252 \330\247\331\204\330\256\331\205\330\263 \330\247\331\204\331\205\330\247\330\266\331\212\330\251 \330\245\331\204\331\211 \331\205\330\244\330\263\330\263\330\247\330\252 \331\205\330\247\331\204\331\212\330\251 \331\205\331\206\330\270\331\205\330\251\330\214 \331\210\330\250\330\247\330\252\330\252 \330\254\330\262\330\241\330\247 \331\205\331\206 \330\247\331\204\331\206\330\270\330\247\331\205 \330\247\331\204\331\205\330\247\331\204\331\212 \331\201\331\212 \330\250\331\204\330\257\330\247\331\206\331\207\330\247\330\214 \331\210\331\204\331\203\331\206\331\207\330\247 \330\252\330\252\330\256\330\265\330\265 \331\201\331\212 \330\256\330\257\331\205\330\251 \331\202\330\267\330\247\330\271 \330\247\331\204\331\205\330\264\330\261\331\210\330\271\330\247\330\252 \330\247\331\204\330\265\330\272\331\212\330\261\330\251. \331\210\330\243\330\255\330\257 \330\243\331\203\330\253\330\261 \331\207\330\260\331\207 \330\247\331\204\331\205\330\244\330\263\330\263\330\247\330\252 \331\206\330\254\330\247\330\255\330\247 \331\207\331\210 \302\273\330\250\330\247\331\206\331\203\331\210\330\263\331\210\331\204\302\253 \331\201\331\212 \330\250\331\210\331\204\331\212\331\201\331\212\330\247.\n\n",
                        "rtl_quote");

        buffer.insertText(iter,
                "You can put widgets in the buffer: Here's a button: ");
        anchor = buffer.createChildAnchor(iter);
        buffer.insertText(iter, " and a menu: ");
        anchor = buffer.createChildAnchor(iter);
        buffer.insertText(iter, " and a scale: ");
        anchor = buffer.createChildAnchor(iter);
        buffer.insertText(iter, " and an animation: ");
        anchor = buffer.createChildAnchor(iter);
        buffer.insertText(iter, " finally a text entry: ");
        anchor = buffer.createChildAnchor(iter);
        buffer.insertText(iter, ".\n");

        buffer
                .insertText(
                        iter,
                        "\n\nThis demo doesn't demonstrate all the GtkTextBuffer features; it leaves out, for example: invisible/hidden text (doesn't work in GTK 2, but planned), tab stops, application-drawn areas on the sides of the widget for displaying breakpoints and such...");

        /* Apply word_wrap tag to whole buffer */
        buffer
                .applyTag("word_wrap", buffer.getStartIter(), buffer
                        .getEndIter());

    }

    private void attachWidgets(TextView view) {
        TextIter iter;
        iter = buffer.getStartIter();
        int i = 0;
        while (findAnchor(iter)) {
            TextChildAnchor anchor = iter.getChildAnchor();
            Widget widget;

            if (i == 0) {
                widget = new Button("Click Me");
                ((Button) widget).addListener((ButtonListener) this);
                // add signal
            } else if (i == 1) {
                widget = new ComboBox();
                ((ComboBox) widget).appendText("Option 1");
                ((ComboBox) widget).appendText("Option 2");
                ((ComboBox) widget).appendText("Option 3");
            } else if (i == 2) {
                widget = new HScale(0, 100, 1);
                // gtk_widget_set_size_request (widget, 70, -1);
                widget.setMinimumSize(70, -1);
            } else if (i == 3) {
                widget = new org.gnu.gtk.Image(
                        "/usr/share/gtk-2.0/demo/floppybuddy.gif");
            } else if (i == 4) {
                widget = new Entry();
            } else {
                throw new RuntimeException("This shouldn't happen");
            }
            view.addChild(widget, anchor);
            widget.showAll();
            i++;

        }
    }

    private boolean findAnchor(TextIter iter) {
        while (iter.moveForwardChar()) {
            if (iter.isChildAnchor()) {
                return true;
            }
        }
        return false;
    }

    private org.gnu.gtk.Window easterEggWindow = null;

    private void recursiveAttachView(int depth, TextView view,
            TextChildAnchor anchor) {
        if (depth > 4) {
            return;
        }
        TextView childView = new TextView(view.getBuffer());
        /* Event box is to add a black border around each child view */
        EventBox eb = new EventBox();

        eb.setBackgroundColor(StateType.NORMAL, org.gnu.gdk.Color.BLACK);

        org.gnu.gtk.Alignment align = new org.gnu.gtk.Alignment(0.5, 0.5, 1.0,
                1.0);
        align.setBorderWidth(1);

        eb.add(align);
        align.add(childView);

        view.addChild(eb, anchor);

        recursiveAttachView(depth + 1, childView, anchor);
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            org.gnu.gtk.TextBuffer buffer = new org.gnu.gtk.TextBuffer();
            TextIter iter = buffer.getStartIter();
            buffer
                    .insertText(iter,
                            "This buffer is shared by a set of nested text views.\n Nested view:\n");

            TextChildAnchor anchor = buffer.createChildAnchor(iter);

            buffer.insertText(iter,
                    "\nDon't do this in real applications, please.\n");

            TextView view = new TextView(buffer);

            recursiveAttachView(0, view, anchor);

            easterEggWindow = new org.gnu.gtk.Window(
                    org.gnu.gtk.WindowType.TOPLEVEL);
            ScrolledWindow sw = new ScrolledWindow();
            sw.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);

            easterEggWindow.add(sw);
            sw.add(view);

            easterEggWindow.setDefaultSize(300, 400);

            easterEggWindow.showAll();
        }
    }

    public static void main(String[] args) {
        Gtk.init(args);

        new TextBuffer();

        Gtk.main();
    }
}
