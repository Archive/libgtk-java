package statusbar;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class StatusBarExample implements ButtonListener, LifeCycleListener {
    StatusBar statusbar;

    Button button1, button2;

    int count = 1;

    int contextId = 0;

    public StatusBarExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setMinimumSize(200, 100);
        window.setTitle("StatusBar Example");
        window.addListener(this);

        VBox vbox = new VBox(false, 1);
        window.add(vbox);

        statusbar = new StatusBar();
        vbox.packStart(statusbar, true, true, 0);

        contextId = statusbar.getContextID("Statusbar example");

        button1 = new Button("push item", false);
        button1.addListener((ButtonListener) this);
        vbox.packStart(button1, true, true, 2);

        button2 = new Button("pop last item", false);
        button2.addListener((ButtonListener) this);
        vbox.packStart(button2, true, true, 2);

        window.showAll();
    }

    public void lifeCycleEvent(LifeCycleEvent event) {
    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                || event.isOfType(LifeCycleEvent.Type.DELETE))
            Gtk.mainQuit();
        return false;
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            if (button1 == event.getSource()) {
                String buff = "Item " + count++;
                statusbar.push(contextId, buff);
            } else if (button2 == event.getSource()) {
                statusbar.pop(contextId);
            }
        }
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        StatusBarExample sb = new StatusBarExample();

        Gtk.main();
    }
}
