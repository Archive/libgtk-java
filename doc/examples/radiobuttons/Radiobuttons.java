package radiobuttons;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.RadioButton;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Radiobuttons {

    RadioButton button1, button2, button3;

    public Radiobuttons() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(0);
        window.setTitle("Radiobuttons Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        VBox box1 = new VBox(false, 0);
        window.add(box1);

        VBox box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, true, true, 0);

        button1 = new RadioButton((RadioButton) null, "button1", false);
        button1.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    if (button1.getState()) {
                        System.out.println("Button 1 Selected");
                    } else {
                        System.out.println("Button 1 Unselected");
                    }
                }
            }
        });
        box2.packStart(button1, true, true, 0);

        button2 = new RadioButton(button1, "button2", false);
        button2.setState(true);
        button2.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    if (button2.getState()) {
                        System.out.println("Button 2 Selected");
                    } else {
                        System.out.println("Button 2 Unselected");
                    }
                }
            }
        });
        box2.packStart(button2, true, true, 0);

        button3 = new RadioButton(button1, "button3", false);
        button3.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    if (button3.getState()) {
                        System.out.println("Button 3 Selected");
                    } else {
                        System.out.println("Button 3 Unselected");
                    }

                }
            }
        });
        box2.packStart(button3, true, true, 0);

        HSeparator separator = new HSeparator();
        box1.packStart(separator, false, true, 0);

        box2 = new VBox(false, 10);
        box2.setBorderWidth(10);
        box1.packStart(box2, false, true, 0);

        Button button = new Button("close");
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });
        box2.packStart(button, true, true, 0);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        Radiobuttons rb = new Radiobuttons();

        Gtk.main();
    }
}
