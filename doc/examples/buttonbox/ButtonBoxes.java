package buttonbox;

import org.gnu.gtk.Button;
import org.gnu.gtk.ButtonBox;
import org.gnu.gtk.ButtonBoxStyle;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HButtonBox;
import org.gnu.gtk.VBox;
import org.gnu.gtk.VButtonBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

/**
 * An example application demonstrating the ButtonBox widgets. This creates a
 * number of button boxes displaying the various properties of the boxes. The
 * actual buttonboxes (and enclosing frames) are created using the createBox
 * method for conveniance.
 */
public class ButtonBoxes {
    public static void main(String[] args) {
        Gtk.init(args);

        // Construct the window
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Java-Gnome ButtonBox Example");
        window.setBorderWidth(10);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        // Create a Vbox to organise the screen
        VBox mainVBox = new VBox(false, 0);
        window.add(mainVBox);

        // The two types of buttonbox will be enclosed in frames.
        Frame horizFrame = new Frame("Horizontal Button Boxes");
        Frame vertFrame = new Frame("Vertical Button Boxes");

        // Add those frames to the main window
        mainVBox.packStart(horizFrame, true, true, 10);
        mainVBox.packStart(vertFrame, true, true, 10);

        // Create Boxes within those frames for drawing the individual
        // buttonboxes
        VBox horiz = new VBox(false, 0);
        HBox vert = new HBox(false, 0);
        horiz.setBorderWidth(10);
        vert.setBorderWidth(10);
        horizFrame.add(horiz);
        vertFrame.add(vert);

        // Add horizontal boxes
        horiz.packStart(createBox(true, "Default Style",
                ButtonBoxStyle.DEFAULT_STYLE));
        horiz.packStart(createBox(true, "Spread", ButtonBoxStyle.SPREAD));
        horiz.packStart(createBox(true, "Edge", ButtonBoxStyle.EDGE));
        horiz.packStart(createBox(true, "Start", ButtonBoxStyle.START));
        horiz.packStart(createBox(true, "End", ButtonBoxStyle.END));

        // Add vertical boxes
        vert.packStart(createBox(false, "Default Style",
                ButtonBoxStyle.DEFAULT_STYLE));
        vert.packStart(createBox(false, "Spread", ButtonBoxStyle.SPREAD));
        vert.packStart(createBox(false, "Edge", ButtonBoxStyle.EDGE));
        vert.packStart(createBox(false, "Start", ButtonBoxStyle.START));
        vert.packStart(createBox(false, "End", ButtonBoxStyle.END));

        window.show();
        window.showAll();
        mainVBox.showAll();
        horizFrame.showAll();
        horiz.showAll();
        Gtk.main();
    }

    /**
     * Construct a single button box enclosed within a frame. Adds a few buttons
     * to the box.
     * 
     * @param horizontal
     *            If true, a horizontal box is created, else a vertical box is
     *            created.
     * @param label
     *            Text label to go at the top of the frame
     * @param layout
     *            Style to use fo rhte buttonBox.
     */
    public static Frame createBox(boolean horizontal, String label,
            ButtonBoxStyle layout) {
        ButtonBox box = null;
        if (horizontal) {
            box = new HButtonBox();
        } else {
            box = new VButtonBox();
        }
        box.setLayout(layout);
        box.setSpacing(40);
        box.setBorderWidth(5);
        box.packStart(new Button(GtkStockItem.OK));
        box.packStart(new Button(GtkStockItem.CANCEL));
        box.packStart(new Button(GtkStockItem.HELP));

        Frame frame = new Frame(label);
        frame.add(box);
        return frame;
    }
}
