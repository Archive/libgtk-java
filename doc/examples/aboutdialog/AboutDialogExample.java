package aboutdialog;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.JGException;
import org.gnu.gtk.AboutDialog;
import org.gnu.gtk.AboutDialogActivateLinkMethod;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.LifeCycleEvent;

public class AboutDialogExample {

    private static final String APPNAME = "AboutDialogExample";

    private static final String VERSION = "0.1";

    private static final String LICENSE = "GPL";

    private static final String DESCRIPTION = "This is an AboutDialog.";

    private static final String AUTHORS[] = { "http://java-gnome.sf.net",
            "http://www.gtk.org" };

    private static final String DOCUMENTERS[] = {
            "<java-gnome-developer@lists.sf.net>", "http://www.gnome.org" };

    private static final String TRANSLATORS = "Language Guys Inc.";

    private static final String WEBSITE = "http://java-gnome.sf.net";

    public static void main(String[] argv) {
        Gtk.init(argv);
        AboutDialogExample example = new AboutDialogExample();
        Gtk.main();
    }

    protected AboutDialog about;

    public AboutDialogExample() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("AboutDialogExample");
        window.addListener(new Life());

        about = new AboutDialog();
        about.setName(APPNAME);
        about.setVersion(VERSION);
        about.setLicense(LICENSE);
        about.setComments(DESCRIPTION);
        about.setAuthors(AUTHORS);
        about.setDocumenters(DOCUMENTERS);
        about.setTranslatorCredits(TRANSLATORS);
        about.setWebsite(WEBSITE);
        try {
            about.setLogo(new Pixbuf("aboutdialog/java-gnome.png"));
        } catch (java.io.FileNotFoundException fe) {
            System.out.println("Could not load icon.");
        } catch (JGException jge) {
            System.out.println("JGException: " + jge.getMessage());
        }
        AboutDialog.setURLHook(new LinkHandler());
        AboutDialog.setEmailHook(new LinkHandler());

        Button button = new Button("Show About");
        button.addListener(new ButtonClickedListener());
        window.add(button);
        window.showAll();
    }

    protected class LinkHandler implements AboutDialogActivateLinkMethod {
        public void linkActivated(AboutDialog about, String link) {
            System.out.println("linkActivated: " + link);
        }
    }

    public class ButtonClickedListener implements ButtonListener {
        public void buttonEvent(ButtonEvent event) {
            try {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    clickButtonEvent(event);
                }
            } catch (Throwable e) {
                System.err.println("Got an exception!?!");
                System.err.println(e);
            }
        }

        protected void clickButtonEvent(ButtonEvent event) {

            // String[] auths = about.getAuthors();
            // for( int i = 0; i < auths.length; i++ )
            // System.err.println( auths[i] );

            about.show();
        }
    }

    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {
        }

        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                    || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                Gtk.mainQuit();
            }
            return false;
        }
    }

}
