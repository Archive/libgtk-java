package image;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Image;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class ImageTest {

    public ImageTest() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        Image image = new Image("image/test.xpm");
        Button button = new Button();
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK))
                    buttonClicked();
            }
        });
        button.add(image);
        window.add(button);
        window.showAll();
    }

    public void buttonClicked() {
        System.out.println("button clicked");
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        ImageTest pm = new ImageTest();

        Gtk.main();
    }
}
