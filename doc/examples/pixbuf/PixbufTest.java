package pixbuf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.gnu.glib.JGException;
import org.gnu.gdk.Pixbuf;
import org.gnu.gdk.PixbufLoader;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Image;
import org.gnu.gtk.Label;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.LifeCycleEvent;

public class PixbufTest {

    public PixbufTest() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.addListener(new Life());
        window.show();

        VBox box = new VBox(true, 5);
        window.add(box);

        String imgfile = "pixbuf/java-gnome-logo.png";

        //
        // Load the image from a file.
        //
        try {
            Pixbuf pix = new Pixbuf(imgfile);
            Image image = new Image(pix);
            box.packStart(image);
        } catch (JGException jg) {
            System.out.println("JGException loading: " + imgfile);
            System.out.println(jg.getMessage());
            box.packStart(new Label("JGException loading: " + imgfile + "\n"
                    + jg.getMessage()));
        } catch (FileNotFoundException fe) {
            System.out.println("File not found: " + imgfile);
            System.out.println(fe.getMessage());
            box.packStart(new Label("File not found: " + imgfile + "\n"
                    + fe.getMessage()));
        }

        // 
        // Load the image using a PixbufLoader. This is useful if you
        // have a stream of data coming from somewhere like a database
        // or HTTP request.
        //
        try {
            FileInputStream img = new FileInputStream(imgfile);
            PixbufLoader loader = new PixbufLoader();
            loader.write(img);
            loader.close();
            Pixbuf pix = loader.getPixbuf();
            Image image = new Image(pix);
            box.packStart(image);
        } catch (IOException ie) {
            System.out.println("Can't load file: " + imgfile);
            System.out.println(ie.getMessage());
            box.packStart(new Label("Can't load file: " + imgfile + "\n"
                    + ie.getMessage()));
        }

        window.showAll();
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new PixbufTest();
        Gtk.main();
    }

    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {
        }

        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                    || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                Gtk.mainQuit();
            }
            return false;
        }
    }
}
