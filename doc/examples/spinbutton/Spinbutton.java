package spinbutton;

import org.gnu.gtk.Adjustment;
import org.gnu.gtk.Button;
import org.gnu.gtk.CheckButton;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Label;
import org.gnu.gtk.SpinButton;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.AdjustmentEvent;
import org.gnu.gtk.event.AdjustmentListener;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class Spinbutton {

    private SpinButton spinner1 = null;

    private SpinButton spinner2 = null;

    private CheckButton cbutton1 = null;

    private CheckButton cbutton2 = null;

    private Label valLabel = null;

    private Window window = null;

    public Spinbutton() {

        window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Spin Button");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        VBox mainVbox = new VBox(false, 5);
        mainVbox.setBorderWidth(10);
        window.add(mainVbox);

        Frame frame = new Frame("Not accelerated");
        mainVbox.packStart(frame, true, true, 0);

        VBox vbox = new VBox(false, 0);
        vbox.setBorderWidth(5);
        frame.add(vbox);

        // Day, month, year spinners
        HBox hbox = new HBox(false, 0);
        vbox.packStart(hbox, true, true, 5);

        VBox vbox2 = new VBox(false, 0);
        hbox.packStart(vbox2, true, true, 5);

        Label label = new Label("Day :");
        vbox2.packStart(label, false, true, 0);

        Adjustment adj = new Adjustment(1.0, 1.0, 31.0, 1.0, 5.0, 0.0);
        SpinButton spinner = new SpinButton(adj, 0, 0);
        spinner.setWrap(true);
        vbox2.packStart(spinner, false, true, 0);

        vbox2 = new VBox(false, 0);
        hbox.packStart(vbox2, true, true, 5);

        label = new Label("Month :");
        vbox2.packStart(label, false, true, 0);

        adj = new Adjustment(1.0, 1.0, 12.0, 1.0, 5.0, 0.0);
        spinner = new SpinButton(adj, 0, 0);
        spinner.setWrap(true);
        vbox2.packStart(spinner, false, true, 0);

        vbox2 = new VBox(false, 0);
        hbox.packStart(vbox2, true, true, 5);

        label = new Label("Year :");
        vbox2.packStart(label, false, true, 0);

        adj = new Adjustment(2000.0, 0.0, 2100.0, 1.0, 100.0, 0.0);
        spinner = new SpinButton(adj, 0, 0);
        spinner.setWrap(false);
        vbox2.packStart(spinner, false, true, 0);

        frame = new Frame("Accelerated");
        mainVbox.packStart(frame, true, true, 0);

        vbox = new VBox(false, 0);
        vbox.setBorderWidth(5);
        frame.add(vbox);

        hbox = new HBox(false, 0);
        vbox.packStart(hbox, false, true, 5);

        vbox2 = new VBox(false, 0);
        hbox.packStart(vbox2, true, true, 5);

        label = new Label("Value :");
        vbox2.packStart(label, false, true, 0);

        adj = new Adjustment(0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0);
        spinner1 = new SpinButton(adj, 1.0, 2);
        spinner1.setWrap(true);
        vbox2.packStart(spinner1, false, true, 0);

        vbox2 = new VBox(false, 0);
        hbox.packStart(vbox2, true, true, 5);

        label = new Label("Digits :");
        vbox2.packStart(label, false, true, 0);

        adj = new Adjustment(2, 1, 5, 1, 1, 0);
        spinner2 = new SpinButton(adj, 0.0, 0);
        spinner2.setWrap(true);
        adj.addListener(new AdjustmentListener() {
            public void adjustmentEvent(AdjustmentEvent event) {
                changeDigits();
            }
        });
        vbox2.packStart(spinner2, false, true, 0);

        hbox = new HBox(false, 0);
        vbox.packStart(hbox, false, true, 5);

        cbutton1 = new CheckButton("Snap to 0.5-ticks", false);
        cbutton1.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    toggleSnap();
                }
            }
        });
        vbox.packStart(cbutton1, true, true, 0);
        cbutton1.setState(true);

        cbutton2 = new CheckButton("Numeric only input mode", false);
        cbutton2.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    toggleNumeric();
                }
            }
        });
        vbox.packStart(cbutton2, true, true, 0);
        cbutton2.setState(true);

        valLabel = new Label("");

        hbox = new HBox(false, 0);
        vbox.packStart(hbox, false, true, 5);
        Button button = new Button("Value as Int", false);
        // button.setUserData(valLabel);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    valLabel.setText("" + spinner1.getIntValue());
                }
            }
        });
        hbox.packStart(button, true, true, 5);

        button = new Button("Value as Float", false);
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    valLabel.setText("" + spinner1.getValue());
                }
            }
        });
        hbox.packStart(button, true, true, 5);

        vbox.packStart(valLabel, true, true, 0);
        valLabel.setText("0");

        hbox = new HBox(false, 0);
        mainVbox.packStart(hbox, false, true, 0);

        button = new Button("Close");
        button.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    window.destroy();
                    Gtk.mainQuit();
                }
            }
        });
        hbox.packStart(button, true, true, 5);

        window.showAll();
    }

    private void changeDigits() {
        spinner1.setPrecision(spinner2.getIntValue());
    }

    private void toggleSnap() {
        spinner1.setSnap(cbutton1.getState());
    }

    private void toggleNumeric() {
        spinner1.setNumeric(cbutton2.getState());
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        Spinbutton sb = new Spinbutton();

        Gtk.main();
    }
}
