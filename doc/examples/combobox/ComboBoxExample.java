package combobox;

import org.gnu.gtk.ComboBox;
import org.gnu.gtk.CellRendererText;
import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnBoolean;
import org.gnu.gtk.DataColumnString;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.ListStore;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreeModel;
import org.gnu.gtk.TreeViewRowSeparatorMethod;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.LifeCycleEvent;

public class ComboBoxExample {

    public static void main(String[] argv) {
        Gtk.init(argv);
        ComboBoxExample example = new ComboBoxExample();
        Gtk.main();
    }

    protected ComboBox box1;

    protected DataColumnBoolean dataBool;

    protected DataColumnString dataStr;

    protected ListStore timestore;

    public ComboBoxExample() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("ComboBoxExample");
        window.addListener(new Life());

        // Create a ListStore to hold the values to be displayed in the
        // ComboBox.
        dataBool = new DataColumnBoolean();
        dataStr = new DataColumnString();
        timestore = new ListStore(new DataColumn[] { dataStr, dataBool });
        TreeIter iter;
        for (int i = 0; i < TIMES.length; i += 2) {
            iter = timestore.appendRow();
            timestore.setValue(iter, dataStr, (String) TIMES[i]);
            timestore.setValue(iter, dataBool, ((Boolean) TIMES[i + 1])
                    .booleanValue());
        }

        // Create the ComboBox with the ListStore as the data model.
        box1 = new ComboBox(timestore);
        // Set to display the dataBool=false rows as separators.
        box1.setRowSeparatorMethod(new RowSeparator());
        // Create a renderer to display the ListStore data.
        CellRendererText renderer = new CellRendererText();
        // Attach the renderer to the ComboBox so it uses it to display
        // the data.
        box1.packStart(renderer, true);
        box1.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                dataStr);
        // Set the active element to be the first element in the liststore.
        box1.setActive(0);

        window.add(box1);
        window.showAll();
    }

    private static final Object[] TIMES = { "00:00", new Boolean(true),
            "01:00", new Boolean(false), "02:00", new Boolean(true), "03:00",
            new Boolean(true), "04:00", new Boolean(false), "05:00",
            new Boolean(true), "06:00", new Boolean(true), "07:00",
            new Boolean(false), "08:00", new Boolean(true), "09:00",
            new Boolean(true), "10:00", new Boolean(false), "11:00",
            new Boolean(true), "12:00", new Boolean(true), "13:00",
            new Boolean(false), "14:00", new Boolean(true), "15:00",
            new Boolean(true), "16:00", new Boolean(false), "17:00",
            new Boolean(true), "18:00", new Boolean(true), "19:00",
            new Boolean(false), "20:00", new Boolean(true), "21:00",
            new Boolean(true), "22:00", new Boolean(false), "23:00",
            new Boolean(true), };

    protected class RowSeparator implements TreeViewRowSeparatorMethod {
        public boolean isSeparator(TreeModel model, TreeIter iter) {
            return !model.getValue(iter, dataBool);
        }
    }

    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {
        }

        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                    || event.isOfType(LifeCycleEvent.Type.DELETE)) {

                TreeIter iter = box1.getActiveIter();
                if (iter != null) {
                    System.out.println("selected was: "
                            + timestore.getValue(iter, dataStr));
                } else {
                    System.out.println("selected was: No Selection");
                }
                Gtk.mainQuit();
            }
            return false;
        }
    }

}
