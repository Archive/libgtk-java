package fixed;

import org.gnu.gtk.Button;
import org.gnu.gtk.Fixed;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class FixedExample implements LifeCycleListener {

    Fixed fixed;

    Button button1, button2, button3;

    int x = 50;

    int y = 50;

    public FixedExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setBorderWidth(10);
        window.setTitle("Fixed Container");
        window.addListener(this);

        fixed = new Fixed();
        window.add(fixed);

        button1 = new Button("Press Me", false);
        button1.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    x = (x + 30) % 300;
                    y = (y + 50) % 300;
                    fixed.moveWidget(button1, x, y);
                }
            }
        });
        fixed.putWidget(button1, 50, 50);

        button2 = new Button("Press Me", false);
        button2.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    x = (x + 30) % 300;
                    y = (y + 50) % 300;
                    fixed.moveWidget(button2, x, y);
                }
            }
        });
        fixed.putWidget(button2, 100, 100);

        button3 = new Button("Press Me", false);
        button3.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    x = (x + 30) % 300;
                    y = (y + 50) % 300;
                    fixed.moveWidget(button3, x, y);
                }
            }
        });
        fixed.putWidget(button3, 150, 150);

        window.showAll();
    }

    public void lifeCycleEvent(LifeCycleEvent event) {

    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                || event.isOfType(LifeCycleEvent.Type.DELETE))
            Gtk.mainQuit();
        return false;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        FixedExample fixed = new FixedExample();

        Gtk.main();
    }
}
