package paned;

import org.gnu.gtk.AttachOptions;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.PolicyType;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.SimpleList;
import org.gnu.gtk.Table;
import org.gnu.gtk.TextBuffer;
import org.gnu.gtk.TextView;
import org.gnu.gtk.VPaned;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class PanedExample {

    TextView text = null;

    TextBuffer textBuffer = null;

    public PanedExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Panned Windows");
        window.setBorderWidth(10);
        window.setDefaultSize(450, 400);
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        VPaned vpaned = new VPaned();
        window.add(vpaned);
        vpaned.show();

        ScrolledWindow list = createList();
        vpaned.add1(list);
        list.show();

        Table table = createText();
        vpaned.add2(table);
        table.show();

        window.show();
    }

    // create the list of messages
    public ScrolledWindow createList() {
        // Create a new scrolled window with scrollbars only if needed.
        ScrolledWindow scrolledWindow = new ScrolledWindow(null, null);
        scrolledWindow.setPolicy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);

        // Create a new list and put it in the scrolled window
        SimpleList list = new SimpleList();
        scrolledWindow.addWithViewport(list);
        list.show();

        // Add some messages to the window
        for (int i = 0; i < 10; i++) {
            list.addEnd("Message #" + i);
        }
        return scrolledWindow;
    }

    // Create a scrolled text area that displays a "message"
    public Table createText() {
        // Create a table to hold the widget and scrollbars
        Table table = new Table(2, 2, false);

        // Put a text widget in the upper left hand corner. Note the
        // use of _SHRINK in the y direction.
        textBuffer = new TextBuffer();
        text = new TextView(textBuffer);
        table.attach(text, 0, 1, 0, 1, AttachOptions.EXPAND
                .or(AttachOptions.FILL), AttachOptions.EXPAND
                .or(AttachOptions.FILL.or(AttachOptions.SHRINK)), 0, 0);
        String str = "From: pathfinder@nasa.gov\n" + "To: mom@nasa.gov\n"
                + "\n" + "We just got in this morning. The weather has been\n"
                + "great - clear but cold, and there are lots of fun sights.\n"
                + "Sojourner says hi. See you soon.\n" + " -Path\n";
        textBuffer.insertText(str);
        text.show();

        return table;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        PanedExample panned = new PanedExample();

        Gtk.main();
    }
}
