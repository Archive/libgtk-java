/**
 * $Id$
 *
 **/
package expander;

import org.gnu.gtk.*;
import org.gnu.gtk.event.*;

public class SimpleExpander {

    private Label label = null;

    private Label indicator = null;

    private HBox hbox = null;

    private int winHeight = 250;

    private int winWidht = 450;

    private String[] items = { "Second", "Third", "Fourth" };

    private String[] styleFlags = { "Collapse", "Expande" };

    private Expander expander = new Expander("Test expander", false);

    private RadioButton[] radioBtns = new RadioButton[styleFlags.length];

    public SimpleExpander() {
        Window mainWin = new Window(WindowType.TOPLEVEL);
        mainWin.setDecorated(true);
        mainWin.setDefaultSize(winWidht, winHeight);

        mainWin.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return true;
            }
        });

        expander.addListener(new ExpanderListener() {
            public boolean expanderEvent(ExpanderEvent event) {
                if (event.isOfType(ExpanderEvent.Type.ACTIVATE)) {
                    indicator.setLabel("YES");
                    setButtonState(expander.getExpanded());
                }

                return false;
            }
        });

        SimpleList list = new SimpleList();
        list.addStart("First list element");
        for (int i = 0; i < items.length; i++) {
            String item = items[i];
            list.addEnd(item + " list element");
        }

        expander.add(list);
        expander.setBorderWidth(3);
        VBox mainBox = new VBox(false, 4);

        HBox topBox = new HBox(true, 4);
        HBox bottomBox = new HBox(false, 4);

        mainBox.packStart(topBox, true, true, 4);
        mainBox.packStart(bottomBox, true, true, 4);

        VBox exBox = new VBox(false, 2);
        // mainBox.packStart(vbox, false, false, 0);
        topBox.packStart(exBox, true, true, 4);

        Frame frame = new Frame("Expander Frame");
        frame.add(expander);

        hbox = new HBox(false, 4);

        Frame evFrame = new Frame("Expander Events");
        evFrame.add(hbox);

        label = new Label("Activate Expander: ");
        hbox.packStart(label, false, true, 0);
        indicator = new Label("");
        hbox.packStart(indicator, false, true, 0);
        exBox.packStart(frame, true, true, 0);
        VBox evBox = new VBox(false, 4);
        bottomBox.packStart(evBox, true, true, 2);
        evBox.packStart(evFrame, false, false, 4);

        VBox chBox = new VBox(false, 4);
        Frame chFrame = new Frame("Expander Style");

        RadioButton rBtn = new RadioButton((RadioButton) null, "Test", false);

        for (int i = 0; i < styleFlags.length; i++) {
            String styleFlag = styleFlags[i];
            radioBtns[i] = new RadioButton(rBtn, styleFlag, false);
            radioBtns[i].addListener(new ButtonListener() {
                public void buttonEvent(ButtonEvent event) {
                    if (event.isOfType(ButtonEvent.Type.CLICK)) {
                        setExpanderStyle();
                    }
                }
            });

            chBox.packStart(radioBtns[i], false, false, 0);
        }

        chFrame.add(chBox);
        topBox.packStart(chFrame, true, true, 0);

        VButtonBox bbox = new VButtonBox();
        evBox.packStart(bbox, true, true, 0);
        bbox.setLayout(ButtonBoxStyle.START);

        Button closeBtn = new Button(GtkStockItem.CLOSE);
        closeBtn.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent event) {
                if (event.isOfType(ButtonEvent.Type.CLICK)) {
                    Gtk.mainQuit();
                }
            }
        });

        bbox.packStart(closeBtn, true, false, 0);

        mainWin.add(mainBox);
        mainWin.showAll();
    }

    private void setButtonState(boolean b) {
        if (b == true)
            radioBtns[0].activate();
        else
            radioBtns[1].activate();
    }

    private void setExpanderStyle() {
        if (radioBtns[0].getState()) {
            expander.setExpanded(false);
        } else if (radioBtns[1].getState()) {
            expander.setExpanded(true);
        }
    }

    public static void main(final String[] args) {
        Gtk.init(args);
        new SimpleExpander();
        Gtk.main();
    }
}
