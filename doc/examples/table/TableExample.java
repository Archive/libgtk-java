package table;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Table;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class TableExample implements ButtonListener, LifeCycleListener {

    protected Button button1, button2, button3;

    public TableExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Table Example");
        window.setBorderWidth(20);
        window.addListener(this);

        Table table = new Table(2, 2, true);
        window.add(table);

        button1 = new Button("button 1", false);
        button1.addListener((ButtonListener) this);
        table.attach(button1, 0, 1, 0, 1);

        button2 = new Button("button 2", false);
        button2.addListener((ButtonListener) this);
        table.attach(button2, 1, 2, 0, 1);

        button3 = new Button("Quit", false);
        button3.addListener((ButtonListener) this);
        table.attach(button3, 0, 2, 1, 2);

        window.showAll();
    }

    public void buttonEvent(ButtonEvent event) {
        if (event.isOfType(ButtonEvent.Type.CLICK)) {
            String source = "";
            if (button1 == event.getSource())
                source = "button 1";
            else if (button2 == event.getSource())
                source = "button 2";
            else if (button3 == event.getSource())
                source = "Quit";
            System.out.println(source + " pressed");
        }
    }

    public void lifeCycleEvent(LifeCycleEvent event) {
    }

    public boolean lifeCycleQuery(LifeCycleEvent event) {
        if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                || event.isOfType(LifeCycleEvent.Type.DELETE))
            Gtk.mainQuit();
        return false;
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);
        new TableExample();
        Gtk.main();
    }
}
