package stockicons;

import org.gnu.gtk.Button;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HBox;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Widget;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class GtkStockIcons {

    public GtkStockIcons() {
        Window w = new Window(WindowType.TOPLEVEL);
        w.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        w.setName("The Java-Gnome team");
        w.setBorderWidth(5);
        w.add(createGUI());
        w.showAll();
    }

    public Widget createGUI() {
        VBox topBox = new VBox(false, 10);
        topBox.add(createFileIconBox());
        topBox.add(createNavigationIconBox());
        topBox.add(createEditIconBox());
        topBox.add(createDialogBox());
        topBox.add(createOtherBox());
        return topBox;
    }

    private Frame createFileIconBox() {
        HBox fileBox1 = new HBox(true, 2);
        Button save = new Button(GtkStockItem.SAVE);
        fileBox1.add(save);
        Button saveAs = new Button(GtkStockItem.SAVE_AS);
        fileBox1.add(saveAs);
        Button close = new Button(GtkStockItem.CLOSE);
        fileBox1.add(close);
        Button open = new Button(GtkStockItem.OPEN);
        fileBox1.add(open);

        HBox fileBox2 = new HBox(true, 2);
        Button print = new Button(GtkStockItem.PRINT);
        fileBox2.add(print);
        Button printPreview = new Button(GtkStockItem.PRINT_PREVIEW);
        fileBox2.add(printPreview);
        Button quit = new Button(GtkStockItem.QUIT);
        fileBox2.add(quit);
        Button newb = new Button(GtkStockItem.NEW);
        fileBox2.add(newb);

        VBox fileIconBox = new VBox(true, 2);
        fileIconBox.add(fileBox1);
        fileIconBox.add(fileBox2);
        Frame f = new Frame("File manipulation Icons");
        f.add(fileIconBox);

        return f;
    }

    private Frame createNavigationIconBox() {
        HBox navBox1 = new HBox(true, 2);
        Button goToBottom = new Button(GtkStockItem.GOTO_BOTTOM);
        navBox1.add(goToBottom);
        Button goToFirst = new Button(GtkStockItem.GOTO_FIRST);
        navBox1.add(goToFirst);
        Button goToLast = new Button(GtkStockItem.GOTO_LAST);
        navBox1.add(goToLast);
        Button goToTop = new Button(GtkStockItem.GOTO_TOP);
        navBox1.add(goToTop);

        HBox navBox2 = new HBox(true, 2);
        Button goBack = new Button(GtkStockItem.GO_BACK);
        navBox2.add(goBack);
        Button goFoward = new Button(GtkStockItem.GO_FORWARD);
        navBox2.add(goFoward);
        Button goUp = new Button(GtkStockItem.GO_UP);
        navBox2.add(goUp);
        Button goDown = new Button(GtkStockItem.GO_DOWN);
        navBox2.add(goDown);

        HBox navBox3 = new HBox(true, 2);
        Button home = new Button(GtkStockItem.HOME);
        navBox3.add(home);
        Button stop = new Button(GtkStockItem.STOP);
        navBox3.add(stop);
        Button jumpTo = new Button(GtkStockItem.JUMP_TO);
        navBox3.add(jumpTo);
        Button index = new Button(GtkStockItem.INDEX);
        navBox3.add(index);

        VBox navigationIconBox = new VBox(true, 2);
        navigationIconBox.add(navBox1);
        navigationIconBox.add(navBox2);
        navigationIconBox.add(navBox3);
        Frame f = new Frame("Navigation Icons");
        f.add(navigationIconBox);

        return f;
    }

    private Frame createEditIconBox() {
        HBox editBox1 = new HBox(true, 2);
        Button clear = new Button(GtkStockItem.CLEAR);
        editBox1.add(clear);
        Button copy = new Button(GtkStockItem.COPY);
        editBox1.add(copy);
        Button cut = new Button(GtkStockItem.CUT);
        editBox1.add(cut);
        Button paste = new Button(GtkStockItem.PASTE);
        editBox1.add(paste);

        HBox editBox2 = new HBox(true, 2);
        Button delete = new Button(GtkStockItem.DELETE);
        editBox2.add(delete);
        Button undelete = new Button(GtkStockItem.UNDELETE);
        editBox2.add(undelete);
        Button find = new Button(GtkStockItem.FIND);
        editBox2.add(find);
        Button findAndReplace = new Button(GtkStockItem.FIND_AND_REPLACE);
        editBox2.add(findAndReplace);

        HBox editBox3 = new HBox(true, 2);
        Button justifyLeft = new Button(GtkStockItem.JUSTIFY_LEFT);
        editBox3.add(justifyLeft);
        Button justifyCenter = new Button(GtkStockItem.JUSTIFY_CENTER);
        editBox3.add(justifyCenter);
        Button justifyRight = new Button(GtkStockItem.JUSTIFY_RIGHT);
        editBox3.add(justifyRight);
        Button justifyFill = new Button(GtkStockItem.JUSTIFY_FILL);
        editBox3.add(justifyFill);

        HBox editBox4 = new HBox(true, 2);
        Button bold = new Button(GtkStockItem.BOLD);
        editBox4.add(bold);
        Button italic = new Button(GtkStockItem.ITALIC);
        editBox4.add(italic);
        Button underline = new Button(GtkStockItem.UNDERLINE);
        editBox4.add(underline);
        Button strikeThrough = new Button(GtkStockItem.STRIKETHROUGH);
        editBox4.add(strikeThrough);

        VBox editVBox = new VBox(true, 2);
        editVBox.add(editBox1);
        editVBox.add(editBox2);
        editVBox.add(editBox3);
        editVBox.add(editBox4);
        Frame f = new Frame("Edition Icons");
        f.add(editVBox);
        return f;
    }

    private Frame createDialogBox() {
        HBox dialogBox1 = new HBox(true, 2);
        Button ok = new Button(GtkStockItem.OK);
        dialogBox1.add(ok);
        Button cancel = new Button(GtkStockItem.CANCEL);
        dialogBox1.add(cancel);
        Button apply = new Button(GtkStockItem.APPLY);
        dialogBox1.add(apply);
        Button help = new Button(GtkStockItem.HELP);
        dialogBox1.add(help);

        HBox dialogBox2 = new HBox(true, 2);
        Button info = new Button(GtkStockItem.DIALOG_INFO);
        dialogBox2.add(info);
        Button warn = new Button(GtkStockItem.DIALOG_WARNING);
        dialogBox2.add(warn);
        Button error = new Button(GtkStockItem.DIALOG_ERROR);
        dialogBox2.add(error);
        Button question = new Button(GtkStockItem.DIALOG_QUESTION);
        dialogBox2.add(question);

        VBox dialogVBox = new VBox(true, 2);
        dialogVBox.add(dialogBox1);
        dialogVBox.add(dialogBox2);
        Frame f = new Frame("Dialog Icons");
        f.add(dialogVBox);
        return f;
    }

    private Frame createOtherBox() {
        HBox otherBox1 = new HBox(true, 2);
        Button floppy = new Button(GtkStockItem.FLOPPY);
        otherBox1.add(floppy);
        Button cdrom = new Button(GtkStockItem.CDROM);
        otherBox1.add(cdrom);
        Button convert = new Button(GtkStockItem.CONVERT);
        otherBox1.add(convert);
        Button spellCheck = new Button(GtkStockItem.SPELL_CHECK);
        otherBox1.add(spellCheck);

        HBox otherBox2 = new HBox(true, 2);
        Button zoomIn = new Button(GtkStockItem.ZOOM_IN);
        otherBox2.add(zoomIn);
        Button zoomOut = new Button(GtkStockItem.ZOOM_OUT);
        otherBox2.add(zoomOut);
        Button zoomFit = new Button(GtkStockItem.ZOOM_FIT);
        otherBox2.add(zoomFit);
        Button zoom = new Button(GtkStockItem.ZOOM_100);
        otherBox2.add(zoom);

        HBox otherBox3 = new HBox(true, 2);
        Button yes = new Button(GtkStockItem.YES);
        otherBox3.add(yes);
        Button no = new Button(GtkStockItem.NO);
        otherBox3.add(no);
        Button undo = new Button(GtkStockItem.UNDO);
        otherBox3.add(undo);
        Button redo = new Button(GtkStockItem.REDO);
        otherBox3.add(redo);

        HBox otherBox4 = new HBox(true, 2);
        Button color = new Button(GtkStockItem.SELECT_COLOR);
        otherBox4.add(color);
        Button font = new Button(GtkStockItem.SELECT_FONT);
        otherBox4.add(font);
        Button preferences = new Button(GtkStockItem.PREFERENCES);
        otherBox4.add(preferences);
        Button properties = new Button(GtkStockItem.PROPERTIES);
        otherBox4.add(properties);

        HBox otherBox5 = new HBox(true, 2);
        Button execute = new Button(GtkStockItem.EXECUTE);
        otherBox5.add(execute);
        Button remove = new Button(GtkStockItem.REMOVE);
        otherBox5.add(remove);
        Button refresh = new Button(GtkStockItem.REFRESH);
        otherBox5.add(refresh);
        Button revertToSaved = new Button(GtkStockItem.REVERT_TO_SAVED);
        otherBox5.add(revertToSaved);

        VBox otherVBox = new VBox(true, 2);
        otherVBox.add(otherBox1);
        otherVBox.add(otherBox2);
        otherVBox.add(otherBox3);
        otherVBox.add(otherBox4);
        otherVBox.add(otherBox5);
        Frame f = new Frame("Other Icons");
        f.add(otherVBox);
        return f;
    }

    public static void main(String args[]) {
        Gtk.init(args);
        new GtkStockIcons();
        Gtk.main();
    }
}
