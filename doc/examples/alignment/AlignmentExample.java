package alignment;

import org.gnu.gtk.Alignment;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class AlignmentExample {

    public AlignmentExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Alignment Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });
        window.setDefaultSize(100, 100);

        Button button = new Button("The Button Label", false);
        // We will create an Alignment and add the button to it.
        // The x alignment is set to 1.0, which keeps the button
        // against the right edge. The y alignment is 0.5, keeping
        // the widget vertically centered. The scale value is set
        // to 0.15 in both directions, which limits the expansion
        // of the button to no more than 15 percent of the area
        // made available to it.
        Alignment alignment = new Alignment(1.0, 0.5, 0.15, 0.15);
        alignment.add(button);
        window.add(alignment);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        AlignmentExample align = new AlignmentExample();

        Gtk.main();
    }
}
