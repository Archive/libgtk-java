package dataTable;

import org.gnu.gtk.CellRenderer;
import org.gnu.gtk.CellRendererText;
import org.gnu.gtk.CellRendererToggle;
import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnBoolean;
import org.gnu.gtk.DataColumnString;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.ListStore;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreeView;
import org.gnu.gtk.TreeViewColumn;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.TreeModelEvent;
import org.gnu.gtk.event.TreeModelListener;

/**
 * An example application creating a table using the TreeView widget.
 */
public class TableExample implements TreeModelListener {

    ListStore store;

    TreeView treeWidget;

    public TableExample() {
        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Table Example - TODO List");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        // Create some data blocks to represent the places where we store data
        DataColumnBoolean dataDone = new DataColumnBoolean();
        DataColumnString dataTarget = new DataColumnString();

        // Create a list store for the data of the table.
        store = new ListStore(new DataColumn[] { dataDone, dataTarget });
        store.addListener(this);
        // create a new row in the store and set values in that row
        TreeIter iter = store.appendRow();
        store.setValue(iter, dataDone, false);
        store.setValue(iter, dataTarget, "Complete all example apps");

        // add another row
        TreeIter iter2 = store.appendRow();
        store.setValue(iter2, dataDone, true);
        store.setValue(iter2, dataTarget, "Start the TableExample application");

        // add another row
        TreeIter iter3 = store.appendRow();
        store.setValue(iter3, dataDone, false);
        store.setValue(iter3, dataTarget, "Release the library");

        // another row.
        TreeIter iter4 = store.insertRowAfter(iter2);
        store.setValue(iter4, dataDone, false);
        store
                .setValue(iter4, dataTarget,
                        "Demonstrate some of the more powerful features of the list objects");

        // //////////////////////////////////////////////////////////////
        // Construct a widget for displaying the data.
        // Associate the widget with the above listStore (we could, in fact,
        // have
        // many TreeView widgets associated with the same store
        treeWidget = new TreeView(store);

        // create a new column in the view, set a title for it and append it to
        // the
        // tree widget.
        TreeViewColumn column1 = new TreeViewColumn();
        column1.setTitle("Target");
        treeWidget.appendColumn(column1);
        // Add renderers to that column so that the data will be displayed.
        // Set an attribute mapping so that the renderer knows which DataColumn
        // in
        // the store contains the data it needs. (datablock 0)
        CellRenderer renderer = new CellRendererText();
        column1.packStart(renderer, true);
        column1.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT,
                dataTarget);

        // Add a second column to display the boolean values as checkboxes.
        TreeViewColumn column2 = new TreeViewColumn();
        column2.setTitle("Done");
        treeWidget.appendColumn(column2);
        // Add a renderer to the column and set the data mapping
        CellRenderer renderer2 = new CellRendererToggle();
        column2.packStart(renderer2, true);
        column2.addAttributeMapping(renderer2,
                CellRendererToggle.Attribute.ACTIVE, dataDone);

        // let the user change the order of the columns by dragging them.
        column1.setReorderable(true);
        column2.setReorderable(true);
        // make them resixable
        column1.setResizable(true);
        column2.setResizable(true);

        window.add(treeWidget);
        treeWidget.show();
        window.show();
        treeWidget.setReorderable(true);

    }

    public void treeModelEvent(TreeModelEvent event) {
        // System.out.println("TreeModelEvent");
        // System.err.println("TreeModelEvent");
        if (event.isOfType(TreeModelEvent.Type.ROWS_REORDERED)) {
            System.out.println("Rows Reordered");
            System.out.println("Iter " + event.getTreeIter().toString());
            System.out.println("Path: " + event.getTreePath().toString());
            Gtk.mainQuit();
        }
    }

    public static void main(String[] args) {
        Gtk.init(args);
        new TableExample();
        Gtk.main();
    }
}
