package fileselector;

import org.gnu.gtk.*;
import org.gnu.gtk.event.*;

public class FileSelectorDemo {

    protected FileSelection filew;

    public FileSelectorDemo() {
        filew = new FileSelection("File selection");

        Button okBtn = filew.getOKButton();
        okBtn.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK)) {
                    System.out.println("Ok button pressed.");
                    Gtk.mainQuit();
                }
            }
        });

        Button cancelBtn = filew.getCancelButton();
        cancelBtn.addListener(new ButtonListener() {
            public void buttonEvent(ButtonEvent evt) {
                if (evt.isOfType(ButtonEvent.Type.CLICK))
                    System.out.println("Cancel button pressed");
            }
        });

        filew.show();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        FileSelectorDemo fs = new FileSelectorDemo();

        Gtk.main();
    }
}
