package invisible;

import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.Invisible;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

public class InvisibleExample {

    public InvisibleExample() {

        Window window = new Window(WindowType.TOPLEVEL);
        window.setTitle("Invisible Example");
        window.addListener(new LifeCycleListener() {
            public void lifeCycleEvent(LifeCycleEvent event) {
            }

            public boolean lifeCycleQuery(LifeCycleEvent event) {
                if (event.isOfType(LifeCycleEvent.Type.DESTROY)
                        || event.isOfType(LifeCycleEvent.Type.DELETE)) {
                    Gtk.mainQuit();
                }
                return false;
            }
        });

        HBox box = new HBox(false, 0);

        Button button = new Button("Left Button", true);
        box.packStart(button, false, false, 0);

        // The Invisible widget displays a blank window.
        // It is a very useful but often forgotten widget
        // that can be used very effectively as a placeholder
        // or spacer to assist in the layout of widgets. You
        // can size the widget using the setSizeRequest() method. Our
        // buttons will have a height of 60 pixels and there
        // will be a space of 30 pixels between them.
        Invisible invisible = new Invisible();
        invisible.setMinimumSize(30, 60);
        box.packStart(invisible, false, false, 0);

        button = new Button("Right Button", true);
        box.packStart(button, false, false, 0);

        window.add(box);

        window.showAll();
    }

    public static void main(String[] args) {
        // Initialize GTK
        Gtk.init(args);

        InvisibleExample inv = new InvisibleExample();

        Gtk.main();
    }
}
