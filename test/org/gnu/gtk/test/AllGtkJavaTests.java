package org.gnu.gtk.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 */
public class AllGtkJavaTests extends TestCase {

    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(ComboBoxTest.class);
        return suite;
    }

    public static void main(String[] args) {
        boolean textMode = false;
        String test[] = new String[1];
        test[0] = AllGtkJavaTests.class.getName();

        if (args.length == 1) {
            if (args[0].equals("text")) {
                textMode = true;
            }
        }

        if (textMode) {
            junit.textui.TestRunner.main(test);
        } else {
            junit.swingui.TestRunner.main(test);
        }
    }
}
