/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.test;

import org.gnu.gtk.Gtk;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;

import junit.framework.TestCase;

/**
 */
public class GtkTestCase extends TestCase {

    protected Window window;

    public GtkTestCase(String name) {
        super(name);
        Gtk.init(new String[] {});
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        window = new Window(WindowType.TOPLEVEL);
        window.realize();
    }
    
    
    protected void tearDown() throws Exception {
        super.tearDown();
        window.destroy();
    }
}
