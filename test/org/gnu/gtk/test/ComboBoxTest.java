/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtk.test;

import org.gnu.gtk.ComboBox;
import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnBoolean;
import org.gnu.gtk.DataColumnString;
import org.gnu.gtk.ListStore;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreeModel;

/**
 */
public class ComboBoxTest extends GtkTestCase {
	
	private ComboBox cb;
	private ListStore store;
    private DataColumnBoolean dataBool;
    private DataColumnString dataStr;
    
    public ComboBoxTest(String name) {
        super(name);
    }
    
    public void testCreateComboBox() {
        createComboBoxWithoutModel();
        assertEquals(cb.getWrapWidth(), 0);
        assertEquals(cb.getRowSpanColumn(), -1);
        assertEquals(cb.getColumnSpanColumn(), -1);
        assertFalse(cb.getAddTearoffs());
        assertTrue(cb.getFocusOnClick());
        assertEquals(cb.getActive(), -1);
        TreeModel tm = cb.getModel();
        assertEquals(tm.getColumnCount(), 1);
    }
    
    public void testCreateComboBoxWithModel() {
    	createComboBoxWithModel();
        addRow("GNU", true);
        TreeModel tm = cb.getModel();
        assertEquals(tm.getColumnCount(), 2);
        TreeIter i = tm.getFirstIter();
        assertEquals(tm.getValue(i, dataStr), "GNU");
    }
    
    public void testWrapWidth() {
        createComboBoxWithModel();
        assertEquals(cb.getWrapWidth(), 0);
        cb.setWrapWidth(10);
        assertEquals(cb.getWrapWidth(), 10);
    }

//    public void testRowSpanColumn() {
//    	createComboBoxWithModel();
//        addRow("One", true);
//        addRow("Two", true);
//        assertEquals(cb.getRowSpanColumn(), -1);
//        cb.setRowSpanColumn(2);
//        assertEquals(cb.getRowSpanColumn(), 2);
//    }
    
//    public void testColumnSpanColumn() {
//    	createComboBoxWithModel();
//        assertEquals(cb.getColumnSpanColumn(), -1);
//        cb.setColumnSpanColumn(2);
//        assertEquals(cb.getColumnSpanColumn(), 2);
//    }
    
    public void testAddTearoffs() {
        createComboBoxWithModel();
    	assertFalse(cb.getAddTearoffs());
    	cb.setAddTearoffs(true);
    	assertTrue(cb.getAddTearoffs());
    }

    public void testFocusOnClick() {
        createComboBoxWithModel();
    	assertTrue(cb.getFocusOnClick());
    	cb.setFocusOnClick(false);
    	assertFalse(cb.getFocusOnClick());
    }

    public void testActive() {
        createComboBoxWithModel();
        addRow("One", true);
        addRow("Two", true);
        assertEquals(cb.getActive(), -1);
    	cb.setActive(1);
    	assertEquals(cb.getActive(), 1);
    	cb.setActive(0);
    	assertEquals(cb.getActive(), 0);
    }

    public void testActiveIter() {
    	createComboBoxWithModel();
        addRow("One", true);
        addRow("Two", true);
        TreeIter iter = cb.getActiveIter();
        assertNull(iter);
        cb.setActive(0);
        iter = cb.getActiveIter();
        assertEquals(store.getValue(iter, dataStr), "One");
        cb.setActive(1);
        iter = cb.getActiveIter();
        assertEquals(store.getValue(iter, dataStr), "Two");
    }
    
    public void testAppendText() {
    	createComboBoxWithoutModel();
    	cb.appendText("One");
    	cb.appendText("Two");
    	cb.appendText("Three");
    	cb.setActive(0);
    	assertEquals(cb.getActiveText(), "One");
    	cb.setActive(1);
    	assertEquals(cb.getActiveText(), "Two");
    }
    
    public void testInsertText() {
    	createComboBoxWithoutModel();
    	cb.appendText("Two");
    	cb.insertText(0, "One");
    	cb.setActive(0);
    	assertEquals(cb.getActiveText(), "One");
    	cb.setActive(1);
    	assertEquals(cb.getActiveText(), "Two");
    }
    
    public void testPrependText() {
    	createComboBoxWithoutModel();
    	cb.appendText("Two");
    	cb.prependText("One");
    	cb.setActive(0);
    	assertEquals(cb.getActiveText(), "One");
    	cb.setActive(1);
    	assertEquals(cb.getActiveText(), "Two");
    }
    
    public void testRemoveText() {
    	createComboBoxWithoutModel();
    	cb.appendText("One");
    	cb.appendText("Two");
    	cb.removeText(0);
    	cb.setActive(0);
    	assertEquals(cb.getActiveText(), "Two");
    }
    
    private ComboBox createComboBoxWithoutModel() {
        cb = new ComboBox();
        window.add(cb);
        return cb;
    }
    
    private ComboBox createComboBoxWithModel() {
        dataBool = new DataColumnBoolean();
        dataStr = new DataColumnString();
        store = new ListStore(new DataColumn[] { dataStr, dataBool });
        cb = new ComboBox(store);
        window.add(cb);
        cb.realize();
    	return cb;
    }
    
    private void addRow(String col0, boolean col1) {
        TreeIter iter = store.appendRow();
        store.setValue(iter, dataStr, col0);
        store.setValue(iter, dataBool, col1);
    }
}