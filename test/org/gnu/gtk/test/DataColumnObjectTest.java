/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtk.test;

import junit.framework.TestCase;

import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnObject;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreeStore;

public class DataColumnObjectTest extends TestCase {
    
    protected void setUp() {
        Gtk.init(null);
    }

    public void testDataColumnObject() {

        DataColumnObject objectDC = new DataColumnObject();

        DataColumn[] columns = new DataColumn[1];
        columns[0] = objectDC;

        TreeStore treeStore = new TreeStore(columns);
        TreeIter iter = treeStore.insertRow(null, 0);

        String in = new String("MyString");
        treeStore.setValue(iter, objectDC, in);

        String out = (String) treeStore.getValue(iter, objectDC);
        assertEquals(in, out);
    }
}
