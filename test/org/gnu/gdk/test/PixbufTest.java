/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gdk.test;
import junit.framework.TestCase;

import org.gnu.gdk.Pixbuf;
import org.gnu.glib.JGException;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Requisition;
import org.gnu.gtk.Window;

/**
 * 
 * @author Ismael Juma (ismael@juma.me.uk)
 *
 */
public class PixbufTest extends TestCase {
    
    protected void setUp() {
        Gtk.init(null);
    }
    
    /**
     * Simply tests whether the buffer returned by saveToBuffer is not empty.
     * @throws JGException
     */
    public void testSaveToBufferLength() throws JGException {
        Window window = new Window();
        window.showAll();
        String[] optionKeys = { "quality" };
        String[] optionValues = { "100" };
        Requisition req = window.getSize();
        Pixbuf pixbuf = new Pixbuf(window.getWindow(), window.getWindow()
                .getColormap(), 0, 0, 0, 0, req.getWidth(), req.getHeight());
        byte[] buf = pixbuf.saveToBuffer("jpeg", optionKeys, optionValues);
        assertFalse(buf.length == 0);
    }
}
